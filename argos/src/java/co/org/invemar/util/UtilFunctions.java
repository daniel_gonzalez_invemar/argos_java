package co.org.invemar.util;

import java.io.IOException;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UtilFunctions {

    public Document readXML(String file){
        
        SAXBuilder builder=new SAXBuilder();
        Document doc=null;
        try {
            doc=builder.build(file);
            
        } catch (JDOMException e) {
           
            e.printStackTrace();
        }catch (IOException e) {
            
            System.out.println(e);
        }
        
        return doc;
    }
}
