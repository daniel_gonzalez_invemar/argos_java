/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.util;

import java.sql.Connection;

/**
 *
 * @author usrsig15
 */
public class ConnectionDBFactory {
    
    public Connection getConnection(String nameConnection,String connectionResource){
        Connection con =null;
        
        if (nameConnection.equalsIgnoreCase("mysql")) {
           con=new MySQL().connect(connectionResource);
        }
        return con;
    }    
    
}
