/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.util;

import static co.org.invemar.util.ConnectionFactory.initialContext;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;



/**
 *
 * @author usrsig15
 */
public class MySQL implements Conection {

    
    
    @Override
    public Connection connect(String connectionResource) {
        InitialContext initialContext;
        DataSource data;
        Context envContext;
        Connection con=null;
        
        try {
            initialContext = new InitialContext();
            envContext = (Context) initialContext.lookup("java:comp/env");
            data = (DataSource) envContext.lookup("jdbc/" + connectionResource);
            con=data.getConnection();

        } catch (NamingException ne) {
            System.out.println("Error!! No se ha podido crear el Objeto InitialContext " + ne);
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  con;
    }

    @Override
    public boolean desconnect(Connection con) {
        boolean result=false;
        try {
            con.close();
            result=true;
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            con=null;
        }
        return  result;
       
    }
    
}
