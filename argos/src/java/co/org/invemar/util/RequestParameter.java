package co.org.invemar.util;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class RequestParameter {

	private String name = null;
	private String value = null;
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setValue(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
}
