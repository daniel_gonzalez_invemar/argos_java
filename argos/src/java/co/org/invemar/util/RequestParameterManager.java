package co.org.invemar.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class RequestParameterManager {

	private List<RequestParameter> parameterList = new ArrayList<RequestParameter>();
	private RequestParameter newRequestParameter = null;
	
	public RequestParameterManager(){
		//do nothing :)
	}
	
	public RequestParameterManager(HttpServletRequest request){
		
		//configure filters values
		Enumeration rpnames = request.getParameterNames();
		String parameter = null;
		
		while(rpnames.hasMoreElements()){
			parameter = (String)rpnames.nextElement();
			this.addRequestParameter(parameter, request.getParameter(parameter));
		}
	}
	
	public void addRequestParameter(RequestParameter rParameter){
		this.parameterList.add(rParameter);
	}
	
	public void addRequestParameter(String name, String value){
		
		this.newRequestParameter = new RequestParameter();
		
		newRequestParameter.setName(name);
		newRequestParameter.setValue(value);
		this.addRequestParameter(newRequestParameter);
		
	}
	
	public List<RequestParameter> getRequestParameterList(){
		return this.parameterList;
	}
	
	public RequestParameter getRequestParameterByName(String rpName){
		
		RequestParameter result = null;
		
		for(RequestParameter rparameter : this.parameterList){
			
			if(rparameter.getName().equals(rpName)){
				result = rparameter;
				break;
			}
		}
		
		return result;
		
	}
	
	public String getRequestParameterValueByName(String rpName){
		
		String result = null;
		
		for(RequestParameter rparameter : this.parameterList){
			
			if(rparameter.getName().equals(rpName)){
				result = rparameter.getValue();
				break;
			}
		}
		
		return result;
		
	}
}