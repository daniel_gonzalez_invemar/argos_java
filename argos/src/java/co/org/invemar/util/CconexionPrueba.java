package co.org.invemar.util;

import java.sql.*;
import java.lang.*;
import java.util.*;

/**
 * Clase de conexion a la base dedatos autor: Rafael E. Lastra C.<br> Version:
 * 1.0 <br> Fecha: 21-03-2001
 */
public class CconexionPrueba {

    private Connection aCon;
    public String Error;

    public CconexionPrueba(String user, String pwd) {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String url = "jdbc:oracle:thin:@192.168.3.77:1521:sci";
            aCon = DriverManager.getConnection(url, user, pwd);

        } catch (Exception e) {
            Error = e.getMessage();
        }
    }

    public Connection getConn() {
        return aCon;
    }

    public void close() {
        try {
            aCon.close();
        } catch (Exception e) {
            Error = e.getMessage();
        }
    }
}
