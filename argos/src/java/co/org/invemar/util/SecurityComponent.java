package co.org.invemar.util;

import java.security.MessageDigest;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class SecurityComponent {

	
	public String getSHAfromString(String textToDigest){
		
		StringBuffer result = new StringBuffer();
		
		MessageDigest md = null;
		byte[] digest = null;
		
		try{
	        md = MessageDigest.getInstance( "SHA-1" );
	        md.update( textToDigest.getBytes("8859_1") );
	        digest = md.digest();			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		int ux;
        for ( byte b : digest )
        {	ux=b & 0xff;
        	if (Integer.toHexString(ux).length() == 1) result.append("0");
        	result.append(Integer.toHexString(ux & 0xff));
	    }
		
		return result.toString();
        
	}
	
	public static void main(String[] s){
		SecurityComponent sc=new SecurityComponent();
		System.out.println("Salida: "+sc.getSHAfromString("2110E52"));
	}
}
