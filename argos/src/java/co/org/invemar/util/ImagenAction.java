package co.org.invemar.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImagenAction extends HttpServlet {

    /**
     * The doGet method of the servlet. <br>
     *
     * This method is called when a form has its tag value method equals to get.
     *
     * @param request the request send by the client to the server
     * @param response the response send by the server to the client
     * @throws ServletException if an error occurred
     * @throws IOException if an error occurred
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doPost(request, response);
    }

    /**
     * The doPost method of the servlet. <br>
     *
     * This method is called when a form has its tag value method equals to
     * post.
     *
     * @param request the request send by the client to the server
     * @param response the response send by the server to the client
     * @throws ServletException if an error occurred
     * @throws IOException if an error occurred
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codigo = request.getParameter("codigo");
        OutputStream outs = null;
        // System.out.println("iniciando jsp....");
        if (codigo != null) { 	//System.out.println("Codigo diferente de null");

            InputStream in = null;
            ResultSet RS = null;
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                ConnectionFactory cFactory;

                cFactory = new ConnectionFactory();
                //try{
                conn = cFactory.createConnection("geograficos");
			//	}catch(SQLException se){
                //	se.printStackTrace();
                //}

				//Connection CON = sibmDS.getConnection();
                //System.out.println("preparedstatement ,,, inicializando.............................................");
                pstmt = conn.prepareStatement("select logo_pro, nvl(length(logo_pro),0) from clst_proyectos where codigo =?");
                pstmt.setString(1, codigo);
                //System.out.println("preparedstatement ,,, terminado");
                RS = pstmt.executeQuery();
                // System.out.println("Antes del if");
                if (RS.next() && RS.getDouble(2) > 0) //			 if (RS.getDouble(2)>0)
                {
                    response.setContentType("image/gif");
                    in = RS.getBinaryStream(1);
                    OutputStream out4 = response.getOutputStream();
                    byte[] buffer = new byte[4096];
                    // System.out.println("antes del for");
                    for (;;) {
                        int nBytes = in.read(buffer);
                        if (nBytes == -1) {
                            break;
                        }
                        out4.write(buffer, 0, nBytes);
                    }
                    //System.out.println("Cerrando conexiones.....");
                    in.close();
                    out4.flush();
                    out4.close();
                } else {

                    String imgPath = getServletContext().getRealPath("images") + File.separator + "logo_argos.gif";
                    FileInputStream ar = new FileInputStream(imgPath);
                    int longitud = ar.available();
                    byte data[];
                    data = new byte[longitud];
                    ar.read(data);
                    ar.close();

                    response.setContentType("image/gif");

                    try {
                        outs = response.getOutputStream();

                        outs.write(data);
                    } catch (Exception err) {

                    } finally {
                        outs.flush();
                        outs.close();

                    }

		        	//System.out.println("No hay datos");
                    //throw new SQLException("image not found");
                    //response.getWriter().write("images/logo_argos.gif");
                }

            } //	      catch(ClassNotFoundException CNFE){out.println("Driver no encontrado");}
            catch (Exception ex) {//out.println("Error al cargar el logo del proyecto: " + codigo);
                //System.out.println(ex.toString());
            } finally {
                try {
                    RS.close();
                    pstmt.close();
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
