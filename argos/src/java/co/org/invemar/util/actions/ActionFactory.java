/**
 * 
 */
package co.org.invemar.util.actions;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Julian J. Pizarro
 * 
 */
public final class ActionFactory {
	private static Map<String, Action> actions = new HashMap<String, Action>();

	// This method is called by the action servlet
	public static Action getAction(String classname)
			throws ClassNotFoundException, IllegalAccessException,
			InstantiationException {
		
		Class actionFactoryClass=ActionFactory.class; 
		
		ClassLoader classLoader =actionFactoryClass.getClassLoader();
		Action action = (Action) actions.get(classname);
		if (action == null) {
			Class klass = classLoader.loadClass(classname);
			action = (Action) klass.newInstance();
			actions.put(classname, action);
		}
		return action;
	}
}
