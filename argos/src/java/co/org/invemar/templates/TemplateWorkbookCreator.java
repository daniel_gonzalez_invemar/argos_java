package co.org.invemar.templates;

import java.sql.SQLException;

import java.io.File;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.UtilFunctions;
import co.org.invemar.workbook.Area;
import co.org.invemar.workbook.Column;
import co.org.invemar.workbook.Entity;
import co.org.invemar.workbook.EntityManager;
import co.org.invemar.workbook.Link;
import co.org.invemar.workbook.Property;
import co.org.invemar.workbook.Sheet;
import co.org.invemar.workbook.SheetContent;
import co.org.invemar.workbook.SheetContentManager;
import co.org.invemar.workbook.WorkbookManager;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class TemplateWorkbookCreator {

	private javax.servlet.ServletContext servletContext;
	//template path
	private String templatePath = null;
	private String scheme = null;
	
	//user's template files paths
	private String wbXmlPath = null;
	private String viewXmlPath = null;
	private String tableXmlPath = null;
	private String infoRefXmlPath = null;
	
	//templates xml documents
	private Document workbook = null;
	private Document view = null;
	private Document table = null;
	private Document infoRef = null;
	
	//content managers
	WorkbookManager wbman = null;
	SheetContentManager viewman = null;
	SheetContentManager tableman = null;
	EntityManager inforefman = null;
	
	//util variables
	private UtilFunctions util = new UtilFunctions();
	
	//DB connection variables
	private ConnectionFactory cFactory;
	private Connection conn=null;
	
	//workbook to return
	private HSSFWorkbook templateWorkBook = null;
	
	public TemplateWorkbookCreator(String templatePath, String scheme, javax.servlet.ServletContext servletContext){
		this.templatePath = templatePath;
		this.servletContext = servletContext;
		this.scheme = scheme;
	}
	
	public HSSFWorkbook createWorkbook(){
				
		if(loadFiles()){
			
			if(createFilesObjects()){
				
				try{
					processWorkbook();
				}catch(Exception e){
					System.out.println("I could not create the worbook : createWorkbook() : "+e);
					templateWorkBook=null;
				}

			}else{
				templateWorkBook=null;
			}
		}else{
			templateWorkBook=null;
		}
		
		return templateWorkBook;
	}
	
	private void processWorkbook() throws SQLException {
		
		//create workbook
		this.templateWorkBook = new HSSFWorkbook();
		//create temp sheet
		HSSFSheet wbSheet;
		String sheetType;
		
		//
		Boolean loadinforef = this.wbman.isLoadInfoRef();
		
		//get all sheets from workbook
		List<Sheet> sheets = this.wbman.getSheets();
		
		for(Sheet sheetObj : sheets){
			//create new sheet
			wbSheet = this.templateWorkBook.createSheet(sheetObj.getName());
			sheetType = sheetObj.getType();
			
			//check sheet type and chose sheet process function
			if(sheetType.equals("inforef")){
				
				createInfoRefSheet(sheetObj, wbSheet, loadinforef);
				
			}else if(sheetType.equals("view")){
					createCustomSheet(sheetObj, wbSheet, viewman);
				}else if(sheetType.equals("table")){
						createCustomSheet(sheetObj, wbSheet, tableman);
					}
			
		}
		
	}
	
	/*holds the style options for inforef sheet header*/
	private HSSFCellStyle getInfoRefOverTitleHeaderStyle(){
		
		HSSFCellStyle style = this.templateWorkBook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		
		HSSFFont font = this.templateWorkBook.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		style.setFont(font);
		
		return style;
	}
	
	/*holds the style options for inforef sheet header*/
	private HSSFCellStyle getInfoRefTitleHeaderStyle(){
		
		HSSFCellStyle style = this.templateWorkBook.createCellStyle();
		
		style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBottomBorderColor(HSSFColor.BLACK.index);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setLeftBorderColor(HSSFColor.BLACK.index);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setRightBorderColor(HSSFColor.BLACK.index);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setTopBorderColor(HSSFColor.BLACK.index);
		
		HSSFFont font = this.templateWorkBook.createFont();
		font.setFontHeightInPoints((short) 8);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	
		style.setFont(font);
		
		return style;
		
	}
	
	/*Creates a sheet for inforef*/
	private void createInfoRefSheet(Sheet sheetObj, HSSFSheet wbSheet, boolean loadinforef) throws SQLException {
		
		//get external id areas
		List<Area> Areas = sheetObj.getAreasList();
		List<Property> properties = null;
		String sqlSentence = null;
		
		Entity entity = null;
		HSSFCell cell = null;
		HSSFCellStyle style = this.templateWorkBook.createCellStyle();
		
		//connection variables
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		int columnsCount = 0;
		int titlesColumnsCount = 0;
		
		//create row for titles
		HSSFRow overTitleRow = wbSheet.createRow(0);
		HSSFRow titleRow = wbSheet.createRow(1);
		HSSFRow infoRefRow = null; 
		int infoRefRowsCount = 2;
		
		//process by area
		for(Area area : Areas){
			
			//GET ENTITY BY ID from inforef manager
			entity = inforefman.getEntityById(area.getExternalId());
					
			properties = entity.getAttributes();//now we have the entity's properties
			
			//if title should be shown
			if(area.isShowTitle()){
				//create over title cell
				cell = overTitleRow.createCell((short)columnsCount);
				
				//enter over title cell value
				cell.setCellValue(new HSSFRichTextString(entity.getDescription().toUpperCase()));
				
				//get and set over title cell Style
				cell.setCellStyle(getInfoRefOverTitleHeaderStyle());
							 
				//merge properties columns on row 0 for title
				wbSheet.addMergedRegion(new Region(0, (short) columnsCount, 0,
						(short) (properties.size() - 1 + columnsCount)));
				
				infoRefRowsCount = 2; //index row from where inforef is going to be
			}else{
				titleRow = overTitleRow;
				infoRefRowsCount = 1;
			}
	
			titlesColumnsCount = columnsCount;//get temp columncount value and use it for putting title 
			
				//now put titles
				for(Property property : properties){
					
					//create title cell
					cell = titleRow.createCell((short)titlesColumnsCount++);
					
					//enter title cell value
					cell.setCellValue(new HSSFRichTextString(property.getAlias().toUpperCase()));

					//get and set title cell Style
					cell.setCellStyle(getInfoRefTitleHeaderStyle());
					
				}
			
			//if its necesary fill some rows with reference information
			
			if(loadinforef){
				
				try{
					
					//prepare SQL sentence
					sqlSentence = entity.getSql();
					
					if(area.getWhereCriteria()!=null){
						sqlSentence = sqlSentence + " " + area.getWhereCriteria();
					}else{
						sqlSentence = sqlSentence + " " + entity.getWhereCriteria();
					}
					
					if(area.getOrderByCriteria()!=null){
						sqlSentence = sqlSentence + " " + area.getOrderByCriteria();
					}else{
						sqlSentence = sqlSentence + " " + entity.getOrderByCriteria();
					}
					
					//get dbconnection
					cFactory = new ConnectionFactory();
					conn = cFactory.createConnection(scheme);
					pstmt = conn.prepareStatement(sqlSentence);
					rs = pstmt.executeQuery();
					
					while(rs.next()){//keep in mind each result set is a new row
						
						titlesColumnsCount = columnsCount;
						//creates new row at index 2 (third row)
						infoRefRow = wbSheet.createRow(infoRefRowsCount++);
						
						//for each property (mmm titles values :) ) we create new cell and put value
						//we are moving left to right
						for(Property property : properties){
							
							//creates cell
							cell = infoRefRow.createCell((short)titlesColumnsCount++);
							
							//enter cell value
							if (property.getType().equalsIgnoreCase("int")
									|| property.getType().equalsIgnoreCase("double")) {
								cell.setCellValue(rs.getInt(property.getAlias()));

							} else if (property.getType().equalsIgnoreCase("String")) {
								cell.setCellValue(new HSSFRichTextString(rs
										.getString(property.getAlias())));

							}
							if (property.getType().equalsIgnoreCase("Date")) {
								style.setDataFormat(HSSFDataFormat
										.getBuiltinFormat("m/d/yy"));
								cell.setCellValue(rs.getDate(property.getAlias()));

							}
							
						}
						
					}
					
				}catch(Exception e){
					System.out.println("I failed when creating inforef sheet : createInfoRefSheet(...) : "+e);
				}finally{
					if (rs != null)
						rs.close();
					if (pstmt != null)
						pstmt.close();
					if(conn!=null){
						ConnectionFactory.closeConnection(conn);
					}
					
				}
				
			}
				
				
			//increase columns count as long as properties list size plus 1 to leave a blank space between inforef
			columnsCount = columnsCount + properties.size() + 1;
		}
		
	}
	
	/*Creates a sheet for view or table*/
	private void createCustomSheet(Sheet sheetObj, HSSFSheet wbSheet, SheetContentManager sheeContentManager) throws SQLException {
		
		//get external id areas
		List<Area> Areas = sheetObj.getAreasList();
		
		SheetContent sheetContent = null;
		List<Column> columns = null;
		Column addColumn = null;
		
		HSSFCell cell = null;
		HSSFCellStyle style = this.templateWorkBook.createCellStyle();
		
		int columnsCount = 0;
		//int titlesColumnsCount = 0;
		
		//create row for titles
		HSSFRow overTitleRow = null;
		HSSFRow titleRow = null;
		HSSFRow linkRow = null; 
		
		//process by area
		for(Area area : Areas){
			
			//GET SHEET CONTENT BY ID from view manager or table manager
			sheetContent = sheeContentManager.getSheetContentById(area.getExternalId());
					
			columns = sheetContent.getColumns();
			
			if(sheetContent.getName()!=null){

				overTitleRow = wbSheet.createRow(0);
				titleRow = wbSheet.createRow(1);
				linkRow = wbSheet.createRow(2);
				
				//create over title cell
				cell = overTitleRow.createCell((short)columnsCount);
				
				//enter over title cell value
				cell.setCellValue(new HSSFRichTextString(sheetContent.getName().toUpperCase()));
				
				//get and set over title cell Style
				cell.setCellStyle(getInfoRefOverTitleHeaderStyle());
							 
				//merge properties columns on row 0 for title
				wbSheet.addMergedRegion(new Region(0, (short) columnsCount, 0,
						(short) (columns.size() - 1 + columnsCount)));

			}else{
				titleRow = wbSheet.createRow(0);
				linkRow = wbSheet.createRow(1);
			}
			//OJO CON �STA CUENTA
			//titlesColumnsCount = columnsCount;//get temp columncount value and use it for putting title 
			
				//now put titles for each column
				for(Column column : columns){
										
					//chek if it is necesary to repeat some columns
					if(column.getRepeat()>0){					
						
						for(int i=0; i<=column.getRepeat() ; i++){
							
							//create title cell
							cell = titleRow.createCell((short)columnsCount++);
							
							//enter title cell value
							cell.setCellValue(new HSSFRichTextString(column.getName().toUpperCase()));

							//get and set title cell Style
							cell.setCellStyle(getInfoRefTitleHeaderStyle());
							
							//if there are links for column
							if(!column.getLinks().isEmpty()){
								
								//create cell for link
								cell = linkRow.createCell((short)(columnsCount-1));
								
								//enter link cell value
								cell.setCellValue(new HSSFRichTextString(processColumnLinkList(column.getLinks())));
							}
							
							//if it is necesary to repeat some next columns with actual one
							if(column.getAddColsToR()>0){
								
								//start to put new columns a their titles
								for(int j=0; j<column.getAddColsToR(); j++){
									
									//get next column from columns list
									addColumn = columns.get(column.getId()+j);
									
									//create title cell
									cell = titleRow.createCell((short)columnsCount++);
									
									//enter title cell value
									cell.setCellValue(new HSSFRichTextString(addColumn.getName().toUpperCase()));

									//get and set title cell Style
									cell.setCellStyle(getInfoRefTitleHeaderStyle());
									
									//if there are links for column
									if(!addColumn.getLinks().isEmpty()){
										
										//create cell for link
										cell = linkRow.createCell((short)(columnsCount-1));
										
										//enter link cell value
										cell.setCellValue(new HSSFRichTextString(processColumnLinkList(addColumn.getLinks())));
									}
								}
								
							}
							
						}
						
					}else{//if no repeat needed
						
						if(column.getRepeat()!=-1){//if column have not been repeated
							
							//create title cell
							cell = titleRow.createCell((short)columnsCount++);
							
							//enter title cell value
							cell.setCellValue(new HSSFRichTextString(column.getName().toUpperCase()));

							//get and set title cell Style
							cell.setCellStyle(getInfoRefTitleHeaderStyle());	
							
							//if there are links for column
							if(!column.getLinks().isEmpty()){
								
								//create cell for link
								cell = linkRow.createCell((short)(columnsCount-1));
								
								//enter link cell value
								cell.setCellValue(new HSSFRichTextString(processColumnLinkList(column.getLinks())));
							}
						}

					}

				}
				
			//increase columns count plus 1 to leave a blank space between sheet content
			columnsCount = columnsCount + 1;
		}
		
	}

	private String processColumnLinkList(List<Link> links){
		
		String linkString = "";
		String linkText = "";
		Sheet sheet = null;
		Entity entity = null;
		SheetContent sheetContent = null;
		
		for(Link link : links){
			
			linkText = "Enlace con ";
			
			//get sheet from workbook manager
			sheet = this.wbman.getSheetById(link.getSheetId());
			
			//process by type
			if(sheet.getType().equals("inforef")){
				
				linkText = linkText + "info de referencia: nombre libro: "+ sheet.getName()+", ";
				entity = inforefman.getEntityById(link.getExternalId());
				linkText = linkText+"elemento: "+entity.getName();	
				
			}else if(sheet.getType().equals("view")){
				
					linkText = linkText + "vista usuario: nombre libro: "+ sheet.getName()+", ";
					sheetContent = viewman.getSheetContentById(link.getExternalId());
					
					linkText = linkText+"columna: "+sheetContent.getColumnById(link.getColumnId()).getName();	
					
				  }else if(sheet.getType().equals("table")){
						
						linkText = linkText + " tabla: nombre libro: "+ sheet.getName()+", ";
						sheetContent = tableman.getSheetContentById(link.getExternalId());
						
						linkText = linkText+"tabla: "+sheetContent.getName();	
						
					  }
			
			linkString = linkString+linkText+" ; ";
			
		}
		
		return linkString;
	}



	
	/*get file path and get xml documents*/
	private boolean loadFiles(){
		
		boolean result = true;
		
		wbXmlPath = templatePath+File.separator+"workbook.xml";
		viewXmlPath = templatePath+File.separator+"view.xml";
		tableXmlPath = templatePath+File.separator+"table.xml";
		infoRefXmlPath = servletContext.getRealPath("xmlconfig")+File.separator+this.scheme+File.separator+"inforef.xml";
		
		try{
			this.workbook= util.readXML(wbXmlPath);
			this.view= util.readXML(viewXmlPath);
			this.table= util.readXML(tableXmlPath);
			this.infoRef= util.readXML(infoRefXmlPath);
			
		}catch(Exception e){
			result = false;
			System.out.println("Exception: I couldn't find some books ; "+e);
		}
		
		return result;
	}
	
	/*create manager objects from xml documents*/
	private boolean createFilesObjects(){
		
		boolean result = true;
		
		try{
			wbman = new WorkbookManager(this.workbook);
			viewman = new  SheetContentManager(this.view);
			tableman = new  SheetContentManager(this.table);
			inforefman = new EntityManager(this.infoRef);
			
			/*Entity test = inforefman.getEntityById(18);
			
			System.out.println("id:"+test.getId()+" name:"+test.getName()+" description:"+test.getDescription());*/
			
		}catch(Exception e){
			result = false;
			System.out.println("Exception: I couldn't create some books ; "+e);
			e.printStackTrace();
		}
		
		return result;
		
	}

}
