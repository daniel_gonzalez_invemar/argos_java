package co.org.invemar.templates;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;

import co.org.invemar.util.UtilFunctions;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class TemplatesAjaxRequest extends HttpServlet {

	
	private javax.servlet.ServletContext servletContext;
	private UtilFunctions util = new UtilFunctions();
	private String templatesPath = null;
	
	/**
	 * Constructor of the object.
	 */
	public TemplatesAjaxRequest() {
		super();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occure
	 */
	public void init(ServletConfig config) throws ServletException {
		servletContext = config.getServletContext();
		templatesPath =  servletContext.getRealPath("templates");
		super.init(config);
	}
	
	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		JSONArray templates = null;
		
		if(action.equals("getTemplates")){
			
			templates = getTemplates();
			if(templates!=null){
				response.setContentType("application/json");
				response.setHeader("Cache-Control", "no-cache");
				response.getWriter().write(templates.toString());				
			}else{
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}
		}else if(action.equals("getTemplateDoc")){
			
			getTemplateDoc(request.getParameter("id"), response);
			
		}else {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/*reads the templates xml and return availables*/
	private JSONArray getTemplates(){
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject;
		
		Document templates = util.readXML(templatesPath+File.separator+"template.xml");
		Element root = templates.getRootElement();
		List<Element> rootChildren=root.getChildren();
		
		try{
			for(Element element:rootChildren){
				
				jsonObject = new JSONObject();
				jsonObject.put("id", element.getAttributeValue("id"));
				jsonObject.put("name", element.getAttributeValue("name"));
				jsonObject.put("description", element.getAttributeValue("description"));
				jsonArray.put(jsonObject);			
			}
		}catch(JSONException e){System.out.println("Exception: "+e);}
		
		return jsonArray;
		
	}
	
	/*process excel doc for an specific template*/
	private void getTemplateDoc(String templateId, HttpServletResponse response)throws ServletException, IOException {
		 
		TemplateWorkbookCreator creator;
		String templatePath;
		String scheme;
		String fileName = "template.xls";
		
		Element telement = getTemplateElementFromXML(templateId);
		
		if(telement!=null){
			templatePath = templatesPath+File.separator+telement.getAttributeValue("folder");
			scheme = telement.getAttributeValue("scheme"); 
			creator = new TemplateWorkbookCreator(templatePath, scheme, servletContext);
			HSSFWorkbook templateWorkbook = creator.createWorkbook();
			
			if(templateWorkbook!=null){
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=\""
						+ fileName + "\"");
				response.setHeader("Pragma", "no-cache");

				ServletOutputStream outs = response.getOutputStream();
				templateWorkbook.write(outs);
				outs.flush();
				outs.close();
			}else{
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}
			
		}else{
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
		
	}
	
	/*gets the template element from templates configuration xml file*/
	private Element getTemplateElementFromXML(String templateId){
		
		Element result = null;
		
		Document templates = util.readXML(templatesPath+File.separator+"template.xml");
		
		Element root = templates.getRootElement();
		List<Element> rootChildren=root.getChildren();

		for(Element element: rootChildren){
			
			if(element.getAttribute("id").getValue().equals(templateId))
				result = element;
			
		}
		
		return result;
	}
	

}
