/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.ushahidi;

/**
 *
 * @author usrsig15
 */
public class Category {
    private String id;
    private String categoryTitle;
    private String categoryVisible;
    private String categoryDescription;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryVisible() {
        return categoryVisible;
    }

    public void setCategoryVisible(String categoryVisible) {
        this.categoryVisible = categoryVisible;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }
    
    
}
