/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.ushahidi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import co.org.invemar.library.siam.Utilidades;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class ManagmentFormFieldUshahidi {

    public String deleteItemFormField(String value, String tableName, Connection con, long id) throws SQLException {
        String newString = "";
        String storeValue = findDefaultValueFormField(tableName, con, id);
        Utilidades u = new Utilidades();
        newString = u.FindStringAndReplaceByOtherStringWithSeparator("1", storeValue, value, "", true);
        updateFormField(tableName, con, newString, id);
        return newString;
    }

    public boolean cleanFormField(String tableName, Connection con, long id) throws SQLException {
        boolean result = false;
        PreparedStatement pstmt = null;
        String query = "UPDATE " + tableName + " SET field_default='Seleccione...' WHERE id=?";
        pstmt = con.prepareStatement(query);
        pstmt.setLong(1, id);
        int rowsUpdate = pstmt.executeUpdate();
        if (rowsUpdate > 0) {          
            result = true;
        }
        return result;
    }
    
    public Category getCategoryParent(String categoryId,Connection con){        
        
       
         Category category = new Category();
         
        try {          
            
            String query="SELECT * FROM `sigmacategory` where id=(select parent_id from sigmacategory where id=?) and category_visible=1";
            PreparedStatement pstmt = null;
            pstmt = con.prepareStatement(query);
            pstmt.setString(1,categoryId);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){               
                 category.setCategoryVisible(rs.getString("category_visible"));
                 category.setCategoryTitle(rs.getString("category_title"));
                 category.setCategoryDescription(rs.getString("category_description"));
                   
            }            
           
        } catch (SQLException ex) {
            Logger.getLogger(ManagmentFormFieldUshahidi.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
       return category;
    }
    public  String  getJsonCategory(Category category){     
    
      JSONObject jsonObject = new JSONObject();  
        try {            
                      
            jsonObject.put("category_title",category.getCategoryTitle());
            jsonObject.put("category_visible",category.getCategoryVisible());
            jsonObject.put("category_description",category.getCategoryDescription());
            jsonObject.put("id",category.getId());
        } catch (JSONException ex) {
            Logger.getLogger(ManagmentFormFieldUshahidi.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
      
      return jsonObject.toString();
    }
    

    public boolean updateFormField(String tableName, Connection con, String value, long id) throws SQLException {
        boolean result = false;
        PreparedStatement pstmt = null;
        String query = "UPDATE " + tableName + " SET field_default=? WHERE id=?";
        pstmt = con.prepareStatement(query);
        pstmt.setString(1, value);
        pstmt.setLong(2, id);
        int rowsUpdate = pstmt.executeUpdate();
        if (rowsUpdate > 0) {          
            result = true;
        }
        return result;
    }

    public String findDefaultValueFormField(String tableName, Connection con, long id) throws SQLException {
        String defualtValue = "";

        PreparedStatement pstmt = null;
        String query = "SELECT field_default FROM " + tableName + " WHERE id =?";

        pstmt = con.prepareStatement(query);
        pstmt.setLong(1, id);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            defualtValue = rs.getString("field_default");
        }

        return defualtValue;
    }

    public void fillFieldManagmentUnit(Connection ConDbManglar, Connection dbUshahidi) {

        Map<String, String> departamentAndInitial = null;
        departamentAndInitial = new HashMap<String, String>();
        departamentAndInitial.put("80000000", "ATL");
        departamentAndInitial.put("50000000", "ANT");
        departamentAndInitial.put("130000000", "BOL");
        departamentAndInitial.put("190000000", "CAU");
        departamentAndInitial.put("270000000", "CHO");
        departamentAndInitial.put("230000000", "COR");
        departamentAndInitial.put("440000000", "GUA");
        departamentAndInitial.put("470000000", "MAG");
        departamentAndInitial.put("520000000", "NAR");
        departamentAndInitial.put("880000000", "SAI");
        departamentAndInitial.put("700000000", "SUC");
        departamentAndInitial.put("760000000", "VAL");

        PreparedStatement pstmt = null;
        String query = "SELECT * "
                + "FROM "
                + "  (SELECT cs.activo,cs.id, "
                + "    cs.id_car, "
                + "    cs.tipo, "
                + "    cs.nombre,"
                +"(SELECT nombre "
                +" FROM datos_casos_estudio "
                +" WHERE codigo_atributo=27 "
                +" AND id               =cs.id1"
                +") AS nombresector,"
                + "    (SELECT trim(valor2) "
                + "    FROM datos_casos_estudio "
                + "    WHERE codigo_atributo=27 "
                + "    AND id               =cs.id1 "
                + "    ) AS coddepartamento, "
                + "    (SELECT toponimo_texto "
                + "    FROM geograficos.clst_toponimiat "
                + "    WHERE toponimo_tp=( "
                + "      (SELECT trim(valor2) "
                + "      FROM datos_casos_estudio "
                + "      WHERE codigo_atributo=27 "
                + "      AND id               =cs.id1 "
                + "      )) "
                + "    ) AS depart "
                + "  FROM casosestudio cs "
                + "  ) "
                + "WHERE tipo =2  and activo='Activado'  and id_car<>'UT' and coddepartamento is not null "
                + "ORDER BY depart,id ASC";

        try {

            pstmt = ConDbManglar.prepareStatement(query);
         
            ResultSet rs = pstmt.executeQuery();
            cleanFormField("sigmaform_field", dbUshahidi, 8);

               
            String storeValue=null;
            while (rs.next()) {
                String codDepartment = departamentAndInitial.get(rs.getString("coddepartamento"));                
                storeValue=findDefaultValueFormField("sigmaform_field", dbUshahidi, 8);                
                storeValue=storeValue+"," + rs.getString("id") + "_" + codDepartment + "_" + rs.getString("nombresector")+"_"+rs.getString("nombre");
                updateFormField("sigmaform_field", dbUshahidi, storeValue, 8);
            }
           

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage());

        } finally {
            try {
                dbUshahidi.close();
                dbUshahidi = null;
                ConDbManglar.close();
                ConDbManglar = null;
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage());
            }
        }

    }

    public void fillFieldSectores(Connection ConDbManglar, Connection dbUshahidi) {

        Map<String, String> departamentAndInitial = null;
        departamentAndInitial = new HashMap<String, String>();
        departamentAndInitial.put("80000000", "ATL");
        departamentAndInitial.put("50000000", "ANT");
        departamentAndInitial.put("130000000", "BOL");
        departamentAndInitial.put("190000000", "CAU");
        departamentAndInitial.put("270000000", "CHO");
        departamentAndInitial.put("230000000", "COR");
        departamentAndInitial.put("440000000", "GUA");
        departamentAndInitial.put("470000000", "MAG");
        departamentAndInitial.put("520000000", "NAR");
        departamentAndInitial.put("880000000", "SAI");
        departamentAndInitial.put("700000000", "SUC");
        departamentAndInitial.put("760000000", "VAL");

        PreparedStatement pstmt = null;

        String query = "SELECT * "
                + "FROM "
                + "  (SELECT cs.activo, cs.id, "
                + "    cs.id_car, "
                + "    cs.tipo, "
                + "    cs.nombre, "
                + "    (SELECT trim(valor2) "
                + "    FROM datos_casos_estudio "
                + "    WHERE codigo_atributo=27 "
                + "    AND id               =cs.id "
                + "    ) AS coddepartamento, "
                + "    (SELECT toponimo_texto "
                + "    FROM geograficos.clst_toponimiat "
                + "    WHERE toponimo_tp=( "
                + "      (SELECT trim(valor2) "
                + "      FROM datos_casos_estudio "
                + "      WHERE codigo_atributo=27 "
                + "      AND id               =cs.id "
                + "      )) "
                + "    ) AS depart "
                + "  FROM casosestudio cs "
                + "  ) "
                + "WHERE tipo =86 and activo='Activado'  AND id_car<>'UT'  ORDER BY depart,id ASC";
        try {

            pstmt = ConDbManglar.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();

            cleanFormField("sigmaform_field", dbUshahidi, 16);

            while (rs.next()) {

                String codDepartment = departamentAndInitial.get(rs.getString("coddepartamento"));

                //System.out.println("toponimo:" + codDepartment);
                String storeValue = findDefaultValueFormField("sigmaform_field", dbUshahidi, 16);

                //System.out.println("storeValue:" + storeValue);
                storeValue = storeValue + "," + rs.getString("id") + "_" + codDepartment + "_" + rs.getString("nombre");

                //System.out.println("new storeValue:" + storeValue);
                updateFormField("sigmaform_field", dbUshahidi, storeValue, 16);
            }

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage());

        } finally {
            try {
                dbUshahidi.close();
                dbUshahidi = null;
                ConDbManglar.close();
                ConDbManglar = null;
            } catch (SQLException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage());
            }
        }

    }

    boolean getJsonObject(Category c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
