package co.org.invemar.log;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;

import co.org.invemar.user.UserPermission;
import co.org.invemar.util.RequestParameterManager;
import co.org.invemar.util.UtilFunctions;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class LogAdminAction {
	
	private LogAdminInfoRequestController infoRequest = new LogAdminInfoRequestController(null);
	
	private HttpSession session = null;	
	
	private String username = null;
	private UserPermission userp = null;
	private String logPermission = null;


	public LogAdminAction(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/text; charset=ISO-8859-1");
		response.addHeader("Expires", "0");
	    response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
	    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
	    response.addHeader("Pragma", "no-cache");
		
		session=request.getSession();
		JSONArray result = null;
		
    	try{
        	username = session.getAttribute("username").toString();
        	userp = (UserPermission)session.getAttribute("userpermission");
    	}catch(Exception e){
    		username = null;
       	}
    	
    	if(username==null){
    		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    		return;
    	}else{
			//check user permission
			UserPermission userp = (UserPermission)session.getAttribute("userpermission");
    	
			if(userp.couldUser(UserPermission.VER_MI_HISTORIAL_DE_EVENTOS_PARA_EL_PROYECTO_ACTUAL)){
				logPermission = "user";
			}else if(userp.couldUser(UserPermission.VER_HISTORIAL_DE_EVENTOS_DE_LA_ENTIDAD_PARA_EL_PROYECTO_ACTUAL)){
				logPermission = "project";
			}else{
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	    		return;
			}
    	}
    	
    	String action = request.getParameter("action");
    	
		if(action.equals("getThematics")){
			
			try{
				int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
								
				result = infoRequest.getThematicsByProject(projectId);
				
				if(result!=null){
					response.setContentType("application/json");
					response.getWriter().write(result.toString());
				}else{
					response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				}				
			}catch(Exception e){
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}

		}else if(action.equals("getLog")){
			
			response.setContentType("application/json");

			int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
			int thematicId = Integer.parseInt(request.getParameter("thematicid"));
			int userId = Integer.parseInt(request.getParameter("userid"));
			int logType = Integer.parseInt(request.getParameter("logtype"));
			
			try{
				
				if(logPermission.equals("user")){
					
					userId = Integer.parseInt(session.getAttribute("userid").toString());
					
					result = infoRequest.getUserLog(projectId, thematicId, logType, userId);
					
				}else if(logPermission.equals("project")){
					
					if(userId==-1){
						userId = Integer.parseInt(session.getAttribute("userid").toString());
						result = infoRequest.getUserLog(projectId, thematicId, logType, userId);
					}else if(userId==0){
						result = infoRequest.getAllUserLog(projectId, thematicId, logType);
					}else{
						result = infoRequest.getUserLog(projectId, thematicId, logType, userId);
					}
					
				}
				
				response.getWriter().write(result.toString());
				
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("getUsers")){
			
			response.setContentType("application/json");

			int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
			String entityId = session.getAttribute("entityid").toString();
			int userId = Integer.parseInt(session.getAttribute("userid").toString());

			try{
				
				if(logPermission.equals("user")){
					
					response.getWriter().write("NOU");
					
				}else if(logPermission.equals("project")){
					
					result = infoRequest.getAllowedUserInformation(projectId, entityId, userId);
					
					response.getWriter().write(result.toString());
				}
				
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
		}else if(action.equals("getLogTypes")){
			
			try{
								
				result = infoRequest.getLogTypes();
				
				if(result!=null){
					response.setContentType("application/json");
					response.getWriter().write(result.toString());
				}else{
					response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				}				
			}catch(Exception e){
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}

		}
    	
    	
		/*
		xmlConfigPath =  servletContext.getRealPath("xmlconfig");
		
		String action = request.getParameter("action");
		JSONArray result = null;
		RequestParameterManager rpManager= null;
		
		if(action.equals("getThematics")){
			
			try{
				int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
								
				result = infoRequest.getThematicsByProject(projectId);
				
				if(result!=null){
					response.setContentType("application/json");
					response.getWriter().write(result.toString());
				}else{
					response.setStatus(HttpServletResponse.SC_NO_CONTENT);
				}				
			}catch(Exception e){
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}

		}else if(action.equals("getInfoRefDoc")){
			
			//create a new request parameter manager to hold all parameter - specially filters ;)
			rpManager = new RequestParameterManager(request);
			
			String schema = rpManager.getRequestParameterValueByName("schema");//request.getParameter("schema");
			int thematicId = Integer.parseInt(rpManager.getRequestParameterValueByName("thematicid"));

			this.getInfoRefDoc(schema, thematicId, rpManager, response);
			
		}else if(action.equals("getFilterInfo")) {
			
			System.out.println(request.getParameter("schema")+ request.getParameter("infoType"));
			getFilterInfo(request.getParameter("schema"), request.getParameter("infoType"), response);
			
		}else if(action.equals("getAddNewInfoRefDoc")) {
			
			int thematicId = Integer.parseInt(request.getParameter("thematicid"));
			this.getAddNewInfoRefDoc(request.getParameter("schema"), thematicId, null, response);
				
		}else if(action.equals("getUploadTableNames")) {
					
			result = this.getUploadTableNames(request.getParameter("schema"));
			
			if(result!=null){
				response.setContentType("application/json");
				response.setHeader("Cache-Control", "no-cache");
				response.getWriter().write(result.toString());				
			}else{
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}
			
		}else if(action.equals("getUserInformation")){
			
			response.setContentType("application/json");
			
			try{
				if(userp.couldUser(UserPermission.ACTUALIZAR_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)){
					int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
					int userId = Integer.parseInt(session.getAttribute("userid").toString());
					String entityId = session.getAttribute("entityid").toString();
					response.getWriter().write(infoRequest.getAllowedUserInformation(projectId, entityId,userId).toString());
				}else {
					response.getWriter().write("NO");
				}
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}*/
		
		
	}


	
}
