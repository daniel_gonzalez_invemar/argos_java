package co.org.invemar.log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import co.org.invemar.util.ConnectionFactory;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class LogCreator {
	
	public static final int INGRESO_DE_DATOS_POR_ASISTENTE = 1;
	public static final int INGRESO_DE_DATOS_MASIVAMENTE = 2;
	public static final int ACTUALIZACION_DE_DATOS = 3;
	public static final int DESCARGA_DE_DATOS = 4;
	
	private static final String SQL_LOG = "INSERT INTO UHISTORIAL (id_tipo, id_usuario, id_proyecto, id_tematica, fecha, registros) values (?,?,?,?,sysdate,?)";
	
	private ConnectionFactory cFactory = null;
	private Connection conn = null;
	
	public void createLog(int typeId, int userId, int projectId, int thematicId, int regs) throws SQLException{
		
		PreparedStatement pstmt = null;
		int rows = 0;
		
		try{
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			pstmt = conn.prepareStatement(SQL_LOG);
			pstmt.setInt(1, typeId);
			pstmt.setInt(2, userId);
			pstmt.setInt(3, projectId);
			pstmt.setInt(4, thematicId);
			pstmt.setInt(5, regs);
			
			pstmt.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();

		}finally{
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}
		}
		
	}
	
	
}
