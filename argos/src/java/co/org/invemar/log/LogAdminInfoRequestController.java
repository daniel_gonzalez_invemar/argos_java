package co.org.invemar.log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.ConnectionFactory;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class LogAdminInfoRequestController {

	//DB connection variables
	private ConnectionFactory cFactory;
	private Connection conn=null;
	private String schema = null;	
	
	public LogAdminInfoRequestController(String schema){
		this.schema = schema;
	}
	
	/*get thematics related with a specific project*/
	public JSONArray getThematicsByProject(int projectId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT BUBICACION_TEMATICA.CODIGO, BUBICACION_TEMATICA.DESCRIPCION, LOWER(BUBICACION_TEMATICA.ESQUEMA) FROM (BUBICACION_TEMATICA INNER JOIN BPROYECTO_X_COMP ON BPROYECTO_X_COMP.ID_COMPONENTE=BUBICACION_TEMATICA.CODIGO) WHERE BPROYECTO_X_COMP.ID_PROYECTO=?");
			pstmt.setInt(1, projectId);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("name", rs.getString(2));
				jsonObject.put("folder", rs.getString(1)+";"+rs.getString(3));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}
	
	/*returns user information but just those that user is allowed to see*/
	public JSONArray getAllowedUserInformation(int projectId, String entityId, int userId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT USUARIOS.ID, USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO FROM (((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) WHERE UROLES.ID_PROYECTO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_DETALLES.VALOR_ASIGNADO=? AND USUARIOS.ID!=? ORDER BY USUARIOS.USERNAME_EMAIL ASC");
			pstmt.setInt(1, projectId);
			pstmt.setString(2, entityId);
			pstmt.setInt(3, userId);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("name", rs.getString(2));
				
				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}	
	
	/*get log types*/
	public JSONArray getLogTypes() throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT ID, DESCRIPCION FROM UHISTORIAL_TIPO WHERE UHISTORIAL_TIPO.id!=4 ORDER BY ID ASC");
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("description", rs.getString(2));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}
	
	/*get all users log*/
	public JSONArray getAllUserLog(int projectId, int thematicId, int logType) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			if(logType>0){
				pstmt = conn.prepareStatement("SELECT USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO, UHISTORIAL_TIPO.DESCRIPCION, UHISTORIAL.REGISTROS, TO_CHAR(UHISTORIAL.FECHA, 'dd/mm/yyyy HH:MM:AM') FROM ((UHISTORIAL INNER JOIN USUARIOS ON UHISTORIAL.ID_USUARIO=USUARIOS.ID) INNER JOIN UHISTORIAL_TIPO ON UHISTORIAL.ID_TIPO=UHISTORIAL_TIPO.ID) WHERE UHISTORIAL.ID_PROYECTO=? AND UHISTORIAL.ID_TEMATICA=? AND UHISTORIAL.ID_TIPO=? ORDER BY UHISTORIAL.FECHA DESC");
				pstmt.setInt(1, projectId);
				pstmt.setInt(2, thematicId);
				pstmt.setInt(3, logType);
			}else{
				pstmt = conn.prepareStatement("SELECT USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO, UHISTORIAL_TIPO.DESCRIPCION, UHISTORIAL.REGISTROS, TO_CHAR(UHISTORIAL.FECHA, 'dd/mm/yyyy HH:MM:AM') FROM ((UHISTORIAL INNER JOIN USUARIOS ON UHISTORIAL.ID_USUARIO=USUARIOS.ID) INNER JOIN UHISTORIAL_TIPO ON UHISTORIAL.ID_TIPO=UHISTORIAL_TIPO.ID) WHERE UHISTORIAL.ID_PROYECTO=? AND UHISTORIAL.ID_TEMATICA=? ORDER BY UHISTORIAL.FECHA DESC");
				pstmt.setInt(1, projectId);
				pstmt.setInt(2, thematicId);				
			}

			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("name", rs.getString(1));
				jsonObject.put("description", rs.getString(2));
				jsonObject.put("registers", rs.getString(3));
				jsonObject.put("date", rs.getString(4));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}
	
	/*get a specific user log*/
	public JSONArray getUserLog(int projectId, int thematicId, int logType, int userId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			if(logType>0){
				pstmt = conn.prepareStatement("SELECT USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO, UHISTORIAL_TIPO.DESCRIPCION, UHISTORIAL.REGISTROS, TO_CHAR(UHISTORIAL.FECHA, 'dd/mm/yyyy HH:MM:AM') FROM ((UHISTORIAL INNER JOIN USUARIOS ON UHISTORIAL.ID_USUARIO=USUARIOS.ID) INNER JOIN UHISTORIAL_TIPO ON UHISTORIAL.ID_TIPO=UHISTORIAL_TIPO.ID) WHERE UHISTORIAL.ID_PROYECTO=? AND UHISTORIAL.ID_TEMATICA=? AND UHISTORIAL.ID_USUARIO=? AND UHISTORIAL.ID_TIPO=? ORDER BY UHISTORIAL.FECHA DESC");
				pstmt.setInt(1, projectId);
				pstmt.setInt(2, thematicId);
				pstmt.setInt(3, userId);
				pstmt.setInt(4, logType);
			}else{
				pstmt = conn.prepareStatement("SELECT USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO, UHISTORIAL_TIPO.DESCRIPCION, UHISTORIAL.REGISTROS, TO_CHAR(UHISTORIAL.FECHA, 'dd/mm/yyyy HH:MM:AM') FROM ((UHISTORIAL INNER JOIN USUARIOS ON UHISTORIAL.ID_USUARIO=USUARIOS.ID) INNER JOIN UHISTORIAL_TIPO ON UHISTORIAL.ID_TIPO=UHISTORIAL_TIPO.ID) WHERE UHISTORIAL.ID_PROYECTO=? AND UHISTORIAL.ID_TEMATICA=? AND UHISTORIAL.ID_USUARIO=? ORDER BY UHISTORIAL.FECHA DESC");
				pstmt.setInt(1, projectId);
				pstmt.setInt(2, thematicId);
				pstmt.setInt(3, userId);
			}
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("name", rs.getString(1));
				jsonObject.put("description", rs.getString(2));
				jsonObject.put("registers", rs.getString(3));
				jsonObject.put("date", rs.getString(4));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}
	
}
