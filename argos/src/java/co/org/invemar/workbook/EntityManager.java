package co.org.invemar.workbook;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import co.org.invemar.util.UtilFunctions;
/**
 *
 * @author ADMINISTRADOR
 *
 *
 */


public class EntityManager {
    
    private List<Entity> entities=new ArrayList<Entity>();
    
    private StringBuffer sql;
    private StringBuffer from;
    private StringBuffer where;
    private StringBuffer orderby;
    private Entity newEntity;
    private Property property;
    private List<Property> attributes;
    private Document entityXml = null;
    
	private UtilFunctions util = new UtilFunctions();
	
	public EntityManager(Document entityXml){
		
		this.entityXml = entityXml;
		
		createObjects();
	}
	
    ///////*****HOLD THIS FOR BACKWARD COMPATIBILITY - START
	
	public EntityManager(){
		/*nothing, nothing!!!, could you believe it?*/
	}
	
    ///////*****HOLD THIS FOR BACKWARD COMPATIBILITY - END
	
    private Entity makeEntity(Element entity){
        
        newEntity =new Entity();
        List<Element> elementChildren = null;
        int numcols = 0;
        int i = 0;
        
        /*where=null;
        orderby=null;*/
        
        sql=new StringBuffer("select ");
        
        //get id
        Attribute id=entity.getAttribute("id");
        newEntity.setId(Integer.parseInt(id.getValue()));
        
        //get name
        Attribute name=entity.getAttribute("name");
        newEntity.setName(name.getValue());
               
        from=new StringBuffer(" from ");
        from.append(name.getValue());
        
        //get description
        Attribute description=entity.getAttribute("descripcion");
        newEntity.setDescription(description.getValue());
        
        //get schema
        Attribute schema=entity.getAttribute("schema");
  
        newEntity.setSchema(schema!=null?schema.getValue():null);
        
        //download options      
	        //get project id column
	        Attribute projectIdCol=entity.getAttribute("projectidcol");
	        newEntity.setProjectIdColumn(projectIdCol!=null?projectIdCol.getValue():null);
	        //get thematic id column
	        Attribute thematicIdCol=entity.getAttribute("thematicidcol");
	        newEntity.setThematicIdColumn(thematicIdCol!=null?thematicIdCol.getValue():null);
	        
        //get columns
        elementChildren=entity.getChildren();
        
        for(Element element :elementChildren){
            
            if(element.getName().equalsIgnoreCase("columns")){
                
            	//getting columns
                attributes= new ArrayList<Property>();
                
                List<Element> entityColumns=element.getChildren();
                
                numcols = entityColumns.size();
                
                i = 0;
                
                //making attributes
                for(Element column : entityColumns){
                	attributes.add(makeProperty(column,i++,numcols));
                }
                
                newEntity.setAttributes(attributes);
                sql.append(from.toString());
                
            }else if(element.getName().equalsIgnoreCase("where")){
                
                Attribute criteria=element.getAttribute("criteria");
                //where=new StringBuffer(" where ");
                //where.append(criteria.getValue());
                newEntity.setWhereCriteria(" where "+criteria.getValue());

            }else if(element.getName().equalsIgnoreCase("orderby")) {
                
                Attribute criteria=element.getAttribute("criteria");

                //orderby=new StringBuffer(" order by ");
                //orderby.append(criteria.getValue());
                
                newEntity.setOrderByCriteria(" order by "+criteria.getValue());
            }
        	
        }
        
        /*if(where!=null){
            sql.append(where);
        }
        
        if(orderby!=null){
            sql.append(orderby);
        }*/
        
        newEntity.setSql(sql.toString());

        return newEntity;
        
    }
    
    private Property makeProperty(Element column, int i,int num){
        property=new Property();
        Attribute nameC=column.getAttribute("name");
        property.setName(nameC.getValue());
        //System.out.println("�ttribute name:.. "+nameC.getValue());
        
        Attribute alias=column.getAttribute("alias");
        
        if(alias==null){
            property.setAlias(nameC.getValue());
        }else {
            property.setAlias(alias.getValue());
        }
        
        Attribute type=column.getAttribute("type");
        property.setType(type.getValue());
        //System.out.println("Attribute type: .. "+type.getValue());
        
        sql.append(nameC.getValue()+" ") ;
        if(i<num-1){
            sql.append(", ");
            
        }
        return property;
        
    }
    
    private void createObjects(){
        
    	Element root=entityXml.getRootElement();
    	
    	List<Element> childrenroot=root.getChildren();
        Iterator<Element> iterator=childrenroot.iterator();
        
        for(Element entity: childrenroot){
        	entities.add(makeEntity(entity));
        }
        
    }
    
    public Entity getEntity(int position){
        
        return entities.get(position);
    }
    
    public List<Entity> getEntities(){
        return entities;
    }
    
    public Entity getEntityById(int id){
    	
    	Entity result = null;
    	
    	for(Entity entity : this.entities){
    		
    		if(entity.getId()==id){
    			result = entity;
    			break;
    		}
    		
    	}
    	
    	return result;
    	
    }

    
    ///////*****HOLD THIS FOR BACKWARD COMPATIBILITY - START
    
    public void createEntities(String file){
        
    	this.entityXml = util.readXML(file);
       
    	createObjects();
        
    }
    
    public void createEntities(Document entityXml){
    	
    	this.entityXml = entityXml;
    	
    	createObjects();
    }

    ///////*****HOLD THIS FOR BACKWARD COMPATIBILITY - END
}
