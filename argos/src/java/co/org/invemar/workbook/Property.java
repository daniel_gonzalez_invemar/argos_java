package co.org.invemar.workbook;

/**
 * @author Julian Jose Pizarro Pertuz
 *
 */
public class Property {
    
    private String name;
    private String type;
    private String alias;
    
    
    /**
     *
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    
    public String getAlias() {
        return alias;
    }
    
    public void setAlias(String alias) {
        this.alias = alias;
    }
    
    
}
