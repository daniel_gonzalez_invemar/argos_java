package co.org.invemar.workbook;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates a workbook with its sheets structure, 
 * in fact it binds Sheet objects to itself
 * with the configuration of each book's sheet.
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class WorkbookManager {
	
	private boolean loadinforef = false;
	private List<Sheet> sheetList = new ArrayList<Sheet>();
	private Document contentXml = null;
	
	public WorkbookManager(Document contentXml){
		this.contentXml = contentXml;
		createObjects();
	}
	

	private void createObjects(){
		//get root
		Element root = this.contentXml.getRootElement();
		
		this.loadinforef = Boolean.parseBoolean(root.getAttributeValue("loadinforef"));
		
		List<Element> rootChildren = root.getChildren();
		
		//create sheet contents
		for(Element element : rootChildren){

			sheetList.add(createSheet(element));
		}
	}
	
	private Sheet createSheet(Element element){
		
		Sheet sheet = new Sheet();
		Area areaObj = null;
		WhereFilter wFilterObj = null;
		
		//get id
		sheet.setId(Integer.parseInt(element.getAttributeValue("id")));
		//get name
		sheet.setName(element.getAttributeValue("name"));
		//get type
		sheet.setType(element.getAttributeValue("type"));
		//get hidden
		sheet.setHidden(Boolean.parseBoolean(element.getAttributeValue("hidden")));
		
		//get areas
		List<Element> elementChildren = element.getChildren();
		
		for(Element areasElement:elementChildren){
			
			List<Element> areas = areasElement.getChildren();
			
			for(Element area: areas){
				
				areaObj = new Area();
				
				areaObj.setExternalId(Integer.parseInt(area.getAttributeValue("eid")));
				
				areaObj.setWhereCriteria(area.getAttributeValue("where")!=null?area.getAttributeValue("where"):null);
				
				areaObj.setShowTitle(area.getAttributeValue("showtitle")!=null?Boolean.parseBoolean(area.getAttributeValue("showtitle")):true);
				
				//areaObj.setWhereFilterCount(area.getAttributeValue("filtercount")!=null?Integer.parseInt(area.getAttributeValue("filtercount")):0);
				
				//check if there are filters configured
				List<Element> whereFilters = area.getChildren();
					
					for(Element filter : whereFilters){
						
						wFilterObj = new WhereFilter();
						wFilterObj.setName(filter.getAttributeValue("name"));
						wFilterObj.setType(filter.getAttributeValue("type"));
						
						areaObj.addWhereFilterType(wFilterObj);
					}
				
				areaObj.setOrderByCriteria(area.getAttributeValue("orderby")!=null?area.getAttributeValue("orderBy"):null);
				
				sheet.addArea(areaObj);
			}
			
		}
		
		return sheet;
	}
	
	public void setLoadInfoRef(boolean set){
		this.loadinforef = set;
	}
	
	public boolean isLoadInfoRef(){
		return this.loadinforef;
	}
	
	public List<Sheet> getSheets(){
		return this.sheetList;
	}
	
	public Sheet getSheetById(int id){
		
		Sheet result = null;
		
		for(Sheet sheet:this.sheetList){
			if(sheet.getId()==id){
				result = sheet;
				break;
			}
		}
		
		return result;
		
	}
	

}
