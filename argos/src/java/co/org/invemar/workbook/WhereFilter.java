package co.org.invemar.workbook;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class WhereFilter {

	private String name = null;
	private String type = null;
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public String getType(){
		return this.type;
	}

}
