package co.org.invemar.workbook;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class SheetContent {
	
	private int id;
	private String name = null;
	private List<Column> columns = new ArrayList<Column>();

	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void addColumn(Column column){
		this.columns.add(column);
	}
	
	public Column getColumnById(int id){
		
		Column result = null;
		
		for(Column column:this.columns){
			if(column.getId()==id){
				result = column;
				break;
			}
		}
		
		return result;
	}
	
	public List<Column> getColumns(){
		return this.columns;
	}
	
}

