package co.org.invemar.workbook;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;

import java.util.ArrayList;
import java.util.List;

/**
 * A base class that creates sheet's content structure.
 * It makes posible to hold view an table structure and create 
 * objects from them (NOT used for inforef)
 * 
 * @author Victor Adri�n Santaf� Torres
 * 
 * */

public class SheetContentManager {
	
	private Document contentXml = null;
	private List<SheetContent> sheetContentList = new ArrayList<SheetContent>();

	public SheetContentManager(Document contentXml){
		this.contentXml = contentXml; 
		createObjects();
	}
	
	private void createObjects(){
		
		//get root
		Element root = this.contentXml.getRootElement();
		
		List<Element> rootChildren = root.getChildren();
		
		//create sheet contents
		for(Element element : rootChildren){

			sheetContentList.add(createSheetContent(element));
		}
		
	}
	
	private SheetContent createSheetContent(Element element){
		
		SheetContent sheetContent = new SheetContent();
		Column columnObj = null;
		Link linkObj = null;
		List<Element> elementChildren = null;
		List<Element> columns = null;
		List<Element> links = null;
		
		//get id
		sheetContent.setId(Integer.parseInt(element.getAttributeValue("id")));
		
		//get name
		sheetContent.setName(element.getAttributeValue("name"));
		
		//preparing getting columns process
		elementChildren = element.getChildren();
			
		//create columns
		for(Element colsElement: elementChildren){
			
			columns = colsElement.getChildren();
			
			//process columns
			for(Element column: columns){
				
				columnObj = new Column();
				//get id
				columnObj.setId(Integer.parseInt(column.getAttributeValue("id")!=null?column.getAttributeValue("id"):"0"));
				//get name
				columnObj.setName(column.getAttributeValue("name"));
				//get hidden value
				columnObj.setHidden(Boolean.parseBoolean(column.getAttributeValue("hidden")!=null?column.getAttributeValue("hidden"):"false"));
				//get repeat value
				columnObj.setRepeat(Integer.parseInt(column.getAttributeValue("repeat")!=null?column.getAttributeValue("repeat"):"0"));
				//get addcolstor value
				columnObj.setAddColsToR(Integer.parseInt(column.getAttributeValue("addcolstor")!=null?column.getAttributeValue("addcolstor"):"0"));
			
				//try to get links from column
				
				links = column.getChildren();
				
					for(Element link: links){
						
						linkObj = new Link();
						//get sheet id
						linkObj.setSheetId(Integer.parseInt(link.getAttributeValue("sheet")));
						//get external id
						linkObj.setExternalId(Integer.parseInt(link.getAttributeValue("eid")));
						//get column id
						linkObj.setColumnId(Integer.parseInt(link.getAttributeValue("col")!=null?link.getAttributeValue("col"):"0"));
						
						//add link to column
						
						columnObj.addLink(linkObj);
						
					}
				
				//add column to sheet content
				sheetContent.addColumn(columnObj);
				
			}
			
		}
		
	 return sheetContent;	
	}
	
	public SheetContent getSheetContentById(int id){
		
		SheetContent result = null;
		
		for(SheetContent content: this.sheetContentList){
			
			if(content.getId()==id){
				result = content;
				break;
			}
			
		}
		
		return result;
		
	}
	
}
