package co.org.invemar.workbook;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class Column {
	
	private int id;
	private String name = null;
	private boolean hidden = false;
	private int repeat = 0;
	private int addcolstor = 0;
	private List<Link> links = new ArrayList<Link>();
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setHidden(boolean set){
		this.hidden = set;
	}
	
	public boolean isHidden(){
		return this.hidden;
	}
	
	public void setRepeat(int repeat){
		this.repeat = repeat;
	}
	
	public int getRepeat(){
		return this.repeat;
	}
	
	public void setAddColsToR(int addcolstor){
		this.addcolstor = addcolstor;
	}
	
	public int getAddColsToR(){
		return this.addcolstor;
	}
	
	public void addLink(Link link){
		this.links.add(link);
	}
	
	public List<Link> getLinks(){
		return this.links;
	}
	
}
