package co.org.invemar.workbook;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class Area {

	private int externalId = 0;
	private String whereCriteria = null;
	private List<WhereFilter> whereFilterList = new ArrayList<WhereFilter>();
	private String orderByCriteria = null;
	private boolean showTitle = false;
	
	public void setExternalId(int eid){
		this.externalId = eid;
	}
	
	public int getExternalId(){
		return this.externalId;
	}
	
	public void setWhereCriteria(String whereCriteria){
		this.whereCriteria = whereCriteria;
	}
	
	public String getWhereCriteria(){
		return this.whereCriteria;
	}
	
	public void addWhereFilterType(WhereFilter wFilter){
		this.whereFilterList.add(wFilter);
	}
	
	public List<WhereFilter> getWhereFilterTypeList(){
		return this.whereFilterList;
	}
	
	public WhereFilter getWhereFilterByName(String filterName){
		
		WhereFilter result = null;
		
		for(WhereFilter wFilter : this.whereFilterList){
			
			if(wFilter.getName().equals(filterName)){
				result = wFilter;
				break;
			}
		}
		
		return result;
		
	}
	
	public void setOrderByCriteria(String orderByCriteria){
		this.orderByCriteria = orderByCriteria;
	}
	
	public String getOrderByCriteria(){
		return this.orderByCriteria;
	}
	
	public void setShowTitle(boolean showTitle){
		this.showTitle = showTitle;		
	}
	
	public boolean isShowTitle(){
		return this.showTitle;
	}
}
