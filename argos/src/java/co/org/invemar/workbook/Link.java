package co.org.invemar.workbook;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class Link {
	private int sheetId;
	private int eid;
	private int columnId;
	
	public void setSheetId(int sheetId){
		this.sheetId = sheetId;
	}
	
	public int getSheetId(){
		return this.sheetId;
	}
	
	public void setExternalId(int eid){
		this.eid = eid;
	}
	
	public int getExternalId(){
		return this.eid;
	}
	
	public void setColumnId(int columnId){
		this.columnId = columnId;
	}
	
	public int getColumnId(){
		return this.columnId;
	}
	
}
