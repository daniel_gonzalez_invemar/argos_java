package co.org.invemar.workbook;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class Sheet {

	 private int id;
	 private String name = null;
	 private String type = null;
	 private boolean hidden = false;
	 private List<Area> areas = new ArrayList<Area>();
	 
	 public void setId(int id){
		 this.id = id;
	 }
	 
	 public int getId(){
		 return this.id;
	 }
	 
	 public void setName(String name){
		 this.name = name;
	 }
	 
	 public String getName(){
		 return this.name;
	 }
	 
	 public void setType(String type){
		 this.type = type;
	 }
	 
	 public String getType(){
		 return this.type;
	 }
	 
	 public void setHidden(boolean set){
		 this.hidden = set;
	 }
	 
	 public boolean isHidden(){
		 return this.hidden;
	 }
	 
	 public void addArea(Area area){
		 this.areas.add(area);
	 }
	 
	 public List<Area> getAreasList(){
		return this.areas; 
	 }
}
