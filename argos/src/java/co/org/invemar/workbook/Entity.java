package co.org.invemar.workbook;

import java.util.List;

/**
 * @author Julian Jose Pizarro Pertuz
 *
 */
public class Entity {
    
	private int id;
    private String name;
    private String description;
    private String schema;
    private List<Property> attributes;
    private String sql;
    private String where = "";
    private String orderby = "";
    
    //variables for downloading
    private String projectIdColumn = null;
    private String thematicIdColumn = null;
    
    /**
     * @return the attributes
     */
    public List<Property> getAttributes(){
        return attributes;
    }
    /**
     * @param attributes the attributes to set
     */
    public void setAttributes(List<Property> attributes) {
        this.attributes = attributes;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getSchema() {
		return schema;
	}
    
	public void setSchema(String schema) {
		this.schema = schema;
	}
	
	public void setId(int id){
    	this.id = id;
    }
    
    public int getId(){
    	return this.id;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the sql
     */
    public String getSql() {
        return sql;
    }
    /**
     * @param sql the sql to set
     */
    public void setSql(String sql) {
        this.sql = sql;
    }
    
    public void setWhereCriteria(String where){
    	this.where = where;
    }
    
    public String getWhereCriteria(){
    	return this.where;
    }
    
    public void setOrderByCriteria(String orderBy){
    	this.orderby = orderBy;
    }
    
    public String getOrderByCriteria(){
    	return this.orderby;
    }
    
    public String getCompoundSql(){
    	return this.sql+this.where+this.orderby;
    }
	public String getProjectIdColumn() {
		return projectIdColumn;
	}
	public void setProjectIdColumn(String projectIdColumn) {
		this.projectIdColumn = projectIdColumn;
	}
	public String getThematicIdColumn() {
		return thematicIdColumn;
	}
	public void setThematicIdColumn(String thematicIdColumn) {
		this.thematicIdColumn = thematicIdColumn;
	}
}
