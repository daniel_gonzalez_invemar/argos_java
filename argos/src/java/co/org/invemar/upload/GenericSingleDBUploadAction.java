package co.org.invemar.upload;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jdom.Document;
import org.jdom.Element;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.org.invemar.log.LogCreator;
import co.org.invemar.upload.table.UploadTable;
import co.org.invemar.upload.table.UploadTableManager;
import co.org.invemar.user.UserPermission;
import co.org.invemar.util.Cmail;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.RequestParameterManager;
import co.org.invemar.util.UtilFunctions;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Victor Adri�n Santaf� Torres.
 *
 *
 */
public class GenericSingleDBUploadAction {

    private ServletContext servletContext;

    private String username = null;
    private String userId = null;
    private String schema = null;
    private String table = null;
    private LogCreator logCreator = null;
    private String userPath = null;

    private Document contentXml = null;
    private UploadTableManager uTableManager;

    private UtilFunctions util = new UtilFunctions();

    //DB connection variables
    private ConnectionFactory cFactory;
    private Connection conn = null;

    public GenericSingleDBUploadAction(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
            throws ServletException, IOException {

        this.servletContext = servletContext;

        response.setContentType("text/text; charset=ISO-8859-1");

        HttpSession session = request.getSession();
        String action = null;
        JSONObject objResult = null;
        JSONArray arrayResult = null;
        boolean result = true;

        username = null;

        try {
            username = session.getAttribute("username").toString();
            userId = session.getAttribute("userid").toString();
        } catch (Exception e) {
            username = null;
        }

        if (username == null) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            return;
        } else {
            //check user permission
            UserPermission userp = (UserPermission) session.getAttribute("userpermission");

            if (!userp.couldUser(UserPermission.INGRESAR_DATOS_EN_BRUTO)) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                return;
            }
        }

        //get user file path
        userPath = servletContext.getRealPath("useruploads") + File.separator + userId;

        action = request.getParameter("action");

        response.addHeader("Expires", "0");
        response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.addHeader("Pragma", "no-cache");
        response.setContentType("application/json");

        if (action.equals("getSchemas")) {

            try {

                JSONArray jResult = getSchemas();

                if (jResult != null) {
                    response.setContentType("application/json");
                    response.getWriter().write(jResult.toString());
                } else {
                    response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
            } catch (Exception e) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }

        } else if (action.equals("getUploadTables")) {

            schema = request.getParameter("schema");
            arrayResult = this.getUploadTables();
            if (arrayResult == null) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            } else {
                response.getWriter().write(arrayResult.toString());
            }

        } else if (action.equals("getTableStructure")) {

            schema = request.getParameter("schema");
            table = request.getParameter("table");

            try {
                arrayResult = getTableStructure();
            } catch (Exception e) {
                e.printStackTrace();
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }

            if (arrayResult == null) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            } else {
                response.getWriter().write(arrayResult.toString());
            }

        } else if (action.equals("getUploadedFileColumnNames")) {

            schema = request.getParameter("schema");
            table = request.getParameter("table");

            try {
                arrayResult = getUploadedFileColumnNames();
            } catch (Exception e) {
                e.printStackTrace();
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }

            if (arrayResult == null) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            } else {
                response.getWriter().write(arrayResult.toString());
            }

        } else if (action.equals("configureFileToUpload")) {

            schema = request.getParameter("schema");
            RequestParameterManager rManager = new RequestParameterManager(request);
            int rows = 0;

            try {
                rows = configureFileToUpload(rManager);
            } catch (Exception e) {
                e.printStackTrace();
                result = false;
            }

            if (rows > 0) {
                response.getWriter().write(String.valueOf(rows));
            } else {
                response.getWriter().write("0");
            }

        } else if (action.equals("createCustomControlFile")) {

            schema = request.getParameter("schema");
            table = request.getParameter("table");

            result = createCustomControlFile();

            if (result) {
                response.getWriter().write("OK");
            } else {
                response.getWriter().write("NO");
            }

        } else if (action.equals("processFile")) {

            schema = request.getParameter("schema");
            table = request.getParameter("table");
            int uploadedRows = Integer.parseInt(request.getParameter("urows"));

            objResult = processFile(uploadedRows);

            if (objResult == null) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            } else {
                response.getWriter().write(objResult.toString());
            }

        }

    }

    /*get thematics related with a specific project*/
    public JSONArray getSchemas() throws SQLException {

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = null;

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection();
            pstmt = conn.prepareStatement("SELECT LOWER(ESQUEMA) FROM BUBICACION_TEMATICA GROUP BY ESQUEMA");

            rs = pstmt.executeQuery();

            while (rs.next()) {

                if (rs.getString(1) != null && !rs.getString(1).equals("") && !rs.getString(1).equals(" ")) {
                    jsonObject = new JSONObject();
                    jsonObject.put("name", rs.getString(1));

                    jsonArray.put(jsonObject);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        return jsonArray;
    }

    /*gets upload tables from xml*/
    private JSONArray getUploadTables() {

        JSONArray result = new JSONArray();
        JSONObject jsonObject = null;

        //read uplad table xml and start processing
        try {
            contentXml = util.readXML(this.servletContext.getRealPath("WEB-INF") + File.separator + "xmlconfig" + File.separator + schema + File.separator + "admindbuploadtables.xml");
            this.uTableManager = new UploadTableManager(contentXml);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        for (UploadTable uTable : uTableManager.getUploadTableList()) {
            try {
                jsonObject = new JSONObject();
                jsonObject.put("name", uTable.getName());
                jsonObject.put("id", String.valueOf(uTable.getId()));
        		//jsonObject.put("description", uTable.getDescription());

                result.put(jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return result;
    }

    /*reads Oracle data dictonary and retrieves table structure*/
    private JSONArray getTableStructure() throws SQLException {

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = null;
        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection(schema);
            pstmt = conn.prepareStatement("select COLUMN_NAME, DATA_TYPE, DATA_LENGTH, NULLABLE from USER_TAB_COLUMNS where TABLE_NAME = ? AND COLUMN_NAME!='UPLOADFLAG'");
            pstmt.setString(1, this.table.toUpperCase());
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {

                jsonObject = new JSONObject();
                jsonObject.put("colname", rs.getString("COLUMN_NAME"));
                jsonObject.put("datatype", rs.getString("DATA_TYPE"));
                jsonObject.put("datalenght", rs.getString("DATA_LENGTH"));
                jsonObject.put("nullable", rs.getString("NULLABLE"));

                jsonArray.put(jsonObject);
            } else {
                jsonArray = null;
            }

            while (rs.next()) {
                jsonObject = new JSONObject();
                jsonObject.put("colname", rs.getString("COLUMN_NAME"));
                jsonObject.put("datatype", rs.getString("DATA_TYPE"));
                jsonObject.put("datalenght", rs.getString("DATA_LENGTH"));
                jsonObject.put("nullable", rs.getString("NULLABLE"));

                jsonArray.put(jsonObject);
            }

        } catch (Exception e) {

            System.out.println(e);
            e.printStackTrace();
            jsonArray = null;
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        return jsonArray;
    }

    /*reads the first line of uploaded file and returns columns names*/
    private JSONArray getUploadedFileColumnNames() {

        String newLine = "";
        String fileName = null;
        BufferedReader buftempfile = null;
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = null;
        String colNames[] = null;

        //prepare master file path
        fileName = this.userPath + File.separator + "dbupload.csv";

        try {

            //read master file
            buftempfile = new BufferedReader(new FileReader(fileName));

            while ((newLine = buftempfile.readLine()) != null) {
                if (newLine != null && !newLine.equals("") && !newLine.equals(" ")) {

                    //take ids from master file
                    colNames = newLine.split(";");

                    for (int i = 0; i < colNames.length; i++) {

                        jsonObject = new JSONObject();
                        jsonObject.put("name", colNames[i]);
                        jsonObject.put("value", String.valueOf(i));
                        jsonArray.put(jsonObject);
                    }

                    break;

                }

            }

            buftempfile.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonArray;
    }


    /*creates a custom control file*/
    private boolean createCustomControlFile() {

        boolean result = true;
        File newFile = null;
        BufferedWriter out = null;
        String fileNamePath = null;
        StringBuffer controlContent = new StringBuffer();
        JSONArray tableStructure = null;
        JSONObject column = null;
        String dataType = null;
        String colDataType = null;

        int i = 0;

        //prepare control file content
        controlContent.append("LOAD DATA\n");
        controlContent.append("APPEND INTO TABLE " + this.schema + "." + this.table + "\n");
        controlContent.append("FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '" + '"' + "'\n");
        controlContent.append("TRAILING NULLCOLS\n");
        controlContent.append("(\n");

		//create table structure
        try {

            tableStructure = getTableStructure();

            for (i = 0; i < tableStructure.length(); i++) {

                //get column
                column = tableStructure.getJSONObject(i);
                controlContent.append(column.getString("colname") + "       ");

                //define data type
                dataType = column.getString("datatype");
                System.out.println(column + " " + dataType);

                if (dataType.equals("CHAR") || dataType.equals("VARCHAR") || dataType.equals("VARCHAR2")) {
                    colDataType = "CHAR";
                } else if (dataType.equals("NUMERIC") || dataType.equals("NUMBER") || dataType.equals("INT") || dataType.equals("INTEGER") || dataType.equals("SMALLINT")) {
                    colDataType = "INTEGER EXTERNAL";
                } else if (dataType.equals("FLOAT")) {
                    colDataType = "FLOAT EXTERNAL";
                } else if (dataType.equals("DECIMAL")) {
                    colDataType = "DECIMAL EXTERNAL";
                } else if (dataType.equals("DATE")) {
                    colDataType = "DATE";
                } else {
                    //mmm, data type still not supported, so return false
                    return false;
                }

                //append data type
                controlContent.append(colDataType);

                System.out.println(controlContent.toString());

                //determines the end of line
                if (i < tableStructure.length() - 1) {
                    controlContent.append(",\n");
                } else {
                    controlContent.append(",\n");//the same because next we are appending other line, however I hold this for future
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }

        controlContent.append("uploadflag CHAR\n");
        controlContent.append(")");

        if (!result) {
            return result;
        }

        try {

            fileNamePath = this.userPath + File.separator + this.table + ".ctl";

            newFile = new File(fileNamePath);

            //create new file
            newFile.createNewFile();

            //put bad ids on file
            out = new BufferedWriter(new FileWriter(newFile));
            out.write(controlContent.toString());
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }

        return result;
    }

    /*create a new file to upload using users options*/
    private int configureFileToUpload(RequestParameterManager rManager) {

        StringBuffer newFileString = new StringBuffer();
        StringBuffer newFileToUploadString = new StringBuffer();
        int colId = 0;
        String newLine = "";
        String newLineReader[] = null;

        File newFile = null;
        String fileName = null;
        BufferedReader buftempfile = null;
        BufferedWriter out = null;
        int i = 0;
        int j = 0;

        fileName = this.userPath + File.separator + "dbupload.csv";

        try {

            buftempfile = new BufferedReader(new FileReader(fileName));

            //start file reading
            while ((newLine = buftempfile.readLine()) != null) {
                //verify it is not the header
                if (i > 0) {

                    if (newLine != null && !newLine.equals("") && !newLine.equals(" ")) {

                        //use parameters sent by user to define what line content should be appended
                        for (j = 0; j < rManager.getRequestParameterList().size() - 3; j++) {

                            //get column user's parameter 
                            colId = Integer.parseInt(rManager.getRequestParameterValueByName("col" + j));
                            System.out.println(colId);

                            //if value is -1 it means that user did not select any column 
                            if (colId == -1) {

                                //if it is not the last column, then append a semicolon
                                if (j < rManager.getRequestParameterList().size() - 4) {
                                    newFileToUploadString.append(";");
                                }

                            } else {

                                newLineReader = newLine.split(";");

                                //if it is not the last column, then append text plus a semicolon
                                if (j < rManager.getRequestParameterList().size() - 4) {
                                    newFileToUploadString.append(newLineReader[colId] + ";");
                                } else {//if it is the last column, just append text
                                    newFileToUploadString.append(newLineReader[colId]);
                                }

                            }

                        }
                        //append UPLOAD FLAG at the end of line 
                        newFileToUploadString.append(";" + this.userId + "_0\n");
                    }
                }
                i++;
            }

            buftempfile.close();

		 	//finally create new file
            fileName = this.userPath + File.separator + "ndbupload.csv";

            newFile = new File(fileName);

            //create new file
            newFile.createNewFile();

            //take actual content of newFileString and write it down the created file
            out = new BufferedWriter(new FileWriter(newFile));
            out.write(newFileToUploadString.toString());
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
            i = 0;
        }

        return i - 1;
    }

    private JSONObject processFile(int uploadedRows) {

        DataBaseUploader dbUploader = null;
        JSONObject result = null;
        boolean success = true;

        //prepare uploader
        dbUploader = new DataBaseUploader(this.userId, this.schema, this.table, "ndbupload.csv", this.servletContext);
        dbUploader.setControlFilePath(servletContext.getRealPath("useruploads") + File.separator + this.userId + File.separator + this.table + ".ctl");

        //process file
        result = dbUploader.processFile();

        //verify whether everything was ok or not
        try {
            if (result == null || !result.getString("filestatus").equals("1")) {
                //mmm, something was wrong
                success = false;
            } else {
    			//now log this insert action

                try {
                    result.put("totalrows", String.valueOf(uploadedRows));
                    logCreator = new LogCreator();
                    logCreator.createLog(logCreator.INGRESO_DE_DATOS_MASIVAMENTE, Integer.parseInt(this.userId), 0, 0, uploadedRows);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //verify process
        if (!success) {

            //start rollback
            try {
                this.rollBack(this.table);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } else {
            //commit
            try {
                this.commit(this.table);
                
                StringBuffer message = new StringBuffer();
                Cmail mailSender = new Cmail();

                //set basic message data
                mailSender.setafrom("administrador_argos@invemar.org.co");
                mailSender.setato("");
                mailSender.setasubject("INVEMAR/ARGOS - Notificacion de carga de datos");

                //set message content
                message.append("<HTML>");
                message.append("<HEAD>");
                message.append("</HEAD>");
                message.append("<BODY>");
                message.append("<p>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</p>");

                message.append("<p>El presente correo es para notificarle que se han cargado nuevos datos del proyecto.</p>");               
                message.append("</BODY>");
                message.append("</HTML>");

                mailSender.setamensaje(message.toString());

                boolean r = mailSender.manda();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return result;

    }

    /*delete rows from table when upload error occurs*/
    private void rollBack(String table) throws SQLException {

        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection(schema);
            pstmt = conn.prepareStatement("DELETE FROM " + table + " WHERE uploadflag='" + this.userId + "_0'");
            pstmt.executeUpdate();
            conn.commit();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

    }

    /*update upload flag from tables when upload process was success*/
    private void commit(String table) throws SQLException {

        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection(schema);
            pstmt = conn.prepareStatement("UPDATE " + table + " SET uploadflag='" + this.userId + "' WHERE uploadflag='" + this.userId + "_0'");
            pstmt.executeUpdate();
            conn.commit();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

    }

}
