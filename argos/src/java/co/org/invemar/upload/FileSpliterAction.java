package co.org.invemar.upload;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletContext;

import org.jdom.Document;

import co.org.invemar.upload.table.UploadTableManager;
import co.org.invemar.user.UserPermission;
import co.org.invemar.util.UtilFunctions;

/**
 *
 * @author Victor Adri�n Santaf� Torres.
 *
 *
 */
public class FileSpliterAction {

    private ServletContext servletContext;
    private String username = null;
    private String userId = null;
    private String ownerId = null;
    private String schema = null;
    private String thematicId = null;
    private String filename = null;
    private String userPath = null;
    private Document contentXml = null;
    private boolean createNegativeFile = false;
    private UtilFunctions util = new UtilFunctions();

    public FileSpliterAction(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
            throws ServletException, IOException {

        this.servletContext = servletContext;
        response.setContentType("text/text; charset=ISO-8859-1");
        response.addHeader("Expires", "0");
        response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.addHeader("Pragma", "no-cache");
        response.setContentType("text/html");

        HttpSession session = request.getSession();

        username = null;

        try {
            username = session.getAttribute("username").toString();
            userId = session.getAttribute("userid").toString();
        } catch (Exception e) {
            username = null;
        }

        if (username == null) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            return;
        } else {
            //check user permission
            UserPermission userp = (UserPermission) session.getAttribute("userpermission");

            if (userp.couldUser(UserPermission.ACTUALIZAR_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {
                ownerId = request.getParameter("ownerid");
                if (ownerId.equals("no")) {
                    ownerId = userId;
                }
            } else if (userp.couldUser(UserPermission.ACTUALIZAR_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL)) {
                ownerId = userId;
            } else if (!userp.couldUser(UserPermission.INGRESAR_DATOS) && !userp.couldUser(UserPermission.INGRESAR_DATOS_EN_BRUTO) && !userp.couldUser(UserPermission.ACTUALIZAR_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) && !userp.couldUser(UserPermission.ACTUALIZAR_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                return;
            } else {
                ownerId = userId;
            }

        }

        schema = request.getParameter("schema");
        thematicId = request.getParameter("thematicid");
        filename = request.getParameter("filename");

        //get user file path
        userPath = servletContext.getRealPath("useruploads") + File.separator + userId;
        System.out.println("user path:"+userId); 
        //create a negative file?
        System.out.println("negative file va:"+request.getParameter("negativefile"));
        if (request.getParameter("negativefile") != null) {
            try {
                createNegativeFile = Boolean.parseBoolean(request.getParameter("negativefile"));
                System.out.println("Creative nevative file:"+createNegativeFile);
            } catch (Exception e) {
                e.printStackTrace();
                createNegativeFile = false;
            }

        }

        int result = processFile();

        if (result > 0) {
            response.getWriter().write(String.valueOf(result));
        } else {
            response.getWriter().write("0");
        }

    }

    private int processFile() {

        StringBuffer newFileString = new StringBuffer();
        StringBuffer newNegativeFileString = new StringBuffer();
        String newLine = "";
        File newFile = null;
        File newNegativeFile = null;
        String fileName = null;
        UploadTableManager uTableManager;
        BufferedReader buftempfile = null;
        BufferedWriter out = null;
        int i = 1;
        int rows = 0;

        //read upload table xml
        contentXml = util.readXML(this.servletContext.getRealPath("WEB-INF") + File.separator + "xmlconfig" + File.separator + schema + File.separator + this.thematicId + File.separator + "userdbuploadtables.xml");

        uTableManager = new UploadTableManager(contentXml);

        try {
            System.out.println("ORI::userPath + File.separator + this.filename::: "+userPath + File.separator + this.filename);
            buftempfile = new BufferedReader(new FileReader(userPath + File.separator + this.filename));

            while ((newLine = buftempfile.readLine()) != null) {

                /*
                 * if it is find an end of file line
                 */
                if (newLine.equals("*******end of file*******")) {

                    //get tables names and complete it
                    fileName = uTableManager.getUploadTableById(i).getName() + ".csv";
                    //System.out.println("this.userPath+File.separator+fileName: "+this.userPath+File.separator+fileName);
                    //try{
                     System.out.println("NEW this.userPath + File.separator + fileName: "+this.userPath + File.separator + fileName);
                    newFile = new File(this.userPath + File.separator + fileName);

                    //create new file
                    boolean r = newFile.createNewFile();
                    System.out.println(" Resultado de creacion de archivo:::::"+r);
                    //}catch(IOException e){System.out.println("errrorrororor: "+e.toString());}

                    //take actual content of newFileString and write it down the created file
                    out = new BufferedWriter(new FileWriter(newFile));
                    //System.out.println("newFileString.toString(): " + newFileString.toString());
                    out.write(newFileString.toString());
                    out.close();

                    //if a negative file should be created
                    if (createNegativeFile) {

                        fileName = "n" + uTableManager.getUploadTableById(i).getName() + ".csv";
                        System.out.println("Path ::::::::::::::::"+this.userPath + File.separator + fileName);
                        newNegativeFile = new File(this.userPath + File.separator + fileName);

                        newNegativeFile.createNewFile();

                        out = new BufferedWriter(new FileWriter(newNegativeFile));
                        out.write(newNegativeFileString.toString());
                        out.close();

                        newNegativeFileString = new StringBuffer();

                    }


                    //reset newFileString for next file content
                    newFileString = new StringBuffer();

                    i++;

                } else {

                    //hold line		 			
                    newFileString.append(newLine + this.ownerId + "_0;\n");

                    //if master file then count rows
                    if (i == 1) {
                        rows = rows + 1;
                    }

                    if (createNegativeFile) {
                        newNegativeFileString.append("-" + newLine + this.ownerId + "_0;\n");
                    }

                }

            }
            buftempfile.close();

            //get tables names and complete it
            fileName = uTableManager.getUploadTableById(i).getName() + ".csv";
            //System.out.println("fileName: "+fileName);
            //try{
            newFile = new File(this.userPath + File.separator + fileName);

            //create new file
            newFile.createNewFile();
            //}catch(IOException e){System.out.println("errrorrororor: "+e.toString());}
            //take actual content of newFileString and write it down the created file
            out = new BufferedWriter(new FileWriter(newFile));
            out.write(newFileString.toString());
            out.close();

            //if a negative file should be created
            if (createNegativeFile) {

                fileName = "n" + uTableManager.getUploadTableById(i).getName() + ".csv";
                newNegativeFile = new File(this.userPath + File.separator + fileName);

                newNegativeFile.createNewFile();

                out = new BufferedWriter(new FileWriter(newNegativeFile));
                out.write(newNegativeFileString.toString());
                out.close();

            }

        }catch (SecurityException e) {           
            rows = 0;
            System.out.println("Security Error :" + e.toString());
            
        } catch (IOException e) {
            
            rows = 0;
            System.out.println("Error IO:" + e.toString());
        } catch (Exception e) {           
            rows = 0;
            System.out.println("Error IO:" + e.toString());
        }
        return rows;
    }
}
