package co.org.invemar.upload;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import co.org.invemar.log.LogCreator;
import co.org.invemar.upload.table.UploadTable;
import co.org.invemar.upload.table.UploadTableManager;
import co.org.invemar.user.UserPermission;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.UtilFunctions;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jdom.Document;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Victor Adri�n Santaf� Torres.
 *
 *
 */
public class MultipleDBUploadAction {

    private ServletContext servletContext;
    private UploadTableManager uTableManager;
    private String username = null;
    private String userId = null;
    private String schema = null;
    private String thematicId = null;
    private String projectId = null;
    private String userPath = null;
    private LogCreator logCreator = null;
    private int uploadedRows = 0;
    private UtilFunctions util = new UtilFunctions();
    //DB connection variables
    private ConnectionFactory cFactory;
    private Connection conn = null;

    public MultipleDBUploadAction(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
            throws ServletException, IOException {

        this.servletContext = servletContext;

        response.setContentType("text/text; charset=ISO-8859-1");

        HttpSession session = request.getSession();
        JSONObject result = null;

        username = null;

        try {
            username = session.getAttribute("username").toString();
            userId = session.getAttribute("userid").toString();
        } catch (Exception e) {
            username = null;
        }

        if (username == null) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            return;
        } else {
            //check user permission
            UserPermission userp = (UserPermission) session.getAttribute("userpermission");

            if (!userp.couldUser(UserPermission.INGRESAR_DATOS) && !userp.couldUser(UserPermission.ACTUALIZAR_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) && !userp.couldUser(UserPermission.ACTUALIZAR_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                return;
            }
        }

        schema = request.getParameter("schema");
        thematicId = request.getParameter("thematicid");
        projectId = session.getAttribute("projectid").toString();
        uploadedRows = Integer.parseInt(request.getParameter("urows"));

        userPath = servletContext.getRealPath("useruploads") + File.separator + userId;

        result = processFile();
        //System.out.println("result:----------------- "+result);
        //erase user upload path files
        //try{
        deleteDirectoryFiles(new File(userPath));
        //}catch(Exception e){

        //}

        if (result == null) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } else {
            response.addHeader("Expires", "0");
            response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.addHeader("Pragma", "no-cache");
            response.setContentType("application/json");
            response.getWriter().write(result.toString());
        }

    }

    private JSONObject processFile() {

        Document contentXml = null;
        DataBaseUploader dbUploader = null;
        UploadTable uTable = null;
        JSONObject result = null;
        boolean success = true;
        File pfile;
        int i = 1;

        //read uplad table xml and start processing
        contentXml = util.readXML(this.servletContext.getRealPath("WEB-INF") + File.separator + "xmlconfig" + File.separator + schema + File.separator + thematicId + File.separator + "userdbuploadtables.xml");

        this.uTableManager = new UploadTableManager(contentXml);

        System.out.println("uTableManager.getUploadTableListSize():--------- "+uTableManager.getUploadTableListSize());
        //process each file
        for (i = 1; i <= uTableManager.getUploadTableListSize(); i++) {

            //get upload table settings
            uTable = uTableManager.getUploadTableById(i);

            System.out.println("uTable.getName():------------------------------------- "+uTable.getName());
            pfile = new File(this.userPath + File.separator + uTable.getName() + ".csv");

            if (pfile.length() != 0) {
                //prepare uploader
                dbUploader = new DataBaseUploader(this.userId, this.schema, uTable.getName(), uTable.getName() + ".csv", this.servletContext);


                //process file
                result = dbUploader.processFile();
                //System.out.println("result dbUploader.processFile():---------------- "+result);
                //verify whether everything was ok or not
                try {
                    if (result == null || !result.getString("filestatus").equals("1")) {
                        //mmm, something was wrong, so put description in result object to give user an idea of what he did
                        //System.out.println("AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIII");
                        result.put("description", uTable.getDescription());
                        success = false;
                        break; //pendiente comentariado por considerar que no permite hacer el rollback
                    } else {
                        //now log this insert action
                        try {
                            logCreator = new LogCreator();
                            //logManager.newLog(logManager.INSERT_DATA_LOG, this.username, this.schema, result.getString("logobservation"));    			
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        //System.out.println("----------------------------------------->>>> "+success);
        //verify that all files where processed
        if (!success) {
            //System.out.println("======================== INICIANDO..... rollback =========================> i: "+i);
            //start rollback from last uTable processed :)

            for (int j = i; j > 0; j--) {
                try {
                    uTable = uTableManager.getUploadTableById(j);
                    //System.out.println(uTable);
                    this.rollBack(uTable.getName());
                } catch (SQLException e) {
                    e.printStackTrace();

                }
            }

        } else {

            //start commit
            //System.out.println("======================== INICIANDO..... commit =========================> i: "+i);
            for (int j = 1; j < i; j++) {
                try {
                    uTable = uTableManager.getUploadTableById(j);

                    this.commit(uTable.getName());

                } catch (SQLException e) {
                    e.printStackTrace();

                }
            }

            //finally log this action
            try {
                logCreator = new LogCreator();
                logCreator.createLog(logCreator.INGRESO_DE_DATOS_POR_ASISTENTE, Integer.parseInt(this.userId), Integer.parseInt(this.projectId), Integer.parseInt(this.thematicId), this.uploadedRows);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return result;

    }

    /*
     * delete rows from table when upload error occurs
     */
    private void rollBack(String table) throws SQLException {

        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection(schema);
            pstmt = conn.prepareStatement("DELETE FROM " + table + " WHERE uploadflag='" + this.userId + "_0'");
            pstmt.executeUpdate();
            conn.commit();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

    }

    /*
     * update upload flag from tables when upload process was success
     */
    private void commit(String table) throws SQLException {

        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection(schema);
            pstmt = conn.prepareStatement("UPDATE " + table + " SET uploadflag='" + this.userId + "' WHERE uploadflag='" + this.userId + "_0'");
            pstmt.executeUpdate();
            conn.commit();
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

    }

    private void deleteDirectoryFiles(File path) {

        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    //deleteDirectoryFiles(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }

        if (!path.getAbsolutePath().equals(this.userPath)) {
            //path.delete();
        }

    }
}
