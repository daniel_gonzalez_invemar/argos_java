package co.org.invemar.upload.table;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UploadTable {
	
	private int id;
	private String name;
	private String description;
	private String idColumn = null;
	
	public String getIdColumn() {
		return idColumn;
	}
	public void setIdColumn(String idColumn) {
		this.idColumn = idColumn;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
