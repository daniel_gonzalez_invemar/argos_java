package co.org.invemar.upload.table;

import java.util.List;
import java.util.ArrayList;

import org.jdom.Document;
import org.jdom.Element;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UploadTableManager {
	
	List<UploadTable> uploadTableList = new ArrayList<UploadTable>();
	private Document contentXml = null;
	
	
	public UploadTableManager(Document contentXml){
		this.contentXml = contentXml;
                System.out.println("content xml:"+this.contentXml);
		this.createObjects();
	}
	
	private void createObjects(){
		
		Element root = this.contentXml.getRootElement();
		UploadTable uTable = null;
		
		List<Element> rootChildren = root.getChildren();
		
		for(Element table : rootChildren){
			
			uTable = new UploadTable();
			
			uTable.setId(Integer.parseInt(table.getAttributeValue("id")));
                        System.out.println("Utableeeeeeeee id:"+table.getAttributeValue("id"));
			uTable.setName(table.getAttributeValue("name"));
                         System.out.println("Utableeeeeeeee id:"+table.getAttributeValue("name"));
			uTable.setDescription(table.getAttributeValue("description"));
			uTable.setIdColumn(table.getAttributeValue("idcolumn"));
			
			this.uploadTableList.add(uTable);
		}
		
	}
	
	public List<UploadTable> getUploadTableList(){
		return this.uploadTableList;
	}
	
	public int getUploadTableListSize(){
		return this.uploadTableList.size();
	}
	
	public UploadTable getUploadTableById(int id){
		
		UploadTable result = null;
		
		for(UploadTable uTable : this.uploadTableList){
			
			if(uTable.getId()==id){
				result = uTable;
				break;
			}
			
		}
		
		return result;
		
	}
	
	
}
