package co.org.invemar.upload;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Iterator;

import com.missiondata.fileupload.MonitoredDiskFileItemFactory;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UploadServlet extends HttpServlet
{
	
	private String username = null;
 
	private javax.servlet.ServletContext servletContext; 
    public void init(ServletConfig config) throws ServletException {
        servletContext=config.getServletContext();
        super.init(config);
    } 
    
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request,response);
  }

  public synchronized void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
	  	synchronized(this){
	  		new UploadServletAction(request, response, servletContext);
	  	}
  }

}
