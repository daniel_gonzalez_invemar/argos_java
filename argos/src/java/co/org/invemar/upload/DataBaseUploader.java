package co.org.invemar.upload;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

import co.org.invemar.util.UtilFunctions;
import javax.servlet.ServletContext;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Attribute;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Map;

/**
 *
 * @author Victor Adri�n Santaf� Torres.
 *
 *
 */
public class DataBaseUploader {

    private String sqlLoaderPath;
    private String DBconnectionString;

    private String userId = null;
    private String schema = null;
    private String fileName = null;
    private String tableName = null;
    private String controlFileName = null;
    private String userUploadPath = null;
    private String controlFilePath = null;

    private String userLogFilePath = null;
    private String userDiscardFilePath = null;
    private String userBadFilePath = null;
    private String userRelativePath = null;

    private ServletContext servletContext;

    private UtilFunctions util = new UtilFunctions();

    public DataBaseUploader(String userId, String schema, String tableName, String fileName, ServletContext servletContext) {

        this.userId = userId;
        this.schema = schema;
        this.fileName = fileName;
        this.tableName = tableName;
        this.servletContext = servletContext;
        this.initializeConfiguration();
        this.controlFileName = controlFilePath + File.separator + this.tableName + ".ctl";
    }

    /*read initial configuration from databaseconfig.xml and configure pathes*/
    private void initializeConfiguration() {

        String dbconfigxml = this.servletContext.getRealPath("WEB-INF") + File.separator + "databaseconfig.xml";

        Document dbConfigDoc = util.readXML(dbconfigxml);

        Element root = dbConfigDoc.getRootElement();

        this.sqlLoaderPath = root.getAttributeValue("sqlloaderpath");
        this.DBconnectionString = root.getAttributeValue(this.schema + "_cs");

        //CONFIGURE ALL PATHES
        //user upload path
        userUploadPath = servletContext.getRealPath("useruploads") + File.separator + userId;

        //control file path
        controlFilePath = this.servletContext.getRealPath("WEB-INF") + File.separator + "xmlconfig" + File.separator + schema + File.separator + "controlfiles";

        //user log file path
        this.userLogFilePath = userUploadPath + File.separator + "logfile";
        //user discard file path
        this.userDiscardFilePath = userUploadPath + File.separator + "discardfile";
        //user bad file path
        this.userBadFilePath = userUploadPath + File.separator + "badfile";

        //check directory existence for each path
        if (!(new File(userLogFilePath)).exists()) {
            new File(userLogFilePath).mkdir();
        }
        if (!(new File(userDiscardFilePath)).exists()) {
            new File(userDiscardFilePath).mkdir();
        }
        if (!(new File(userBadFilePath)).exists()) {
            new File(userBadFilePath).mkdir();
        }

        //user relative path
        userRelativePath = "useruploads/" + userId;

    }

    public void setControlFilePath(String ctlFilePath) {

        this.controlFileName = ctlFilePath;
    }

    public JSONObject processFile() {

        JSONObject resp = new JSONObject();

        //CONFIGURE ARGS FOR SQLLOADER
        String[] args = new String[8];

        args[0] = sqlLoaderPath == null ? "" : sqlLoaderPath + "sqlldr".trim();
        System.out.println("DBconnectionString:------------------------ " + DBconnectionString);
        args[1] = "userid=" + DBconnectionString;
        args[2] = "control=" + this.controlFileName;
        args[3] = "log=" + userLogFilePath + File.separator + fileName + ".log";
        args[4] = "bad=" + userBadFilePath + File.separator + fileName;
        args[5] = "discard=" + userDiscardFilePath + File.separator + fileName;
        args[6] = "data=" + userUploadPath + File.separator + fileName;
        //args[7]="skip=1";
        args[7] = "ERRORS=555";

        try {

            //run SQLLoader
            //System.out.println("------"+args[0]+" "+args[1]+" "+args[2]+" "+args[3]+" "+args[4]+" "+args[5]+" "+args[6]+" "+args[7]);//+" "+args[8]);
            ProcessBuilder pb = null;
            pb = new ProcessBuilder(args);

            String oraclePath = "/usr/lib/oracle/12.2/client64";

            Map<String, String> evn = pb.environment();

            evn.put("ORACLE_HOME", oraclePath);

            String lib=":/usr/lib/oracle/12.2/client64/lib";
            System.out.println("**************************************");
            System.out.println(lib);
            System.out.println("*****************************************");
            evn.put("LD_LIBRARY_PATH",evn.get("LD_LIBRARY_PATH")+lib);
            evn.put("TNS_ADMIN", oraclePath);

           

            pb.redirectErrorStream(true);
            System.out.println("Start run process....");
            Process process = pb.start();

            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;

            System.out.printf("Output of running is %s :", Arrays.toString(args));

            while ((line = br.readLine()) != null) {
                System.out.println(line);

            }
            //take it out - it makes java to work slower

            StringLog[] logs = readValuesOfFile(userLogFilePath + File.separator + fileName + ".log", schema.toUpperCase(), tableName.toUpperCase());

            //filesName.put(fileLoaded);
            resp.put("filename", fileName);
            System.out.println("processFile............ LOGS " + logs);
            if (logs == null) {
                resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                resp.put("filestatus", "5");
                //System.out.println("FILESTATUS: ------------------------------ 5");
            } else {

                if (logs[0].value != 0 && logs[1].value == 0 && logs[2].value == 0 && logs[3].value == 0) {
                    resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                    resp.put("filestatus", "1");
                    resp.put("logobservation", "table: " + tableName + ", nrows: " + logs[0].getValue());
                    //System.out.println("FILESTATUS: ------------------------------ 1");
                } else if (logs[1].value != 0 && logs[2].value != 0) {
                    resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                    resp.put("badfile", userRelativePath + "/badfile/" + fileName);
                    resp.put("discardfile", userRelativePath + "/discardfile/" + fileName);
                    resp.put("filestatus", "2");
                    //System.out.println("FILESTATUS: ------------------------------ 2");
                } else if (logs[1].value != 0) {
                    resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                    resp.put("badfile", userRelativePath + "/badfile/" + fileName);
                    resp.put("filestatus", "3");
                    //System.out.println("FILESTATUS: ------------------------------ 3");
                } else if (logs[2].value != 0) {
                    resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                    resp.put("discardfile", userRelativePath + "/discardfile/" + fileName);
                    resp.put("filestatus", "4");
                    //System.out.println("FILESTATUS: ------------------------------ 4");
                } else if (logs[0].value == 0 && logs[1].value == 0 && logs[2].value == 0 && logs[3].value == 0) {
                    resp.put("logfile", userRelativePath + "/logfile/" + fileName + ".log");
                    resp.put("filestatus", "5");
                    System.out.println("FILESTATUS: ------------------------------ 5");
                }

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("processFile: ERROR " + e.toString());
        }
        System.out.println("processFile resp: " + resp);
        return resp;

    }

    /*process content of log file generated by SQLLoader*/
    public StringLog[] readValuesOfFile(String fileName, String schema, String table) {

        StringLog[] values;

        try {
            //System.out.println("fileName= "+fileName);
            String[] valuesOfFile = readLogFile(fileName, schema, table);

            values = readValuesOfLog(valuesOfFile);

            return values;

        } catch (IOException e) {
            System.out.println("readValuesOfFile: " + e);
            return null;
        }

    }

    /*read de log file generated by SQLLoader*/
    public String[] readLogFile(String fileName, String schema, String table) throws IOException {

        String[] logs = new String[0];
        String fileLine = null;
        String searchString = null;
        String optionalSearchString = null;

        try {
            //System.out.println("READLOGFILE......... ");
            // System.out.println("FIleNAme: "+fileName);
            // System.out.println("Schema: "+schema);
            // System.out.println("Table: "+table);

            BufferedReader entrada = new BufferedReader(new FileReader(fileName));
            //  System.out.println("file size: "+entrada.readLine());
            //  System.out.println("\n\n");
            searchString = "Tabla " + schema.toUpperCase() + "." + table.toUpperCase() + ":";
            optionalSearchString = "Table " + schema.toUpperCase() + "." + table.toUpperCase() + ":";

            //  System.out.println(" searchString : "+searchString);
            while ((fileLine = entrada.readLine()) != null) {

                //  System.out.println("LINE:.. "+fileLine);
                if (fileLine.equalsIgnoreCase(searchString) || fileLine.equalsIgnoreCase(optionalSearchString)) {

                    logs = new String[4];
                    for (int i = 0; i < 4; i++) {
                        //System.out.println(entrada.readLine());
                        logs[i] = entrada.readLine();
                        //System.out.println("readLogFile log::::::::::::::::::::::::::::::::::::::::::::::::::::::: "+logs[i]);
                    }

                    break;
                }
            }
            entrada.close();

        } catch (java.io.FileNotFoundException fnfex) {
            System.out.println("se presento el error: " + fnfex.toString());
        }
        //System.out.println("long: "+logs.length);

        if (logs.length == 4) {
            //System.out.println("return logs ...");
            return logs;
        } else {
            return null;
        }
    }


    /*parses read information from log file generated by SQLLoader*/
    public StringLog[] readValuesOfLog(String[] log) {

        if (log == null) {
            return null;
        }

        StringLog[] stringLog = new StringLog[log.length];
        String value;
        String name;

        //System.out.println("========================");
        //System.out.println("=    readValueOfLog    =");
        //System.out.println("========================");
        //System.out.println("======================== log.length: "+log.length);
        for (int i = 0; i < log.length; i++) {
            //System.out.println("======================== i: "+i);
            String text = log[i];
            //System.out.println("======================== log[i]: "+text);
            try {
                text = text.substring(text.indexOf("  ") + 2);

                value = text.substring(0, text.indexOf(" "));
                name = text.substring(text.indexOf(" ") + 1, text.length());
                //System.out.println("texto ... VALUE= "+value);
                //System.out.println("ttexto ...NAME= "+name);
            } catch (Exception e) {

                System.out.println("readValuesOfLog: exception1.. " + e);
                return null;
            }
            try {
                stringLog[i] = new StringLog(Integer.parseInt(value), name);
            } catch (NumberFormatException e) {
                System.out.println("readValuesOfLog: .. " + e);
                return null;

            }
        }
        //System.out.println("========================");

        return stringLog;

    }

    private class StringLog {

        private int value;
        private String name;

        /**
         * @param value
         * @param name
         */
        public StringLog(int value, String name) {
            super();
            this.value = value;
            this.name = name;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the value
         */
        public int getValue() {
            return value;
        }

        /**
         * @param value the value to set
         */
        public void setValue(int value) {
            this.value = value;
        }

    }

}
