package co.org.invemar.upload;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jdom.Document;
import org.jdom.Element;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import co.org.invemar.log.LogCreator;
import co.org.invemar.upload.table.UploadTable;
import co.org.invemar.upload.table.UploadTableManager;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.UtilFunctions;
import co.org.invemar.util.RequestParameter;
import co.org.invemar.util.RequestParameterManager;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class GenericSingleDBUpload extends HttpServlet {

	private javax.servlet.ServletContext servletContext; 

    public void init(ServletConfig config) throws ServletException {
        servletContext=config.getServletContext();
        super.init(config);
    }
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			
		doPost(request,response);
	}


	public synchronized void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		synchronized(this){
			new GenericSingleDBUploadAction(request,response,servletContext);
		}
		
	}
	

	
}
