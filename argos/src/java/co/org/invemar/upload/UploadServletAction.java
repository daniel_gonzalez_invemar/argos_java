package co.org.invemar.upload;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import javax.servlet.ServletContext;

import co.org.invemar.user.UserPermission;

import com.missiondata.fileupload.MonitoredDiskFileItemFactory;

/**
 *
 * @author Victor Adri�n Santaf� Torres.
 *
 *
 */
public class UploadServletAction {

    private String username = null;
    private String userId = null;
    private ServletContext servletContext;

    public UploadServletAction(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext) throws ServletException, IOException {

        this.servletContext = servletContext;

        response.setContentType("text/text; charset=ISO-8859-1");

        HttpSession session = request.getSession();

        username = null;

        try {
            username = session.getAttribute("username").toString();
            userId = session.getAttribute("userid").toString();

        } catch (Exception e) {
            username = null;
        }

        if (username == null) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            return;
        } else {
            //check user permission
            UserPermission userp = (UserPermission) session.getAttribute("userpermission");

            if (!userp.couldUser(UserPermission.INGRESAR_DATOS) && !userp.couldUser(UserPermission.INGRESAR_DATOS_EN_BRUTO) && !userp.couldUser(UserPermission.ACTUALIZAR_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) && !userp.couldUser(UserPermission.ACTUALIZAR_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                return;
            }
        }

        if ("view".equals(request.getParameter("action"))) {
            System.out.println("Status:... ");
            doStatus(session, response);
        } else {
            doFileUpload(session, request, response);
        }

    }

    private void doFileUpload(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Make sure the status response is not cached by the browser
        response.addHeader("Expires", "0");
        response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.addHeader("Pragma", "no-cache");
        response.setContentType("text/html");

        try {

            //configure path where file will be upload
            String pathUpload = servletContext.getRealPath("useruploads") + File.separator + userId;

            /*
             * creates an upload directory for user
             */
            if (!(new File(pathUpload)).exists()) {
                new File(pathUpload).mkdir();
            }

            System.out.println("pathUpload:.. " + pathUpload);

            FileUploadListener listener = new FileUploadListener(request.getContentLength());

            session.setAttribute("FILE_UPLOAD_STATS", listener.getFileUploadStats());

            FileItemFactory factory = new MonitoredDiskFileItemFactory(listener);
            ((DiskFileItemFactory) factory).setSizeThreshold(4096);

            System.out.println("TMP directory: " + System.getProperty("java.io.tmpdir"));
            ((DiskFileItemFactory) factory).setRepository(new File(System.getProperty("java.io.tmpdir")));
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(200000000);

            List items = upload.parseRequest(request);

            //boolean hasError = false;

            for (Iterator i = items.iterator(); i.hasNext();) {
                FileItem fileItem = (FileItem) i.next();

                if (!fileItem.isFormField()) {
                    String fileName = fileItem.getName();


                    // *************************************************
                    // This is where you would process the uploaded file
                    // *************************************************
                    if (fileName != null) {
                        fileName = FilenameUtils.getName(fileName);
                        System.out.println("Nombre del archivo:.. " + fileName);
                        //session.setAttribute("FILENAME", fileName);
                        System.out.println("--- FileName ----  " + fileName);


                    }

                    //fileItem.delete();
                    File file = new File(pathUpload, fileName);
                    
                    fileItem.write(file);
                }
            }

            while (isFileStillLoading(session)) {
                //wait for file to be loaded
            }
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);

        }catch (IOException e) {
           
            System.out.println("Error:... " + e);
            response.getWriter().write("NO");

        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error:... " + e);
            response.getWriter().write("NO");

        }
    }

    private void doStatus(HttpSession session, HttpServletResponse response) throws IOException {
        // Make sure the status response is not cached by the browser
        response.addHeader("Expires", "0");
        response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.addHeader("Pragma", "no-cache");
        response.setContentType("text/html");

        FileUploadListener.FileUploadStats fileUploadStats = (FileUploadListener.FileUploadStats) session.getAttribute("FILE_UPLOAD_STATS");
        if (fileUploadStats != null) {
            long bytesProcessed = fileUploadStats.getBytesRead();
            long sizeTotal = fileUploadStats.getTotalSize();
            long percentComplete = (long) Math.floor(((double) bytesProcessed / (double) sizeTotal) * 100.0);
            long timeInSeconds = fileUploadStats.getElapsedTimeInSeconds();
            double uploadRate = bytesProcessed / (timeInSeconds + 0.00001);
            double estimatedRuntime = sizeTotal / (uploadRate + 0.00001);
        }

        if (fileUploadStats != null && fileUploadStats.getBytesRead() == fileUploadStats.getTotalSize()) {
            session.removeAttribute("FILE_UPLOAD_STATS");
            response.getWriter().write("OK");
        } else {

            response.getWriter().write("NO");
        }
    }

    private boolean isFileStillLoading(HttpSession session) {

        boolean result = true;

        FileUploadListener.FileUploadStats fileUploadStats = (FileUploadListener.FileUploadStats) session.getAttribute("FILE_UPLOAD_STATS");
        if (fileUploadStats != null) {
            long bytesProcessed = fileUploadStats.getBytesRead();
            long sizeTotal = fileUploadStats.getTotalSize();
            long percentComplete = (long) Math.floor(((double) bytesProcessed / (double) sizeTotal) * 100.0);
            long timeInSeconds = fileUploadStats.getElapsedTimeInSeconds();
            double uploadRate = bytesProcessed / (timeInSeconds + 0.00001);
            double estimatedRuntime = sizeTotal / (uploadRate + 0.00001);
        }

        if (fileUploadStats != null && fileUploadStats.getBytesRead() == fileUploadStats.getTotalSize()) {
            result = false;
        } else {
            result = true;
        }

        return result;
    }
}
