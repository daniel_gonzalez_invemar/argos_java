package co.org.invemar.upload;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jdom.Document;

import co.org.invemar.upload.table.UploadTableManager;
import co.org.invemar.util.UtilFunctions;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class FileSpliter extends HttpServlet {

	private javax.servlet.ServletContext servletContext; 
	private ServletConfig config;
	
    public void init(ServletConfig config) throws ServletException {
        servletContext=config.getServletContext();
        this.config = config;
        super.init(config);
    }
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		doPost(request,response);
	}


	public synchronized void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		synchronized(this){
			new FileSpliterAction(request, response, servletContext);
		}
	}
	

}
