package co.org.invemar.upload;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jdom.Document;
import org.json.JSONException;
import org.json.JSONObject;

import co.org.invemar.log.LogCreator;
import co.org.invemar.upload.table.UploadTable;
import co.org.invemar.upload.table.UploadTableManager;
import co.org.invemar.user.UserPermission;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.UtilFunctions;
import javax.servlet.ServletContext;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class MultipleDBUploadUpdateAction {


	private static final String UPDATEPROCESS_RESULT_OK = "1";
	private static final String UPDATEPROCESS_RESULT_ERROR_NOIDFOUND = "2";
	private static final String UPDATEPROCESS_RESULT_ERROR_NOIDOWNER = "3";
	private static final String UPDATEPROCESS_RESULT_ERROR_BADNEWDATA = "4";
	private static final String UPDATEPROCESS_RESULT_ERROR_ILEGAL = "5";
	private static final String UPDATEPROCESS_RESULT_ERROR_BAD_DETAILFILE = "6";
	
	private ServletContext servletContext; 

	private String username = null;
	private String userId = null;
	private String projectId = null;
	private String entityId = null;
	private String ownerId = null;
	private String schema = null;
	private String thematicId = null;
	private String updateType = null;
	private LogCreator logCreator = null;
	private String userPath = null;
	private int uploadedRows = 0;
	
	Document contentXml = null;
	private UploadTableManager uTableManager;
	
	private UtilFunctions util = new UtilFunctions();
		
	//DB connection variables
	private ConnectionFactory cFactory;
	private Connection conn=null;
	
	//ids from master file
	List<String> idList = null;
	
	public MultipleDBUploadUpdateAction(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
	throws ServletException, IOException {
		
		this.servletContext = servletContext;
		
		response.setContentType("text/text; charset=ISO-8859-1");
		
		HttpSession session=request.getSession();
		JSONObject result = null;
		
		username = null;
		
    	try{
        	username = session.getAttribute("username").toString();
        	userId = session.getAttribute("userid").toString();
    	}catch(Exception e){
    		username = null;
       	}
    	
    	if(username==null){
    		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    		return;
    	}else{
			//check user permission
			UserPermission userp = (UserPermission)session.getAttribute("userpermission");
			
			if(userp.couldUser(UserPermission.ACTUALIZAR_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)){
				updateType = "project";
				ownerId = request.getParameter("ownerid");
				entityId = session.getAttribute("entityid").toString();
				if(ownerId.equals("no")){
					updateType = "user";
					ownerId = userId;
				}
			}else if(userp.couldUser(UserPermission.ACTUALIZAR_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL)){
				updateType = "user";
				ownerId = userId;
			}else{
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	    		return;
			}
			
    	}
    	
    	schema = request.getParameter("schema");
    	thematicId = request.getParameter("thematicid");
		projectId = session.getAttribute("projectid").toString();
    	uploadedRows = Integer.parseInt(request.getParameter("urows"));
    	//get user file path
    	userPath = servletContext.getRealPath("useruploads")+File.separator+userId;

    	
    	result = initProcess();
    	
    	//erase user upload path files
    	try{
    		deleteDirectoryFiles(new File (userPath));
    	}catch(Exception e){
    		
    	}
    	
    	if(result==null){
    		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    	}else{
		    response.addHeader("Expires", "0");
		    response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		    response.addHeader("Pragma", "no-cache");
			response.setContentType("application/json");
			response.getWriter().write(result.toString());
    	}
    	
	}
	
	/*leads the update process, it does the magic*/
	private JSONObject initProcess(){
		
		String verificationResult = null;
		JSONObject dataVerificationResult = null;
		JSONObject newDataInsertResult = null;
		JSONObject result = new JSONObject();
		
    	//read uplad table xml and start processing
    	contentXml = util.readXML(this.servletContext.getRealPath("WEB-INF")+File.separator+"xmlconfig"+File.separator+schema+File.separator+thematicId+File.separator+"userdbuploadtables.xml");
    	this.uTableManager = new UploadTableManager(contentXml);
    	
    	//if user is going to update data from other user, then check if it can be done!
    	if (updateType.equals("project")){
    	
    		try{
        
	        	verificationResult = this.checkUserEditionPermission(Integer.parseInt(this.ownerId), Integer.parseInt(projectId), entityId);
	        
	    	}catch(Exception e){
	    		e.printStackTrace();
	    		verificationResult="NO";
	    	}
	    	
	    	
	    	if(verificationResult.equals("NO")){
				try{
					result.put("updatestatus", this.UPDATEPROCESS_RESULT_ERROR_ILEGAL);
				}catch(Exception e){
					e.printStackTrace();
				}
				return result;
	    	}
    	
    	}
    	
    	//get ids from master file
    	idList = getIdFromMasterFile();
    	    	
    	if(idList.isEmpty()){
    		//ERROR IF IDS WERE NOT FOUND
    		try{
    			result.put("updatestatus", this.UPDATEPROCESS_RESULT_ERROR_NOIDFOUND);
    			result.put("description", "No fueron encontrados identificadores en el archivo proporcionado.");
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		
    	}else{//if ids could be getted
    		
    		//verify that ids from detail files exists in master file
    		if(verifyDetailAgainstMasterFile()){
    			
	    		//verify that the current user is the owner of those ids
	    		verificationResult = verifyIdList();
	    		
	    		//if id verification OK then 
	    		if(verificationResult.equals("true")){
	    			
	    			dataVerificationResult = processNegativeFile();
	    			
	    			try{
	        			//if data verification was OK
	        			if(dataVerificationResult.getString("filestatus").equals("1")){
	        				
	        				//AT THE END OF AGES, AFTER WHOLE THIS LOOOOONG PROCESS, PLEASE ERASE OLD DATA AND SAVE NEW :)
	        				
	        				//so, first erase old data
	        				eraseOldData();
	        				
	        				//insert new data, could you believe it? :P
	        				newDataInsertResult = processFile();
	        				        				
	        	    		try{
	        	    			result = newDataInsertResult;
	        	    			result.put("updatestatus", this.UPDATEPROCESS_RESULT_OK);
	        	    		}catch(Exception e){
	        	    			e.printStackTrace();
	        	    		}
	        	    		
	        				//now log this insert action
	        	    		/*try{
	        	    			logManager = new LogManager();
	        	    			logManager.newLog(logManager.UPDATE_DATA_LOG, this.username, this.schema);    			
	        	    		}catch(SQLException e){
	        	    			e.printStackTrace();
	        	    		}*/
	        				
	        			}else{
	        				//ERROR IF THERE DATA WAS WRONG
	        				result = dataVerificationResult;
	        				
	        	    		try{
	            				result.put("updatestatus", this.UPDATEPROCESS_RESULT_ERROR_BADNEWDATA);
	        	    		}catch(Exception e){
	        	    			e.printStackTrace();
	        	    		}
	
	        			}
	    			}catch(Exception e){
	    				e.printStackTrace();
	    			}
	    			
	    		}else{
	    			//ERROR IF SOME IDS DONT BELONG TO USER
	        		try{
	        			result.put("updatestatus", this.UPDATEPROCESS_RESULT_ERROR_NOIDOWNER);
	        			result.put("description", "Algunos registros que se desean actualizar no fueron ingresados por el usuario actual. El siguiente archivo contiene los n�meros err�neos: ");
	        			if(!verificationResult.equals("false")){
	        				result.put("badidfilepath", verificationResult);
	        			}
	        		}catch(Exception e){
	        			e.printStackTrace();
	        		}
	    		}
    		}else{
    			//ERROR IF DETAILS FILES DOES NOT BELONG TO MASTER
        		try{
        			result.put("updatestatus", this.UPDATEPROCESS_RESULT_ERROR_BAD_DETAILFILE);
        			result.put("description", "Existen registros en los archivos detalle que no coinciden con los contenidos en el archivo maestro.");
        		}catch(Exception e){
        			e.printStackTrace();
        		}
    		}
    	}
    	
    	return result;
    	
	}
	
	/*gets ids (nroinvemar) from master table*/
	private List<String> getIdFromMasterFile(){
		
		String newLine = "";
		String fileName = null;
		BufferedReader buftempfile = null;
		String id = null;
		List<String> idList = new ArrayList<String>();
		
		//prepare master file path
		fileName = this.userPath + File.separator + this.uTableManager.getUploadTableById(1).getName()+".csv"; 
		//System.out.println("fileName: "+fileName);
		try{
			
			//read master file
	 		buftempfile=new BufferedReader(new FileReader(fileName));
	 		
		 	while ((newLine = buftempfile.readLine()) != null) 
			{
		 		if(newLine!=null && !newLine.equals("") && !newLine.equals(" ")){
		 			
		 			//take ids from master file
		 			id = newLine.split(";")[0];
		 			
		 			idList.add(id);
		 			
		 		}
		 		
			}
		 	
		 	buftempfile.close();
			
		}catch(Exception e){
			e.printStackTrace();
			idList = new ArrayList<String>();
		}
		
	 	return idList;
		
	}

	/*ensure that ids from details files are related with master one*/
	private boolean verifyDetailAgainstMasterFile(){
		
		boolean result = true;
		String newLine = "";
		String fileName = null;
		BufferedReader buftempfile = null;
		String id = null;
		
		int i = 2;
		
    	//process each file - star from 2 cause 1 is master
    	for(i = 2 ; i<= uTableManager.getUploadTableListSize(); i++){
    		    		
 			if(!result){
 				break;
 			}
    		
    		//prepare file path
    		fileName = this.userPath + File.separator + this.uTableManager.getUploadTableById(i).getName()+".csv";
    		
    		try{
    			
    			//read master file
    	 		buftempfile=new BufferedReader(new FileReader(fileName));
    	 		
    		 	while ((newLine = buftempfile.readLine()) != null) 
    			{
    		 		if(newLine!=null && !newLine.equals("") && !newLine.equals(" ")){
    		 			
    		 			//take ids from master file
    		 			id = newLine.split(";")[0];
    		 			   		 			
    		 			//consult or verify
    		 			result = doesIdExistInMasterFile(id);
    		 			
    		 			if(!result){
    		 				break;
    		 			}
    		 			
    		 		}
    		 		
    			}
    		 	
    		 	buftempfile.close();
    			
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		
    	}
		
	 	return result;
		
	}
	
	/*checks if one specific id exists in master file*/
	private boolean doesIdExistInMasterFile(String id){
		
		boolean result = false;
		
		for(String mid : this.idList){
			
			if(mid.equals(id)){
				result=true;
				break;
			}
			
		}
		
		return result;
		
	}
		
	
	/*takes id  wich were obtained from master table and makes a verification*/
	private String verifyIdList(){
		
		String result = "true";
		boolean verify = true;
		
		StringBuffer badIdList = new StringBuffer();
		
		for(String id : idList){
			
			try{
				//verify id
				verify = verifyUserIdOwnerShip(id);
			}catch(Exception e){
				e.printStackTrace();
				verify = false;
			}
						
			//if the id does not belong to user append it to badIdList
			if(!verify){
				badIdList.append(id+"\n");
			}
			
		}
		
		if(badIdList.length()==0){
			result = "true";
		}else if(badIdList.length()>0){
			//if there are bad ids
			result = createBadIdFile(badIdList);
			
		}else{
			result = "false";
		}
		
		return result;
	}
	
	/*verifies that an specific id belongs to an user by making a comparision with data base*/
	private boolean verifyUserIdOwnerShip(String id) throws SQLException{
		
		boolean result = true;
		
		PreparedStatement pstmt = null;
		UploadTable uTable = uTableManager.getUploadTableById(1);
		
		try{
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection(schema);
			pstmt = conn.prepareStatement("SELECT "+uTable.getIdColumn()+" FROM "+uTable.getName()+" WHERE "+uTable.getIdColumn()+"=? AND uploadflag=?");
			pstmt.setString(1, id);
			pstmt.setString(2, this.ownerId);
			ResultSet rs = pstmt.executeQuery();
			
			//System.out.println("id: "+id+"owner: "+this.ownerId);
			
			if(rs.next()){
				result = true;
			}else{
				result = false;
			}
			
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
			result = false;
		}
		finally {
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}
	
	/*makes a file with bad ids, so user can check it after process*/
	private String createBadIdFile(StringBuffer badIdList){
		
		File newFile = null;
		BufferedWriter out = null;
		String fileNamePath = null;
		String relativeFilePath = "useruploads/"+this.userId+"/badids.txt"; 
			
		try{
			
			fileNamePath = this.userPath+File.separator+"badids.txt";
			
			newFile = new File(fileNamePath);
			
			//create new file
			newFile.createNewFile();
			
			//put bad ids on file
			out = new BufferedWriter(new FileWriter(newFile));
	       	out.write(badIdList.toString());
	        out.close();
			
		}catch(Exception e){
			e.printStackTrace();
			relativeFilePath = null;
		}
		
		return relativeFilePath;
	}
	
	/*process negative files in order to test data*/
	private JSONObject processNegativeFile(){
		
		DataBaseUploader dbUploader = null;
		UploadTable uTable = null;
		JSONObject result = null;
		boolean success = true;
		
		int i = 1;
    	
    	//process each file
    	for(i = 1 ; i<= uTableManager.getUploadTableListSize(); i++){
    		
    		//get upload table settings
    		uTable = uTableManager.getUploadTableById(i);
       		    		
    		//prepare uploader
    		dbUploader = new DataBaseUploader(this.userId, this.schema, uTable.getName(), "n"+uTable.getName()+".csv", this.servletContext);
    		
    		//process file
    		result = dbUploader.processFile();
    		
    		//verify whether everything was ok or not
    		try{
        		if(result == null || !result.getString("filestatus").equals("1")){
            		//mmm, something was wrong, so put description in result object to give user an idea of what he did
            		result.put("description", uTable.getDescription());
        			success = false;
        			break;
        		}    			
    		}catch(JSONException e){
    			e.printStackTrace();
    		}
    		    		
    	}
    	
    	for(int j = i-1 ; j>0; j--){
    		//get upload table settings
    		uTable = uTableManager.getUploadTableById(j);
			//rollback the changes made to the table
			try{
				this.rollBack(uTable.getName());
			}catch(Exception e){
				e.printStackTrace();
			}
    	}
    	return result;
    	
	}
	
	/*deletes rows from tables that cotains specific id*/
	private void eraseOldData(){
		
		UploadTable uTable = null;
		int i = 1;
		
    	//delete for each file
    	for(i = uTableManager.getUploadTableListSize() ; i>0 ; i--){
    		
    		//get upload table settings
    		uTable = uTableManager.getUploadTableById(i);
    		
    		//start erase rows containing ids
    		for(String id : idList){
    			try{
    				deleteRow(uTable, id);
    			}catch(Exception e){
    				e.printStackTrace();
    			}
    		}
    		
    	}
	}

	/*delete rows from table when upload error occurs*/
	private void deleteRow(UploadTable uTable, String id)throws SQLException {
		
		PreparedStatement pstmt = null;
		
		try{
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection(schema);
			pstmt = conn.prepareStatement("DELETE FROM "+uTable.getName()+" WHERE "+uTable.getIdColumn()+"=? AND uploadflag=?");
			pstmt.setString(1, id);
			pstmt.setString(2, this.ownerId);
			pstmt.executeUpdate();
			conn.commit();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
	}
		
	/*process new file*/
	private JSONObject processFile(){
		
		Document contentXml = null;
		DataBaseUploader dbUploader = null;
		UploadTable uTable = null;
		JSONObject result = null;
		boolean success = true;
		
		int i = 1;
    	
    	//process each file
    	for(i = 1 ; i<= uTableManager.getUploadTableListSize(); i++){
    		
    		//get upload table settings
    		uTable = uTableManager.getUploadTableById(i);
       		    		
    		//prepare uploader
    		dbUploader = new DataBaseUploader(this.userId, this.schema, uTable.getName(), uTable.getName()+".csv", this.servletContext);
    		
    		//process file
    		result = dbUploader.processFile();
    		
    		//verify whether everything was ok or not
    		try{
	        		if(result == null || !result.getString("filestatus").equals("1")){
	            		//mmm, something was wrong, so put description in result object to give user an idea of what he did
	            		result.put("description", uTable.getDescription());
	        			success = false;
	        			break;
	        		}
        		}catch(JSONException e){
    			e.printStackTrace();
    		}
    		
    	}
    	
    	//verify that all files where processed
    	if(!success){
    		
    		//start rollback from last uTable processed :)

    		for(int j=i; j>0; j--){
        		try{
    			uTable = uTableManager.getUploadTableById(j);
    			
    			this.rollBack(uTable.getName());
        		}catch(SQLException e){
        			e.printStackTrace();
    			    			
        		}    			
    		}
    	    		
    	}else{

    		//start commit
    		
    		for(int j=1; j<i; j++){
    			try{
    			uTable = uTableManager.getUploadTableById(j);
    			
    			this.commit(uTable.getName());
    			    			
        		}catch(SQLException e){
        			e.printStackTrace();
    			    			
        		}    			
    		}
    		
    		//finally log this action
    		try{
    			logCreator = new LogCreator();
    			logCreator.createLog(logCreator.ACTUALIZACION_DE_DATOS, Integer.parseInt(this.userId), Integer.parseInt(this.projectId), Integer.parseInt(this.thematicId), this.uploadedRows);    			
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    		
    	}
		
    	return result;
    	
	}
	
	/*delete rows from table when upload error occurs*/
	private void rollBack(String table)throws SQLException {
		
		PreparedStatement pstmt = null;
		
		try{
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection(schema);
			pstmt = conn.prepareStatement("DELETE FROM "+table+" WHERE uploadflag='"+this.ownerId+"_0'");
			pstmt.executeUpdate();
			conn.commit();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
	}

	/*update upload flag from tables when upload process was success*/
	private void commit(String table)throws SQLException {
		
		PreparedStatement pstmt = null;
		
		try{
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection(schema);
			pstmt = conn.prepareStatement("UPDATE "+table+" SET uploadflag='" + this.ownerId + "' WHERE uploadflag='"+this.ownerId+"_0'");
			pstmt.executeUpdate();
			conn.commit();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
	}
	
	/*checks if current user can edit information of other user*/
	public String checkUserEditionPermission(int userId, int projectId, String entityId) throws SQLException {
		
		String result = "OK";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT USUARIOS.ID FROM ((((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN CLST_PROYECTOS ON CLST_PROYECTOS.CODIGO=UROLES.ID_PROYECTO) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) WHERE USUARIOS.ID=? AND CLST_PROYECTOS.CODIGO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_DETALLES.VALOR_ASIGNADO=?");
			pstmt.setInt(1, userId);
			pstmt.setInt(2, projectId);
			pstmt.setString(3, entityId);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				result = "OK";			
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return result;
	}
	
	private void deleteDirectoryFiles(File path) {
		
	    if( path.exists() ) {
	      File[] files = path.listFiles();
	      for(int i=0; i<files.length; i++) {
	         if(files[i].isDirectory()) {
	        	 //deleteDirectoryFiles(files[i]);
	         }
	         else {
	        	 
	           if(!files[i].getName().equals("badids.txt"))
	        	 files[i].delete();
	         }
	      }
	    }
	    
	    if(!path.getAbsolutePath().equals(this.userPath))
	    	path.delete(); 
	    
	}
	
}
