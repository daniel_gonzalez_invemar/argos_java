package co.org.invemar.upload;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import co.org.invemar.log.LogCreator;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.UtilFunctions;

import org.jdom.Document;
import java.io.File;
import java.sql.Connection;

import org.json.JSONObject;
import org.json.JSONException;

import co.org.invemar.upload.table.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class MultipleDBUploadUpdate extends HttpServlet {

	private javax.servlet.ServletContext servletContext; 
	
    public void init(ServletConfig config) throws ServletException {
        servletContext=config.getServletContext();
        super.init(config);
    }
    
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			doPost(request,response);

	}

	public synchronized void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		synchronized(this){
			new MultipleDBUploadUpdateAction(request, response, this.servletContext);
		}

	}


}
