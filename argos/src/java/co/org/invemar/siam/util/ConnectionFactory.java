// Decompiled by DJ v3.12.12.96 Copyright 2011 Atanas Neshkov  Date: 13/03/2012 04:14:02 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ConnectionFactory.java

package co.org.invemar.siam.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.*;
import javax.sql.DataSource;

public class ConnectionFactory
{
   
    static InitialContext initialContext;
    private String Error;
    private Connection con  =null;
    

    public synchronized Connection createConnection(String resource)
    {
        
       try
        {
            Context envContext = (Context)initialContext.lookup("java:comp/env");
            DataSource data = (DataSource)envContext.lookup("jdbc/"+resource);
            con = data.getConnection();
        }
        catch(Exception ex)
        {
            return null;
        }
        return con;
    }
    
    public void closeConnection(Connection connection)
    {
        if(con != null)
        {
            try
            {
                con.close();
            }
            catch(SQLException e) { }
            con = null;
        }
    }
    
    
}
