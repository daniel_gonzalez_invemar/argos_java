/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.siam.servlets;

import co.org.invemar.argos.usuarios_sci.model.Udirentidades;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class DatosEntidades extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       // response.setContentType("text/event-stream");
        PrintWriter out = response.getWriter();
        
        
        String entidad = request.getParameter("entidad");
        /*
        try {
            EntityManagerFactory factory = Persistence.createEntityManagerFactory("argosPU");
            EntityManager em = factory.createEntityManager();      //em.getTransaction().begin();       

            TypedQuery<Udirentidades> queryUdirentidades = em.createNamedQuery("Udirentidades.findByCodigoIn", Udirentidades.class);
            queryUdirentidades.setParameter("codigoIn", entidad);
            List<Udirentidades> uDirentidades = queryUdirentidades.getResultList();
            out.write("<div id='datosentidad'>");
            Iterator<Udirentidades> it = uDirentidades.iterator();
            if (it.hasNext()) {
                Udirentidades u = it.next();
                out.write("<span id='direccion'>"+u.getDireccionIn()+" </span>");
                out.write("<span id='ciudad'> "+u.getCiudadIn()+" </span>");
                out.write("<span id='pais'>,"+u.getPaisIn()+" </span>");
                out.write("<span id='apartadoaereo'>| A.A "+u.getApAereoIn()+"</span><br/>");
                out.write("<span id='telefonos'>| Telefonos: "+u.getTelefonoIn()+"</span>");
                out.write("<span id='fax'>| Fax"+u.getFaxIn()+"</span><br/>");
                out.write("<span id='sitioweb'> | "+u.getContactonameIn()+"</span>");
               
             }
            
            em.close();           

            out.write("</div>");
            out.flush();
            out.close();
            
        } finally {
            out.close();
        }*/
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
