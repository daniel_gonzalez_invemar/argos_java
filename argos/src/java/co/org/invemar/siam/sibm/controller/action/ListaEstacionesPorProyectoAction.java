/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.sibm.controller.action;

import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.siam.sibm.model.ProyectosDM;
import co.org.invemar.siam.sibm.vo.Localidad;
import co.org.invemar.siam.sibm.vo.Proyecto;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.sql.Connection;

/**
 *
 * @author usrsig15
 */
public class ListaEstacionesPorProyectoAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        ConnectionFactory cFactory = null;
        Connection connection = null;
        cFactory = new ConnectionFactory();

        ProyectosDM pdm = new ProyectosDM();

        try {


            long codigoProyecto = Integer.parseInt(request.getParameter("proy"));
            String sqlextra = request.getParameter("sql");

            connection = cFactory.createConnection();
            List<Localidad> estaciones = pdm.findStationPorProyecto(connection, codigoProyecto, sqlextra.trim());

            connection = cFactory.createConnection();
            List<Proyecto> listaproyectos = pdm.findProjectsPorCodigo(connection, codigoProyecto);

            Proyecto proyecto = listaproyectos.get(0);

            StringBuffer html = new StringBuffer();

            html.append("<table align='center' border='0' width='100%'>");
            html.append("<tbody>");
            html.append("<tr>");
            html.append("<td colspan='29'>");
            html.append("<table border='0' width='100%' cellspacing='0'>");
            html.append("<tr class='tabla_fondopagina'>");
            html.append("<td  width='200' height='24'  align='left' class='linksnegros'>Estaciones Asociadas: " + estaciones.size() + "</td>");
            html.append("<td width='300'><img src='./images/browser.png' border='0' style='vertical-align:middle;'/><a class='linksnegros' href=\"javascript:PopWindow('estacionmap.jsp?proyecto=" + proyecto.getCodigo() + "&nombre=" + proyecto.getNombreAlterno() + "','Estaciones','890','600')\">Ver distribuci&oacute;n espacial aqu&iacute;</a></td>");
            html.append("<td ><img src='./images/Excel_icon.png' border='0' style='vertical-align:middle;'/><a class='linksnegros' href='./services?action=ProyectoExcel&proy=" + proyecto.getCodigo() + "&name=" + proyecto.getNombreAlterno() + "'> Descargar informaci&oacute;n en formato excel</a></td>");
            html.append("</tr>");
            html.append("</table>");
            html.append("</td>");
            html.append("</tr>");

            html.append("<tr class='tabla_fondotitulo2'>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Vigente</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Estacion</td>");
            if (codigoProyecto == 2050) {
                html.append("<td nowrap='nowrap' class='titulostablas'>Sector</td>");
            } else {
                html.append("<td nowrap='nowrap' class='titulostablas'>Ecorregi&oacute;n</td>");
            }

            html.append("<td nowrap='nowrap' class='titulostablas'>Top&oacute;nimo</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Zona protegida</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Descripcion</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Pais</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Departamento</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Municipio</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Lugar</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Latitud de inicio</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Latitud de fin</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Longitud de inicio</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Longitud de fin</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Cuerpo de agua</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Distancia de costa</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Tipo de costa</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Profundidad</td>");


            html.append("<td nowrap='nowrap' class='titulostablas'>Barco</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Campa&ntilde;a</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Sustrato</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Ambiente</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Notas</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Mar o R&iacute;o</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Salinidad</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Profundidad min (Metros)</td>");
            html.append("<td nowrap='nowrap' class='titulostablas'>Profundidad max (Metros)</td>");
            html.append("</tr>");
            for (Localidad estacion : estaciones) {


                html.append("<tr class='tabla_fondopagina' id='rowestaciones'>");
                html.append("<td nowrap='nowrap' class='texttablas'>" + estacion.getVigente() + "</td>");
                if (estacion.getPrefijoEstacion().equalsIgnoreCase("")) {
                    html.append("<td nowrap='nowrap' class='texttablas'><span style='background-color:#E6E6E6;color:#E6E6E6;font-size: 9px;padding-right: 35px;'>" + estacion.getSecuencialoc() + "</span></td>");
                } else {
                    html.append("<td nowrap='nowrap' class='texttablas'><span style='background-color:#E6E6E6;color:#E6E6E6;font-size: 9px;padding-right: 35px;'>" + estacion.getSecuencialoc() + "</span>" + estacion.getPrefijoEstacion() + "-" + estacion.getCodigoEstacion() + "</td>");
                }

                html.append("<td nowrap='nowrap' class='texttablas'>" + estacion.getEcorregion() + "</td>");
                html.append("<td nowrap='nowrap' class='texttablas'>" + estacion.getToponimo() + "</td>");
                html.append("<td nowrap='nowrap' class='texttablas'>" + estacion.getZonaProtegida() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getDescripcionEstacion() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getPais() + "</td>");
                if (codigoProyecto == 2050) {
                    html.append("<td nowrap='nowrap' class='titulostablas'>"+estacion.getSector()+"</td>");
                } else {
                    html.append("<td nowrap='nowrap' class='titulostablas'>"+estacion.getEcorregion()+"</td>");
                }
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getDepartamento() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getMunicipio() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getLugar() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getLatitudInicio() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getLatitudFin() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getLongitudInicio() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getLongitudFin() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getCuerpoAgua() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getDistanciaCosta() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getTipoCosta() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getHondoAgua() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getBarco() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getCampana() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getSustrato() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getAmbiente() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getNotas() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getMarrio() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getSalodulce() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getProfMin() + "</td>");
                html.append("<td nowrap='nowrap'  class='texttablas'>" + estacion.getProfMax() + "</td>");


                html.append("</tr>");
            }

            html.append("</tbody>");
            html.append("</table>");

            /*
             * html.append("<table cellpadding='0' cellspacing='0' border='0'
             * class='display dataTable' id='allan'>"); html.append("<thead>");
             * html.append("<tr>"); html.append("<th>Estacion</th>");
             * html.append("<th>Pais</th>");
             * html.append("<th>Departamento</th>");
             * html.append("<th>Sector</th>"); html.append("<th>Lugar</th>");
             * html.append("<th>Latitud </th>"); html.append("<th>Longitud
             * </th>"); html.append("<th>Activo</th>"); html.append("</tr>");
             * html.append("</thead>"); html.append("<tbody>"); for (Localidad
             * estacion : estaciones) { html.append("<tr class='gradeX'>");
             * html.append("<td>"+estacion.getPrefijoEstacion() + "-" +
             * estacion.getCodigoEstacion()+"</td>"); html.append("<td>" +
             * estacion.getPais() + "</td>"); html.append("<td>" +
             * estacion.getDepartamento() + "</td>"); html.append("<td>" +
             * estacion.getSector() + "</td>");
             *
             * html.append("<td>" + estacion.getLugar() + "</td>");
             * html.append("<td>" + estacion.getLatitudInicio()+"</td>");
             * html.append("<td >" + estacion.getLongitudInicio() + "</td>");
             * html.append("<td >" + estacion.getVigente() + "</td>");
             * html.append("</tr>"); } html.append("</tbody>");
             * html.append("</table>");
             */
            //System.out.println(html.toString());
            response.setContentType("text/html charset=ISO-8859-1");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(html.toString());
            //request.setAttribute("PROYS", proyectos);            
            return null;//ActionRouter("/ListaEstacionesCam.jsp");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                ConnectionFactory.closeConnection(connection);
            }
        }
        return null;
    }
}
