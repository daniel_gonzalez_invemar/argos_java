/**
 * 
 */
package co.org.invemar.siam.sibm.vo;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Administrador
 *
 */
public class Proyecto implements Serializable{
	private int codigo;
	private int metadato;
	private String titulo;
	private String nombreAlterno;
	private String fechaInicio;
	private String fechaFinalizo;
	private String ejecutor;
        private String cssaplicado;
        private String fechaEjecucion;
        private String pathLogo;
        private Date fechaInicioDate;
        private Date fechaFinalizacionDate;
        private String fechaRegistroSistemas;
        private InputStream logo;
        

    public String getCssaplicado() {
        return cssaplicado;
    }

    public void setCssaplicado(String ccsaplicado) {
        this.cssaplicado = ccsaplicado;
    }
	
	private List<Localidad> estaciones;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}



	public List<Localidad> getEstaciones() {
		return estaciones;
	}

	public void setEstaciones(List<Localidad> estaciones) {
		this.estaciones = estaciones;
	}

	public String getEjecutor() {
		return ejecutor;
	}

	public void setEjecutor(String ejecutor) {
		this.ejecutor = ejecutor;
	}

	public String getFechaFinalizo() {
		return fechaFinalizo;
	}

	public void setFechaFinalizo(String fechaFinalizo) {
		this.fechaFinalizo = fechaFinalizo;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public int getMetadato() {
		return metadato;
	}

	public void setMetadato(int metadato) {
		this.metadato = metadato;
	}

	public String getNombreAlterno() {
		return nombreAlterno;
	}

	public void setNombreAlterno(String nombreAlterno) {
		this.nombreAlterno = nombreAlterno;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

    public String getFechaEjecucion() {
        return fechaEjecucion;
    }

    public void setFechaEjecucion(String fechaEjecucion) {
        this.fechaEjecucion = fechaEjecucion;
    }

    public String getPathLogo() {
        return pathLogo;
    }

    public void setPathLogo(String pathLogo) {
        this.pathLogo = pathLogo;
    }

    public Date getFechaInicioDate() {
        return fechaInicioDate;
    }

    public void setFechaInicioDate(Date fechaInicioDate) {
        this.fechaInicioDate = fechaInicioDate;
    }

    public Date getFechaFinalizacionDate() {
        return fechaFinalizacionDate;
    }

    public void setFechaFinalizacionDate(Date fechaFinalizacionDate) {
        this.fechaFinalizacionDate = fechaFinalizacionDate;
    }

    public String getFechaRegistroSistemas() {
        return fechaRegistroSistemas;
    }

    public void setFechaRegistroSistemas(String fechaRegistroSistemas) {
        this.fechaRegistroSistemas = fechaRegistroSistemas;
    }

    public InputStream getLogo() {
        return logo;
    }

    public void setLogo(InputStream logo) {
        this.logo = logo;
    }
	
	
	
}
