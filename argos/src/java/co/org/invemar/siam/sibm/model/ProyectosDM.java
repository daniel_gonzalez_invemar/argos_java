/**
 *
 */
package co.org.invemar.siam.sibm.model;

import co.org.invemar.siam.sibm.vo.CamposBusquedaEstaciones;
import co.org.invemar.siam.sibm.vo.Departamentos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.org.invemar.siam.sibm.vo.Localidad;
import co.org.invemar.siam.sibm.vo.Proyecto;
import co.org.invemar.siam.sibm.vo.Sectores;
import co.org.invemar.siam.util.TesterConexion;
import co.org.invemar.user.UserPermission;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.DriverManager;
import java.sql.Types;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Administrador
 *
 */
public class ProyectosDM implements Serializable {

    public static void main(String[] args) {
        try {
            TesterConexion testconexion = new TesterConexion();
            Connection con = testconexion.connect("geograficos", "estaciones");
            
            ProyectosDM proyectoDAO = new ProyectosDM();
            int tarchivo = 0;

            String resultadoTransaccion = proyectoDAO.editaProyectoConArchivoImagenOCss(con, 9999, "prueba11", "minombrealterno1", new java.sql.Date(2013, 1, 20), new java.sql.Date(2013, 1, 28), "ejecutor1", 0, null, 0, "nuevaprueba.css");
            System.out.println("resultado transaccion:" + resultadoTransaccion);
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProyectosDM.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int count(Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int rtn = 0;
        String SQL = "  select count(*) from clst_proyectos ";
        try {
            pstmt = connection.prepareStatement(SQL);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                rtn = rs.getInt(1);
            }

        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        }
        return rtn;
    }

    public String findProjectsInJSON(Connection connection) throws SQLException {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = null;
        List<Proyecto> proyectos = findProjects(connection);
        Iterator<Proyecto> it = proyectos.iterator();
        int registros = 0;
        while (it.hasNext()) {
            try {
                registros++;
                jsonObject = new JSONObject();
                Proyecto proyecto = it.next();
                // jsonObject.put("ca",new Integer(registros));
                jsonObject.put("C�digo Proyecto", new Integer(proyecto.getCodigo()));
                jsonObject.put("Nombre", proyecto.getNombreAlterno());
                jsonObject.put("Ejecutor", String.valueOf(proyecto.getEjecutor()));
                jsonObject.put("Periodo Ejecuci�n", proyecto.getFechaInicio());
                jsonObject.put("Titulo Proyecto", proyecto.getTitulo());
                jsonObject.put("Tools ", "<div id='tool_proyectos'><a href=\"javascript:editarProyecto('" + String.valueOf(proyecto.getCodigo()).trim() + "')\"><div><img title='Editar' src='../../images/edit.gif'  /><a/><a href='' ><img title='Configurar Componentes' src='../../images/configurar.png'/></a><a href='' ><img title='Agregar uno Nuevo' src='../../images/agregar.png' /></a></div>");
                jsonArray.put(jsonObject);

            } catch (JSONException ex) {
                System.out.println("Error generando el json array de proyectos argos:" + ex.getMessage());
            }

        }
        return jsonArray.toString();


    }

    public String editaProyectoConArchivoImagenOCss(Connection connection,
            int codigo,
            String titulo,
            String nombrealterno,
            java.sql.Date finicio,
            java.sql.Date ffinalizo,
            String ejecutor,
            int idmetadatopro,
            InputStream logoProyecto,
            int tamanoArchivoLogo,
            String cssProyecto) {
        String resultadoTransaccion = null;
        CallableStatement st = null;
        try {

            String sql = "{call updateProyectosArgos(?,?,?,?,?,?,?,?,?,?) }";
            st = connection.prepareCall(sql);
            st.setInt(1, codigo);
            st.setString(2, titulo);
            st.setString(3, nombrealterno);
            st.setDate(4, finicio);
            st.setDate(5, ffinalizo);
            st.setString(6, ejecutor);
            st.setInt(7, idmetadatopro);
            st.setBinaryStream(8, logoProyecto, tamanoArchivoLogo);
            st.setString(9, cssProyecto);
            st.registerOutParameter(10, Types.VARCHAR);
            st.executeUpdate();
            resultadoTransaccion = st.getString(10);
        } catch (SQLException ex) {
            System.out.println("error actulizando los datos del proyecto con archivo de imagen o css:"
                    + ex.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                    connection.close();
                } catch (SQLException ex) {
                    System.out.println("Error cerrando la conexion:" + ex.toString());
                }
            }

        }
        return resultadoTransaccion;
    }
    
     public String agregaProyectoConArchivoImagenOCss(Connection connection,            
            String titulo,
            String nombrealterno,
            java.sql.Date finicio,
            java.sql.Date ffinalizo,
            String ejecutor,
            int idmetadatopro,
            InputStream logoProyecto,
            int tamanoArchivoLogo,
            String cssProyecto) {
        String resultadoTransaccion = null;
        CallableStatement st = null;
        try {

            String sql = "{call agregaProyectosArgos(?,?,?,?,?,?,?,?,?) }";
            st = connection.prepareCall(sql);           
            st.setString(1, titulo);
            st.setString(2, nombrealterno);
            st.setDate(3, finicio);
            st.setDate(4, ffinalizo);
            st.setString(5, ejecutor);
            st.setInt(6, idmetadatopro);
            st.setBinaryStream(7, logoProyecto, tamanoArchivoLogo);
            st.setString(8, cssProyecto);
            st.registerOutParameter(9, Types.VARCHAR);
            st.executeUpdate();
            resultadoTransaccion = st.getString(9);
        } catch (SQLException ex) {
            System.out.println("error agregando datos del proyecto con archivo de imagen o css:"
                    + ex.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                    connection.close();
                } catch (SQLException ex) {
                    System.out.println("Error cerrando la conexion:" + ex.toString());
                }
            }

        }
        return resultadoTransaccion;
    }

    
    

    public byte[] getByteImagenLogoProyecto(Connection connection, String codigoproyecto) {
        String sql = "SELECT pro.logo_pro FROM clst_proyectos pro WHERE pro.codigo=? ";
        byte[] buffer = null;
        PreparedStatement pstmt = null;
        java.sql.ResultSet rs = null;

        try {

            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, codigoproyecto);
            rs = pstmt.executeQuery();
            while (rs.next()) {

                Blob bin = rs.getBlob("logo_pro");
                InputStream inStream = bin.getBinaryStream();
                int size = (int) bin.length();
                buffer = new byte[size];
                buffer = bin.getBytes(1, size);
            }

        } catch (SQLException e) {
            System.out.println("Error obteniendo los byte del logo del proyecto:" + e.getMessage());

        } catch (Exception e) {
            System.out.println("Error obteniendo los byte del logo del proyecto:" + e.getMessage());

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Error cerrando la conexion:" + ex.toString());
            }

        }
        return buffer;
    }

    public List<Proyecto> findProjectsPorCodigo(Connection connection, long codigo) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Proyecto> projects = null;
        String query = "SELECT pro.codigo, "
                + "  pro.titulo, "
                + "  pro.nombre_alterno, "
                + "  pro.ejecutor, "
                + "  TO_CHAR(to_date(finicio,'dd/mm/YY')) AS finicio , "
                + "  TO_CHAR(to_date(ffinalizo,'dd/mm/YY')) AS ffinalizo, "
                + "  pro.id_metadato_pro, "
                + "  pro.css_pro "
                + "FROM clst_proyectos pro "
                + "WHERE pro.codigo=? "
                + "ORDER BY nombre_alterno";

        try {
            pstmt = connection.prepareStatement(query);
            pstmt.setLong(1, codigo);
            rs = pstmt.executeQuery();
            projects = new ArrayList<Proyecto>();
            Proyecto project = null;
            if (!rs.next()) {
                return null;
            }

            do {
                project = new Proyecto();
                project.setCodigo(rs.getInt("codigo"));
                project.setTitulo(nullValue(rs.getString("titulo")));
                project.setEjecutor(nullValue(rs.getString("ejecutor")));
                project.setNombreAlterno(nullValue(rs.getString("nombre_alterno")));
                project.setFechaInicio(nullValue(rs.getString("finicio")));
                project.setFechaFinalizo(rs.getString("ffinalizo"));
                project.setMetadato(rs.getInt("id_metadato_pro"));
                project.setCssaplicado(rs.getString("css_pro"));

                projects.add(project);
            } while (rs.next());

        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            connection.close();
        }
        return projects;
    }

    public List<Proyecto> findProjects(Connection connection) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Proyecto> projects = null;
        String query = " SELECT pro.codigo, titulo, nombre_alterno,pro.ejecutor,pro.finicio,pro.ffinalizo,pro.fecha_sistema, "
                + " finicio || ' - ' || ffinalizo periodo_ejecucion, css_pro,pro.id_metadato_pro "
                + " FROM clst_proyectos pro "
                + " order by nombre_alterno";

        try {
           // System.out.println("query:"+query);
            pstmt = connection.prepareStatement(query);

            rs = pstmt.executeQuery();
            projects = new ArrayList<Proyecto>();
            Proyecto project = null;
            if (!rs.next()) {
                return null;
            }

            do {
                project = new Proyecto();
                project.setCodigo(rs.getInt("codigo"));
                project.setTitulo(nullValue(rs.getString("titulo")));
                project.setEjecutor(nullValue(rs.getString("ejecutor")));
                project.setNombreAlterno(nullValue(rs.getString("nombre_alterno")));
                project.setFechaInicio(nullValue(rs.getString("finicio")));
                project.setFechaFinalizo(nullValue(rs.getString("ffinalizo")));
                project.setFechaEjecucion(nullValue(rs.getString("periodo_ejecucion")));
                project.setCssaplicado(nullValue(rs.getString("css_pro")));
                project.setPathLogo("../../imagenAction?codigo=" + rs.getInt("codigo"));
                project.setMetadato(rs.getInt("id_metadato_pro"));
                project.setFechaRegistroSistemas(rs.getString("fecha_sistema"));

                if (rs.getDate("finicio") != null) {
                    project.setFechaInicioDate(new java.util.Date(rs.getDate("finicio").getTime()));
                } else {
                    project.setFechaInicioDate(new java.util.Date(0));
                }
                if (rs.getDate("ffinalizo") != null) {
                    project.setFechaFinalizacionDate(new java.util.Date(rs.getDate("ffinalizo").getTime()));
                } else {
                    project.setFechaFinalizacionDate(new java.util.Date(0));
                }


//                System.out.println("F ini:"+project.getFechaInicioDate().toString());
//                System.out.println("F fin:"+project.getFechaFinalizacionDate().toString());

                projects.add(project);
            } while (rs.next());

        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            connection.close();
        }
        return projects;

    }

    public List<Localidad> findStationByProject(Connection connection, int proy, UserPermission userPermission, String pais) throws SQLException {
        List<Localidad> estaciones = null;
        PreparedStatement pstmt = null;
        Localidad estacion = null;
        ResultSet rs = null;    

        StringBuffer query = new StringBuffer("SELECT loc.codigo_estacion_loc, "
                + "  loc.descripcion_estacion_loc, "
                + "  loc.pais_loc, "
                + "  loc.marrio_loc, "
                + "  loc.salodulce_loc, "
                + "  loc.lugar, "
                + "  TRUNC(loc.latitudinicio_loc,8)                         AS latitudinicio_loc, "
                + "  TRUNC(loc.latitudfin_loc,8)                            AS latitudfin_loc, "
                + "  GEOGRAFICOS.FLATITUD(TRUNC(loc.latitudinicio_loc,8))   AS latitudiniciogms, "
                + "  GEOGRAFICOS.FLATITUD(TRUNC(loc.latitudfin_loc,8))      AS latitudfingms, "
                + "  TRUNC(loc.longitudinicio_loc,8)                        AS longitudinicio_loc, "
                + "  TRUNC(loc.longitudfin_loc,8)                           AS longitudfin_loc, "
                + "  GEOGRAFICOS.FLONGITUD(TRUNC(loc.longitudinicio_loc,8)) AS longitudiniciogms , "
                + "  GEOGRAFICOS.FLONGITUD(TRUNC(loc.longitudfin_loc,8))    AS longitudfingms, "
                + "  loc.cuerpo_agua_loc, "
                + "  loc.distanciacosta_loc, "
                + "  loc.tipocosta_loc, "
                + "  loc.hondoagua_loc, "
                + "  zn.descripcion zonaprotegida , "
                + "  loc.prefijo_cdg_est_loc, "
                + "  loc.barco_loc, "
                + "  loc.campana_loc, "
                + "  sus.descripcion_lov sustrato, "
                + "  amb.descripcion_lov ambiente, "
                + "  loc.notas_loc, "
                + "  loc.prof_min_loc, "
                + "  loc.prof_max_loc, "
                + "  loc.VIGENTE_LOC_texto, "
                + "  depto_loc_texto, "
                + "  mcpio_loc_texto, "
                + "  secuencia_loc, "
                + "  ecorregion_loc_texto,top.toponimo_texto "
                + "FROM geograficos.vestaciones_siam loc, "
                + "  curador.codigoslov sus, "
                + "  curador.codigoslov amb, "
                + "  clst_toponimia top, "
                + "  cmlst_zonas_protegidas zn "
                + "WHERE loc.proyecto_loc =" + proy + " "
                + "AND sus.codigo_lov(+)  = loc.sustrato_loc "
                + "AND sus.tabla_lov(+)   = 8 "
                + "AND amb.codigo_lov(+)  = loc.ambiente_loc "
                + "AND amb.tabla_lov(+)   = 7 "
                + "and top.toponimo_tp(+) = loc.toponimia_loc "
                + "AND zn.codigo(+)       =loc.zona_protegida ");

        if (userPermission != null) {
            if (userPermission.couldUser(UserPermission.VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) || userPermission.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {
                query.append("AND loc.pais_loc='" + pais + "'");
            }
        }
        String orderBy = " order by loc.pais_loc asc, loc.codigo_estacion_loc asc ";
        query.append(orderBy);
        try {
            
            pstmt = connection.prepareStatement(query.toString());
           
           
            rs = pstmt.executeQuery();

            estaciones = new ArrayList<Localidad>();
            if (!rs.next()) {
                return null;
            }
            do {        
                estaciones.add(populateStation(rs));
               
            } while (rs.next());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            connection.close();
        }
        return estaciones;
    }

    public List<Localidad> findStationByProject(Connection connection, int proy) throws SQLException {
        List<Localidad> estaciones = null;
        PreparedStatement pstmt = null;
        Localidad estacion = null;
        ResultSet rs = null;
        /*
         * StringBuffer query = new StringBuffer(" SELECT
         * loc.codigo_estacion_loc, loc.descripcion_estacion_loc, loc.pais_loc,
         * " + " loc.marrio_loc, loc.salodulce_loc, loc.lugar, " + "
         * TRUNC(loc.latitudinicio_loc,8) as latitudinicio_loc,
         * TRUNC(loc.latitudfin_loc,8) as latitudfin_loc, " + "
         * GEOGRAFICOS.FLATITUD(TRUNC(loc.latitudinicio_loc,8)) as
         * latitudiniciogms, GEOGRAFICOS.FLATITUD(TRUNC(loc.latitudfin_loc,8))
         * as latitudfingms, " + "TRUNC(loc.longitudinicio_loc,8) as
         * longitudinicio_loc, TRUNC(loc.longitudfin_loc,8) as longitudfin_loc,
         * " + " GEOGRAFICOS.FLONGITUD(TRUNC(loc.longitudinicio_loc,8)) as
         * longitudiniciogms ,
         * GEOGRAFICOS.FLONGITUD(TRUNC(loc.longitudfin_loc,8)) as
         * longitudfingms, " + " loc.cuerpo_agua_loc, loc.distanciacosta_loc, "
         * + " loc.tipocosta_loc, loc.hondoagua_loc,zn.descripcion zonaprotegida
         * , " + " top.toponimo_texto toponimo, loc.prefijo_cdg_est_loc,
         * loc.barco_loc, " + " loc.campana_loc, sus.descripcion_lov sustrato, "
         * + " amb.descripcion_lov ambiente, loc.notas_loc, ec.descripcion
         * ecorregion, " + " loc.prof_min_loc, loc.prof_max_loc,
         * loc.vigente_loc,dpto_loc_texto,
         * mcpio_loc_texto,secuencia_loc,ecorregion_loc_texto " + " FROM
         * vestaciones_siam loc, clst_ecorregion ec, codigoslov sus, codigoslov
         * amb, " + " clst_toponimia top, " + " cmlst_zonas_protegidas zn " + "
         * WHERE loc.proyecto_loc=" + proy + " AND ec.consecutivo(+) =
         * loc.ecorregion_loc " + " AND sus.codigo_lov(+) = loc.sustrato_loc " +
         * " AND sus.tabla_lov(+) = 8 " + " AND amb.codigo_lov(+) =
         * loc.ambiente_loc " + " AND amb.tabla_lov(+) = 7 " + " AND
         * top.toponimo_tp(+) = loc.toponimia_loc " + " AND
         * zn.codigo(+)=loc.zona_protegida ");
         */


        StringBuffer query = new StringBuffer("SELECT loc.codigo_estacion_loc, "
                + "  loc.descripcion_estacion_loc, "
                + "  loc.pais_loc, "
                + "  loc.marrio_loc, "
                + "  loc.salodulce_loc, "
                + "  loc.lugar, "
                + "  TRUNC(loc.latitudinicio_loc,8)                         AS latitudinicio_loc, "
                + "  TRUNC(loc.latitudfin_loc,8)                            AS latitudfin_loc, "
                + "  GEOGRAFICOS.FLATITUD(TRUNC(loc.latitudinicio_loc,8))   AS latitudiniciogms, "
                + "  GEOGRAFICOS.FLATITUD(TRUNC(loc.latitudfin_loc,8))      AS latitudfingms, "
                + "  TRUNC(loc.longitudinicio_loc,8)                        AS longitudinicio_loc, "
                + "  TRUNC(loc.longitudfin_loc,8)                           AS longitudfin_loc, "
                + "  GEOGRAFICOS.FLONGITUD(TRUNC(loc.longitudinicio_loc,8)) AS longitudiniciogms , "
                + "  GEOGRAFICOS.FLONGITUD(TRUNC(loc.longitudfin_loc,8))    AS longitudfingms, "
                + "  loc.cuerpo_agua_loc, "
                + "  loc.distanciacosta_loc, "
                + "  loc.tipocosta_loc, "
                + "  loc.hondoagua_loc, "
                + "  zn.descripcion zonaprotegida , "
                + "  loc.prefijo_cdg_est_loc, "
                + "  loc.barco_loc, "
                + "  loc.campana_loc, "
                + "  sus.descripcion_lov sustrato, "
                + "  amb.descripcion_lov ambiente, "
                + "  loc.notas_loc, "
                + "  loc.prof_min_loc, "
                + "  loc.prof_max_loc, "
                + "  loc.VIGENTE_LOC_texto, "
                + "  depto_loc_texto, "
                + "  mcpio_loc_texto, "
                + "  secuencia_loc, "
                + "  ecorregion_loc_texto,top.toponimo_texto "
                + "FROM geograficos.vestaciones_siam loc, "
                + "  curador.codigoslov sus, "
                + "  curador.codigoslov amb, "
                + "  clst_toponimia top, "
                + "  cmlst_zonas_protegidas zn "
                + "WHERE loc.proyecto_loc =" + proy + " "
                + "AND sus.codigo_lov(+)  = loc.sustrato_loc "
                + "AND sus.tabla_lov(+)   = 8 "
                + "AND amb.codigo_lov(+)  = loc.ambiente_loc "
                + "AND amb.tabla_lov(+)   = 7 "
                + "and top.toponimo_tp(+) = loc.toponimia_loc "
                + "AND zn.codigo(+)       =loc.zona_protegida "
                + "ORDER BY loc.pais_loc ASC, loc.codigo_estacion_loc ASC");
        // String orderBy = " order by loc.pais_loc asc, loc.codigo_estacion_loc asc ";
        // query.append(orderBy);
         //System.out.println("sql estaciones:*******************************" + query);
        try {
            pstmt = connection.prepareStatement(query.toString());
            // pstmt.setInt(1, proy);
            rs = pstmt.executeQuery();

            estaciones = new ArrayList<Localidad>();
            if (!rs.next()) {
                return null;
            }
            do {
                estaciones.add(populateStation(rs));
            } while (rs.next());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            connection.close();
        }
        return estaciones;
    }

    public List<Departamentos> findDepartamentByProyecto(Connection connection, int idProyecto) throws SQLException {
        List<Departamentos> listaDepartamentos = new ArrayList<Departamentos>();
        PreparedStatement pstmt = null;

        ResultSet rs = null;
        String query = new String("SELECT DISTINCT depto_loc, "
                + "  initcap(dpto_texto) AS nom_departamento "
                + "FROM geograficos.vestaciones_siam loc, "
                + "  curador.clst_ecorregion ec, "
                + "  curador.codigoslov sus, "
                + "  curador.codigoslov amb, "
                + "  clst_toponimia top, "
                + "  cmlst_zonas_protegidas zn "
                + "WHERE loc.proyecto_loc =? "
                + "AND ec.consecutivo(+)  = loc.ecorregion_loc "
                + "AND sus.codigo_lov(+)  = loc.sustrato_loc "
                + "AND sus.tabla_lov(+)   = 8 "
                + "AND amb.codigo_lov(+)  = loc.ambiente_loc "
                + "AND amb.tabla_lov(+)   = 7 "
                + "AND top.toponimo_tp(+) = loc.toponimia_loc "
                + "AND zn.codigo(+)       =loc.zona_protegida "
                + "ORDER BY nom_departamento");

        try {
            pstmt = connection.prepareStatement(query);
            pstmt.setInt(1, idProyecto);
            rs = pstmt.executeQuery();
            if (!rs.next()) {
                return null;
            } else {
                do {
                    Departamentos departamento = new Departamentos();
                    departamento.setCodigo(rs.getString("depto_loc"));
                    departamento.setNombre(rs.getString("nom_departamento"));
                    listaDepartamentos.add(departamento);
                } while (rs.next());
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            connection.close();
        }
        return listaDepartamentos;
    }

    private Localidad populateStation(ResultSet rs) {
        Localidad estacion = new Localidad();
        try {

            estacion.setCodigoEstacion(nullValue(rs.getString("codigo_estacion_loc")));
            estacion.setDescripcionEstacion(nullValue(rs.getString("descripcion_estacion_loc")));
            estacion.setPais(nullValue(rs.getString("pais_loc")));
            estacion.setMarrio(nullValue(rs.getString("marrio_loc")));
            estacion.setSalodulce(nullValue(rs.getString("salodulce_loc")));
            estacion.setLugar(nullValue(rs.getString("lugar")));
            estacion.setLatitudInicio(nullValue(rs.getString("latitudinicio_loc")));
            estacion.setLatitudInicioGMS(nullValue(rs.getString("latitudiniciogms")));
            estacion.setLatitudFin(nullValue(rs.getString("latitudfin_loc")));
            estacion.setLatitudFinGMS(nullValue(rs.getString("latitudfingms")));
            estacion.setLongitudInicio(nullValue(rs.getString("longitudinicio_loc")));
            estacion.setLongitudInicioGMS(nullValue(rs.getString("longitudiniciogms")));
            estacion.setLongitudFin(nullValue(rs.getString("longitudfin_loc")));
            estacion.setLongitudFinGMS(nullValue(rs.getString("longitudfingms")));
            estacion.setCuerpoAgua(nullValue(rs.getString("cuerpo_agua_loc")));
            estacion.setDistanciaCosta(nullValue(rs.getString("distanciacosta_loc")));
            estacion.setTipoCosta(nullValue(rs.getString("tipocosta_loc")));
            estacion.setHondoAgua(nullValue(rs.getString("hondoagua_loc")));
            estacion.setZonaProtegida(nullValue(rs.getString("zonaprotegida")));
            estacion.setToponimo(nullValue(rs.getString("toponimo_texto")));
            estacion.setPrefijoEstacion(nullValue(rs.getString("prefijo_cdg_est_loc")));
            estacion.setBarco(nullValue(rs.getString("barco_loc")));
            estacion.setCampana(nullValue(rs.getString("campana_loc")));
            estacion.setSustrato(nullValue(rs.getString("sustrato")));
            estacion.setAmbiente(nullValue(rs.getString("ambiente")));
            estacion.setNotas(nullValue(rs.getString("notas_loc")));
            estacion.setEcorregion(nullValue(rs.getString("ecorregion_loc_texto")));
            estacion.setProfMin(nullValue(rs.getString("prof_min_loc")));
            estacion.setProfMax(nullValue(rs.getString("prof_max_loc")));
            estacion.setVigente(nullValue(rs.getString("VIGENTE_LOC_texto")));
            estacion.setDepartamento(nullValue(rs.getString("depto_loc_texto")));
            estacion.setMunicipio(nullValue(rs.getString("mcpio_loc_texto")));
            estacion.setSector(nullValue(rs.getString("ecorregion_loc_texto")));
            //estacion.setVigente(rs.getString("vigente_loct"));
            estacion.setSecuencialoc(nullValue(rs.getString("secuencia_loc")));

        } catch (SQLException ex) {
            System.out.println("Error en cargando los datos de las estaciones:" + ex.toString());
        }
        return estacion;
    }

    public List<Localidad> findStationPorProyecto(Connection connection, long proy, String sqlextra) throws SQLException {
        List<Localidad> estaciones = null;
        PreparedStatement pstmt = null;
        Localidad estacion = null;
        ResultSet rs = null;

        /*
         * System.out.println(codSector); if ((codSector != null &&
         * !codSector.equalsIgnoreCase("undefined")) && codDepart != null) {
         * sqlextra = " and ve.sector='" + codSector + "' and ve.depto=" +
         * codDepart;
         *
         * } else if ((codSector != null &&
         * !codSector.equalsIgnoreCase("undefined")) && codDepart == null) {
         * sqlextra = " and ve.sector='" + codSector + "'"; } else if
         * ((codSector == null || codSector.equalsIgnoreCase("undefined")) &&
         * codDepart != null) { sqlextra = " and ve.depto=" + codDepart; } else
         * if ((codSector == null || codSector.equalsIgnoreCase("undefined")) &&
         * codDepart == null) { sqlextra = ""; }
         */
        StringBuffer query = new StringBuffer("SELECT loc.codigo_estacion_loc, "
                + "  loc.descripcion_estacion_loc, "
                + "  loc.pais_loc, "
                + "  loc.marrio_loc, "
                + "  loc.salodulce_loc, "
                + "  loc.lugar, "
                + "  TRUNC(loc.latitudinicio_loc,8)                         AS latitudinicio_loc, "
                + "  TRUNC(loc.latitudfin_loc,8)                            AS latitudfin_loc, "
                + "  GEOGRAFICOS.FLATITUD(TRUNC(loc.latitudinicio_loc,8))   AS latitudiniciogms, "
                + "  GEOGRAFICOS.FLATITUD(TRUNC(loc.latitudfin_loc,8))      AS latitudfingms, "
                + "  TRUNC(loc.longitudinicio_loc,8)                        AS longitudinicio_loc, "
                + "  TRUNC(loc.longitudfin_loc,8)                           AS longitudfin_loc, "
                + "  GEOGRAFICOS.FLONGITUD(TRUNC(loc.longitudinicio_loc,8)) AS longitudiniciogms , "
                + "  GEOGRAFICOS.FLONGITUD(TRUNC(loc.longitudfin_loc,8))    AS longitudfingms, "
                + "  loc.cuerpo_agua_loc, "
                + "  loc.distanciacosta_loc, "
                + "  loc.tipocosta_loc, "
                + "  loc.hondoagua_loc, "
                + "  zn.descripcion zonaprotegida , "
                + "  loc.prefijo_cdg_est_loc, "
                + "  loc.barco_loc, "
                + "  loc.campana_loc, "
                + "  sus.descripcion_lov sustrato, "
                + "  amb.descripcion_lov ambiente, "
                + "  loc.notas_loc, "
                + "  loc.prof_min_loc, "
                + "  loc.prof_max_loc, "
                + "  loc.VIGENTE_LOC_texto, "
                + "  depto_loc_texto, "
                + "  mcpio_loc_texto, "
                + "  secuencia_loc, "
                + "  ecorregion_loc_texto,top.toponimo_texto "
                + "FROM vestaciones_siam loc, "
                + "  codigoslov sus, "
                + "  codigoslov amb, "
                + "  clst_toponimia top, "
                + "  cmlst_zonas_protegidas zn "
                + "WHERE loc.proyecto_loc =" + proy + " "
                + "AND sus.codigo_lov(+)  = loc.sustrato_loc "
                + "AND sus.tabla_lov(+)   = 8 "
                + "AND amb.codigo_lov(+)  = loc.ambiente_loc "
                + "AND amb.tabla_lov(+)   = 7 "
                + "and top.toponimo_tp(+) = loc.toponimia_loc "
                + "AND zn.codigo(+)       =loc.zona_protegida " + sqlextra
                + " ORDER BY loc.pais_loc ASC, loc.codigo_estacion_loc ASC");

        //System.out.println("sql execute:" + query);
        try {
            pstmt = connection.prepareStatement(query.toString());
            // pstmt.setInt(1, proy);
            rs = pstmt.executeQuery();

            estaciones = new ArrayList<Localidad>();
            if (!rs.next()) {
                return null;
            }
            do {
                estaciones.add(populateStation(rs));
            } while (rs.next());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            connection.close();
        }
        return estaciones;
    }

    public List<Sectores> findSectorPorProyecto(Connection connection, int idProyecto) throws SQLException {
        List<Sectores> listaSectores = new ArrayList<Sectores>();
        PreparedStatement pstmt = null;

        ResultSet rs = null;
        StringBuffer query = new StringBuffer("SELECT distinct ecorregion_loc,eco_sector_texto "
                + "FROM vestaciones_siam loc, "
                + "  clst_ecorregion ec, "
                + "  codigoslov sus, "
                + "  codigoslov amb, "
                + "  clst_toponimia top, "
                + "  cmlst_zonas_protegidas zn "
                + "WHERE loc.proyecto_loc =? "
                + "AND ec.consecutivo(+)  = loc.ecorregion_loc "
                + "AND sus.codigo_lov(+)  = loc.sustrato_loc "
                + "AND sus.tabla_lov(+)   = 8 "
                + "AND amb.codigo_lov(+)  = loc.ambiente_loc "
                + "AND amb.tabla_lov(+)   = 7 "
                + "AND top.toponimo_tp(+) = loc.toponimia_loc "
                + "AND zn.codigo(+)       =loc.zona_protegida "
                + "ORDER BY eco_sector_texto ASC");

        try {
            pstmt = connection.prepareStatement(query.toString());
            pstmt.setLong(1, idProyecto);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Sectores sector = new Sectores();
                sector.setCodigo(rs.getString("ecorregion_loc"));
                sector.setNombre(rs.getString("eco_sector_texto"));
                listaSectores.add(sector);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            connection.close();
        }
        return listaSectores;
    }

    public List<CamposBusquedaEstaciones> findCamposValoresPorProyectos(Connection connection, String campo, int idProyecto) {
        List<CamposBusquedaEstaciones> listacamposBusquedaEstaciones = null;
        PreparedStatement pstmt = null;
        CamposBusquedaEstaciones campobusqueda = null;
        ResultSet rs = null;
        String query = "SELECT DISTINCT nvl(" + campo + ",null) as " + campo + ", nvl(initcap(" + campo + "),'Valor no especificado...') as etiqueta"
                + " FROM vestaciones_siam loc, "
                + "  codigoslov sus, "
                + "  codigoslov amb, "
                + "  clst_toponimia top, "
                + "  cmlst_zonas_protegidas zn "
                + "WHERE loc.proyecto_loc =" + idProyecto + " AND sus.codigo_lov(+)  = loc.sustrato_loc "
                + "AND sus.tabla_lov(+)   = 8 "
                + "AND amb.codigo_lov(+)  = loc.ambiente_loc "
                + "AND amb.tabla_lov(+)   = 7 "
                + "AND top.toponimo_tp(+) = loc.toponimia_loc "
                + "AND zn.codigo(+)       =loc.zona_protegida order by " + campo;

        try {
            //System.out.println("sql query:"+query);
            pstmt = connection.prepareStatement(query);
            rs = pstmt.executeQuery();

            listacamposBusquedaEstaciones = new ArrayList<CamposBusquedaEstaciones>();
            if (!rs.next()) {

                return null;

            }
            do {
                campobusqueda = new CamposBusquedaEstaciones();
                campobusqueda.setValor(rs.getString(campo));
                campobusqueda.setEtiqueta(rs.getString("etiqueta"));
                listacamposBusquedaEstaciones.add(campobusqueda);
            } while (rs.next());

        } catch (SQLException ex) {
            System.out.println("Error cargando el array de campos para la busqueda:" + ex.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    connection.close();
                }
                if (pstmt != null) {
                    pstmt.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(ProyectosDM.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return listacamposBusquedaEstaciones;
    }

    public List<CamposBusquedaEstaciones> findCamposBusquedaEstaciones(Connection connection) {
        List<CamposBusquedaEstaciones> listacamposBusquedaEstaciones = null;
        PreparedStatement pstmt = null;
        CamposBusquedaEstaciones campobusqueda = null;
        ResultSet rs = null;
        String query = "SELECT column_name, column_name ||'_TEXTO' cnametexto, comments etiqueta FROM user_col_comments WHERE table_name = ('VESTACIONES_SIAM') AND comments    IS NOT NULL ORDER BY 3";

        try {

            pstmt = connection.prepareStatement(query);

            rs = pstmt.executeQuery();

            listacamposBusquedaEstaciones = new ArrayList<CamposBusquedaEstaciones>();
            if (!rs.next()) {

                return null;

            }
            do {

                campobusqueda = new CamposBusquedaEstaciones();
                campobusqueda.setNombrecampo(rs.getString("column_name"));
                campobusqueda.setDescripcioncampo(rs.getString("cnametexto"));
                campobusqueda.setEtiqueta(rs.getString("etiqueta"));
                listacamposBusquedaEstaciones.add(campobusqueda);
            } while (rs.next());

        } catch (SQLException ex) {
            System.out.println("Error cargando el array de campos para la busqueda:" + ex.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    connection.close();
                }
                if (pstmt != null) {
                    pstmt.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(ProyectosDM.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return listacamposBusquedaEstaciones;
    }

    public List<Localidad> findEstationsByPoryect(Connection connection, int proy, UserPermission userPermission, String pais) throws SQLException {
        List<Localidad> estaciones = null;
        PreparedStatement pstmt = null;
        Localidad estacion = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer(" SELECT prefijo_cdg_est_loc, codigo_estacion_loc,descripcion_estacion_loc,lugar, TRUNC(latitudinicio_loc,8) latitudinicio_loc,TRUNC(latitudfin_loc,8) latitudfin_loc , TRUNC(longitudinicio_loc,8) longitudinicio_loc, TRUNC(longitudfin_loc,8) longitudfin_loc,  vigente_loc_texto "
                + " FROM vestaciones_siam "
                + " where proyecto_loc=" + proy);

        if (userPermission != null) {
            if (userPermission.couldUser(UserPermission.VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) || userPermission.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {
                query.append("AND pais_loc='" + pais + "'");
            }
        }

        try {
            //System.out.println("sql:"+query.toString());
            pstmt = connection.prepareStatement(query.toString());
            // pstmt.setInt(1, proy);
            rs = pstmt.executeQuery();

            estaciones = new ArrayList<Localidad>();
            if (!rs.next()) {
                return null;
            }
            do {
                estacion = new Localidad();
                estacion.setLatitudInicio(rs.getString("latitudinicio_loc"));
                estacion.setLatitudFin(rs.getString("latitudfin_loc"));
                estacion.setLongitudInicio(rs.getString("longitudinicio_loc"));
                estacion.setLongitudFin(rs.getString("longitudfin_loc"));
                estacion.setLugar(rs.getString("lugar"));
                estacion.setCodigoEstacion(nullValue(rs.getString("codigo_estacion_loc")));
                estacion.setPrefijoEstacion(nullValue(rs.getString("prefijo_cdg_est_loc")));
                estacion.setVigente(nullValue(rs.getString("vigente_loc_texto")));
                estaciones.add(estacion);
            } while (rs.next());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            connection.close();
        }
        return estaciones;
    }

    public List<Proyecto> findProyects(Connection connection, int begin, int length) throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Proyecto> proyectos = null;
        List<Proyecto> rtnProyectos = null;
        Proyecto proyecto = null;
        List<Localidad> estaciones = null;
        Localidad estacion = null;
        String query = " SELECT pro.codigo, titulo, nombre_alterno,pro.ejecutor, "
                + " finicio || ' - ' || ffinalizo periodo_ejecucion, id_metadato_pro, "
                + " loc.codigo_estacion_loc, loc.descripcion_estacion_loc, loc.pais_loc, "
                + " loc.marrio_loc, loc.salodulce_loc, loc.lugar, "
                + " TRUNC(loc.latitudinicio_loc,8) as latitudinicio_loc, TRUNC(loc.latitudfin_loc,8) as latitudfin_loc, TRUNC(loc.longitudinicio_loc,8) as longitudinicio_loc, "
                + " TRUNC(loc.longitudfin_loc,8) as longitudfin_loc, loc.cuerpo_agua_loc, loc.distanciacosta_loc, "
                + " loc.tipocosta_loc, loc.hondoagua_loc,zn.descripcion zonaprotegida , "
                + " top.toponimo_texto toponimo, loc.prefijo_cdg_est_loc, loc.barco_loc, "
                + " loc.campana_loc, sus.descripcion_lov sustrato, "
                + " amb.descripcion_lov ambiente, loc.notas_loc, ec.descripcion ecorregion, "
                + " loc.prof_min_loc, loc.prof_max_loc "
                + " FROM clst_proyectos pro, "
                + " vestaciones_siam loc, "
                + " clst_ecorregion ec, "
                + " codigoslov sus, "
                + " codigoslov amb, "
                + " clst_toponimia top, "
                + " cmlst_zonas_protegidas zn "
                + " WHERE pro.codigo = loc.proyecto_loc "
                + " AND ec.consecutivo(+) = loc.ecorregion_loc "
                + " AND sus.codigo_lov(+) = loc.sustrato_loc "
                + " AND sus.tabla_lov(+) = 8 "
                + " AND amb.codigo_lov(+) = loc.ambiente_loc "
                + " AND amb.tabla_lov(+) = 7 "
                + " AND top.toponimo_tp(+) = loc.toponimia_loc "
                + " and zn.codigo(+)=loc.zona_protegida "
                + " order by pro.nombre_alterno asc, loc.prefijo_cdg_est_loc asc, loc.codigo_estacion_loc asc";

        //String sqlPagin="SELECT * FROM (SELECT a.*, ROWNUM AS LIMIT FROM (" + query + ") a) WHERE LIMIT BETWEEN " + begin + " AND " + (begin + length - 1);
        //System.out.println("sqlPagin: "+sqlPagin);
        try {
            pstmt = connection.prepareStatement(query);
            rs = pstmt.executeQuery();
            int oldCodigo = -99999999;
            int newCodigo;
            int i = 0;
            int log = 0;
            proyectos = new ArrayList<Proyecto>();

            if (!rs.next()) {
                return null;
            }

            do {

                newCodigo = rs.getInt("codigo");
                if (newCodigo != oldCodigo) {
                    //System.out.println("log "+log);
                    oldCodigo = newCodigo;

                    proyecto = new Proyecto();
                    proyecto.setCodigo(newCodigo);
                    proyecto.setTitulo(nullValue(rs.getString("titulo")));
                    proyecto.setEjecutor(nullValue(rs.getString("ejecutor")));
                    proyecto.setNombreAlterno(nullValue(rs.getString("nombre_alterno")));
                    proyecto.setFechaInicio(nullValue(rs.getString("periodo_ejecucion")));
                    //proyecto.setFechaFinalizo(rs.getString(""));
                    proyecto.setMetadato(rs.getInt("id_metadato_pro"));

                    proyectos.add(proyecto);

                    estacion = new Localidad();
                    estacion.setCodigoEstacion(nullValue(rs.getString("codigo_estacion_loc")));
                    estacion.setDescripcionEstacion(nullValue(rs.getString("descripcion_estacion_loc")));
                    estacion.setPais(nullValue(rs.getString("pais_loc")));
                    estacion.setMarrio(nullValue(rs.getString("marrio_loc")));
                    estacion.setSalodulce(nullValue(rs.getString("salodulce_loc")));
                    estacion.setLugar(nullValue(rs.getString("lugar")));
                    estacion.setLatitudInicio(nullValue(rs.getString("latitudinicio_loc")));
                    estacion.setLatitudFin(nullValue(rs.getString("latitudfin_loc")));
                    estacion.setLongitudInicio(nullValue(rs.getString("longitudinicio_loc")));
                    estacion.setLongitudFin(nullValue(rs.getString("longitudfin_loc")));
                    estacion.setCuerpoAgua(nullValue(rs.getString("cuerpo_agua_loc")));
                    estacion.setDistanciaCosta(nullValue(rs.getString("distanciacosta_loc")));
                    estacion.setTipoCosta(nullValue(rs.getString("tipocosta_loc")));
                    estacion.setHondoAgua(nullValue(rs.getString("hondoagua_loc")));
                    estacion.setZonaProtegida(nullValue(rs.getString("zonaprotegida")));
                    estacion.setToponimo(nullValue(rs.getString("toponimo")));
                    estacion.setPrefijoEstacion(nullValue(rs.getString("prefijo_cdg_est_loc")));
                    estacion.setBarco(nullValue(rs.getString("barco_loc")));
                    estacion.setCampana(nullValue(rs.getString("campana_loc")));
                    estacion.setSustrato(nullValue(rs.getString("sustrato")));
                    estacion.setAmbiente(nullValue(rs.getString("ambiente")));
                    estacion.setNotas(nullValue(rs.getString("notas_loc")));
                    estacion.setEcorregion(nullValue(rs.getString("ecorregion")));
                    estacion.setProfMin(nullValue(rs.getString("prof_min_loc")));
                    estacion.setProfMax(nullValue(rs.getString("prof_max_loc")));




                    if (estaciones != null) {
                        //System.out.println("pesta length: "+estaciones.size());

                        proyectos.get(i).setEstaciones(estaciones);

                        //System.out.println("i: "+i);
                        i++;
                        estaciones = null;
                    }
                    if (estaciones == null) {
                        //System.out.println("Create estaciones");
                        estaciones = new ArrayList<Localidad>();

                        //System.out.println("proy "+proyectos.get(i).getNombreAlterno());
                        estaciones.add(estacion);
                        //System.out.println("estaciones.add(estacion) if "+estaciones.size());
                    }



                } else {
                    //System.out.println("log: "+log++);
                    estacion = new Localidad();
                    estacion.setCodigoEstacion(nullValue(rs.getString("codigo_estacion_loc")));
                    estacion.setDescripcionEstacion(nullValue(rs.getString("descripcion_estacion_loc")));
                    estacion.setPais(nullValue(rs.getString("pais_loc")));
                    estacion.setMarrio(nullValue(rs.getString("marrio_loc")));
                    estacion.setSalodulce(nullValue(rs.getString("salodulce_loc")));
                    estacion.setLugar(nullValue(rs.getString("lugar")));
                    estacion.setLatitudInicio(nullValue(rs.getString("latitudinicio_loc")));
                    estacion.setLatitudFin(nullValue(rs.getString("latitudfin_loc")));
                    estacion.setLongitudInicio(nullValue(rs.getString("longitudinicio_loc")));
                    estacion.setLongitudFin(nullValue(rs.getString("longitudfin_loc")));
                    estacion.setCuerpoAgua(nullValue(rs.getString("cuerpo_agua_loc")));
                    estacion.setDistanciaCosta(nullValue(rs.getString("distanciacosta_loc")));
                    estacion.setTipoCosta(nullValue(rs.getString("tipocosta_loc")));
                    estacion.setHondoAgua(nullValue(rs.getString("hondoagua_loc")));
                    estacion.setZonaProtegida(nullValue(rs.getString("zonaprotegida")));
                    estacion.setToponimo(nullValue(rs.getString("toponimo")));
                    estacion.setPrefijoEstacion(nullValue(rs.getString("prefijo_cdg_est_loc")));
                    estacion.setBarco(nullValue(rs.getString("barco_loc")));
                    estacion.setCampana(nullValue(rs.getString("campana_loc")));
                    estacion.setSustrato(nullValue(rs.getString("sustrato")));
                    estacion.setAmbiente(nullValue(rs.getString("ambiente")));
                    estacion.setNotas(nullValue(rs.getString("notas_loc")));
                    estacion.setEcorregion(nullValue(rs.getString("ecorregion")));
                    estacion.setProfMin(nullValue(rs.getString("prof_min_loc")));
                    estacion.setProfMax(nullValue(rs.getString("prof_max_loc")));

                    estaciones.add(estacion);
                    //System.out.println("estaciones.add(estacion) "+estaciones.size());

                }


            } while (rs.next());
            //System.out.println("AQUIIIIIIIIIII=== "+i);
            proyectos.get(i).setEstaciones(estaciones);
            rtnProyectos = new ArrayList<Proyecto>();
            for (int k = begin; k < begin + length; k++) {
                rtnProyectos.add(proyectos.get(k));
            }


        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            connection.close();
        }
        return rtnProyectos;
    }

    public List<Proyecto> findProjectsByCodigoFuente(Connection connection, String fuenteDatos) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Proyecto> projects = null;
        String query = " SELECT pro.codigo, titulo, nombre_alterno,pro.ejecutor, "
                + " finicio || ' - ' || ffinalizo periodo_ejecucion, pro.id_metadato_pro "
                + " FROM clst_proyectos pro "
                + " WHERE bitand(pro.sistema,?) > 0 "
                + " order by nombre_alterno";

        try {
            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, fuenteDatos);
            rs = pstmt.executeQuery();
            projects = new ArrayList<Proyecto>();
            Proyecto project = null;
            if (!rs.next()) {
                return null;
            }

            do {
                project = new Proyecto();
                project.setCodigo(rs.getInt("codigo"));
                project.setTitulo(nullValue(rs.getString("titulo")));
                project.setEjecutor(nullValue(rs.getString("ejecutor")));
                project.setNombreAlterno(nullValue(rs.getString("nombre_alterno")));
                project.setFechaInicio(nullValue(rs.getString("periodo_ejecucion")));
                //proyecto.setFechaFinalizo(rs.getString(""));
                project.setMetadato(rs.getInt("id_metadato_pro"));

                projects.add(project);
            } while (rs.next());

        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            connection.close();
        }
        return projects;

    }

    private String nullValue(String value) {
        return value == null ? "" : value;
    }
}
