/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.sibm.controller.action;

import co.org.invemar.siam.sibm.model.ProyectosDM;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class getLogoProyectoAction implements Action {

    public ActionRouter execute(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        try {

            ConnectionFactory cFactory = new ConnectionFactory();
            Connection connection = cFactory.createConnection();
            ProyectosDM proyecto = new ProyectosDM();

            String codigoproyecto = request.getParameter("codigoproyecto");
          
            byte[] imageBytes = null;
            if (codigoproyecto != null) {
                imageBytes = proyecto.getByteImagenLogoProyecto(connection, codigoproyecto);
                response.setContentType("image/png");
                response.setContentLength(imageBytes.length);
                response.getOutputStream().write(imageBytes);
                System.out.println("llegue aca 1");
            }


        } catch (SQLException ex) {
            System.out.println("Error obteniendo los datos la imagen logo del proyecto:" + ex.toString());
        }
        return null;
    }
}
