/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.sibm.vo;


/**
 *
 * @author usrsig15
 */

public class Localidad {

    private String codigoEstacion;
    private String secuencialoc;
    private String descripcionEstacion;
    private String pais;
    private String marrio;
    private String salodulce;
    private String lugar;
    private String latitudInicio;
    private String latitudInicioGMS;
    private String latitudFin;
    private String latitudFinGMS;
    private String longitudInicio;
    private String longitudInicioGMS;
    private String longitudFin;
    private String longitudFinGMS;
    private String cuerpoAgua;
    private String distanciaCosta;
    private String tipoCosta;
    private String hondoAgua;
    private String zonaProtegida;
    private String toponimo;
    private String prefijoEstacion;
    private String barco;
    private String campana;
    private String sustrato;
    private String ambiente;
    private String notas;
    private String ecorregion;
    private String profMin;
    private String profMax;
    private String vigente;
    private String sector;
    private String departamento;
    private String municipio;

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }
    private String etiqueta;
    private String estacion;

    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getSecuencialoc() {
        return secuencialoc;
    }

    public void setSecuencialoc(String secuencialoc) {
        this.secuencialoc = secuencialoc;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String getBarco() {
        return barco;
    }

    public void setBarco(String barco) {
        this.barco = barco;
    }

    public String getCampana() {
        return campana;
    }

    public void setCampana(String campana) {
        this.campana = campana;
    }

    public String getCodigoEstacion() {
        return codigoEstacion;
    }

    public void setCodigoEstacion(String codigoEstacion) {
        this.codigoEstacion = codigoEstacion;
    }

    public String getCuerpoAgua() {
        return cuerpoAgua;
    }

    public void setCuerpoAgua(String cuerpoAgua) {
        this.cuerpoAgua = cuerpoAgua;
    }

    public String getDescripcionEstacion() {
        return descripcionEstacion;
    }

    public void setDescripcionEstacion(String descripcionEstacion) {
        this.descripcionEstacion = descripcionEstacion;
    }

    public String getDistanciaCosta() {
        return distanciaCosta;
    }

    public void setDistanciaCosta(String distanciaCosta) {
        this.distanciaCosta = distanciaCosta;
    }

    public String getEcorregion() {
        return ecorregion;
    }

    public void setEcorregion(String ecorregion) {
        this.ecorregion = ecorregion;
    }

    public String getHondoAgua() {
        return hondoAgua;
    }

    public void setHondoAgua(String hondoAgua) {
        this.hondoAgua = hondoAgua;
    }

    public String getLatitudFin() {
        return latitudFin;
    }

    public void setLatitudFin(String latitudFin) {
        this.latitudFin = latitudFin;
    }

    public String getLatitudInicio() {
        return latitudInicio;
    }

    public void setLatitudInicio(String latitudInicio) {
        this.latitudInicio = latitudInicio;
    }

    public String getLongitudFin() {
        return longitudFin;
    }

    public void setLongitudFin(String longitudFin) {
        this.longitudFin = longitudFin;
    }

    public String getLongitudInicio() {
        return longitudInicio;
    }

    public void setLongitudInicio(String longitudInicio) {
        this.longitudInicio = longitudInicio;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getMarrio() {
        return marrio;
    }

    public void setMarrio(String marrio) {
        this.marrio = marrio;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getPrefijoEstacion() {
        return prefijoEstacion;
    }

    public void setPrefijoEstacion(String prefijoEstacion) {
        this.prefijoEstacion = prefijoEstacion;
    }

    public String getProfMax() {
        return profMax;
    }

    public void setProfMax(String profMax) {
        this.profMax = profMax;
    }

    public String getProfMin() {
        return profMin;
    }

    public void setProfMin(String profMin) {
        this.profMin = profMin;
    }

    public String getSalodulce() {
        return salodulce;
    }

    public void setSalodulce(String salodulce) {
        this.salodulce = salodulce;
    }

    public String getSustrato() {
        return sustrato;
    }

    public void setSustrato(String sustrato) {
        this.sustrato = sustrato;
    }

    public String getTipoCosta() {
        return tipoCosta;
    }

    public void setTipoCosta(String tipoCosta) {
        this.tipoCosta = tipoCosta;
    }

    public String getToponimo() {
        return toponimo;
    }

    public void setToponimo(String toponimo) {
        this.toponimo = toponimo;
    }

    public String getZonaProtegida() {
        return zonaProtegida;
    }

    public void setZonaProtegida(String zonaProtegida) {
        this.zonaProtegida = zonaProtegida;
    }

    public String getLatitudInicioGMS() {
        return latitudInicioGMS;
    }

    public void setLatitudInicioGMS(String latitudInicioGMS) {
        this.latitudInicioGMS = latitudInicioGMS;
    }

    public String getLatitudFinGMS() {
        return latitudFinGMS;
    }

    public void setLatitudFinGMS(String latitudFinGMS) {
        this.latitudFinGMS = latitudFinGMS;
    }

    public String getLongitudInicioGMS() {
        return longitudInicioGMS;
    }

    public void setLongitudInicioGMS(String longitudInicioGMS) {
        this.longitudInicioGMS = longitudInicioGMS;
    }

    public String getLongitudFinGMS() {
        return longitudFinGMS;
    }

    public void setLongitudFinGMS(String longitudFinGMS) {
        this.longitudFinGMS = longitudFinGMS;
    }

    public String getVigente() {
        return vigente;
    }

    public void setVigente(String vigente) {
        this.vigente = vigente;
    }
}
