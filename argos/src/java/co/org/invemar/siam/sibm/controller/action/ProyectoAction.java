/**
 * 
 */
package co.org.invemar.siam.sibm.controller.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.org.invemar.siam.sibm.model.ProyectosDM;
import co.org.invemar.siam.sibm.vo.Localidad;
import co.org.invemar.siam.sibm.vo.Proyecto;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

/**
 * @author Administrador
 *
 */
public class ProyectoAction implements Action{

	public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		ConnectionFactory cFactory = null;
		Connection connection = null;
		cFactory = new ConnectionFactory();
		
		List<Proyecto> proyectos=null;
		ProyectosDM pdm=new ProyectosDM();
		String codFuente=request.getParameter("cdo");
		codFuente=codFuente==null?"all":codFuente;
		if(codFuente.equalsIgnoreCase("all")){
			codFuente=String.valueOf(((int)Math.pow(2, 16)-1));
		}
		try {
			connection = cFactory.createConnection();
			proyectos=pdm.findProjectsByCodigoFuente(connection,codFuente);
			request.setAttribute("PROYS", proyectos);
			
			return new ActionRouter("/proyectos.jsp"); 
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		
		return new ActionRouter("/proyectos.jsp");
		
	}
	
}
