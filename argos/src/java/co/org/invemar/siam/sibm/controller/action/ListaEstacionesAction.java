/**
 * 
 */
package co.org.invemar.siam.sibm.controller.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.org.invemar.siam.sibm.model.ProyectosDM;
import co.org.invemar.siam.sibm.vo.Localidad;
import co.org.invemar.user.UserPermission;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

/**
 * @author Administrador
 *
 */
public class ListaEstacionesAction implements Action{

	public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ConnectionFactory cFactory = null;
		Connection connection = null;
		cFactory = new ConnectionFactory();
		List<Localidad> estaciones=null;
		ProyectosDM pdm=new ProyectosDM();
		JSONObject jsonEstacion=null;
		JSONArray jsonEstaciones=new JSONArray();
		int proyecto=Integer.parseInt(request.getParameter("proyect"));
		try{
			
			connection = cFactory.createConnection();
			HttpSession session=request.getSession();
			UserPermission userPermission=(UserPermission)session.getAttribute("userpermission");
			String pais=(String)session.getAttribute("pais");
			estaciones= pdm.findEstationsByPoryect(connection, proyecto,userPermission,pais);
			String est;
			for(Localidad estacion:estaciones){
				jsonEstacion=new JSONObject();
				est=estacion.getPrefijoEstacion()==null || estacion.getPrefijoEstacion()==""?"":estacion.getPrefijoEstacion()+"-";
				jsonEstacion.put("estacion", est+""+estacion.getCodigoEstacion());
				jsonEstacion.put("descripcion",estacion.getDescripcionEstacion() );
				jsonEstacion.put("lugar", estacion.getLugar());
				jsonEstacion.put("latinicio", estacion.getLatitudInicio());
				jsonEstacion.put("latfin", estacion.getLatitudFin());
				jsonEstacion.put("longinicio", estacion.getLongitudInicio());
				jsonEstacion.put("longfin", estacion.getLongitudFin());
				jsonEstacion.put("vigente", estacion.getVigente());
				
				jsonEstaciones.put(jsonEstacion);
			}
		}catch(SQLException e){
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(connection!=null){
				ConnectionFactory.closeConnection(connection);
			}
		}
		response.getWriter().write(jsonEstaciones.toString());
		return null;
	}

}
