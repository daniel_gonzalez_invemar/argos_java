/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.sibm.controller.action;

import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import co.org.invemar.siam.sibm.model.ProyectosDM;
import co.org.invemar.siam.sibm.vo.Departamentos;

import co.org.invemar.siam.sibm.vo.Sectores;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class DepartamentosPorProyectoAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            ConnectionFactory cFactory = null;
            Connection connection = null;
            cFactory = new ConnectionFactory();


            ProyectosDM pdm = new ProyectosDM();
            int idproyecto = Integer.parseInt(request.getParameter("idproyecto"));
            StringBuffer select = null;

            connection = cFactory.createConnection();
            List<Departamentos> listadepartamentos = pdm.findDepartamentByProyecto(connection, idproyecto);
            select = new StringBuffer("<select id='selparametro' name ='selparametro'  >");
            Iterator<Departamentos> it = listadepartamentos.iterator();
            while (it.hasNext()) {
                Departamentos departamento = it.next();
                select.append("<option value=" + departamento.getCodigo() + ">" + departamento.getNombre() + "</option>");
            }
            select.append("</select>");



            response.setContentType("text/html charset=ISO-8859-1");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(select.toString());


        } catch (SQLException ex) {
            Logger.getLogger(DepartamentosPorProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
