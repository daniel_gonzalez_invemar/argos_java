/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.sibm.vo;

/**
 *
 * @author usrsig15
 */
public class Especie {
    private String nombre;
    private String codEspecie;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodEspecie() {
        return codEspecie;
    }

    public void setCodEspecie(String codEspecie) {
        this.codEspecie = codEspecie;
    }
    
}
