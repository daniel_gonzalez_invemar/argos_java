/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.sibm.vo;

/**
 *
 * @author usrsig15
 */
public class CamposBusquedaEstaciones {
    private String nombrecampo = null;
    private String descripcioncampo = null;
    private String etiqueta = null;
    private String valor    = null;
    

    public String getDescripcioncampo() {
        return descripcioncampo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public void setDescripcioncampo(String descripcioncampo) {
        this.descripcioncampo = descripcioncampo;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getNombrecampo() {
        return nombrecampo;
    }

    public void setNombrecampo(String nombrecampo) {
        this.nombrecampo = nombrecampo;
    }
    
    
}
