/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.sibm.controller.action;

import co.org.invemar.siam.sibm.model.ProyectosDM;
import co.org.invemar.util.ConnectionFactory;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author usrsig15
 */
public class EditarProyectoConArchivoImagenOCssAction extends HttpServlet {

    private String dirUploadFiles;
    private File archivologo = null;
    private File archivocss = null;
    private long tamanoarchivologo = 0;
    private String nombreachivologo = "";
    private String tipoarchivologo = "";
    private String nombreachivologocss = "";
    private long tamanoarchivocss = 0;
    private String titulo = "";
    private String codigo = "";
    private String nombreAlterno = "";
    private String fechainicio = "";
    private String fechafinalizo = "";
    private String ejecutor = "";
    private String sistemasiamalquepertenece = "";
    private String idmetadatopro = "0";
    private InputStream islogo = null;
   /* private InputStream iscss = null;*/

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();


        dirUploadFiles = getServletContext().getRealPath(
                getServletContext().getInitParameter("dirUploadFiles"));

        if (ServletFileUpload.isMultipartContent(request)) {

            FileItemFactory factory = new DiskFileItemFactory();

            ServletFileUpload upload = new ServletFileUpload(factory);

            upload.setSizeMax(new Long(getServletContext().getInitParameter(
                    "maxFileSize")).longValue()); // 1024 x 300 = 307200 bytes
            // = 300 Kb

            List listUploadFiles = null;
            FileItem item = null;

            try {
                listUploadFiles = upload.parseRequest(request);

                Iterator it = listUploadFiles.iterator();
                int vecesejecutado = 0;
                while (it.hasNext()) {
                    item = (FileItem) it.next();
                    vecesejecutado++;

                    if (!item.isFormField()) {

                        if (item.getSize() > 0) {

                            if (item.getFieldName().equalsIgnoreCase("logoproyectofile")) {
                                tamanoarchivologo = item.getSize();
                                nombreachivologo = item.getName();
                                //System.out.println("archivo tam�ano logo:" + tamanoarchivologo);
                                String tipo = item.getContentType();

                                String extension = nombreachivologo.substring(nombreachivologo.lastIndexOf("."));
                                archivologo = new File(dirUploadFiles, nombreachivologo);
                                item.write(archivologo);

                                try {
                                    islogo = new BufferedInputStream(new FileInputStream(archivologo));
                                } catch (FileNotFoundException e) {

                                    System.out.println("error leyendo el archivo para la db:" + e.getMessage());
                                }

                            }
//                            if (item.getFieldName().equalsIgnoreCase("cssproyectofile")) {
//                                tamanoarchivocss = item.getSize();
//                                System.out.println("archivo tam�ano css:" + tamanoarchivocss);
//                                nombreachivologocss = item.getName();
//                                String tipo = item.getContentType();
//
//                                String extension = nombreachivologocss.substring(nombreachivologocss.lastIndexOf("."));
//                                archivocss = new File(dirUploadFiles, nombreachivologocss);
//                                item.write(archivocss);
//                                
//                                  try {
//                                    iscss = new BufferedInputStream(new FileInputStream(archivocss));
//                                    
//                                } catch (FileNotFoundException e) {
//
//                                    System.out.println("error leyecto el archivo para la db:" + e.getMessage());
//                                }
//                            }


                        }
                    } else {
                        String param = item.getFieldName();

                         if (param.equals("codigo")) {
                            codigo = item.getString();
                            System.out.println("codigo:" + codigo);
                        } 
                        if (param.equals("titulotxt")) {
                            titulo = item.getString();
                            System.out.println("titulo:" + titulo);
                        }
                        if (param.equals("nombrealternotxt")) {
                            nombreAlterno = item.getString();
                            System.out.println("alterno:" + nombreAlterno);
                        }
                        if (param.equals("fechainiciotxt")) {
                            fechainicio = item.getString();
                            System.out.println("fechainiciotxt" + fechainicio);
                        }
                        if (param.equals("fechafinalizaciontxt")) {
                            fechafinalizo = item.getString();
                            System.out.println(fechafinalizo);
                        }
                        if (param.equals("ejecutortxt")) {
                            ejecutor = item.getString();
                            System.out.println(ejecutor);
                        }
                        if (param.equals("sistemasiamcmb")) {
                            sistemasiamalquepertenece = item.getString();
                            System.out.println(sistemasiamalquepertenece);

                        }
                        if (param.equals("idmetadatopro")) {
                            idmetadatopro = item.getString();
                            System.out.println(idmetadatopro);

                        }
                        if (param.equals("cssproyectofile")) {
                            idmetadatopro = item.getString();
                            System.out.println(idmetadatopro);

                        }

                    }
                }



                ConnectionFactory cFactory = new ConnectionFactory();
                Connection connection = cFactory.createConnection("geograficos");
                ProyectosDM proyectoDAO = new ProyectosDM();
                int tarchivo = (int)tamanoarchivologo;
                String resultadoTransaccion = "con error";
//                       resultadoTransaccion= proyectoDAO.editaProyectoConArchivoImagenOCss(connection,codigo,titulo,fechainicio,nombreAlterno,fechafinalizo, ejecutor, idmetadatopro,sistemasiamalquepertenece,islogo,tarchivo,nombreachivologocss); 
                System.out.println("resultado transaccion:"+resultadoTransaccion);
                String redireccionaPagina= "/editarProyecto.jsp?codigoproyecto="+codigo;
                if (resultadoTransaccion.equalsIgnoreCase("sin errror")) {                   
                    redireccionaPagina="/editarProyecto.jsp?codigoproyecto="+codigo+"&error=false&mensaje=Se han actualizado los datos de forma correcta.";
                }else{
                    redireccionaPagina="/editarProyecto.jsp??codigoproyecto="+codigo+"&error=true&mensaje=Ha ocurrid un error en la actualizacion de datos...Consulte con el Administrador.";
                }
               
                response.sendRedirect(redireccionaPagina);
                        // Boolean resultado = archivo.delete();
                        // if (resultado) {
                        //   System.out.println("archivo borrado");
                        // }


            } catch (FileUploadException e) {
                System.out.println("Error subiento al servidor de aplicaciones:" + e.getMessage());

            } catch (Exception e) {
                System.out.println("Error subiento al servidor de aplicaciones:" + e.getMessage());

            } finally {
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
