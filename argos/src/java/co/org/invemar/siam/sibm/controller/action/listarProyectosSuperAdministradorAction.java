/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.siam.sibm.controller.action;

import co.org.invemar.siam.sibm.model.ProyectosDM;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class listarProyectosSuperAdministradorAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        response.setContentType("text/text charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();

        ConnectionFactory cFactory = new ConnectionFactory();
        Connection connection = null;

        try {
            connection = cFactory.createConnection();
            ProyectosDM proyectos = new ProyectosDM();
            String jsonarray = proyectos.findProjectsInJSON(connection);
            pw.println(jsonarray);

        } catch (SQLException e) {
            System.out.println("Error en action de listar proyectos:" + e.getMessage());
        } finally {
            if (connection != null) {
                ConnectionFactory.closeConnection(connection);
            }
        }

        return null;

    }
}
