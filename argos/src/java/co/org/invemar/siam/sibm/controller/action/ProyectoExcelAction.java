package co.org.invemar.siam.sibm.controller.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import co.org.invemar.siam.sibm.model.ProyectosDM;
import co.org.invemar.siam.sibm.vo.Localidad;
import co.org.invemar.siam.sibm.vo.Proyecto;
import co.org.invemar.user.UserPermission;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;

public class ProyectoExcelAction implements Action {

    public ActionRouter execute(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        // TODO Auto-generated method stub

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("Hoja de datos");
        HSSFRow row = null;
        int i = 0;
        int j = 0;


        ConnectionFactory cFactory = null;
        Connection connection = null;
        cFactory = new ConnectionFactory();

        ProyectosDM pdm = new ProyectosDM();

        String proyId = request.getParameter("proy");
        String proyName = request.getParameter("name");
        List<Localidad> estaciones = null;
        row = sheet.createRow((short) i++);
        row = sheet.createRow((short) i++);
        createCell("PROYECTO", wb, row, (short) j++);
        createCell(proyName, wb, row, (short) j++);
        row = sheet.createRow((short) i++);

        row = sheet.createRow((short) i++);
        j = 0;
        createCell("VIGENTE", wb, row, (short) j++);
        createCell("ESTACION", wb, row, (short) j++);
        if (proyId.equalsIgnoreCase("2050")) {
            createCell("SECTOR", wb, row, (short) j++);
        } else {
            createCell("ECORREGION", wb, row, (short) j++);
        }

        createCell("TOPONIMO", wb, row, (short) j++);
        createCell("ZONA PROTEGIDA", wb, row, (short) j++);
        createCell("DESCRIPCION", wb, row, (short) j++);
        createCell("PAIS", wb, row, (short) j++);
        createCell("DEPARTAMENTO", wb, row, (short) j++);
        createCell("LUGAR", wb, row, (short) j++);
        createCell("LATITUD DE INICIO", wb, row, (short) j++);
        createCell("LATITUD DE INICIO GMS", wb, row, (short) j++);
        createCell("LATITUD DE FIN", wb, row, (short) j++);
        createCell("LATITUD DE FIN GMS", wb, row, (short) j++);
        createCell("LONGITUD DE INICIO", wb, row, (short) j++);
        createCell("LONGITUD DE INICIO GMS", wb, row, (short) j++);
        createCell("LONGITUD DE FIN", wb, row, (short) j++);
        createCell("LONGITUD DE FIN GMS", wb, row, (short) j++);
        createCell("CUERPO DE AGUA", wb, row, (short) j++);
        createCell("DISTANCIA DE COSTA", wb, row, (short) j++);
        createCell("TIPO DE COSTA", wb, row, (short) j++);
        createCell("PROFUNDIDAD", wb, row, (short) j++);
        createCell("BARCO", wb, row, (short) j++);
        createCell("CAMPA�A", wb, row, (short) j++);
        createCell("SUSTRATO", wb, row, (short) j++);
        createCell("AMBIENTE", wb, row, (short) j++);
        createCell("NOTAS", wb, row, (short) j++);
        createCell("MAR O RIO", wb, row, (short) j++);
        createCell("SALINIDAD", wb, row, (short) j++);
        createCell("PRODUNDIAD MIN (METROS)", wb, row, (short) j++);
        createCell("PRODUNDIAD MAX (METROS)", wb, row, (short) j++);
       

        try {



            connection = cFactory.createConnection();
            HttpSession session = request.getSession();
            UserPermission userPermission = (UserPermission) session.getAttribute("userpermission");
            String pais = (String) session.getAttribute("pais");
            estaciones = pdm.findStationByProject(connection, Integer.parseInt(proyId), userPermission, pais);

            for (Localidad estacion : estaciones) {
                row = sheet.createRow((short) i);
                j = 0;
                createCell(isNull(estacion.getVigente()), wb, row, (short) j++);
                createCell(isNull((estacion.getPrefijoEstacion() == "" ? "" : estacion.getPrefijoEstacion() + " ") + estacion.getCodigoEstacion()), wb, row,
                        (short) j++);
                createCell(isNull(estacion.getEcorregion()), wb, row, (short) j++);
                createCell(isNull(estacion.getToponimo()), wb, row, (short) j++);
                createCell(isNull(estacion.getZonaProtegida()), wb, row, (short) j++);
                createCell(isNull(estacion.getDescripcionEstacion()), wb, row, (short) j++);
                createCell(isNull(estacion.getPais()), wb, row, (short) j++);
                createCell(isNull(estacion.getDepartamento()), wb, row, (short) j++);
                createCell(isNull(estacion.getLugar()), wb, row, (short) j++);
                createCell(isNull(estacion.getLatitudInicio()), wb, row, (short) j++);
                createCell(isNull(estacion.getLatitudInicioGMS()), wb, row, (short) j++);
                createCell(isNull(estacion.getLatitudFin()), wb, row, (short) j++);
                createCell(isNull(estacion.getLatitudFinGMS()), wb, row, (short) j++);
                createCell(isNull(estacion.getLongitudInicio()), wb, row, (short) j++);
                createCell(isNull(estacion.getLongitudInicioGMS()), wb, row, (short) j++);
                createCell(isNull(estacion.getLongitudFin()), wb, row, (short) j++);
                createCell(isNull(estacion.getLongitudFinGMS()), wb, row, (short) j++);
                createCell(isNull(estacion.getCuerpoAgua()), wb, row, (short) j++);
                createCell(isNull(estacion.getDistanciaCosta()), wb, row, (short) j++);
                createCell(isNull(estacion.getTipoCosta()), wb, row, (short) j++);
                createCell(isNull(estacion.getHondoAgua()), wb, row, (short) j++);
                createCell(isNull(estacion.getBarco()), wb, row, (short) j++);
                createCell(isNull(estacion.getCampana()), wb, row, (short) j++);
                createCell(isNull(estacion.getSustrato()), wb, row, (short) j++);
                createCell(isNull(estacion.getAmbiente()), wb, row, (short) j++);
                createCell(isNull(estacion.getNotas()), wb, row, (short) j++);
                createCell(isNull(estacion.getMarrio()), wb, row, (short) j++);
                createCell(isNull(estacion.getSalodulce()), wb, row, (short) j++);
                createCell(isNull(estacion.getProfMin()), wb, row, (short) j++);
                createCell(isNull(estacion.getProfMax()), wb, row, (short) j++);
               

                i++;
            }


        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (connection != null) {
                ConnectionFactory.closeConnection(connection);
            }
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\""
                + "Estaciones_" + proyName + ".xls" + "\"");
        response.setHeader("Pragma", "no-cache");

        ServletOutputStream outs = response.getOutputStream();
        wb.write(outs);
        outs.flush();
        outs.close();

        return null;
    }

    private static void createCell(String value, HSSFWorkbook wb, HSSFRow row,
            short column) {
        HSSFCell cell = row.createCell(column);
        cell.setCellValue(new HSSFRichTextString(value));
        /*
         * HSSFCellStyle cellStyle = wb.createCellStyle();
         * cellStyle.setAlignment(align); cell.setCellStyle(cellStyle);
         */
    }

    private static void createCell(int value, HSSFWorkbook wb, HSSFRow row,
            short column/*
             * , short align
             */) {
        HSSFCell cell = row.createCell(column);
        cell.setCellValue(value);
        /*
         * HSSFCellStyle cellStyle = wb.createCellStyle();
         * cellStyle.setAlignment(align); cell.setCellStyle(cellStyle);
         */
    }

    private String isNull(String str) {
        String cadena = "";
        if (str != null) {
            cadena = str;
        }
        return cadena;
    }
}
