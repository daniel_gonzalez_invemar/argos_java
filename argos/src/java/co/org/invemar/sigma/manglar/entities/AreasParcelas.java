/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "AREAS_PARCELAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AreasParcelas.findAll", query = "SELECT a FROM AreasParcelas a"),
    @NamedQuery(name = "AreasParcelas.findByCodparcela", query = "SELECT a FROM AreasParcelas a WHERE a.areasParcelasPK.codparcela = :codparcela"),
    @NamedQuery(name = "AreasParcelas.findByAnio", query = "SELECT a FROM AreasParcelas a WHERE a.areasParcelasPK.anio = :anio"),
    @NamedQuery(name = "AreasParcelas.findByNumeroSubparcelas", query = "SELECT a FROM AreasParcelas a WHERE a.areasParcelasPK.numeroSubparcelas = :numeroSubparcelas"),
    @NamedQuery(name = "AreasParcelas.findByArea", query = "SELECT a FROM AreasParcelas a WHERE a.area = :area"),
    @NamedQuery(name = "AreasParcelas.findByUnidad", query = "SELECT a FROM AreasParcelas a WHERE a.unidad = :unidad")})
public class AreasParcelas implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AreasParcelasPK areasParcelasPK;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "UNIDAD")
    private String unidad;

    public AreasParcelas() {
    }

    public AreasParcelas(AreasParcelasPK areasParcelasPK) {
        this.areasParcelasPK = areasParcelasPK;
    }

    public AreasParcelas(BigInteger codparcela, BigInteger anio, BigInteger numeroSubparcelas) {
        this.areasParcelasPK = new AreasParcelasPK(codparcela, anio, numeroSubparcelas);
    }

    public AreasParcelasPK getAreasParcelasPK() {
        return areasParcelasPK;
    }

    public void setAreasParcelasPK(AreasParcelasPK areasParcelasPK) {
        this.areasParcelasPK = areasParcelasPK;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (areasParcelasPK != null ? areasParcelasPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AreasParcelas)) {
            return false;
        }
        AreasParcelas other = (AreasParcelas) object;
        if ((this.areasParcelasPK == null && other.areasParcelasPK != null) || (this.areasParcelasPK != null && !this.areasParcelasPK.equals(other.areasParcelasPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.sigma.manglar.entities.AreasParcelas[ areasParcelasPK=" + areasParcelasPK + " ]";
    }
    
}
