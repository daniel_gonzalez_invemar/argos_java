/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "CASOSESTUDIO_ATRIBUTOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CasosestudioAtributos.findAll", query = "SELECT c FROM CasosestudioAtributos c"),
    @NamedQuery(name = "CasosestudioAtributos.findByCodigoAtributo", query = "SELECT c FROM CasosestudioAtributos c WHERE c.codigoAtributo = :codigoAtributo"),
    @NamedQuery(name = "CasosestudioAtributos.findByValor2", query = "SELECT c FROM CasosestudioAtributos c WHERE c.valor2 = :valor2"),
    @NamedQuery(name = "CasosestudioAtributos.findById", query = "SELECT c FROM CasosestudioAtributos c WHERE c.id = :id")})
public class CasosestudioAtributos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "CODIGO_ATRIBUTO")
    private BigInteger codigoAtributo;
    @Column(name = "VALOR2")
    private String valor2;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private BigDecimal id;
    @JoinColumn(name = "VALORES_CODIGO", referencedColumnName = "CODIGO")
    @ManyToOne
    private Atributos valoresCodigo;
    @JoinColumn(name = "CASOSESTUDIO_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Casosestudio casosestudioId;

    public CasosestudioAtributos() {
    }

    public CasosestudioAtributos(BigDecimal id) {
        this.id = id;
    }

    public BigInteger getCodigoAtributo() {
        return codigoAtributo;
    }

    public void setCodigoAtributo(BigInteger codigoAtributo) {
        this.codigoAtributo = codigoAtributo;
    }

    public String getValor2() {
        return valor2;
    }

    public void setValor2(String valor2) {
        this.valor2 = valor2;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Atributos getValoresCodigo() {
        return valoresCodigo;
    }

    public void setValoresCodigo(Atributos valoresCodigo) {
        this.valoresCodigo = valoresCodigo;
    }

    public Casosestudio getCasosestudioId() {
        return casosestudioId;
    }

    public void setCasosestudioId(Casosestudio casosestudioId) {
        this.casosestudioId = casosestudioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CasosestudioAtributos)) {
            return false;
        }
        CasosestudioAtributos other = (CasosestudioAtributos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.sigma.manglar.entities.CasosestudioAtributos[ id=" + id + " ]";
    }
    
}
