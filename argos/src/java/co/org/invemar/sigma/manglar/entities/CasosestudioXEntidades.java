/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "CASOSESTUDIO_X_ENTIDADES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CasosestudioXEntidades.findAll", query = "SELECT c FROM CasosestudioXEntidades c"),
    @NamedQuery(name = "CasosestudioXEntidades.findByIdCasoEstudio", query = "SELECT c FROM CasosestudioXEntidades c WHERE c.casosestudioXEntidadesPK.idCasoEstudio = :idCasoEstudio"),
    @NamedQuery(name = "CasosestudioXEntidades.findByNombreEntidad", query = "SELECT c FROM CasosestudioXEntidades c WHERE c.casosestudioXEntidadesPK.nombreEntidad = :nombreEntidad"),
    @NamedQuery(name = "CasosestudioXEntidades.findById", query = "SELECT c FROM CasosestudioXEntidades c WHERE c.casosestudioXEntidadesPK.id = :id")})
public class CasosestudioXEntidades implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CasosestudioXEntidadesPK casosestudioXEntidadesPK;
    @JoinColumn(name = "ID_CASO_ESTUDIO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Casosestudio casosestudio;

    public CasosestudioXEntidades() {
    }

    public CasosestudioXEntidades(CasosestudioXEntidadesPK casosestudioXEntidadesPK) {
        this.casosestudioXEntidadesPK = casosestudioXEntidadesPK;
    }

    public CasosestudioXEntidades(BigInteger idCasoEstudio, String nombreEntidad, BigInteger id) {
        this.casosestudioXEntidadesPK = new CasosestudioXEntidadesPK(idCasoEstudio, nombreEntidad, id);
    }

    public CasosestudioXEntidadesPK getCasosestudioXEntidadesPK() {
        return casosestudioXEntidadesPK;
    }

    public void setCasosestudioXEntidadesPK(CasosestudioXEntidadesPK casosestudioXEntidadesPK) {
        this.casosestudioXEntidadesPK = casosestudioXEntidadesPK;
    }

    public Casosestudio getCasosestudio() {
        return casosestudio;
    }

    public void setCasosestudio(Casosestudio casosestudio) {
        this.casosestudio = casosestudio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (casosestudioXEntidadesPK != null ? casosestudioXEntidadesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CasosestudioXEntidades)) {
            return false;
        }
        CasosestudioXEntidades other = (CasosestudioXEntidades) object;
        if ((this.casosestudioXEntidadesPK == null && other.casosestudioXEntidadesPK != null) || (this.casosestudioXEntidadesPK != null && !this.casosestudioXEntidadesPK.equals(other.casosestudioXEntidadesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.sigma.manglar.entities.CasosestudioXEntidades[ casosestudioXEntidadesPK=" + casosestudioXEntidadesPK + " ]";
    }
    
}
