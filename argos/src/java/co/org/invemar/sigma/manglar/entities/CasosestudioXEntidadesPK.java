/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class CasosestudioXEntidadesPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ID_CASO_ESTUDIO")
    private BigInteger idCasoEstudio;
    @Basic(optional = false)
    @Column(name = "NOMBRE_ENTIDAD")
    private String nombreEntidad;
    @Basic(optional = false)
    @Column(name = "ID")
    private BigInteger id;

    public CasosestudioXEntidadesPK() {
    }

    public CasosestudioXEntidadesPK(BigInteger idCasoEstudio, String nombreEntidad, BigInteger id) {
        this.idCasoEstudio = idCasoEstudio;
        this.nombreEntidad = nombreEntidad;
        this.id = id;
    }

    public BigInteger getIdCasoEstudio() {
        return idCasoEstudio;
    }

    public void setIdCasoEstudio(BigInteger idCasoEstudio) {
        this.idCasoEstudio = idCasoEstudio;
    }

    public String getNombreEntidad() {
        return nombreEntidad;
    }

    public void setNombreEntidad(String nombreEntidad) {
        this.nombreEntidad = nombreEntidad;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCasoEstudio != null ? idCasoEstudio.hashCode() : 0);
        hash += (nombreEntidad != null ? nombreEntidad.hashCode() : 0);
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CasosestudioXEntidadesPK)) {
            return false;
        }
        CasosestudioXEntidadesPK other = (CasosestudioXEntidadesPK) object;
        if ((this.idCasoEstudio == null && other.idCasoEstudio != null) || (this.idCasoEstudio != null && !this.idCasoEstudio.equals(other.idCasoEstudio))) {
            return false;
        }
        if ((this.nombreEntidad == null && other.nombreEntidad != null) || (this.nombreEntidad != null && !this.nombreEntidad.equals(other.nombreEntidad))) {
            return false;
        }
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.sigma.manglar.entities.CasosestudioXEntidadesPK[ idCasoEstudio=" + idCasoEstudio + ", nombreEntidad=" + nombreEntidad + ", id=" + id + " ]";
    }
    
}
