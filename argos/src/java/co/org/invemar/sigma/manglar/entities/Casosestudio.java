/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "CASOSESTUDIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Casosestudio.findAll", query = "SELECT c FROM Casosestudio c"),
    @NamedQuery(name = "Casosestudio.findById", query = "SELECT c FROM Casosestudio c WHERE c.id = :id"),
    @NamedQuery(name = "Casosestudio.findByNombre", query = "SELECT c FROM Casosestudio c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Casosestudio.findByIdCar", query = "SELECT c FROM Casosestudio c WHERE c.idCar = :idCar"),
    @NamedQuery(name = "Casosestudio.findByActivo", query = "SELECT c FROM Casosestudio c WHERE c.activo = :activo"),
    @NamedQuery(name = "Casosestudio.findByCodproyecto", query = "SELECT c FROM Casosestudio c WHERE c.codproyecto = :codproyecto"),
    @NamedQuery(name = "Casosestudio.findByDescripcion", query = "SELECT c FROM Casosestudio c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "Casosestudio.findByAuditoria", query = "SELECT c FROM Casosestudio c WHERE c.auditoria = :auditoria"),
    @NamedQuery(name = "Casosestudio.findByFechacreacion", query = "SELECT c FROM Casosestudio c WHERE c.fechacreacion = :fechacreacion")})
public class Casosestudio implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private BigDecimal id;
    @Column(name = "NOMBRE")
    private String nombre;
    @Lob
    @Column(name = "ADJUNTO")
    private Serializable adjunto;
    @Column(name = "ID_CAR")
    private String idCar;
    @Column(name = "ACTIVO")
    private String activo;
    @Column(name = "CODPROYECTO")
    private BigInteger codproyecto;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "AUDITORIA")
    private String auditoria;
    @Column(name = "FECHACREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacreacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "casosestudioId")
    private List<CasosestudioAtributos> casosestudioAtributosList;
    @OneToMany(mappedBy = "id1")
    private List<Casosestudio> casosestudioList;
    @JoinColumn(name = "ID1", referencedColumnName = "ID")
    @ManyToOne
    private Casosestudio id1;
    @JoinColumn(name = "TIPO", referencedColumnName = "ID")
    @ManyToOne
    private Categoria tipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "casosestudio")
    private List<CasosestudioXEntidades> casosestudioXEntidadesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "casosestudio")
    private List<Parcelasxunidadmanejo> parcelasxunidadmanejoList;

    public Casosestudio() {
    }

    public Casosestudio(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Serializable getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(Serializable adjunto) {
        this.adjunto = adjunto;
    }

    public String getIdCar() {
        return idCar;
    }

    public void setIdCar(String idCar) {
        this.idCar = idCar;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public BigInteger getCodproyecto() {
        return codproyecto;
    }

    public void setCodproyecto(BigInteger codproyecto) {
        this.codproyecto = codproyecto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAuditoria() {
        return auditoria;
    }

    public void setAuditoria(String auditoria) {
        this.auditoria = auditoria;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    @XmlTransient
    public List<CasosestudioAtributos> getCasosestudioAtributosList() {
        return casosestudioAtributosList;
    }

    public void setCasosestudioAtributosList(List<CasosestudioAtributos> casosestudioAtributosList) {
        this.casosestudioAtributosList = casosestudioAtributosList;
    }

    @XmlTransient
    public List<Casosestudio> getCasosestudioList() {
        return casosestudioList;
    }

    public void setCasosestudioList(List<Casosestudio> casosestudioList) {
        this.casosestudioList = casosestudioList;
    }

    public Casosestudio getId1() {
        return id1;
    }

    public void setId1(Casosestudio id1) {
        this.id1 = id1;
    }

    public Categoria getTipo() {
        return tipo;
    }

    public void setTipo(Categoria tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    public List<CasosestudioXEntidades> getCasosestudioXEntidadesList() {
        return casosestudioXEntidadesList;
    }

    public void setCasosestudioXEntidadesList(List<CasosestudioXEntidades> casosestudioXEntidadesList) {
        this.casosestudioXEntidadesList = casosestudioXEntidadesList;
    }

    @XmlTransient
    public List<Parcelasxunidadmanejo> getParcelasxunidadmanejoList() {
        return parcelasxunidadmanejoList;
    }

    public void setParcelasxunidadmanejoList(List<Parcelasxunidadmanejo> parcelasxunidadmanejoList) {
        this.parcelasxunidadmanejoList = parcelasxunidadmanejoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Casosestudio)) {
            return false;
        }
        Casosestudio other = (Casosestudio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.sigma.manglar.entities.Casosestudio[ id=" + id + " ]";
    }
    
}
