/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import co.org.invemar.argos.manglares.model.SectorManglaresModel;
import co.org.invemar.util.ResultadoTransaccion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author usrsig15
 */
public class PersistenceArea {

    private EntityManagerFactory emfactory;
    private EntityManager entityManager;
    private CriteriaBuilder criteriaBuilder;
    private CriteriaQuery criteriaQuery;

    /**
     * *
     *
     */
    public PersistenceArea() {
        emfactory = Persistence.createEntityManagerFactory("manglares");

        entityManager = emfactory.createEntityManager();
        criteriaBuilder = entityManager.getCriteriaBuilder();
        criteriaQuery = criteriaBuilder.createQuery();
    }

    /**
     * *
     *
     * @param idArea
     * @return
     */
    public boolean deleteEntityOfArea(int idArea) {
        boolean result = true;
        try {
            entityManager.getTransaction().begin();
            Query query = entityManager.createQuery("delete FROM CasosestudioXEntidades c WHERE c.casosestudioXEntidadesPK.idCasoEstudio = :idCasoEstudio");
            int deletedCount = query.setParameter("idCasoEstudio", idArea).executeUpdate();
            entityManager.getTransaction().commit();

        } catch (Exception e) {
            result = false;
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return result;
    }

    /**
     * *
     *
     * @param idArea
     * @return
     */
    public String[] getEntitiesOfArea(int idArea) {

        Query query = entityManager.createNamedQuery("CasosestudioXEntidades.findByIdCasoEstudio");
        query.setParameter("idCasoEstudio", idArea);
        List<CasosestudioXEntidades> results = query.getResultList();
        String vector[] = new String[results.size()];
        int index = 0;
        for (CasosestudioXEntidades csxe : results) {
            vector[index] = csxe.getCasosestudioXEntidadesPK().getNombreEntidad();
            index++;
        }

        return vector;
    }

    /**
     * *
     *
     * @param entityName
     * @return
     */
    public List<CasosestudioXEntidades> getCodAreas(String entityName) {
        Query query = entityManager.createNamedQuery("CasosestudioXEntidades.findByNombreEntidad");
        query.setParameter("nombreEntidad", entityName);
        List<CasosestudioXEntidades> results = query.getResultList();       
        return results;
    }

    /**
     *
     * @param idCaseStudio
     * @param entities
     * @return true or false
     */
    public boolean insertEntitiesOfCaseStudio(Connection con, String idCaseStudio, String[] entities) {
        boolean result = false;
        try {
            ResultadoTransaccion rt = new ResultadoTransaccion();
            if (entities.length > 0) {
                for (int i = 0; i < entities.length; i++) {
                    rt = this.insertEntityInCaseStudio(con, idCaseStudio, entities[i]);
                }
                if (rt.getCodigo() != -1) {
                    result = true;
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        return result;
    }

    /**
     *
     * @param idCaseStudio
     * @param entity
     * @return ResultadoTransaccion
     */
    public ResultadoTransaccion insertEntityInCaseStudio(Connection con, String idCaseStudio, String entity) {

        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();

        try {

            String sql = "insert into CASOSESTUDIO_X_ENTIDADES(id_caso_estudio,nombre_entidad) values (?,?)";
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, idCaseStudio);
            pstmt.setString(2, entity);
            int result = pstmt.executeUpdate();
            resultadoTransaccion.setCodigo(result);

        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            resultadoTransaccion.setCodigoError(ex.getErrorCode());
            resultadoTransaccion.setMensaje(ex.getMessage());

        }
        return resultadoTransaccion;
    }

}
