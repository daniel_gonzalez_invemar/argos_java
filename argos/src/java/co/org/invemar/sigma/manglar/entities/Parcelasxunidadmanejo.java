/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "PARCELASXUNIDADMANEJO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parcelasxunidadmanejo.findAll", query = "SELECT p FROM Parcelasxunidadmanejo p"),
    @NamedQuery(name = "Parcelasxunidadmanejo.findByIdparcela", query = "SELECT p FROM Parcelasxunidadmanejo p WHERE p.parcelasxunidadmanejoPK.idparcela = :idparcela"),
    @NamedQuery(name = "Parcelasxunidadmanejo.findByIdestacion", query = "SELECT p FROM Parcelasxunidadmanejo p WHERE p.parcelasxunidadmanejoPK.idestacion = :idestacion"),
    @NamedQuery(name = "Parcelasxunidadmanejo.findByVinculoactivo", query = "SELECT p FROM Parcelasxunidadmanejo p WHERE p.vinculoactivo = :vinculoactivo")})
public class Parcelasxunidadmanejo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ParcelasxunidadmanejoPK parcelasxunidadmanejoPK;
    @Basic(optional = false)
    @Column(name = "VINCULOACTIVO")
    private String vinculoactivo;
    @JoinColumn(name = "IDESTACION", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Casosestudio casosestudio;

    public Parcelasxunidadmanejo() {
    }

    public Parcelasxunidadmanejo(ParcelasxunidadmanejoPK parcelasxunidadmanejoPK) {
        this.parcelasxunidadmanejoPK = parcelasxunidadmanejoPK;
    }

    public Parcelasxunidadmanejo(ParcelasxunidadmanejoPK parcelasxunidadmanejoPK, String vinculoactivo) {
        this.parcelasxunidadmanejoPK = parcelasxunidadmanejoPK;
        this.vinculoactivo = vinculoactivo;
    }

    public Parcelasxunidadmanejo(BigInteger idparcela, BigInteger idestacion) {
        this.parcelasxunidadmanejoPK = new ParcelasxunidadmanejoPK(idparcela, idestacion);
    }

    public ParcelasxunidadmanejoPK getParcelasxunidadmanejoPK() {
        return parcelasxunidadmanejoPK;
    }

    public void setParcelasxunidadmanejoPK(ParcelasxunidadmanejoPK parcelasxunidadmanejoPK) {
        this.parcelasxunidadmanejoPK = parcelasxunidadmanejoPK;
    }

    public String getVinculoactivo() {
        return vinculoactivo;
    }

    public void setVinculoactivo(String vinculoactivo) {
        this.vinculoactivo = vinculoactivo;
    }

    public Casosestudio getCasosestudio() {
        return casosestudio;
    }

    public void setCasosestudio(Casosestudio casosestudio) {
        this.casosestudio = casosestudio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parcelasxunidadmanejoPK != null ? parcelasxunidadmanejoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parcelasxunidadmanejo)) {
            return false;
        }
        Parcelasxunidadmanejo other = (Parcelasxunidadmanejo) object;
        if ((this.parcelasxunidadmanejoPK == null && other.parcelasxunidadmanejoPK != null) || (this.parcelasxunidadmanejoPK != null && !this.parcelasxunidadmanejoPK.equals(other.parcelasxunidadmanejoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.sigma.manglar.entities.Parcelasxunidadmanejo[ parcelasxunidadmanejoPK=" + parcelasxunidadmanejoPK + " ]";
    }
    
}
