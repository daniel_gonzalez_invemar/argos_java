/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "ATRIBUTOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Atributos.findAll", query = "SELECT a FROM Atributos a"),
    @NamedQuery(name = "Atributos.findByCodigo", query = "SELECT a FROM Atributos a WHERE a.codigo = :codigo"),
    @NamedQuery(name = "Atributos.findByNombre", query = "SELECT a FROM Atributos a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Atributos.findByOrden", query = "SELECT a FROM Atributos a WHERE a.orden = :orden"),
    @NamedQuery(name = "Atributos.findByActivo", query = "SELECT a FROM Atributos a WHERE a.activo = :activo")})
public class Atributos implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private BigDecimal codigo;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "ORDEN")
    private BigInteger orden;
    @Column(name = "ACTIVO")
    private Character activo;
    @OneToMany(mappedBy = "valoresCodigo")
    private List<CasosestudioAtributos> casosestudioAtributosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "atributosCodigo")
    private List<Valores> valoresList;
    @JoinColumn(name = "CATEGORIA_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Categoria categoriaId;

    public Atributos() {
    }

    public Atributos(BigDecimal codigo) {
        this.codigo = codigo;
    }

    public Atributos(BigDecimal codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public BigDecimal getCodigo() {
        return codigo;
    }

    public void setCodigo(BigDecimal codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigInteger getOrden() {
        return orden;
    }

    public void setOrden(BigInteger orden) {
        this.orden = orden;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    @XmlTransient
    public List<CasosestudioAtributos> getCasosestudioAtributosList() {
        return casosestudioAtributosList;
    }

    public void setCasosestudioAtributosList(List<CasosestudioAtributos> casosestudioAtributosList) {
        this.casosestudioAtributosList = casosestudioAtributosList;
    }

    @XmlTransient
    public List<Valores> getValoresList() {
        return valoresList;
    }

    public void setValoresList(List<Valores> valoresList) {
        this.valoresList = valoresList;
    }

    public Categoria getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(Categoria categoriaId) {
        this.categoriaId = categoriaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Atributos)) {
            return false;
        }
        Atributos other = (Atributos) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.sigma.manglar.entities.Atributos[ codigo=" + codigo + " ]";
    }
    
}
