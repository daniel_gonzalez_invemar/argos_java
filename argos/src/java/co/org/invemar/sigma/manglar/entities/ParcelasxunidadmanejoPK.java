/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class ParcelasxunidadmanejoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "IDPARCELA")
    private BigInteger idparcela;
    @Basic(optional = false)
    @Column(name = "IDESTACION")
    private BigInteger idestacion;

    public ParcelasxunidadmanejoPK() {
    }

    public ParcelasxunidadmanejoPK(BigInteger idparcela, BigInteger idestacion) {
        this.idparcela = idparcela;
        this.idestacion = idestacion;
    }

    public BigInteger getIdparcela() {
        return idparcela;
    }

    public void setIdparcela(BigInteger idparcela) {
        this.idparcela = idparcela;
    }

    public BigInteger getIdestacion() {
        return idestacion;
    }

    public void setIdestacion(BigInteger idestacion) {
        this.idestacion = idestacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idparcela != null ? idparcela.hashCode() : 0);
        hash += (idestacion != null ? idestacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParcelasxunidadmanejoPK)) {
            return false;
        }
        ParcelasxunidadmanejoPK other = (ParcelasxunidadmanejoPK) object;
        if ((this.idparcela == null && other.idparcela != null) || (this.idparcela != null && !this.idparcela.equals(other.idparcela))) {
            return false;
        }
        if ((this.idestacion == null && other.idestacion != null) || (this.idestacion != null && !this.idestacion.equals(other.idestacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.sigma.manglar.entities.ParcelasxunidadmanejoPK[ idparcela=" + idparcela + ", idestacion=" + idestacion + " ]";
    }
    
}
