/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "VALORES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Valores.findAll", query = "SELECT v FROM Valores v"),
    @NamedQuery(name = "Valores.findById", query = "SELECT v FROM Valores v WHERE v.id = :id"),
    @NamedQuery(name = "Valores.findByValor", query = "SELECT v FROM Valores v WHERE v.valor = :valor")})
public class Valores implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private BigDecimal id;
    @Column(name = "VALOR")
    private String valor;
    @Lob
    @Column(name = "ADJUNTO")
    private Serializable adjunto;
    @JoinColumn(name = "ATRIBUTOS_CODIGO", referencedColumnName = "CODIGO")
    @ManyToOne(optional = false)
    private Atributos atributosCodigo;

    public Valores() {
    }

    public Valores(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Serializable getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(Serializable adjunto) {
        this.adjunto = adjunto;
    }

    public Atributos getAtributosCodigo() {
        return atributosCodigo;
    }

    public void setAtributosCodigo(Atributos atributosCodigo) {
        this.atributosCodigo = atributosCodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Valores)) {
            return false;
        }
        Valores other = (Valores) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.sigma.manglar.entities.Valores[ id=" + id + " ]";
    }
    
}
