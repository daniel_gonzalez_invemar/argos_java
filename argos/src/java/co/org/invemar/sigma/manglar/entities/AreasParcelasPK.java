/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.sigma.manglar.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class AreasParcelasPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "CODPARCELA")
    private BigInteger codparcela;
    @Basic(optional = false)
    @Column(name = "ANIO")
    private BigInteger anio;
    @Basic(optional = false)
    @Column(name = "NUMERO_SUBPARCELAS")
    private BigInteger numeroSubparcelas;

    public AreasParcelasPK() {
    }

    public AreasParcelasPK(BigInteger codparcela, BigInteger anio, BigInteger numeroSubparcelas) {
        this.codparcela = codparcela;
        this.anio = anio;
        this.numeroSubparcelas = numeroSubparcelas;
    }

    public BigInteger getCodparcela() {
        return codparcela;
    }

    public void setCodparcela(BigInteger codparcela) {
        this.codparcela = codparcela;
    }

    public BigInteger getAnio() {
        return anio;
    }

    public void setAnio(BigInteger anio) {
        this.anio = anio;
    }

    public BigInteger getNumeroSubparcelas() {
        return numeroSubparcelas;
    }

    public void setNumeroSubparcelas(BigInteger numeroSubparcelas) {
        this.numeroSubparcelas = numeroSubparcelas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codparcela != null ? codparcela.hashCode() : 0);
        hash += (anio != null ? anio.hashCode() : 0);
        hash += (numeroSubparcelas != null ? numeroSubparcelas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AreasParcelasPK)) {
            return false;
        }
        AreasParcelasPK other = (AreasParcelasPK) object;
        if ((this.codparcela == null && other.codparcela != null) || (this.codparcela != null && !this.codparcela.equals(other.codparcela))) {
            return false;
        }
        if ((this.anio == null && other.anio != null) || (this.anio != null && !this.anio.equals(other.anio))) {
            return false;
        }
        if ((this.numeroSubparcelas == null && other.numeroSubparcelas != null) || (this.numeroSubparcelas != null && !this.numeroSubparcelas.equals(other.numeroSubparcelas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.sigma.manglar.entities.AreasParcelasPK[ codparcela=" + codparcela + ", anio=" + anio + ", numeroSubparcelas=" + numeroSubparcelas + " ]";
    }
    
}
