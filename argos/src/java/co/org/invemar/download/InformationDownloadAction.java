package co.org.invemar.download;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.jdom.Document;
import org.jdom.Element;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.org.invemar.log.LogCreator;
import co.org.invemar.user.UserPermission;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.UtilFunctions;
import co.org.invemar.workbook.Entity;
import co.org.invemar.workbook.EntityManager;
import co.org.invemar.workbook.Property;

/**
 * Clase que contiene la logica del negocio para el modulo que permite la
 * descarga de informacion de la base de datos en libros de Excel.
 *
 * @author Victor Adri�n Santaf� Torres.
 *
 *
 */
public class InformationDownloadAction {

    private static final int DOWNLOAD_BY_PROJECT_ALL_ENTITIES = 1;
    private static final int DOWNLOAD_BY_PROJECT_MY_ENTITY = 2;
    private static final int DOWNLOAD_BY_PROJECT_MY_USER_DATA = 3;

    private ServletContext servletContext;

    private String username = null;
    private String schema = null;
    private String userPath = null;
    private String projectId = null;
    private String entityId = null;
    private String userId = null;
    private String thematicId = null;

    private LogCreator logCreator = null;
    private UtilFunctions util = new UtilFunctions();

    //workbook to return
    private HSSFWorkbook workBookResult = null;
    private String xmlPath = null;
    private Document downloadDocument = null;
    private EntityManager downloadEM = null;
    private int userDownloadPermission = -1;
    private int dType = -1;

    //DB connection variables
    private ConnectionFactory cFactory;
    private Connection conn = null;

    /**
     * Recibe par�metros desde el servlet InformationDownload y a partir de la
     * accion solicitada realiza los procedimientos correspondientes.
     *
     * @param request
     * @param response
     * @param servletContext
     * @throws ServletException
     * @throws IOException
     */
    public InformationDownloadAction(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
            throws ServletException, IOException {

        response.setContentType("text/text; charset=ISO-8859-1");

        this.servletContext = servletContext;

        HttpSession session = request.getSession();
        String action = null;
        JSONObject objResult = null;
        JSONArray arrayResult = null;
        boolean result = true;

        try {
            username = session.getAttribute("username").toString();

        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }

        if (username == null) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } else {
            //check user permission
            UserPermission userp = (UserPermission) session.getAttribute("userpermission");

            if (userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_TODAS_LAS_ENTIDADES_PARTICIPANTES)) {
                userDownloadPermission = DOWNLOAD_BY_PROJECT_ALL_ENTITIES;
            } else if (userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {
                userDownloadPermission = DOWNLOAD_BY_PROJECT_MY_ENTITY;
            } else if (userp.couldUser(UserPermission.VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL)) {
                userDownloadPermission = DOWNLOAD_BY_PROJECT_MY_USER_DATA;
            } else {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                return;
            }

        }

        projectId = session.getAttribute("projectid").toString();
        entityId = session.getAttribute("entityid").toString();
        userId = session.getAttribute("userid").toString();

        //get user file path
        userPath = servletContext.getRealPath("useruploads") + File.separator + username;

        action = request.getParameter("action");

        if (action.equals("getThematics")) {

            response.addHeader("Expires", "0");
            response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.addHeader("Pragma", "no-cache");
            response.setContentType("application/json");

            try {

                arrayResult = getThematicsByProject(Integer.parseInt(projectId));

            } catch (Exception e) {
                e.printStackTrace();
            }

            response.getWriter().write(arrayResult.toString());

        }
        if (action.equals("getWorkbook")) {

            schema = request.getParameter("schema");

            thematicId = request.getParameter("thematicid");

            dType = Integer.parseInt(request.getParameter("dtype"));

            if (dType < this.userDownloadPermission) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                return;
            }

            this.workBookResult = createWorkbook();

            if (workBookResult != null) {

                //now log this insert action
                try {
                    logCreator = new LogCreator();
                    logCreator.createLog(logCreator.DESCARGA_DE_DATOS, Integer.parseInt(this.userId), Integer.parseInt(this.projectId), Integer.parseInt(this.thematicId), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                response.setContentType("application/vnd.ms-excel");
                response.setHeader("Content-Disposition", "attachment; filename=\"muestras.xls\"");
                response.setHeader("Pragma", "no-cache");

                ServletOutputStream outs = response.getOutputStream();
                workBookResult.write(outs);
                outs.flush();
                outs.close();

            } else {

                response.setStatus(HttpServletResponse.SC_NO_CONTENT);

            }

        }

    }

    /**
     * Obtiene las tematicas relacionadas con un proyecto especifico
     *
     * @param projectId Id del proyecto
     * @return Arreglo JSON que contiene objetos compuestos por el nombre de la
     * tematica y su codigo anidado al esquema al cual pertenece
     * @throws SQLException
     */
    public JSONArray getThematicsByProject(int projectId) throws SQLException {

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = null;

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection();
            pstmt = conn.prepareStatement("SELECT BUBICACION_TEMATICA.CODIGO, BUBICACION_TEMATICA.DESCRIPCION, LOWER(BUBICACION_TEMATICA.ESQUEMA) FROM (BUBICACION_TEMATICA INNER JOIN BPROYECTO_X_COMP ON BPROYECTO_X_COMP.ID_COMPONENTE=BUBICACION_TEMATICA.CODIGO) WHERE BPROYECTO_X_COMP.ID_PROYECTO=?");
            pstmt.setInt(1, projectId);

            rs = pstmt.executeQuery();

            while (rs.next()) {

                jsonObject = new JSONObject();
                jsonObject.put("name", rs.getString(2));
                jsonObject.put("folder", rs.getString(1) + ";" + rs.getString(3));

                jsonArray.put(jsonObject);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        return jsonArray;
    }

    /**
     * Realiza validaciones necesarias para la posterior creacion de un libro
     * excel
     *
     * @return La estructura del nuevo libro excel con los datos solicitados
     * (HSSFWorkbook)
     */
    private HSSFWorkbook createWorkbook() {

        if (loadDownloadFiles()) {

            if (createDownloadObjects()) {

                try {
                    processWorkbook();
                } catch (Exception e) {
                    System.out.println("I could not create the worbook : createWorkbook() : " + e);
                    workBookResult = null;
                }

            } else {
                workBookResult = null;
            }
        } else {
            workBookResult = null;
        }

        return workBookResult;
    }

    /**
     *
     * Obtiene la ruta real en la cu�l se encuentra el xml que describe la
     * estructura de la tabla/vista desde la cual se realiza la descarga de
     * datos y posteriormente lee dicho archivo y crea un objeto con la
     * estructura de lo leido.
     *
     * @return true si la lectura del archivo xml fue satisfactoria, false en
     * caso contrario
     */
    private boolean loadDownloadFiles() {

        boolean result = true;

        String schemaPath = servletContext.getRealPath("WEB-INF") + File.separator + "xmlconfig" + File.separator + this.schema + File.separator;

        xmlPath = schemaPath + "userdbdownloadtable.xml";

        try {
            this.downloadDocument = util.readXML(xmlPath);

        } catch (Exception e) {
            result = false;
            System.out.println("Exception: I couldn't find some books ; " + e);
        }

        return result;
    }

    /**
     * Toma el objeto, producto de la lectura del archivo xml, generado en el
     * metodo loadDownloadFiles() y a partir del mismo crea un objeto
     * EntityManager
     *
     * @see #loadDownloadFiles()
     * @return true si la creacion del objeto EntityManager fue exitosa, false
     * en el caso contrario
     */
    private boolean createDownloadObjects() {

        boolean result = true;

        try {

            downloadEM = new EntityManager(this.downloadDocument);

            /*Entity test = inforefman.getEntityById(18);
			
             System.out.println("id:"+test.getId()+" name:"+test.getName()+" description:"+test.getDescription());*/
        } catch (Exception e) {
            result = false;
            System.out.println("Exception: I couldn't create some books ; " + e);
            e.printStackTrace();
        }

        return result;

    }

    /**
     *
     * @throws SQLException
     */
    private void processWorkbook() throws SQLException {

        //create workbook
        this.workBookResult = new HSSFWorkbook();

        //create temp sheet
        HSSFSheet wbSheet;

        //create new sheet
        wbSheet = this.workBookResult.createSheet("Muestras");

        createSheet(wbSheet);

    }

    /*holds the style options for  sheet header*/
    private HSSFCellStyle getTitleHeaderStyle() {

        HSSFCellStyle style = this.workBookResult.createCellStyle();

        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBottomBorderColor(HSSFColor.BLACK.index);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setLeftBorderColor(HSSFColor.BLACK.index);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setRightBorderColor(HSSFColor.BLACK.index);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setTopBorderColor(HSSFColor.BLACK.index);

        HSSFFont font = this.workBookResult.createFont();
        font.setFontHeightInPoints((short) 8);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        style.setFont(font);

        return style;

    }

    /*Creates a sheet for inforef*/
    private void createSheet(HSSFSheet wbSheet) throws SQLException {

        List<Property> properties = null;
        String sqlSentence = null;

        Entity entity = null;
        HSSFCell cell = null;
        HSSFCellStyle style = this.workBookResult.createCellStyle();

        //connection variables
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        int columnsCount = 0;
        int rowsCount = 1;

        //create row for titles
        HSSFRow titleRow = wbSheet.createRow(0);
        HSSFRow infoRow = null;

        //GET ENTITY BY ID from download entity manager
        entity = this.downloadEM.getEntityById(1);

        properties = entity.getAttributes();//now we have the entity's properties

        //now put titles
        for (Property property : properties) {

            //create title cell
            cell = titleRow.createCell((short) columnsCount++);

            //enter title cell value
            cell.setCellValue(new HSSFRichTextString(property.getAlias().toUpperCase()));

            //get and set title cell Style
            cell.setCellStyle(getTitleHeaderStyle());

        }

			// fill rows with information
        try {

            //get dbconnection
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection(schema);
            //prepare SQL sentence
            sqlSentence = entity.getSql();

            if (this.dType == this.DOWNLOAD_BY_PROJECT_ALL_ENTITIES) {
                sqlSentence = sqlSentence + " WHERE " + entity.getProjectIdColumn() + "=? AND " + entity.getThematicIdColumn() + "=?";
                pstmt = conn.prepareStatement(sqlSentence);
                pstmt.setString(1, this.projectId);
                pstmt.setString(2, this.thematicId);
            } else if (this.dType == this.DOWNLOAD_BY_PROJECT_MY_ENTITY) {
                sqlSentence = sqlSentence + " INNER JOIN USUARIOS_SCI.USUARIOS_DETALLES ON " + entity.getName() + ".UPLOADFLAG=USUARIOS_SCI.USUARIOS_DETALLES.ID_USUARIO WHERE USUARIOS_SCI.USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_SCI.USUARIOS_DETALLES.VALOR_ASIGNADO=? AND " + entity.getName() + "." + entity.getProjectIdColumn() + "=? AND " + entity.getName() + "." + entity.getThematicIdColumn() + "=?";
                pstmt = conn.prepareStatement(sqlSentence);
                pstmt.setString(1, this.entityId);
                pstmt.setString(2, this.projectId);
                pstmt.setString(3, this.thematicId);
            } else if (this.dType == this.DOWNLOAD_BY_PROJECT_MY_USER_DATA) {
                sqlSentence = sqlSentence + " WHERE " + entity.getProjectIdColumn() + "=? AND UPLOADFLAG=? AND " + entity.getThematicIdColumn() + "=?";
                pstmt = conn.prepareStatement(sqlSentence);
                pstmt.setString(1, this.projectId);
                pstmt.setString(2, this.userId);
                pstmt.setString(3, this.thematicId);
            }

            rs = pstmt.executeQuery();

            while (rs.next()) {//keep in mind each result set is a new row

                columnsCount = 0;

                //creates new row at index 2 (third row)
                infoRow = wbSheet.createRow(rowsCount++);

						//for each property (mmm titles values :) ) we create new cell and put value
                //we are moving left to right
                for (Property property : properties) {

                    //creates cell
                    cell = infoRow.createCell((short) columnsCount++);

                    //enter cell value
                    if (property.getType().equalsIgnoreCase("int")
                            || property.getType().equalsIgnoreCase("double")) {
                        cell.setCellValue(rs.getInt(property.getAlias()));

                    } else if (property.getType().equalsIgnoreCase("String")) {
                        cell.setCellValue(new HSSFRichTextString(rs
                                .getString(property.getAlias())));

                    }
                    if (property.getType().equalsIgnoreCase("Date")) {
                        style.setDataFormat(HSSFDataFormat
                                .getBuiltinFormat("m/d/yy"));
                        cell.setCellValue(rs.getDate(property.getAlias()));

                    }

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("I failed when creating download document sheet : createInfoRefSheet(...) : " + e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }
    }

}
