package co.org.invemar.download;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletContext;

import org.jdom.Document;
import org.jdom.Element;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

import co.org.invemar.user.UserPermission;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.UtilFunctions;
import co.org.invemar.workbook.Area;
import co.org.invemar.workbook.Entity;
import co.org.invemar.workbook.EntityManager;
import co.org.invemar.workbook.Property;
import co.org.invemar.workbook.Sheet;
import co.org.invemar.workbook.WorkbookManager;

/**
 * 
 * Class which receives and redirect the http requests from client
 *  
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class InformationDownload extends HttpServlet {

	private static final int DOWNLOAD_BY_PROJECT_ALL_ENTITIES = 1;
	private static final int DOWNLOAD_BY_PROJECT_MY_ENTITY = 2;
	private static final int DOWNLOAD_BY_PROJECT_MY_USER_DATA = 3;
	
	private ServletContext servletContext; 
	
    public void init(ServletConfig config) throws ServletException {
        servletContext=config.getServletContext();
        super.init(config);
    }
    
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request,response);
		
	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		synchronized(this){
			new InformationDownloadAction(request, response, this.servletContext);
		}
		
	}
	

	
}
