package co.org.invemar.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.SecurityComponent;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UserDetailInfoRequestController {

	//DB connection variables
	private ConnectionFactory cFactory;
	private Connection conn=null;
	private String schema = null;
	
	public UserDetailInfoRequestController(String schema){
		this.schema = schema;
	}
	
/////USER DETAILS - START	

	/*checks if current user can edit information of other user*/
	public String checkUserEditionPermission(int userId, int projectId, String entityId) throws SQLException {
		
		String result = "OK";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT USUARIOS.ID FROM ((((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN CLST_PROYECTOS ON CLST_PROYECTOS.CODIGO=UROLES.ID_PROYECTO) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) WHERE USUARIOS.ID=? AND CLST_PROYECTOS.CODIGO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_DETALLES.VALOR_ASIGNADO=?");
			pstmt.setInt(1, userId);
			pstmt.setInt(2, projectId);
			pstmt.setString(3, entityId);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				result = "OK";			
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return result;
	}
	
	/*returns detail titles but just those who have not been registered for a specific user*/
	public JSONArray getUserDetailTitles(int userId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT UCODIGOS_LOV.CODIGO, UCODIGOS_LOV.DESCRIPCION FROM UCODIGOS_LOV WHERE NOT EXISTS (SELECT USUARIOS_DETALLES.CODIGO_ATRIBUTO FROM USUARIOS_DETALLES WHERE USUARIOS_DETALLES.ID_USUARIO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=UCODIGOS_LOV.CODIGO) AND UCODIGOS_LOV.GRUPO=3");
			pstmt.setInt(1, userId);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("name", rs.getString(2));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}	
	
	/*returns detail titles but just those who have not been registered for a specific user*/
	public JSONArray getUserDetails(int userId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT USUARIOS_DETALLES.CODIGO_ATRIBUTO, UCODIGOS_LOV.DESCRIPCION, USUARIOS_DETALLES.VALOR_ASIGNADO FROM (USUARIOS_DETALLES INNER JOIN UCODIGOS_LOV ON USUARIOS_DETALLES.CODIGO_ATRIBUTO=UCODIGOS_LOV.CODIGO) WHERE USUARIOS_DETALLES.ID_USUARIO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO!=19 AND UCODIGOS_LOV.GRUPO=3");
			pstmt.setInt(1, userId);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("name", rs.getString(2));
				jsonObject.put("text", rs.getString(3));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}
	
	/*saves user detail*/
	public String saveUserDetail(int userId, int detailId, String detailText) throws SQLException {

		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("INSERT INTO USUARIOS_DETALLES (id_usuario, codigo_atributo, valor_asignado) values(?,?,?)");
			pstmt.setInt(1, userId);
			pstmt.setInt(2, detailId);
			pstmt.setString(3, detailText);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}	

	/*updates user detail*/
	public String updateUserDetail(int userId, int detailId, String detailText) throws SQLException {

		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("UPDATE USUARIOS_DETALLES set valor_asignado=? WHERE id_usuario=? AND codigo_atributo=?");
			pstmt.setString(1, detailText);
			pstmt.setInt(2, userId);
			pstmt.setInt(3, detailId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}	

	/*delete user detail*/
	public String deleteUserDetail(int userId, int detailId) throws SQLException {

		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("DELETE FROM USUARIOS_DETALLES WHERE id_usuario=? AND codigo_atributo=?");
			pstmt.setInt(1, userId);
			pstmt.setInt(2, detailId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}	

				
/////USER DETAILS - END	
	
}
