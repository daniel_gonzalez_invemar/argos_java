package co.org.invemar.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.RequestParameterManager;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class RoleAdminAction {

	private RoleAdminInfoRequestController infoRequest = new RoleAdminInfoRequestController(null);
		
	private String username = null;
	private UserPermission userp = null;
	
	private HttpSession session = null;
	
	public RoleAdminAction(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
	    
		response.setContentType("text/text; charset=ISO-8859-1");
		response.addHeader("Expires", "0");
	    response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
	    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
	    response.addHeader("Pragma", "no-cache");
	    
		session=request.getSession();
		String action = null;
		JSONArray arrayResult = null;
		JSONObject objectResult = null;
		
		username = null;
		
    	try{
        	username = session.getAttribute("username").toString();
        	userp = (UserPermission)session.getAttribute("userpermission");
    	}catch(Exception e){
    		username = null;
       	}
    	
    	if(username==null){
    		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    		return;
    	}else{
			//check user permission
			UserPermission userp = (UserPermission)session.getAttribute("userpermission");
			
			if(!userp.couldUser(UserPermission.ADMINISTRAR_ROLES)){
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	    		return;
			}
    	}
	    
    	action = request.getParameter("action");
    	
    	if(action.equals("getProjects")){
			
			response.setContentType("application/json");
			
			try{
				response.getWriter().write(infoRequest.getProjects().toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("getRolesByProject")){
			
			response.setContentType("application/json");
			
			int projectId = Integer.parseInt(request.getParameter("projectid").toString());
						
			try{
				response.getWriter().write(infoRequest.getRolesByProject(projectId).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("getPermissions")){
			
			response.setContentType("application/json");
									
			try{
				response.getWriter().write(infoRequest.getPermissions().toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("createNewRole")){
			
			response.setContentType("application/json");
			
			int projectId = Integer.parseInt(request.getParameter("projectid").toString());
			String rolName = request.getParameter("rolname");
			RequestParameterManager rpManager= new RequestParameterManager(request);
			
			try{
				response.getWriter().write(infoRequest.createRole(projectId, rolName, rpManager).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("getDeniedPermissionsByRole")){
			
			response.setContentType("application/json");
			
			int roleId = Integer.parseInt(request.getParameter("roleid").toString());
			
			try{
				response.getWriter().write(infoRequest.getDeniedPermissionsByRole(roleId).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("getAllowedPermissionsByRole")){
			
			response.setContentType("application/json");
			
			int roleId = Integer.parseInt(request.getParameter("roleid").toString());
			
			try{
				response.getWriter().write(infoRequest.getAllowedPermissionsByRole(roleId).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("addRolePermission")){
			
			response.setContentType("application/json");
			
			int roleId = Integer.parseInt(request.getParameter("roleid").toString());
			int permissionId = Integer.parseInt(request.getParameter("permissionid").toString());
			
			try{
				response.getWriter().write(infoRequest.addRolePermission(roleId,permissionId).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("removeRolePermission")){
			
			response.setContentType("application/json");
			
			int roleId = Integer.parseInt(request.getParameter("roleid").toString());
			int permissionId = Integer.parseInt(request.getParameter("permissionid").toString());
			
			try{
				response.getWriter().write(infoRequest.removeRolePermission(roleId,permissionId).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("updateRoleDescription")){
			
			response.setContentType("application/json");
			
			int roleId = Integer.parseInt(request.getParameter("roleid").toString());
			String roleDescription = request.getParameter("description").toString();
			
			try{
				response.getWriter().write(infoRequest.updateRoleDescription(roleId, roleDescription).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("deleteRole")){
			
			response.setContentType("application/json");
			
			int roleId = Integer.parseInt(request.getParameter("roleid").toString());
			
			try{
				response.getWriter().write(infoRequest.deleteRole(roleId).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}
	    
	}


}
