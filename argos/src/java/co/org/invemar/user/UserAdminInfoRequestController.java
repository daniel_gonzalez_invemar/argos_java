package co.org.invemar.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.ConnectionFactory;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UserAdminInfoRequestController {

	//DB connection variables
	private ConnectionFactory cFactory;
	private Connection conn=null;
	private String schema = null;
	
	public UserAdminInfoRequestController(String schema){
		this.schema = schema;
	}


	/*returns detail titles but just those who have not been registered for a specific user*/
	public String checkUserEditionPermission(int userId, int projectId, String entityId) throws SQLException {
		
		String result = "OK";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT USUARIOS.ID FROM ((((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN CLST_PROYECTOS ON CLST_PROYECTOS.CODIGO=UROLES.ID_PROYECTO) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) WHERE USUARIOS.ID=? AND CLST_PROYECTOS.CODIGO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_DETALLES.VALOR_ASIGNADO=?");
			pstmt.setInt(1, userId);
			pstmt.setInt(2, projectId);
			pstmt.setString(3, entityId);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				result = "OK";			
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return result;
	}
		
	/*returns detail titles but just those who have not been registered for a specific user*/
	public JSONArray getProjects(int projectId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			if(projectId == -1){
				
				query = "SELECT CLST_PROYECTOS.CODIGO, CLST_PROYECTOS.NOMBRE_ALTERNO FROM CLST_PROYECTOS ORDER BY CLST_PROYECTOS.NOMBRE_ALTERNO ASC";
				
			}else{
				query = "SELECT CLST_PROYECTOS.CODIGO, CLST_PROYECTOS.NOMBRE_ALTERNO FROM CLST_PROYECTOS WHERE CLST_PROYECTOS.CODIGO=? ORDER BY CLST_PROYECTOS.NOMBRE_ALTERNO ASC";
			}
			
			pstmt = conn.prepareStatement(query);
			
			if(projectId != -1){
				pstmt.setInt(1, projectId);	
			}
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("name", rs.getString(2));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}		
	
	/*returns user information but just those that user is allowed to see*/
	public JSONArray getAllowedUserInformation(int projectId, String entityId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT USUARIOS.ID, USUARIOS.USERNAME_EMAIL, USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO, USUARIOS.ACTIVO FROM (((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) WHERE UROLES.ID_PROYECTO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_DETALLES.VALOR_ASIGNADO=? ORDER BY USUARIOS.USERNAME_EMAIL ASC");
			pstmt.setInt(1, projectId);
			pstmt.setString(2, entityId);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("email", rs.getString(2));
				jsonObject.put("name", rs.getString(3));
				jsonObject.put("active", rs.getString(4));
				
				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}	

	/*returns user information but just those that user is allowed to see (search)*/
	public JSONArray searchAllowedUserInformation(int projectId, String entityId, String email, String name) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			if(email.equals("null")){
				query="SELECT USUARIOS.ID, USUARIOS.USERNAME_EMAIL, USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO, USUARIOS.ACTIVO FROM (((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) WHERE UROLES.ID_PROYECTO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_DETALLES.VALOR_ASIGNADO=? AND UPPER(USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO) LIKE UPPER('%"+name+"%') ORDER BY USUARIOS.USERNAME_EMAIL ASC";
			}else if(name.equals("null")){
				query="SELECT USUARIOS.ID, USUARIOS.USERNAME_EMAIL, USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO, USUARIOS.ACTIVO FROM (((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) WHERE UROLES.ID_PROYECTO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_DETALLES.VALOR_ASIGNADO=? AND UPPER(USUARIOS.USERNAME_EMAIL) LIKE UPPER('%"+email+"%') ORDER BY USUARIOS.USERNAME_EMAIL ASC";
			}
			
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, projectId);
			pstmt.setString(2, entityId);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("email", rs.getString(2));
				jsonObject.put("name", rs.getString(3));
				jsonObject.put("active", rs.getString(4));
				
				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}	

	/*returns user information of all users*/
	public JSONArray getUserInformationByProject(int projectId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT USUARIOS.ID, USUARIOS.USERNAME_EMAIL, USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO, USUARIOS.ACTIVO FROM ((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) WHERE UROLES.ID_PROYECTO=? ORDER BY USUARIOS.USERNAME_EMAIL ASC");
			pstmt.setInt(1, projectId);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("email", rs.getString(2));
				jsonObject.put("name", rs.getString(3));
				jsonObject.put("active", rs.getString(4));
				
				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}	

	/*returns user information of all users (search)*/
	public JSONArray searchUserInformation(String email, String name) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			if(email.equals("null")){
				query = "SELECT USUARIOS.ID, USUARIOS.USERNAME_EMAIL, (USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO), USUARIOS.ACTIVO FROM USUARIOS WHERE UPPER(USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO) LIKE UPPER('%"+name+"%') ORDER BY USUARIOS.USERNAME_EMAIL ASC";
			}else if(name.equals("null")){
				query = "SELECT USUARIOS.ID, USUARIOS.USERNAME_EMAIL, USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO, USUARIOS.ACTIVO FROM USUARIOS WHERE UPPER(USUARIOS.USERNAME_EMAIL) LIKE UPPER('%"+email+"%') ORDER BY USUARIOS.USERNAME_EMAIL ASC";
			}
			
			pstmt = conn.prepareStatement(query);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("email", rs.getString(2));
				jsonObject.put("name", rs.getString(3));
				jsonObject.put("active", rs.getString(4));
				
				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}	

	/*saves user detail*/
	public String changeUserStatus(int userId, String newStatus) throws SQLException {

		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("UPDATE USUARIOS SET ACTIVO=? WHERE USUARIOS.ID=?");
			pstmt.setString(1, newStatus);
			pstmt.setInt(2, userId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}
	
}
