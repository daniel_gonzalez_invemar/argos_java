package co.org.invemar.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.RequestParameterManager;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class RoleAdminInfoRequestController {

	//DB connection variables
	private ConnectionFactory cFactory;
	private Connection conn=null;
	private String schema = null;
	
	public RoleAdminInfoRequestController(String schema){
		this.schema = schema;
	}

	/*checks if a role name exists*/
	public boolean checkRoleName(int projectId, String roleName) throws SQLException {
		
		boolean result = true;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			pstmt = conn.prepareStatement("SELECT UROLES.ID FROM UROLES WHERE UPPER(UROLES.DESCRIPCION)=UPPER(TRIM(?)) AND UROLES.ID_PROYECTO=?");
			pstmt.setString(1,roleName);
			pstmt.setInt(2, projectId);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				
				result = true;				
			}else{
				
				result = false;
				
			}

		}catch(Exception e){
			result = true;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return result;
	}		

	
	
	/*returns projects details*/
	public JSONArray getProjects() throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
					
			pstmt = conn.prepareStatement("SELECT CLST_PROYECTOS.CODIGO, CLST_PROYECTOS.NOMBRE_ALTERNO FROM CLST_PROYECTOS ORDER BY CLST_PROYECTOS.NOMBRE_ALTERNO ASC");
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("name", rs.getString(2));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}		

	/*returns permissions details*/
	public JSONArray getPermissions() throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
					
			pstmt = conn.prepareStatement("SELECT CODIGO, DESCRIPCION FROM UCODIGOS_LOV WHERE GRUPO=2 ORDER BY DESCRIPCION ASC");
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("description", rs.getString(2));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}

	/*returns allowed permissions details*/
	public JSONArray getAllowedPermissionsByRole(int roleId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
					
			pstmt = conn.prepareStatement("SELECT UCODIGOS_LOV.CODIGO, UCODIGOS_LOV.DESCRIPCION FROM (UPERMISOS_ROLES INNER JOIN UCODIGOS_LOV ON UPERMISOS_ROLES.ID_PERMISO=UCODIGOS_LOV.CODIGO) WHERE UPERMISOS_ROLES.ID_ROL=? ORDER BY UCODIGOS_LOV.DESCRIPCION ASC");
			pstmt.setInt(1,roleId); 
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("description", rs.getString(2));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}

	/*returns allowed permissions details*/
	public JSONArray getDeniedPermissionsByRole(int roleId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
					
			pstmt = conn.prepareStatement("SELECT UCODIGOS_LOV.CODIGO, UCODIGOS_LOV.DESCRIPCION FROM UCODIGOS_LOV WHERE NOT EXISTS (SELECT UPERMISOS_ROLES.ID_ROL FROM UPERMISOS_ROLES WHERE UPERMISOS_ROLES.ID_PERMISO=UCODIGOS_LOV.CODIGO AND UPERMISOS_ROLES.ID_ROL=?) AND UCODIGOS_LOV.GRUPO=2 ORDER BY UCODIGOS_LOV.DESCRIPCION ASC");
			pstmt.setInt(1,roleId); 
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("description", rs.getString(2));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}
	
	/*returns roles by project*/
	public JSONArray getRolesByProject(int projectId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
					
			pstmt = conn.prepareStatement("SELECT ID, DESCRIPCION FROM UROLES WHERE ID_PROYECTO=? ORDER BY DESCRIPCION ASC");
			pstmt.setInt(1,projectId); 
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("description", rs.getString(2));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}		

	/*creates new role*/
	public String createRole(int projectId, String rolName, RequestParameterManager rpManager) throws SQLException {
		
		String result = "OK";
		int roleId = 0;
		int rows = -1;
		int i = 0;
		int permissionId = 0;
		String temp = null;
		
		
		//check role name
		if(this.checkRoleName(projectId, rolName)){
			
			result = "ENAME";
			return result;
			
		}
		
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("INSERT INTO UROLES (ID_PROYECTO, DESCRIPCION) VALUES (?,?)");
			pstmt.setInt(1, projectId);
			pstmt.setString(2, rolName);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
		}
		
		//get new role id and then register permission
		if(result.equals("OK")){
			
			roleId = getRoleId(conn);
			
			for(i=0 ; i< rpManager.getRequestParameterList().size()-4; i++){
				
				permissionId = Integer.parseInt(rpManager.getRequestParameterValueByName("perm"+i));
				
				temp = addRolePermission(roleId, permissionId);
				
			}
			
			
		}else{
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}
		}
		
		
		return result;
		
	}
	
	/*returns last created role id*/
	public int getRoleId(Connection conn) throws SQLException {
		
		int result = 0;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
					
			pstmt = conn.prepareStatement("SELECT UROLES_SQ.CURRVAL FROM DUAL");
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				
				result = rs.getInt(1);				
			}

		}catch(Exception e){
			result = 0;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return result;
	}			
	
	/*set a permission for a role*/
	public String addRolePermission(int roleId, int permissionId) throws SQLException {

		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("INSERT INTO UPERMISOS_ROLES (ID_ROL, ID_PERMISO) VALUES (?,?)");
			pstmt.setInt(1, roleId);
			pstmt.setInt(2, permissionId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}

	/*remove a permission from a role*/
	public String removeRolePermission(int roleId, int permissionId) throws SQLException {

		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("DELETE FROM UPERMISOS_ROLES WHERE ID_ROL=? AND ID_PERMISO=?");
			pstmt.setInt(1, roleId);
			pstmt.setInt(2, permissionId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}

	/*updates a role description*/
	public String updateRoleDescription(int roleId, String roleDescription) throws SQLException {

		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("UPDATE UROLES SET DESCRIPCION=? WHERE ID=?");
			pstmt.setString(1, roleDescription);
			pstmt.setInt(2, roleId);
			
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}

	/*updates a role description*/
	public String deleteRole(int roleId) throws SQLException {

		deleteRoleForUsers(roleId);
		deleteRolePermissions(roleId);
		
		
		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("DELETE FROM UROLES WHERE ID=?");
			pstmt.setInt(1, roleId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}

	/*updates a role description*/
	public boolean deleteRoleForUsers(int roleId) throws SQLException {

		boolean result = true;
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("DELETE FROM UROLES_USUARIOS WHERE ID_ROL=?");
			pstmt.setInt(1, roleId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = true;
			}else{
				result = false;
			}

		}catch(Exception e){
			result = false;
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}
	
	/*updates a role description*/
	public boolean deleteRolePermissions(int roleId) throws SQLException {

		boolean result = true;
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("DELETE FROM UPERMISOS_ROLES WHERE ID_ROL=?");
			pstmt.setInt(1, roleId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = true;
			}else{
				result = false;
			}

		}catch(Exception e){
			result = false;
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}
		
}
