package co.org.invemar.user;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.Cmail;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.SecurityComponent;

/**
 *
 * @author Victor Adri�n Santaf� Torres.
 *
 *
 */
public class LoginAction {

    //DB connection variables
    private ConnectionFactory cFactory;
    private Connection conn = null;
    private String siteUrl = "http://cinto.invemar.org.co/argos/login.jsp";
    private LoginInfoRequestController infoRequest = new LoginInfoRequestController(null);
    //security component
    private SecurityComponent security = null;
    private String username = null;

    public LoginAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/text; charset=ISO-8859-1");
        response.addHeader("Expires", "0");
        response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.addHeader("Pragma", "no-cache");


        HttpSession session = request.getSession();

        String action = null;

        JSONObject objResult = null;
        JSONArray arrayResult = null;
        boolean result = true;
        String sresult = null;

        action = request.getParameter("action");


        if (action.equalsIgnoreCase("administrar")) {

            response.sendRedirect("login_unep.jsp");
        }

        //verify that user is not logged on
        if (action.equalsIgnoreCase("cerrarSession")) {

            String prj = (String) session.getAttribute("projectid");

            session.removeAttribute("username");
            session.removeAttribute("userid");
            session.removeAttribute("projectid");
            session.removeAttribute("projectname");
            session.removeAttribute("entityid");
            session.invalidate();

            if (prj != null && prj.equals("2027")) {
                response.sendRedirect("login_unep.jsp");
            } else {

                response.sendRedirect("login.jsp");
            }
        }

        try {
            username = session.getAttribute("username").toString();

        } catch (Exception e) {
            username = null;
        }


        if (username != null && !action.equals("setuserproject")) {

            action="ver";

        }

        if (action.equals("ver")) {            
            username = request.getParameter("email");
            String password = request.getParameter("password");
            // System.out.println("In login action:"+username);

            try {
                arrayResult = infoRequest.login(username, password, session);
                if (arrayResult != null) {
                    infoRequest.configureUserPermissions(session);
                }
            } catch (Exception e) {
                arrayResult = null;
            }

            if (arrayResult != null) {
                response.setContentType("application/json");
                response.getWriter().write(arrayResult.toString());


            } else {
                response.setContentType("text/html");
                response.getWriter().write("NO");

            }

        } else if (action.equals("passregen")) {

            response.setContentType("application/json");

            String email = request.getParameter("email");

            response.getWriter().write(passwordRegenConfirmation(email));

        } else if (action.equals("cpassgen")) {

            response.setContentType("application/json");

            String email = request.getParameter("email");
            String ckey = request.getParameter("ckey");          
            

            int userId = Integer.parseInt(request.getParameter("uid").toString());

            if (ckey == null || ckey.equals("") || ckey.equals(" ")) {
                response.getWriter().write("{\"result\":\"NO\",\"desc\":\"La clave de confirmacion suministrada es invalida.\"}");
                return;
            } else {
                response.getWriter().write(changePassword(userId, ckey, email));
            }

        } else if (action.equals("setuserproject")) {

            response.setContentType("text/html");

            try {
                String pid = request.getParameter("pid");
                String pname = request.getParameter("pname");
                String title = request.getParameter("projecttitle");
          
                session.setAttribute("projectid", pid);
                session.setAttribute("projectname", pname);
                session.setAttribute("projecttitle", title);

                String css = infoRequest.getCSSPorProyecto(pid);

                session.setAttribute("CSSPRO", css);

                infoRequest.configureUserPermissions(session);


            } catch (Exception e) {
                response.getWriter().write("NO");
                return;
            }

            response.getWriter().write("OK");
        }

    }

    /*starts password regeneration process*/
    private String passwordRegenConfirmation(String email) {

        String result = null;

        int userId = -1;

        String confirmationKey = null;

        try {

            userId = infoRequest.confirmEmail(email);

            if (userId > -1) {

                this.security = new SecurityComponent();

                //create sha-1 string from email
                confirmationKey = security.getSHAfromString(email + new Date().getTime() + "//-*777");

                //get substring, it is going to be the confirmation key
                confirmationKey = confirmationKey.substring(5, 25);

                //store key on database
                if (infoRequest.storeConfirmationKey(userId, confirmationKey)) {

                    //send a message with confirmation link
                    if (sendConfirmationMessage(userId, email, confirmationKey)) {
                    } else {
                        result = "{\"result\":\"NO\",\"desc\":\"No fue posible enviar el mensaje de confirmacion a su correo, por favor vuelva a intentarlo mas tarde.\"}";
                        //System.out.println("send msg");
                    }

                } else {
                    result = "{\"result\":\"NO\",\"desc\":\"No fue posible generar la clave de confirmacion, por favor vuelva a intentarlo mas tarde.\"}";
                    //System.out.println("store confirmation");
                }

            } else {
                result = "{\"result\":\"NO\",\"desc\":\"El correo electronico ingresado no concuerda con el de nuestra base de datos.\"}";
                //System.out.println("confirma email");
            }

        } catch (Exception e) {
            e.printStackTrace();
            result = "{\"result\":\"NO\",\"desc\":\"Ocurrio un error inesperado, por favor vuelva a intentarlo mas tarde.\"}";
        }

        if (result == null) {
            result = "{\"result\":\"OK\",\"desc\":\"Ha pedido al sistema que genere una nueva password para su ingreso al sistema, le ha sido enviado un correo electronico con un link de confirmacion para continuar el proceso.\"}";
        }

        return result;

    }

    /*sends a message to user with confirmation key*/
    private boolean sendConfirmationMessage(int userId, String email, String confirmationKey) {

        boolean result = true;

        StringBuffer message = new StringBuffer();

        Cmail mailSender = new Cmail();

        //set basic message data
        mailSender.setafrom("administrador_argos@invemar.org.co");
        mailSender.setato(email);
        mailSender.setasubject("INVEMAR/ARGOS - Recuperaci�n de contrase�a");

        //set message content
        message.append("<HTML>");
        message.append("<HEAD>");
        message.append("</HEAD>");
        message.append("<BODY>");
        message.append("<p>ARGOS - Sistema de Soporte Multitem�tico para el Monitoreo Ambiental</p>");
        message.append("<p>Usted ha olvidado su contrase&ntilde;a y ha pedido al sistema que genere una nueva, "
                + "por favor utilice el siguiente link para confirmar dicha acci&oacute;n:</p>");
        message.append("<p><a href='" + siteUrl + "?action=cpassgen&uid=" + userId + "&email=" + email + "&ckey=" + confirmationKey + "'>Haga click aqu&iacute;</a></p>");
        message.append("Si el link proporcionado no funciona, copie el siguiente texto en la barra de navegaci&oacute;n de su navegador de internet: </p>");
        message.append("</p>" + siteUrl + "?action=cpassgen&uid=" + userId + "&email=" + email + "&ckey=" + confirmationKey + "</p>");
        message.append("<p>Si usted no ha solicitado el cambio de contrase&ntilde;a haga caso omiso a &eacute;ste mensaje.</p>");
        message.append("</BODY>");
        message.append("</HTML>");

        mailSender.setamensaje(message.toString());

        result = mailSender.manda();

        return result;

    }

    /*changes password after validate confirmation key...*/
    private String changePassword(int userId, String cKey, String email) {

        String result = null;

        String currentPassword = null;
        String newPassword = null;

        try {

            /*validate confirmation key*/
            currentPassword = infoRequest.validateConfirmationKey(userId, cKey);

            if (currentPassword != null) {

                //get new password from actual password string
                newPassword = currentPassword.substring(7, 15);

                this.security = new SecurityComponent();

                //store new password -> it is going to be shaed (shaed?, jajaja)
                if (infoRequest.storeNewPassword(userId, this.security.getSHAfromString(newPassword))) {

                    //send new password to user 
                    sendNewPasswordMessage(email, newPassword);

                } else {
                    result = "{\"result\":\"NO\",\"desc\":\"No fue posible generar su nueva password, por favor vuelva a intentarlo mas tarde.\"}";
                }

            } else {
                result = "{\"result\":\"NO\",\"desc\":\"La clave de confirmacion suministrada es invalida.\"}";
            }


        } catch (Exception e) {
            e.printStackTrace();
            result = "{\"result\":\"NO\",\"desc\":\"Ocurrion un error inesperado, por favor vuelva a intentarlo mas tarde.\"}";
        }

        if (result == null) {
            result = "{\"result\":\"OK\",\"desc\":\"Su nueva password ha sido generada exitosamente, en los proximos instantes recibir� un correo electr�nico con su informaci�n de usuario.\"}";
        }

        return result;
    }

    /*sends a message to user with new password*/
    private boolean sendNewPasswordMessage(String email, String newPassword) {

        boolean result = true;

        StringBuffer message = new StringBuffer();

        Cmail mailSender = new Cmail();

        //set basic message data
        mailSender.setafrom("administrador_argos@invemar.org.co");
        mailSender.setato(email);
        mailSender.setasubject("INVEMAR/ARGOS - Nueva contrase�a");

        //set message content
        message.append("<HTML>");
        message.append("<HEAD>");
        message.append("</HEAD>");
        message.append("<BODY>");
        message.append("<p>ARGOS - Sistema de Soporte Multitem�tico para el Monitoreo Ambiental</p>");

        message.append("<p>El sistema ha generado una nueva contrase&ntilde;a para su ingreso al sistema, "
                + "una vez haya realizado su ingreso por favor cambiela a una que recuerde con mayor facilidad:</p>");
        message.append("<p>Nueva contrase&ntilde;a:  " + newPassword + "</p>");
        message.append("<p>Si desea ingresar al sistema haga <a href='" + this.siteUrl + "'>click aqu&iacute;</a></p>");
        message.append("</BODY>");
        message.append("</HTML>");

        mailSender.setamensaje(message.toString());

        result = mailSender.manda();

        return result;

    }
}
