package co.org.invemar.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UserRoleAction {

	private UserRoleInfoRequestController infoRequest = new UserRoleInfoRequestController(null);
	
	private String username = null;
	private UserPermission userp = null;
	
	private HttpSession session = null;
	
	public UserRoleAction(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
	    
		response.setContentType("text/text; charset=ISO-8859-1");
		response.addHeader("Expires", "0");
	    response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
	    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
	    response.addHeader("Pragma", "no-cache");
	    
		session=request.getSession();
		String action = null;
		
		username = null;
		
    	try{
        	username = session.getAttribute("username").toString();
        	userp = (UserPermission)session.getAttribute("userpermission");
    	}catch(Exception e){
    		username = null;
       	}
    	
    	if(username==null){
    		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    		return;
    	}else{
			//check user permission
			UserPermission userp = (UserPermission)session.getAttribute("userpermission");
			
			if(!userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)&&!userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	    		return;
			}
    	}
	    
    	action = request.getParameter("action");
    	
    	if(action.equals("getProjects")){
			
			response.setContentType("application/json");
			
			int projectId= Integer.parseInt(session.getAttribute("projectid").toString());
			
			try{
				if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)){
					response.getWriter().write(infoRequest.getProjects(projectId).toString());
				}if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
					response.getWriter().write(infoRequest.getProjects(-1).toString());
				}
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("getProjectRoles")){
			
			response.setContentType("application/json");
			
			int projectId= Integer.parseInt(request.getParameter("projectid").toString());
			
			try{
				response.getWriter().write(infoRequest.getProjectRoles(projectId).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("getUserRoles")){
			
    		response.setContentType("text/html");
			
			int userId= Integer.parseInt(request.getParameter("userid").toString());

			int projectId= Integer.parseInt(session.getAttribute("projectid").toString());
			
			try{
				if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)){
					response.getWriter().write(infoRequest.getUserRoles(userId, projectId).toString());
				}if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
					response.getWriter().write(infoRequest.getUserRoles(userId, -1).toString());
				}
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("saveUserRole")){
			
			response.setContentType("application/json");
			
			int userId= Integer.parseInt(request.getParameter("userid").toString());
			
			int roleId= Integer.parseInt(request.getParameter("roleid").toString());
			
			int projectId= Integer.parseInt(request.getParameter("projectid").toString());
			
			try{
				response.getWriter().write(infoRequest.setUserRole(userId, roleId, projectId).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("removeUserRole")){
			
			response.setContentType("application/json");
			
			int userId= Integer.parseInt(request.getParameter("userid").toString());
			
			int roleId= Integer.parseInt(request.getParameter("roleid").toString());
			
			try{
				response.getWriter().write(infoRequest.removeUserRole(userId, roleId).toString());
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("checkUserEditionPermission")){
			
    		response.setContentType("text/html");
			
			int userId= Integer.parseInt(request.getParameter("userid").toString());
			int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
			String entityId = session.getAttribute("entityid").toString();
						
			try{
				if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)){
					response.getWriter().write(infoRequest.checkUserEditionPermission(userId, projectId, entityId));
				}if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
					response.getWriter().write("OK");	
				}
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}
    	
	}
		
	
	
}
