package co.org.invemar.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpSession;

import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.SecurityComponent;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class LoginInfoRequestController {

    //DB connection variables
    private ConnectionFactory cFactory;
    private Connection conn = null;
    private String schema = null;
    //security component
    private SecurityComponent security = null;

    public LoginInfoRequestController(String schema) {
        this.schema = schema;
    }

/////LOGIN - START

    /*checks username and password against data base*/
    public JSONArray login(String username, String password, HttpSession session) throws SQLException {

        JSONArray jsonArray = null;
        JSONObject jsonObject = null;

        this.security = new SecurityComponent();

        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection();
            pstmt = conn.prepareStatement("SELECT USUARIOS.ID, CLST_PROYECTOS.CODIGO, CLST_PROYECTOS.NOMBRE_ALTERNO, USUARIOS_DETALLES.VALOR_ASIGNADO,(select valor_asignado from usuarios_detalles where CODIGO_ATRIBUTO=809 and id_usuario=USUARIOS.ID) as aporta_datos,CLST_PROYECTOS.TITULO,udirentidades.pais_in  FROM ((((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN CLST_PROYECTOS ON CLST_PROYECTOS.CODIGO=UROLES.ID_PROYECTO) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) inner join udirentidades on udirentidades.codigo_in=USUARIOS_DETALLES.VALOR_ASIGNADO WHERE USUARIOS.USERNAME_EMAIL=? AND USUARIOS.PASSWORD=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS.ACTIVO='S' ORDER BY CLST_PROYECTOS.nombre_alterno desc");
            pstmt.setString(1, username);
            pstmt.setString(2, this.security.getSHAfromString(password));

            ResultSet rs = pstmt.executeQuery();           
            

            
            while (rs.next()) {

                if (rs.isFirst()) {
                    jsonArray = new JSONArray();
                    session.setAttribute("username", username);
                    session.setAttribute("userid", rs.getString(1));
                    session.setAttribute("projectid", rs.getString(2));
                    session.setAttribute("projectname", rs.getString(3));
                    session.setAttribute("entityid", rs.getString(4));                   
                    session.setAttribute("projecttitle", rs.getString("TITULO"));
                    session.setAttribute("pais", rs.getString("pais_in"));
                    session.setAttribute("aportadatos", rs.getString("aporta_datos"));
                    


                }
                
                jsonObject = new JSONObject();
                jsonObject.put("id", rs.getString(2));
                jsonObject.put("name", rs.getString(3));
                jsonObject.put("projecttitle", rs.getString("TITULO"));
                jsonArray.put(jsonObject);

            }
           
        } catch (Exception e) {
            jsonArray = null;
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        return jsonArray;

    }

    /*creates an UserPermission object with user permission configuration and save it to session*/
    public boolean configureUserPermissions(HttpSession session) throws SQLException {

        boolean result = true;

        PreparedStatement pstmt = null;

        UserPermission userp = new UserPermission();

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection();
            pstmt = conn.prepareStatement("SELECT UPERMISOS_ROLES.ID_PERMISO,uroles_usuarios.id_rol FROM ((UROLES_USUARIOS INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN UPERMISOS_ROLES ON UPERMISOS_ROLES.ID_ROL = UROLES.ID) WHERE UROLES_USUARIOS.ID_USUARIO=? AND UROLES.ID_PROYECTO=? ORDER BY UPERMISOS_ROLES.ID_PERMISO ASC");
            pstmt.setInt(1, Integer.parseInt(session.getAttribute("userid").toString()));
            pstmt.setInt(2, Integer.parseInt(session.getAttribute("projectid").toString()));

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                userp.allowUserTo(rs.getInt(1));
                session.setAttribute("idrol", rs.getString("id_rol"));
                //System.out.println("permission "+rs.getInt(1)+" test "+userp.couldUser(rs.getInt(1)-700));


            }

        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        if (result) {
            session.setAttribute("userpermission", userp);
        }

        return result;

    }

    /*confirms the existence of user with a specific email*/
    public int confirmEmail(String email) throws SQLException {

        int result = -1;

        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection();
            pstmt = conn.prepareStatement("SELECT ID FROM USUARIOS WHERE USERNAME_EMAIL=?");
            pstmt.setString(1, email);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                result = rs.getInt(1);
            } else {
                result = -1;
            }

        } catch (Exception e) {
            result = -1;
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        return result;

    }

    /*stores confirmation key on database*/
    public boolean storeConfirmationKey(int userId, String confirmationKey) throws SQLException {

        boolean result = true;

        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection();
            pstmt = conn.prepareStatement("UPDATE USUARIOS set llavepassword=? WHERE id=?");
            pstmt.setString(1, confirmationKey);
            pstmt.setInt(2, userId);

            int rs = pstmt.executeUpdate();

            if (rs > 0) {
                result = true;
            } else {
                result = false;
            }

        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        return result;

    }

    /*checks confirmation key against data base*/
    public String validateConfirmationKey(int userId, String cKey) throws SQLException {

        String result = null;

        this.security = new SecurityComponent();

        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection();
            pstmt = conn.prepareStatement("SELECT PASSWORD FROM USUARIOS WHERE ID=? AND LLAVEPASSWORD=?");
            pstmt.setInt(1, userId);
            pstmt.setString(2, cKey);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                result = rs.getString(1);
            } else {
                result = null;
            }

        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        return result;

    }

    /*stores confirmation key on database*/
    public boolean storeNewPassword(int userId, String password) throws SQLException {

        boolean result = true;

        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection();
            pstmt = conn.prepareStatement("UPDATE USUARIOS set password=?, llavepassword='' WHERE id=?");
            pstmt.setString(1, password);
            pstmt.setInt(2, userId);

            int rs = pstmt.executeUpdate();

            if (rs > 0) {
                result = true;
            } else {
                result = false;
            }

        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        return result;

    }

/////LOGIN - END
/////USER ADMINISTRATION - START	
    public JSONArray getEntities() throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection();
            pstmt = conn.prepareStatement("SELECT id, nombre FROM entidad");

            rs = pstmt.executeQuery();

            while (rs.next()) {
                jsonObject = new JSONObject();
                jsonObject.put("id", rs.getString("id"));
                jsonObject.put("name", rs.getString("nombre"));
                jsonArray.put(jsonObject);
            }

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        return jsonArray;
    }

    public String getCSSPorProyecto(String idProyecto) {
        String result = "siamcss.css";


        PreparedStatement pstmt = null;

        try {
            cFactory = new ConnectionFactory();
            conn = cFactory.createConnection();
            pstmt = conn.prepareStatement("select css_pro from clst_proyectos where codigo=?");
            pstmt.setString(1, idProyecto);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                result = rs.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                ConnectionFactory.closeConnection(conn);
            }

        }

        return result;
    }
/////USER ADMINISTRATION - END
}
