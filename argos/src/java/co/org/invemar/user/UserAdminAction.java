package co.org.invemar.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UserAdminAction {

	private UserAdminInfoRequestController infoRequest = new UserAdminInfoRequestController(null);
	
	private String username = null;
	private UserPermission userp = null;
	
	private HttpSession session = null;
	
	public UserAdminAction(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
	    
		response.setContentType("text/text; charset=ISO-8859-1");
		response.addHeader("Expires", "0");
	    response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
	    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
	    response.addHeader("Pragma", "no-cache");
	    
		session=request.getSession();
		String action = null;
		JSONArray arrayResult = null;
		JSONObject objectResult = null;
		
		username = null;
		
    	try{
        	username = session.getAttribute("username").toString();
        	userp = (UserPermission)session.getAttribute("userpermission");
    	}catch(Exception e){
    		username = null;
       	}
    	
    	if(username==null){
    		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    		return;
    	}else{
			//check user permission
			UserPermission userp = (UserPermission)session.getAttribute("userpermission");
			
			if(!userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)&&!userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	    		return;
			}
    	}
	    
    	action = request.getParameter("action");
    	
    	if(action.equals("getProjects")){
			
			response.setContentType("application/json");
			
			int projectId= Integer.parseInt(session.getAttribute("projectid").toString());
			
			try{
				if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)){
					response.getWriter().write(infoRequest.getProjects(projectId).toString());
				}else if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
					response.getWriter().write(infoRequest.getProjects(-1).toString());
				}
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("getUserInformation")){
			
			response.setContentType("application/json");
			
			try{
				if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)){
					int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
					String entityId = session.getAttribute("entityid").toString();
					response.getWriter().write(infoRequest.getAllowedUserInformation(projectId, entityId).toString());
				}else if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
					int projectId= Integer.parseInt(request.getParameter("projectid").toString());
					response.getWriter().write(infoRequest.getUserInformationByProject(projectId).toString());
				}
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("searchUserInformation")){
			
			response.setContentType("application/json");
			
			String email = request.getParameter("email").toString();
			String name = request.getParameter("name").toString();
			
			try{
				if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)){
					int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
					String entityId = session.getAttribute("entityid").toString();
					response.getWriter().write(infoRequest.searchAllowedUserInformation(projectId, entityId, email, name).toString());
				}else if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
					response.getWriter().write(infoRequest.searchUserInformation(email, name).toString());
				}
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("changeUserStatus")){
			
    		response.setContentType("text/html");
			
			int userId= Integer.parseInt(request.getParameter("userid").toString());
			String newStatus = request.getParameter("newstatus").toString();
						
			try{
				if(userp.couldUser(UserPermission.ACTIVAR_DESACTIVAR_USUARIOS)){
					
					if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)){
						int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
						String entityId = session.getAttribute("entityid").toString();
						response.getWriter().write(updateUserStatus(userId, projectId, entityId, newStatus).toString());
					}else if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
						if(infoRequest.changeUserStatus(userId, newStatus).toString().equals("OK")){
							response.getWriter().write(userId + "_" + newStatus);
						}
					}
					
				}else{
					response.getWriter().write("NOP");
				}
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}    	
	    
	}
	
	/*update user status*/
	public String updateUserStatus(int userId, int projectId, String entityId, String newStatus){
		
		String result = "OK";
		
		try{
			
			result = infoRequest.checkUserEditionPermission(userId, projectId, entityId);
			
			if(result.equals("OK")){
				
				result = infoRequest.changeUserStatus(userId, newStatus);
				
				if(result.equals("OK")){
					result = userId + "_" + newStatus;
				}
				
			}else{
				result = "NOP";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println(result);
		return result;
		
	}
	
	
}
