package co.org.invemar.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.Cmail;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UserRegistrationAction {
	
	private String username = null;
	private UserPermission userp = null;
	
	private HttpSession session = null;
	
	UserRegistrationInfoRequestController infoRequest = new UserRegistrationInfoRequestController(null);
	
	private String siteUrl = "http://cinto.invemar.org.co/argos/login.jsp";
	
	public UserRegistrationAction(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		
		response.setContentType("text/text; charset=ISO-8859-1");
	    response.addHeader("Expires", "0");
	    response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
	    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
	    response.addHeader("Pragma", "no-cache");
	    
		session=request.getSession();
		String action = null;
		JSONArray arrayResult = null;
		JSONObject objectResult = null;
		
		username = null;
		
		
    	try{
        	username = session.getAttribute("username").toString();
        	userp = (UserPermission)session.getAttribute("userpermission");
    	}catch(Exception e){
    		username = null;
       	}
    	
    	if(username==null){
    		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    		return;
    	}else{
    			//check user permission
    			UserPermission userp = (UserPermission)session.getAttribute("userpermission");
    			
    			if(!userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)&&!userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
    				response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    	    		return;
    			}
    	}
    	
    	action = request.getParameter("action");
    	
		if(action.equals("getEntities")){
			
			response.setContentType("application/json");

			arrayResult = getEntities(session);
			response.getWriter().write(arrayResult.toString());
			
		}else if(action.equals("checkEmail")){
			
			response.setContentType("text/html");
			
			String email= request.getParameter("email");
			
			try{
				response.getWriter().write(infoRequest.checkEmail(email));
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("regUserGenInfo")){
			
			response.setContentType("text/html");
			
			String fname = request.getParameter("fname");
			String lname = request.getParameter("lname");
			String entity = request.getParameter("entity");
			String email = request.getParameter("email");
			String password= request.getParameter("password");
			boolean sendMail = Boolean.parseBoolean(request.getParameter("sendmail").toString());
			
			try{
				response.getWriter().write(this.registerNewUser(email, password, fname, lname, entity, sendMail));
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("getUserGenInfo")){
			
			response.setContentType("application/json");
			
			int userId= Integer.parseInt(request.getParameter("userid").toString());		
			
			try{
				objectResult = infoRequest.getUserGeneralInformation(userId);
				response.getWriter().write(objectResult.toString());
			}catch(Exception e){
				e.printStackTrace();
			}
			
			try{
				if(objectResult==null){
					objectResult = new JSONObject();
					objectResult.put("result", "NO");
					response.getWriter().write(objectResult.toString());
				}			
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}else if(action.equals("updateUserGenInfo")){
			
			response.setContentType("text/html");
			
			int userId= Integer.parseInt(request.getParameter("userid").toString());
			String fname = request.getParameter("fname");
			String lname = request.getParameter("lname");
			String entity = request.getParameter("entity");
			String email = request.getParameter("email");
			String password= request.getParameter("password");
			boolean sendMail = Boolean.parseBoolean(request.getParameter("sendmail").toString());
			
			try{
				response.getWriter().write(this.updateUserGeneralInfo(userId, email, password, fname, lname, entity, sendMail));
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("checkEmailToUpdate")){
			
			response.setContentType("text/html");
			
			String email= request.getParameter("email");
			int userId= Integer.parseInt(request.getParameter("userid").toString());
			
			try{
				response.getWriter().write(infoRequest.checkEmailToUpdate(userId, email));
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}else if(action.equals("checkUserEditionPermission")){
			
    		response.setContentType("text/html");
			
			int userId= Integer.parseInt(request.getParameter("userid").toString());
			int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
			String entityId = session.getAttribute("entityid").toString();
						
			try{
				if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)){
					response.getWriter().write(infoRequest.checkUserEditionPermission(userId, projectId, entityId));
				}if(userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
					response.getWriter().write("OK");	
				}
			}catch(Exception e){
				response.getWriter().write("NO");
				e.printStackTrace();
			}
			
		}
	}
	
	/*get entities from database*/
	private JSONArray getEntities(HttpSession session){
		
		JSONArray result = null;
		
		try{
			
		if(userp.couldUser(userp.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO))
			result = infoRequest.getEntityInformation(session.getAttribute("entityid").toString());
		else if(userp.couldUser(userp.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE))
			result = infoRequest.getAllEntities();

		}catch(Exception e){
			result = null;
		}
		
		return result;
		
	}
	
	/*register new user process (really? :P)*/
	public String registerNewUser(String email, String password, String fname, String lname, String entity, boolean sendMail){
		
		String result = "OK";
		String ckEmail = null;
		
		int userId = -1;
		
		try{
			
			ckEmail = infoRequest.checkEmail(email);
			
			if(ckEmail.equals("OK")){
				
				//if email does not exists save user info
				if(infoRequest.saveNewUserGeneralInformation(email, password, fname, lname)){
					
					//get new user ID
					userId = infoRequest.getNewUserId(email);
					
					if(userId>-1){
						
						//save user entity
						if(infoRequest.saveUserDetail(userId, 19, entity)){
							
							if(sendMail){
								sendPasswordMessage(email, password);
							}
							
						}else{
							result = "NO";
						}
					}
				}else{
					result = "NO";
				}
				
			}else{
				result = "EMAILE";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}

	/*update user general info*/
	public String updateUserGeneralInfo(int userId, String email, String password, String fname, String lname, String entity, boolean sendMail){
		
		String result = "OK";
		String ckEmail = null;
		
		try{
			
			ckEmail = infoRequest.checkEmailToUpdate(userId, email);
			
			if(ckEmail.equals("OK")){
				
				//if email does not exists save user info
				if(infoRequest.updateUserGeneralInformation(userId, email, password, fname, lname)){
					
						//save user entity
						if(infoRequest.updateUserDetail(userId, 19, entity)){
							
							if(sendMail && !password.equals("null")){
								sendPasswordMessage(email, password);
							}
							
						}else{
							result = "NO";
						}

				}else{
					result = "NO";
				}
				
			}else{
				result = "EMAILE";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	/*sends a message to user with password*/
	private boolean sendPasswordMessage(String email, String Password){	
		
		boolean result = true;
		
		StringBuffer message = new StringBuffer();
		
		Cmail mailSender = new Cmail();
		
		//set basic message data
		mailSender.setafrom(session.getAttribute("username").toString());
		mailSender.setato(email);
		mailSender.setasubject("INVEMAR/ARGOS - Nueva contrase�a");
		
		//set message content
		message.append("<HTML>");
		message.append("<HEAD>");
		message.append("</HEAD>");
		message.append("<BODY>");
		message.append("<p>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</p>");

		message.append("<p>Un administrador del sistema ha creado/actualizado una cuenta de usuario para el propietario de &eacute;sta direcci&oacute;n de correo, a continuaci&oacute;n se suministra la contrase&ntilde;a configurada para su cuenta.</p>");
		message.append("<p>Contrase&ntilde;a:  "+ Password +"</p>");
		message.append("<p>Si desea ingresar al sistema haga <a href='"+this.siteUrl+"'>click aqu&iacute;</a></p>");		
		message.append("</BODY>");
		message.append("</HTML>");
		
		mailSender.setamensaje(message.toString());
		
		result = mailSender.manda();
		
		return result;
		
	}
	
}
