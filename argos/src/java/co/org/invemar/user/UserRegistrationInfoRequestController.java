package co.org.invemar.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpSession;

import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.SecurityComponent;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UserRegistrationInfoRequestController {

	//DB connection variables
	private ConnectionFactory cFactory;
	private Connection conn=null;
	private String schema = null;
	
	//security component
	private SecurityComponent security = null;
	
	public UserRegistrationInfoRequestController(String schema){
		this.schema = schema;
	}

/////NEW REGISTRATION - START	
	
	public JSONArray getAllEntities() throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT CODIGO_IN, NOMBRE_IN FROM UDIRENTIDADES");
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("name", rs.getString(2));
				jsonArray.put(jsonObject);
			}

		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return jsonArray;
	}
	
	public JSONArray getEntityInformation(String entityId) throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT CODIGO_IN, NOMBRE_IN FROM UDIRENTIDADES WHERE CODIGO_IN=?");
			pstmt.setString(1, entityId);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("name", rs.getString(2));
				jsonArray.put(jsonObject);
			}

		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return jsonArray;
	}
	
	/*confirms the existence of user with a specific email*/
	public String checkEmail(String email) throws SQLException{
		
		String result = "OK";
				
	    PreparedStatement pstmt = null;
	    
	    try{
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT ID FROM USUARIOS WHERE USERNAME_EMAIL=?");
			pstmt.setString(1, email);
						
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next()){
				result = "NO";
			}else{
				result = "OK";
			}
			
	    }catch(Exception e){
	    	result = "NO";
	    	e.printStackTrace();
	    }finally {
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
	    return result;
	    
	}
		
	/*saves new user general information in database*/
	public boolean saveNewUserGeneralInformation(String email, String password, String fname, String lname) throws SQLException {

		boolean result = true;
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		this.security = new SecurityComponent();
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("INSERT INTO USUARIOS (username_email, password, nombre, apellido, activo) values(?,?,?,?,'S')");
			pstmt.setString(1, email);
			pstmt.setString(2, this.security.getSHAfromString(password));
			pstmt.setString(3, fname);
			pstmt.setString(4, lname);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = true;
			}else{
				result = false;
			}

		}catch(Exception e){
			result = false;
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}

	public int getNewUserId(String email) throws SQLException {
		
		int result = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT ID FROM USUARIOS WHERE USERNAME_EMAIL=?");
			pstmt.setString(1, email);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				
				result = rs.getInt(1);
				
			}

		}catch(Exception e){
			result = -1;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}
	
	/*saves user detail information*/
	public boolean saveUserDetail(int userId, int detailId, String detail) throws SQLException {

		boolean result = true;
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("INSERT INTO USUARIOS_DETALLES (id_usuario, codigo_atributo, valor_asignado) values(?,?,?)");
			pstmt.setInt(1, userId);
			pstmt.setInt(2, detailId);
			pstmt.setString(3, detail);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = true;
			}else{
				result = false;
			}

		}catch(Exception e){
			result = false;
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}

/////NEW REGISTRATION - END	

/////EDIT USER GENERAL INFORMATION - START

	/*returns detail titles but just those who have not been registered for a specific user*/
	public String checkUserEditionPermission(int userId, int projectId, String entityId) throws SQLException {
		
		String result = "OK";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT USUARIOS.ID FROM ((((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN CLST_PROYECTOS ON CLST_PROYECTOS.CODIGO=UROLES.ID_PROYECTO) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) WHERE USUARIOS.ID=? AND CLST_PROYECTOS.CODIGO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_DETALLES.VALOR_ASIGNADO=?");
			pstmt.setInt(1, userId);
			pstmt.setInt(2, projectId);
			pstmt.setString(3, entityId);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				result = "OK";			
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return result;
	}
		
	public JSONObject getUserGeneralInformation(int userId) throws SQLException {
		
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT USUARIOS.NOMBRE, USUARIOS.APELLIDO, USUARIOS.USERNAME_EMAIL, USUARIOS_DETALLES.VALOR_ASIGNADO FROM (USUARIOS LEFT JOIN USUARIOS_DETALLES ON USUARIOS.ID=USUARIOS_DETALLES.ID_USUARIO AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19) WHERE USUARIOS.ID=?");
			pstmt.setInt(1, userId);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("result", "OK");
				jsonObject.put("fname", rs.getString(1));
				jsonObject.put("lname", rs.getString(2));
				jsonObject.put("email", rs.getString(3));
				jsonObject.put("entity", rs.getString(4));
				
			}else{
				jsonObject = new JSONObject();
				jsonObject.put("result", "NO");				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		try{
			if(jsonObject==null){
				jsonObject = new JSONObject();
				jsonObject.put("result", "NO");	
			}			
		}catch(Exception e){
			e.printStackTrace();
		}

		return jsonObject;
	}	

	/*confirms the existence of user with a specific email*/
	public String checkEmailToUpdate(int userId, String email) throws SQLException{
		
		String result = "OK";
				
	    PreparedStatement pstmt = null;
	    
	    try{
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT ID FROM USUARIOS WHERE USERNAME_EMAIL=? AND ID!=?");
			pstmt.setString(1, email);
			pstmt.setInt(2, userId);
						
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next()){
				result = "NO";
			}else{
				result = "OK";
			}
			
	    }catch(Exception e){
	    	result = "NO";
	    	e.printStackTrace();
	    }finally {
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
	    return result;
	    
	}
	
	/*saves new user general information in database*/
	public boolean updateUserGeneralInformation(int userId, String email, String password, String fname, String lname) throws SQLException {

		boolean result = true;
		int rows = -1;
		int i = 1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = null;
		this.security = new SecurityComponent();
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			if(password.equals("null")){
				query = "UPDATE USUARIOS SET username_email=?, nombre=?, apellido=? WHERE ID=?";
			}else{
				query = "UPDATE USUARIOS SET username_email=?, nombre=?, apellido=?, password=? WHERE ID=?";
			}
			
			pstmt = conn.prepareStatement(query);
			pstmt.setString(i++, email);
			pstmt.setString(i++, fname);
			pstmt.setString(i++, lname);
			if(!password.equals("null")){
				pstmt.setString(i++, this.security.getSHAfromString(password));
			}
			pstmt.setInt(i++, userId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = true;
			}else{
				result = false;
			}

		}catch(Exception e){
			result = false;
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}

	/*saves new user general information in database*/
	public String updateUserPassword(int userId, String newPassword) throws SQLException {

		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String query = null;
		this.security = new SecurityComponent();
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			query = "UPDATE USUARIOS SET password=? WHERE ID=?";

			
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, this.security.getSHAfromString(newPassword));
			pstmt.setInt(2, userId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}	
	
	/*updates user detail information*/
	public boolean updateUserDetail(int userId, int detailId, String detail) throws SQLException {

		boolean result = true;
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("UPDATE USUARIOS_DETALLES SET valor_asignado=? WHERE ID_USUARIO=? AND CODIGO_ATRIBUTO=?");
			pstmt.setString(1, detail);
			pstmt.setInt(2, userId);
			pstmt.setInt(3, detailId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = true;
			}else{
				result = false;
			}

		}catch(Exception e){
			result = false;
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		if(!result){
			result = saveUserDetail(userId, detailId, detail);
		}
		
		return result;
	}

	
/////EDIT USER GENERAL INFORMATION - END


}
