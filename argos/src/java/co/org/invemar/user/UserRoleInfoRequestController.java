package co.org.invemar.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.ConnectionFactory;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UserRoleInfoRequestController {

	//DB connection variables
	private ConnectionFactory cFactory;
	private Connection conn=null;
	private String schema = null;
	
	public UserRoleInfoRequestController(String schema){
		this.schema = schema;
	}

	/*returns detail titles but just those who have not been registered for a specific user*/
	public String checkUserEditionPermission(int userId, int projectId, String entityId) throws SQLException {
		
		String result = "OK";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT USUARIOS.ID FROM ((((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN CLST_PROYECTOS ON CLST_PROYECTOS.CODIGO=UROLES.ID_PROYECTO) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) WHERE USUARIOS.ID=? AND CLST_PROYECTOS.CODIGO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_DETALLES.VALOR_ASIGNADO=?");
			pstmt.setInt(1, userId);
			pstmt.setInt(2, projectId);
			pstmt.setString(3, entityId);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				result = "OK";			
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return result;
	}

	/*returns detail titles but just those who have not been registered for a specific user*/
	public JSONArray getProjects(int projectId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			if(projectId == -1){
				
				query = "SELECT CLST_PROYECTOS.CODIGO, CLST_PROYECTOS.NOMBRE_ALTERNO FROM CLST_PROYECTOS ORDER BY CLST_PROYECTOS.NOMBRE_ALTERNO ASC";
				
			}else{
				query = "SELECT CLST_PROYECTOS.CODIGO, CLST_PROYECTOS.NOMBRE_ALTERNO FROM CLST_PROYECTOS WHERE CLST_PROYECTOS.CODIGO=?";
			}
			
			pstmt = conn.prepareStatement(query);
			
			if(projectId != -1){
				pstmt.setInt(1, projectId);	
			}
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("name", rs.getString(2));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}		

	/*returns detail titles but just those who have not been registered for a specific user*/
	public JSONArray getProjectRoles(int projectId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT UROLES.ID, UROLES.DESCRIPCION FROM UROLES WHERE UROLES.ID_PROYECTO=? ORDER BY UROLES.ID ASC");
			pstmt.setInt(1, projectId);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("name", rs.getString(2));

				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}

	/*returns user roles*/
	public JSONArray getUserRoles(int userId, int projectId) throws SQLException {
		
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String query = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			
			if(projectId==-1){
				query = "SELECT CLST_PROYECTOS.NOMBRE_ALTERNO, UROLES.ID, UROLES.DESCRIPCION FROM ((UROLES_USUARIOS INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN CLST_PROYECTOS ON UROLES.ID_PROYECTO=CLST_PROYECTOS.CODIGO) WHERE UROLES_USUARIOS.ID_USUARIO=?";
			}else{
				query = "SELECT CLST_PROYECTOS.NOMBRE_ALTERNO, UROLES.ID, UROLES.DESCRIPCION FROM ((UROLES_USUARIOS INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN CLST_PROYECTOS ON UROLES.ID_PROYECTO=CLST_PROYECTOS.CODIGO) WHERE UROLES_USUARIOS.ID_USUARIO=? AND UROLES.ID_PROYECTO=?";
			}
			
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, userId);
			
			if(projectId!=-1)
				pstmt.setInt(2, projectId);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				jsonObject = new JSONObject();
				jsonObject.put("projectname", rs.getString(1));
				jsonObject.put("roleid", rs.getString(2));
				jsonObject.put("rolename", rs.getString(3));
				
				jsonArray.put(jsonObject);
				
			}

		}catch(Exception e){
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return jsonArray;
	}
	
	/*set user role*/
	public String setUserRole(int userId, int roleId, int projectId){
		
		String result = "OK";
		
		try{
			
			result = checkUserRoleAvailability(userId, projectId);
			
			if(result.equals("OK")){
				
				result = setRole(roleId, userId);
				
			}else{
				result = "NOR";
			}
			
		}catch(Exception e){
			e.printStackTrace();
			result = "NO";
		}
		
		return result;
		
	}
	
	/*checks if user has a role for a project*/
	public String checkUserRoleAvailability(int userId, int projectId) throws SQLException {
		
		String result = "OK";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT UROLES.ID FROM (UROLES_USUARIOS INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) WHERE UROLES_USUARIOS.ID_USUARIO=? AND UROLES.ID_PROYECTO=?");
			pstmt.setInt(1, userId);
			pstmt.setInt(2, projectId);
			
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				result = "NO";			
			}else{
				result = "OK";
			}

		}catch(Exception e){
			result = "NO";
			System.out.println(e);
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}
		
		return result;
	}	
	
	/*set a role for an user*/
	public String setRole(int roleId, int userId) throws SQLException {

		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("INSERT INTO UROLES_USUARIOS (ID_ROL, ID_USUARIO) VALUES (?,?)");
			pstmt.setInt(1, roleId);
			pstmt.setInt(2, userId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}	
	
	/*remove user role*/
	public String removeUserRole(int userId, int roleId) throws SQLException {

		String result = "OK";
		int rows = -1;
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn.prepareStatement("DELETE FROM UROLES_USUARIOS WHERE ID_ROL=? AND ID_USUARIO=?");
			pstmt.setInt(1, roleId);
			pstmt.setInt(2, userId);
			
			rows = pstmt.executeUpdate();
			
			if(rows>0){
				result = "OK";
			}else{
				result = "NO";
			}

		}catch(Exception e){
			result = "NO";
			e.printStackTrace();
		}
		finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if(conn!=null){
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}	
	
}
