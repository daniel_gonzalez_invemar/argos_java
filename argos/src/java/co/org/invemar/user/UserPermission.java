package co.org.invemar.user;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UserPermission {

	public static final int REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE = 1;
	public static final int REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO = 2;
	public static final int ACTIVAR_DESACTIVAR_USUARIOS = 3;
	public static final int INGRESAR_DATOS = 4;
	public static final int INGRESAR_DATOS_EN_BRUTO= 5;
	public static final int VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL = 6;
	public static final int VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD = 7;
	public static final int VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_TODAS_LAS_ENTIDADES_PARTICIPANTES = 8;
	public static final int ACTUALIZAR_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL = 9;
	public static final int ACTUALIZAR_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD = 10;
	public static final int VER_MI_HISTORIAL_DE_EVENTOS_PARA_EL_PROYECTO_ACTUAL = 11;
	public static final int VER_HISTORIAL_DE_EVENTOS_DE_LA_ENTIDAD_PARA_EL_PROYECTO_ACTUAL = 12;
	public static final int ADMINISTRAR_ROLES = 13;
	
	//this is the range of permissions in the data base
	private int permRange = 700;
	
	/*currently there are 13 permision types, deny all of them*/
	private boolean[] permissions = {false, false, false, false, false, false, false, false, false, false, false, false, false, false};
	
	/*allow user to do something*/
	public void allowUserTo(int permission){
		
		this.permissions[permission-this.permRange]=true;
		
	}

	/*deny user to do something*/
	public void denyUserTo(int permission){
		
		this.permissions[permission-this.permRange]=false;
		
	}
	
	/*answers the question �could user do something?*/
	public boolean couldUser(int permission){
		
		return this.permissions[permission];
		
	}

	
}

