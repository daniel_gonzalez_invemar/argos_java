package co.org.invemar.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.SecurityComponent;
import co.org.invemar.util.Cmail;

import java.util.Date;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class Login extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request,response);
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		synchronized(this){
			new LoginAction(request, response);
		}
		
	    
	}

}
