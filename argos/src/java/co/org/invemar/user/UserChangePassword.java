package co.org.invemar.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpSession;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 * */
public class UserChangePassword extends HttpServlet {


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
		
	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		synchronized(this){
			
			HttpSession session=request.getSession();
			String username = null;
			
			
	    	try{
	        	username = session.getAttribute("username").toString();
	    	}catch(Exception e){
	    		username = null;
	       	}
	    	
	    	if(username==null){
	    		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	    		return;
	    	}
			
	    	response.setContentType("text/text; charset=ISO-8859-1");
		    response.addHeader("Expires", "0");
		    response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		    response.addHeader("Pragma", "no-cache");
		    response.setContentType("text/html");
		    
		    String newPassword= request.getParameter("newpassword");
		    
		    UserRegistrationInfoRequestController infoRequest = new UserRegistrationInfoRequestController(null);
		    
		    int userId = Integer.parseInt(session.getAttribute("userid").toString());
		    
		    try{
		    	response.getWriter().write(infoRequest.updateUserPassword(userId, newPassword));	
		    }catch(Exception e){
		    	
		    }
		    
		    
		}
		
	}

}
