/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.model.RegenerationStatisticModel;
import co.org.invemar.library.siam.metadatos.Metadatos;
import co.org.invemar.library.siam.vo.Responsable;
import co.org.invemar.util.ConnectionFactory;

import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class GetResearcherAction implements Action {

    @Override
    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        String jsonData="";
        Connection con = null;
        try {                                         
            
          String thematic= request.getParameter("thematic");
          String timeseries= request.getParameter("timeseries");
          String codSector= request.getParameter("codsector");           
          
          
        
          ConnectionFactory conectionfactory = new ConnectionFactory();
          con = conectionfactory.createConnection("monitoreom");
          Metadatos metadatos = new Metadatos();   
          ArrayList<Responsable> responsibleList = metadatos.getResponsableDatosBySector(con,Long.parseLong(codSector),timeseries, thematic);
          JSONArray jsonArray = new JSONArray();
            for (Responsable r : responsibleList) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("lastname",r.getApellido());
                jsonObject.put("name",r.getNombre());
                jsonObject.put("company", r.getVinculado());     
                jsonArray.put(jsonObject);
            }          
          jsonData = jsonArray.toString();
         
          
        } catch (Exception e) {
            Logger.getLogger(ListCaseStudioByEntityAction.class.getName()).log(Level.SEVERE,e.getMessage(), e);
        }
        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");      
        response.getWriter().write(jsonData);
        return null;
        
        
    }
    
}
