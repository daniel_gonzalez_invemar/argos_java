/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.model.CoordenadasIndividuosModel;
import co.org.invemar.argos.manglares.model.RegenerationStatisticModel;
import co.org.invemar.library.siam.exportadores.ExportXLS;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class ExportExcelStatisticRegenerationAction implements Action {

    @Override
    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

       byte file[]=null;
        File f =null;
        try {
            
            String variable= request.getParameter("variable");
            String nivel= request.getParameter("nivel");
            String codCasoStudio= request.getParameter("codCasoStudio"); 
            String categoria    =request.getParameter("categoria");
            
            RegenerationStatisticModel rsm = new RegenerationStatisticModel();
            Hashtable<String,String> parameters = new Hashtable<String,String>();   
            parameters.put("categoria",categoria);
           
            
            rsm.getStatisticByVariableAndNivelAndCodArea(variable, Integer.parseInt(nivel), codCasoStudio,parameters);                        
            
            String path= request.getSession().getServletContext().getRealPath("/downloaddata/");
            String fileName = path+File.separator+"export"+Calendar.getInstance().getTimeInMillis()+ ".xls";         
            ExportXLS exportxls = new ExportXLS(fileName, rsm.getDataColum(), rsm.getRowsData());
            f =new File(fileName);        
            
            file=exportxls.generateExcelFile();
            
           

        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }finally{
            if(f.exists()){
                f.delete();
            }
        }
        response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-Disposition", "attachment; filename=\"data.xls\"");
	response.setHeader("Pragma", "no-cache");
	ServletOutputStream outs = response.getOutputStream();
	outs.write(file);
	outs.flush();
	outs.close();
        
        return null;

    }
}
