/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.model.CoordenadasIndividuosModel;
import co.org.invemar.argos.manglares.vo.CoordenadasIndividuos;
import co.org.invemar.util.ResultadoTransaccion;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class UpdateCoordenadasIndividuosAction implements Action {

    @Override
    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        String jsonData="";
        JSONObject json= new JSONObject();
        try {
            String codParcel = request.getParameter("codParcel");                     
            String idIndividuo = request.getParameter("idindividuo");  
            String tagIndividuo = request.getParameter("tagindividuo");  
            String codSpecie = request.getParameter("codSpecie");  
            String distance = request.getParameter("distance");             
            String azimut = request.getParameter("azimut");      
            String coorx= request.getParameter("coorx");   
            String coory= request.getParameter("coory");  
            String entity=request.getParameter("entity");
            String pk    = request.getParameter("pk");
            
        
            
            
              
            
            CoordenadasIndividuosModel cim = new CoordenadasIndividuosModel();
            CoordenadasIndividuos ci = new CoordenadasIndividuos();
            ci.setCodParcel(Long.parseLong(codParcel));
            ci.setIdElement(Long.parseLong(idIndividuo));
            ci.setTag(Long.parseLong(tagIndividuo));
            ci.setCodSpecies(codSpecie);
            ci.setDistance(Double.parseDouble(distance));
            ci.setAzimut(Long.parseLong(azimut));
            ci.setX(Long.parseLong(coorx));
            ci.setY(Double.parseDouble(coory));
            ci.setEntity(entity);
            ci.setPk(Long.parseLong(pk));
            ResultadoTransaccion rt=cim.updateCoordenateTree(ci);
          
            json.put("result",rt.getCodigo());
            json.put("message",rt.getMensaje());
            jsonData = json.toString();           
            Logger.getLogger(getClass().getName()).debug(jsonData);
            
           
          

        } catch (Exception e) {
           Logger.getLogger(getClass().getName()).error(e.getMessage());
           try {
                Logger.getLogger(getClass().getName()).error(e.getMessage());
                json.put("result","-1");
                json.put("message",e.getMessage());         
                jsonData = json.toString();
            } catch (JSONException ex) {
                java.util.logging.Logger.getLogger(RegisterCoordenadasIndividuosAction.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");      
        response.getWriter().write(jsonData);
        return null;
        
        
    }
    
}
