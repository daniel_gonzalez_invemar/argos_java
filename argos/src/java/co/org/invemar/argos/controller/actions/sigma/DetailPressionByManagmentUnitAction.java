/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.presiones.model.EgretaProcessing;
import co.org.invemar.library.siam.Utilidades;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;

/**
 *
 * @author usrsig15
 */
public class DetailPressionByManagmentUnitAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        JSONArray jsonarray = null;
        String html = null;
        try {
            String department = request.getParameter("d");
            Utilidades u  = new Utilidades();
            
            department = u.RecodeString(department);             
            String managmentUnit = request.getParameter("area");   
            String anio = request.getParameter("a");
         

            EgretaProcessing ep = new EgretaProcessing();
            jsonarray = ep.DetailPressionByManagmentUnit(department, managmentUnit, Integer.parseInt(anio));
          
           

        } catch (Exception e) {
            Logger.getLogger(IndicatorByDepartmentAction.class.getName()).log(Level.SEVERE, e.getMessage());
        }
      
        response.setContentType("text/test; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonarray.toString());
        return null;
    }

    
}
