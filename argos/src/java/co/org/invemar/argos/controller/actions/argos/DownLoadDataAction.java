/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.argos;

import co.org.invemar.argos.configuracion.ReaderDownLoaderSettingFile;
import co.org.invemar.argos.downloadData.DownLoadData;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author usrsig15
 */


public class DownLoadDataAction implements Action {


 

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String fileName = "";
        String miliseconds = "";
        JSONObject jsonObject = null;
        try {

            String userId = request.getParameter("idUser");
            String year = request.getParameter("year");
            String entityUser = request.getParameter("idEntity");
            String idThematic = request.getParameter("idThematic");
            String idProject = request.getParameter("idproject");

            System.out.println("action:" + this.getClass()); 
            
            

            String pathFileSetting = request.getSession().getServletContext().getRealPath("/conf/datadownloader/" + idProject + ".xml");

            DownLoadData downloaddata = new DownLoadData(new ReaderDownLoaderSettingFile(pathFileSetting));
            
            String path = request.getSession().getServletContext().getRealPath("/downloaddata/");
            miliseconds = String.valueOf(Calendar.getInstance().getTimeInMillis());
            fileName = path + File.separator + "data" + miliseconds;

            System.out.println("Starting writing.........");
            
            downloaddata.setFillCSVFile(fileName+".csv", downloaddata.builderQuery(idThematic, entityUser, userId, year, idProject), idThematic);
            downloaddata.setFillExcelFile(fileName+".xlsx", downloaddata.builderQuery(idThematic, entityUser, userId, year, idProject), idThematic);
            
           
            System.out.println("file writed....");

            jsonObject = new JSONObject();
            jsonObject.put("pathCSV", "downloaddata/data" + miliseconds + ".csv");
            jsonObject.put("pathEXCEL", "downloaddata/data" + miliseconds + ".xlsx");

            response.setContentType("text/json; charset=ISO-8859-1");
            response.setHeader("Cache-Control", "no-cache");

            PrintWriter pw = response.getWriter();

            pw.write(jsonObject.toString());
            pw.close();
            pw.flush();
            
        } catch (JSONException ex) {
            Logger.getLogger(DownLoadDataAction.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (Exception e) {
            Logger.getLogger(DownLoadDataAction.class.getName()).log(Level.SEVERE, e.getMessage(),e);
        }

        return null;
    }

}
