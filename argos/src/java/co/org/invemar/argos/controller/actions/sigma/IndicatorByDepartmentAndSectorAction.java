/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.presiones.model.EgretaProcessing;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class IndicatorByDepartmentAndSectorAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        JSONArray jsonArray = null;
        try {
            String department = request.getParameter("d");
            String sector = request.getParameter("s");
            String anio = request.getParameter("a");

            EgretaProcessing ep = new EgretaProcessing();            
            
            jsonArray = ep.DetailIndicatorByPression(department,sector,Integer.parseInt(anio));  

        } catch (Exception e) {
            Logger.getLogger(IndicatorByDepartmentAction.class.getName()).log(Level.SEVERE, e.getMessage());
        }
        response.setContentType("text/text; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonArray.toString());
        return null;
    }
}
