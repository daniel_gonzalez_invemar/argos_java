/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.presiones.model.EgretaProcessing;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class DepartamentoUshashidiAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        JSONArray arrayJsonData = new JSONArray();
        try {
            String department = request.getParameter("d");            
            String anio = request.getParameter("a");

            EgretaProcessing ep = new EgretaProcessing();
            List<String> list = ep.getDepartmentList();
            Iterator<String> it = list.iterator();

            while (it.hasNext()) {
                String d = it.next();
                JSONObject obj = new JSONObject();
                obj.put("id", d);
                obj.put("name", d);
                arrayJsonData.put(obj);

            }

        } catch (Exception e) {
            Logger.getLogger(DepartamentoUshashidiAction.class.getName()).log(Level.SEVERE, e.getMessage());
        }
        response.setContentType("text/text; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(arrayJsonData.toString());
        return null;
    }

}
