/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.model.AreasParcelasModel;
import co.org.invemar.util.ResultadoTransaccion;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author root
 */
public class recordParcelAreaAction implements Action{
    
     public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String result = "No Actualizada";
        AreasParcelasModel areaParcelasModel = new AreasParcelasModel();
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        
            
            String codParcel = request.getParameter("codparcel");
            String anio = request.getParameter("anio");
            String parcelNumber = request.getParameter("parcelmumber");
            String area = request.getParameter("area");
            String unit = request.getParameter("unit");
            String user_id = request.getParameter("user_id");
            String entity = request.getParameter("entity");          
            resultadoTransaccion = areaParcelasModel.SaveAreaParcel(codParcel, anio, parcelNumber, area, unit, user_id, entity);
            if(resultadoTransaccion.getCodigoError() == 0){
                result = "Actualizada";
            }
       
        response.setContentType("text/text; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(result);
        return null;
    }
    
}
