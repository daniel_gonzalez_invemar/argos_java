/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.controller.actions.sigma;


import co.org.invemar.argos.manglares.model.CaseStudioModel;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class ListCaseStudioByTypeAction implements Action {

    @Override
    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
         JSONObject jsonObject = new JSONObject();
        try {
            String caseStudioType = request.getParameter("casestudiotype");         
            
            CaseStudioModel caseStudioModel = new CaseStudioModel();
            jsonObject= caseStudioModel.getAllCaseStudioByType(Integer.parseInt(caseStudioType));          
          

        } catch (Exception e) {
            Logger.getLogger(DepartamentoUshashidiAction.class.getName()).log(Level.SEVERE, e.getMessage());
        }
        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");      
        response.getWriter().write(jsonObject.toString());
        return null;
        
        
    }
    
    
}
