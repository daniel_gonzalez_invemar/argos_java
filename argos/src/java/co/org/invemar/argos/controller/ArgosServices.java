package co.org.invemar.argos.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionFactory;
import co.org.invemar.util.actions.ActionRouter;

public class ArgosServices extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 8599964840309396540L;
    private final static String CLASSPACKAGE = "co.org.invemar.argos.controller.";

    /**
     * The doGet method of the servlet. <br>
     *
     * This method is called when a form has its tag value method equals to get.
     *
     * @param request the request send by the client to the server
     * @param response the response send by the server to the client
     * @throws ServletException if an error occurred
     * @throws IOException if an error occurred
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        doPost(request, response);
    }

    /**
     * The doPost method of the servlet. <br>
     *
     * This method is called when a form has its tag value method equals to
     * post.
     *
     * @param request the request send by the client to the server
     * @param response the response send by the server to the client
     * @throws ServletException if an error occurred
     * @throws IOException if an error occurred
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        String action = request.getParameter("action");
        String packa = request.getParameter("pkg");
        String className = CLASSPACKAGE + packa + action + "Action";      

        try {
            Action actions = ActionFactory.getAction(className);

            ActionRouter router = actions.execute(request, response);
            if (router != null) {
                router.route(this, request, response);
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }

    }

}
