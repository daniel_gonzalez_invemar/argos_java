/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.presiones.model.EgretaProcessing;
import co.org.invemar.siam.util.Testmysql;
import co.org.invemar.ushahidi.ManagmentFormFieldUshahidi;
import co.org.invemar.util.MySQL;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class getCategoryByIdAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
       
        String jsonCategory = null;
        try {
            String categoryId = request.getParameter("categoryId");             
            
            MySQL mysqlDB = new MySQL();
            Connection con = mysqlDB.connect("ushahidiegreta");           
            
            ManagmentFormFieldUshahidi mffu = new ManagmentFormFieldUshahidi();            
            jsonCategory=mffu.getJsonCategory(mffu.getCategoryParent(categoryId, con));
                       

        } catch (Exception e) {
           Logger.getLogger(IndicatorByDepartmentAction.class.getName()).log(Level.SEVERE,  e.getMessage());
        }
        response.setContentType("text/text; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonCategory);
        return null;
    }
    
}
