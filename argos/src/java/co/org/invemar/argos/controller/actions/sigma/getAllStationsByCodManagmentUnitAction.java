/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.model.SitioManglarModel;
import co.org.invemar.argos.manglares.model.UnidadManejoModel;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class getAllStationsByCodManagmentUnitAction implements Action {

    @Override
    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        String jsonData="";
        try {                                         
            
          String codManagmentUnit= request.getParameter("codManagmentUnit");    
          SitioManglarModel smm = new SitioManglarModel();
          smm.getTodosLosSitiosPorSectoresConEstadistica(Integer.parseInt(codManagmentUnit),"4",2) ;
         jsonData=smm.getJsonDataSite();
          
          
        } catch (Exception e) {
            Logger.getLogger(ListCaseStudioByEntityAction.class.getName()).log(Level.SEVERE,e.getMessage(), e);
        }
        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");      
        response.getWriter().write(jsonData);
        return null;
        
        
    }
    
}
