/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.model.RegenerationStatisticModel;
import co.org.invemar.argos.manglares.model.UnidadManejoModel;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class GraphicStatisticAction implements Action {

    @Override
    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        String jsonData="";
        try {                                         
            
          String variable= request.getParameter("variable");
          String nivel= request.getParameter("nivel");
          String codCasoStudio= request.getParameter("codCasoStudio");  
          String categoria    =request.getParameter("categoria");
          
          RegenerationStatisticModel rsm = new RegenerationStatisticModel();
          
          Hashtable<String,String> parameters = new Hashtable<String,String>();
          parameters.put("categoria",categoria);
          
          
          rsm.getStatisticByVariableAndNivelAndCodArea(variable, Integer.parseInt(nivel), codCasoStudio,parameters);
          jsonData =  rsm.getJsonDataStatistic();  
          
       
          
          
        } catch (Exception e) {
            Logger.getLogger(ListCaseStudioByEntityAction.class.getName()).log(Level.SEVERE,e.getMessage(), e);
        }
        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");      
        response.getWriter().write(jsonData);
        return null;
        
        
    }
}
