/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.geograficos.model.Toponimia;
import co.org.invemar.argos.manglares.model.SectorManglaresModel;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class getAllSectorByCodDepartamentAction implements Action {

    @Override
    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        String jsonData="";
        try {                                         
            
          String codDepartament= request.getParameter("coddepartament");
          SectorManglaresModel smm = new SectorManglaresModel();
          jsonData=smm.getAllSectorWithStatisticByDepartament(4,codDepartament);
          
          
        } catch (Exception e) {
            Logger.getLogger(ListCaseStudioByEntityAction.class.getName()).log(Level.SEVERE,e.getMessage(), e);
        }
        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");      
        response.getWriter().write(jsonData);
        return null;
        
        
    }
    
}
