/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.argos;


import co.org.invemar.argos.configuracion.ReaderDownLoaderSettingFile;
import co.org.invemar.argos.downloadData.DownLoadData;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usrsig15
 */
public class GetYearMonitoreomThematicAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String data="";        
        try {

            String idThematic = request.getParameter("idThematic");
            String idEntity   = request.getParameter("idEntity");
            String idUser      = request.getParameter("idUser");
            String idProject      = request.getParameter("idproject");    
            
            System.out.println("action:"+this.getClass()); 
           String pathFileSetting =  request.getSession().getServletContext().getRealPath("/conf/datadownloader/"+idProject+".xml");  
           
           DownLoadData downloaddata = new DownLoadData(new ReaderDownLoaderSettingFile(pathFileSetting));        
           System.out.println("getting year....");
           data=downloaddata.getYearMonitoreumByThematic(idThematic, idUser, idEntity,idProject);
           System.out.println("gotten data....");
            

            
        } catch (Exception e) {
            Logger.getLogger(GetYearMonitoreomThematicAction.class.getName()).log(Level.SEVERE, e.getMessage());
        }
        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");       
        response.getWriter().print(data);
        response.getWriter().close();
        response.flushBuffer();
        
        
   
        return null;
    }

}
