/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.presiones.model.EgretaProcessing;
import co.org.invemar.library.siam.Utilidades;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class IndicatorByManagmentUnitAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        JSONObject jsonObject = new JSONObject();       
        
        try {
            String department = request.getParameter("d");         
            String sector = request.getParameter("s");      
            String year = request.getParameter("a");           

            EgretaProcessing ep = new EgretaProcessing();
                   
            jsonObject = ep.Indicator(department,sector,Integer.parseInt(year));  
           

        } catch (Exception e) {
            Logger.getLogger(IndicatorByManagmentUnitAction.class.getName()).log(Level.SEVERE, e.getMessage());
        }
        
        response.setContentType("text/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonObject.toString());
        return null;
    }
    
}
