/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.controller.actions.sigma;

import co.org.invemar.argos.manglares.presiones.model.EgretaProcessing;
import co.org.invemar.util.actions.Action;
import co.org.invemar.util.actions.ActionRouter;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import co.org.invemar.library.siam.Utilidades;

/**
 *
 * @author usrsig15
 */
public class DetailPresionBySectorAction implements Action {

    public ActionRouter execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        JSONArray jsonarray = null;
        String html = null;
        try {
            String department = request.getParameter("d");
            Utilidades u  = new Utilidades();
            
            department = u.RecodeString(department);     
            String sector = request.getParameter("area");     
            String anio = request.getParameter("a");          

            EgretaProcessing ep = new EgretaProcessing();
            jsonarray = ep.DetailIndicatorByPression(department, sector, Integer.parseInt(anio));
             
           

        } catch (Exception e) {
            Logger.getLogger(IndicatorByDepartmentAction.class.getName()).log(Level.SEVERE, e.getMessage());
        }
      
        response.setContentType("text/test; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().write(jsonarray.toString());
        return null;
    }

}
