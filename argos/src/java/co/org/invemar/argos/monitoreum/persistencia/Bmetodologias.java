/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BMETODOLOGIAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bmetodologias.findAll", query = "SELECT b FROM Bmetodologias b"),
    @NamedQuery(name = "Bmetodologias.findByIdMetodologia", query = "SELECT b FROM Bmetodologias b WHERE b.idMetodologia = :idMetodologia"),
    @NamedQuery(name = "Bmetodologias.findByNombre", query = "SELECT b FROM Bmetodologias b WHERE b.nombre = :nombre"),
    @NamedQuery(name = "Bmetodologias.findByDescripcion", query = "SELECT b FROM Bmetodologias b WHERE b.descripcion = :descripcion")})
public class Bmetodologias implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_METODOLOGIA")
    private Short idMetodologia;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bmetodologias")
    private List<BmetodologiasComp> bmetodologiasCompList;
    @OneToMany(mappedBy = "idMetodologia")
    private List<Bmuestreos> bmuestreosList;

    public Bmetodologias() {
    }

    public Bmetodologias(Short idMetodologia) {
        this.idMetodologia = idMetodologia;
    }

    public Bmetodologias(Short idMetodologia, String nombre) {
        this.idMetodologia = idMetodologia;
        this.nombre = nombre;
    }

    public Short getIdMetodologia() {
        return idMetodologia;
    }

    public void setIdMetodologia(Short idMetodologia) {
        this.idMetodologia = idMetodologia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<BmetodologiasComp> getBmetodologiasCompList() {
        return bmetodologiasCompList;
    }

    public void setBmetodologiasCompList(List<BmetodologiasComp> bmetodologiasCompList) {
        this.bmetodologiasCompList = bmetodologiasCompList;
    }

    @XmlTransient
    public List<Bmuestreos> getBmuestreosList() {
        return bmuestreosList;
    }

    public void setBmuestreosList(List<Bmuestreos> bmuestreosList) {
        this.bmuestreosList = bmuestreosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMetodologia != null ? idMetodologia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bmetodologias)) {
            return false;
        }
        Bmetodologias other = (Bmetodologias) object;
        if ((this.idMetodologia == null && other.idMetodologia != null) || (this.idMetodologia != null && !this.idMetodologia.equals(other.idMetodologia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Bmetodologias[ idMetodologia=" + idMetodologia + " ]";
    }
    
}
