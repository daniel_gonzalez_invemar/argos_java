/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BRANGOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Brangos.findAll", query = "SELECT b FROM Brangos b"),
    @NamedQuery(name = "Brangos.findByCodigoVariableRn", query = "SELECT b FROM Brangos b WHERE b.brangosPK.codigoVariableRn = :codigoVariableRn"),
    @NamedQuery(name = "Brangos.findByCodigoLegislacionRn", query = "SELECT b FROM Brangos b WHERE b.brangosPK.codigoLegislacionRn = :codigoLegislacionRn"),
    @NamedQuery(name = "Brangos.findByCodigoSustratoRn", query = "SELECT b FROM Brangos b WHERE b.brangosPK.codigoSustratoRn = :codigoSustratoRn"),
    @NamedQuery(name = "Brangos.findByValorMinRn", query = "SELECT b FROM Brangos b WHERE b.valorMinRn = :valorMinRn"),
    @NamedQuery(name = "Brangos.findByCodigoIndiceRn", query = "SELECT b FROM Brangos b WHERE b.brangosPK.codigoIndiceRn = :codigoIndiceRn")})
public class Brangos implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BrangosPK brangosPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR_MIN_RN")
    private BigDecimal valorMinRn;
    @JoinColumn(name = "CODIGO_VARIABLE_RN", referencedColumnName = "CODIGO_VR", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Bvariables bvariables;
    @JoinColumn(name = "CODIGO_SUSTRATO_RN", referencedColumnName = "CODIGO_TIPO_SUSTRATO_TS", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private BtipoSustratos btipoSustratos;

    public Brangos() {
    }

    public Brangos(BrangosPK brangosPK) {
        this.brangosPK = brangosPK;
    }

    public Brangos(String codigoVariableRn, String codigoLegislacionRn, String codigoSustratoRn, String codigoIndiceRn) {
        this.brangosPK = new BrangosPK(codigoVariableRn, codigoLegislacionRn, codigoSustratoRn, codigoIndiceRn);
    }

    public BrangosPK getBrangosPK() {
        return brangosPK;
    }

    public void setBrangosPK(BrangosPK brangosPK) {
        this.brangosPK = brangosPK;
    }

    public BigDecimal getValorMinRn() {
        return valorMinRn;
    }

    public void setValorMinRn(BigDecimal valorMinRn) {
        this.valorMinRn = valorMinRn;
    }

    public Bvariables getBvariables() {
        return bvariables;
    }

    public void setBvariables(Bvariables bvariables) {
        this.bvariables = bvariables;
    }

    public BtipoSustratos getBtipoSustratos() {
        return btipoSustratos;
    }

    public void setBtipoSustratos(BtipoSustratos btipoSustratos) {
        this.btipoSustratos = btipoSustratos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (brangosPK != null ? brangosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Brangos)) {
            return false;
        }
        Brangos other = (Brangos) object;
        if ((this.brangosPK == null && other.brangosPK != null) || (this.brangosPK != null && !this.brangosPK.equals(other.brangosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Brangos[ brangosPK=" + brangosPK + " ]";
    }
    
}
