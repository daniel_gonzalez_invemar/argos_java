/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BLST_PARAMETROS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BlstParametros.findAll", query = "SELECT b FROM BlstParametros b"),
    @NamedQuery(name = "BlstParametros.findByCodigo", query = "SELECT b FROM BlstParametros b WHERE b.codigo = :codigo"),
    @NamedQuery(name = "BlstParametros.findByGrupo", query = "SELECT b FROM BlstParametros b WHERE b.grupo = :grupo"),
    @NamedQuery(name = "BlstParametros.findBySigla", query = "SELECT b FROM BlstParametros b WHERE b.sigla = :sigla"),
    @NamedQuery(name = "BlstParametros.findByDescripcion", query = "SELECT b FROM BlstParametros b WHERE b.descripcion = :descripcion"),
    @NamedQuery(name = "BlstParametros.findByTablaAsociada", query = "SELECT b FROM BlstParametros b WHERE b.tablaAsociada = :tablaAsociada")})
public class BlstParametros implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private Integer codigo;
    @Basic(optional = false)
    @Column(name = "GRUPO")
    private short grupo;
    @Column(name = "SIGLA")
    private String sigla;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "TABLA_ASOCIADA")
    private String tablaAsociada;

    public BlstParametros() {
    }

    public BlstParametros(Integer codigo) {
        this.codigo = codigo;
    }

    public BlstParametros(Integer codigo, short grupo, String descripcion) {
        this.codigo = codigo;
        this.grupo = grupo;
        this.descripcion = descripcion;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public short getGrupo() {
        return grupo;
    }

    public void setGrupo(short grupo) {
        this.grupo = grupo;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTablaAsociada() {
        return tablaAsociada;
    }

    public void setTablaAsociada(String tablaAsociada) {
        this.tablaAsociada = tablaAsociada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BlstParametros)) {
            return false;
        }
        BlstParametros other = (BlstParametros) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BlstParametros[ codigo=" + codigo + " ]";
    }
    
}
