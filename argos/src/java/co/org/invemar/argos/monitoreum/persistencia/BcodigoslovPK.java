/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BcodigoslovPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "TABLA_LOV")
    private int tablaLov;
    @Basic(optional = false)
    @Column(name = "CODIGO_LOV")
    private int codigoLov;

    public BcodigoslovPK() {
    }

    public BcodigoslovPK(int tablaLov, int codigoLov) {
        this.tablaLov = tablaLov;
        this.codigoLov = codigoLov;
    }

    public int getTablaLov() {
        return tablaLov;
    }

    public void setTablaLov(int tablaLov) {
        this.tablaLov = tablaLov;
    }

    public int getCodigoLov() {
        return codigoLov;
    }

    public void setCodigoLov(int codigoLov) {
        this.codigoLov = codigoLov;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) tablaLov;
        hash += (int) codigoLov;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BcodigoslovPK)) {
            return false;
        }
        BcodigoslovPK other = (BcodigoslovPK) object;
        if (this.tablaLov != other.tablaLov) {
            return false;
        }
        if (this.codigoLov != other.codigoLov) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BcodigoslovPK[ tablaLov=" + tablaLov + ", codigoLov=" + codigoLov + " ]";
    }
    
}
