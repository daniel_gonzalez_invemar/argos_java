/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BESPECIES_X_COMPONENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BespeciesXComponente.findAll", query = "SELECT b FROM BespeciesXComponente b"),
    @NamedQuery(name = "BespeciesXComponente.findByCodEspecie", query = "SELECT b FROM BespeciesXComponente b WHERE b.bespeciesXComponentePK.codEspecie = :codEspecie"),
    @NamedQuery(name = "BespeciesXComponente.findByCodComponente", query = "SELECT b FROM BespeciesXComponente b WHERE b.bespeciesXComponentePK.codComponente = :codComponente")})
public class BespeciesXComponente implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BespeciesXComponentePK bespeciesXComponentePK;

    public BespeciesXComponente() {
    }

    public BespeciesXComponente(BespeciesXComponentePK bespeciesXComponentePK) {
        this.bespeciesXComponentePK = bespeciesXComponentePK;
    }

    public BespeciesXComponente(String codEspecie, int codComponente) {
        this.bespeciesXComponentePK = new BespeciesXComponentePK(codEspecie, codComponente);
    }

    public BespeciesXComponentePK getBespeciesXComponentePK() {
        return bespeciesXComponentePK;
    }

    public void setBespeciesXComponentePK(BespeciesXComponentePK bespeciesXComponentePK) {
        this.bespeciesXComponentePK = bespeciesXComponentePK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bespeciesXComponentePK != null ? bespeciesXComponentePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BespeciesXComponente)) {
            return false;
        }
        BespeciesXComponente other = (BespeciesXComponente) object;
        if ((this.bespeciesXComponentePK == null && other.bespeciesXComponentePK != null) || (this.bespeciesXComponentePK != null && !this.bespeciesXComponentePK.equals(other.bespeciesXComponentePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BespeciesXComponente[ bespeciesXComponentePK=" + bespeciesXComponentePK + " ]";
    }
    
}
