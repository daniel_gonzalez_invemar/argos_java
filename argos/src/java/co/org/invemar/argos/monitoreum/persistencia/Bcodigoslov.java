/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BCODIGOSLOV")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bcodigoslov.findAll", query = "SELECT b FROM Bcodigoslov b"),
    @NamedQuery(name = "Bcodigoslov.findByTablaLov", query = "SELECT b FROM Bcodigoslov b WHERE b.bcodigoslovPK.tablaLov = :tablaLov"),
    @NamedQuery(name = "Bcodigoslov.findByCodigoLov", query = "SELECT b FROM Bcodigoslov b WHERE b.bcodigoslovPK.codigoLov = :codigoLov"),
    @NamedQuery(name = "Bcodigoslov.findByDescripcionLov", query = "SELECT b FROM Bcodigoslov b WHERE b.descripcionLov = :descripcionLov"),
    @NamedQuery(name = "Bcodigoslov.findByDescripciontablaLov", query = "SELECT b FROM Bcodigoslov b WHERE b.descripciontablaLov = :descripciontablaLov"),
    @NamedQuery(name = "Bcodigoslov.findByAsociadoconLov", query = "SELECT b FROM Bcodigoslov b WHERE b.asociadoconLov = :asociadoconLov"),
    @NamedQuery(name = "Bcodigoslov.findByDescripcionSiglas", query = "SELECT b FROM Bcodigoslov b WHERE b.descripcionSiglas = :descripcionSiglas"),
    @NamedQuery(name = "Bcodigoslov.findByUploadflag", query = "SELECT b FROM Bcodigoslov b WHERE b.uploadflag = :uploadflag")})
public class Bcodigoslov implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BcodigoslovPK bcodigoslovPK;
    @Column(name = "DESCRIPCION_LOV")
    private String descripcionLov;
    @Column(name = "DESCRIPCIONTABLA_LOV")
    private String descripciontablaLov;
    @Column(name = "ASOCIADOCON_LOV")
    private String asociadoconLov;
    @Column(name = "DESCRIPCION_SIGLAS")
    private String descripcionSiglas;
    @Column(name = "UPLOADFLAG")
    private String uploadflag;

    public Bcodigoslov() {
    }

    public Bcodigoslov(BcodigoslovPK bcodigoslovPK) {
        this.bcodigoslovPK = bcodigoslovPK;
    }

    public Bcodigoslov(int tablaLov, int codigoLov) {
        this.bcodigoslovPK = new BcodigoslovPK(tablaLov, codigoLov);
    }

    public BcodigoslovPK getBcodigoslovPK() {
        return bcodigoslovPK;
    }

    public void setBcodigoslovPK(BcodigoslovPK bcodigoslovPK) {
        this.bcodigoslovPK = bcodigoslovPK;
    }

    public String getDescripcionLov() {
        return descripcionLov;
    }

    public void setDescripcionLov(String descripcionLov) {
        this.descripcionLov = descripcionLov;
    }

    public String getDescripciontablaLov() {
        return descripciontablaLov;
    }

    public void setDescripciontablaLov(String descripciontablaLov) {
        this.descripciontablaLov = descripciontablaLov;
    }

    public String getAsociadoconLov() {
        return asociadoconLov;
    }

    public void setAsociadoconLov(String asociadoconLov) {
        this.asociadoconLov = asociadoconLov;
    }

    public String getDescripcionSiglas() {
        return descripcionSiglas;
    }

    public void setDescripcionSiglas(String descripcionSiglas) {
        this.descripcionSiglas = descripcionSiglas;
    }

    public String getUploadflag() {
        return uploadflag;
    }

    public void setUploadflag(String uploadflag) {
        this.uploadflag = uploadflag;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bcodigoslovPK != null ? bcodigoslovPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bcodigoslov)) {
            return false;
        }
        Bcodigoslov other = (Bcodigoslov) object;
        if ((this.bcodigoslovPK == null && other.bcodigoslovPK != null) || (this.bcodigoslovPK != null && !this.bcodigoslovPK.equals(other.bcodigoslovPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Bcodigoslov[ bcodigoslovPK=" + bcodigoslovPK + " ]";
    }
    
}
