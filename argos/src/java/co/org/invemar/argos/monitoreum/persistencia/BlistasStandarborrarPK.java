/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BlistasStandarborrarPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ID_VARIABLE")
    private String idVariable;
    @Basic(optional = false)
    @Column(name = "ID_VALOR")
    private short idValor;

    public BlistasStandarborrarPK() {
    }

    public BlistasStandarborrarPK(String idVariable, short idValor) {
        this.idVariable = idVariable;
        this.idValor = idValor;
    }

    public String getIdVariable() {
        return idVariable;
    }

    public void setIdVariable(String idVariable) {
        this.idVariable = idVariable;
    }

    public short getIdValor() {
        return idValor;
    }

    public void setIdValor(short idValor) {
        this.idValor = idValor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVariable != null ? idVariable.hashCode() : 0);
        hash += (int) idValor;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BlistasStandarborrarPK)) {
            return false;
        }
        BlistasStandarborrarPK other = (BlistasStandarborrarPK) object;
        if ((this.idVariable == null && other.idVariable != null) || (this.idVariable != null && !this.idVariable.equals(other.idVariable))) {
            return false;
        }
        if (this.idValor != other.idValor) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BlistasStandarborrarPK[ idVariable=" + idVariable + ", idValor=" + idValor + " ]";
    }
    
}
