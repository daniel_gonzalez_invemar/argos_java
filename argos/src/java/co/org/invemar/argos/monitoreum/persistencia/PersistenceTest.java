/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author usrsig15
 */
public class PersistenceTest {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("argosPU");
        EntityManager em = emf.createEntityManager();
      
        TypedQuery<BproyectoXComp> query = em.createNamedQuery("BproyectoXComp.findByIdProyecto", BproyectoXComp.class);
        query.setParameter("idProyecto", 2239);        
        List<BproyectoXComp> results = query.getResultList();     
        Iterator<BproyectoXComp> it = results.iterator();

        while (it.hasNext()) {
           BproyectoXComp pc = it.next();
           System.out.println(" pc:"+pc.getBubicacionTematica().getDescripcion());
        }
        
    }
}
