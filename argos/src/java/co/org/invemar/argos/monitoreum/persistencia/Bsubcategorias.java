/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BSUBCATEGORIAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bsubcategorias.findAll", query = "SELECT b FROM Bsubcategorias b"),
    @NamedQuery(name = "Bsubcategorias.findByCodSubcategoria", query = "SELECT b FROM Bsubcategorias b WHERE b.codSubcategoria = :codSubcategoria"),
    @NamedQuery(name = "Bsubcategorias.findByNombre", query = "SELECT b FROM Bsubcategorias b WHERE b.nombre = :nombre"),
    @NamedQuery(name = "Bsubcategorias.findByDescripcion", query = "SELECT b FROM Bsubcategorias b WHERE b.descripcion = :descripcion")})
public class Bsubcategorias implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "COD_SUBCATEGORIA")
    private Short codSubcategoria;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @JoinColumn(name = "COD_CATEGORIA", referencedColumnName = "COD_CATEGORIA")
    @ManyToOne(optional = false)
    private Bcategorias codCategoria;

    public Bsubcategorias() {
    }

    public Bsubcategorias(Short codSubcategoria) {
        this.codSubcategoria = codSubcategoria;
    }

    public Bsubcategorias(Short codSubcategoria, String nombre) {
        this.codSubcategoria = codSubcategoria;
        this.nombre = nombre;
    }

    public Short getCodSubcategoria() {
        return codSubcategoria;
    }

    public void setCodSubcategoria(Short codSubcategoria) {
        this.codSubcategoria = codSubcategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Bcategorias getCodCategoria() {
        return codCategoria;
    }

    public void setCodCategoria(Bcategorias codCategoria) {
        this.codCategoria = codCategoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codSubcategoria != null ? codSubcategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bsubcategorias)) {
            return false;
        }
        Bsubcategorias other = (Bsubcategorias) object;
        if ((this.codSubcategoria == null && other.codSubcategoria != null) || (this.codSubcategoria != null && !this.codSubcategoria.equals(other.codSubcategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Bsubcategorias[ codSubcategoria=" + codSubcategoria + " ]";
    }
    
}
