/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BgrupoTematicosPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ID_COMPONENTE")
    private short idComponente;
    @Basic(optional = false)
    @Column(name = "ID_GRUPO")
    private short idGrupo;

    public BgrupoTematicosPK() {
    }

    public BgrupoTematicosPK(short idComponente, short idGrupo) {
        this.idComponente = idComponente;
        this.idGrupo = idGrupo;
    }

    public short getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(short idComponente) {
        this.idComponente = idComponente;
    }

    public short getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(short idGrupo) {
        this.idGrupo = idGrupo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idComponente;
        hash += (int) idGrupo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BgrupoTematicosPK)) {
            return false;
        }
        BgrupoTematicosPK other = (BgrupoTematicosPK) object;
        if (this.idComponente != other.idComponente) {
            return false;
        }
        if (this.idGrupo != other.idGrupo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BgrupoTematicosPK[ idComponente=" + idComponente + ", idGrupo=" + idGrupo + " ]";
    }
    
}
