/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BTIPO_VARIABLES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BtipoVariables.findAll", query = "SELECT b FROM BtipoVariables b"),
    @NamedQuery(name = "BtipoVariables.findByCodigoTp", query = "SELECT b FROM BtipoVariables b WHERE b.codigoTp = :codigoTp"),
    @NamedQuery(name = "BtipoVariables.findByDescripcionTp", query = "SELECT b FROM BtipoVariables b WHERE b.descripcionTp = :descripcionTp"),
    @NamedQuery(name = "BtipoVariables.findByIdGrupo", query = "SELECT b FROM BtipoVariables b WHERE b.idGrupo = :idGrupo")})
public class BtipoVariables implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO_TP")
    private String codigoTp;
    @Column(name = "DESCRIPCION_TP")
    private String descripcionTp;
    @Column(name = "ID_GRUPO")
    private Short idGrupo;
    @JoinColumn(name = "ID_COMPONENTE", referencedColumnName = "ID_COMPONENTE")
    @ManyToOne
    private Bcomponentes idComponente;

    public BtipoVariables() {
    }

    public BtipoVariables(String codigoTp) {
        this.codigoTp = codigoTp;
    }

    public String getCodigoTp() {
        return codigoTp;
    }

    public void setCodigoTp(String codigoTp) {
        this.codigoTp = codigoTp;
    }

    public String getDescripcionTp() {
        return descripcionTp;
    }

    public void setDescripcionTp(String descripcionTp) {
        this.descripcionTp = descripcionTp;
    }

    public Short getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Short idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Bcomponentes getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(Bcomponentes idComponente) {
        this.idComponente = idComponente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoTp != null ? codigoTp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BtipoVariables)) {
            return false;
        }
        BtipoVariables other = (BtipoVariables) object;
        if ((this.codigoTp == null && other.codigoTp != null) || (this.codigoTp != null && !this.codigoTp.equals(other.codigoTp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BtipoVariables[ codigoTp=" + codigoTp + " ]";
    }
    
}
