/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BvalidacionesPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ID_VARIABLE")
    private String idVariable;
    @Basic(optional = false)
    @Column(name = "ID_UNIDAD")
    private short idUnidad;

    public BvalidacionesPK() {
    }

    public BvalidacionesPK(String idVariable, short idUnidad) {
        this.idVariable = idVariable;
        this.idUnidad = idUnidad;
    }

    public String getIdVariable() {
        return idVariable;
    }

    public void setIdVariable(String idVariable) {
        this.idVariable = idVariable;
    }

    public short getIdUnidad() {
        return idUnidad;
    }

    public void setIdUnidad(short idUnidad) {
        this.idUnidad = idUnidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVariable != null ? idVariable.hashCode() : 0);
        hash += (int) idUnidad;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BvalidacionesPK)) {
            return false;
        }
        BvalidacionesPK other = (BvalidacionesPK) object;
        if ((this.idVariable == null && other.idVariable != null) || (this.idVariable != null && !this.idVariable.equals(other.idVariable))) {
            return false;
        }
        if (this.idUnidad != other.idUnidad) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BvalidacionesPK[ idVariable=" + idVariable + ", idUnidad=" + idUnidad + " ]";
    }
    
}
