/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BrangosPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "CODIGO_VARIABLE_RN")
    private String codigoVariableRn;
    @Basic(optional = false)
    @Column(name = "CODIGO_LEGISLACION_RN")
    private String codigoLegislacionRn;
    @Basic(optional = false)
    @Column(name = "CODIGO_SUSTRATO_RN")
    private String codigoSustratoRn;
    @Basic(optional = false)
    @Column(name = "CODIGO_INDICE_RN")
    private String codigoIndiceRn;

    public BrangosPK() {
    }

    public BrangosPK(String codigoVariableRn, String codigoLegislacionRn, String codigoSustratoRn, String codigoIndiceRn) {
        this.codigoVariableRn = codigoVariableRn;
        this.codigoLegislacionRn = codigoLegislacionRn;
        this.codigoSustratoRn = codigoSustratoRn;
        this.codigoIndiceRn = codigoIndiceRn;
    }

    public String getCodigoVariableRn() {
        return codigoVariableRn;
    }

    public void setCodigoVariableRn(String codigoVariableRn) {
        this.codigoVariableRn = codigoVariableRn;
    }

    public String getCodigoLegislacionRn() {
        return codigoLegislacionRn;
    }

    public void setCodigoLegislacionRn(String codigoLegislacionRn) {
        this.codigoLegislacionRn = codigoLegislacionRn;
    }

    public String getCodigoSustratoRn() {
        return codigoSustratoRn;
    }

    public void setCodigoSustratoRn(String codigoSustratoRn) {
        this.codigoSustratoRn = codigoSustratoRn;
    }

    public String getCodigoIndiceRn() {
        return codigoIndiceRn;
    }

    public void setCodigoIndiceRn(String codigoIndiceRn) {
        this.codigoIndiceRn = codigoIndiceRn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoVariableRn != null ? codigoVariableRn.hashCode() : 0);
        hash += (codigoLegislacionRn != null ? codigoLegislacionRn.hashCode() : 0);
        hash += (codigoSustratoRn != null ? codigoSustratoRn.hashCode() : 0);
        hash += (codigoIndiceRn != null ? codigoIndiceRn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BrangosPK)) {
            return false;
        }
        BrangosPK other = (BrangosPK) object;
        if ((this.codigoVariableRn == null && other.codigoVariableRn != null) || (this.codigoVariableRn != null && !this.codigoVariableRn.equals(other.codigoVariableRn))) {
            return false;
        }
        if ((this.codigoLegislacionRn == null && other.codigoLegislacionRn != null) || (this.codigoLegislacionRn != null && !this.codigoLegislacionRn.equals(other.codigoLegislacionRn))) {
            return false;
        }
        if ((this.codigoSustratoRn == null && other.codigoSustratoRn != null) || (this.codigoSustratoRn != null && !this.codigoSustratoRn.equals(other.codigoSustratoRn))) {
            return false;
        }
        if ((this.codigoIndiceRn == null && other.codigoIndiceRn != null) || (this.codigoIndiceRn != null && !this.codigoIndiceRn.equals(other.codigoIndiceRn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BrangosPK[ codigoVariableRn=" + codigoVariableRn + ", codigoLegislacionRn=" + codigoLegislacionRn + ", codigoSustratoRn=" + codigoSustratoRn + ", codigoIndiceRn=" + codigoIndiceRn + " ]";
    }
    
}
