/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BESPECIES_X_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BespeciesXProyecto.findAll", query = "SELECT b FROM BespeciesXProyecto b"),
    @NamedQuery(name = "BespeciesXProyecto.findByCodEspecie", query = "SELECT b FROM BespeciesXProyecto b WHERE b.bespeciesXProyectoPK.codEspecie = :codEspecie"),
    @NamedQuery(name = "BespeciesXProyecto.findByCodEspecieLetras", query = "SELECT b FROM BespeciesXProyecto b WHERE b.bespeciesXProyectoPK.codEspecieLetras = :codEspecieLetras"),
    @NamedQuery(name = "BespeciesXProyecto.findByCodProyecto", query = "SELECT b FROM BespeciesXProyecto b WHERE b.bespeciesXProyectoPK.codProyecto = :codProyecto")})
public class BespeciesXProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BespeciesXProyectoPK bespeciesXProyectoPK;

    public BespeciesXProyecto() {
    }

    public BespeciesXProyecto(BespeciesXProyectoPK bespeciesXProyectoPK) {
        this.bespeciesXProyectoPK = bespeciesXProyectoPK;
    }

    public BespeciesXProyecto(String codEspecie, String codEspecieLetras, BigInteger codProyecto) {
        this.bespeciesXProyectoPK = new BespeciesXProyectoPK(codEspecie, codEspecieLetras, codProyecto);
    }

    public BespeciesXProyectoPK getBespeciesXProyectoPK() {
        return bespeciesXProyectoPK;
    }

    public void setBespeciesXProyectoPK(BespeciesXProyectoPK bespeciesXProyectoPK) {
        this.bespeciesXProyectoPK = bespeciesXProyectoPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bespeciesXProyectoPK != null ? bespeciesXProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BespeciesXProyecto)) {
            return false;
        }
        BespeciesXProyecto other = (BespeciesXProyecto) object;
        if ((this.bespeciesXProyectoPK == null && other.bespeciesXProyectoPK != null) || (this.bespeciesXProyectoPK != null && !this.bespeciesXProyectoPK.equals(other.bespeciesXProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BespeciesXProyecto[ bespeciesXProyectoPK=" + bespeciesXProyectoPK + " ]";
    }
    
}
