/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BCATEGORIAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bcategorias.findAll", query = "SELECT b FROM Bcategorias b"),
    @NamedQuery(name = "Bcategorias.findByCodCategoria", query = "SELECT b FROM Bcategorias b WHERE b.codCategoria = :codCategoria"),
    @NamedQuery(name = "Bcategorias.findByNombre", query = "SELECT b FROM Bcategorias b WHERE b.nombre = :nombre"),
    @NamedQuery(name = "Bcategorias.findByDescripcion", query = "SELECT b FROM Bcategorias b WHERE b.descripcion = :descripcion")})
public class Bcategorias implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "COD_CATEGORIA")
    private Short codCategoria;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codCategoria")
    private List<Bsubcategorias> bsubcategoriasList;

    public Bcategorias() {
    }

    public Bcategorias(Short codCategoria) {
        this.codCategoria = codCategoria;
    }

    public Bcategorias(Short codCategoria, String nombre) {
        this.codCategoria = codCategoria;
        this.nombre = nombre;
    }

    public Short getCodCategoria() {
        return codCategoria;
    }

    public void setCodCategoria(Short codCategoria) {
        this.codCategoria = codCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Bsubcategorias> getBsubcategoriasList() {
        return bsubcategoriasList;
    }

    public void setBsubcategoriasList(List<Bsubcategorias> bsubcategoriasList) {
        this.bsubcategoriasList = bsubcategoriasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codCategoria != null ? codCategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bcategorias)) {
            return false;
        }
        Bcategorias other = (Bcategorias) object;
        if ((this.codCategoria == null && other.codCategoria != null) || (this.codCategoria != null && !this.codCategoria.equals(other.codCategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Bcategorias[ codCategoria=" + codCategoria + " ]";
    }
    
}
