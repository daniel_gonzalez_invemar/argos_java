/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BespeciesXComponentePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "COD_ESPECIE")
    private String codEspecie;
    @Basic(optional = false)
    @Column(name = "COD_COMPONENTE")
    private int codComponente;

    public BespeciesXComponentePK() {
    }

    public BespeciesXComponentePK(String codEspecie, int codComponente) {
        this.codEspecie = codEspecie;
        this.codComponente = codComponente;
    }

    public String getCodEspecie() {
        return codEspecie;
    }

    public void setCodEspecie(String codEspecie) {
        this.codEspecie = codEspecie;
    }

    public int getCodComponente() {
        return codComponente;
    }

    public void setCodComponente(int codComponente) {
        this.codComponente = codComponente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEspecie != null ? codEspecie.hashCode() : 0);
        hash += (int) codComponente;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BespeciesXComponentePK)) {
            return false;
        }
        BespeciesXComponentePK other = (BespeciesXComponentePK) object;
        if ((this.codEspecie == null && other.codEspecie != null) || (this.codEspecie != null && !this.codEspecie.equals(other.codEspecie))) {
            return false;
        }
        if (this.codComponente != other.codComponente) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BespeciesXComponentePK[ codEspecie=" + codEspecie + ", codComponente=" + codComponente + " ]";
    }
    
}
