/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BunidadVariablesPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "CODIGO_UM_UV")
    private short codigoUmUv;
    @Basic(optional = false)
    @Column(name = "CODIGO_VR_UV")
    private String codigoVrUv;
    @Basic(optional = false)
    @Column(name = "CODIGO_TS_UV")
    private String codigoTsUv;

    public BunidadVariablesPK() {
    }

    public BunidadVariablesPK(short codigoUmUv, String codigoVrUv, String codigoTsUv) {
        this.codigoUmUv = codigoUmUv;
        this.codigoVrUv = codigoVrUv;
        this.codigoTsUv = codigoTsUv;
    }

    public short getCodigoUmUv() {
        return codigoUmUv;
    }

    public void setCodigoUmUv(short codigoUmUv) {
        this.codigoUmUv = codigoUmUv;
    }

    public String getCodigoVrUv() {
        return codigoVrUv;
    }

    public void setCodigoVrUv(String codigoVrUv) {
        this.codigoVrUv = codigoVrUv;
    }

    public String getCodigoTsUv() {
        return codigoTsUv;
    }

    public void setCodigoTsUv(String codigoTsUv) {
        this.codigoTsUv = codigoTsUv;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codigoUmUv;
        hash += (codigoVrUv != null ? codigoVrUv.hashCode() : 0);
        hash += (codigoTsUv != null ? codigoTsUv.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BunidadVariablesPK)) {
            return false;
        }
        BunidadVariablesPK other = (BunidadVariablesPK) object;
        if (this.codigoUmUv != other.codigoUmUv) {
            return false;
        }
        if ((this.codigoVrUv == null && other.codigoVrUv != null) || (this.codigoVrUv != null && !this.codigoVrUv.equals(other.codigoVrUv))) {
            return false;
        }
        if ((this.codigoTsUv == null && other.codigoTsUv != null) || (this.codigoTsUv != null && !this.codigoTsUv.equals(other.codigoTsUv))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BunidadVariablesPK[ codigoUmUv=" + codigoUmUv + ", codigoVrUv=" + codigoVrUv + ", codigoTsUv=" + codigoTsUv + " ]";
    }
    
}
