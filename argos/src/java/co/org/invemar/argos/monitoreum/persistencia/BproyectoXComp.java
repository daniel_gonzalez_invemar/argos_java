/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BPROYECTO_X_COMP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BproyectoXComp.findAll", query = "SELECT b FROM BproyectoXComp b"),
    @NamedQuery(name = "BproyectoXComp.findByIdProyecto", query = "SELECT b FROM BproyectoXComp b WHERE b.bproyectoXCompPK.idProyecto = :idProyecto"),
    @NamedQuery(name = "BproyectoXComp.findByIdComponente", query = "SELECT b FROM BproyectoXComp b WHERE b.bproyectoXCompPK.idComponente = :idComponente")})
public class BproyectoXComp implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BproyectoXCompPK bproyectoXCompPK;
    @JoinColumn(name = "ID_COMPONENTE", referencedColumnName = "CODIGO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private BubicacionTematica bubicacionTematica;

    public BproyectoXComp() {
    }

    public BproyectoXComp(BproyectoXCompPK bproyectoXCompPK) {
        this.bproyectoXCompPK = bproyectoXCompPK;
    }

    public BproyectoXComp(BigInteger idProyecto, int idComponente) {
        this.bproyectoXCompPK = new BproyectoXCompPK(idProyecto, idComponente);
    }

    public BproyectoXCompPK getBproyectoXCompPK() {
        return bproyectoXCompPK;
    }

    public void setBproyectoXCompPK(BproyectoXCompPK bproyectoXCompPK) {
        this.bproyectoXCompPK = bproyectoXCompPK;
    }

    public BubicacionTematica getBubicacionTematica() {
        return bubicacionTematica;
    }

    public void setBubicacionTematica(BubicacionTematica bubicacionTematica) {
        this.bubicacionTematica = bubicacionTematica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bproyectoXCompPK != null ? bproyectoXCompPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BproyectoXComp)) {
            return false;
        }
        BproyectoXComp other = (BproyectoXComp) object;
        if ((this.bproyectoXCompPK == null && other.bproyectoXCompPK != null) || (this.bproyectoXCompPK != null && !this.bproyectoXCompPK.equals(other.bproyectoXCompPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BproyectoXComp[ bproyectoXCompPK=" + bproyectoXCompPK + " ]";
    }
    
}
