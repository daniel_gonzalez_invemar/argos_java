/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BCOMPONENTES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bcomponentes.findAll", query = "SELECT b FROM Bcomponentes b"),
    @NamedQuery(name = "Bcomponentes.findByIdComponente", query = "SELECT b FROM Bcomponentes b WHERE b.idComponente = :idComponente"),
    @NamedQuery(name = "Bcomponentes.findByNombre", query = "SELECT b FROM Bcomponentes b WHERE b.nombre = :nombre"),
    @NamedQuery(name = "Bcomponentes.findByDescripcion", query = "SELECT b FROM Bcomponentes b WHERE b.descripcion = :descripcion")})
public class Bcomponentes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_COMPONENTE")
    private Short idComponente;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(mappedBy = "idComponente")
    private List<BtipoVariables> btipoVariablesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bcomponentes")
    private List<BgrupoTematicos> bgrupoTematicosList;
    @OneToMany(mappedBy = "idComponente")
    private List<Btemporadas> btemporadasList;

    public Bcomponentes() {
    }

    public Bcomponentes(Short idComponente) {
        this.idComponente = idComponente;
    }

    public Short getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(Short idComponente) {
        this.idComponente = idComponente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<BtipoVariables> getBtipoVariablesList() {
        return btipoVariablesList;
    }

    public void setBtipoVariablesList(List<BtipoVariables> btipoVariablesList) {
        this.btipoVariablesList = btipoVariablesList;
    }

    @XmlTransient
    public List<BgrupoTematicos> getBgrupoTematicosList() {
        return bgrupoTematicosList;
    }

    public void setBgrupoTematicosList(List<BgrupoTematicos> bgrupoTematicosList) {
        this.bgrupoTematicosList = bgrupoTematicosList;
    }

    @XmlTransient
    public List<Btemporadas> getBtemporadasList() {
        return btemporadasList;
    }

    public void setBtemporadasList(List<Btemporadas> btemporadasList) {
        this.btemporadasList = btemporadasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComponente != null ? idComponente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bcomponentes)) {
            return false;
        }
        Bcomponentes other = (Bcomponentes) object;
        if ((this.idComponente == null && other.idComponente != null) || (this.idComponente != null && !this.idComponente.equals(other.idComponente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Bcomponentes[ idComponente=" + idComponente + " ]";
    }
    
}
