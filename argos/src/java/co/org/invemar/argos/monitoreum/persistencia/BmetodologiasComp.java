/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BMETODOLOGIAS_COMP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmetodologiasComp.findAll", query = "SELECT b FROM BmetodologiasComp b"),
    @NamedQuery(name = "BmetodologiasComp.findByIdMetodologia", query = "SELECT b FROM BmetodologiasComp b WHERE b.bmetodologiasCompPK.idMetodologia = :idMetodologia"),
    @NamedQuery(name = "BmetodologiasComp.findByIdComponente", query = "SELECT b FROM BmetodologiasComp b WHERE b.bmetodologiasCompPK.idComponente = :idComponente"),
    @NamedQuery(name = "BmetodologiasComp.findByIdProyecto", query = "SELECT b FROM BmetodologiasComp b WHERE b.idProyecto = :idProyecto"),
    @NamedQuery(name = "BmetodologiasComp.findByParametroBaseUno", query = "SELECT b FROM BmetodologiasComp b WHERE b.parametroBaseUno = :parametroBaseUno"),
    @NamedQuery(name = "BmetodologiasComp.findByNotas", query = "SELECT b FROM BmetodologiasComp b WHERE b.notas = :notas")})
public class BmetodologiasComp implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BmetodologiasCompPK bmetodologiasCompPK;
    @Column(name = "ID_PROYECTO")
    private String idProyecto;
    @Column(name = "PARAMETRO_BASE_UNO")
    private Long parametroBaseUno;
    @Column(name = "NOTAS")
    private String notas;
    @JoinColumn(name = "ID_COMPONENTE", referencedColumnName = "CODIGO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private BubicacionTematica bubicacionTematica;
    @JoinColumn(name = "ID_METODOLOGIA", referencedColumnName = "ID_METODOLOGIA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Bmetodologias bmetodologias;

    public BmetodologiasComp() {
    }

    public BmetodologiasComp(BmetodologiasCompPK bmetodologiasCompPK) {
        this.bmetodologiasCompPK = bmetodologiasCompPK;
    }

    public BmetodologiasComp(short idMetodologia, int idComponente) {
        this.bmetodologiasCompPK = new BmetodologiasCompPK(idMetodologia, idComponente);
    }

    public BmetodologiasCompPK getBmetodologiasCompPK() {
        return bmetodologiasCompPK;
    }

    public void setBmetodologiasCompPK(BmetodologiasCompPK bmetodologiasCompPK) {
        this.bmetodologiasCompPK = bmetodologiasCompPK;
    }

    public String getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Long getParametroBaseUno() {
        return parametroBaseUno;
    }

    public void setParametroBaseUno(Long parametroBaseUno) {
        this.parametroBaseUno = parametroBaseUno;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public BubicacionTematica getBubicacionTematica() {
        return bubicacionTematica;
    }

    public void setBubicacionTematica(BubicacionTematica bubicacionTematica) {
        this.bubicacionTematica = bubicacionTematica;
    }

    public Bmetodologias getBmetodologias() {
        return bmetodologias;
    }

    public void setBmetodologias(Bmetodologias bmetodologias) {
        this.bmetodologias = bmetodologias;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bmetodologiasCompPK != null ? bmetodologiasCompPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmetodologiasComp)) {
            return false;
        }
        BmetodologiasComp other = (BmetodologiasComp) object;
        if ((this.bmetodologiasCompPK == null && other.bmetodologiasCompPK != null) || (this.bmetodologiasCompPK != null && !this.bmetodologiasCompPK.equals(other.bmetodologiasCompPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BmetodologiasComp[ bmetodologiasCompPK=" + bmetodologiasCompPK + " ]";
    }
    
}
