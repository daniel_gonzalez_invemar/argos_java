/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BVARIABLES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bvariables.findAll", query = "SELECT b FROM Bvariables b"),
    @NamedQuery(name = "Bvariables.findByTipoVr", query = "SELECT b FROM Bvariables b WHERE b.tipoVr = :tipoVr"),
    @NamedQuery(name = "Bvariables.findByCodigoVr", query = "SELECT b FROM Bvariables b WHERE b.codigoVr = :codigoVr"),
    @NamedQuery(name = "Bvariables.findByNombreVr", query = "SELECT b FROM Bvariables b WHERE b.nombreVr = :nombreVr"),
    @NamedQuery(name = "Bvariables.findBySiglaVr", query = "SELECT b FROM Bvariables b WHERE b.siglaVr = :siglaVr"),
    @NamedQuery(name = "Bvariables.findByVigenciaVr", query = "SELECT b FROM Bvariables b WHERE b.vigenciaVr = :vigenciaVr")})
public class Bvariables implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "TIPO_VR")
    private String tipoVr;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO_VR")
    private String codigoVr;
    @Basic(optional = false)
    @Column(name = "NOMBRE_VR")
    private String nombreVr;
    @Basic(optional = false)
    @Column(name = "SIGLA_VR")
    private String siglaVr;
    @Column(name = "VIGENCIA_VR")
    private Character vigenciaVr;
    @ManyToMany(mappedBy = "bvariablesList")
    private List<BubicacionTematica> bubicacionTematicaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bvariables")
    private List<Brangos> brangosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bvariables")
    private List<BunidadVariables> bunidadVariablesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bvariables")
    private List<BmetodoVariables> bmetodoVariablesList;

    public Bvariables() {
    }

    public Bvariables(String codigoVr) {
        this.codigoVr = codigoVr;
    }

    public Bvariables(String codigoVr, String tipoVr, String nombreVr, String siglaVr) {
        this.codigoVr = codigoVr;
        this.tipoVr = tipoVr;
        this.nombreVr = nombreVr;
        this.siglaVr = siglaVr;
    }

    public String getTipoVr() {
        return tipoVr;
    }

    public void setTipoVr(String tipoVr) {
        this.tipoVr = tipoVr;
    }

    public String getCodigoVr() {
        return codigoVr;
    }

    public void setCodigoVr(String codigoVr) {
        this.codigoVr = codigoVr;
    }

    public String getNombreVr() {
        return nombreVr;
    }

    public void setNombreVr(String nombreVr) {
        this.nombreVr = nombreVr;
    }

    public String getSiglaVr() {
        return siglaVr;
    }

    public void setSiglaVr(String siglaVr) {
        this.siglaVr = siglaVr;
    }

    public Character getVigenciaVr() {
        return vigenciaVr;
    }

    public void setVigenciaVr(Character vigenciaVr) {
        this.vigenciaVr = vigenciaVr;
    }

    @XmlTransient
    public List<BubicacionTematica> getBubicacionTematicaList() {
        return bubicacionTematicaList;
    }

    public void setBubicacionTematicaList(List<BubicacionTematica> bubicacionTematicaList) {
        this.bubicacionTematicaList = bubicacionTematicaList;
    }

    @XmlTransient
    public List<Brangos> getBrangosList() {
        return brangosList;
    }

    public void setBrangosList(List<Brangos> brangosList) {
        this.brangosList = brangosList;
    }

    @XmlTransient
    public List<BunidadVariables> getBunidadVariablesList() {
        return bunidadVariablesList;
    }

    public void setBunidadVariablesList(List<BunidadVariables> bunidadVariablesList) {
        this.bunidadVariablesList = bunidadVariablesList;
    }

    @XmlTransient
    public List<BmetodoVariables> getBmetodoVariablesList() {
        return bmetodoVariablesList;
    }

    public void setBmetodoVariablesList(List<BmetodoVariables> bmetodoVariablesList) {
        this.bmetodoVariablesList = bmetodoVariablesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoVr != null ? codigoVr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bvariables)) {
            return false;
        }
        Bvariables other = (Bvariables) object;
        if ((this.codigoVr == null && other.codigoVr != null) || (this.codigoVr != null && !this.codigoVr.equals(other.codigoVr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Bvariables[ codigoVr=" + codigoVr + " ]";
    }
    
}
