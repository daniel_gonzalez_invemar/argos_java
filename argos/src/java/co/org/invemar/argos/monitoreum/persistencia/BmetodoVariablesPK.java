/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BmetodoVariablesPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "COD_VARIABLE_MV")
    private String codVariableMv;
    @Basic(optional = false)
    @Column(name = "COD_METODO_MV")
    private String codMetodoMv;

    public BmetodoVariablesPK() {
    }

    public BmetodoVariablesPK(String codVariableMv, String codMetodoMv) {
        this.codVariableMv = codVariableMv;
        this.codMetodoMv = codMetodoMv;
    }

    public String getCodVariableMv() {
        return codVariableMv;
    }

    public void setCodVariableMv(String codVariableMv) {
        this.codVariableMv = codVariableMv;
    }

    public String getCodMetodoMv() {
        return codMetodoMv;
    }

    public void setCodMetodoMv(String codMetodoMv) {
        this.codMetodoMv = codMetodoMv;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codVariableMv != null ? codVariableMv.hashCode() : 0);
        hash += (codMetodoMv != null ? codMetodoMv.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmetodoVariablesPK)) {
            return false;
        }
        BmetodoVariablesPK other = (BmetodoVariablesPK) object;
        if ((this.codVariableMv == null && other.codVariableMv != null) || (this.codVariableMv != null && !this.codVariableMv.equals(other.codVariableMv))) {
            return false;
        }
        if ((this.codMetodoMv == null && other.codMetodoMv != null) || (this.codMetodoMv != null && !this.codMetodoMv.equals(other.codMetodoMv))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BmetodoVariablesPK[ codVariableMv=" + codVariableMv + ", codMetodoMv=" + codMetodoMv + " ]";
    }
    
}
