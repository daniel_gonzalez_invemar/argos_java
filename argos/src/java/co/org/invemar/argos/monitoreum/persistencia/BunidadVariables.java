/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BUNIDAD_VARIABLES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BunidadVariables.findAll", query = "SELECT b FROM BunidadVariables b"),
    @NamedQuery(name = "BunidadVariables.findByCodigoUmUv", query = "SELECT b FROM BunidadVariables b WHERE b.bunidadVariablesPK.codigoUmUv = :codigoUmUv"),
    @NamedQuery(name = "BunidadVariables.findByCodigoVrUv", query = "SELECT b FROM BunidadVariables b WHERE b.bunidadVariablesPK.codigoVrUv = :codigoVrUv"),
    @NamedQuery(name = "BunidadVariables.findByCodigoTsUv", query = "SELECT b FROM BunidadVariables b WHERE b.bunidadVariablesPK.codigoTsUv = :codigoTsUv"),
    @NamedQuery(name = "BunidadVariables.findByLimInfUv", query = "SELECT b FROM BunidadVariables b WHERE b.limInfUv = :limInfUv"),
    @NamedQuery(name = "BunidadVariables.findByLimSupUv", query = "SELECT b FROM BunidadVariables b WHERE b.limSupUv = :limSupUv"),
    @NamedQuery(name = "BunidadVariables.findByObservacionUv", query = "SELECT b FROM BunidadVariables b WHERE b.observacionUv = :observacionUv")})
public class BunidadVariables implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BunidadVariablesPK bunidadVariablesPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "LIM_INF_UV")
    private BigDecimal limInfUv;
    @Column(name = "LIM_SUP_UV")
    private BigDecimal limSupUv;
    @Column(name = "OBSERVACION_UV")
    private String observacionUv;
    @JoinColumn(name = "CODIGO_VR_UV", referencedColumnName = "CODIGO_VR", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Bvariables bvariables;
    @JoinColumn(name = "CODIGO_UM_UV", referencedColumnName = "CODIGO_UM", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private BunidadDeMedidas bunidadDeMedidas;

    public BunidadVariables() {
    }

    public BunidadVariables(BunidadVariablesPK bunidadVariablesPK) {
        this.bunidadVariablesPK = bunidadVariablesPK;
    }

    public BunidadVariables(short codigoUmUv, String codigoVrUv, String codigoTsUv) {
        this.bunidadVariablesPK = new BunidadVariablesPK(codigoUmUv, codigoVrUv, codigoTsUv);
    }

    public BunidadVariablesPK getBunidadVariablesPK() {
        return bunidadVariablesPK;
    }

    public void setBunidadVariablesPK(BunidadVariablesPK bunidadVariablesPK) {
        this.bunidadVariablesPK = bunidadVariablesPK;
    }

    public BigDecimal getLimInfUv() {
        return limInfUv;
    }

    public void setLimInfUv(BigDecimal limInfUv) {
        this.limInfUv = limInfUv;
    }

    public BigDecimal getLimSupUv() {
        return limSupUv;
    }

    public void setLimSupUv(BigDecimal limSupUv) {
        this.limSupUv = limSupUv;
    }

    public String getObservacionUv() {
        return observacionUv;
    }

    public void setObservacionUv(String observacionUv) {
        this.observacionUv = observacionUv;
    }

    public Bvariables getBvariables() {
        return bvariables;
    }

    public void setBvariables(Bvariables bvariables) {
        this.bvariables = bvariables;
    }

    public BunidadDeMedidas getBunidadDeMedidas() {
        return bunidadDeMedidas;
    }

    public void setBunidadDeMedidas(BunidadDeMedidas bunidadDeMedidas) {
        this.bunidadDeMedidas = bunidadDeMedidas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bunidadVariablesPK != null ? bunidadVariablesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BunidadVariables)) {
            return false;
        }
        BunidadVariables other = (BunidadVariables) object;
        if ((this.bunidadVariablesPK == null && other.bunidadVariablesPK != null) || (this.bunidadVariablesPK != null && !this.bunidadVariablesPK.equals(other.bunidadVariablesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BunidadVariables[ bunidadVariablesPK=" + bunidadVariablesPK + " ]";
    }
    
}
