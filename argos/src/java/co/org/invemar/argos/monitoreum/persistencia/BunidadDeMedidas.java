/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BUNIDAD_DE_MEDIDAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BunidadDeMedidas.findAll", query = "SELECT b FROM BunidadDeMedidas b"),
    @NamedQuery(name = "BunidadDeMedidas.findByCodigoUm", query = "SELECT b FROM BunidadDeMedidas b WHERE b.codigoUm = :codigoUm"),
    @NamedQuery(name = "BunidadDeMedidas.findByDetalleUm", query = "SELECT b FROM BunidadDeMedidas b WHERE b.detalleUm = :detalleUm")})
public class BunidadDeMedidas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO_UM")
    private Short codigoUm;
    @Basic(optional = false)
    @Column(name = "DETALLE_UM")
    private String detalleUm;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bunidadDeMedidas")
    private List<BunidadVariables> bunidadVariablesList;

    public BunidadDeMedidas() {
    }

    public BunidadDeMedidas(Short codigoUm) {
        this.codigoUm = codigoUm;
    }

    public BunidadDeMedidas(Short codigoUm, String detalleUm) {
        this.codigoUm = codigoUm;
        this.detalleUm = detalleUm;
    }

    public Short getCodigoUm() {
        return codigoUm;
    }

    public void setCodigoUm(Short codigoUm) {
        this.codigoUm = codigoUm;
    }

    public String getDetalleUm() {
        return detalleUm;
    }

    public void setDetalleUm(String detalleUm) {
        this.detalleUm = detalleUm;
    }

    @XmlTransient
    public List<BunidadVariables> getBunidadVariablesList() {
        return bunidadVariablesList;
    }

    public void setBunidadVariablesList(List<BunidadVariables> bunidadVariablesList) {
        this.bunidadVariablesList = bunidadVariablesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoUm != null ? codigoUm.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BunidadDeMedidas)) {
            return false;
        }
        BunidadDeMedidas other = (BunidadDeMedidas) object;
        if ((this.codigoUm == null && other.codigoUm != null) || (this.codigoUm != null && !this.codigoUm.equals(other.codigoUm))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BunidadDeMedidas[ codigoUm=" + codigoUm + " ]";
    }
    
}
