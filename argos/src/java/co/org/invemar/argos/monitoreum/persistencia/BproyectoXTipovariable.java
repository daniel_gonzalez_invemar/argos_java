/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BPROYECTO_X_TIPOVARIABLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BproyectoXTipovariable.findAll", query = "SELECT b FROM BproyectoXTipovariable b"),
    @NamedQuery(name = "BproyectoXTipovariable.findByProyecto", query = "SELECT b FROM BproyectoXTipovariable b WHERE b.bproyectoXTipovariablePK.proyecto = :proyecto"),
    @NamedQuery(name = "BproyectoXTipovariable.findByTipoVariable", query = "SELECT b FROM BproyectoXTipovariable b WHERE b.bproyectoXTipovariablePK.tipoVariable = :tipoVariable")})
public class BproyectoXTipovariable implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BproyectoXTipovariablePK bproyectoXTipovariablePK;

    public BproyectoXTipovariable() {
    }

    public BproyectoXTipovariable(BproyectoXTipovariablePK bproyectoXTipovariablePK) {
        this.bproyectoXTipovariablePK = bproyectoXTipovariablePK;
    }

    public BproyectoXTipovariable(String proyecto, short tipoVariable) {
        this.bproyectoXTipovariablePK = new BproyectoXTipovariablePK(proyecto, tipoVariable);
    }

    public BproyectoXTipovariablePK getBproyectoXTipovariablePK() {
        return bproyectoXTipovariablePK;
    }

    public void setBproyectoXTipovariablePK(BproyectoXTipovariablePK bproyectoXTipovariablePK) {
        this.bproyectoXTipovariablePK = bproyectoXTipovariablePK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bproyectoXTipovariablePK != null ? bproyectoXTipovariablePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BproyectoXTipovariable)) {
            return false;
        }
        BproyectoXTipovariable other = (BproyectoXTipovariable) object;
        if ((this.bproyectoXTipovariablePK == null && other.bproyectoXTipovariablePK != null) || (this.bproyectoXTipovariablePK != null && !this.bproyectoXTipovariablePK.equals(other.bproyectoXTipovariablePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BproyectoXTipovariable[ bproyectoXTipovariablePK=" + bproyectoXTipovariablePK + " ]";
    }
    
}
