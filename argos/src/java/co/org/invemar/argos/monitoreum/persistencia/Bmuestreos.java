/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BMUESTREOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bmuestreos.findAll", query = "SELECT b FROM Bmuestreos b"),
    @NamedQuery(name = "Bmuestreos.findByIdMuestreo", query = "SELECT b FROM Bmuestreos b WHERE b.idMuestreo = :idMuestreo"),
    @NamedQuery(name = "Bmuestreos.findByIdEstacion", query = "SELECT b FROM Bmuestreos b WHERE b.idEstacion = :idEstacion"),
    @NamedQuery(name = "Bmuestreos.findByFechaHora", query = "SELECT b FROM Bmuestreos b WHERE b.fechaHora = :fechaHora"),
    @NamedQuery(name = "Bmuestreos.findByIdProyecto", query = "SELECT b FROM Bmuestreos b WHERE b.idProyecto = :idProyecto"),
    @NamedQuery(name = "Bmuestreos.findByProfundidad", query = "SELECT b FROM Bmuestreos b WHERE b.profundidad = :profundidad"),
    @NamedQuery(name = "Bmuestreos.findByIdEntidad", query = "SELECT b FROM Bmuestreos b WHERE b.idEntidad = :idEntidad"),
    @NamedQuery(name = "Bmuestreos.findByObservaciones", query = "SELECT b FROM Bmuestreos b WHERE b.observaciones = :observaciones"),
    @NamedQuery(name = "Bmuestreos.findByUploadflag", query = "SELECT b FROM Bmuestreos b WHERE b.uploadflag = :uploadflag"),
    @NamedQuery(name = "Bmuestreos.findByProfMin", query = "SELECT b FROM Bmuestreos b WHERE b.profMin = :profMin"),
    @NamedQuery(name = "Bmuestreos.findByProfMax", query = "SELECT b FROM Bmuestreos b WHERE b.profMax = :profMax")})
public class Bmuestreos implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID_MUESTREO")
    private BigDecimal idMuestreo;
    @Basic(optional = false)
    @Column(name = "ID_ESTACION")
    private String idEstacion;
    @Basic(optional = false)
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @Column(name = "ID_PROYECTO")
    private String idProyecto;
    @Column(name = "PROFUNDIDAD")
    private BigDecimal profundidad;
    @Column(name = "ID_ENTIDAD")
    private String idEntidad;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @Column(name = "UPLOADFLAG")
    private String uploadflag;
    @Column(name = "PROF_MIN")
    private BigDecimal profMin;
    @Column(name = "PROF_MAX")
    private BigDecimal profMax;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bmuestreos")
    private List<Bresponsables> bresponsablesList;
    @JoinColumn(name = "ID_COMPONENTE", referencedColumnName = "CODIGO")
    @ManyToOne
    private BubicacionTematica idComponente;
    @JoinColumn(name = "CLASE_SUSTRATO", referencedColumnName = "CODIGO_TIPO_SUSTRATO_TS")
    @ManyToOne
    private BtipoSustratos claseSustrato;
    @JoinColumn(name = "TEMPORADA", referencedColumnName = "CODIGO_TM")
    @ManyToOne(optional = false)
    private Btemporadas temporada;
    @JoinColumn(name = "SUSTRATO", referencedColumnName = "CODIGO_SUSTRATO_SS")
    @ManyToOne
    private Bsustratos sustrato;
    @JoinColumn(name = "ID_METODOLOGIA", referencedColumnName = "ID_METODOLOGIA")
    @ManyToOne
    private Bmetodologias idMetodologia;

    public Bmuestreos() {
    }

    public Bmuestreos(BigDecimal idMuestreo) {
        this.idMuestreo = idMuestreo;
    }

    public Bmuestreos(BigDecimal idMuestreo, String idEstacion, Date fechaHora) {
        this.idMuestreo = idMuestreo;
        this.idEstacion = idEstacion;
        this.fechaHora = fechaHora;
    }

    public BigDecimal getIdMuestreo() {
        return idMuestreo;
    }

    public void setIdMuestreo(BigDecimal idMuestreo) {
        this.idMuestreo = idMuestreo;
    }

    public String getIdEstacion() {
        return idEstacion;
    }

    public void setIdEstacion(String idEstacion) {
        this.idEstacion = idEstacion;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    public BigDecimal getProfundidad() {
        return profundidad;
    }

    public void setProfundidad(BigDecimal profundidad) {
        this.profundidad = profundidad;
    }

    public String getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(String idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getUploadflag() {
        return uploadflag;
    }

    public void setUploadflag(String uploadflag) {
        this.uploadflag = uploadflag;
    }

    public BigDecimal getProfMin() {
        return profMin;
    }

    public void setProfMin(BigDecimal profMin) {
        this.profMin = profMin;
    }

    public BigDecimal getProfMax() {
        return profMax;
    }

    public void setProfMax(BigDecimal profMax) {
        this.profMax = profMax;
    }

    @XmlTransient
    public List<Bresponsables> getBresponsablesList() {
        return bresponsablesList;
    }

    public void setBresponsablesList(List<Bresponsables> bresponsablesList) {
        this.bresponsablesList = bresponsablesList;
    }

    public BubicacionTematica getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(BubicacionTematica idComponente) {
        this.idComponente = idComponente;
    }

    public BtipoSustratos getClaseSustrato() {
        return claseSustrato;
    }

    public void setClaseSustrato(BtipoSustratos claseSustrato) {
        this.claseSustrato = claseSustrato;
    }

    public Btemporadas getTemporada() {
        return temporada;
    }

    public void setTemporada(Btemporadas temporada) {
        this.temporada = temporada;
    }

    public Bsustratos getSustrato() {
        return sustrato;
    }

    public void setSustrato(Bsustratos sustrato) {
        this.sustrato = sustrato;
    }

    public Bmetodologias getIdMetodologia() {
        return idMetodologia;
    }

    public void setIdMetodologia(Bmetodologias idMetodologia) {
        this.idMetodologia = idMetodologia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMuestreo != null ? idMuestreo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bmuestreos)) {
            return false;
        }
        Bmuestreos other = (Bmuestreos) object;
        if ((this.idMuestreo == null && other.idMuestreo != null) || (this.idMuestreo != null && !this.idMuestreo.equals(other.idMuestreo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Bmuestreos[ idMuestreo=" + idMuestreo + " ]";
    }
    
}
