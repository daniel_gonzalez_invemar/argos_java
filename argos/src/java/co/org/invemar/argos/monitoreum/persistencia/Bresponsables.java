/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BRESPONSABLES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bresponsables.findAll", query = "SELECT b FROM Bresponsables b"),
    @NamedQuery(name = "Bresponsables.findByMuestreo", query = "SELECT b FROM Bresponsables b WHERE b.bresponsablesPK.muestreo = :muestreo"),
    @NamedQuery(name = "Bresponsables.findByTarea", query = "SELECT b FROM Bresponsables b WHERE b.bresponsablesPK.tarea = :tarea"),
    @NamedQuery(name = "Bresponsables.findByOrden", query = "SELECT b FROM Bresponsables b WHERE b.bresponsablesPK.orden = :orden"),
    @NamedQuery(name = "Bresponsables.findByCodigoFuncionario", query = "SELECT b FROM Bresponsables b WHERE b.bresponsablesPK.codigoFuncionario = :codigoFuncionario"),
    @NamedQuery(name = "Bresponsables.findByFecha", query = "SELECT b FROM Bresponsables b WHERE b.fecha = :fecha"),
    @NamedQuery(name = "Bresponsables.findByFechasistema", query = "SELECT b FROM Bresponsables b WHERE b.fechasistema = :fechasistema"),
    @NamedQuery(name = "Bresponsables.findByNotas", query = "SELECT b FROM Bresponsables b WHERE b.notas = :notas"),
    @NamedQuery(name = "Bresponsables.findByUploadflag", query = "SELECT b FROM Bresponsables b WHERE b.uploadflag = :uploadflag")})
public class Bresponsables implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BresponsablesPK bresponsablesPK;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "FECHASISTEMA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechasistema;
    @Column(name = "NOTAS")
    private String notas;
    @Column(name = "UPLOADFLAG")
    private String uploadflag;
    @JoinColumn(name = "MUESTREO", referencedColumnName = "ID_MUESTREO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Bmuestreos bmuestreos;

    public Bresponsables() {
    }

    public Bresponsables(BresponsablesPK bresponsablesPK) {
        this.bresponsablesPK = bresponsablesPK;
    }

    public Bresponsables(BigInteger muestreo, short tarea, short orden, int codigoFuncionario) {
        this.bresponsablesPK = new BresponsablesPK(muestreo, tarea, orden, codigoFuncionario);
    }

    public BresponsablesPK getBresponsablesPK() {
        return bresponsablesPK;
    }

    public void setBresponsablesPK(BresponsablesPK bresponsablesPK) {
        this.bresponsablesPK = bresponsablesPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechasistema() {
        return fechasistema;
    }

    public void setFechasistema(Date fechasistema) {
        this.fechasistema = fechasistema;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getUploadflag() {
        return uploadflag;
    }

    public void setUploadflag(String uploadflag) {
        this.uploadflag = uploadflag;
    }

    public Bmuestreos getBmuestreos() {
        return bmuestreos;
    }

    public void setBmuestreos(Bmuestreos bmuestreos) {
        this.bmuestreos = bmuestreos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bresponsablesPK != null ? bresponsablesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bresponsables)) {
            return false;
        }
        Bresponsables other = (Bresponsables) object;
        if ((this.bresponsablesPK == null && other.bresponsablesPK != null) || (this.bresponsablesPK != null && !this.bresponsablesPK.equals(other.bresponsablesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Bresponsables[ bresponsablesPK=" + bresponsablesPK + " ]";
    }
    
}
