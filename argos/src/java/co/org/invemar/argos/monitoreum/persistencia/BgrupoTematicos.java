/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BGRUPO_TEMATICOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BgrupoTematicos.findAll", query = "SELECT b FROM BgrupoTematicos b"),
    @NamedQuery(name = "BgrupoTematicos.findByIdComponente", query = "SELECT b FROM BgrupoTematicos b WHERE b.bgrupoTematicosPK.idComponente = :idComponente"),
    @NamedQuery(name = "BgrupoTematicos.findByIdGrupo", query = "SELECT b FROM BgrupoTematicos b WHERE b.bgrupoTematicosPK.idGrupo = :idGrupo"),
    @NamedQuery(name = "BgrupoTematicos.findByNombre", query = "SELECT b FROM BgrupoTematicos b WHERE b.nombre = :nombre"),
    @NamedQuery(name = "BgrupoTematicos.findByDescripcion", query = "SELECT b FROM BgrupoTematicos b WHERE b.descripcion = :descripcion")})
public class BgrupoTematicos implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BgrupoTematicosPK bgrupoTematicosPK;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @JoinColumn(name = "ID_COMPONENTE", referencedColumnName = "ID_COMPONENTE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Bcomponentes bcomponentes;

    public BgrupoTematicos() {
    }

    public BgrupoTematicos(BgrupoTematicosPK bgrupoTematicosPK) {
        this.bgrupoTematicosPK = bgrupoTematicosPK;
    }

    public BgrupoTematicos(short idComponente, short idGrupo) {
        this.bgrupoTematicosPK = new BgrupoTematicosPK(idComponente, idGrupo);
    }

    public BgrupoTematicosPK getBgrupoTematicosPK() {
        return bgrupoTematicosPK;
    }

    public void setBgrupoTematicosPK(BgrupoTematicosPK bgrupoTematicosPK) {
        this.bgrupoTematicosPK = bgrupoTematicosPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Bcomponentes getBcomponentes() {
        return bcomponentes;
    }

    public void setBcomponentes(Bcomponentes bcomponentes) {
        this.bcomponentes = bcomponentes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bgrupoTematicosPK != null ? bgrupoTematicosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BgrupoTematicos)) {
            return false;
        }
        BgrupoTematicos other = (BgrupoTematicos) object;
        if ((this.bgrupoTematicosPK == null && other.bgrupoTematicosPK != null) || (this.bgrupoTematicosPK != null && !this.bgrupoTematicosPK.equals(other.bgrupoTematicosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BgrupoTematicos[ bgrupoTematicosPK=" + bgrupoTematicosPK + " ]";
    }
    
}
