/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BproyectoXTipovariablePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "PROYECTO")
    private String proyecto;
    @Basic(optional = false)
    @Column(name = "TIPO_VARIABLE")
    private short tipoVariable;

    public BproyectoXTipovariablePK() {
    }

    public BproyectoXTipovariablePK(String proyecto, short tipoVariable) {
        this.proyecto = proyecto;
        this.tipoVariable = tipoVariable;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public short getTipoVariable() {
        return tipoVariable;
    }

    public void setTipoVariable(short tipoVariable) {
        this.tipoVariable = tipoVariable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proyecto != null ? proyecto.hashCode() : 0);
        hash += (int) tipoVariable;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BproyectoXTipovariablePK)) {
            return false;
        }
        BproyectoXTipovariablePK other = (BproyectoXTipovariablePK) object;
        if ((this.proyecto == null && other.proyecto != null) || (this.proyecto != null && !this.proyecto.equals(other.proyecto))) {
            return false;
        }
        if (this.tipoVariable != other.tipoVariable) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BproyectoXTipovariablePK[ proyecto=" + proyecto + ", tipoVariable=" + tipoVariable + " ]";
    }
    
}
