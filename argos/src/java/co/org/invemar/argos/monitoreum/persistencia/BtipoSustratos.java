/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BTIPO_SUSTRATOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BtipoSustratos.findAll", query = "SELECT b FROM BtipoSustratos b"),
    @NamedQuery(name = "BtipoSustratos.findByCodigoTipoSustratoTs", query = "SELECT b FROM BtipoSustratos b WHERE b.codigoTipoSustratoTs = :codigoTipoSustratoTs"),
    @NamedQuery(name = "BtipoSustratos.findByNombreTs", query = "SELECT b FROM BtipoSustratos b WHERE b.nombreTs = :nombreTs")})
public class BtipoSustratos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO_TIPO_SUSTRATO_TS")
    private String codigoTipoSustratoTs;
    @Column(name = "NOMBRE_TS")
    private String nombreTs;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "btipoSustratos")
    private List<Brangos> brangosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codigoTipoSustratoSs")
    private List<Bsustratos> bsustratosList;
    @OneToMany(mappedBy = "claseSustrato")
    private List<Bmuestreos> bmuestreosList;

    public BtipoSustratos() {
    }

    public BtipoSustratos(String codigoTipoSustratoTs) {
        this.codigoTipoSustratoTs = codigoTipoSustratoTs;
    }

    public String getCodigoTipoSustratoTs() {
        return codigoTipoSustratoTs;
    }

    public void setCodigoTipoSustratoTs(String codigoTipoSustratoTs) {
        this.codigoTipoSustratoTs = codigoTipoSustratoTs;
    }

    public String getNombreTs() {
        return nombreTs;
    }

    public void setNombreTs(String nombreTs) {
        this.nombreTs = nombreTs;
    }

    @XmlTransient
    public List<Brangos> getBrangosList() {
        return brangosList;
    }

    public void setBrangosList(List<Brangos> brangosList) {
        this.brangosList = brangosList;
    }

    @XmlTransient
    public List<Bsustratos> getBsustratosList() {
        return bsustratosList;
    }

    public void setBsustratosList(List<Bsustratos> bsustratosList) {
        this.bsustratosList = bsustratosList;
    }

    @XmlTransient
    public List<Bmuestreos> getBmuestreosList() {
        return bmuestreosList;
    }

    public void setBmuestreosList(List<Bmuestreos> bmuestreosList) {
        this.bmuestreosList = bmuestreosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoTipoSustratoTs != null ? codigoTipoSustratoTs.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BtipoSustratos)) {
            return false;
        }
        BtipoSustratos other = (BtipoSustratos) object;
        if ((this.codigoTipoSustratoTs == null && other.codigoTipoSustratoTs != null) || (this.codigoTipoSustratoTs != null && !this.codigoTipoSustratoTs.equals(other.codigoTipoSustratoTs))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BtipoSustratos[ codigoTipoSustratoTs=" + codigoTipoSustratoTs + " ]";
    }
    
}
