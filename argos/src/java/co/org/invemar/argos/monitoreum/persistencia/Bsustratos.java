/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BSUSTRATOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bsustratos.findAll", query = "SELECT b FROM Bsustratos b"),
    @NamedQuery(name = "Bsustratos.findByCodigoSustratoSs", query = "SELECT b FROM Bsustratos b WHERE b.codigoSustratoSs = :codigoSustratoSs"),
    @NamedQuery(name = "Bsustratos.findByNombreSs", query = "SELECT b FROM Bsustratos b WHERE b.nombreSs = :nombreSs"),
    @NamedQuery(name = "Bsustratos.findByDescripcionSs", query = "SELECT b FROM Bsustratos b WHERE b.descripcionSs = :descripcionSs")})
public class Bsustratos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO_SUSTRATO_SS")
    private String codigoSustratoSs;
    @Basic(optional = false)
    @Column(name = "NOMBRE_SS")
    private String nombreSs;
    @Column(name = "DESCRIPCION_SS")
    private String descripcionSs;
    @JoinColumn(name = "CODIGO_TIPO_SUSTRATO_SS", referencedColumnName = "CODIGO_TIPO_SUSTRATO_TS")
    @ManyToOne(optional = false)
    private BtipoSustratos codigoTipoSustratoSs;
    @OneToMany(mappedBy = "sustrato")
    private List<Bmuestreos> bmuestreosList;

    public Bsustratos() {
    }

    public Bsustratos(String codigoSustratoSs) {
        this.codigoSustratoSs = codigoSustratoSs;
    }

    public Bsustratos(String codigoSustratoSs, String nombreSs) {
        this.codigoSustratoSs = codigoSustratoSs;
        this.nombreSs = nombreSs;
    }

    public String getCodigoSustratoSs() {
        return codigoSustratoSs;
    }

    public void setCodigoSustratoSs(String codigoSustratoSs) {
        this.codigoSustratoSs = codigoSustratoSs;
    }

    public String getNombreSs() {
        return nombreSs;
    }

    public void setNombreSs(String nombreSs) {
        this.nombreSs = nombreSs;
    }

    public String getDescripcionSs() {
        return descripcionSs;
    }

    public void setDescripcionSs(String descripcionSs) {
        this.descripcionSs = descripcionSs;
    }

    public BtipoSustratos getCodigoTipoSustratoSs() {
        return codigoTipoSustratoSs;
    }

    public void setCodigoTipoSustratoSs(BtipoSustratos codigoTipoSustratoSs) {
        this.codigoTipoSustratoSs = codigoTipoSustratoSs;
    }

    @XmlTransient
    public List<Bmuestreos> getBmuestreosList() {
        return bmuestreosList;
    }

    public void setBmuestreosList(List<Bmuestreos> bmuestreosList) {
        this.bmuestreosList = bmuestreosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoSustratoSs != null ? codigoSustratoSs.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bsustratos)) {
            return false;
        }
        Bsustratos other = (Bsustratos) object;
        if ((this.codigoSustratoSs == null && other.codigoSustratoSs != null) || (this.codigoSustratoSs != null && !this.codigoSustratoSs.equals(other.codigoSustratoSs))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Bsustratos[ codigoSustratoSs=" + codigoSustratoSs + " ]";
    }
    
}
