/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BresponsablesPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "MUESTREO")
    private BigInteger muestreo;
    @Basic(optional = false)
    @Column(name = "TAREA")
    private short tarea;
    @Basic(optional = false)
    @Column(name = "ORDEN")
    private short orden;
    @Basic(optional = false)
    @Column(name = "CODIGO_FUNCIONARIO")
    private int codigoFuncionario;

    public BresponsablesPK() {
    }

    public BresponsablesPK(BigInteger muestreo, short tarea, short orden, int codigoFuncionario) {
        this.muestreo = muestreo;
        this.tarea = tarea;
        this.orden = orden;
        this.codigoFuncionario = codigoFuncionario;
    }

    public BigInteger getMuestreo() {
        return muestreo;
    }

    public void setMuestreo(BigInteger muestreo) {
        this.muestreo = muestreo;
    }

    public short getTarea() {
        return tarea;
    }

    public void setTarea(short tarea) {
        this.tarea = tarea;
    }

    public short getOrden() {
        return orden;
    }

    public void setOrden(short orden) {
        this.orden = orden;
    }

    public int getCodigoFuncionario() {
        return codigoFuncionario;
    }

    public void setCodigoFuncionario(int codigoFuncionario) {
        this.codigoFuncionario = codigoFuncionario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (muestreo != null ? muestreo.hashCode() : 0);
        hash += (int) tarea;
        hash += (int) orden;
        hash += (int) codigoFuncionario;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BresponsablesPK)) {
            return false;
        }
        BresponsablesPK other = (BresponsablesPK) object;
        if ((this.muestreo == null && other.muestreo != null) || (this.muestreo != null && !this.muestreo.equals(other.muestreo))) {
            return false;
        }
        if (this.tarea != other.tarea) {
            return false;
        }
        if (this.orden != other.orden) {
            return false;
        }
        if (this.codigoFuncionario != other.codigoFuncionario) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BresponsablesPK[ muestreo=" + muestreo + ", tarea=" + tarea + ", orden=" + orden + ", codigoFuncionario=" + codigoFuncionario + " ]";
    }
    
}
