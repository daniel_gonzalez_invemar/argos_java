/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BUBICACION_TEMATICA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BubicacionTematica.findAll", query = "SELECT b FROM BubicacionTematica b"),
    @NamedQuery(name = "BubicacionTematica.findByCodigo", query = "SELECT b FROM BubicacionTematica b WHERE b.codigo = :codigo"),
    @NamedQuery(name = "BubicacionTematica.findByRaiz", query = "SELECT b FROM BubicacionTematica b WHERE b.raiz = :raiz"),
    @NamedQuery(name = "BubicacionTematica.findByDescripcion", query = "SELECT b FROM BubicacionTematica b WHERE b.descripcion = :descripcion"),
    @NamedQuery(name = "BubicacionTematica.findByNivel", query = "SELECT b FROM BubicacionTematica b WHERE b.nivel = :nivel"),
    @NamedQuery(name = "BubicacionTematica.findByOrden", query = "SELECT b FROM BubicacionTematica b WHERE b.orden = :orden"),
    @NamedQuery(name = "BubicacionTematica.findByUltimo", query = "SELECT b FROM BubicacionTematica b WHERE b.ultimo = :ultimo"),
    @NamedQuery(name = "BubicacionTematica.findByEsquema", query = "SELECT b FROM BubicacionTematica b WHERE b.esquema = :esquema")})
public class BubicacionTematica implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private Integer codigo;
    @Basic(optional = false)
    @Column(name = "RAIZ")
    private int raiz;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "NIVEL")
    private short nivel;
    @Basic(optional = false)
    @Column(name = "ORDEN")
    private short orden;
    @Column(name = "ULTIMO")
    private String ultimo;
    @Column(name = "ESQUEMA")
    private String esquema;
    @JoinTable(name = "BVARIABLES_X_COMP", joinColumns = {
        @JoinColumn(name = "COMPONENTE", referencedColumnName = "CODIGO")}, inverseJoinColumns = {
        @JoinColumn(name = "VARIABLE", referencedColumnName = "CODIGO_VR")})
    @ManyToMany
    private List<Bvariables> bvariablesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bubicacionTematica")
    private List<BproyectoXComp> bproyectoXCompList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bubicacionTematica")
    private List<BmetodologiasComp> bmetodologiasCompList;
    @OneToMany(mappedBy = "idComponente")
    private List<Bmuestreos> bmuestreosList;

    public BubicacionTematica() {
    }

    public BubicacionTematica(Integer codigo) {
        this.codigo = codigo;
    }

    public BubicacionTematica(Integer codigo, int raiz, String descripcion, short nivel, short orden) {
        this.codigo = codigo;
        this.raiz = raiz;
        this.descripcion = descripcion;
        this.nivel = nivel;
        this.orden = orden;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public int getRaiz() {
        return raiz;
    }

    public void setRaiz(int raiz) {
        this.raiz = raiz;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public short getNivel() {
        return nivel;
    }

    public void setNivel(short nivel) {
        this.nivel = nivel;
    }

    public short getOrden() {
        return orden;
    }

    public void setOrden(short orden) {
        this.orden = orden;
    }

    public String getUltimo() {
        return ultimo;
    }

    public void setUltimo(String ultimo) {
        this.ultimo = ultimo;
    }

    public String getEsquema() {
        return esquema;
    }

    public void setEsquema(String esquema) {
        this.esquema = esquema;
    }

    @XmlTransient
    public List<Bvariables> getBvariablesList() {
        return bvariablesList;
    }

    public void setBvariablesList(List<Bvariables> bvariablesList) {
        this.bvariablesList = bvariablesList;
    }

    @XmlTransient
    public List<BproyectoXComp> getBproyectoXCompList() {
        return bproyectoXCompList;
    }

    public void setBproyectoXCompList(List<BproyectoXComp> bproyectoXCompList) {
        this.bproyectoXCompList = bproyectoXCompList;
    }

    @XmlTransient
    public List<BmetodologiasComp> getBmetodologiasCompList() {
        return bmetodologiasCompList;
    }

    public void setBmetodologiasCompList(List<BmetodologiasComp> bmetodologiasCompList) {
        this.bmetodologiasCompList = bmetodologiasCompList;
    }

    @XmlTransient
    public List<Bmuestreos> getBmuestreosList() {
        return bmuestreosList;
    }

    public void setBmuestreosList(List<Bmuestreos> bmuestreosList) {
        this.bmuestreosList = bmuestreosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BubicacionTematica)) {
            return false;
        }
        BubicacionTematica other = (BubicacionTematica) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BubicacionTematica[ codigo=" + codigo + " ]";
    }
    
}
