/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BespeciesXProyectoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "COD_ESPECIE")
    private String codEspecie;
    @Basic(optional = false)
    @Column(name = "COD_ESPECIE_LETRAS")
    private String codEspecieLetras;
    @Basic(optional = false)
    @Column(name = "COD_PROYECTO")
    private BigInteger codProyecto;

    public BespeciesXProyectoPK() {
    }

    public BespeciesXProyectoPK(String codEspecie, String codEspecieLetras, BigInteger codProyecto) {
        this.codEspecie = codEspecie;
        this.codEspecieLetras = codEspecieLetras;
        this.codProyecto = codProyecto;
    }

    public String getCodEspecie() {
        return codEspecie;
    }

    public void setCodEspecie(String codEspecie) {
        this.codEspecie = codEspecie;
    }

    public String getCodEspecieLetras() {
        return codEspecieLetras;
    }

    public void setCodEspecieLetras(String codEspecieLetras) {
        this.codEspecieLetras = codEspecieLetras;
    }

    public BigInteger getCodProyecto() {
        return codProyecto;
    }

    public void setCodProyecto(BigInteger codProyecto) {
        this.codProyecto = codProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codEspecie != null ? codEspecie.hashCode() : 0);
        hash += (codEspecieLetras != null ? codEspecieLetras.hashCode() : 0);
        hash += (codProyecto != null ? codProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BespeciesXProyectoPK)) {
            return false;
        }
        BespeciesXProyectoPK other = (BespeciesXProyectoPK) object;
        if ((this.codEspecie == null && other.codEspecie != null) || (this.codEspecie != null && !this.codEspecie.equals(other.codEspecie))) {
            return false;
        }
        if ((this.codEspecieLetras == null && other.codEspecieLetras != null) || (this.codEspecieLetras != null && !this.codEspecieLetras.equals(other.codEspecieLetras))) {
            return false;
        }
        if ((this.codProyecto == null && other.codProyecto != null) || (this.codProyecto != null && !this.codProyecto.equals(other.codProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BespeciesXProyectoPK[ codEspecie=" + codEspecie + ", codEspecieLetras=" + codEspecieLetras + ", codProyecto=" + codProyecto + " ]";
    }
    
}
