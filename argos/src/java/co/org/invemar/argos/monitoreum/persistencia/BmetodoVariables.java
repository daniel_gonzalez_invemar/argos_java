/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BMETODO_VARIABLES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmetodoVariables.findAll", query = "SELECT b FROM BmetodoVariables b"),
    @NamedQuery(name = "BmetodoVariables.findByCodVariableMv", query = "SELECT b FROM BmetodoVariables b WHERE b.bmetodoVariablesPK.codVariableMv = :codVariableMv"),
    @NamedQuery(name = "BmetodoVariables.findByCodMetodoMv", query = "SELECT b FROM BmetodoVariables b WHERE b.bmetodoVariablesPK.codMetodoMv = :codMetodoMv")})
public class BmetodoVariables implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BmetodoVariablesPK bmetodoVariablesPK;
    @JoinColumn(name = "COD_VARIABLE_MV", referencedColumnName = "CODIGO_VR", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Bvariables bvariables;

    public BmetodoVariables() {
    }

    public BmetodoVariables(BmetodoVariablesPK bmetodoVariablesPK) {
        this.bmetodoVariablesPK = bmetodoVariablesPK;
    }

    public BmetodoVariables(String codVariableMv, String codMetodoMv) {
        this.bmetodoVariablesPK = new BmetodoVariablesPK(codVariableMv, codMetodoMv);
    }

    public BmetodoVariablesPK getBmetodoVariablesPK() {
        return bmetodoVariablesPK;
    }

    public void setBmetodoVariablesPK(BmetodoVariablesPK bmetodoVariablesPK) {
        this.bmetodoVariablesPK = bmetodoVariablesPK;
    }

    public Bvariables getBvariables() {
        return bvariables;
    }

    public void setBvariables(Bvariables bvariables) {
        this.bvariables = bvariables;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bmetodoVariablesPK != null ? bmetodoVariablesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmetodoVariables)) {
            return false;
        }
        BmetodoVariables other = (BmetodoVariables) object;
        if ((this.bmetodoVariablesPK == null && other.bmetodoVariablesPK != null) || (this.bmetodoVariablesPK != null && !this.bmetodoVariablesPK.equals(other.bmetodoVariablesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BmetodoVariables[ bmetodoVariablesPK=" + bmetodoVariablesPK + " ]";
    }
    
}
