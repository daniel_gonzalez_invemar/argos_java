/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BproyectoXCompPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ID_PROYECTO")
    private BigInteger idProyecto;
    @Basic(optional = false)
    @Column(name = "ID_COMPONENTE")
    private int idComponente;

    public BproyectoXCompPK() {
    }

    public BproyectoXCompPK(BigInteger idProyecto, int idComponente) {
        this.idProyecto = idProyecto;
        this.idComponente = idComponente;
    }

    public BigInteger getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(BigInteger idProyecto) {
        this.idProyecto = idProyecto;
    }

    public int getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(int idComponente) {
        this.idComponente = idComponente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProyecto != null ? idProyecto.hashCode() : 0);
        hash += (int) idComponente;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BproyectoXCompPK)) {
            return false;
        }
        BproyectoXCompPK other = (BproyectoXCompPK) object;
        if ((this.idProyecto == null && other.idProyecto != null) || (this.idProyecto != null && !this.idProyecto.equals(other.idProyecto))) {
            return false;
        }
        if (this.idComponente != other.idComponente) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BproyectoXCompPK[ idProyecto=" + idProyecto + ", idComponente=" + idComponente + " ]";
    }
    
}
