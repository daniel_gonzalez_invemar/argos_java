/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BTEMPORADAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Btemporadas.findAll", query = "SELECT b FROM Btemporadas b"),
    @NamedQuery(name = "Btemporadas.findByCodigoTm", query = "SELECT b FROM Btemporadas b WHERE b.codigoTm = :codigoTm"),
    @NamedQuery(name = "Btemporadas.findByNombreTm", query = "SELECT b FROM Btemporadas b WHERE b.nombreTm = :nombreTm"),
    @NamedQuery(name = "Btemporadas.findBySistemaTm", query = "SELECT b FROM Btemporadas b WHERE b.sistemaTm = :sistemaTm"),
    @NamedQuery(name = "Btemporadas.findByFechafinTm", query = "SELECT b FROM Btemporadas b WHERE b.fechafinTm = :fechafinTm")})
public class Btemporadas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO_TM")
    private String codigoTm;
    @Basic(optional = false)
    @Column(name = "NOMBRE_TM")
    private String nombreTm;
    @Column(name = "SISTEMA_TM")
    private String sistemaTm;
    @Column(name = "FECHAFIN_TM")
    private String fechafinTm;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "temporada")
    private List<Bmuestreos> bmuestreosList;
    @JoinColumn(name = "ID_COMPONENTE", referencedColumnName = "ID_COMPONENTE")
    @ManyToOne
    private Bcomponentes idComponente;

    public Btemporadas() {
    }

    public Btemporadas(String codigoTm) {
        this.codigoTm = codigoTm;
    }

    public Btemporadas(String codigoTm, String nombreTm) {
        this.codigoTm = codigoTm;
        this.nombreTm = nombreTm;
    }

    public String getCodigoTm() {
        return codigoTm;
    }

    public void setCodigoTm(String codigoTm) {
        this.codigoTm = codigoTm;
    }

    public String getNombreTm() {
        return nombreTm;
    }

    public void setNombreTm(String nombreTm) {
        this.nombreTm = nombreTm;
    }

    public String getSistemaTm() {
        return sistemaTm;
    }

    public void setSistemaTm(String sistemaTm) {
        this.sistemaTm = sistemaTm;
    }

    public String getFechafinTm() {
        return fechafinTm;
    }

    public void setFechafinTm(String fechafinTm) {
        this.fechafinTm = fechafinTm;
    }

    @XmlTransient
    public List<Bmuestreos> getBmuestreosList() {
        return bmuestreosList;
    }

    public void setBmuestreosList(List<Bmuestreos> bmuestreosList) {
        this.bmuestreosList = bmuestreosList;
    }

    public Bcomponentes getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(Bcomponentes idComponente) {
        this.idComponente = idComponente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoTm != null ? codigoTm.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Btemporadas)) {
            return false;
        }
        Btemporadas other = (Btemporadas) object;
        if ((this.codigoTm == null && other.codigoTm != null) || (this.codigoTm != null && !this.codigoTm.equals(other.codigoTm))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Btemporadas[ codigoTm=" + codigoTm + " ]";
    }
    
}
