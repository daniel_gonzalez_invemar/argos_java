/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BVALIDACIONES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bvalidaciones.findAll", query = "SELECT b FROM Bvalidaciones b"),
    @NamedQuery(name = "Bvalidaciones.findByIdVariable", query = "SELECT b FROM Bvalidaciones b WHERE b.bvalidacionesPK.idVariable = :idVariable"),
    @NamedQuery(name = "Bvalidaciones.findByIdUnidad", query = "SELECT b FROM Bvalidaciones b WHERE b.bvalidacionesPK.idUnidad = :idUnidad"),
    @NamedQuery(name = "Bvalidaciones.findByValorMinimo", query = "SELECT b FROM Bvalidaciones b WHERE b.valorMinimo = :valorMinimo"),
    @NamedQuery(name = "Bvalidaciones.findByValorMaximo", query = "SELECT b FROM Bvalidaciones b WHERE b.valorMaximo = :valorMaximo"),
    @NamedQuery(name = "Bvalidaciones.findByValorMinPlausible", query = "SELECT b FROM Bvalidaciones b WHERE b.valorMinPlausible = :valorMinPlausible"),
    @NamedQuery(name = "Bvalidaciones.findByValorMaxPlausible", query = "SELECT b FROM Bvalidaciones b WHERE b.valorMaxPlausible = :valorMaxPlausible")})
public class Bvalidaciones implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BvalidacionesPK bvalidacionesPK;
    @Column(name = "VALOR_MINIMO")
    private BigInteger valorMinimo;
    @Column(name = "VALOR_MAXIMO")
    private BigInteger valorMaximo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR_MIN_PLAUSIBLE")
    private BigDecimal valorMinPlausible;
    @Column(name = "VALOR_MAX_PLAUSIBLE")
    private BigDecimal valorMaxPlausible;

    public Bvalidaciones() {
    }

    public Bvalidaciones(BvalidacionesPK bvalidacionesPK) {
        this.bvalidacionesPK = bvalidacionesPK;
    }

    public Bvalidaciones(String idVariable, short idUnidad) {
        this.bvalidacionesPK = new BvalidacionesPK(idVariable, idUnidad);
    }

    public BvalidacionesPK getBvalidacionesPK() {
        return bvalidacionesPK;
    }

    public void setBvalidacionesPK(BvalidacionesPK bvalidacionesPK) {
        this.bvalidacionesPK = bvalidacionesPK;
    }

    public BigInteger getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(BigInteger valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public BigInteger getValorMaximo() {
        return valorMaximo;
    }

    public void setValorMaximo(BigInteger valorMaximo) {
        this.valorMaximo = valorMaximo;
    }

    public BigDecimal getValorMinPlausible() {
        return valorMinPlausible;
    }

    public void setValorMinPlausible(BigDecimal valorMinPlausible) {
        this.valorMinPlausible = valorMinPlausible;
    }

    public BigDecimal getValorMaxPlausible() {
        return valorMaxPlausible;
    }

    public void setValorMaxPlausible(BigDecimal valorMaxPlausible) {
        this.valorMaxPlausible = valorMaxPlausible;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bvalidacionesPK != null ? bvalidacionesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bvalidaciones)) {
            return false;
        }
        Bvalidaciones other = (Bvalidaciones) object;
        if ((this.bvalidacionesPK == null && other.bvalidacionesPK != null) || (this.bvalidacionesPK != null && !this.bvalidacionesPK.equals(other.bvalidacionesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.Bvalidaciones[ bvalidacionesPK=" + bvalidacionesPK + " ]";
    }
    
}
