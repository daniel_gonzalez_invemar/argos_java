/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "BLISTAS_STANDARBORRAR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BlistasStandarborrar.findAll", query = "SELECT b FROM BlistasStandarborrar b"),
    @NamedQuery(name = "BlistasStandarborrar.findByIdVariable", query = "SELECT b FROM BlistasStandarborrar b WHERE b.blistasStandarborrarPK.idVariable = :idVariable"),
    @NamedQuery(name = "BlistasStandarborrar.findByIdValor", query = "SELECT b FROM BlistasStandarborrar b WHERE b.blistasStandarborrarPK.idValor = :idValor"),
    @NamedQuery(name = "BlistasStandarborrar.findByNombre", query = "SELECT b FROM BlistasStandarborrar b WHERE b.nombre = :nombre"),
    @NamedQuery(name = "BlistasStandarborrar.findByDescripcion", query = "SELECT b FROM BlistasStandarborrar b WHERE b.descripcion = :descripcion")})
public class BlistasStandarborrar implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BlistasStandarborrarPK blistasStandarborrarPK;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "DESCRIPCION")
    private String descripcion;

    public BlistasStandarborrar() {
    }

    public BlistasStandarborrar(BlistasStandarborrarPK blistasStandarborrarPK) {
        this.blistasStandarborrarPK = blistasStandarborrarPK;
    }

    public BlistasStandarborrar(String idVariable, short idValor) {
        this.blistasStandarborrarPK = new BlistasStandarborrarPK(idVariable, idValor);
    }

    public BlistasStandarborrarPK getBlistasStandarborrarPK() {
        return blistasStandarborrarPK;
    }

    public void setBlistasStandarborrarPK(BlistasStandarborrarPK blistasStandarborrarPK) {
        this.blistasStandarborrarPK = blistasStandarborrarPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (blistasStandarborrarPK != null ? blistasStandarborrarPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BlistasStandarborrar)) {
            return false;
        }
        BlistasStandarborrar other = (BlistasStandarborrar) object;
        if ((this.blistasStandarborrarPK == null && other.blistasStandarborrarPK != null) || (this.blistasStandarborrarPK != null && !this.blistasStandarborrarPK.equals(other.blistasStandarborrarPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BlistasStandarborrar[ blistasStandarborrarPK=" + blistasStandarborrarPK + " ]";
    }
    
}
