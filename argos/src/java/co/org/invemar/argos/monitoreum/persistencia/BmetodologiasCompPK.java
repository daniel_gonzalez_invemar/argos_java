/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.monitoreum.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usrsig15
 */
@Embeddable
public class BmetodologiasCompPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ID_METODOLOGIA")
    private short idMetodologia;
    @Basic(optional = false)
    @Column(name = "ID_COMPONENTE")
    private int idComponente;

    public BmetodologiasCompPK() {
    }

    public BmetodologiasCompPK(short idMetodologia, int idComponente) {
        this.idMetodologia = idMetodologia;
        this.idComponente = idComponente;
    }

    public short getIdMetodologia() {
        return idMetodologia;
    }

    public void setIdMetodologia(short idMetodologia) {
        this.idMetodologia = idMetodologia;
    }

    public int getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(int idComponente) {
        this.idComponente = idComponente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idMetodologia;
        hash += (int) idComponente;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmetodologiasCompPK)) {
            return false;
        }
        BmetodologiasCompPK other = (BmetodologiasCompPK) object;
        if (this.idMetodologia != other.idMetodologia) {
            return false;
        }
        if (this.idComponente != other.idComponente) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.monitoreum.persistencia.BmetodologiasCompPK[ idMetodologia=" + idMetodologia + ", idComponente=" + idComponente + " ]";
    }
    
}
