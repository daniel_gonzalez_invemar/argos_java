package co.org.invemar.argos.manglares.presiones.model;

public class TablaIndicatorByDepartament {
   private  String departament;
   private  String sector;
   private  double indicatorValue;
   private int anio;
   private  String unidadManejo;
   private String presion;
        
    public TablaIndicatorByDepartament() {
        super();
    }

    public void setUnidadManejo(String unidadManejo) {
        this.unidadManejo = unidadManejo;
    }

    public String getUnidadManejo() {
        return unidadManejo;
    }

    public String getDepartament() {
        return departament;
    }

    public void setDepartament(String departament) {
        this.departament = departament;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public double getIndicatorValue() {
        return indicatorValue;
    }

    public void setIndicatorValue(double indicatorValue) {
        this.indicatorValue = indicatorValue;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getPresion() {
        return presion;
    }

    public void setPresion(String presion) {
        this.presion = presion;
    }
}
