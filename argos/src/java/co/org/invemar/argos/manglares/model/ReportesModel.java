/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.Presion;
import co.org.invemar.argos.manglares.vo.Reporte;
import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel Gonzalez
 */
public class ReportesModel {
  
    
    public String getTodasLasActividadesEconomicasPorReporte(int idReporte) {
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
//        List<ActividadEconomica> listaActividadEconomica = new ArrayList<ActividadEconomica>();
        String actividadesEconomicas = "";
        try {

            String query = "SELECT rea.ID,\n"
                    + "  rea.IDREPORTE,\n"
                    + "  rea.IDACTIVIDADECONOMICA, v.valor\n"
                    + "FROM REPORTESXACTIVIDADESECONOMICAS rea inner join  valores  v on rea.IDACTIVIDADECONOMICA=v.id and atributos_codigo=2 and idreporte=" + idReporte;
//            System.out.println("query:" + query);
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
//                ActividadEconomica actividadEconomica = new ActividadEconomica();
//                actividadEconomica.setId(rs.getLong("id"));
//                actividadEconomica.setNombre(rs.getString("valor"));//                
//                listaActividadEconomica.add(actividadEconomica);    
                actividadesEconomicas = actividadesEconomicas + " " + rs.getString("valor") + "<br />";

            }
        } catch (SQLException ex) {
            System.out.println("Error en el objeto ReportesModel  m�todo getTodasLasActividadesEconomicasPorReporte:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return actividadesEconomicas;
    }

    public List<Reporte> getTablaDiagnostico(int idReporte) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        List<Reporte> diagnostico = new ArrayList<Reporte>();

        ArrayList<String> factoresSeleccionados = new ArrayList<String>();
        ResultSet rs = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        try {

            String query = "SELECT distinct rp.idfactor,f.nombre fROM REPORTESXPRESIONES rp inner join factores f on rp.idfactor= f.id where idreporte=" + idReporte;
//            System.out.println("query:" + query);
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                factoresSeleccionados.add(rs.getString("IDFACTOR"));
                Reporte reporte = new Reporte();
                reporte.setFactores(rs.getString("nombre"));
                String idFactor = rs.getString("idfactor");
                // query = "SELECT distinct  rp.idpresion,p.nombre fROM REPORTESXPRESIONES rp inner join presiones p on rp.idpresion=p.id where rp.idfactor=" + idFactor;
                query = "SELECT distinct  rp.idpresion,p.nombre,DECODE(intensidad,0,'No selecionado',1,'Menor',2,'Moderado',3,'Mayor')as  intensidad1,intensidad,\n"
                        + "DECODE(intensidad,0,'No selecionado',1,'Muy Frecuente',2,'Poco Frecuente',3,'Mayor')as  frecuencia1,frecuencia \n"
                        + "fROM REPORTESXPRESIONES rp  inner join presiones p on rp.idpresion=p.id where rp.idfactor=" + idFactor;
//                System.out.println("query:" + query);
                pstmt = con.prepareStatement(query);
                rs1 = pstmt.executeQuery();
                String presiones = "<ul>";
                while (rs1.next()) {
                    presiones = presiones + "<li>" + rs1.getString("nombre") + " - Intensidad :"+rs1.getString("intensidad1")+" - Frecuencia: "+rs1.getString("frecuencia1")+"</li>";
//                    Presion p  = new Presion();
//                    p.setNombre(rs1.getString("nombre"));
//                    p.setIntensidadCualitativa(rs1.getString("intensidad1"));
//                    p.setFrecuenciaCualitativa(rs1.getString("frecuencia1"));                  
//                    listaPresiones.add(p);
                    
                    String idPresion = rs1.getString("idPresion");
                    query = "select pe.idestado, e.id, e.nombre from presionesxestados pe inner join estados e on pe.idestado=e.id where idpresion=" + idPresion;
//                    System.out.println("query:" + query);
                    pstmt = con.prepareStatement(query);
                    rs2 = pstmt.executeQuery();
                    String estados = "<ul>";
                    
                    while (rs2.next()) {
                        String idEstados = rs2.getString("id");
                        estados = estados + "<li>" + rs2.getString("nombre") + "</li>";
                        query = "SELECT i.id, i.nombre FROM ESTADOSXIMPACTOS fi inner join impactos i on fi.idimpacto=i.id where fi.idestado=" + idEstados;
                        pstmt = con.prepareStatement(query);
//                        System.out.println("query:" + query);
                        rs3 = pstmt.executeQuery();
                        String impactos = "";
                        impactos="<ul>";
                        while (rs3.next()) {
                            impactos=impactos+"<li>"+rs3.getString("nombre")+"</li>";                           
                        }
                        impactos=impactos+"</ul>";
                        reporte.setImpactos(impactos);
                    }
                    estados=estados+"</ul>";
                    reporte.setEstados(estados);
                }
                presiones=presiones+"</ul>";
                reporte.setPresiones(presiones);
                diagnostico.add(reporte);
            }
            rs3.close();
            rs2.close();
            rs1.close();
            rs.close();

        } catch (SQLException ex) {
            System.out.println("Error en el objeto ReportesModel  m�todo getTablaDiagnostico:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);

        }
        return diagnostico;

    }

    

}
