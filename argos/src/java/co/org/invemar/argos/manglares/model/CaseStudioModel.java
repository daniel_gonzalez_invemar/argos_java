/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import org.apache.log4j.Logger;
import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class CaseStudioModel {

    public String getAllCaseStudioByEntity(String entity, String typeCaseStudio, String AttibuteValue) {
        JSONArray jsonarrayData = new JSONArray();
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
       
       
        String query = "SELECT id,valoratributo, id||' '||valoratributo||' '||nombre as nombre "
                + "FROM "
                + "  (SELECT id,"
                + "    manglares_nacional.getValorAttributeByName(68,id,94) AS valoratributo,"
                + "    nombre "
                + "  FROM casosestudio "
                + "  WHERE id_car=?"
                + "  AND tipo    =? "
                + "  ORDER BY nombre ASC"
                + "  ) ";
        if(AttibuteValue!=null){
          query=query+" where valoratributo=?";
        }
        try {

            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1,entity);
            pstmt.setString(2,typeCaseStudio);
            if(AttibuteValue!=null){
               pstmt.setString(3,AttibuteValue);
           }          
           
            rs = pstmt.executeQuery();

            while (rs.next()) {
                JSONObject jo = new JSONObject();
                jo.put("id", rs.getString("id"));
                jo.put("name", rs.getString("nombre"));

                jsonarrayData.put(jo);

            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).error(ex.getMessage());
        } catch (JSONException ex) {
            Logger.getLogger(getClass().getName()).error(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).error(ex.getMessage());

        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(getClass().getName()).error(ex.getMessage());
            }

        }

        return jsonarrayData.toString();
    }

    public JSONObject getAllCaseStudioByType(int caseStudioType) {
        JSONObject jsonObject = new JSONObject();
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT nombre , manglares_nacional.getNameSector(id,tipo) as nombresector, "
                + "  manglares_nacional.getValorAttributeByName(44,id,tipo) AS nombrecorto, "
                + "  id, "
                + "  id1, "
                + "  tipo, "
                + "  manglares_nacional.getValorAttributeByName(25,id,tipo) AS nombreregion, "
                + "  manglares_nacional.getValorAttributeByName(29,id,tipo) AS zonaprotegida, "
                + "  manglares_nacional.getValorAttributeByName(30,id,tipo) AS cuencahidrografica, "
                + "  manglares_nacional.getValorAttributeByName(64,id,tipo) AS categoriaUnidadManejo, "
                + "  manglares_nacional.getValorAttributeByName(26,id,tipo) AS UAC, "
                + "  manglares_nacional.getValorAttributeByName(27,id,tipo) AS departamento, "
                + "  nvl(descripcion,'-') as descripcion, "
                + "  id_car                                                 AS CAR_Administra, "
                + "  manglares_nacional.getValorAttributeByName(69,id,tipo) AS azimut, "
                + "  manglares_nacional.getValorAttributeByName(70,id,tipo) AS fechaInstalacion, "
                + "  manglares_nacional.getValorAttributeByName(45,id,tipo) AS area, "
                + "  manglares_nacional.getValorAttributeByName(68,id,tipo) AS formaParcela, "
                + "  manglares_nacional.getValorAttributeByName(85,id,tipo) AS longitud, "
                + "  manglares_nacional.getValorAttributeByName(86,id,tipo) AS latitud, "
                + "  manglares_nacional.getValorAttributeByName(87,id,tipo) AS tipoFisiografico,"
                + "  MANGLARES_NACIONAL.getCountEnableDataByParcel(id)  as datosdisponibles "
                + "FROM casosestudio "
                + "WHERE tipo =? "
                + " AND id_car!='UT' and activo='Activado' "
                + "ORDER BY id desc";
        try {

            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
            
            pstmt.setInt(1, caseStudioType);         
            rs = pstmt.executeQuery();
            JSONArray jsonarrayData = new JSONArray();
            while (rs.next()) {
                JSONObject jo = new JSONObject();
                jo.put("nombre", rs.getString("nombre"));
                jo.put("nombresector", rs.getString("nombresector"));
                jo.put("nombrecorto", rs.getString("nombrecorto"));
                jo.put("id", rs.getString("id"));
                jo.put("id1", rs.getString("id1"));
                jo.put("tipo", rs.getString("tipo"));
                jo.put("nombreregion", rs.getString("nombreregion"));
                jo.put("zonaprotegida", rs.getString("zonaprotegida"));
                jo.put("cuencahidrografica", rs.getString("cuencahidrografica"));
                jo.put("categoriaUnidadManejo", rs.getString("categoriaUnidadManejo"));
                jo.put("UAC", rs.getString("UAC"));
                jo.put("departamento", rs.getString("departamento"));
                jo.put("categoriaUnidadManejo", rs.getString("categoriaUnidadManejo"));
                jo.put("descripcion", rs.getString("descripcion"));
                jo.put("CAR_Administra", rs.getString("CAR_Administra"));
                jo.put("azimut", rs.getString("azimut"));
                jo.put("fechaInstalacion", rs.getString("fechaInstalacion"));
                jo.put("area", rs.getString("area"));
                jo.put("formaParcela", rs.getString("formaParcela"));
                jo.put("longitud", rs.getString("longitud"));
                jo.put("latitud", rs.getString("latitud"));
                jo.put("tipoFisiografico", rs.getString("tipoFisiografico"));
                jo.put("datosdisponibles", rs.getString("datosdisponibles"));

                jsonarrayData.put(jo);

            }
            jsonObject.put("data", jsonarrayData);

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).error(ex.getMessage());

        } catch (JSONException ex) {
            Logger.getLogger(getClass().getName()).error(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).error(ex.getMessage());

        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(getClass().getName()).error(ex.getMessage());
            }

        }
        System.out.println("json string:"+jsonObject);
        return jsonObject;
    }
}
