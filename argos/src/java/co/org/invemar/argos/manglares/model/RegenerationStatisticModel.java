/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.library.siam.exportadores.Data;
import co.org.invemar.library.siam.exportadores.Rows;
import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class RegenerationStatisticModel {

    private LinkedHashSet<String> categoryYearList = null;
    private LinkedHashSet<String> especiesList = null;
    private ArrayList<DatosCalculados> dataList = null;
    private JSONObject jsonData = new JSONObject();
    private ArrayList<String> dataColum = null;
    private ArrayList<Rows> rowsData = null;
    private String variable="";

    public String getStatisticByVariableAndNivelAndCodArea(String variable, int nivel, String codCasoStudio, Map parameters) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        JSONArray jsonArray = new JSONArray();

        dataList = new ArrayList<DatosCalculados>();
        String sqlExt = "";
        switch (nivel) {
            case 1:/*parcel level*/

                sqlExt = " AND codest=" + codCasoStudio;
                break;
            case 2:/*station level*/

                sqlExt = " AND codsitio=" + codCasoStudio;
                break;
            case 3:/*managment unit*/

                sqlExt = " AND  codunidadmanejo=" + codCasoStudio;
                break;
        }

        String query = "";
        this.variable=variable;
            
        if (variable.equalsIgnoreCase("IER")) {
            query = "SELECT ano, "
                    + "   especie||' '||categoria as especie,categoria, "
                    + " (DECODE (categoria,'A',SUM(DENSIDADABSOLUTAVIVOS)/250,'B',SUM(DENSIDADABSOLUTAVIVOS)/25000,'C',SUM(DENSIDADABSOLUTAVIVOS)/2500)) AS dato "
                    + " FROM "
                    + "  (SELECT NIVEL, "
                    + "    ANO, "
                    + "    CODEST, "
                    + "    CODESP, "
                    + "    ESPECIE, "
                    + "    TEMATICA, "
                    + "    categoria, "
                    + "    DENSIDADABSOLUTAVIVOS "
                    + "  FROM MANGLARES_NACIONALES "
                    + "  where  nivel      ="+nivel+" "
                    + "  and categoria is not null "
                    + "  AND tematica =4  "+sqlExt+" )  GROUP BY ano,especie,categoria   ORDER BY ano";
        } else if( variable.equalsIgnoreCase("DENSIDADABSOLUTAVIVOS") || variable.equalsIgnoreCase("ALTURAPROMEDIO") || variable.equalsIgnoreCase("ABUNDANCIAABSOLUTAVIVOS") ||  variable.equalsIgnoreCase("ABUNDANCIARELATIVAVIVOS") ) {
            query = "SELECT especie, ano,  NVL(AVG(" + variable + "),0) AS dato "
                    + "FROM "
                    + "  (SELECT ano,"
                    + "    codesp, "
                    + "    especie, " + variable + ",parcela "
                    + "  FROM manglares_nacionales "
                    + "  WHERE tematica =4 "
                    + "  AND nivel      ="+nivel+""
                    + sqlExt + " "
                    + "  AND categoria='" + (String) parameters.get("categoria") + "' "
                    + "  ORDER BY ano, "
                    + "    categoria, "
                    + "    especie "
                    + "  ) "
                    + "GROUP BY especie,ano "
                    + "ORDER BY ano,especie ";
        }else{
            String sqlExt1="";
            if (variable.equalsIgnoreCase("apa") || variable.equalsIgnoreCase("dpa") || variable.equalsIgnoreCase("dproa") ) {
                sqlExt1=" AND codesp    IS  NULL ";
            }else{
               sqlExt1=" AND codesp    IS NOT NULL "; 
            }
            
            query = "SELECT ano, "
                    + "  codesp, "
                    + " nvl(especie,'Todas las especies') as especie,"
                    + "  CASE '"+variable+"' "
                    + "    WHEN 'abt' "
                    + "    THEN num "
                    + "    WHEN 'abm' "
                    + "    THEN abundanciaabsolutamuertos "
                    + "    WHEN 'abv' "
                    + "    THEN abundanciaabsolutavivos "
                    + "    WHEN 'np' "
                    + "    THEN numeropropagulos "
                    + "    WHEN 'abrv' "
                    + "    THEN abundanciarelativavivos "
                    + "    WHEN 'abrm' "
                    + "    THEN abundanciarelativamuertos "
                    + "    WHEN 'dav' "
                    + "    THEN densidadabsolutavivos "
                    + "    WHEN 'dam' "
                    + "    THEN densidadabsolutamuertos "
                    + "    WHEN 'ap'  THEN alturapromedio "
                    + "    WHEN 'apa'  THEN alturapromedio "
                    + "    WHEN 'da' "
                    + "    THEN densidad "
                    + "    WHEN 'ab' "
                    + "    THEN ROUND(AB_MH,3) "
                    + "    WHEN 'aba' "
                    + "    THEN ROUND(NUM,3) "
                    + "    WHEN 'abr' "
                    + "    THEN ROUND(ABUND_REL,3) "
                    + "    WHEN 'fr' "
                    + "    THEN ROUND(FREC_REL,3) "
                    + "    WHEN 'ivi' "
                    + "    THEN ROUND(IVI,3) "
                    + "    WHEN 'fa' "
                    + "    THEN ROUND(FREC_ABS,3) "
                    + "    WHEN 'dr' "
                    + "    THEN ROUND(DOM_REL,3) "
                    + "    WHEN 'dp' "
                    + "    THEN ROUND(DENSIDADPROPALUGOS,3) "
                    + "    WHEN 'dplantulas' THEN ROUND(DENSIDADPLANTULAS,3) "
                    + "    WHEN 'dpa' THEN ROUND(DENSIDADPLANTULAS,3) "
                    + "    ELSE 0 "
                    + "  END AS dato "
                    + "FROM MANGLARES_NACIONALES "
                    + "WHERE tematica ='4' "
                    + "AND nivel      ="+nivel+" "
                    +  sqlExt1 +" "
                    + sqlExt + " "
                    + "AND categoria IS NULL "
                    + "ORDER BY ano,especie";
        }

        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            //pstmt.setInt(1, nivel);
            

           //System.out.println("query:" + query);
            rs = pstmt.executeQuery();
            especiesList = new LinkedHashSet<String>();
            categoryYearList = new LinkedHashSet<String>();

            dataColum = new ArrayList<String>();
            dataColum.add("Especie");
            dataColum.add("A�io");
            dataColum.add("dato");

            rowsData = new ArrayList<Rows>();

            while (rs.next()) {
                JSONObject jo = new JSONObject();

                jo.put("especie", rs.getString("especie"));              
                jo.put("dato", rs.getString("dato"));
                jo.put("ano", rs.getString("ano"));
                jsonArray.put(jo);

                Rows r = new Rows();
                r.addData(new Data(rs.getString("especie")));
                r.addData(new Data(rs.getString("ano")));
                r.addData(new Data(rs.getString("dato")));
                rowsData.add(r);

                categoryYearList.add(rs.getString("ano"));

                especiesList.add(rs.getString("especie"));

                DatosCalculados dc = new DatosCalculados();
                dc.setEspecie(rs.getString("especie"));
                dc.setValor(rs.getDouble("dato"));
                dc.setAnio(rs.getString("ano"));
                dataList.add(dc);

            }
            jsonData.put("data", jsonArray);

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (JSONException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;
        }
        return jsonArray.toString();
    }

    public String getJsonDataStatistic() {
        String jsonString = "";

        
        try {

            JSONArray catetoriesArray = new JSONArray();
            for (String category : categoryYearList) {
                catetoriesArray.put(category);
            }
            jsonData.put("categories", catetoriesArray);

            JSONArray series = new JSONArray();

            for (String especie : especiesList) {
                JSONObject joe = new JSONObject();
                joe.put("name", especie);
                JSONArray jArrayDataByEspecie = new JSONArray();
                for (DatosCalculados dc : dataList) {                   
                    if (especie.equalsIgnoreCase(dc.getEspecie())) {
                         JSONObject jsonCoordenate = new JSONObject();
                       
                            jsonCoordenate.put("x", dc.getAnio());
                            jsonCoordenate.put("y", dc.getValor());                           
                            jArrayDataByEspecie.put(jsonCoordenate);
                       
                        
                    }
                }
                joe.put("data", jArrayDataByEspecie);
                series.put(joe);
            }
            
            jsonData.put("series", series);
            jsonString = jsonData.toString();

        } catch (JSONException ex) {
            Logger.getLogger(RegenerationStatisticModel.class.getName()).log(Level.SEVERE, null, ex);
        }

        return jsonString;
    }

    public ArrayList<String> getDataColum() {
        return dataColum;
    }

    public void setDataColum(ArrayList<String> dataColum) {
        this.dataColum = dataColum;
    }

    public ArrayList<Rows> getRowsData() {
        return rowsData;
    }

    public void setRowsData(ArrayList<Rows> rowsData) {
        this.rowsData = rowsData;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

}
