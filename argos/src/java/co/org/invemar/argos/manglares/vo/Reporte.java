/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.manglares.vo;

/**
 *
 * @author Daniel Gonzalez
 */
public class Reporte {
    private String factores;
    private String presiones;
    private String estados;
    private String impactos;
    private String actividadesEconomicas;
    private String id;
    private String fecha;
    private String entidad;
    
    

    public String getFactores() {
        return factores;
    }

    public void setFactores(String factores) {
        this.factores = factores;
    }

    public String getPresiones() {
        return presiones;
    }

    public void setPresiones(String presiones) {
        this.presiones = presiones;
    }

    public String getEstados() {
        return estados;
    }

    public void setEstados(String estados) {
        this.estados = estados;
    }

    public String getImpactos() {
        return impactos;
    }

    public void setImpactos(String impactos) {
        this.impactos = impactos;
    }

    public String getActividadesEconomicas() {
        return actividadesEconomicas;
    }

    public void setActividadesEconomicas(String actividadesEconomicas) {
        this.actividadesEconomicas = actividadesEconomicas;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }
    
}
