/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.TablaGrafica;
import co.org.invemar.library.siam.metadatos.Metadatos;
import co.org.invemar.library.siam.vo.Responsable;
import co.org.invemar.siam.sibm.vo.Especie;
import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author usrsig15
 */
public class Graficador {

    private boolean resultadoObtenido = false;
    private List<TablaGrafica> tablavalores = null;
    private ArrayList<Responsable> responsables = null;

    private HashSet<String> especies = new HashSet<String>();
    private HashSet<String> anios = new HashSet<String>();
    private ArrayList<DatosCalculados> valores = new ArrayList<DatosCalculados>();
    private Map<String, String> codeEspeciesAnios = new HashMap<String, String>();

    private CartesianChartModel categoryModel = new CartesianChartModel();
    private ChartSeries variables = new ChartSeries();

    private Metadatos metadatos = new Metadatos();
    private String cadenaAnios = "";

    TablaGrafica tablagrafica = null;

    public CartesianChartModel getValoresFQ(String codParcela, String variable, String nivel) {
        CartesianChartModel linearModel = new CartesianChartModel();

        LineChartSeries prom = new LineChartSeries();
        LineChartSeries max = new LineChartSeries();
        LineChartSeries min = new LineChartSeries();
        LineChartSeries desviacion = new LineChartSeries();

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String nombreVariable;

        String query = "SELECT NIVEL,\n"
                + "  ANO,\n"
                + "  PROM,\n"
                + "  MAX,\n"
                + "  MIX,\n"
                + "  DESVEST,\n"
                + "  VARIABLE,\n"
                + "  CUALIDAD,\n"
                + "  codest\n"
                + "FROM MANGLARES_NACIONALES_FQ  where  nivel=1 and codest=" + codParcela + " and variable='" + variable + "'and cualidad='" + nivel + "' ";
        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();

            prom.setLabel("Promedio");
            max.setLabel("M�ximos");
            min.setLabel("M�nimos");
            desviacion.setLabel("Desviacion  ");
            while (rs.next()) {
                prom.set(rs.getString("ANO"), rs.getDouble("PROM"));
                max.set(rs.getString("ANO"), rs.getDouble("MAX"));
                min.set(rs.getString("ANO"), rs.getDouble("MIX"));
                desviacion.set(rs.getString("ANO"), rs.getDouble("DESVEST"));

            }
            linearModel.addSeries(prom);
            linearModel.addSeries(max);
            linearModel.addSeries(min);
            linearModel.addSeries(desviacion);
        } catch (SQLException ex) {
            System.out.println("Error en el objeto Graficador en el metodo getValoresFQ:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return linearModel;
    }

    public CartesianChartModel getValoresCalculadosPorVariable(String nivel, String variable, String sector, String sitio, String unidadManejo, String tipoUnidadManejo, String parcela, String codigoEspecie, String tematica) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String nombreVariable;
        String queryModificado = "";

        String query = "SELECT ano,codesp,\n"
                + "  especie,\n"
                + "  CASE '" + variable + "'\n"
                + " WHEN 'abt' THEN num "
                + " WHEN 'abm' THEN abundanciaabsolutamuertos "
                + " WHEN 'abv' THEN abundanciaabsolutavivos "
                + " WHEN 'np' THEN numeropropagulos "
                + " WHEN 'abrv' THEN abundanciarelativavivos "
                + " WHEN 'abrm' THEN abundanciarelativamuertos "
                + " WHEN 'dav' THEN densidadabsolutavivos "
                + " WHEN 'dam' THEN densidadabsolutamuertos "
                + " WHEN 'ap' THEN alturapromedio "
                + " WHEN 'da' THEN densidad "
                + " WHEN 'ab' THEN round(AB_MH,3)"
                + " WHEN 'aba' THEN round(NUM,3) "
                + " WHEN 'abr' THEN round(ABUND_REL,3)"
                + " WHEN 'fr' THEN round(FREC_REL,3) "
                + " WHEN 'ivi' THEN round(IVI,3) "
                + " WHEN 'fa' THEN round(FREC_ABS,3)"
                + " WHEN 'dr' THEN round(DOM_REL,3) "
                + " WHEN 'dp' THEN round(DENSIDADPROPALUGOS,3)"
                + " WHEN 'dplantulas' THEN round(DENSIDADPLANTULAS,3)"
                + "  ELSE 0\n"
                + "  END as valor\n"
                + "FROM MANGLARES_NACIONALES\n"
                + "WHERE tematica='" + tematica + "' and  nivel=" + nivel + " ";

        if (codigoEspecie != null) {
            if (codigoEspecie.equals("te")) {
                queryModificado = " and codesp is null ";
            } else if (codigoEspecie.equals("tde")) {
                queryModificado = " and codesp is not null ";
            } else {
                queryModificado = " and codesp='" + codigoEspecie + "'";
            }
        }

        if (nivel.equals("1")) {
            if (parcela != null && (!parcela.equals("te") || !parcela.equals("tde"))) {
                queryModificado = queryModificado + " and codest='" + parcela + "' and categoria is null ";
            }

        }
        if (nivel.equals("2")) {

            if (sitio != null && (!sitio.equals("te") || !sitio.equals("tde"))) {
                queryModificado = queryModificado + " and codsitio=" + sitio + " and categoria is null ";
            }
        }

        if (nivel.equals("3")) {

            if (unidadManejo != null && (!unidadManejo.equals("te") || !unidadManejo.equals("tde"))) {

                queryModificado = queryModificado + " and codunidadmanejo=" + unidadManejo + " and categoria is null ";
            }
        }

        queryModificado = queryModificado + " order by ano,especie";
        query = query + queryModificado;

        
       // System.out.println("query:"+query);
        
        if (variable.equals("ab")) {
            nombreVariable = "�rea Basal.";
        } else if (variable.equals("aba")) {
            nombreVariable = "Abundancia absoluta.";
        } else if (variable.equals("da")) {
            nombreVariable = "Densidad Absoluta.";
        } else if (variable.equals("abr")) {
            nombreVariable = "Abundancia relativa.";
        } else if (variable.equals("fr")) {
            nombreVariable = "Frecuencia Relativa.";
        } else if (variable.equals("ivi")) {
            nombreVariable = "IVI.";
        } else if (variable.equals("fa")) {
            nombreVariable = "Frecuencia Absoluta.";
        } else if (variable.equals("dr")) {
            nombreVariable = "Dominancia relativa.";
        } else if (variable.equals("dplantulas")) {
            nombreVariable = "Densidad de plantulas.";
        } else if (variable.equals("dp")) {
            nombreVariable = "Densidad de prop&oacute;gulos.";
        } else {
            nombreVariable = "Variable no convertida";
        }

        variables.setLabel(nombreVariable);

        try {
            con = conectionfactory.createConnection("monitoreom");
          
            pstmt = con.prepareStatement(query);

            rs = pstmt.executeQuery();
            tablavalores = new ArrayList<TablaGrafica>();

            
            if (nivel.equals("3")) {
                if (codigoEspecie.equals("te")) {
                    settingTodaElArea(con,Long.parseLong(sector),tematica,rs);
                } else if (codigoEspecie.equals("tde")) {
                    settingModelGrafico(con,Long.parseLong(sector),tematica,rs);
                } else {
                    settingModelPorEspecie(con,Long.parseLong(sector),tematica,rs);
                }
            } else if (nivel.equals("2")) {
                if (codigoEspecie.equals("te")) {
                    settingTodaElArea(con,Long.parseLong(sector),tematica,rs);
                } else if (codigoEspecie.equals("tde")) {
                    settingModelGrafico(con,Long.parseLong(sector),tematica,rs);
                } else {
                    settingModelPorEspecie(con,Long.parseLong(sector),tematica,rs);
                }

            } else if (nivel.equals("1")) {
                if (codigoEspecie.equals("te")) {
                    settingTodaElArea(con,Long.parseLong(sector),tematica,rs);
                } else if (codigoEspecie.equals("tde")) {
                    settingModelGrafico(con,Long.parseLong(sector),tematica,rs);
                } else {
                    settingModelPorEspecie(con,Long.parseLong(sector),tematica,rs);
                }

            }
        } catch (Exception ex) {
            System.out.println("Error en el objeto Graficador en el metodo getValoresCalculadosPorVariable:" + ex.getMessage());
            categoryModel = new CartesianChartModel();
            ChartSeries series = new ChartSeries();
            series.setLabel(" ");
            series.set("0", 0);
            categoryModel.addSeries(series);
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }

        return categoryModel;
    }

    public CartesianChartModel getPromedioTasasCrecimiento(String nivel, String sector, String sitio, String unidadManejo, String parcela) {

        Connection con = null;
        resultadoObtenido = false;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String nombreVariable;
        String queryExtend = "";

        //System.out.println("nivel:" + nivel);

        if (nivel.equalsIgnoreCase("1")) {            
            
            if (parcela != null) {
                queryExtend = " and  codparcela=" + parcela + " ";
            }
        } else if (nivel.equalsIgnoreCase("2")) {          
            if (sitio != null) {
                queryExtend = " and codsitio=" + sitio + " ";
            }
            

        } else if (nivel.equalsIgnoreCase("3")) {          
            if (sitio != null) {
                queryExtend = " and codunidadmanejo=" + unidadManejo + " ";
            }

        }

        String query = "SELECT\n"
                + "  ANIO,\n"
                + "  round(avg(TASACRECIMIENTO),3) as promediocrecimiento,\n"
                + "  ESPECIE,nombreespecie, \n"
                + " nivel\n"
                + "FROM\n"
                + "  TEMP_TASAS_CRECIMIENTO where nivel=" + nivel + " " + queryExtend + "  group by especie,nombreespecie, anio,nivel order by anio asc";

        nombreVariable = "Promedio TasaCrecimiento.";
        variables.setLabel(nombreVariable);

        try {
            con = conectionfactory.createConnection("manglar");
            //System.out.println("consulta:" + query);
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            settingModelGraficoTasaCrecimiento(Long.parseLong(sector),rs);

            resultadoObtenido = true;

        } catch (Exception ex) {

            System.out.println("Error en el objeto Graficador en el metodo getPromedioTasasCrecimiento:" + ex.getMessage());
            categoryModel = new CartesianChartModel();
            ChartSeries series = new ChartSeries();
            series.setLabel(" ");
            series.set("0", 0);
            categoryModel.addSeries(series);
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }

        return categoryModel;
    }

    private void settingModelGraficoTasaCrecimiento(long codSector, ResultSet rs) throws Exception {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        con = conectionfactory.createConnection("monitoreom");
        
        tablavalores = new ArrayList<TablaGrafica>();

        List<String> lanios = new ArrayList<String>();

        while (rs.next()) {

            especies.add(rs.getString("nombreespecie"));

            lanios.add(rs.getString("anio"));

            codeEspeciesAnios.put(rs.getString("nombreespecie") + "-" + rs.getString("anio"), rs.getString("promediocrecimiento"));

            tablagrafica = new TablaGrafica();
            tablagrafica.setAnio(rs.getString("anio"));
            tablagrafica.setValor(rs.getDouble("promediocrecimiento"));
            tablagrafica.setNombreEspecie(rs.getString("nombreespecie"));
            tablavalores.add(tablagrafica);

        }
        Collections.sort(lanios);

        Iterator<String> it = especies.iterator();
        double valor = 0;

        while (it.hasNext()) {
            String e = it.next();

            ChartSeries serie = new ChartSeries();
            Iterator<DatosCalculados> itValores = valores.iterator();
            serie.setLabel(e);

            Iterator<String> ianios = lanios.iterator();

            while (ianios.hasNext()) {
                String a = ianios.next();

                if (codeEspeciesAnios.containsKey(e + "-" + a)) {
                    serie.set(a, Double.parseDouble(codeEspeciesAnios.get(e + "-" + a)));
                    valor = Double.parseDouble(codeEspeciesAnios.get(e + "-" + a));

                } else {
                    valor = 0;
                    serie.set(a, valor);

                }

                cadenaAnios = cadenaAnios + "'" + a + "',";

            }

            categoryModel.addSeries(serie);
           
            
            

        }
        responsables = metadatos.getResponsableDatosBySector(con, codSector, cadenaAnios, "4");
           

    }

    private void settingModelGrafico(Connection con, long codSector,String tematica,ResultSet rs) throws Exception {
        List<String> lanios = new ArrayList<String>();
        valores.clear();
        especies.clear();
        codeEspeciesAnios.clear();
        tablavalores.clear();
        while (rs.next()) {

            resultadoObtenido = true;

            Especie e = new Especie();
            e.setNombre(rs.getString("especie"));
            e.setCodEspecie(rs.getString("codesp"));
            especies.add(rs.getString("especie"));

            lanios.add(rs.getString("ano"));

            Collections.sort(lanios);

            codeEspeciesAnios.put(rs.getString("especie") + "-" + rs.getString("ano"), rs.getString("valor"));

            tablagrafica = new TablaGrafica();
            tablagrafica.setAnio(rs.getString("ano"));
            tablagrafica.setValor(Double.parseDouble(rs.getString("valor")));
            tablagrafica.setNombreEspecie(rs.getString("especie"));
            tablavalores.add(tablagrafica);

            DatosCalculados dc = new DatosCalculados();
            dc.setCodEspecie(rs.getString("codesp"));
            dc.setAnio(rs.getString("ano"));
            dc.setValor(rs.getDouble("valor"));
            dc.setEspecie(rs.getString("especie"));
            valores.add(dc);
        }

        Iterator<DatosCalculados> itDC = valores.iterator();
        while (itDC.hasNext()) {
            DatosCalculados dc = itDC.next();
           // System.out.println("anio:" + dc.getAnio() + "- Especie:" + dc.getEspecie() + "- Valor:" + dc.getValor());
        }

        Iterator<String> it = especies.iterator();
        double valor = 0;

        //-- System.out.println("cantidad de especies:"+especies.size());
        //-- System.out.println("cantidad de codeEspeciesAnios:"+codeEspeciesAnios.size());
        while (it.hasNext()) {
            String e = it.next();
            ChartSeries serie = new ChartSeries();
            serie.setLabel(e);
            Iterator<String> ianios = lanios.iterator();

            //-- System.out.println("cantidad de ianios:"+lanios.size());
            while (ianios.hasNext()) {
                String a = ianios.next();
                //System.out.println("anios:" + a);
                if (codeEspeciesAnios.containsKey(e + "-" + a)) {
                    serie.set(a, Double.parseDouble(codeEspeciesAnios.get(e + "-" + a)));
                    valor = Double.parseDouble(codeEspeciesAnios.get(e + "-" + a));
                } else {
                    valor = 0;
                    serie.set(a, valor);

                }

                cadenaAnios = cadenaAnios + "'" + a + "',";

            }
            categoryModel.addSeries(serie);
            responsables = metadatos.getResponsableDatosBySector(con,codSector,cadenaAnios, tematica);

        }

    }

    private void settingModelPorEspecie(Connection con, long codSector,String tematica,ResultSet rs) throws Exception {
        while (rs.next()) {
            resultadoObtenido = true;
            variables.set(rs.getString("ano"), rs.getDouble("valor"));

            tablagrafica = new TablaGrafica();
            tablagrafica.setAnio(rs.getString("ano"));
            tablagrafica.setValor(rs.getDouble("valor"));
            tablagrafica.setNombreEspecie(rs.getString("especie"));
            tablavalores.add(tablagrafica);

            cadenaAnios = cadenaAnios + "'" + rs.getString("ano") + "',";
        }
        categoryModel.addSeries(variables);
        responsables = metadatos.getResponsableDatosBySector(con,codSector,cadenaAnios, tematica);
    }

    private void settingTodaElArea(Connection con, long codSector,String tematica, ResultSet rs) throws Exception {
        variables.setLabel("Todas las especies");
        tablavalores.clear();
        while (rs.next()) {
            resultadoObtenido = true;
            variables.set(rs.getString("ano"), rs.getDouble("valor"));

            tablagrafica = new TablaGrafica();
            tablagrafica.setAnio(rs.getString("ano"));
            tablagrafica.setValor(rs.getDouble("valor"));
            tablavalores.add(tablagrafica);
            cadenaAnios = cadenaAnios + "'" + rs.getString("ano") + "',";

        }
        responsables = metadatos.getResponsableDatosBySector(con,codSector,cadenaAnios, tematica);
        categoryModel.addSeries(variables);
    }

    public boolean isResultadoObtenido() {
        return resultadoObtenido;
    }

    public void setResultadoObtenido(boolean resultadoObtenido) {
        this.resultadoObtenido = resultadoObtenido;
    }

    public List<TablaGrafica> getTablavalores() {
        return tablavalores;
    }

    public void setTablavalores(List<TablaGrafica> tablavalores) {
        this.tablavalores = tablavalores;
    }

    public ArrayList<Responsable> getResponsables() {
        return responsables;
    }

    public void setResponsables(ArrayList<Responsable> responsables) {
        this.responsables = responsables;
    }
}
