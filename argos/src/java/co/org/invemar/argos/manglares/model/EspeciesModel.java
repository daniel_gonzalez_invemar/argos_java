/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.util.ConnectionFactory;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class EspeciesModel {

    public Map<String, String> getTodosLasEspeciesConEstadistica(String tematica) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> especies = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT distinct codesp, especie FROM MANGLARES_NACIONALES where codesp is not null and tematica=?";
        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, tematica);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                especies.put(rs.getString("especie"), rs.getString("codesp"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, ex.getMessage());

        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(EspeciesModel.class.getName()).log(Level.SEVERE, ex.getMessage());
            }
        }
        return especies;
    }

    public String getAllSpeciesByProject(String idProject) {
        JSONArray jsonarrayData = new JSONArray();
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "select clave_sibm as id, descripcion as name from MONITOREOM.BLST_ESPECIES where proyecto=? order by descripcion ";
        try {

            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1,idProject);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                JSONObject jo = new JSONObject();
                jo.put("id", rs.getString("id"));
                jo.put("name", rs.getString("name"));

                jsonarrayData.put(jo);

            }

        } catch (SQLException ex) {
            Logger.getLogger(EspeciesModel.class.getName()).log(Level.SEVERE, ex.getMessage());

        } catch (JSONException ex) {
            Logger.getLogger(EspeciesModel.class.getName()).log(Level.SEVERE, ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(EspeciesModel.class.getName()).log(Level.SEVERE, ex.getMessage());

        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(EspeciesModel.class.getName()).log(Level.SEVERE, ex.getMessage());
            }

        }

        return jsonarrayData.toString();
    }

}
