/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.vo;

import java.io.InputStream;
import java.io.Serializable;

/**
 *
 * @author usrsig15
 */
public class SectorManglarVo  implements Serializable{

    private int id;
    private String Region;
    private String UAC;
    private String Departamento;
    private String cuencaHidrografica;
    private String coordenadasGeograficas;
    private double extension;
    private String isAreaProtegida;
    private String AutoridadAmbientalACargo;
    private String impactosConocidos;
    private String nivelIntervencionAntropica;
    private String actividadesEconomnicas;
    private String descripcion;
    private String registroFotografico;
    private String model;
    private int codProyecto;
    private String activo;
   
    private String isActivo;
    private String abreriatura;
    private String nombre;
    private String areaProtegida;
    private String idCAR;
    private String textoDepartamento;
    private InputStream filegeografico;
    private String[] entities; 
    private String isSectorAssociate;

    public String getActivo() {
        return activo;
    }  

    public void setActivo(String activo) {
        this.activo = activo;
    }    

    public String getIsActivo() {
        return isActivo;
    }

    public void setIsActivo(String isActivo) {
        this.isActivo = isActivo;
    }    
    

    public int getCodProyecto() {
        return codProyecto;
    }

    public void setCodProyecto(int codProyecto) {
        this.codProyecto = codProyecto;
    }

    public String getModel() {
        return model;
    }

    public String getAbreriatura() {
        return abreriatura;
    }

    public void setAbreriatura(String abreriatura) {
        this.abreriatura = abreriatura;
    }
   

    public String getIdCAR() {
        return idCAR;
    }

    public void setIdCAR(String idCAR) {
        this.idCAR = idCAR;
    }
    

    public String getAreaProtegida() {
        return areaProtegida;
    }

    public void setAreaProtegida(String areaProtegida) {
        this.areaProtegida = areaProtegida;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getUAC() {
        return UAC;
    }

    public void setUAC(String UAC) {
        this.UAC = UAC;
    }

    public String getDepartamento() {
        return Departamento;
    }

    public void setDepartamento(String Departamento) {
        this.Departamento = Departamento;
    }

    public String getCuencaHidrografica() {
        return cuencaHidrografica;
    }

    public void setCuencaHidrografica(String cuencaHidrografica) {
        this.cuencaHidrografica = cuencaHidrografica;
    }

    public String getCoordenadasGeograficas() {
        return coordenadasGeograficas;
    }

    public void setCoordenadasGeograficas(String coordenadasGeograficas) {
        this.coordenadasGeograficas = coordenadasGeograficas;
    }

    public double getExtension() {
        return extension;
    }

    public void setExtension(double extension) {
        this.extension = extension;
    }

    public String getIsAreaProtegida() {
        return isAreaProtegida;
    }

    public void setIsAreaProtegida(String isAreaProtegida) {
        this.isAreaProtegida = isAreaProtegida;
    }

    public String getAutoridadAmbientalACargo() {
        return AutoridadAmbientalACargo;
    }

    public void setAutoridadAmbientalACargo(String AutoridadAmbientalACargo) {
        this.AutoridadAmbientalACargo = AutoridadAmbientalACargo;
    }

    public String getImpactosConocidos() {
        return impactosConocidos;
    }

    public void setImpactosConocidos(String impactosConocidos) {
        this.impactosConocidos = impactosConocidos;
    }

    public String getNivelIntervencionAntropica() {
        return nivelIntervencionAntropica;
    }

    public void setNivelIntervencionAntropica(String nivelIntervencionAntropica) {
        this.nivelIntervencionAntropica = nivelIntervencionAntropica;
    }

    public String getActividadesEconomnicas() {
        return actividadesEconomnicas;
    }

    public void setActividadesEconomnicas(String actividadesEconomnicas) {
        this.actividadesEconomnicas = actividadesEconomnicas;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRegistroFotografico() {
        return registroFotografico;
    }

    public void setRegistroFotografico(String registroFotografico) {
        this.registroFotografico = registroFotografico;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTextoDepartamento() {
        return textoDepartamento;
    }

    public void setTextoDepartamento(String textoDepartamento) {
        this.textoDepartamento = textoDepartamento;
    }

    public InputStream getFilegeografico() {
        return filegeografico;
    }

    public void setFilegeografico(InputStream filegeografico) {
        this.filegeografico = filegeografico;
    }

    public String[] getEntities() {
        return entities;
    }

    public void setEntities(String[] entities) {
        this.entities = entities;
    }

    public String getIsSectorAssociate() {
        return isSectorAssociate;
    }

    public void setIsSectorAssociate(String isSectorAssociate) {
        this.isSectorAssociate = isSectorAssociate;
    }
   
}
