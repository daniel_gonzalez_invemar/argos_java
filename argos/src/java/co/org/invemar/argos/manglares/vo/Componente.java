/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.manglares.vo;

/**
 *
 * @author usrsig15
 */
public class Componente {
    private int id;
    private String nombre="";

    
    public Componente( ){
       
    }
    
    public Componente( int id, String nombre){
        this.id= id;
        this.nombre = nombre;
    }
    
    public Componente( int id){
      this.id= id;  
    }
    public Componente( String nombre){
      this.nombre = nombre;  
    }     
   
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
