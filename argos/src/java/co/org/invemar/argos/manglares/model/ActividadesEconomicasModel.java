/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author usrsig15
 */
public class ActividadesEconomicasModel {

    public Map<String, String> getTodosLasActividadesEconomicas() {
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> actividadesEconomicas = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "select id, valor from valores where atributos_codigo=2 order by valor";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
           // System.out.println("query actividades economicas:" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                actividadesEconomicas.put(rs.getString("valor"), rs.getString("id"));
            }

        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto ActividadesEconomicas en el metodo getTodosLasActividadesEconomicas:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return actividadesEconomicas;
    }
}
