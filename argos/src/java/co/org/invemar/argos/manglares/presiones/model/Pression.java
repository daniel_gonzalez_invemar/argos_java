/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.manglares.presiones.model;

import co.org.invemar.library.ushahidi.Category;




/**
 *
 * @author usrsig15
 */
public class Pression extends Category {
    private int Intensity = 0;
    private String intensityDescripcion;
    private int irreversibilty = 0;
    private String irreversibilityDescription;
    private int extension = 0;
    private String extensioDescription;
    private int frecuency = 0;
    private String frecuencyDescription;
    private int persistence=0;
    private String persistenceDescription;
    private int tendency=0;
    private String tendencyDescription;
    


    public int getIntensity() {
        return Intensity;
    }

    public void setIntensity(int Intensity) {
        this.Intensity = Intensity;
    }

    public int getIrreversibilty() {
        return irreversibilty;
    }

    public void setIrreversibilty(int irreversibilty) {
        this.irreversibilty = irreversibilty;
    }

    public int getExtension() {
        return extension;
    }

    public void setExtension(int extension) {
        this.extension = extension;
    }

    public int getFrecuencia() {
        return frecuency;
    }

    public void setFrecuencia(int ocurrencia) {
        this.frecuency = ocurrencia;
    }

    public String getIntensityDescripcion() {
        return intensityDescripcion;
    }

    public void setIntensityDescripcion(String intensityDescripcion) {
        this.intensityDescripcion = intensityDescripcion;
    }

    public String getIrreversibilityDescription() {
        return irreversibilityDescription;
    }

    public void setIrreversibilityDescription(String irreversibilityDescription) {
        this.irreversibilityDescription = irreversibilityDescription;
    }

    public String getExtensioDescription() {
        return extensioDescription;
    }

    public void setExtensioDescription(String extensioDescription) {
        this.extensioDescription = extensioDescription;
    }

    public String getFrecuencyDescription() {
        return frecuencyDescription;
    }

    public void setFrecuencyDescription(String frecuencyDescription) {
        this.frecuencyDescription = frecuencyDescription;
    }

    public int getFrecuency() {
        return frecuency;
    }

    public void setFrecuency(int frecuency) {
        this.frecuency = frecuency;
    }

    public int getPersistence() {
        return persistence;
    }

    public void setPersistence(int persistence) {
        this.persistence = persistence;
    }

    public String getPersistenceDescription() {
        return persistenceDescription;
    }

    public void setPersistenceDescription(String persistenceDescription) {
        this.persistenceDescription = persistenceDescription;
    }

    public int getTendency() {
        return tendency;
    }

    public void setTendency(int tendency) {
        this.tendency = tendency;
    }

    public String getTendencyDescription() {
        return tendencyDescription;
    }

    public void setTendencyDescription(String tendencyDescription) {
        this.tendencyDescription = tendencyDescription;
    }

}
