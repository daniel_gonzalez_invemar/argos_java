package co.org.invemar.argos.manglares.presiones.model;


public class DataIndicatorBySite {
    private int idSitio;
    private String sectorName;
    private String sitioName;
    private String presionName;    
    private double Presiones;
    private int numero;

    public void setIdSitio(int idSitio) {
        this.idSitio = idSitio;
    }

    public int getIdSitio() {
        return idSitio;
    }


    public DataIndicatorBySite(String sectorName, int idSitio, String sitioName, String presionName, double presiones){
        this.idSitio = idSitio;
        this.sectorName = sectorName;
        this.sitioName = sitioName;
        this.presionName = presionName;
        this.Presiones = presiones;
    }

 
    public void setSitioName(String sitioName) {
        this.sitioName = sitioName;
    }

    public String getSitioName() {
        return sitioName;
    }

    public void setPresionName(String presionName) {
        this.presionName = presionName;
    }

    public String getPresionName() {
        return presionName;
    }

    public void setSectorName(String sectorName) {
        this.sectorName = sectorName;
    }

    public String getSectorName() {
        return sectorName;
    }

    public void setPresiones(double presiones) {
        this.Presiones = presiones;
    }

    public double getPresiones() {
        return Presiones;
    }

    public DataIndicatorBySite() {
        super();
    }
}
