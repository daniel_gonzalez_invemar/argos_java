/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.vo;

import co.org.invemar.argos.UI.Beans.casos.manglares.Nodos;

/**
 *
 * @author usrsig15
 */
public class Presion extends Nodos {
    
    private String intensidadCualitativa;
    private String frecuenciaCualitativa;
    
    public Presion() {
      
    } 
    
    public Presion(String nombre, String id, String tipo) {
        super(nombre, id, tipo);
    }  

    public String getIntensidadCualitativa() {
        return intensidadCualitativa;
    }

    public void setIntensidadCualitativa(String intensidadCualitativa) {
        this.intensidadCualitativa = intensidadCualitativa;
    }

    public String getFrecuenciaCualitativa() {
        return frecuenciaCualitativa;
    }

    public void setFrecuenciaCualitativa(String frecuenciaCualitativa) {
        this.frecuenciaCualitativa = frecuenciaCualitativa;
    }
   
    
    
    
    
}
