/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.UnidadManejo;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.ResultadoTransaccion;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class UnidadManejoModel implements Serializable{

    private ArrayList<UnidadManejo> unidadesManejo;
    private String jsonDataManagmentUnit;

    public UnidadManejoModel() {
        unidadesManejo = new ArrayList<UnidadManejo>();
    }

    private void getDatosCaso(UnidadManejo unidadManejo, int id, Connection con) {

        PreparedStatement pstmt = null;
        ResultSet rs1 = null;    
        try {

            String query = "select * from casosestudio_atributos where casosestudio_id=?";
            pstmt = con.prepareStatement(query);
            pstmt.setInt(1, unidadManejo.getId());
           
            rs1 = pstmt.executeQuery();

            while (rs1.next()) {
                if (rs1.getString("CODIGO_ATRIBUTO").equals("64")) {
                    unidadManejo.setCategoria(rs1.getInt("valores_codigo"));
                }

            }
        } catch (Exception ex) {
            System.out.println("Error en el objeto UnidadManejoModel  m�todo getDatosCaso:" + ex.getMessage());

        } finally {
            try {
                rs1.close();
                rs1=null;
            } catch (SQLException ex) {
                Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ArrayList<UnidadManejo> getTodosLasUnidadesManejoPorCorporacion(int idSitio, String idCAR, int idProyecto) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        unidadesManejo.clear();
        try {

            String query = "SELECT ID,\n"
                    + "  NOMBRE,\n"
                    + "  ID1,\n"
                    + "  ADJUNTO,\n"
                    + "  TIPO,\n"
                    + "  ID_CAR,\n"
                    + "  ACTIVO,\n"
                    + "  CODPROYECTO,\n"
                    + "  DESCRIPCION\n"
                    + "FROM CASOSESTUDIO  where id1=? and id_car=? AND CODPROYECTO=? and tipo=2";
            con = conectionfactory.createConnection("manglar");         
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setInt(1, idSitio);
            pstmt.setString(2, idCAR);
            pstmt.setInt(3, idProyecto);
            getDatosUnidadManejo(pstmt, con);
        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto UnidadManejoModel  m�todo getTodosLasUnidadesManejoPorCorporacion:" + ex.getMessage());
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        return unidadesManejo;
    }
    
     public ArrayList<UnidadManejo> getTodosLasUnidadesManejoPorSector(int idSector, int idProyecto) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        unidadesManejo.clear();
        try {

            String query = "SELECT ID,\n"
                    + "  NOMBRE,\n"
                    + "  ID1,\n"
                    + "  ADJUNTO,\n"
                    + "  TIPO,\n"
                    + "  ID_CAR,\n"
                    + "  ACTIVO,\n"
                    + "  CODPROYECTO,\n"
                    + "  DESCRIPCION\n"
                    + "FROM CASOSESTUDIO  where id1=?  AND CODPROYECTO=? and tipo=2";
            con = conectionfactory.createConnection("manglar");         
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setInt(1, idSector);          
            pstmt.setInt(2, idProyecto);
            getDatosUnidadManejo(pstmt, con);
        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, "Error en el objeto UnidadManejoModel  m�todo getTodosLasUnidadesManejoPorCorporacion:", ex.getMessage());            
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return unidadesManejo;
    }

    public ArrayList<UnidadManejo> getUnidadManejoPorID(int idProyecto, int idUnidadManejo) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        unidadesManejo.clear();
        try {

            String query = "SELECT ID,   NOMBRE, ID1, ADJUNTO, TIPO, ID_CAR, ACTIVO,CODPROYECTO,DESCRIPCION FROM CASOSESTUDIO  where tipo=2 and codproyecto=?  and ID=?";
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);           
          
            pstmt.setInt(1, idProyecto);
            pstmt.setInt(2, idUnidadManejo);
            getDatosUnidadManejo(pstmt, con);
        } catch (SQLException ex) {
            System.out.println("Error en el objeto UnidadManeModel  m�todo getUnidadManejoPorID:" + ex.getMessage());
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return unidadesManejo;
    }

    private void getDatosUnidadManejo(PreparedStatement pstmt, Connection con) {

        ResultSet rs = null;
        try {
      
            rs = pstmt.executeQuery();
            while (rs.next()) {
                UnidadManejo unidadManejo = new UnidadManejo();
                unidadManejo.setNombre(rs.getString("NOMBRE"));             
                unidadManejo.setId(rs.getInt("ID"));
                unidadManejo.setCodProyecto(rs.getInt("CODPROYECTO"));
                unidadManejo.setIdCAR(rs.getString("ID_CAR"));
                unidadManejo.setDescripcion(rs.getString("DESCRIPCION"));
                unidadManejo.setIsActivo(rs.getString("ACTIVO"));           
                getDatosCaso(unidadManejo, rs.getInt("ID"), con);
                unidadesManejo.add(unidadManejo);
            }
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el objeto UnidadManejoManglarModel  m�todo getDatosUnidadManejo:" + ex.getMessage());

        }

    }

    public ResultadoTransaccion registrarUnidadManejo(UnidadManejo unidadmanejo) {
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();

        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();
        CallableStatement st = null;
        try {

            con = cFactory.createConnection("manglar");
            String sql = "{call  MANGLARES_NACIONAL.INSERTARUnidadManejoAlSector(?,?,?,?,?,?,?,?,?)}";
            st = con.prepareCall(sql);
            st.setInt(1, unidadmanejo.getCategoria());
            st.setString(2, unidadmanejo.getDescripcion());
            st.setInt(3, unidadmanejo.getIdSector());
            st.setString(4, unidadmanejo.getIdCAR());
            st.setString(5, unidadmanejo.getIsActivo());
            st.setInt(6, unidadmanejo.getCodProyecto());
            st.registerOutParameter(7, Types.INTEGER);
            st.registerOutParameter(8, Types.VARCHAR);
            st.registerOutParameter(9, Types.VARCHAR);
            st.executeUpdate();
            
            resultadoTransaccion.setCodigoError(st.getInt(7));
            resultadoTransaccion.setMensaje(st.getString(8));
            resultadoTransaccion.setCodigo(st.getInt(9));

        } catch (SQLException ex) {
           
            System.out.println("codigo de error:" + resultadoTransaccion.getCodigoError());
            System.out.println("error en el metodo registrarUnidadManejo del objeto UnidadManejoModelo:"
                    + ex.getMessage());
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return resultadoTransaccion;

    }

    public ResultadoTransaccion actualizarUnidadManejo(UnidadManejo unidadmanejo) {
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();
        CallableStatement st = null;
        try {

            con = cFactory.createConnection("manglar");
            String sql = "{call manglares_nacional.ActualizarUnidadManejoDSector(?,?,?,?,?,?,?,?,?)}";
            st = con.prepareCall(sql);
            
            st.setInt(1, unidadmanejo.getId());
            st.setInt(2, unidadmanejo.getIdSector());
            st.setInt(3, unidadmanejo.getCategoria());
            st.setString(4, unidadmanejo.getDescripcion());
            st.setString(5, unidadmanejo.getIdCAR());
            st.setString(6, unidadmanejo.getIsActivo());
            st.setInt(7, unidadmanejo.getCodProyecto());
            st.registerOutParameter(8, Types.INTEGER);
            st.registerOutParameter(9, Types.VARCHAR);
            st.executeUpdate();
            resultadoTransaccion.setCodigoError(st.getInt(8));
            resultadoTransaccion.setMensaje(st.getString(9));

        } catch (SQLException ex) {
           
            System.out.println("codigo de error:" + resultadoTransaccion.getCodigoError());
            System.out.println("error en el metodo actualizarUnidadManejo del objeto UnidadManejoManglarModel:"
                    + ex.getMessage());
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return resultadoTransaccion;

    }   
    
     public Map<String, String> getTodosLasUnidadesManejoActivas(int idSector,int  idUnidadManejoActual, String idCAR, int idProyecto) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> unidadesManejo = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT ID,\n"
                    + "  NOMBRE,\n"
                    + "  ID1,\n"
                    + "  ADJUNTO,\n"
                    + "  TIPO,\n"
                    + "  ID_CAR,\n"
                    + "  ACTIVO,\n"
                    + "  CODPROYECTO,\n"
                    + "  DESCRIPCION\n"
                    + " FROM CASOSESTUDIO  where id1=? and id_car=? AND CODPROYECTO=? and tipo=2 and activo='Activado' and id!=? ";
        try {
            con = conectionfactory.createConnection("manglar");           
            pstmt = con.prepareStatement(query);
            pstmt.setInt(1, idSector);
            pstmt.setString(2, idCAR);
            pstmt.setInt(3, idProyecto);
            pstmt.setInt(4,idUnidadManejoActual);   
           
            
            rs = pstmt.executeQuery();
            while (rs.next()) {
                unidadesManejo.put(rs.getString("ID")+"-"+rs.getString("NOMBRE"), rs.getString("ID"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex.getMessage());            
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return unidadesManejo;
    }

    public Map<String, String> getTodosLasUnidadesManejoConEstadistica(int codSector, String nivel,String tematica) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> unidadesManejo = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        JSONArray jsonArray = new JSONArray();
        

             
        String query = "SELECT distinct unidad_manejo,codunidadmanejo FROM MANGLARES_NACIONALES  where codsector=? and nivel=? and tematica=? and unidad_manejo is not null order by unidad_manejo";
        try {
           
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);          
            pstmt.setInt(1, codSector);           
            pstmt.setString(2, nivel);
            pstmt.setString(3, tematica);            
            rs = pstmt.executeQuery();
          
            while (rs.next()) {
                JSONObject jo = new JSONObject();
                jo.put("id",rs.getString("codunidadmanejo"));
                jo.put("name",rs.getString("codunidadmanejo")+"-"+rs.getString("unidad_manejo"));
                jsonArray.put(jo);
                
                unidadesManejo.put(rs.getString("codunidadmanejo")+"-"+rs.getString("unidad_manejo"), rs.getString("codunidadmanejo"));
            }
            jsonDataManagmentUnit=jsonArray.toString();

        } catch (SQLException ex) {
            Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex.getMessage());            
        } catch (JSONException ex) {
            Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con=null;
        }
        return unidadesManejo;
    }
    
    public Map<String, String> getTodosLasUnidadesManejoConEstadisticaTasaCrecimiento(String codSector,String nivel) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> unidadesManejo = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;        
        
        
        String query = "SELECT distinct unidad_manejo,codunidadmanejo FROM TEMP_TASAS_CRECIMIENTO  where codsector=? and nivel=? and unidad_manejo is not null order by unidad_manejo";
        try {            
            
            
            con = conectionfactory.createConnection("manglar");          
            pstmt = con.prepareStatement(query);          
            pstmt.setString(1, codSector);           
            pstmt.setString(2, nivel);             
            
            
            rs = pstmt.executeQuery();
            while (rs.next()) {
                unidadesManejo.put( rs.getString("codunidadmanejo")+"-"+rs.getString("unidad_manejo"), rs.getString("codunidadmanejo"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex.getMessage());            
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(UnidadManejoModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return unidadesManejo;
    }

    public String getJsonDataManagmentUnit() {
        return jsonDataManagmentUnit;
    }

    public void setJsonDataManagmentUnit(String jsonDataManagmentUnit) {
        this.jsonDataManagmentUnit = jsonDataManagmentUnit;
    }
    
}
