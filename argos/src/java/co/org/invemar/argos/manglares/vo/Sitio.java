/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.vo;

import java.io.Serializable;

/**
 *
 * @author usrsig15
 */
public class Sitio extends SectorManglarVo {
  private int idSector;
  private int idSitio;
  private int idUnidadManejo; 
  
  private String auditoria;

    public int getIdSector() {
        return idSector;
    }

    public void setIdSector(int idSector) {
        this.idSector = idSector;
    }

    public int getIdSitio() {
        return idSitio;
    }

    public void setIdSitio(int idSitio) {
        this.idSitio = idSitio;
    }

    public int getIdUnidadManejo() {
        return idUnidadManejo;
    }

    public void setIdUnidadManejo(int idUnidadManejo) {
        this.idUnidadManejo = idUnidadManejo;
    }

    public String getAuditoria() {
        return auditoria;
    }

    public void setAuditoria(String auditoria) {
        this.auditoria = auditoria;
    }
  
  
}
