/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.manglares.presiones.model;
import co.org.invemar.library.ushahidi.*;
import co.org.invemar.library.siam.Utilidades;

import java.util.Calendar;
import java.util.Date;


/**
 *
 * @author usrsig15
 */
public class Report extends Incident {
    private Pression presion;
    private String sector;
    private String sitio;
    private int year;
    private int month;
    private int idInt;

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDepartamento() {
        return departamento;
    }
    private String departamento;

    public int getIdInt() {
        return idInt;
    }

    public Report() {


    }

    public void setReport(Incident i) {
        this.setId(i.getId());
        this.setActive(i.getActive());
        this.setDate(i.getDate());
        this.setDescripcion(i.getDescripcion());
        this.setDomain(i.getDomain());
        this.setLatitute(i.getLatitute());
        this.setLongitude(i.getLongitude());
        this.setTitle(i.getTitle());
        this.idInt= Integer.parseInt(i.getId());

        Utilidades u = new Utilidades();
        Date d = u.StringToDate(i.getDate(), "yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTime(d);

        month = c.get(Calendar.MONTH);
        year = c.get(Calendar.YEAR);

    }

    public Pression getPresion() {
        return presion;
    }

    public void setPresion(Pression presion) {
        this.presion = presion;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getSitio() {
        return sitio;
    }

    public void setSitio(String sitio) {
        this.sitio = sitio;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
