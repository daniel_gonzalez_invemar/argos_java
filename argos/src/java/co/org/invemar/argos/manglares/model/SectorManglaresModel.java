/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.SectorManglarVo;
import co.org.invemar.sigma.manglar.entities.CasosestudioXEntidades;
import co.org.invemar.sigma.manglar.entities.PersistenceArea;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.ResultadoTransaccion;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class SectorManglaresModel implements Serializable {

    final private ArrayList<SectorManglarVo> listasectores;
    private PersistenceArea persistenceArea;

    /**
     *
     */
    public SectorManglaresModel() {
        listasectores = new ArrayList<SectorManglarVo>();
        persistenceArea = new PersistenceArea();

    }

    /**
     * *
     *
     * @param sectorList
     * @return
     */
    public boolean setAddSectorManagement(ArrayList<SectorManglarVo> sectorList, String idCAR) {
        boolean result = false;

        List<CasosestudioXEntidades> casosEstudioXEntidadesList = persistenceArea.getCodAreas(idCAR);

        System.out.println("Sector list size before-->>>>>>>>>>>>:" + sectorList.size());
        for (CasosestudioXEntidades csxe : casosEstudioXEntidadesList) {
            int idCaseStudio = csxe.getCasosestudioXEntidadesPK().getIdCasoEstudio().intValue();
            System.out.println("id caso estudio:" + idCaseStudio);
            boolean r = sectorList.add(this.getSectorPorId(idCaseStudio));
            System.out.println("Resulta adding:" + r);
        }
        System.out.println("all sectors-------------------------------");
        Iterator<SectorManglarVo> it = sectorList.iterator();
        while (it.hasNext()) {
            SectorManglarVo smv = it.next();
            System.out.println("------> idCode:" + smv.getId());
        }
        System.out.println("Sector list size after--------------->:" + sectorList.size());

        return result;
    }

    /**
     * *
     *
     * @param idSector
     * @return
     */
    public SectorManglarVo getSectorPorId(int idSector) {
        ConnectionFactory conectionfactory = new ConnectionFactory();
        SectorManglarVo sectorManglarVo = new SectorManglarVo();
        Connection con = null;
        try {

            String query = "SELECT ID,   NOMBRE, ID1, ADJUNTO, TIPO, ID_CAR, ACTIVO,CODPROYECTO,  'No' AS entidad_asociada FROM CASOSESTUDIO  where tipo=86 and ID=?";
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setInt(1, idSector);
            getDatosDelSector(pstmt, con);
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return sectorManglarVo;
    }

    /**
     *
     * @param sectormanglarvo
     * @param id
     * @param con
     */
    private void getDatosCaso(SectorManglarVo sectormanglarvo, int id, Connection con) {
        String attributeCode = "";
        try {

            String query = "select * from datos_casos_estudio where id=?";
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                if (rs.getString("CODIGO_ATRIBUTO").equals("4")) {
                    sectormanglarvo.setDescripcion(rs.getString("valor2"));
                }
                if (rs.getString("CODIGO_ATRIBUTO").equals("25")) {
                    if (rs.getString("valor2").equalsIgnoreCase("CARIBE")) {
                        sectormanglarvo.setRegion("11");
                    } else if (rs.getString("valor2").equalsIgnoreCase("PACIFICO")) {
                        sectormanglarvo.setRegion("21");
                    } else {
                        sectormanglarvo.setRegion("13");
                    }

                }
                if (rs.getString("CODIGO_ATRIBUTO").equals("26")) {
                    sectormanglarvo.setUAC(rs.getString("valor2"));
                }
                if (rs.getString("CODIGO_ATRIBUTO").equals("27")) {
                    sectormanglarvo.setDepartamento(rs.getString("valor2"));

                }
                if (rs.getString("CODIGO_ATRIBUTO").equals("28")) {
                    sectormanglarvo.setIsAreaProtegida(rs.getString("valor2"));
                }
                if (rs.getString("CODIGO_ATRIBUTO").equals("29")) {
                    sectormanglarvo.setAreaProtegida(rs.getString("valor2"));
                }
                if (rs.getString("CODIGO_ATRIBUTO").equals("30")) {
                    sectormanglarvo.setCuencaHidrografica(rs.getString("valor2"));
                }
                if (rs.getString("CODIGO_ATRIBUTO").equals("43")) {
                    sectormanglarvo.setAutoridadAmbientalACargo(rs.getString("valor2"));
                }
                if (rs.getString("CODIGO_ATRIBUTO").equals("44")) {
                    sectormanglarvo.setAbreriatura(rs.getString("valor2"));
                }

                sectormanglarvo.setEntities(persistenceArea.getEntitiesOfArea(sectormanglarvo.getId()));
            }
        } catch (Exception ex) {
            System.out.println("Error en el objeto SectorManglaresModel  m�todo getDatosCaso:" + ex.getMessage());

        }
    }

    /**
     *
     * @param idProyecto
     * @return ArrayList<SectorManglarVo>
     */
    public ArrayList<SectorManglarVo> getTodosLosSectores(int idProyecto) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        try {

            String query = "SELECT ID,   NOMBRE, ID1, ADJUNTO, TIPO, ID_CAR, ACTIVO,CODPROYECTO, 'No' as entidad_asociada  FROM CASOSESTUDIO  where tipo=86 and  codproyecto=" + idProyecto + " order by id desc";
            con = conectionfactory.createConnection("manglar");
            System.out.println("query:"+query);
            PreparedStatement pstmt = con.prepareStatement(query);
            getDatosDelSector(pstmt, con);
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;
        }
        return listasectores;
    }

    /**
     *
     * @param idCAR
     * @param idProyecto
     * @return ArrayList<SectorManglarVo>
     */
    public ArrayList<SectorManglarVo> getTodosLosSectoresPorCorporacion(String idCAR, int idProyecto) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        try {

            String query = "SELECT id, "
                    + "  nombre, "
                    + "  id1, "
                    + "  tipo, "
                    + "  id_car, "
                    + "  activo, "
                    + "  codproyecto, "
                    +"entidad_asociada "
                    + "FROM "
                    + "  (SELECT id, "
                    + "    nombre, "
                    + "    id1, "
                    + "    tipo, "
                    + "    id_car, "
                    + "    activo, "
                    + "    codproyecto, "
                    + " 'No' as entidad_asociada "
                    + "  FROM casosestudio "
                    + "  WHERE tipo     =86 "
                    + "  AND id_car    IN (?) "
                    + "  AND codproyecto=2239 "
                    + "  UNION "
                    + "  SELECT id, "
                    + "    nombre, "
                    + "    id1, "
                    + "    tipo, "
                    + "    id_car, "
                    + "    activo, "
                    + "    codproyecto, "
                    + " 'Si' as entidad_asociada "
                    + "  FROM casosestudio "
                    + "  WHERE tipo=86 "
                    + "  AND id   IN "
                    + "    (SELECT ID_CASO_ESTUDIO "
                    + "    FROM CASOSESTUDIO_X_ENTIDADES "
                    + "    WHERE nombre_entidad=?"
                    + "    ) "
                    + "  ) "
                    + "ORDER BY id DESC";                    
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setString(1, idCAR);
            pstmt.setString(2, idCAR);
            getDatosDelSector(pstmt, con);
            System.out.println("sql :"+query);
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return listasectores;
    }

    public ArrayList<SectorManglarVo> getSectorPorID(String idCAR, int idProyecto, int id) {

        ConnectionFactory conectionfactory = new ConnectionFactory();
        listasectores.clear();
        Connection con = null;
        try {

            String query = "SELECT ID,   NOMBRE, ID1, ADJUNTO, TIPO, ID_CAR, ACTIVO,CODPROYECTO,'No' AS entidad_asociada  FROM CASOSESTUDIO  where tipo=86 and  codproyecto=? and ID=?";
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setInt(1, idProyecto);
            pstmt.setInt(2, id);
            getDatosDelSector(pstmt, con);
        } catch (SQLException ex) {
            System.out.println("Error en el objeto SectorManglaresModel  m�todo getSectorPorID:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return listasectores;
    }

    private void getDatosDelSector(PreparedStatement pstmt, Connection con) {

        try {
            if (pstmt != null) {
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    SectorManglarVo sectorManglarvo = new SectorManglarVo();
                    sectorManglarvo.setNombre(rs.getString("NOMBRE"));
                    sectorManglarvo.setIsSectorAssociate(rs.getString("entidad_asociada"));
                    sectorManglarvo.setId(rs.getInt("ID"));
                    sectorManglarvo.setCodProyecto(rs.getInt("CODPROYECTO"));
                    sectorManglarvo.setIdCAR(rs.getString("ID_CAR"));
                    sectorManglarvo.setActivo(rs.getString("ACTIVO"));
                    sectorManglarvo.setEntities(persistenceArea.getEntitiesOfArea(rs.getInt("ID")));
                    getDatosCaso(sectorManglarvo, rs.getInt("ID"), con);
                    listasectores.add(sectorManglarvo);
                }

            }
        } catch (Exception ex) {
            System.out.println("Error en el objeto SectorManglaresModel  m�todo getTodosLosSectoresPorCorporacion:" + ex.getMessage());

        } finally {

            try {
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     *
     * @param SectorManglarVo
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public ResultadoTransaccion registraSector(SectorManglarVo SectorManglarVo) throws IOException, SQLException {
        String fecha_toma, descripcionImagen;
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        ConnectionFactory cFactory = new ConnectionFactory();

        Connection con = cFactory.createConnection("manglar");

        String sql = "{call MANGLARES_NACIONAL.INSERTAR_CASO_SECTOR(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        CallableStatement st = con.prepareCall(sql);
        st.setString(1, SectorManglarVo.getNombre());
        st.setString(2, SectorManglarVo.getDescripcion());
        st.setString(3, SectorManglarVo.getRegion());
        st.setString(4, SectorManglarVo.getUAC());
        st.setString(5, SectorManglarVo.getDepartamento());
        st.setString(6, SectorManglarVo.getCuencaHidrografica());
        st.setString(7, SectorManglarVo.getIsAreaProtegida());
        st.setString(8, SectorManglarVo.getAreaProtegida());
        st.setString(9, SectorManglarVo.getAutoridadAmbientalACargo());
        st.setString(10, SectorManglarVo.getImpactosConocidos());
        st.setString(11, SectorManglarVo.getNivelIntervencionAntropica());
        st.setString(12, SectorManglarVo.getActividadesEconomnicas());
        st.setString(13, SectorManglarVo.getIdCAR());
        st.setString(14, SectorManglarVo.getAbreriatura());
        st.setString(15, String.valueOf(SectorManglarVo.getExtension()));
        st.setString(16, SectorManglarVo.getActivo());
        st.registerOutParameter(17, Types.NUMERIC);
        st.registerOutParameter(18, Types.NUMERIC);
        st.registerOutParameter(19, Types.VARCHAR);

        st.executeUpdate();

        resultadoTransaccion = new ResultadoTransaccion();
        resultadoTransaccion.setCodigo(st.getInt(17));
        resultadoTransaccion.setCodigoError(st.getInt(18));
        resultadoTransaccion.setMensaje(st.getString(19));

        String idSector = String.valueOf(resultadoTransaccion.getCodigo());
        PersistenceArea persistenceArea = new PersistenceArea();
        boolean result = persistenceArea.insertEntitiesOfCaseStudio(con, idSector, SectorManglarVo.getEntities());
        if (!result) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.INFO, "It haven�t registered successfully the entities ");
        }

        System.out.println("resultado de la transaction:" + st.getString(19));

        con.close();
        con = null;

        return resultadoTransaccion;

    }

    /**
     *
     * @param SectorManglarVo
     * @return ResultadoTransaccion
     */
    public ResultadoTransaccion actualizarSector(SectorManglarVo SectorManglarVo) {
        String fecha_toma, descripcionImagen;
        InputStream archivoImage;
        int tamanoArchivo;
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();
        CallableStatement st = null;

        try {

            con = cFactory.createConnection("manglar");
            String sql = "{call MANGLARES_NACIONAL.ACTUALIZAR_CASO_SECTOR(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            st = con.prepareCall(sql);
            st.setInt(1, SectorManglarVo.getId());
            st.setString(2, SectorManglarVo.getNombre());
            st.setString(3, SectorManglarVo.getAbreriatura());
            st.setString(4, SectorManglarVo.getDescripcion());
            st.setString(5, SectorManglarVo.getRegion());
            st.setString(6, SectorManglarVo.getUAC());
            st.setString(7, SectorManglarVo.getDepartamento());
            st.setString(8, SectorManglarVo.getCuencaHidrografica());
            st.setString(9, SectorManglarVo.getIsAreaProtegida());
            st.setString(10, SectorManglarVo.getAreaProtegida());
            st.setString(11, SectorManglarVo.getAbreriatura());
            st.setString(12, SectorManglarVo.getActivo());
            st.registerOutParameter(13, Types.NUMERIC);
            st.registerOutParameter(14, Types.VARCHAR);

            st.executeUpdate();

            resultadoTransaccion = new ResultadoTransaccion();
            resultadoTransaccion.setCodigoError(st.getInt(13));
            resultadoTransaccion.setMensaje(st.getString(14));

            if (persistenceArea.deleteEntityOfArea(SectorManglarVo.getId())) {
                PersistenceArea persistenceArea = new PersistenceArea();
                persistenceArea.insertEntitiesOfCaseStudio(con, String.valueOf(SectorManglarVo.getId()), SectorManglarVo.getEntities());
            }

        } catch (SQLException ex) {

            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("codigo de error:" + resultadoTransaccion.getCodigoError());
            System.out.println("error en el metodo actualizarSector del objeto SectorManglaresModel:" + resultadoTransaccion.getMensaje());
        } finally {
            cFactory.closeConnectionInstancia(con);
        }
        return resultadoTransaccion;

    }

    public Map<String, String> getTodosLosSectoresSectoresConEstadistica(String tematica, int searchParameter, String codDepartament) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> sectores = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT sector,codsector, "
                + "  nombrelargo, "
                + "  coddepartamento "
                + "FROM "
                + "  (SELECT codsector, "
                + "    sector, "
                + "    nombrelargo, "
                + "    (SELECT valor2 "
                + "    FROM manglar.datos_casos_estudio "
                + "    WHERE tipo         =86 "
                + "    AND codigo_atributo=27 "
                + "    AND id             =codsector "
                + "    ) AS coddepartamento "
                + "  FROM MANGLARES_NACIONALES "
                + "  WHERE tematica =? "
                + "  AND nivel      =? "
                + "  ) "
                + "WHERE coddepartamento=?";
        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, tematica);
            pstmt.setInt(2, searchParameter);
            pstmt.setString(3, codDepartament);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                sectores.put(rs.getString("codsector") + "-" + rs.getString("sector") + "-" + rs.getString("nombrelargo"), rs.getString("codsector"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;

        }
        return sectores;
    }

    public String getAllSectorWithStatisticByDepartament(int tematica, String codDepartament) {

        Connection con = null;
        JSONArray jsonArray = new JSONArray();
        ConnectionFactory conectionfactory = new ConnectionFactory();
        try {
            con = conectionfactory.createConnection("monitoreom");
            String query = "SELECT codsector,sector,coddepartamento "
                    + "FROM "
                    + "  (SELECT DISTINCT codsector, "
                    + "    sector, "
                    + "    (SELECT valor2 "
                    + "    FROM manglar.datos_casos_estudio "
                    + "    WHERE tipo         =86 "
                    + "    AND codigo_atributo=27 "
                    + "    AND id             =codsector "
                    + "    ) AS coddepartamento "
                    + "  FROM MANGLARES_NACIONALES "
                    + "  WHERE tematica=? "
                    + "  ) "
                    + "WHERE coddepartamento=" + codDepartament;

            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt = con.prepareStatement(query);
            pstmt.setInt(1, tematica);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                JSONObject jo = new JSONObject();
                jo.put("id", rs.getString("codsector"));
                jo.put("name", rs.getString("codsector") + " " + rs.getString("sector"));
                jsonArray.put(jo);
            }
            if (rs != null) {
                rs.close();
                rs = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;

        }
        return jsonArray.toString();
    }

    public Map<String, String> getTodosLosSectoresEstadisticaTasaCrecimiento(String codDepartament) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> sectores = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT sector,nombresector,codsector,coddepartamento "
                + "FROM "
                + "  (SELECT distinct sector,nombresector,codsector, "
                + "    (SELECT valor2 "
                + "    FROM manglar.datos_casos_estudio "
                + "    WHERE tipo         =86 "
                + "    AND codigo_atributo=27 "
                + "    AND id             =codsector "
                + "    ) AS coddepartamento "
                + "  FROM TEMP_TASAS_CRECIMIENTO "
                + "  ) "
                + "WHERE coddepartamento=?";

        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, codDepartament);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                sectores.put(rs.getString("codsector") + "-" + rs.getString("sector") + "-" + rs.getString("nombresector"), rs.getString("codsector"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                con.close();
                pstmt.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return sectores;
    }

    /**
     *
     * @param CAR
     * @param codTopominia
     * @return Map<String, String>
     */
    public Map<String, String> getTodosLosSectoresDelDepartamento(String CAR, String codTopominia) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> sectores = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT id, nombre FROM manglar.DATOS_CASOS_ESTUDIO \n"
                + "where id_CAR='" + CAR + "'  \n"
                + "AND TIPO=86 \n"
                + "AND CODPROYECTO=2239 \n"
                + "and valor2='" + codTopominia + "'\n"
                + "and codigo_atributo=27 and activo='Activado'";
        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                sectores.put(rs.getString("nombre"), rs.getString("ID"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return sectores;
    }
}
