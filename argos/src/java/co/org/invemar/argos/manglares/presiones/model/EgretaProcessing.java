/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.presiones.model;

import co.org.invemar.library.ushahidi.*;
import co.org.invemar.ushahidi.ManagmentFormFieldUshahidi;
import co.org.invemar.util.MySQL;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class EgretaProcessing {

    private ArrayList<Report> reportList;
    private ArrayList<TablaIndicatorByDepartament> listIndicatorByDepartament = new ArrayList<TablaIndicatorByDepartament>();
    private Set<String> sectors = new HashSet();
    private Set<String> sites = new HashSet();
    private JSONArray sectorsFinded = new JSONArray();

    private List<String> departmentList = new ArrayList<String>();

    DefaultMutableTreeNode tree = new DefaultMutableTreeNode("departamentos");

    private Set<Integer> anios = new HashSet();
    private Set<String> listMonth = new HashSet<String>();
    private List<Site> listSiteIndicators = new ArrayList<Site>();
    private List<DataIndicatorBySite> listPresionIndicators = new ArrayList<DataIndicatorBySite>();

    public EgretaProcessing() {
        ProcesingIncidents();
        buscarInicidentes();
    }

    private String convertToStringMonth(int month) {
        String m = "";
        return m;
    }

    public String getJsonOfCategoryById(String categoryId) {
        String jsonCategory = "";

        return jsonCategory;
    }

    public void ProcesingIncidents() {
        reportList = new ArrayList<Report>();
        JSONProcessing jSONProcessing = new JSONProcessing("http://cinto.invemar.org.co/egreta/api?task=incidents");
        ArrayList<Incident> incidentsList = jSONProcessing.JSONToObject();

        if (incidentsList != null) {

            Iterator<Incident> iterator = incidentsList.iterator();
            int incidentsAcounter = 0;

            while (iterator.hasNext()) {
                //System.out.println("********************************************************Setting Incident.....****************");

                Incident incident = iterator.next();

                incidentsAcounter++;
                Report r = new Report();
                r.setReport(incident);
                if (Integer.parseInt(incident.getId()) >= 94) {
                    // System.out.println("incident id:" + incident.getId());

                    ArrayList<Category> categories = incident.getCategory();
                    Iterator<Category> itCategory = categories.iterator();

                    Pression presion = new Pression();
                    if (itCategory.hasNext()) {
                        Category category = itCategory.next();

                        presion.setId(category.getId());
                        presion.setTitle(category.getTitle());

                    }

                    ArrayList<CustomFields> customsFieldList = incident.getCustomsFieldList();
                    Iterator<CustomFields> itCustomsFieldList = customsFieldList.iterator();
                    while (itCustomsFieldList.hasNext()) {
                        CustomFields customsField = itCustomsFieldList.next();

                        if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("Sectores")) {
                            r.setSector(customsField.getFieldResponse());
                        }

                        if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("Intensidad")) {

                            if (customsField.getFieldResponse().equalsIgnoreCase("D�bil-Modificacones minimas sobre el ecosistema")) {
                                presion.setIntensity(1);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("Moderada-Alteraciones moderadas sobre el ecosistema")) {
                                presion.setIntensity(3);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("Fuerte-Alteracion muy grave y evidente")) {
                                presion.setIntensity(5);
                            } else {
                                //System.out.println("no ha sido configurado intensidad: ************* " + customsField.getFieldResponse());
                            }

                            presion.setIntensityDescripcion(customsField.getFieldResponse());

                        }

                        if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("Reversibilidad")) {

                            if (customsField.getFieldResponse().equalsIgnoreCase("Corto Plazo -Recuperacion Menor a un a�o")) {
                                presion.setIrreversibilty(1);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("Medio Plazo -Recuperacion entre uno y cinco a�os")) {
                                presion.setIrreversibilty(3);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("Largo Plazo -Recuperacion mayor a cinco a�os")) {
                                presion.setIrreversibilty(5);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("Irrecuperable - No hay recuperacion")) {
                                presion.setIrreversibilty(7);
                            } else {
                                // System.out.println("no ha sido configurado la Reversibilidad: ************* " + customsField.getFieldResponse());
                            }
                            presion.setIrreversibilityDescription(customsField.getFieldResponse());

                        }

                        if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("Extensi�n")) {

                            if (customsField.getFieldResponse().equalsIgnoreCase("La actividad tiene un efecto puntual")) {
                                presion.setExtension(1);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("La actividad tiene un efecto local")) {
                                presion.setExtension(3);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("La actividad tiene un efecto parcial")) {
                                presion.setExtension(5);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("La actividad tiene un efecto extenso")) {
                                presion.setExtension(9);
                            } else {
                                //  System.out.println("no ha sido configurado  Extensi�n: ************* " + customsField.getFieldResponse());
                            }
                            presion.setExtensioDescription(customsField.getFieldResponse());

                        }

                        if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("Frecuencia")) {

                            if (customsField.getFieldResponse().equalsIgnoreCase("Inusual-Se presenta una vez al a�o")) {
                                presion.setFrecuency(1);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("Ocasional- Se presenta dos veces por a�o")) {
                                presion.setFrecuency(2);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("Frecuente-Se presenta mas de dos veces por a�o")) {
                                presion.setFrecuency(3);
                            } else {
                                //  System.out.println("no ha sido configurdo Frecuencia: ************* " + customsField.getFieldResponse());
                            }
                            presion.setFrecuencyDescription(customsField.getFieldResponse());
                        }

                        if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("Persistencia")) {

                            if (customsField.getFieldResponse().equalsIgnoreCase("El efecto dura menos de cinco a�os")) {
                                presion.setPersistence(1);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("El efecto dura entre cinco y diez a�os")) {
                                presion.setPersistence(2);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("El efecto dura mas de diez a�os")) {
                                presion.setPersistence(3);
                            } else {
                                //  System.out.println("no ha sido configurado Persistencia: ************* " + customsField.getFieldResponse());
                            }
                            presion.setPersistenceDescription(customsField.getFieldResponse());

                        }

                        if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("Tendencia")) {

                            if (customsField.getFieldResponse().equalsIgnoreCase("Decreciente-Tiende a desaparecer en el tiempo")) {
                                presion.setTendency(1);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("Estable-Se mantiene constante en el tiempo")) {
                                presion.setTendency(2);
                            } else if (customsField.getFieldResponse().equalsIgnoreCase("Creciente-tiende a incrementarse en el tiempo")) {
                                presion.setTendency(5);
                            } else {
                                //  System.out.println("no ha sido configurado Persistencia: ************* " + customsField.getFieldResponse());
                            }
                            presion.setPersistenceDescription(customsField.getFieldResponse());
                        }

                        if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("sector")) {

                            r.setSector(customsField.getFieldResponse());
                        }

                        if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("Unidad de Manejo")) {

                            r.setSitio(customsField.getFieldResponse());
                        }

                        if (customsField.getFieldName() != null && customsField.getFieldName().equalsIgnoreCase("Departamentos")) {
                            r.setDepartamento(customsField.getFieldResponse());
                        }

                    }
                    r.setPresion(presion);

                    anios.add(r.getYear());
                    sectors.add(r.getSector());
                    sites.add(r.getSitio());
                    if (!r.getDepartamento().equals("")) {
                        reportList.add(r);
                    }
                }

                //System.out.println("************************************************Incident setting******************************************");
            }
        }

    }

    public void buscarInicidentes() {

        HashSet<String> dList = new HashSet<String>();
        Iterator<Report> iterator = getReportList().iterator();
        int contador = 0;

        while (iterator.hasNext()) {
            Report myReport = iterator.next();
            contador++;

            dList.add(myReport.getDepartamento());
            //System.out.println("Department:" + myReport.getDepartamento() + " id report:" + myReport.getId());

        }

        departmentList = new ArrayList<String>(dList);
        Collections.sort(departmentList);

        Iterator<String> it = departmentList.iterator();
        while (it.hasNext()) {
            String departamento = it.next();
            if (departamento != null) {
                tree.add(new DefaultMutableTreeNode(departamento));
            }
        }

        Enumeration children = tree.children();
        while (children.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) children.nextElement();
            String d = (String) node.getUserObject();
            HashSet sectorsHashSet = new HashSet();
            iterator = getReportList().iterator();
            while (iterator.hasNext()) {
                Report myReport = iterator.next();
                if (d.equalsIgnoreCase(myReport.getDepartamento())) {

                    String sector = myReport.getSector();
                    DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode(sector);

                    if (!sectorsHashSet.contains(sector)) {
                        sectorsHashSet.add(sector);
                        node.add(dmtn);
                    }

                }
            }
        }
    }

    public JSONArray findSectorByDepartment(String deparment) {
        Enumeration children = tree.children();
        JSONArray jsonarray = new JSONArray();
        try {
            while (children.hasMoreElements()) {
                DefaultMutableTreeNode d = (DefaultMutableTreeNode) children.nextElement();
                Enumeration eaux = d.children();
                String departamento = (String) d.getUserObject();
                if (deparment.equalsIgnoreCase(departamento)) {

                    while (eaux.hasMoreElements()) {
                        DefaultMutableTreeNode dd = (DefaultMutableTreeNode) eaux.nextElement();
                        String um = (String) dd.getUserObject();
                        JSONObject jsonobject = new JSONObject();
                        jsonobject.put("id", um);
                        jsonobject.put("name", um);
                        jsonarray.put(jsonobject);
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(EgretaProcessing.class.getName()).log(Level.SEVERE, e.getMessage());
        }

        return jsonarray;

    }

    public JSONArray findYearByDepartamentWithData(String department) {
        JSONArray jsonarray = new JSONArray();
        Set<Integer> anios = new HashSet();

        Iterator<Report> iterator = getReportList().iterator();
        while (iterator.hasNext()) {
            Report myReport = iterator.next();
            if (department.equalsIgnoreCase(myReport.getDepartamento())) {
                anios.add(myReport.getYear());
            }
        }
        ArrayList yearList = new ArrayList(anios);
        Collections.sort(yearList);
        Iterator<Integer> iYearList = yearList.iterator();
        while (iYearList.hasNext()) {
            JSONObject jsonObject = new JSONObject();
            Integer i = iYearList.next();
            try {
                jsonObject.put("id", i.intValue());
                jsonObject.put("name", i.intValue());
            } catch (JSONException ex) {
                Logger.getLogger(EgretaProcessing.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            }
            jsonarray.put(jsonObject);

        }

        return jsonarray;

    }

    public JSONArray findYearByDepartamentAndSectorWithData(String department, String sector) {
        JSONArray jsonarray = new JSONArray();
        Set<Integer> anios = new HashSet();

        Iterator<Report> iterator = getReportList().iterator();
        while (iterator.hasNext()) {
            Report myReport = iterator.next();
            if (department.equalsIgnoreCase(myReport.getDepartamento()) && myReport.getSector().contains(sector)) {
                anios.add(myReport.getYear());
            }
        }
        ArrayList yearList = new ArrayList(anios);
        Collections.sort(yearList);
        Iterator<Integer> iYearList = yearList.iterator();
        while (iYearList.hasNext()) {
            JSONObject jsonObject = new JSONObject();
            Integer i = iYearList.next();
            try {
                jsonObject.put("id", i.intValue());
                jsonObject.put("name", i.intValue());
            } catch (JSONException ex) {
                Logger.getLogger(EgretaProcessing.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            }
            jsonarray.put(jsonObject);

        }

        return jsonarray;

    }

    public static void main(String[] args) {
        EgretaProcessing egretaProcessing = new EgretaProcessing();
        JSONObject jo = egretaProcessing.Indicator("GUA_Guajira", 2015);
        System.out.println(jo.toString());

        //egretaProcessing.DetailIndicatorByPression("ANT_Antioquia", 2014);
        //egretaProcessing.DetailIndicatorByPression("ANT_Antioquia", "ANT_EnsenadadeRionegro", 2014);
        //System.out.println("years:"+egretaProcessing.findYearByDepartamentAndSectorWithData("Guajira","Sector3"));
        //System.out.println("Arry size:"+egretaProcessing.findSectorByDepartment("ANT_Antioquia"));
        //   egretaProcessing.findSectorByDepartment("ANT_Antioquia");
//        System.out.println("-------------------------------------------All Report-****------------------");
//        Iterator<Report> iterator = egretaProcessing.getReportList().iterator();
//        int contador = 0;
//        while (iterator.hasNext()) {
//            Report myReport = iterator.next();
//            contador++;
//            System.out.println("Contador:" + contador + " Departamento: " + myReport.getDepartamento() + " Month:"
//                    + myReport.getMonth() + " Year report: " + myReport.getYear() + " Report Unidad Manejo: "
//                    + myReport.getSitio() + " Sector: " + myReport.getSector() + " Report id: "
//                    + myReport.getId() + " report name: " + myReport.getTitle() + " Presion: "
//                    + myReport.getPresion().getTitle() + " - intensidad:"
//                    + myReport.getPresion().getIntensity() + " Irreversibilidad:"
//                    + myReport.getPresion().getIrreversibilty() + " -Extension:"
//                    + myReport.getPresion().getExtension() + "- Ocurrencia:"
//                    + myReport.getPresion().getOcurrencia());
//        }
        //egretaProcessing.DetailIndicatorByPression("ATL_Atl�ntico",2014);
//        System.out.println("/************************************************************");
//        System.out.println("/************************************************************");
//        System.out.println("/************************************************************");
//        egretaProcessing.DetailIndicatorByPression("Magdalena","CGSM",2014);
    }

    public JSONArray Indicator(String departamento) {

//        System.out.println("--------------------------------------------Department DetailIndicatorByPression calculus------------------");
        JSONArray jsonarrayindicator = new JSONArray();

        Set<Integer> anios = new HashSet<Integer>();

        List<DatosIndicator> IndicatorList = null;
        ArrayList<Report> reportFiltered = new ArrayList<Report>();
        String deparmentSectorSelected;
        try {
            if (departamento != null) {

                Iterator<Report> iterator = getReportList().iterator();
//                System.out.println("tamanio de incidencias reportadas:" + getReportList().size());
                sectors.clear();

                while (iterator.hasNext()) {
                    Report myReport = iterator.next();
                    anios.add(new Integer(myReport.getYear()));

                }

                Iterator<Integer> iteratoranios = anios.iterator();
                listIndicatorByDepartament.clear();
                while (iteratoranios.hasNext()) {
                    Integer a = iteratoranios.next();

                    iterator = getReportList().iterator();

                    while (iterator.hasNext()) {
                        Report myReport = iterator.next();
                        if (departamento.equalsIgnoreCase(myReport.getDepartamento())) {
                            if (a.intValue() == myReport.getYear()) {
                                /*System.out.println("Departamento: " + myReport.getDepartamento() + " Month:"+ myReport.getMonth() + " Year report: " + myReport.getYear()+ " Report Unidad de manejo: " + myReport.getSitio() + " Sector: "
                                 + myReport.getSector() + " Report id: " + myReport.getId()+ " report name: " + myReport.getTitle() + "  Presion:  "
                                 + myReport.getPresion().getTitle() 
                                 + " id Tipo de presion:"                                         + myReport.getPresion().getId() + "  - intensidad:"
                                 + myReport.getPresion().getExtension() + " Extension:"
                                 + myReport.getPresion().getIrreversibilty() + " -Extension:"
                                 + myReport.getPresion().getExtension() + " Ocurrencia:"
                                 + myReport.getPresion().getOcurrencia());*/

                                //System.out.println("Id presion:"+ myReport.getPresion().getId()+ "Extension:"+myReport.getPresion().getExtension()+ "Frecuencia:"+myReport.getPresion().getFrecuencia());
                                deparmentSectorSelected = myReport.getDepartamento();
                                sectors.add(myReport.getSector());
                                reportFiltered.add(myReport);
                            }

                            // }
                        }
                    }

                    Iterator<String> sectorIteratorIt = sectors.iterator();
                    double sumatotal = 0;
                    double indicator = 0;
                    String nameSector = "";

                    while (sectorIteratorIt.hasNext()) {

                        String sector = sectorIteratorIt.next();
                        // System.out.println("Sector:" + sector);

                        Iterator<Report> reportFilteredIterator = reportFiltered.iterator();
                        int sizeReportFiltered = reportFiltered.size();
                        double criterionSum = 0;

                        Report r = null;
                        int anio = 0;
                        while (reportFilteredIterator.hasNext()) {
                            r = reportFilteredIterator.next();
                            departamento = r.getDepartamento();
                            anio = r.getYear();
                            if (r.getSector().equalsIgnoreCase(sector)) {
                                sumatotal = sumatotal + r.getPresion().getIntensity() + r.getPresion().getIrreversibilty() + r.getPresion().getExtension() + r.getPresion().getFrecuencia();
                                indicator = sumatotal / sizeReportFiltered;
                                //nameSector=sector;
                            }

                        }
                        //System.out.println("sumatotal:" + sumatotal);
                        TablaIndicatorByDepartament tid = new TablaIndicatorByDepartament();
                        tid.setDepartament(departamento);
                        tid.setIndicatorValue(indicator);
                        tid.setSector(sector);
                        tid.setAnio(anio);
                        listIndicatorByDepartament.add(tid);

                    }

                }
            }
//            System.out.println("Indicador por departamento:...............");

            Iterator<Integer> iteratoranios = anios.iterator();

            JSONObject jsonobject = new JSONObject();

            while (iteratoranios.hasNext()) {
                Integer anio = iteratoranios.next();
                // System.out.println("anio:" + anio.intValue());
                jsonobject = new JSONObject();
                jsonobject.put("anio", anio.intValue());
                Iterator<TablaIndicatorByDepartament> itt = listIndicatorByDepartament.iterator();

                JSONArray jsonarrayaux = new JSONArray();
                while (itt.hasNext()) {
                    TablaIndicatorByDepartament tibd = itt.next();

                    if (anio.intValue() == tibd.getAnio()) {
                        // System.out.println("Anio:" + tibd.getAnio() + " Departamento tibd:" + tibd.getDepartament() + " -Sector:" + tibd.getSector() + " -value:" + tibd.getIndicatorValue());                       
                        JSONObject jsonobject2 = new JSONObject();
                        // System.out.println("--anio:"+tibd.getAnio()+" ---Sectores:" + tibd.getSector());                       
                        jsonobject2.put("sector", tibd.getSector());
                        jsonobject2.put("indicator", tibd.getIndicatorValue());
                        jsonarrayaux.put(jsonobject2);
                    }
                }
                jsonobject.put("sectores", jsonarrayaux);
                jsonarrayindicator.put(jsonobject);
            }

        } catch (Exception e) {
            System.out.println("Error calculando el indicador:" + e.getMessage());
        }
//        System.out.println("json array:" + jsonarrayindicator.toString());
        return jsonarrayindicator;

    }

    public JSONObject Indicator(String departamento, int year) {

//        System.out.println("--------------------------------------------DetailIndicatorByPression calculus-1111-----------------");
        JSONArray jsonarrayindicator = new JSONArray();
        JSONObject jsonobject = new JSONObject();

        Set<Integer> anios = new HashSet<Integer>();

        List<DatosIndicator> IndicatorList = null;
        ArrayList<Report> reportFiltered = new ArrayList<Report>();
        String deparmentSectorSelected;
        try {
            if (departamento != null) {

                Iterator<Report> iterator = getReportList().iterator();
                // System.out.println("tamanio de incidencias reportadas:" + getReportList().size());
                sectors.clear();
                listIndicatorByDepartament.clear();

                while (iterator.hasNext()) {
                    Report myReport = iterator.next();

                    if (departamento.equalsIgnoreCase(myReport.getDepartamento())) {
                        if (year == myReport.getYear()) {
                            deparmentSectorSelected = myReport.getDepartamento();
                            // System.out.println("id:"+myReport.getPresion().getId()+" Exten: "+myReport.getPresion().getExtension()+" frec:"+myReport.getPresion().getFrecuency()+ "In:"+myReport.getPresion().getIntensity()+" Irr:"+myReport.getPresion().getIrreversibilty() +" Pers:"+myReport.getPresion().getPersistence() + "Tend:"+myReport.getPresion().getTendency());
                            sectors.add(myReport.getSector());
                            reportFiltered.add(myReport);
                        }

                    }
                }

                Iterator<String> sectorIteratorIt = sectors.iterator();

                double indicator = 0;
                String nameSector = "";

                while (sectorIteratorIt.hasNext()) {

                    String sector = sectorIteratorIt.next();
                    Iterator<Report> reportFilteredIterator = reportFiltered.iterator();
                    int sizeReportFiltered = reportFiltered.size();
                    double criterionSum = 0;

                    Report r = null;
                    int anio = 0;
                    double sumatotal = 0;
                    while (reportFilteredIterator.hasNext()) {
                        r = reportFilteredIterator.next();

                        departamento = r.getDepartamento();
                        anio = r.getYear();
                        if (r.getSector().equalsIgnoreCase(sector)) {
                            //System.out.println("    id:"+r.getId()); 
                            System.out.println("");
                            sumatotal = sumatotal + r.getPresion().getExtension() + r.getPresion().getFrecuency() + r.getPresion().getIntensity() + r.getPresion().getIrreversibilty() + r.getPresion().getPersistence() + r.getPresion().getTendency();
                            //System.out.println("    Parcial sum:"+sumatotal);
                            indicator = sumatotal;

                        }

                    }
                    //  System.out.println("DetailIndicatorByPression:" + indicator);
                    TablaIndicatorByDepartament tid = new TablaIndicatorByDepartament();
                    tid.setDepartament(departamento);
                    tid.setIndicatorValue(indicator);
                    tid.setSector(sector);
                    tid.setAnio(anio);
                    listIndicatorByDepartament.add(tid);

                }

            }

            Iterator<TablaIndicatorByDepartament> itt = listIndicatorByDepartament.iterator();
            jsonobject.put("anio", year);

            while (itt.hasNext()) {
                TablaIndicatorByDepartament tibd = itt.next();
                JSONObject jsonobject2 = new JSONObject();
                jsonobject2.put("name", tibd.getSector());
                jsonobject2.put("indicator", tibd.getIndicatorValue());
                jsonarrayindicator.put(jsonobject2);
            }
            jsonobject.put("data", jsonarrayindicator);

        } catch (JSONException e) {
            System.out.println("Error calculando el indicador:" + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error calculando el indicador:" + e.getMessage());
        }

        // System.out.println("json array:" + jsonobject.toString());
        return jsonobject;

    }

    public JSONObject Indicator(String departamento, String sectorbusqueda, int year) {

        //  System.out.println("--------------------------------------------Indicator by department, sectorbusqueda and year-----------------");
        JSONArray jsonarrayindicator = new JSONArray();
        JSONObject jsonobject = new JSONObject();
        ArrayList<Report> reportFiltered = new ArrayList<Report>();
        String deparmentSectorSelected;
        ArrayList<TablaIndicatorByManagmentUnit> indicatorList = new ArrayList<TablaIndicatorByManagmentUnit>();

        HashSet<String> listIndicatorByManagmentUnit = new HashSet<String>();
        try {
            if (departamento != null) {

                Iterator<Report> iterator = getReportList().iterator();
                //  System.out.println("tamanio de incidencias reportadas:" + getReportList().size());

                while (iterator.hasNext()) {
                    Report myReport = iterator.next();

                    if (departamento.equalsIgnoreCase(myReport.getDepartamento())) {
                        if (year == myReport.getYear()) {
                            if (myReport.getSector().contains(sectorbusqueda)) {
                                deparmentSectorSelected = myReport.getDepartamento();
                                //  System.out.println("managment unit:" + myReport.getSitio() + " id:" + myReport.getId());
                                String[] arrayCode = myReport.getSitio().split("_");
                                listIndicatorByManagmentUnit.add(arrayCode[0]);
                                reportFiltered.add(myReport);
                            }
                        }

                    }
                }

                Iterator<String> listIndicatorByManagmentUnitIterator = listIndicatorByManagmentUnit.iterator();

                double indicator = 0;
                String nameManagmentUnit = "";

                while (listIndicatorByManagmentUnitIterator.hasNext()) {

                    String codeManagmentUnit = listIndicatorByManagmentUnitIterator.next();
                    Iterator<Report> reportFilteredIterator = reportFiltered.iterator();
                    int sizeReportFiltered = reportFiltered.size();
                    // System.out.println("codeManagmentUnit:" + codeManagmentUnit);
                    double criterionSum = 0;

                    Report r = null;
                    int anio = 0;
                    double sumatotal = 0;
                    while (reportFilteredIterator.hasNext()) {
                        r = reportFilteredIterator.next();
                        departamento = r.getDepartamento();
                        anio = r.getYear();
                        if (r.getSitio().contains(codeManagmentUnit)) {
                            nameManagmentUnit = r.getSitio();
                            //   System.out.println("    id:" + r.getId());
                            sumatotal = sumatotal + r.getPresion().getExtension() + r.getPresion().getFrecuency() + r.getPresion().getIntensity() + r.getPresion().getIrreversibilty() + r.getPresion().getPersistence() + r.getPresion().getTendency();
                            //    System.out.println("    Parcial sum:" + sumatotal);
                            indicator = sumatotal;
                        }
                    }
                    TablaIndicatorByManagmentUnit timu = new TablaIndicatorByManagmentUnit();
                    timu.setDepartament(departamento);
                    timu.setUnidadManejo(nameManagmentUnit);
                    timu.setIndicatorValue(indicator);
                    timu.setAnio(year);
                    indicatorList.add(timu);
                }
            }

            Iterator<TablaIndicatorByManagmentUnit> itt = indicatorList.iterator();
            jsonobject.put("anio", year);

            while (itt.hasNext()) {
                TablaIndicatorByManagmentUnit tibd = itt.next();
                JSONObject jsonobject2 = new JSONObject();
                jsonobject2.put("name", tibd.getUnidadManejo());
                jsonobject2.put("indicator", tibd.getIndicatorValue());
                jsonarrayindicator.put(jsonobject2);
            }
            jsonobject.put("data", jsonarrayindicator);

        } catch (JSONException e) {
            System.out.println("Error calculando el indicador:" + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error calculando el indicador:" + e.getMessage());
        }

        return jsonobject;
    }

    public JSONArray DetailIndicatorByPression(String departamento, String sectorbusqueda, int anio) {

        List<DatosIndicator> IndicatorList = null;
        ArrayList<Report> reportFiltered = new ArrayList<Report>();
        String deparmentSectorSelected;
        JSONArray jsonarray = new JSONArray();
        Set<String> presionList = new HashSet();
        ArrayList<IndicatorByDepartmentAndSector> listCalculatePression = new ArrayList<IndicatorByDepartmentAndSector>();
        try {
            if (departamento != null) {

                Iterator<Report> iterator = getReportList().iterator();
                sectors.clear();

                int filteredReport = 0;

                while (iterator.hasNext()) {
                    Report myReport = iterator.next();
                    if (departamento.equalsIgnoreCase(myReport.getDepartamento())) {

                        if (anio == myReport.getYear()) {

                            String[] textSeparator = sectorbusqueda.split("_");

                            if (myReport.getSector().contains(textSeparator[0])) {

                                filteredReport++;
                                deparmentSectorSelected = myReport.getDepartamento();
                                sectors.add(myReport.getSector());
                                reportFiltered.add(myReport);
                            }

                        }

                    }
                }

                listIndicatorByDepartament.clear();
                Iterator<String> sectorIteratorIt = sectors.iterator();
                double sumatotal = 0;
                double sumByPression = 0;
                double indicator = 0;
                String nameManagmentUnit = "";

                while (sectorIteratorIt.hasNext()) {

                    String sector = sectorIteratorIt.next();

                    Iterator<Report> reportFilteredIterator = reportFiltered.iterator();
                    int sizeReportFiltered = reportFiltered.size();
                    double criterionSum = 0;
                    nameManagmentUnit = "";
                    Report r = null;
                    while (reportFilteredIterator.hasNext()) {
                        r = reportFilteredIterator.next();
                        departamento = r.getDepartamento();
                        if (r.getSector().equalsIgnoreCase(sector)) {
                            presionList.add(r.getPresion().getId() + "-" + r.getPresion().getTitle());
                        }
                    }
                }

                Iterator<String> itPression = presionList.iterator();
                String fullPressionName = " ";
                while (itPression.hasNext()) {
                    String p = itPression.next();

                    Iterator<Report> reportFilteredIterator = reportFiltered.iterator();
                    while (reportFilteredIterator.hasNext()) {
                        Report r = reportFilteredIterator.next();

                        if (p.contentEquals(r.getPresion().getId() + "-" + r.getPresion().getTitle())) {
                            fullPressionName = this.getParentNameCategory(r.getPresion().getId()) + " - " + r.getPresion().getTitle();
                            sumByPression = sumByPression + r.getPresion().getIntensity() + r.getPresion().getIrreversibilty() + r.getPresion().getExtension() + r.getPresion().getFrecuencia();
                        }
                    }
                    IndicatorByDepartmentAndSector tidp = new IndicatorByDepartmentAndSector();
                    tidp.setPressionName(fullPressionName);
                    tidp.setValue(sumByPression);
                    listCalculatePression.add(tidp);
                    sumatotal = sumatotal + sumByPression;
                }

                Iterator<IndicatorByDepartmentAndSector> itt = listCalculatePression.iterator();

                while (itt.hasNext()) {
                    IndicatorByDepartmentAndSector ids = itt.next();

                    JSONObject jsonobject = new JSONObject();

                    jsonobject.put("name", ids.getPressionName());
                    jsonobject.put("y", ids.getValue());
                    jsonarray.put(jsonobject);
                }

            }

        } catch (Exception e) {
            System.out.println("Error calculando el indicador:" + e.getMessage());
        }

        return jsonarray;
    }

    private String getParentNameCategory(int parentIdCategory) {
        String nameParentName = " ";
        MySQL mysqlDB = new MySQL();
        Connection con = mysqlDB.connect("ushahidiegreta");
        ManagmentFormFieldUshahidi mffu = new ManagmentFormFieldUshahidi();
        nameParentName = mffu.getCategoryParent(String.valueOf(parentIdCategory), con).getCategoryTitle();
        try {
            con.close();
        } catch (SQLException ex) {
            con = null;
            Logger.getLogger(EgretaProcessing.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return nameParentName;
    }

    public JSONArray DetailPressionByManagmentUnit(String departamento, String managmentUnit, int anio) {

        List<DatosIndicator> IndicatorList = null;
        ArrayList<Report> reportFiltered = new ArrayList<Report>();
        String deparmentSectorSelected;
        JSONArray jsonarray = new JSONArray();
        Set<String> presionList = new HashSet();
        Set<String> listManagmentUnits = new HashSet();
        ArrayList<IndicatorByDepartmentAndSector> listCalculatePression = new ArrayList<IndicatorByDepartmentAndSector>();
        try {
            if (departamento != null) {

                Iterator<Report> iterator = getReportList().iterator();

                int filteredReport = 0;

                while (iterator.hasNext()) {
                    Report myReport = iterator.next();
                    if (departamento.equalsIgnoreCase(myReport.getDepartamento())) {

                        if (anio == myReport.getYear()) {

                            String[] textSeparator = managmentUnit.split("_");

                            if (myReport.getSitio().contains(textSeparator[0])) {

                                filteredReport++;
                                deparmentSectorSelected = myReport.getDepartamento();
                                listManagmentUnits.add(myReport.getSitio());

                                reportFiltered.add(myReport);
                            }

                        }

                    }
                }

                Iterator<String> listManagmentUnitsIterator = listManagmentUnits.iterator();
                double sumatotal = 0;
                double sumByPression = 0;
                double indicator = 0;
                String nameManagmentUnit = "";

                while (listManagmentUnitsIterator.hasNext()) {

                    String mu = listManagmentUnitsIterator.next();

                    Iterator<Report> reportFilteredIterator = reportFiltered.iterator();
                    int sizeReportFiltered = reportFiltered.size();
                    double criterionSum = 0;
                    nameManagmentUnit = "";
                    Report r = null;
                    while (reportFilteredIterator.hasNext()) {
                        r = reportFilteredIterator.next();
                        departamento = r.getDepartamento();
                        if (r.getSitio().contains(mu)) {

                            presionList.add(r.getPresion().getId() + "-" + r.getPresion().getTitle());
                        }
                    }
                }

                Iterator<String> itPression = presionList.iterator();
                String fullNamePression = "";
                while (itPression.hasNext()) {
                    String p = itPression.next();
                    Iterator<Report> reportFilteredIterator = reportFiltered.iterator();
                    while (reportFilteredIterator.hasNext()) {
                        Report r = reportFilteredIterator.next();
                        if (p.contentEquals(r.getPresion().getId() + "-" + r.getPresion().getTitle())) {
                            fullNamePression = this.getParentNameCategory(r.getPresion().getId()) + " " + r.getPresion().getTitle();
                            sumByPression = sumByPression + r.getPresion().getIntensity() + r.getPresion().getIrreversibilty() + r.getPresion().getExtension() + r.getPresion().getFrecuencia();

                        }
                    }

                    IndicatorByDepartmentAndSector tidp = new IndicatorByDepartmentAndSector();
                    tidp.setPressionName(fullNamePression);
                    tidp.setValue(sumByPression);
                    listCalculatePression.add(tidp);
                    sumatotal = sumatotal + sumByPression;
                }

                Iterator<IndicatorByDepartmentAndSector> itt = listCalculatePression.iterator();

                while (itt.hasNext()) {
                    IndicatorByDepartmentAndSector ids = itt.next();

                    JSONObject jsonobject = new JSONObject();
                    jsonobject.put("name", ids.getPressionName());
                    jsonobject.put("y", ids.getValue());
                    jsonarray.put(jsonobject);
                }

            }

        } catch (Exception e) {
            System.out.println("Error calculando el indicador:" + e.getMessage());
        }

        return jsonarray;
    }

    public double calculutePercentaje(double total, double part) {
        double percentaje = 0;

        if (total > 0) {
            percentaje = (part * 100) / total;
        }
        return percentaje;
    }

    public ArrayList<Report> getReportList() {
        return reportList;
    }

    public void setReportList(ArrayList<Report> reportList) {
        this.reportList = reportList;
    }

    public Set<String> getSectors() {
        return sectors;
    }

    public void setSectors(Set<String> sectors) {
        this.sectors = sectors;
    }

    public Set<String> getSites() {
        return sites;
    }

    public void setSites(Set<String> sites) {
        this.sites = sites;
    }

    public void setAnios(Set<Integer> anios) {
        this.anios = anios;
    }

    public Set<Integer> getAnios() {
        return anios;
    }

    public void setListMonth(Set<String> listMonth) {
        this.listMonth = listMonth;
    }

    public Set<String> getListMonth() {
        return listMonth;
    }

    public void setListPresionIndicators(List<DataIndicatorBySite> listPresionIndicators) {
        this.listPresionIndicators = listPresionIndicators;
    }

    public List<DataIndicatorBySite> getListPresionIndicators() {
        return listPresionIndicators;
    }

    public void setListSiteIndicators(List<Site> listSiteIndicators) {
        this.listSiteIndicators = listSiteIndicators;
    }

    public List<Site> getListSiteIndicators() {
        return listSiteIndicators;
    }

    public void setListIndicatorByDepartament(ArrayList<TablaIndicatorByDepartament> listIndicatorByDepartament) {
        this.listIndicatorByDepartament = listIndicatorByDepartament;
    }

    public ArrayList<TablaIndicatorByDepartament> getListIndicatorByDepartament() {
        return listIndicatorByDepartament;
    }

    public List<String> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<String> departmentList) {
        this.departmentList = departmentList;
    }

    public JSONArray getSectorsFinded() {
        return sectorsFinded;
    }

    public void setSectorsFinded(JSONArray sectorsFinded) {
        this.sectorsFinded = sectorsFinded;
    }

}
