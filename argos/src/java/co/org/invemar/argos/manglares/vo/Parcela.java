/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.vo;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author usrsig15
 */
public class Parcela extends Sitio  {

    private double latitud;
    private double longitud;
    private int azimut;
    private Date fechaInstalación;
    private int unidadManejo;
    private int formaParcela;
    private Componente componente;
    private int idEstacion;
    private String vigenciaLoc;
    private long tipoFisiografico;
    

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getAzimut() {
        return azimut;
    }

    public void setAzimut(int azimut) {
        this.azimut = azimut;
    }

    public Date getFechaInstalacion() {
        return fechaInstalación;
    }

    public void setFechaInstalacion(Date fechaInstalación) {
        this.fechaInstalación = fechaInstalación;
    }

    public int getUnidadManejo() {
        return unidadManejo;
    }

    public void setUnidadManejo(int unidadManejo) {
        this.unidadManejo = unidadManejo;
    }

    public int getFormaParcela() {
        return formaParcela;
    }

    public void setFormaParcela(int formaParcela) {
        this.formaParcela = formaParcela;
    }

    public Componente getComponente() {
        return componente;
    }

    public void setComponente(Componente componente) {
        this.componente = componente;
    }

    public int getIdEstacion() {
        return idEstacion;
    }

    public void setIdEstacion(int idEstacion) {
        this.idEstacion = idEstacion;
    }

    public String getVigenciaLoc() {
        return vigenciaLoc;
    }

    public void setVigenciaLoc(String vigenciaLoc) {
        this.vigenciaLoc = vigenciaLoc;
    }

    public long getTipoFisiografico() {
        return tipoFisiografico;
    }

    public void setTipoFisiografico(long tipoFisiografico) {
        this.tipoFisiografico = tipoFisiografico;
    }

   
}
