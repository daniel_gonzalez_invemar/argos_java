/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.manglares.presiones.model;

/**
 *
 * @author usrsig15
 */
public class IndicatorByDepartmentAndSector {
    private String PressionName;
    private double value;

    public String getPressionName() {
        return PressionName;
    }

    public void setPressionName(String PressionName) {
        this.PressionName = PressionName;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
    
}
