/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.Sitio;
import co.org.invemar.sigma.manglar.entities.PersistenceArea;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.ResultadoTransaccion;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class SitioManglarModel implements Serializable {

    private ArrayList<Sitio> sitios;
    private String jsonDataSite;
   

    public SitioManglarModel() {
        sitios = new ArrayList<Sitio>();
      
    }

    private void getDatosCaso(Sitio sitio, int id, Connection con) {

        PreparedStatement pstmt = null;
        ResultSet rs1 = null;
        try {

            String query = "select * from casosestudio_atributos where casosestudio_id=?";
            pstmt = con.prepareStatement(query);
            pstmt.setInt(1, sitio.getId());
            rs1 = pstmt.executeQuery();

            while (rs1.next()) {
                if (rs1.getString("CODIGO_ATRIBUTO").equals("44")) {
                    sitio.setAbreriatura(rs1.getString("valor2"));
                }
            }
        } catch (Exception ex) {
            System.out.println("Error en el objeto SitioManglarModel  m�todo getDatosCaso:" + ex.getMessage());

        }
    }

    public ArrayList<Sitio> getTodosLosSectoresPorCorporacion(int idUnidadManejo, String idCAR, int idProyecto) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        sitios.clear();
        try {

            String query = "SELECT ID,\n"
                    + "  NOMBRE,\n"
                    + "  ID1,\n"
                    + "  ADJUNTO,\n"
                    + "  TIPO,\n"
                    + "  ID_CAR,\n"
                    + "  ACTIVO,\n"
                    + "  CODPROYECTO,\n"
                    + "  DESCRIPCION, AUDITORIA "
                    + "FROM CASOSESTUDIO  where id1=? and id_car in ('" + idCAR + "') AND CODPROYECTO=? and tipo=1";
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setInt(1, idUnidadManejo);
            pstmt.setInt(2, idProyecto);
            getDatosDelSitio(pstmt, con);
        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, "Error en el objeto SectorManglaresModel  m�todo getTodosLosSectoresPorCorporacion", ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SitioManglarModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;
        }
        return sitios;
    }

    public ArrayList<Sitio> getSitioPorID(int idProyecto, int idSitio) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        sitios.clear();
        try {

            String query = "SELECT ID,   NOMBRE, ID1, ADJUNTO, TIPO, ID_CAR, ACTIVO,CODPROYECTO,DESCRIPCION, AUDITORIA FROM CASOSESTUDIO  where tipo=1 and  codproyecto=?  and ID=?";
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);
            pstmt.setInt(1, idProyecto);
            pstmt.setInt(2, idSitio);
            getDatosDelSitio(pstmt, con);
        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, "Error en el objeto SectorManglaresModel  m�todo getSitioPorID", ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SitioManglarModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;
        }
        return sitios;
    }

    private void getDatosDelSitio(PreparedStatement pstmt, Connection con) {

        ResultSet rs = null;
        try {

            rs = pstmt.executeQuery();
            while (rs.next()) {
                Sitio sitio = new Sitio();
                sitio.setNombre(rs.getString("NOMBRE"));
                sitio.setId(rs.getInt("ID"));
                sitio.setCodProyecto(rs.getInt("CODPROYECTO"));
                sitio.setIdCAR(rs.getString("ID_CAR"));
                sitio.setDescripcion(rs.getString("DESCRIPCION"));
                sitio.setIsActivo(rs.getString("ACTIVO"));
                sitio.setAuditoria(rs.getString("AUDITORIA"));                
                getDatosCaso(sitio, rs.getInt("ID"), con);

                sitios.add(sitio);
            }
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el objeto SitioManglarModel  m�todo getDatosDelSitio:" + ex.getMessage());

        }

    }

    public ResultadoTransaccion registraSitio(Sitio sitio) {
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();

        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();
        CallableStatement st = null;
        try {

            con = cFactory.createConnection("manglar");
            String sql = "{call MANGLARES_NACIONAL.INSERTAR_Estacion(?,?,?,?,?,?,?,?,?)}";
            st = con.prepareCall(sql);
            st.setString(1, sitio.getNombre());
            st.setString(2, sitio.getDescripcion());
            st.setInt(3, sitio.getIdUnidadManejo());
            st.setString(4, sitio.getIdCAR());
            st.setString(5, sitio.getAbreriatura());
            st.setString(6, sitio.getIsActivo());
            st.setInt(7, sitio.getCodProyecto());
            st.registerOutParameter(8, Types.INTEGER);
            st.registerOutParameter(9, Types.VARCHAR);
            st.executeUpdate();
            resultadoTransaccion.setCodigoError(st.getInt(8));
            resultadoTransaccion.setMensaje(st.getString(9));

            

        } catch (SQLException ex) {

            System.out.println("codigo de error:" + resultadoTransaccion.getCodigo());
            System.out.println("error en el metodo registraSitio del objeto SitioManglarModel:"
                    + ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SitioManglarModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;
        }
        return resultadoTransaccion;

    }

    public ResultadoTransaccion reasignaEstacionAUnidadManejo(Sitio sitio, int unidadManejo) {
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        resultadoTransaccion.setCodigoError(0);
        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();
        CallableStatement st = null;
        try {

            con = cFactory.createConnection("manglar");
            String sql = "{call MANGLARES_NACIONAL.reasignarEstacioAUnidadManejo(?,?,?,?)}";
            st = con.prepareCall(sql);
            st.setInt(1, sitio.getId());
            st.setInt(2, unidadManejo);
            st.registerOutParameter(3, Types.INTEGER);
            st.registerOutParameter(4, Types.VARCHAR);
            st.executeUpdate();
            resultadoTransaccion.setCodigoError(st.getInt(3));
            resultadoTransaccion.setMensaje(st.getString(4));

        } catch (SQLException ex) {

            System.out.println("codigo de error:" + resultadoTransaccion.getCodigoError());
            System.out.println("error en el metodo reasignaEstacionAUnidadManejo del objeto SitioManglarModel:"
                    + ex.getMessage());
        } finally {
            cFactory.closeConnectionInstancia(con);
        }
        return resultadoTransaccion;

    }

  

    public ResultadoTransaccion actualizarSitio(Sitio sitio) {
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();
        CallableStatement st = null;
        try {

            con = cFactory.createConnection("manglar");
            String sql = "{call MANGLARES_NACIONAL.actualizarSitio(?,?,?,?,?,?,?)}";
            st = con.prepareCall(sql);
            st.setInt(1, sitio.getId());
            st.setString(2, sitio.getIsActivo());
            st.setString(3, sitio.getNombre());
            st.setString(4, sitio.getDescripcion());
            st.setString(5, sitio.getAbreriatura());
            st.registerOutParameter(6, Types.INTEGER);
            st.registerOutParameter(7, Types.VARCHAR);
            st.executeUpdate();
            resultadoTransaccion.setCodigoError(st.getInt(6));
            resultadoTransaccion.setMensaje(st.getString(7));
            

        } catch (SQLException ex) {

            System.out.println("codigo de error:" + resultadoTransaccion.getCodigo());
            System.out.println("error en el metodo actualizarSitio del objeto SitioManglarModel:"
                    + ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SitioManglarModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;
        }
        return resultadoTransaccion;

    }

    public Map<String, String> getTodosLosTiposUnidadManejoUnidadesManejo(String sitio) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> tipoUnidadManejo = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT distinct unidad_manejo,(select valor from manglar.valores where id=unidad_manejo  ) as nombretipo FROM MANGLARES_NACIONALES   where sitio='" + sitio + "' and nivel=3 ";
        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                tipoUnidadManejo.put(rs.getString("nombretipo"), rs.getString("unidad_manejo"));
            }

        } catch (SQLException ex) {

            System.out.println("Error en el objeto SitioManglarModel en el metodo getTodosLosTiposUnidadManejoUnidadesManejo:" + ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SitioManglarModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;
        }
        return tipoUnidadManejo;
    }

    public Map<String, String> getTodosLosSitiosPorSectoresConEstadistica(int codUnidadManejo, String tematica, int nivel) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> sitios = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        JSONArray jsonArray = new JSONArray();

        String query = "SELECT distinct sitio,nombrecompletositio,codsitio FROM MANGLARES_NACIONALES  where codunidadmanejo=? and tematica=? and nivel=?";
        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            pstmt.setInt(1, codUnidadManejo);
            pstmt.setString(2, tematica);
            pstmt.setInt(3, nivel);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                JSONObject jo = new JSONObject();
                jo.put("id", rs.getString("codsitio"));
                jo.put("name", rs.getString("codsitio") + "-" + rs.getString("sitio") + "-" + rs.getString("nombrecompletositio"));
                jsonArray.put(jo);
                sitios.put(rs.getString("codsitio") + "-" + rs.getString("sitio") + "-" + rs.getString("nombrecompletositio"), rs.getString("codsitio"));
            }
            jsonDataSite = jsonArray.toString();

        } catch (SQLException ex) {

            System.out.println("Error en el objeto SitioManglarModel en el metodo getTodosLasSitiosPorSectoresConEstadistica:" + ex.getMessage());
        } catch (JSONException ex) {
            Logger.getLogger(SitioManglarModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(SitioManglarModel.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return sitios;
    }

    public Map<String, String> getTodosLosSitiosPorSectoresConEstadisticaTasaCrecimiento(String codUnidadManejo) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> sitios = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT distinct sitio,nombresitio,codsitio FROM TEMP_TASAS_CRECIMIENTO  where codunidadmanejo=? and codsitio is not null";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, codUnidadManejo);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                sitios.put(rs.getString("codsitio") + "-" + rs.getString("sitio") + "-" + rs.getString("nombresitio"), rs.getString("codsitio"));
            }

        } catch (SQLException ex) {

            System.out.println("Error en el objeto SitioManglarModel en el metodo getTodosLasSitiosPorSectoresConEstadistica:" + ex.getMessage());
        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(SitioManglarModel.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return sitios;
    }

    public Map<String, String> getTodosLosSitiosDelSector(String sector, String IdCar) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> sitios = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "select id, nombre from CASOSESTUDIO  \n"
                + "where ID_CAR='" + IdCar + "'  \n"
                + "and tipo=90\n"
                + "AND CODPROYECTO=2239 \n"
                + "and id1=" + sector + "\n"
                + "and activo='Activado'";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                sitios.put(rs.getString("nombre"), rs.getString("id"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(SitioManglarModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con = null;
        }
        return sitios;
    }

    public String getJsonDataSite() {
        return jsonDataSite;
    }

    public void setJsonDataSite(String jsonDataSite) {
        this.jsonDataSite = jsonDataSite;
    }
}
