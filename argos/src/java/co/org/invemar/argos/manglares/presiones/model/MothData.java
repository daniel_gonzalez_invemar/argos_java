package co.org.invemar.argos.manglares.presiones.model;


public class MothData {
    
   private  String month;
   private  int idMonth;

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMonth() {
        return month;
    }

    public void setIdMonth(int idMonth) {
        this.idMonth = idMonth;
    }

    public int getIdMonth() {
        return idMonth;
    }
    
    
    public MothData() {
        super();
    }
    
    public MothData(int idMoth, String nameMonth ) {
        this.idMonth = idMonth;
        this.month = nameMonth;
    }
}
