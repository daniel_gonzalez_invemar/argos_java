package co.org.invemar.argos.manglares.presiones.model;

import java.util.ArrayList;
import java.util.List;

public class Site {
    private List<PresionIndicator> presionsIndicators = new ArrayList<PresionIndicator>();
    private String name;
    private int id;
    private double value;
   
    private List<Site> sites = new ArrayList<Site>();
    private double[] v={1,2,3};


    public void setV(double[] v) {
        this.v = v;
    }

    public double[] getV() {
        return v;
    }


    public Site() {
        sites.add(new Site("s1",1,20));
        sites.add(new Site("s2",2,30));
    }
    public Site(String name, int id, double value){
        this.name=name;
        this.id= id;
        this.value=value;        
    }

    public Site(String name) {
        this.name= name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    
    public void setPresionsIndicators(List<PresionIndicator> presionsIndicators) {
        this.presionsIndicators = presionsIndicators;
    }

    public List<PresionIndicator> getPresionsIndicators() {
        return presionsIndicators;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setSites(List<Site> sites) {
        this.sites = sites;
    }

    public List<Site> getSites() {
        return sites;
    }
   

   
}
