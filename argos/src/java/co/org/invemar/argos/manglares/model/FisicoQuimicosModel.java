/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.manglares.model;

import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Daniel Gonzalez
 */
public class FisicoQuimicosModel {
    
    
    public Map<String, String> getSectores( )
    {
       Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> sectores = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT distinct sector FROM MANGLARES_NACIONALES_FQ  order by sector";
        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
//            System.out.println("sectores:"+query);
            //pstmt.setString(1, nombreSector);
            rs = pstmt.executeQuery();
            while (rs.next()) {
               sectores.put(rs.getString("sector"),rs.getString("sector"));
            }

        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto FisicoQuimicos en el metodo getSectores:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return sectores;
    }
    
    public Map<String, String> getParcelas(String sector)
    {
       Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> parcelas = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT distinct nombreparcela,codest FROM MANGLARES_NACIONALES_FQ  where nivel=1 and sector='"+sector+"'";
        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
//            System.out.println("parcelas:"+query);
            //pstmt.setString(1, nombreSector);
            rs = pstmt.executeQuery();
            while (rs.next()) {
               parcelas.put(rs.getString("nombreparcela"),rs.getString("codest"));
            }

        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto FisicoQuimicos en el metodo getParcelas:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return parcelas;
    }
    public Map<String, String> getVariables()
    {
       Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> parcelas = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT distinct variable,cualidad FROM MANGLARES_NACIONALES_FQ  where nivel=1 order by variable";
        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
//            System.out.println("variables:"+query);
            //pstmt.setString(1, nombreSector);
            rs = pstmt.executeQuery();
            while (rs.next()) {
               parcelas.put(rs.getString("variable")+"   "+rs.getString("cualidad"),rs.getString("variable")+"-"+rs.getString("cualidad"));
            }

        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto FisicoQuimicos en el metodo getVariables:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return parcelas;
    }
    
    
}
