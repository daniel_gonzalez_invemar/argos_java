/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.TipoFisiografico;
import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class TiposFisiograficosModel {
 
    public HashMap<String, String>  getAllTiposFisiograficos(){
       
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
         HashMap<String, String> listTiposFisiograficos= new HashMap<String, String>();
        try {

            String query = "SELECT PK, NOMBRE, DESCRIPCION FROM TIPOS_FISIOGRAFICOS order by nombre ";
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){               
                listTiposFisiograficos.put(rs.getString("nombre"),rs.getString("pk"));       
            }
                    
            
            
        } catch (SQLException ex) {
            Logger.getLogger(TiposFisiograficosModel.class.getName()).log(Level.SEVERE, "Error en el objeto TiposFisiograficosModel  m�todo getAllTiposFisiograficos", ex.getMessage());           
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(TiposFisiograficosModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listTiposFisiograficos;
    }
    
}
