/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.Presion;
import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author usrsig15
 */
public class PresionesModel {
    
     public ArrayList<Presion> getTodosLasPresionesDelFactor(int idFactor) {
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        ArrayList<Presion> presiones= new ArrayList<Presion>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "select idpresion, nompresion  from FACTOREYPRESIONES where idfactor="+idFactor+" order by nompresion";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
           // System.out.println("query presiones :" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Presion presion = new Presion();
                presion.setId(rs.getString("idpresion"));
                presion.setNombre(rs.getString("nompresion"));                 
                presiones.add(presion);
            }

        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto PresionesModel en el metodo getTodosLasPresionesDelFactor:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return presiones;
    }
    
}
