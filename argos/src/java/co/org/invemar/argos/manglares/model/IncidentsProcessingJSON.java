/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author usrsig15
 */
public class IncidentsProcessingJSON {

    public static void main(String[] args) {
        IncidentsProcessingJSON processingJSON = new IncidentsProcessingJSON();
        processingJSON.JsonToObject();
    }

    public void JsonToObject() {
        try {
            URL url = new URL("http://cinto.invemar.org.co/egreta/api?task=incidents");

            InputStream is = url.openStream();
            JsonReader rdr = Json.createReader(is);

            JsonObject obj;
            obj = rdr.readObject();
            JsonArray results = obj.getJsonArray("data");
            for (JsonObject result : results.getValuesAs(JsonObject.class)) {
                System.out.print(result.getJsonObject("from").getString("name"));
                System.out.print(": ");
                System.out.println(result.getString("message", ""));
                System.out.println("-----------");
            }
        } catch (MalformedURLException e) {

        } catch (IOException ex) {
            Logger.getLogger(IncidentsProcessingJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
