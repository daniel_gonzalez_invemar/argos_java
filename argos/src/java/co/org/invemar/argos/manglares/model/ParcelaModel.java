/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.Componente;
import co.org.invemar.argos.manglares.vo.Parcela;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.ResultadoTransaccion;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class ParcelaModel implements Serializable{

    private ArrayList<Parcela> parcelas;
    private String jsonDataParcel;

    public ParcelaModel() {
        parcelas = new ArrayList<Parcela>();
    }

    private void getDatosCaso(Parcela parcela, int id, Connection con) {

        PreparedStatement pstmt = null;
        ResultSet rs1 = null;       
        try {

            String query = "select * from casosestudio_atributos where casosestudio_id=" + id;
            pstmt = con.prepareStatement(query);
            rs1 = pstmt.executeQuery();
            
           
            while (rs1.next()) {             
                
              
                
                 if (rs1.getString("CODIGO_ATRIBUTO").equals("45")) {
                     parcela.setExtension(Double.parseDouble(rs1.getString("valor2").trim()));
                }                
                 
                 if (rs1.getString("CODIGO_ATRIBUTO").equals("87")) {
                     parcela.setTipoFisiografico(Long.parseLong(rs1.getString("valor2").trim()));
                }   
               
                if (rs1.getString("CODIGO_ATRIBUTO").equals("69")) {
                    parcela.setAzimut(Integer.parseInt(rs1.getString("valor2")));
                }
                if (rs1.getString("CODIGO_ATRIBUTO").equals("70")) {
                    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
                    Date fechaconvertida = formatoFecha.parse(rs1.getString("valor2"));               

                    parcela.setFechaInstalacion(new java.sql.Date(fechaconvertida.getTime()));

                }

                if (rs1.getString("CODIGO_ATRIBUTO").equals("68")) {
                    parcela.setFormaParcela(Integer.parseInt(rs1.getString("valor2")));

                }
                Componente c = new Componente();
               
                
                 if (rs1.getString("CODIGO_ATRIBUTO")!=null && rs1.getString("CODIGO_ATRIBUTO").equals("84")) {
                     
                    
                     if (Integer.parseInt(rs1.getString("valor2"))==87) {
                          c.setId(87);
                          c.setNombre("Estructura");
                     }else if (Integer.parseInt(rs1.getString("valor2"))==4){
                          c.setId(4);
                          c.setNombre("Regeneracion");
                     } else {
                          c.setId(0);
                          c.setNombre("Sin componente");
                     }                    
                   
                    
                      parcela.setComponente(c);
                }           
                  
            }

             
        } catch (Exception ex) {
            System.out.println("Error en el objeto ParcelaModel  m�todo getDatosCaso:" + ex.getMessage());

        } finally {
            try {
                rs1.close();
                rs1=null;
            } catch (SQLException ex) {
                Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void getDatosArgos(Parcela parcela) {

        PreparedStatement pstmt = null;
        ResultSet rs2 = null;
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        try {

            String query = "SELECT LATITUDINICIO_LOC,  \n"
                    + "  LONGITUDINICIO_LOC,     \n"
                    + "  VIGENTE_LOC,  \n"
                    + "  (SELECT VALOR2 FROM  MANGLAR.DATOS_CASOS_ESTUDIO WHERE ID=secuencia_loc AND CODIGO_ATRIBUTO=45) AS  AREA,\n"
                    + " DESCRIPCION_ESTACION_LOC "
                    + "FROM CLOCALIDADT where secuencia_loc=?";
            con = conectionfactory.createConnection("geograficos");
            pstmt = con.prepareStatement(query);
            pstmt.setInt(1, parcela.getId());
            rs2 = pstmt.executeQuery();

            while (rs2.next()) {
                parcela.setLatitud(rs2.getDouble("LATITUDINICIO_LOC"));          
                parcela.setLongitud(rs2.getDouble("LONGITUDINICIO_LOC"));             
                parcela.setDescripcion(rs2.getString("DESCRIPCION_ESTACION_LOC"));
                if (rs2.getString("vigente_loc").equals("S")) {
                    parcela.setIsActivo("Activado");
                } else {
                    parcela.setIsActivo("Desactivado");
                }


            }
        } catch (Exception ex) {
            System.out.println("Error en el objeto ParcelaModel  m�todo getDatosArgos:" + ex.getMessage());

        } finally {
            try {
                rs2.close();
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public ArrayList<Parcela> getTodosLasParcelasPorCorporacion(int idUnidadManejo, String idCAR, int idProyecto) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        parcelas.clear();
        try {

            String query = "SELECT ID,\n"
                    + "  NOMBRE,\n"
                    + "  ID1,\n"
                    + "  ADJUNTO,\n"
                    + "  TIPO,\n"
                    + "  ID_CAR,\n"
                    + "  ACTIVO,\n"
                    + "  CODPROYECTO,\n"
                    + "  DESCRIPCION\n"
                    + "FROM CASOSESTUDIO\n"
                    + "WHERE  id_car     ='"+idCAR+"'\n"
                    + "AND CODPROYECTO="+idProyecto+"\n"
                    + "AND tipo       =94\n"
                    + "AND ID IN (SELECT IDPARCELA FROM PARCELASXUNIDADMANEJO UM WHERE um.idunidadmanejo="+idUnidadManejo+")";
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);
            getDatosParcela(pstmt, con);

        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto ParcelaModel  m�todo getTodosLasParcelasPorCorporacion:" + ex.getMessage());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con=null;
        }
        return parcelas;
    }
    
     public ArrayList<Parcela> getTodosLasParcelasPorEstacion(int idEstacion, int idProyecto) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        parcelas.clear();
        try {

            String query = "SELECT ID,\n"
                    + "  NOMBRE,\n"
                    + "  ID1,\n"
                    + "  ADJUNTO,\n"
                    + "  TIPO,\n"
                    + "  ID_CAR,\n"
                    + "  ACTIVO,\n"
                    + "  CODPROYECTO,\n"
                    + "  DESCRIPCION\n"
                    + "FROM CASOSESTUDIO\n"
                    + "WHERE  CODPROYECTO="+idProyecto+"\n"
                    + "AND tipo       =94\n"
                    + "AND ID IN (SELECT IDPARCELA FROM PARCELASXUNIDADMANEJO UM WHERE um.idestacion="+idEstacion+") order by id desc";
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);

            getDatosParcela(pstmt, con);

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, ex.getMessage(),ex);
            
        } finally {
            try {
                con.close();
                
            } catch (SQLException ex) {
                Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            con=null;
        }
        return parcelas;
    }
    

    public ArrayList<Parcela> getParcelaPorID(int idProyecto, int idParcela) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        
        try {

            String query = "SELECT ID,   NOMBRE, ID1, ADJUNTO, TIPO, ID_CAR, ACTIVO,CODPROYECTO,DESCRIPCION FROM CASOSESTUDIO  where tipo=94 and codproyecto=?  and ID=?";
            con = conectionfactory.createConnection("manglar");
            PreparedStatement pstmt = con.prepareStatement(query);          
            pstmt.setInt(1, idProyecto);
            pstmt.setInt(2, idParcela);
            getDatosParcela(pstmt, con);
        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto ParcelaModel  m�todo getParcelaPorID:" + ex.getMessage());
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return parcelas;
    }

    private void getDatosParcela(PreparedStatement pstmt, Connection con) {

        ResultSet rs = null;
        parcelas.clear();
        try {

           
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Parcela parcela = new Parcela();
                parcela.setNombre(rs.getString("NOMBRE"));
                parcela.setId(rs.getInt("ID"));
                parcela.setCodProyecto(rs.getInt("CODPROYECTO"));
                parcela.setIdCAR(rs.getString("ID_CAR"));
                parcela.setDescripcion(rs.getString("DESCRIPCION"));
                parcela.setIsActivo(rs.getString("ACTIVO"));               
                getDatosCaso(parcela, rs.getInt("ID"), con);
                getDatosArgos(parcela);
                parcelas.add(parcela);
            }
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el objeto Parcela Model  m�todo getDatosParcela:" + ex.getMessage());

        }

    }

    public ResultadoTransaccion registrarParcela(Parcela parcela) {
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();

        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();
        CallableStatement st = null;
        try {

            con = cFactory.createConnection("manglar");
            String sql = "{call  MANGLARES_NACIONAL.INSERTARParcela(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            st = con.prepareCall(sql);
            st.setInt(1, parcela.getIdEstacion());
            st.setInt(2, parcela.getFormaParcela());
            st.setInt(3, parcela.getAzimut());      
            st.setDouble(4, parcela.getExtension());
            st.setString(5, parcela.getFechaInstalacion().toString());
            st.setDouble(6, parcela.getLatitud());
            st.setDouble(7, parcela.getLongitud());
            st.setString(8, parcela.getDescripcion());
            st.setString(9, parcela.getIdCAR());
            st.setString(10, parcela.getIsActivo());
            st.setInt(11, parcela.getCodProyecto());          
            st.setInt(12, parcela.getComponente().getId());               
            st.setLong(13, parcela.getTipoFisiografico());   
            st.registerOutParameter(14, Types.INTEGER);
            st.registerOutParameter(15, Types.VARCHAR);
            st.executeUpdate();
            resultadoTransaccion.setCodigoError(st.getInt(14));
            resultadoTransaccion.setMensaje(st.getString(15));
            System.out.println("resultadoTransaccion:" + resultadoTransaccion.getCodigoError());
            System.out.println("resultado proceso:" + resultadoTransaccion.getMensaje());

        } catch (SQLException ex) {            
            System.out.println("codigo de error:" + resultadoTransaccion.getCodigo());
            System.out.println("error en el metodo registrarParcela del objeto ParcelaModelo:"
                    + ex.getMessage());
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return resultadoTransaccion;

    }

    public ResultadoTransaccion actualizarParcela(Parcela parcela) {
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();
        CallableStatement st = null;
        try {

            con = cFactory.createConnection("manglar");
            String sql = "{call MANGLARES_NACIONAL.actualizarParcela(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            st = con.prepareCall(sql);
            st.setInt(1, parcela.getId());
            st.setInt(2, parcela.getFormaParcela());
            st.setInt(3, parcela.getAzimut());        
            st.setDouble(4, parcela.getExtension());
            st.setString(5, parcela.getFechaInstalacion().toString());
            st.setDouble(6, parcela.getLatitud());
            st.setDouble(7, parcela.getLongitud());
            st.setString(8, parcela.getDescripcion());
            st.setString(9, parcela.getIdCAR());
            st.setString(10, parcela.getIsActivo());
            st.setInt(11, parcela.getCodProyecto());
            st.setInt(12, parcela.getComponente().getId());
            st.setLong(13,parcela.getTipoFisiografico());
            st.registerOutParameter(14, Types.INTEGER);
            st.registerOutParameter(15, Types.VARCHAR);


            st.executeUpdate();
            resultadoTransaccion.setCodigoError(st.getInt(14));
            resultadoTransaccion.setMensaje(st.getString(15));


        } catch (SQLException ex) {            
            System.out.println("codigo de error:" + resultadoTransaccion.getMensaje());
            System.out.println("error en el metodo actualizarParcela del objeto ParcelaManglarModel:" + ex.getMessage());
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return resultadoTransaccion;

    }

    public Map<String, String> getTodosLasParcelasConEstadistica(int codSitio, String tematica) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> parcelas = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        JSONArray jsonArray = new JSONArray();
        
        

        String query = "SELECT distinct parcela,codest FROM MANGLARES_NACIONALES  \n"
                + "where codsitio=? and tematica=? and parcela is not null order by parcela";
        try {
            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            pstmt.setInt(1, codSitio);                     
            pstmt.setString(2, tematica);
            
            
            rs = pstmt.executeQuery();
            while (rs.next()) {
                JSONObject jo  = new JSONObject();
               
                jo.put("id",rs.getString("codest"));
                jo.put("name",rs.getString("codest")+"-"+rs.getString("parcela"));
                jsonArray.put(jo);  
                
                parcelas.put(rs.getString("codest")+"-"+rs.getString("parcela"), rs.getString("codest"));
            }
            jsonDataParcel=jsonArray.toString();
                    

        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            
        } catch (JSONException ex) {
            Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
           con=null;
        }
        return parcelas;
    }
    
    public Map<String, String> getTodosLasParcelasConEstadisticaTasaCrecimiento(String sitio) {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> parcelas = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "SELECT distinct parcela,codparcela FROM TEMP_TASAS_CRECIMIENTO  where codsitio=? and parcela is not null order by parcela";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);                      
            pstmt.setString(1, sitio);
            
            
            rs = pstmt.executeQuery();
            while (rs.next()) {
                parcelas.put(rs.getString("codparcela")+"-"+rs.getString("parcela"), rs.getString("codparcela"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
          
        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(ParcelaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return parcelas;
    }

    public String getJsonDataParcel() {
        return jsonDataParcel;
    }

    public void setJsonDataParcel(String jsonDataParcel) {
        this.jsonDataParcel = jsonDataParcel;
    }
    
}
