/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.CoordenadasIndividuos;
import co.org.invemar.argos.manglares.vo.SectorManglarVo;
import co.org.invemar.library.siam.exportadores.Data;
import co.org.invemar.library.siam.exportadores.Rows;
import org.apache.log4j.Logger;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.ResultadoTransaccion;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class CoordenadasIndividuosModel {

    private ArrayList<String> dataColum = null;
    private ArrayList<Rows> rowsData = null;

    public ResultadoTransaccion insertCoordenateTree(CoordenadasIndividuos ci) throws IOException, SQLException {
        String fecha_toma, descripcionImagen;
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();

        CallableStatement st = null;

        con = cFactory.createConnection("manglar");

        String sql = "{call MANGLARES_NACIONAL.insertCoordenateTree(?,?,?,?,?,?,?,?,?,?,?,?)}";
        st = con.prepareCall(sql);
        st.setString(1, ci.getEntity());
        st.setLong(2, ci.getIdElement());
        st.setLong(3, ci.getTag());
        st.setLong(4, ci.getAzimut());
        st.setDouble(5, ci.getDistance());
        st.setDouble(6, ci.getX());
        st.setDouble(7, ci.getY());
        st.setLong(8, ci.getCodParcel());
        st.setString(9, ci.getCodSpecies());
        st.setString(10, ci.getParcelShade());
        st.registerOutParameter(11, Types.NUMERIC);
        st.registerOutParameter(12, Types.VARCHAR);

        st.executeUpdate();

        resultadoTransaccion = new ResultadoTransaccion();
        resultadoTransaccion.setCodigo(st.getInt(11));
        resultadoTransaccion.setCodigoError(st.getInt(11));
        resultadoTransaccion.setMensaje(st.getString(12));

        con.close();
        con = null;

        return resultadoTransaccion;

    }
    public String ChangeValueByString(String source, String value,String replace){
        String newString="";
        if (source.equalsIgnoreCase(value)) {
            newString=replace;
        }else{
         newString=source; 
        }
        return newString;
    }
   

    public String getAllCoordenadasIndivudosByEntity(String entity,String parcelId) {
        JSONArray jsonarrayData = new JSONArray();
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT ENTIDAD, "
                + "  ID_ELEMENTO, "
                + "  TAG, "
                + "  AZIMUT, "
                + "  DISTANCIA, "
                + "  X, "
                + "  Y, "
                + "  ID_PARCELA, "
                + "  CODIGO_ESPECIE, "
                + "  (SELECT valor2  FROM DATOS_CASOS_ESTUDIO where codigo_atributo=68 and id=ID_PARCELA) as forma_parcela, "
                + "  PK,  "
                + " (select nombre from casosestudio where id=ID_PARCELA) as nombreParcela,"
                + " (SELECT descripcion FROM monitoreom.BLST_ESPECIES where CLAVE_SIBM=CODIGO_ESPECIE) as nombreEspecie "
                + "FROM COORDENADASPORINDIVIDUOS where entidad=? ";
        try {

            if(parcelId!=null){
                query=query+" and id_parcela=? ";
            }
            query=query+" ORDER BY pk DESC";
            
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1,entity );
            
             if(parcelId!=null){
                pstmt.setString(2,parcelId);
            }
            
           
            rs = pstmt.executeQuery();

            dataColum = new ArrayList<String>();
            dataColum.add("ID_REGISTRO");
            dataColum.add("ID_ELEMENTO");
            dataColum.add("TAG");
            dataColum.add("AZIMUT");
            dataColum.add("DISTANCIA");         
            dataColum.add("X");
            dataColum.add("Y");
            dataColum.add("NOMBRE PARCELA");
            dataColum.add("ESPECIE");
          
            

            rowsData=new ArrayList<Rows>();
            
            while (rs.next()) {
                JSONObject jo = new JSONObject();
               
                Rows r = new Rows();
                r.addData(new Data(rs.getString("PK")));
                r.addData(new Data(rs.getString("ID_ELEMENTO")));
                r.addData(new Data(rs.getString("TAG")));
                r.addData(new Data(ChangeValueByString(rs.getString("AZIMUT"), "-1","No aplica")));
                r.addData(new Data(ChangeValueByString(rs.getString("DISTANCIA"), "-1","No aplica")));
                r.addData(new Data(ChangeValueByString(rs.getString("X"), "-1","No aplica")));
                r.addData(new Data(ChangeValueByString(rs.getString("Y"), "-1","No aplica")));
                r.addData(new Data(rs.getString("nombreParcela")));
                r.addData(new Data(rs.getString("nombreEspecie")));
                rowsData.add(r);

                jo.put("ID_ELEMENTO", rs.getString("ID_ELEMENTO"));
                jo.put("TAG", rs.getString("TAG"));
                jo.put("AZIMUT", rs.getString("AZIMUT"));
                jo.put("DISTANCIA", rs.getString("DISTANCIA"));
                jo.put("FORMA_PARCELA", rs.getString("forma_parcela"));
                jo.put("X", rs.getString("X"));
                jo.put("Y", rs.getString("Y"));
                jo.put("ID_PARCELA", rs.getString("ID_PARCELA"));
                jo.put("CODIGO_ESPECIE", rs.getString("CODIGO_ESPECIE"));
                jo.put("ID_PARCELA", rs.getString("ID_PARCELA"));
                jo.put("PK", rs.getString("PK"));
                jo.put("nombreParcela", rs.getString("nombreParcela"));
                jo.put("nombreEspecie", rs.getString("nombreEspecie"));

                jsonarrayData.put(jo);

            }

        } catch (SQLException ex) {
            Logger.getLogger(getClass().getName()).error(ex.getMessage());
        } catch (JSONException ex) {
            Logger.getLogger(getClass().getName()).error(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).error(ex.getMessage());

        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(getClass().getName()).error(ex.getMessage());
            }

        }

        return jsonarrayData.toString();
    }

    public ResultadoTransaccion updateCoordenateTree(CoordenadasIndividuos ci) throws IOException, SQLException {
        String fecha_toma, descripcionImagen;
        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();

        CallableStatement st = null;

        con = cFactory.createConnection("manglar");

        String sql = "{call MANGLARES_NACIONAL.updateCoordenateTree(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        st = con.prepareCall(sql);
        st.setLong(1, ci.getPk());
        st.setString(2, ci.getEntity());
        st.setLong(3, ci.getIdElement());
        st.setLong(4, ci.getTag());
        st.setLong(5, ci.getAzimut());
        st.setDouble(6, ci.getDistance());
        st.setDouble(7, ci.getX());
        st.setDouble(8, ci.getY());
        st.setLong(9, ci.getCodParcel());
        st.setString(10, ci.getCodSpecies());
        st.setString(11, ci.getParcelShade());
        st.registerOutParameter(12, Types.NUMERIC);
        st.registerOutParameter(13, Types.VARCHAR);

        st.executeUpdate();

        resultadoTransaccion = new ResultadoTransaccion();
        resultadoTransaccion.setCodigo(st.getInt(12));
        resultadoTransaccion.setCodigoError(st.getInt(12));
        resultadoTransaccion.setMensaje(st.getString(13));

        System.out.println("resultado de la transaction:" + st.getString(13));

        con.close();
        con = null;

        return resultadoTransaccion;

    }

    public ArrayList<String> getDataColum() {
        return dataColum;
    }

    public void setDataColum(ArrayList<String> dataColum) {
        this.dataColum = dataColum;
    }

    public ArrayList<Rows> getRowsData() {
        return rowsData;
    }

    public void setRowsData(ArrayList<Rows> rowsData) {
        this.rowsData = rowsData;
    }

}
