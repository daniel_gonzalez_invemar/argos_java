/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.FactorForzante;
import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author usrsig15
 */
public class FactoresForzanteModel {
    
    public ArrayList<FactorForzante> getTodosLosFactoresNivel1() {
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        ArrayList<FactorForzante> factoresForzantes= new ArrayList<FactorForzante>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "select id, nombre from factores where id=nodopadre";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
            //System.out.println("query factores forzante nivel 1:" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                FactorForzante factorForzante = new FactorForzante();
                factorForzante.setId(rs.getLong("id"));
                factorForzante.setNombre(rs.getString("nombre"));                
                factoresForzantes.add(factorForzante);
            }

        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto FactoresForzanteModel en el metodo getTodosLosFactoresNivel1:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return factoresForzantes;
    }
    
    public ArrayList<FactorForzante> getTodosLosFactoresNivel2(int idPadre) {
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        ArrayList<FactorForzante> factoresForzantes= new ArrayList<FactorForzante>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String query = "select id,nombre from factores where nodopadre="+idPadre+" and id!=nodopadre";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
            //System.out.println("query factores forzante nivel 2:" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                FactorForzante factorForzante = new FactorForzante();
                factorForzante.setId(rs.getLong("id"));
                factorForzante.setNombre(rs.getString("nombre"));                
                factoresForzantes.add(factorForzante);
            }

        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto FactoresForzanteModel en el metodo getTodosLosFactoresNivel2:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return factoresForzantes;
    }
    
}
