/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

/**
 *
 * @author usrsig15
 */
public class DatosCalculados {
    private String anio;
    private String Especie;
    private String codEspecie;    
    private Number valor;

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public Number getValor() {
        return valor;
    }

    public void setValor(Number valor) {
        this.valor = valor;
    }

    public String getEspecie() {
        return Especie;
    }

    public void setEspecie(String Especie) {
        this.Especie = Especie;
    }

    public String getCodEspecie() {
        return codEspecie;
    }

    public void setCodEspecie(String codEspecie) {
        this.codEspecie = codEspecie;
    }
    
}
