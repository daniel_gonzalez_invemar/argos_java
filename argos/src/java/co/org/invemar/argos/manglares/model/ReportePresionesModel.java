/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.argos.manglares.vo.Reporte;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.ResultadoTransaccion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Daniel Gonzalez
 */
public class ReportePresionesModel {

    public ArrayList<Reporte> getTodaLosReportes(String fecha, String codDepartamento, String codSector, String codSitio) {
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        ArrayList<Reporte> reportes = new ArrayList<Reporte>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "select idcar as entidad, id, to_char(fecha,'dd-MM-yyyy') as fecha  from reportes  \n"
                + "where coddepartamento="+codDepartamento+" \n"
                + "and sector="+codSector+" \n"
                + "and codsitio="+codSitio+" \n"
                + "and fecha=to_date('"+fecha+"','dd-MM-yyyy') order by fecha";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
            System.out.println("consulta fechas:" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Reporte reporte = new Reporte();
                reporte.setId(rs.getString("id"));
                reporte.setFecha(rs.getString("fecha"));
                reporte.setEntidad(rs.getString("entidad"));                
                reportes.add(reporte);
            }
        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto ReportePresionesModel en el metodo getTodaLosReportes:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return reportes;

    }

    public Map<String, String> getTodasLasFechasReportes() {
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> fechas = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "select distinct to_char(fecha,'dd-MM-yyyy') as fecha  from reportes";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
//            System.out.println("consulta fechas:" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                fechas.put(rs.getString("fecha"), rs.getString("fecha"));
            }
        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto ReportePresionesModel en el metodo getTodasLasFechasReportes:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return fechas;

    }

    public Map<String, String> getTodasLosSectores(String codDepartamento) {
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> sectores = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT distinct sector,(select nombre from casosestudio where id=sector) as nombresector FROM reportes WHERE coddepartamento=" + codDepartamento + "";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
//            System.out.println("consulta sectores :" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                sectores.put(rs.getString("nombresector"), rs.getString("sector"));
            }
        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto ReportePresionesModel en el metodo getTodasLosSectores:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return sectores;

    }

    public Map<String, String> getTodasLosSitios(String sector) {
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> sitios = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT distinct codsitio,(select nombre from casosestudio where id=codsitio) as nombresitio FROM reportes WHERE sector=" + sector;
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
//            System.out.println("consulta sitios :" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                sitios.put(rs.getString("nombresitio"), rs.getString("codsitio"));
            }
        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto ReportePresionesModel en el metodo getTodasLosSitios:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return sitios;

    }

    public Map<String, String> getTodasLosDepartamentos(String fecha) {
        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> departamentos = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT DISTINCT coddepartamento,\n"
                + "  (SELECT TOPONIMO_TEXTO\n"
                + "  FROM geograficos.CLST_TOPONIMIAT\n"
                + "  WHERE toponimo_tp=coddepartamento\n"
                + "  ) AS nombredepartamento\n"
                + "FROM reportes\n"
                + "WHERE fecha=to_date('" + fecha + "','dd-MM-yyyy')";
        try {
            con = conectionfactory.createConnection("manglar");
            pstmt = con.prepareStatement(query);
//            System.out.println("consulta fechas:" + query);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                departamentos.put(rs.getString("nombredepartamento"), rs.getString("coddepartamento"));
            }
        } catch (SQLException ex) {
            //Logger.getLogger(SectorManglaresModel.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error en el objeto ReportePresionesModel en el metodo getTodasLosDepartamentos:" + ex.getMessage());
        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return departamentos;

    }

    public ResultadoTransaccion ingresaReporte(String idCAR, Date fecha, String codDepartamento, String codSector, String codSitio, List ListaActividadesEconomicas, ArrayList ListaPresiones) {

        ResultadoTransaccion resultadoTransaccion = new ResultadoTransaccion();
        Connection con = null;
        ConnectionFactory cFactory = new ConnectionFactory();
        CallableStatement st = null;
        String actividadesEconomicas = "-1";
        String presiones = "-1";
        try {
            Iterator<String> it = ListaActividadesEconomicas.iterator();
            while (it.hasNext()) {
                actividadesEconomicas = actividadesEconomicas + "," + it.next();
            }
            actividadesEconomicas = actividadesEconomicas + ",-1";
//            System.out.println("String activicades Econimicas:"+actividadesEconomicas);

            it = ListaPresiones.iterator();
            while (it.hasNext()) {
                presiones = presiones + "," + (String) it.next();
            }
            presiones = presiones + ",-1";
//            System.out.println("String presiones:"+presiones);

            java.sql.Date fechaDB = new java.sql.Date(fecha.getTime());

            con = cFactory.createConnection("manglar");
            String sql = "{call MANGLARES_NACIONAL.INSERTAR_reportePresiones(?,?,?,?,?,?,?,?,?)}";
            st = con.prepareCall(sql);
            st.setString(1, idCAR);
            st.setDate(2, fechaDB);
            st.setString(3, codDepartamento);
            st.setString(4, codSector);
            st.setString(5, codSitio);
            st.setString(6, actividadesEconomicas);
            st.setString(7, presiones);
            st.registerOutParameter(8, Types.INTEGER);
            st.registerOutParameter(9, Types.VARCHAR);
            st.executeUpdate();
            resultadoTransaccion.setCodigoError(Integer.parseInt(st.getString(8)));
            resultadoTransaccion.setMensaje(st.getString(9));

        } catch (Exception ex) {
            System.out.println("codigo de error:" + resultadoTransaccion.getCodigoError());
            System.out.println("error en el metodo ingresaReporte del objeto ReportePresionesModel:" + ex.getMessage());
        } finally {
            cFactory.closeConnectionInstancia(con);
        }
        return resultadoTransaccion;

    }

}
