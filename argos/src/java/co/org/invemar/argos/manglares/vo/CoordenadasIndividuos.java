/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.vo;

/**
 *
 * @author usrsig15
 */
public class CoordenadasIndividuos {
    private long pk;
    private String entity;
    private long idElement;
    private long tag;
    private long azimut;
    private double distance;
    private double  x;
    private double y;
    private long codParcel;
    private String codSpecies;
    private String parcelShade;

    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public long getIdElement() {
        return idElement;
    }

    public void setIdElement(long idElement) {
        this.idElement = idElement;
    }

    public long getTag() {
        return tag;
    }

    public void setTag(long tag) {
        this.tag = tag;
    }

    public long getAzimut() {
        return azimut;
    }

    public void setAzimut(long azimut) {
        this.azimut = azimut;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public long getCodParcel() {
        return codParcel;
    }

    public void setCodParcel(long codParcel) {
        this.codParcel = codParcel;
    }

    public String getCodSpecies() {
        return codSpecies;
    }

    public void setCodSpecies(String codSpecies) {
        this.codSpecies = codSpecies;
    }

    public String getParcelShade() {
        return parcelShade;
    }

    public void setParcelShade(String parcelShade) {
        this.parcelShade = parcelShade;
    }
    
}
