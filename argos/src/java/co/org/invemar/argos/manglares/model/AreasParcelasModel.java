/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.ResultadoTransaccion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author root
 */
public class AreasParcelasModel {
    
    
     public ResultadoTransaccion SaveAreaParcel(String codParcel, String anio, String parcelNumber, String area, String unit, String userId, String entity)
    {
        ResultadoTransaccion resultadoTransaccion=new ResultadoTransaccion();
       try {         
               
        ConnectionFactory cFactory = new ConnectionFactory();        
        Connection con = cFactory.createConnection("manglar");
        String sql = "{call  manglares_nacional.insert_areas_parcelas(?,?,?,?,?,?,?,?,?)}";
        CallableStatement st = con.prepareCall(sql);        
        st.setString(1, codParcel);
        st.setString(2, anio);
        st.setString(3, parcelNumber);
        st.setString(4, area);
        st.setString(5, unit);
        st.setString(6, entity);
        st.setString(7, userId);
        st.registerOutParameter(8, 4);
        st.registerOutParameter(9, 12);
        st.executeUpdate();
        resultadoTransaccion.setCodigoError(st.getInt(8));
        resultadoTransaccion.setMensaje(st.getString(9));      
        
         } catch (SQLException ex) {
             Logger.getLogger(AreasParcelasModel.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
         }
              
       
        return resultadoTransaccion;
    }

    
    
}
