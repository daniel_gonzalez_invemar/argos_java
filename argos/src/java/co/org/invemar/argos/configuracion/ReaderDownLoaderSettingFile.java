/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.configuracion;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.jdom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author usrsig15
 */
public class ReaderDownLoaderSettingFile {

    private String pathFile = "";
  

    public ReaderDownLoaderSettingFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public ArrayList<Field> getField(String Codthematic) {
        ArrayList<Field> fieldList = new ArrayList<>();

        File fXmlFile = new File(pathFile);
        if (fXmlFile.exists()) {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;
            try {
                dBuilder = dbFactory.newDocumentBuilder();
                org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);

                NodeList nList = doc.getElementsByTagName("tematica");

                for (int x = 0; x < nList.getLength(); x++) {
                    if (Codthematic.equalsIgnoreCase(nList.item(x).getAttributes().getNamedItem("id").getNodeValue())) {

                        for (int i = 0; i < nList.item(x).getChildNodes().item(1).getChildNodes().getLength(); i++) {
                            if ("field".equalsIgnoreCase(nList.item(x).getChildNodes().item(1).getChildNodes().item(i).getNodeName())) {
                                NamedNodeMap nnm = nList.item(x).getChildNodes().item(1).getChildNodes().item(i).getAttributes();
                                Field field = new Field();
                                Map<String, String> map = new HashMap<String, String>();
                                for (int j = 0; j < nnm.getLength(); j++) {
                                    map.put(nnm.item(j).getNodeName(), nnm.item(j).getNodeValue());
                                }
                                field.setAttribute(map);
                                fieldList.add(field);
                            }
                        }

                    } else {
                        //Logger.getLogger(ReaderDownLoaderSettingFile.class.getName()).log(Level.INFO, "thematic not found", "thematic not found");
                    }
                }

            } catch (ParserConfigurationException ex) {
                Logger.getLogger(ReaderDownLoaderSettingFile.class.getName()).log(Level.SEVERE, ex.getMessage(), ex.getMessage());
            } catch (SAXException ex) {
                Logger.getLogger(ReaderDownLoaderSettingFile.class.getName()).log(Level.SEVERE, ex.getMessage(), ex.getMessage());
            } catch (IOException ex) {
                Logger.getLogger(ReaderDownLoaderSettingFile.class.getName()).log(Level.SEVERE, ex.getMessage(), ex.getMessage());
            }
            
        } 
        return fieldList;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

   
}
