/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.configuracion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author usrsig15
 */
public class CargaConfiguracion {

    public Properties leeArchivoConfiguracion(String path) {
        Properties propiedades = new Properties();
         
        try {          
            
            propiedades.load(new FileInputStream(path+"configuracion.properties"));         

        } catch (FileNotFoundException e) {
            System.out.println("Error en el objeto:CargaConfiguracion. M�todo: leeArchivoConfiguracion."+e.getMessage());
        } catch (IOException e) {
             System.out.println("Error en el objeto:CargaConfiguracion. M�todo: leeArchivoConfiguracion."+e.getMessage());
        }
        return propiedades;

    }
}
