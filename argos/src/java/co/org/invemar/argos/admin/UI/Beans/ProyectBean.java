/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.admin.UI.Beans;

import co.org.invemar.argos.monitoreum.persistencia.BproyectoXComp;
import co.org.invemar.argos.monitoreum.persistencia.BubicacionTematica;
import co.org.invemar.siam.sibm.model.ProyectosDM;
import co.org.invemar.siam.sibm.vo.Proyecto;
import co.org.invemar.util.ConnectionFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author usrsig15
 */
@ManagedBean(name = "proyectosBean")
@SessionScoped
public class ProyectBean implements Serializable {

    private ProyectosDM proyectoDM;
    private List<Proyecto> proyectos;
    private Proyecto proyectoSeleccionado;
    private List<Proyecto> filterprojects;
    private long tamanioArchivoLogoProyecto;
    private InputStream logoProyecto;
    private List<BubicacionTematica> locationsThematics;
    private int selectedProject = 0;
    private TematicaBean thematicBean;
    /*Manejo de la ventade proyectos.*/
    private boolean tabBasicDataProjectEnabled = false;
    private int activeIndexWinProject = 0;
    /**/
    //Variables formulari proyectos
    private String nombrecorto = "";
    private String nombreCompleto = "";
    private String ejecutor = "";
    private Date fechaInicio;
    private Date fechaFinalizcion;
    private String nombreArchivoCSS = "";

    private boolean logged = false;
    //Cierre variables proyectos.

    public ProyectBean() {
        proyectoDM = new ProyectosDM();
//        locationsThematics = new ArrayList<BubicacionTematica>();
        LoadProjectTable();
       
    }

    public void addNewComponent() {

        FacesContext fc = FacesContext.getCurrentInstance();

        Object bean;

        ELContext elContext = fc.getELContext();
        bean = elContext.getELResolver().getValue(elContext, null, "TematicBean");

        loginControl();

        //locationsThematics.add(null);
    }

    private void loginControl() {
//        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//        String entity = (String) session.getAttribute("entityid");
//
//        if (entity == null) {
//            try {
//                String servername = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
//                int serverport = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
//                System.out.println("servername:" + servername);
//                System.out.println("serverport:" + serverport);
//                System.out.println("Entity logged in argos:"+entity);
//                logged = true;
//
//                FacesContext.getCurrentInstance().getExternalContext().redirect("http://" + servername + ":" + serverport + "/argos/login.jsp");
//            } catch (IOException ex) {
//                Logger.getLogger(ProyectBean.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }

    public void LoadProjectTable() {
        Connection con = null;
        loginControl();
        try {
            ConnectionFactory conectionfactory = new ConnectionFactory();
            con = conectionfactory.createConnection("geograficos");
            proyectos = proyectoDM.findProjects(con);

        } catch (SQLException ex) {
            Logger.getLogger(ProyectBean.class.getName()).log(Level.SEVERE, null, ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProyectBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void setearObjeto() {

        nombrecorto = proyectoSeleccionado.getNombreAlterno();
        ejecutor = proyectoSeleccionado.getEjecutor();
        nombreCompleto = proyectoSeleccionado.getTitulo();
        fechaInicio = proyectoSeleccionado.getFechaInicioDate();
        fechaFinalizcion = proyectoSeleccionado.getFechaFinalizacionDate();
        nombreArchivoCSS = proyectoSeleccionado.getCssaplicado();

    }

    private void projectComponents() {

        loginControl();
        locationsThematics.clear();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("argosPU");
        EntityManager em = emf.createEntityManager();

        TypedQuery<BproyectoXComp> query = em.createNamedQuery("BproyectoXComp.findByIdProyecto", BproyectoXComp.class);

        query.setParameter("idProyecto", selectedProject);
        List<BproyectoXComp> results = query.getResultList();
        Iterator<BproyectoXComp> it = results.iterator();

        while (it.hasNext()) {
            BproyectoXComp pc = it.next();
            locationsThematics.add(pc.getBubicacionTematica());
        }
        em.close();
        emf.close();

    }

    private void systemMessage(String rt, String idMessage, String infoMessage, String errorMessage) {
        if (rt.equals("0")) {
            FacesContext.getCurrentInstance().addMessage(idMessage, new FacesMessage(FacesMessage.SEVERITY_INFO, infoMessage, ""));
        } else {
            System.out.println(rt);
            FacesContext.getCurrentInstance().addMessage("growproyectos", new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, rt));

        }
    }

    public void actualizarProyecto() {

        Connection con = null;
        loginControl();
        try {

            ConnectionFactory conectionfactory = new ConnectionFactory();
            ProyectosDM proyectoDM = new ProyectosDM();
            con = conectionfactory.createConnection("geograficos");

            String resultadoTransaccion = proyectoDM.editaProyectoConArchivoImagenOCss(con, proyectoSeleccionado.getCodigo(), proyectoSeleccionado.getTitulo(), proyectoSeleccionado.getNombreAlterno(), new java.sql.Date(proyectoSeleccionado.getFechaInicioDate().getTime()), new java.sql.Date(proyectoSeleccionado.getFechaFinalizacionDate().getTime()), proyectoSeleccionado.getEjecutor(), 0, proyectoSeleccionado.getLogo(), (int) tamanioArchivoLogoProyecto, proyectoSeleccionado.getCssaplicado());
            systemMessage(resultadoTransaccion, "growproyectos", "Se ha modificado con exito los datos del proyecto", "No se ha modificado con exito los datos del proyecto");

        } catch (SQLException ex) {
            Logger.getLogger(ProyectBean.class.getName()).log(Level.SEVERE, null, ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProyectBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void handleSettingProject() {
        tabBasicDataProjectEnabled = true;
        activeIndexWinProject = 1;
        selectedProject = proyectoSeleccionado.getCodigo();
        projectComponents();
    }

    public void handleAddProject() {

        nombrecorto = "";
        nombreCompleto = "";
        nombreArchivoCSS = "";
        ejecutor = "";
        fechaInicio = new Date();
        fechaFinalizcion = new Date();

        tabBasicDataProjectEnabled = false;
        activeIndexWinProject = 0;
    }

    public void handleNewProject() {
        Connection con = null;
        String resultadoTransaction = "-1";
        loginControl();
        try {
            ConnectionFactory conectionfactory = new ConnectionFactory();
            con = conectionfactory.createConnection("geograficos");
            resultadoTransaction = proyectoDM.agregaProyectoConArchivoImagenOCss(con, nombreCompleto, nombrecorto, new java.sql.Date(fechaInicio.getTime()), new java.sql.Date(fechaFinalizcion.getTime()), ejecutor, 0, logoProyecto, (int) tamanioArchivoLogoProyecto, nombreArchivoCSS);
            System.out.println("Debug:" + resultadoTransaction);
            systemMessage(resultadoTransaction, "meditaproyectos", "Se ha creado con �xito un nuevo proyecto", "No se ha creado con �xito un nuevo proyecto");

        } catch (SQLException ex) {
            Logger.getLogger(ProyectBean.class.getName()).log(Level.SEVERE, null, "Mensaje de error:" + resultadoTransaction + " " + ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProyectBean.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            }
        }

    }

    public void onEdit(RowEditEvent event) {
        actualizarProyecto();

    }

    public void manejadorCargaLogo(FileUploadEvent event) throws IOException {

        if (event.getComponent().getId().equalsIgnoreCase("newlogofileupload")) {
            tamanioArchivoLogoProyecto = event.getFile().getSize();
            logoProyecto = event.getFile().getInputstream();
        } else {
            tamanioArchivoLogoProyecto = event.getFile().getSize();
            proyectoSeleccionado.setLogo(event.getFile().getInputstream());
            actualizarProyecto();
        }
    }

    public List<Proyecto> getProyectos() {
        return proyectos;
    }

    public void setProyectos(List<Proyecto> proyectos) {
        this.proyectos = proyectos;
    }

    public Proyecto getProyectoSeleccionado() {
        return proyectoSeleccionado;
    }

    public void setProyectoSeleccionado(Proyecto proyectoSeleccionado) {
        this.proyectoSeleccionado = proyectoSeleccionado;
    }

    public List<Proyecto> getFilterproject() {
        return filterprojects;
    }

    public void setFilterproject(List<Proyecto> filterprojects) {
        this.filterprojects = filterprojects;
    }

    public List<Proyecto> getFilterprojects() {
        return filterprojects;
    }

    public void setFilterprojects(List<Proyecto> filterprojects) {
        this.filterprojects = filterprojects;
    }

    public String getNombrecorto() {
        return nombrecorto;
    }

    public void setNombrecorto(String nombrecorto) {
        this.nombrecorto = nombrecorto;
    }

    public String getTitulo() {
        return nombreCompleto;
    }

    public void setTitulo(String titulo) {
        this.nombreCompleto = titulo;
    }

    public String getEjecutor() {
        return ejecutor;
    }

    public void setEjecutor(String ejecutor) {
        this.ejecutor = ejecutor;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinalizcion() {
        return fechaFinalizcion;
    }

    public void setFechaFinalizcion(Date fechaFinalizcion) {
        this.fechaFinalizcion = fechaFinalizcion;
    }

    public String getNombreArchivoCSS() {
        return nombreArchivoCSS;
    }

    public void setNombreArchivoCSS(String nombreArchivoCSS) {
        this.nombreArchivoCSS = nombreArchivoCSS;
    }

    public long getTamanioArchivoLogoProyecto() {
        return tamanioArchivoLogoProyecto;
    }

    public void setTamanioArchivoLogoProyecto(long tamanioArchivoLogoProyecto) {
        this.tamanioArchivoLogoProyecto = tamanioArchivoLogoProyecto;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public boolean isTabBasicDataProjectEnabled() {
        return tabBasicDataProjectEnabled;
    }

    public void setTabBasicDataProjectEnabled(boolean tabBasicDataProjectEnabled) {
        this.tabBasicDataProjectEnabled = tabBasicDataProjectEnabled;
    }

    public int getActiveIndexWinProject() {
        return activeIndexWinProject;
    }

    public void setActiveIndexWinProject(int activeIndexWinProject) {
        this.activeIndexWinProject = activeIndexWinProject;
    }

    public ProyectosDM getProyectoDM() {
        return proyectoDM;
    }

    public void setProyectoDM(ProyectosDM proyectoDM) {
        this.proyectoDM = proyectoDM;
    }

    public InputStream getLogoProyecto() {
        return logoProyecto;
    }

    public void setLogoProyecto(InputStream logoProyecto) {
        this.logoProyecto = logoProyecto;
    }

    public List<BubicacionTematica> getLocationsThematics() {
        return locationsThematics;
    }

    public void setLocationsThematics(List<BubicacionTematica> locationsThematics) {
        this.locationsThematics = locationsThematics;
    }
}
