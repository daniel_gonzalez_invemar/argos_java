/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.admin.UI.Beans;

import co.org.invemar.siam.sibm.vo.Proyecto;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author usrsig15
 */
public class ProjectDataModel extends ListDataModel<Proyecto> implements SelectableDataModel<Proyecto>, Serializable { 

    public ProjectDataModel() {  
    }  
  
    public ProjectDataModel(List<Proyecto> proyecto) {  
        super(proyecto);  
    }  
      
    @Override  
    public Proyecto getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<Proyecto> proyectos = (List<Proyecto>) getWrappedData();  
          
        for(Proyecto proyecto : proyectos) {  
            String codigo = String.valueOf(proyecto.getCodigo());
            if(codigo.equals(rowKey))  
                return proyecto;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Proyecto proyecto) {  
        return proyecto.getCodigo();  
    } 
    
}
