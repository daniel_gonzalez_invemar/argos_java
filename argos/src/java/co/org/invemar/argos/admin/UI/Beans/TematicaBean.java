/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.admin.UI.Beans;


import co.org.invemar.argos.monitoreum.persistencia.BubicacionTematica;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author usrsig15
 */
@ManagedBean(name = "thematic")
@SessionScoped
public class TematicaBean implements Serializable{
   private Map<String,String> thematics = new HashMap<String, String>();  
   private BubicacionTematica thematicLocation ;
   
   public TematicaBean(){
       fillThematicSelect();
   }
   public void fillThematicSelect(){
       
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("argosPU");
        EntityManager entityManager = emf.createEntityManager();
       
        CriteriaBuilder cb = emf.getCriteriaBuilder();
        CriteriaQuery<BubicacionTematica> q = cb.createQuery(BubicacionTematica.class);        
        Root<BubicacionTematica> c = q.from(BubicacionTematica.class);       
        List<BubicacionTematica> result = entityManager.createQuery(q).getResultList();
        Iterator<BubicacionTematica> it = result.iterator();
        
        while(it.hasNext()){
            BubicacionTematica theme = it.next();          
            thematics.put(String.valueOf(theme.getCodigo())+"- "+theme.getDescripcion(),String.valueOf(theme.getCodigo()));
            
        }
        entityManager.close();
        emf.close();
        
       
         
   }

    public Map<String,String> getThematics() {
        return thematics;
    }

    public void setThematics(Map<String,String> thematics) {
        this.thematics = thematics;
    }

    public BubicacionTematica getThematicLocation() {
        return thematicLocation;
    }

    public void setThematicLocation(BubicacionTematica thematicLocation) {
        this.thematicLocation = thematicLocation;
    }
}
