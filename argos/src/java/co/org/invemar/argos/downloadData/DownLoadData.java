/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.downloadData;

import co.org.invemar.argos.configuracion.Field;
import co.org.invemar.argos.configuracion.ReaderDownLoaderSettingFile;
import co.org.invemar.argos.manglares.model.CaseStudioModel;
import co.org.invemar.library.siam.exportadores.Data;
import co.org.invemar.library.siam.exportadores.ExportCSV;
import co.org.invemar.library.siam.exportadores.ExportXLS;
import co.org.invemar.library.siam.exportadores.ExportadorExcel;
import co.org.invemar.library.siam.exportadores.Rows;
import co.org.invemar.util.CconexionPrueba;
import co.org.invemar.util.ConnectionFactory;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.logging.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.logging.Level;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class DownLoadData {

    private ReaderDownLoaderSettingFile readerDownLoaderFile = null;
    private ExportCSV exportCSV = null;
    private ExportadorExcel exportadorExcel = null;

    public DownLoadData(ReaderDownLoaderSettingFile readerDownLoaderFile) {
        this.readerDownLoaderFile = readerDownLoaderFile;
    }

    public String getYearMonitoreumByThematic(String thematic, String upload, String companyUser, String idProject) {
        JSONObject object = new JSONObject();
        String data = null;

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "";

        if (upload != null && companyUser == null && idProject == null) {
            query = "SELECT distinct to_char(fecha,'YYYY') as years\n"
                    + "   FROM VM_DATOS_MONITOREO WHERE ID_TEMATICA=" + thematic + "  \n"
                    + "   and uploadflag=" + upload + " \n"
                    + "  order by to_char(fecha,'YYYY') desc";
        }

        if (companyUser != null && upload == null && idProject == null) {
            query = "SELECT distinct to_char(fecha,'YYYY') as years\n"
                    + "   FROM VM_DATOS_MONITOREO WHERE ID_TEMATICA=" + thematic + "  \n"
                    + "  and entidad_upload like '%" + companyUser + "%'\n"
                    + "  order by to_char(fecha,'YYYY') desc";
        }

        if (idProject != null && upload != null) {
            query = "SELECT distinct to_char(fecha,'YYYY') as years\n"
                    + "   FROM VM_DATOS_MONITOREO WHERE ID_TEMATICA=" + thematic + "  \n"
                    + "   and uploadflag=" + upload + " and  id_proyecto=" + idProject + "  \n"
                    + "  order by to_char(fecha,'YYYY') desc";
        }

        try {

            con = conectionfactory.createConnection("monitoreom");
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
             //System.out.println("sql:"+query);
            JSONArray jsonArray = new JSONArray();
            boolean isData = false;
            while (rs.next()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("year", rs.getString("years"));
                jsonArray.put(jsonObject);
                isData=true;

            }
            if (isData==false) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("year", "Sin datos");
                jsonArray.put(jsonObject);
            }
            return jsonArray.toString();
        } catch (SQLException ex) {
            Logger.getLogger(DownLoadData.class.getName()).log(Level.SEVERE, "Error message sql:", ex.getMessage());

        } catch (Exception ex) {
            Logger.getLogger(DownLoadData.class.getName()).log(Level.SEVERE, "Error message exception:", ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CaseStudioModel.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            }

        }

        return data;
    }

    public void setFillExcelFile(String fileName, String query, String idComponent) throws SQLException {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        //CconexionPrueba cp = new CconexionPrueba("monitoreom", "mon2007");
        try {

             con = conectionfactory.createConnection("monitoreom");
           // con = cp.getConn();
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();

            ArrayList data = new ArrayList();
            File file = new File(readerDownLoaderFile.getPathFile());
            
            ArrayList<Field> fieldList = readerDownLoaderFile.getField(idComponent);
         
            
            ArrayList<Rows> arrayRows = new ArrayList<Rows>();
            while (rs.next()) {
                Rows row = new Rows();
               
                if (file.exists()) {
                   
                    Iterator<Field> it = fieldList.iterator();
                   
                    while (it.hasNext()) {                       
                        Field f = it.next();
                        row.addData(new Data(rs.getString(f.getAttribute().get("name"))));
                        
                    }
                    arrayRows.add(row);
                } else {
                    row.addData(new Data(rs.getString("id_muestreo")));
                    row.addData(new Data(rs.getString("fecha")));
                    row.addData(new Data(rs.getString("temporada")));
                    row.addData(new Data(rs.getString("id_entidad")));
                    row.addData(new Data(rs.getString("estacion_o_lugar")));
                    row.addData(new Data(rs.getString("Consecutivo_Estacion_o_lugar")));
                    row.addData(new Data(rs.getString("latitud")));
                    row.addData(new Data(rs.getString("longitud")));
                    row.addData(new Data(rs.getString("profundidad")));
                    row.addData(new Data(rs.getString("profmax")));
                    row.addData(new Data(rs.getString("MUESTRAPROFMAX")));
                    row.addData(new Data(rs.getString("ECORREGION")));
                    row.addData(new Data(rs.getString("zona_protegida")));
                    row.addData(new Data(rs.getString("metodo_muestreo")));
                    row.addData(new Data(rs.getString("toponimia_loc")));
                    row.addData(new Data(rs.getString("tematica")));
                    row.addData(new Data(rs.getString("id_especie")));
                    row.addData(new Data(rs.getString("id_elemento")));
                    row.addData(new Data(rs.getString("cod_variable")));
                    row.addData(new Data(rs.getString("VARIABLE")));
                    row.addData(new Data(rs.getString("valor")));
                    row.addData(new Data(rs.getString("valor_num")));
                    row.addData(new Data(rs.getString("PRECISION")));
                    row.addData(new Data(rs.getString("unidad")));
                    row.addData(new Data(rs.getString("metodo_analitico")));
                    row.addData(new Data(rs.getString("nomproyecto")));
                    row.addData(new Data(rs.getString("id_cualidad")));
                    row.addData(new Data(rs.getString("ubicacion_especifica")));
                    row.addData(new Data(rs.getString("clase_sustrato")));
                    row.addData(new Data(rs.getString("nomclase_sustrato")));
                    row.addData(new Data(rs.getString("sustrato")));
                    row.addData(new Data(rs.getString("nomsustrato")));
                    row.addData(new Data(rs.getString("entidad_upload")));
                    row.addData(new Data(rs.getString("replica")));
                    arrayRows.add(row);
                }
            }

            StringBuilder columnNameExcel = new StringBuilder();
            if (file.exists()) {

                Iterator<Field> aliasIterator = fieldList.iterator();

                while (aliasIterator.hasNext()) {
                    Field f = aliasIterator.next();
                    columnNameExcel.append(f.getAttribute().get("alias") + ",");
                }

            } else {
                columnNameExcel.append("id Muestreo,");
                columnNameExcel.append("fecha,");
                columnNameExcel.append("temporada,");
                columnNameExcel.append("id_entidad,");
                columnNameExcel.append("estacion_o_lugar,");
                columnNameExcel.append("consecutivo_Estacion_o_lugar,");
                columnNameExcel.append("latitud,");
                columnNameExcel.append("longitud,");
                columnNameExcel.append("profundidad,");
                columnNameExcel.append("Profundidad Maxima,");
                columnNameExcel.append("Profundidad max muestra,");
                columnNameExcel.append("ECORREGION,");
                columnNameExcel.append("zona_protegida,");
                columnNameExcel.append("metodo_muestreo,");
                columnNameExcel.append("toponimia_loc,");
                columnNameExcel.append("tematica,");
                columnNameExcel.append("id_especie,");
                columnNameExcel.append("id_elemento,");
                columnNameExcel.append("cod_variable,");
                columnNameExcel.append("VARIABLE,");
                columnNameExcel.append("valor,");
                columnNameExcel.append("valor_num,");
                columnNameExcel.append("PRECISION,");
                columnNameExcel.append("unidad,");
                columnNameExcel.append("metodo_analitico,");
                columnNameExcel.append("nomproyecto,");
                columnNameExcel.append("id_cualidad,");
                columnNameExcel.append("ubicacion_especifica,");
                columnNameExcel.append("clase_sustrato,");
                columnNameExcel.append("nomclase_sustrato,");
                columnNameExcel.append("sustrato,");
                columnNameExcel.append("Nombre Sustrato,");
                columnNameExcel.append("entidad_upload,");
                columnNameExcel.append("replica");

            }
           
            ExportadorExcel ee = new ExportadorExcel("Data", columnNameExcel.toString(), arrayRows, fileName);     
            boolean r = ee.writeExcelFile();
            Logger.getLogger(DownLoadData.class.getName()).log(Level.INFO,"Excel "+fileName+" created:"+r, new String(""));
            

        } catch (SQLException ex) {
            Logger.getLogger(DownLoadData.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (Exception ex) {
            Logger.getLogger(DownLoadData.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CaseStudioModel.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            }

        }
    }

    public void setFillCSVFile(String filename, String query, String idComponent) throws SQLException {

        Connection con = null;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        //CconexionPrueba cp = new CconexionPrueba("monitoreom", "mon2007");

        try {

            con = conectionfactory.createConnection("monitoreom");
            //con = cp.getConn();
           
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();

            ArrayList<Rows> rowsData = new ArrayList<Rows>();
            File file = new File(readerDownLoaderFile.getPathFile());

            while (rs.next()) {

                Rows r = new Rows();

                if (file.exists()) {
                    r.addData(new Data(rs.getString("rownum")));

                    ArrayList<Field> fieldList = readerDownLoaderFile.getField(idComponent);
                    Iterator<Field> it = fieldList.iterator();

                    while (it.hasNext()) {
                        Field f = it.next();
                        r.addData(new Data(rs.getString(f.getAttribute().get("name"))));
                    }
                    rowsData.add(r);

                } else {

                    r.addData(new Data(rs.getString("rownum")));
                    r.addData(new Data(rs.getString("id_muestreo")));
                    r.addData(new Data(rs.getString("fecha")));
                    r.addData(new Data(rs.getString("temporada")));
                    r.addData(new Data(rs.getString("id_entidad")));
                    r.addData(new Data(rs.getString("estacion_o_lugar")));
                    r.addData(new Data(rs.getString("Consecutivo_Estacion_o_lugar")));
                    r.addData(new Data(rs.getString("latitud")));
                    r.addData(new Data(rs.getString("longitud")));
                    r.addData(new Data(rs.getString("profundidad")));
                    r.addData(new Data(rs.getString("profmax")));
                    r.addData(new Data(rs.getString("MUESTRAPROFMAX")));
                    r.addData(new Data(rs.getString("ECORREGION")));
                    r.addData(new Data(rs.getString("zona_protegida")));
                    r.addData(new Data(rs.getString("metodo_muestreo")));
                    r.addData(new Data(rs.getString("toponimia_loc")));
                    r.addData(new Data(rs.getString("tematica")));
                    r.addData(new Data(rs.getString("id_especie")));
                    r.addData(new Data(rs.getString("id_elemento")));
                    r.addData(new Data(rs.getString("cod_variable")));
                    r.addData(new Data(rs.getString("VARIABLE")));
                    r.addData(new Data(rs.getString("valor")));
                    r.addData(new Data(rs.getString("valor_num")));
                    r.addData(new Data(rs.getString("PRECISION")));
                    r.addData(new Data(rs.getString("unidad")));
                    r.addData(new Data(rs.getString("metodo_analitico")));
                    r.addData(new Data(rs.getString("nomproyecto")));
                    r.addData(new Data(rs.getString("id_cualidad")));
                    r.addData(new Data(rs.getString("ubicacion_especifica")));
                    r.addData(new Data(rs.getString("clase_sustrato")));
                    r.addData(new Data(rs.getString("nomclase_sustrato")));

                    r.addData(new Data(rs.getString("sustrato")));
                    r.addData(new Data(rs.getString("nomsustrato")));
                    r.addData(new Data(rs.getString("entidad_upload")));
                    r.addData(new Data(rs.getString("replica")));

                    rowsData.add(r);
                }
            }

            String columnNameCSV = "";
            if (file.exists()) {
                ArrayList<Field> fieldList = readerDownLoaderFile.getField(idComponent);
                Iterator<Field> it = fieldList.iterator();
                columnNameCSV = "#;";
                while (it.hasNext()) {
                    Field f = it.next();
                    columnNameCSV = columnNameCSV + f.getAttribute().get("alias") + ";";
                }

            } else {
                columnNameCSV = "#;id Muestreo;"
                        + "fecha;temporada;"
                        + "id_entidad;"
                        + "estacion_o_lugar;"
                        + "consecutivo_Estacion_o_lugar;"
                        + "latitud;"
                        + "longitud;"
                        + "profundidad;"
                        + "Profundidad Maxima;"
                        + "Profundidad max muestra;"
                        + "ECORREGION;"
                        + "zona_protegida;"
                        + "metodo_muestreo;"
                        + "toponimia_loc;"
                        + "tematica;id_especie;"
                        + "id_elemento;"
                        + "cod_variable;"
                        + "VARIABLE;"
                        + "valor;"
                        + "valor_num;"
                        + "PRECISION;"
                        + "unidad;"
                        + "metodo_analitico;"
                        + "nomproyecto;"
                        + "id_cualidad;"
                        + "ubicacion_especifica;"
                        + "clase_sustrato;"
                        + "nomclase_sustrato;"
                        + "sustrato;"
                        + "Nombre Sustrato;"
                        + "entidad_upload;"
                        + "replica";
            }

            ExportCSV exportCSV = new ExportCSV(columnNameCSV, ";", "\n");
            exportCSV.writeCsvFile(filename, rowsData);

        } catch (SQLException ex) {
            Logger.getLogger(DownLoadData.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } catch (Exception ex) {
            Logger.getLogger(DownLoadData.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CaseStudioModel.class.getName()).log(Level.SEVERE, null, ex.getMessage());
            }

        }

    }

    public String builderQuery(String idComponent, String idEntity, String idUser, String year, String idproject) {

        String query = "SELECT rownum,to_char(ID_MUESTREO) as ID_MUESTREO,\n"
                + "  FECHA,\n"
                + "  NVL(decode(temporada,'L','Lluviosa','Seca'),'-')    AS temporada,\n"
                + "  NVL(id_entidad,'-')   AS id_entidad,\n"
                + "  nvl(pais,'-')         AS pais,\n"
                + "  NVL(id_estacion,'-')  AS id_estacion,\n"
                + "  NVL(ESTACION,'-')     AS estacion_o_lugar,\n"
                + "  NVL(CDG_ESTACION,'-') AS Consecutivo_Estacion_o_lugar,\n"
                + "  LAT              AS latitud,\n"
                + "  LON          AS longitud ,\n"
                + "  nvl(to_char(profundidad),' ') as profundidad,\n"
                + "  nvl(to_char(MUESTRAPROFMIN),'-') as profmax,\n"
                + "  nvl(to_char(MUESTRAPROFMAX),'-') as MUESTRAPROFMAX,\n"
                + "  nvl(to_char(DESC_ESTACION),'-') as DESC_ESTACION,\n"
                + "  nvl(to_char(ECORREGION_CDG),'-') as ECORREGION, nvl(zona_protegida,'-') as zona_protegida,\n"
                + "  nvl(metodo_muestreo,'-') as metodo_muestreo,\n"
                + "  nvl(to_char(toponimia_loc),'-') as toponimia_loc, \n"
                + "  tematica,\n"
                + "  nvl(to_char(id_especie),'-') as id_especie,\n"
                + "  nvl(to_char(id_elemento),'-') as id_elemento,\n"
                + "  nvl(to_char(cod_variable),'-') as cod_variable,\n"
                + "  nvl(to_char(VARIABLE),'-') as VARIABLE,  \n"
                + "  valor,\n"
                + "  valor_num,  \n"
                + "  nvl(to_char(PRECISION),'-') as PRECISION,\n"
                + "  nvl(to_char(unidad),'-') as unidad,\n"
                + "  nvl(to_char(metodo_analitico),'-') as metodo_analitico,\n"
                + "  nomproyecto,  \n"
                + "  nvl(to_char(id_cualidad),'-') as id_cualidad,\n"
                + "  ubicacion_especifica,\n"
                + "  clase_sustrato,\n"
                + "  nomclase_sustrato,\n"
                + "  sustrato,\n"
                + "  nomsustrato,\n"
                + "  entidad_upload,\n"
                + "  replica FROM VM_DATOS_MONITOREO WHERE ID_TEMATICA=" + idComponent +" and to_char(fecha,'YYYY')='" + year + "'";

        if (idUser != null && idEntity == null && idproject == null) {
            query = query + " and uploadflag=" + idUser+" " ;
        } else if (idEntity != null && idUser == null && idproject == null) {
            query = query + " and ENTIDAD_UPLOAD like '%" + idEntity + "%'  ";
        } else if (idproject != null && idUser != null) {
            query = query + " and uploadflag=" + idUser + " and id_proyecto=" + idproject+" ";
        }

        return query;
    }

}
