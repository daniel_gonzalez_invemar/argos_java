/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.geograficos.model;

import co.org.invemar.util.ConnectionFactory;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.HashMap;

/**
 *
 * @author usrsig15
 */
public class ZonasProtegidas implements Serializable {
    
    private String descripcion=null;   
    private String codigo = null;
            
    public HashMap<String, String> getAllZonasProtegidas() {
        Connection con= null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;      
        String query = "SELECT CODIGO, DESCRIPCION, MUNICIPIO FROM CMLST_ZONAS_PROTEGIDAS   order by  municipio asc";
       // System.out.println("query:"+query);
        ConnectionFactory conectionfactory = new ConnectionFactory();
        HashMap<String, String> zonasProtegidasHasMap= new HashMap<String, String>();
        try {
            con = conectionfactory.createConnection("geograficos");                  
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            
            while (rs.next()) {                               
                zonasProtegidasHasMap.put(rs.getString("descripcion"),rs.getString("codigo"));               
            }        
           
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el objeto Zonas Protegidas  m�todo getZonasProtegidas:" + ex.getMessage());

        } finally {
            
            conectionfactory.closeConnectionInstancia(con);
        }
       
        return zonasProtegidasHasMap;        
    }
     public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
