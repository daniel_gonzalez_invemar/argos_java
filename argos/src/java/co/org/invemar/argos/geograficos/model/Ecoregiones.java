/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.geograficos.model;


import java.io.Serializable;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import co.org.invemar.util.ConnectionFactory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author usrsig15
 */

public class Ecoregiones {
    
    public HashMap<String, String> EcoregionesPorRegion(String region) {
        Connection con= null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;      
        String query = "select codigo,descripcion from codigos_lov where SUBSTR(codigo,1,2) ="+region;
        ConnectionFactory conectionfactory = new ConnectionFactory();
        HashMap<String, String> ecorregionesHasMap= new HashMap<String, String>();
        try {
            con = conectionfactory.createConnection("geograficos");                  
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            //System.out.println("Ecorregiones:"+query);     
            
            while (rs.next()) {                               
                ecorregionesHasMap.put(rs.getString("descripcion"),rs.getString("codigo"));               
            }        
           
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el objeto Ecorregiones  m�todo EcoregionesPorRegion:" + ex.getMessage());

        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return ecorregionesHasMap;        
    }

}
