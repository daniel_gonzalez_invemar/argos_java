/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.geograficos.model;

import co.org.invemar.util.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author usrsig15
 */
public class Toponimia {

    private Map<String, String> coastDepartament=null;
    
    
    public HashMap<String, String> FillMapToponimosPorRegion(String categoriaToponimo, String codRegion) {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT TOPONIMO_TP,\n"
                + "  TOPONIMO_TEXTO\n"
                + "FROM CLST_TOPONIMIAT\n"
                + "WHERE categoria_tp    ='" + categoriaToponimo + "'\n"
                + "AND SUBSTR(region,1,2)='" + codRegion + "'\n"
                + "ORDER BY TOPONIMO_TEXTO";
        ConnectionFactory conectionfactory = new ConnectionFactory();
        HashMap<String, String> toponimosMap = new HashMap<String, String>();
        try {
            con = conectionfactory.createConnection("geograficos");
            pstmt = con.prepareStatement(query);

            rs = pstmt.executeQuery();

            while (rs.next()) {

                toponimosMap.put(rs.getString("TOPONIMO_TEXTO"), rs.getString("TOPONIMO_TP"));

            }

            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo FillMapToponimos del objeto Toponimia:" + ex.getMessage());

        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(Toponimia.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return toponimosMap;
    }

    public Map<String, String> FillMapTodosToponimos(String categoriaToponimo) {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT TOPONIMO_TP, TOPONIMO_TEXTO FROM CLST_TOPONIMIAT where categoria_tp=?  and region<30000 order by toponimo_texto asc";
        ConnectionFactory conectionfactory = new ConnectionFactory();
        Map<String, String> toponimosMap = new HashMap<String, String>();
        try {
            con = conectionfactory.createConnection("geograficos");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, categoriaToponimo);
            // System.out.println("sql toponimia:"+query);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                toponimosMap.put(rs.getString("TOPONIMO_TEXTO"), rs.getString("TOPONIMO_TP"));
            }

            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo FillMapToponimos del objeto Toponimia:" + ex.getMessage());

        } finally {
            try {
                con.close();
                con=null;
            } catch (SQLException ex) {
                Logger.getLogger(Toponimia.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
            
        }
        return toponimosMap;
    }

    public String getAllCoastDepartament() {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        JSONArray jsonArray  = new JSONArray();
        String query = "SELECT * "
                + "                FROM CLST_TOPONIMIAT"
                + "                WHERE categoria_tp    ='D' "
                + "               and  (SUBSTR(region,1,2)='20' or SUBSTR(region,1,2)='13' or  SUBSTR(region,1,2)='10' ) "
                + "                ORDER BY TOPONIMO_TEXTO";
        ConnectionFactory conectionfactory = new ConnectionFactory();       
        try {
            con = conectionfactory.createConnection("geograficos");
            pstmt = con.prepareStatement(query);           
            rs = pstmt.executeQuery();
            coastDepartament = new LinkedHashMap<String, String>();
            while (rs.next()) {
                JSONObject jo = new JSONObject();
                jo.put("id", rs.getString("TOPONIMO_TP"));
                jo.put("name",rs.getString("TOPONIMO_TEXTO"));              
                
                coastDepartament.put(rs.getString("TOPONIMO_TEXTO"), rs.getString("TOPONIMO_TP"));
                
                jsonArray.put(jo);     
            }
            

            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo FillMapToponimos del objeto Toponimia:" + ex.getMessage());

        } finally {
            try {
                con.close();
                con = null;
            } catch (SQLException ex) {
                Logger.getLogger(Toponimia.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
        return jsonArray.toString();
    }

    public Map<String, String> getCoastDepartament() {
        return coastDepartament;
    }

    public void setCoastDepartament(Map<String, String> coastDepartament) {
        this.coastDepartament = coastDepartament;
    }

}
