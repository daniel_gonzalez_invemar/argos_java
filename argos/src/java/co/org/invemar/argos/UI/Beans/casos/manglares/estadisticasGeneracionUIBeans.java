/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.manglares.model.EspeciesModel;
import co.org.invemar.argos.manglares.model.Graficador;
import co.org.invemar.argos.manglares.model.SectorManglaresModel;
import co.org.invemar.argos.manglares.model.SitioManglarModel;
import co.org.invemar.argos.manglares.model.UnidadManejoModel;
import co.org.invemar.argos.manglares.model.ParcelaModel;
import co.org.invemar.argos.manglares.vo.TablaGrafica;
import co.org.invemar.library.siam.vo.Responsable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author usrsig15
 */
@ManagedBean(name = "estadisticasGeneracionUIBeans")
@SessionScoped
public class estadisticasGeneracionUIBeans {

    private String informacionZonasManejo = "<br> SZ: Sin Zonificar.<br> ZUS: Zona de uso sostenible. <br> ZR: Zona de recuperación. <br> ZP: Zona proteginda. ";
    
    private CartesianChartModel categoryModel;
    private String stackedBar="false";
    
    private String ayuda;
    private String parametroBusqueda;
    private String sector;

    private Map<String, String> sectores = new HashMap<String, String>();
    private SectorManglaresModel modeloSector = new SectorManglaresModel();

    private String cmbSector = "true";

    private String sitio;
    private Map<String, String> sitios = new HashMap<String, String>();
    private String cmbSitio = "true";

    private SitioManglarModel modeloSitio = new SitioManglarModel();

    private String unidadManejo;
    private Map<String, String> unidadesManejo = new HashMap<String, String>();
    private String cmbUnidadManejo = "true";

    private UnidadManejoModel unidadManejoModel = new UnidadManejoModel();

    private String parcela;
    private Map<String, String> parcelas = new HashMap<String, String>();
    private String cmbParcela = "true";

    private ParcelaModel parcelaModel = new ParcelaModel();
    private String variable;
    private Map<String, String> variables = new HashMap<String, String>();
    private String especie;
    private Map<String, String> especies = new HashMap<String, String>();
    private EspeciesModel especieModel = new EspeciesModel();
    private String nombreButonGenerarEstadistica = "Generar estadística";
    private String mensajes;
    private Map<String, String> unidadesMedida = new HashMap<String, String>();

    private String tipoUnidadManejo;
    private Map<String, String> tiposUnidadManejo = new HashMap<String, String>();
    private String cmbTipoUnidadManejo = "true";

    private String unidadMedida;
    private String tituloGrafica;

    private List<TablaGrafica> tablavalores;
    private List<Responsable> responsables = new ArrayList<Responsable>();

    public estadisticasGeneracionUIBeans() {

        cargaSectores();
        cargaEspecies();
        CargaVariables();
        categoryModel = new CartesianChartModel();
        ChartSeries series = new ChartSeries();
        series.setLabel(" ");
        series.set("0", 0);
        categoryModel.addSeries(series);

    }
    
       public void reiniciarBusqueda() {
        parametroBusqueda = "0";
        parametroBusqueda = "Seleccione";

        cmbSector = "true";
        cmbSitio = "true";
        cmbParcela = "true";
        cmbUnidadManejo = "true";

        ayuda = "";

    }

    private void cargaSectores() {
        try {
            int searchParameter = Integer.parseInt(parametroBusqueda);
            sectores = modeloSector.getTodosLosSectoresSectoresConEstadistica("4", searchParameter,"");
        } catch (NumberFormatException e) {
            Logger.getLogger(estadisticasCrecimientoUIBeans.class.getName()).log(Level.SEVERE, null, e.getMessage());
        } 
    }

    private void cargaEspecies() {
        especies = especieModel.getTodosLasEspeciesConEstadistica("4");
    }

    public void handleSitios() {

        try {
            if (unidadManejo != null) {
                int codUnidadManejo = Integer.parseInt(unidadManejo);
                int searchParameter = Integer.parseInt(parametroBusqueda);
                sitios = modeloSitio.getTodosLosSitiosPorSectoresConEstadistica(codUnidadManejo, "4", searchParameter);
            }
        } catch (NumberFormatException e) {
            Logger.getLogger(estadisticasCrecimientoUIBeans.class.getName()).log(Level.SEVERE, null, e.getMessage());
        }
        unidadManejo = "Seleccione";
        parcela = "Seleccione";
    }
    
      public void postProcessXLS(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow header = sheet.getRow(0);

        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        cellStyle.setWrapText(true);

        for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
            HSSFCell cell = header.getCell(i);
            cell.setCellStyle(cellStyle);
        }

        int lastrow = sheet.getLastRowNum();
        HSSFRow newRow = sheet.createRow(lastrow + 2);

        newRow.createCell(0).setCellValue("Parametros seleccionados:");
        
       if (parametroBusqueda.equals("3")) {
            newRow.createCell(1).setCellValue("Sector:" + sector);
            newRow.createCell(2).setCellValue("Sitio:" + sitio);

            newRow.createCell(4).setCellValue("Variable:" + tituloGrafica);

        } else if (parametroBusqueda.equals("2")) {
            newRow.createCell(1).setCellValue("Sector:" + sector);
            newRow.createCell(2).setCellValue("Sitio:" + sitio);
            newRow.createCell(3).setCellValue("Unidad de manejo:" + unidadManejo);

            newRow.createCell(4).setCellValue("Variable:" + tituloGrafica);
        } else if (parametroBusqueda.equals("1")) {
            newRow.createCell(1).setCellValue("Sector:" + sector);
            newRow.createCell(2).setCellValue("Sitio:" + sitio);
            newRow.createCell(3).setCellValue("Unidad de manejo:" + unidadManejo);
            newRow.createCell(3).setCellValue("Parcela:" + parcela);
            
        }

        newRow.createCell(4).setCellValue("Variable:" + tituloGrafica+" .Unidad de medida:( "+unidadMedida+" ) ");
         
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
       
    }

    public void handletiposUnidadManejo() {
        if (sitio != null) {
            sitios = modeloSitio.getTodosLosTiposUnidadManejoUnidadesManejo(sitio);
        }

    }

    public void handleUnidadesManejo() {
        if (sector != null) {
            int codSector =Integer.parseInt(sector);
            unidadesManejo = unidadManejoModel.getTodosLasUnidadesManejoConEstadistica(codSector, parametroBusqueda, "4");

        }
    }

    public void handleParcelas() {
        if (sitio != null) {
            int codSitio = Integer.parseInt(sitio); 
            
            parcelas = parcelaModel.getTodosLasParcelasConEstadistica(codSitio,"4");
        }
    }

    public void handleGeneraEstadistica(ActionEvent actionEvent) {
        Graficador graficador = new Graficador();
               
               variables.put("Abundancia absoluta total", "abt");
                variables.put("Abundancia absoluta muertos", "abm");
                variables.put("Abundancia absoluta vivos", "abv");
                variables.put("Numero de propagulos", "np");
                variables.put("Abundancia relativa de vivos", "abrv");
                variables.put("Abundancia relativa de muertos", "abrm");
                variables.put("Densidad absoluta  muertos", "dam");      
                variables.put("Densidad absoluta de vivos", "dav");               
                variables.put("Altura Promedio", "ap");
                variables.put("Densidad Plantulas", "dplantulas");
                variables.put("Densidad Propagulos", "dp");
        
         if (variable.equals("dam")) {
            unidadMedida = "Unidad";
            tituloGrafica = "Densidad absoluta  muertos";

        }else if (variable.equals("dav")) {
            unidadMedida = "Unidad";
            tituloGrafica = "Densidad absoluta de vivos";

        }else if (variable.equals("abrm")) {
            unidadMedida = "%";
            tituloGrafica = "Abundancia relativa de muertos";

        }else if (variable.equals("abrv")) {
            unidadMedida = "%";
            tituloGrafica = "Abundancia relativa de vivos";

        }else if (variable.equals("np")) {
            unidadMedida = "Numero";
            tituloGrafica = "Numero de propagulos";

        }else if  (variable.equals("abv")) {
            unidadMedida = "Numero";
            tituloGrafica = "Abundancia absoluta vivos";

        }else if (variable.equals("abm")) {
            unidadMedida = "Numero";
            tituloGrafica = "Abundancia absoluta muertos";

        } else if (variable.equals("abt")) {
            unidadMedida = "";
            tituloGrafica = "Abundancia absoluta total";

        } else if (variable.equals("dplantulas")) {
            unidadMedida = "";
            tituloGrafica = "Densidad Plantulas";

        } else if (variable.equals("ap")) {
            unidadMedida = "";
            tituloGrafica = "Altura Promedio";

        } else if (variable.equals("dp")) {
            unidadMedida = "";
            tituloGrafica = "Densidad Prop&aacute;gulos ";

        }
         
        if (parametroBusqueda.equals("4")) {
            tituloGrafica = tituloGrafica + " del sector " + sector;
        } else if (parametroBusqueda.equals("3")) {
            tituloGrafica = tituloGrafica + " de la estaci&oacute;n " + sitio;

        } else if (parametroBusqueda.equals("2")) {
            tituloGrafica = tituloGrafica + " de la unidad de manejo " + unidadManejo;

        } else if (parametroBusqueda.equals("1")) {
            tituloGrafica = tituloGrafica + " de la parcela ";

        }

        categoryModel = graficador.getValoresCalculadosPorVariable(parametroBusqueda, variable, sector, sitio, unidadManejo, tipoUnidadManejo, parcela, especie, "4");
        tablavalores = graficador.getTablavalores();
        responsables = graficador.getResponsables();
        if (!graficador.isResultadoObtenido()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "No hay dato para esta configuración", ""));
        }
    }

    public void CargoVariablesCalculo() {
        variables.clear();
       
        
        
        if (parametroBusqueda.equals("3")) {
            
            
            if (especie.equals("te")) {
                variables.put("Abundancia absoluta total", "abt");
                variables.put("Abundancia absoluta muertos", "abm");
                variables.put("Abundancia absoluta vivos", "abv");
                variables.put("Numero de propagulos", "np");
                variables.put("Abundancia relativa de vivos", "abrv");
                variables.put("Abundancia relativa de muertos", "abrm");
                variables.put("Densidad de muertos", "dm");              
                variables.put("Altura Promedio", "ap");                
                variables.put("Densidad Plantulas", "dplantulas");
                variables.put("Densidad Propagulos", "dp");

            } else {

                variables.put("Abundancia absoluta total", "abt");
                variables.put("Abundancia absoluta muertos", "abm");
                variables.put("Abundancia absoluta vivos", "abv");
                variables.put("Numero de propagulos", "np");
                variables.put("Abundancia relativa de vivos", "abrv");
                variables.put("Abundancia relativa de muertos", "abrm");
                variables.put("Densidad absoluta  muertos", "dam");      
                variables.put("Densidad absoluta de vivos", "dav");               
                variables.put("Altura Promedio", "ap");
                variables.put("Densidad Plantulas", "dplantulas");
                variables.put("Densidad Propagulos", "dp");
            }

        } else if (parametroBusqueda.equals("2")) {

            if (especie.equals("te")) {
                variables.put("Altura Promedio", "ap");
                variables.put("Densidad Plantulas", "dplantulas");
                variables.put("Densidad Propagulos", "dp");
            } else {
                variables.put("Abundancia absoluta total", "abt");
                variables.put("Abundancia absoluta muertos", "abm");
                variables.put("Abundancia absoluta vivos", "abv");
                variables.put("Numero de propagulos", "np");
                variables.put("Abundancia relativa de vivos", "abrv");
                variables.put("Abundancia relativa de muertos", "abrm");
                variables.put("Densidad absoluta  muertos", "dam");      
                variables.put("Densidad absoluta de vivos", "dav");               
                variables.put("Altura Promedio", "ap");
                variables.put("Densidad Plantulas", "dplantulas");
                variables.put("Densidad Propagulos", "dp");
            }
        } else if (parametroBusqueda.equals("1")) {

            if (especie.equals("te")) {
                variables.put("Altura Promedio", "ap");
                variables.put("Densidad Plantulas", "dplantulas");
                variables.put("Densidad Propagulos", "dp");
            } else {
                variables.put("Abundancia absoluta total", "abt");
                variables.put("Abundancia absoluta muertos", "abm");
                variables.put("Abundancia absoluta vivos", "abv");
                variables.put("Numero de propagulos", "np");
                variables.put("Abundancia relativa de vivos", "abrv");
                variables.put("Abundancia relativa de muertos", "abrm");
                variables.put("Densidad absoluta  muertos", "dam");      
                variables.put("Densidad absoluta de vivos", "dav");               
                variables.put("Altura Promedio", "ap");
                variables.put("Densidad Plantulas", "dplantulas");
                variables.put("Densidad Propagulos", "dp");
            }

        }

    }

    public void handleParametroBusqueda() {

        variables.clear();
        if (parametroBusqueda.equals("3")) {
            cmbSitio = "true";
            cmbUnidadManejo = "false";
            cmbSector = "false";
            cmbTipoUnidadManejo = "true";
            cmbParcela = "true";

            ayuda = "Debe seleccionar: <br/> 1. Sector <br /> 2. Unidad  de manejo  <br/> 3. La variable <br/>  5. La especie";

            variables.put("Altura Promedio", "ap"); //lo tengo por especie y por parcela.
            variables.put("Densidad Plantulas", "dp"); //ya la tengo por especie y por parcela.            
            variables.put("Densidad Propagulos", "dp"); // solo por especie.
            
            unidadManejo="Seleccione";
           
            

        } else if (parametroBusqueda.equals("2")) {
            ayuda = "Debe seleccionar: <br/> 1. Sector <br/>2. Sitio <br/> 3. La Unidad de Manejo <br/> 5. La variable <br/>  5. La especie ";
            cmbSitio = "false";
            cmbUnidadManejo = "false";
            cmbSector = "false";
            cmbTipoUnidadManejo = "true";
            cmbParcela = "true";

            variables.put("Altura Promedio", "ap"); //lo tengo por especie y por parcela.
            variables.put("Densidad Plantulas", "dp"); //ya la tengo por especie y por parcela.            
            variables.put("Densidad Propagulos", "dp"); // solo por especie.

        } else if (parametroBusqueda.equals("1")) {
            cmbSitio = "false";
            cmbUnidadManejo = "false";
            cmbSector = "false";
            cmbTipoUnidadManejo = "true";
            cmbParcela = "false";
            ayuda = "Debe seleccionar: <br/>1. Sector <br/>2.  Sitio <br/> 3. La Unidad de Manejo <br/> 4. La parcela <br/>  6. La variable <br/> 7. La especie ";

            variables.put("Altura Promedio", "ap"); //lo tengo por especie y por parcela.
            variables.put("Densidad Plantulas", "dp"); //ya la tengo por especie y por parcela.            
            variables.put("Densidad Propagulos", "dp"); // solo por especie.

        }
        cargaSectores();
        sector = "Seleccione";
        sitio="Seleccione";
        unidadManejo = "Seleccione";
        parcela = "Seleccione";

    }

    private void CargaVariables() {

       
        variables.put("Altura Promedio", "ap"); //lo tengo por especie y por parcela.
        variables.put("Densidad Plantulas", "dp"); //ya la tengo por especie y por parcela.            
        variables.put("Densidad Propagulos", "dp"); // solo por especie.

    }

    public String getAyuda() {
        return ayuda;
    }

    public void setAyuda(String ayuda) {
        this.ayuda = ayuda;
    }

    public String getParametroBusqueda() {
        return parametroBusqueda;
    }

    public void setParametroBusqueda(String parametroBusqueda) {
        this.parametroBusqueda = parametroBusqueda;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public SectorManglaresModel getModeloSector() {
        return modeloSector;
    }

    public void setModeloSector(SectorManglaresModel modeloSector) {
        this.modeloSector = modeloSector;
    }

    public Map<String, String> getSectores() {
        return sectores;
    }

    public void setSectores(Map<String, String> sectores) {
        this.sectores = sectores;
    }

    public UnidadManejoModel getUnidadManejoModel() {
        return unidadManejoModel;
    }

    public void setUnidadManejoModel(UnidadManejoModel unidadManejoModel) {
        this.unidadManejoModel = unidadManejoModel;
    }

    public String getUnidadManejo() {
        return unidadManejo;
    }

    public void setUnidadManejo(String unidadManejo) {
        this.unidadManejo = unidadManejo;
    }

    public Map<String, String> getUnidadesManejo() {
        return unidadesManejo;
    }

    public void setUnidadesManejo(Map<String, String> unidadesManejo) {
        this.unidadesManejo = unidadesManejo;
    }

    public String getSitio() {
        return sitio;
    }

    public void setSitio(String sitio) {
        this.sitio = sitio;
    }

    public Map<String, String> getSitios() {
        return sitios;
    }

    public void setSitios(Map<String, String> sitios) {
        this.sitios = sitios;
    }

    public SitioManglarModel getModeloSitio() {
        return modeloSitio;
    }

    public void setModeloSitio(SitioManglarModel modeloSitio) {
        this.modeloSitio = modeloSitio;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public Map<String, String> getEspecies() {
        return especies;
    }

    public void setEspecies(Map<String, String> especies) {
        this.especies = especies;
    }

    public EspeciesModel getEspecieModel() {
        return especieModel;
    }

    public void setEspecieModel(EspeciesModel especieModel) {
        this.especieModel = especieModel;
    }

    public String getNombreButonGenerarEstadistica() {
        return nombreButonGenerarEstadistica;
    }

    public void setNombreButonGenerarEstadistica(String nombreButonGenerarEstadistica) {
        this.nombreButonGenerarEstadistica = nombreButonGenerarEstadistica;
    }

    public CartesianChartModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CartesianChartModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public Map<String, String> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, String> variables) {
        this.variables = variables;
    }

    public String getParcela() {
        return parcela;
    }

    public void setParcela(String parcela) {
        this.parcela = parcela;
    }

    public Map<String, String> getParcelas() {
        return parcelas;
    }

    public void setParcelas(Map<String, String> parcelas) {
        this.parcelas = parcelas;
    }

    public ParcelaModel getParcelaModel() {
        return parcelaModel;
    }

    public void setParcelaModel(ParcelaModel parcelaModel) {
        this.parcelaModel = parcelaModel;
    }

    public String getMensajes() {
        return mensajes;
    }

    public void setMensajes(String mensajes) {
        this.mensajes = mensajes;
    }

    public Map<String, String> getUnidadesMedida() {
        return unidadesMedida;
    }

    public void setUnidadesMedida(Map<String, String> unidadesMedida) {
        this.unidadesMedida = unidadesMedida;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getTituloGrafica() {
        return tituloGrafica;
    }

    public void setTituloGrafica(String tituloGrafica) {
        this.tituloGrafica = tituloGrafica;
    }

    public String getTipoUnidadManejo() {
        return tipoUnidadManejo;
    }

    public void setTipoUnidadManejo(String tipoUnidadManejo) {
        this.tipoUnidadManejo = tipoUnidadManejo;
    }

    public Map<String, String> getTiposUnidadManejo() {
        return tiposUnidadManejo;
    }

    public void setTiposUnidadManejo(Map<String, String> tiposUnidadManejo) {
        this.tiposUnidadManejo = tiposUnidadManejo;
    }

    public String getCmbTipoUnidadManejo() {
        return cmbTipoUnidadManejo;
    }

    public void setCmbTipoUnidadManejo(String cmbTipoUnidadManejo) {
        this.cmbTipoUnidadManejo = cmbTipoUnidadManejo;
    }

    public String getCmbUnidadManejo() {
        return cmbUnidadManejo;
    }

    public void setCmbUnidadManejo(String cmbUnidadManejo) {
        this.cmbUnidadManejo = cmbUnidadManejo;
    }

    public String getCmbParcela() {
        return cmbParcela;
    }

    public void setCmbParcela(String cmbParcela) {
        this.cmbParcela = cmbParcela;
    }

    public String getCmbSector() {
        return cmbSector;
    }

    public void setCmbSector(String cmbSector) {
        this.cmbSector = cmbSector;
    }

    public String getCmbSitio() {
        return cmbSitio;
    }

    public void setCmbSitio(String cmbSitio) {
        this.cmbSitio = cmbSitio;
    }

    public List<TablaGrafica> getTablavalores() {
        return tablavalores;
    }

    public void setTablavalores(List<TablaGrafica> tablavalores) {
        this.tablavalores = tablavalores;
    }

    public List<Responsable> getResponsables() {
        return responsables;
    }

    public void setResponsables(List<Responsable> responsables) {
        this.responsables = responsables;
    }

    public String getStackedBar() {
        return stackedBar;
    }

    public void setStackedBar(String stackedBar) {
        this.stackedBar = stackedBar;
    }

}
