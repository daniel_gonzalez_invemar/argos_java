/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.manglares.model.ParcelaModel;
import co.org.invemar.argos.manglares.model.SitioManglarModel;
import co.org.invemar.argos.manglares.model.UnidadManejoModel;
import co.org.invemar.argos.manglares.vo.Parcela;
import co.org.invemar.argos.manglares.vo.SectorManglarVo;
import co.org.invemar.argos.manglares.vo.Sitio;
import co.org.invemar.argos.manglares.vo.UnidadManejo;
import co.org.invemar.library.siam.FinderFile;
import co.org.invemar.util.ResultadoTransaccion;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author usrsig15
 */
@ManagedBean(name = "sitiobean")
@ViewScoped
public class SitioUIBean {

    private SectorManglarVo sector = null;

    private int idSitio;
    private String nombreSitio;
    private String descripcionGeneralSitio;
    private String mensajeVentanaNuevoSitio;
    private ModeloDatosSitio modelodatositios;
    private Sitio sitio;
    private SitioManglarModel sitiomanglarmodel;
    private ArrayList<Sitio> sitios;
    private String abreviaturaSitio;
    private String sitioActivo;
    private String labelbotonRegistroSitios;
    private String disabledBotonActualizarSitio;
    private String pathImageSitio;
    private String pathkmlSitio;
    private UploadedFile kmlsitio;
    private UploadedFile picSitio;

    private String rutaAplicacion = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
    private String rutarelativa = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");

    private String rutaDescargaKml;
    private String rutaImagenSitio;

    private String pathKml;
    private String pathFotos;
    private String pathTemp;

    private String idCAR;

    private int idProyecto;
    private int idSector;

    private int idproyecto;

    private int idUnidadManejo;
    private String nombreUnidadManejo;
    private String descripcionGeneralUnidadManejo;
    private String categoriaUnidadManejo;
    private String isActivadaUnidadManejo;
    private String nombreCortoUnidadManejo;
    private String mensajeVentanaUnidadManejo;
    private ModeloDatosUnidadManejo unidadmanejomodelodatos;
    private UnidadManejoModel unidadManejoModelo;
    private UnidadManejo unidadManejo;
    private ArrayList<UnidadManejo> unidadesManejo;
    private String disabledBotonActualizarUnidadManejo;
    private String tituloVentanaUnidadManejo;

    ////////////////////////////////////////
    private String formaParcela;
    private int azimut;
    private double areaParcela;
    private Date fechaInstalacion;
    private double latitudParcela;
    private double longitudParcela;
    private int idParcela;
    private String descripcionParcela;
    private ModeloDatosParcela modeloDatosParcela;
    private String nombreCortoParcela;
    private String nombreParcela;
    private String isParcelaActiva;
    private String mensajeVentanaParcela;
    private Parcela parcela;
    private ArrayList<Parcela> parcelas;
    private String disabledBotonActualizarParcela;
    private ParcelaModel parcelaModelo;
    private String azimutActivo = "true";

    private String mansajeVentanaUnidadManejo = "";

    private String disableBotonAgregarORegistrarUnidadManejo;
    private String disableBotonAgregarORegistrarParcela;

    public SitioUIBean() {

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        idCAR = (String) session.getAttribute("entityid");
        String idP = (String) session.getAttribute("projectid");
        String idS = (String) session.getAttribute("sector");

//        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        String idS = request.getParameter("sector");
        System.out.println("idS:" + idS);
        System.out.println("id sector:" + idS);
        System.out.println("idP:" + idP);

        if (idP != null && idS != null) {
            idProyecto = Integer.parseInt(idP);
            idSector = Integer.parseInt(idS);
        } else {
            try {
                String servername = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
                int serverport = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://" + servername + ":" + serverport + "/argos/login?action=cerrarSession");
            } catch (IOException ex) {
                Logger.getLogger(EstacionesUIBeans1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        disabledBotonActualizarSitio = "true";
        labelbotonRegistroSitios = "Registrar";
        pathKml = rutaAplicacion + File.separator + "ingresos" + File.separator + "manglares" + File.separator + "kml";
        pathFotos = rutaAplicacion + File.separator + "ingresos" + File.separator + "manglares" + File.separator + "fotos";
        pathTemp = rutaAplicacion + File.separator + "ingresos" + File.separator + "manglares" + File.separator + "temp";

        sitiomanglarmodel = new SitioManglarModel();
        actualizarTablaSitiosAgregados();

        configuraUnidadManejo();

    }

    private void configuraUnidadManejo() {

        disabledBotonActualizarParcela = "true";
        disabledBotonActualizarUnidadManejo = "true";
        disableBotonAgregarORegistrarUnidadManejo = "false";
        disableBotonAgregarORegistrarParcela = "false";

        unidadManejoModelo = new UnidadManejoModel();
        parcelaModelo = new ParcelaModel();

        actualizarTablaUnidadesManejo();

        System.out.println("arranque de formulario");

    }

    public void handleRegistrarSitio(ActionEvent actionEvent) {
        System.out.println("nombre sitio:" + nombreSitio);
        registrarSitio();
    }

    public void onRowSelectTablaSitios(SelectEvent event) {
        int idSitio = ((Sitio) event.getObject()).getId();
        seleccionarSitio(idSitio);
    }

    private void ResetForm() {
        nombreSitio = "";
        abreviaturaSitio = "";
        descripcionGeneralSitio = "";
        actualizarTablaSitiosAgregados();
    }

    public void actualizarTablaSitiosAgregados() {
        sitios = sitiomanglarmodel.getTodosLosSectoresPorCorporacion(idSector, idCAR, idProyecto);
        if (sitios != null) {
            modelodatositios = new ModeloDatosSitio(sitios);
        }
    }

    public void seleccionarSitio(int idSitio) {
        try {


            disabledBotonActualizarSitio = "false";
            tituloVentanaUnidadManejo = "Gestionar unidades de manejo del sitio:" + idSitio;


            disabledBotonActualizarSitio = "false";
            tituloVentanaUnidadManejo = "Gestionar unidades de manejo del sitio:" + idSitio;

            sitios = sitiomanglarmodel.getSitioPorID(idProyecto, idSitio);

            sitio = sitios.get(0);
            idSitio = sitio.getId();

            nombreSitio = sitio.getNombre();
            descripcionGeneralSitio = sitio.getDescripcion();
            abreviaturaSitio = sitio.getAbreriatura();
            sitioActivo = sitio.getIsActivo();
            rutaDescargaKml = "../../" + "ingresos" + "/" + "manglares" + "/" + "kml" + "/" + idSitio + ".kml";
            rutaImagenSitio = CreatePathFileSitio("../../" + "ingresos" + "/" + "manglares" + "/" + "fotos" + "/", pathFotos);

            actualizarTablaUnidadesManejo();
            actualizarTablaSitiosAgregados();
        } catch (Exception e) {
            System.out.println("Error seleccionado el sitio:" + e.getMessage());
        }

    }

    private String CreatePathFileSitio(String path, String pathLoad) {
        FinderFile ff = new FinderFile();
        String p = null;
        if (sitio != null) {
            boolean r = ff.findFile(String.valueOf(sitio.getId()), new File(pathLoad));
            if (r) {
                p = path + sitio.getId() + "." + ff.getExtension();
            }
        }
        return p;
    }

    public void registrarSitio() {
        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        try {

            Sitio s = new Sitio();
            s.setIdSector(idSector);
            s.setIdCAR(idCAR);
            s.setCodProyecto(idProyecto);

            s.setNombre(nombreSitio);
            s.setDescripcion(descripcionGeneralSitio);
            s.setAbreriatura(abreviaturaSitio);
            s.setIsActivo(sitioActivo);

            System.out.println("nombre sitio:" + idSector);
            System.out.println("nombre sitio:" + nombreSitio);
            System.out.println("descripcionGeneralSitio:" + descripcionGeneralSitio);
            System.out.println("abreviaturaSitio:" + abreviaturaSitio);

            rt = sitiomanglarmodel.registraSitio(s);

            if (picSitio.getFileName().length() != 0) {
                subirArchivo(String.valueOf(rt.getCodigoError()), picSitio, pathFotos);

            }

            if (kmlsitio.getFileName().length() != 0) {

                co.org.invemar.library.siam.File f = new co.org.invemar.library.siam.File();

                if (f.getExtension(kmlsitio.getFileName()).equalsIgnoreCase("kml")) {
                    subirArchivo(String.valueOf(rt.getCodigoError()), kmlsitio, pathKml);
                } else {
                    FacesMessage msg = new FacesMessage("Archivo no valido:", kmlsitio.getFileName());
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }

            }

            System.out.println("pathkml:" + pathKml);
            System.out.println("pathfotos:" + pathFotos);

            if (rt != null && rt.getCodigoError() == 0) {
                idSitio = rt.getCodigoError();
                mensajeVentanaNuevoSitio = "Se ha registrado correctamente el sitio";
            } else if (rt != null && rt.getCodigoError() == -1) {
                mensajeVentanaNuevoSitio = "No se ha registrado correctamente el sitio";
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo registrarSitio del objeto EstacionesUIBeans:" + ex.getMessage());
        } finally {
            actualizarTablaSitiosAgregados();
        }

    }

    private void subirArchivo(String idArea, UploadedFile file, String pathfile) {
        try {
            co.org.invemar.library.siam.File f = new co.org.invemar.library.siam.File();
            f.copyFile(pathfile, idArea + "." + f.getExtension(file.getFileName()), file.getInputstream(), 5024);

        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error cargando el archivo:", file.getFileName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } finally {
            FacesMessage msg = new FacesMessage("Archivo cargado exitosamente", file.getFileName() + "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void actualizarSitio() {
        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        try {
            int idSitio = sitio.getId();
            Sitio s = new Sitio();
            s.setId(idSitio);
            s.setNombre(nombreSitio);
            s.setDescripcion(descripcionGeneralSitio);
            s.setAbreriatura(abreviaturaSitio);
            s.setIsActivo(sitioActivo);

            rt = sitiomanglarmodel.actualizarSitio(s);

            System.out.println("pic sitio:" + picSitio.getFileName());
            System.out.println("cod sitio:" + idSitio);

            if (picSitio.getFileName().length() != 0) {
                subirArchivo(String.valueOf(idSitio), picSitio, pathFotos);
            }
            System.out.println("kmlsitio:" + kmlsitio.getFileName());
            if (kmlsitio.getFileName().length() != 0) {

                co.org.invemar.library.siam.File f = new co.org.invemar.library.siam.File();

                if (f.getExtension(kmlsitio.getFileName()).equalsIgnoreCase("kml")) {
                    subirArchivo(String.valueOf(idSitio), kmlsitio, pathKml);
                } else {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Archivo no valido:", kmlsitio.getFileName());
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }

            }

            if (rt != null && rt.getCodigoError() == 0) {

                mensajeVentanaNuevoSitio = "Se ha actulizado correctamente el sitio";
            } else if (rt != null && rt.getCodigoError() == -1) {
                mensajeVentanaNuevoSitio = "No se ha actualizado correctamente el sitio";
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo actualizarSitio del objeto EstacionesUIBeans:" + ex.getMessage());
        } finally {
            actualizarTablaSitiosAgregados();
        }
    }

    public void onRowSelectTablaUnidadManejo(SelectEvent event) {
         
        UnidadManejo um=null;
        
        if(um!=null)
        {
        int idUnidadManejo = ((UnidadManejo) event.getObject()).getId();
        seleccionarUnidadManejo(idUnidadManejo);
        }else 
        {
            System.out.println("Unidad  de manejo es null");
        }


        if (unidadManejo!=null) {
               seleccionarUnidadManejo(unidadManejo.getId());
               System.out.println("unida de manejo no es null");
        }
      
 

    }

    public void actualizarTablaUnidadesManejo() {

        try {
            if (sitio != null) {
                unidadesManejo = unidadManejoModelo.getTodosLasUnidadesManejoPorCorporacion(sitio.getId(), idCAR, idproyecto);
                if (unidadesManejo != null) {
                    System.out.println("diferente de null");
                    unidadmanejomodelodatos = new ModeloDatosUnidadManejo(unidadesManejo);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(UnidadesManejoUIBeans.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void actualizarUnidadManejo(ActionEvent actionEvent) {
        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        try {

            int idUnidadManejo = unidadManejo.getId();
            UnidadManejo unidadManejo = new UnidadManejo();
            unidadManejo.setId(idUnidadManejo);
            unidadManejo.setIdSitio(idSitio);
            unidadManejo.setIdCAR(idCAR);
            unidadManejo.setCodProyecto(idproyecto);
            unidadManejo.setCategoria(Integer.parseInt(categoriaUnidadManejo));
            unidadManejo.setDescripcion(descripcionGeneralUnidadManejo);
            unidadManejo.setIsActivo(isActivadaUnidadManejo);

            rt = unidadManejoModelo.actualizarUnidadManejo(unidadManejo);

            if (rt != null && rt.getCodigo() == 0) {

                mensajeVentanaNuevoSitio = "Se ha actulizado correctamente la unidad de manejo";
            } else if (rt != null && rt.getCodigo() == -1) {
                mensajeVentanaNuevoSitio = "No se ha actualizado correctamente la unidad de manejo";
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo actualizarUnidadManejo del objeto EstacionesUIBeans:" + ex.getMessage());
            System.out.println("resultado de la insercion:" + rt.getMensaje());
        } finally {
            actualizarTablaUnidadesManejo();
        }

    }

    public void registrarUnidadManejo(ActionEvent actionEvent) {

        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        try {

            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            idCAR = (String) session.getAttribute("entityid");
            String idP = (String) session.getAttribute("projectid");
            idProyecto = Integer.parseInt(idP);

            UnidadManejo unidadManejo = new UnidadManejo();
            unidadManejo.setIdSitio(sitio.getId());
            unidadManejo.setCategoria(Integer.parseInt(categoriaUnidadManejo));

            unidadManejo.setIdCAR(idCAR);
            unidadManejo.setCodProyecto(idproyecto);
            unidadManejo.setDescripcion(descripcionGeneralUnidadManejo);
            unidadManejo.setIsActivo(isActivadaUnidadManejo);

            System.out.println("idCAR:" + idCAR);
            System.out.println("idproyecto:" + idP);
            System.out.println("idSitio:" + sitio.getId());
            System.out.println("categoriaUnidadManejo:" + categoriaUnidadManejo);
            System.out.println("descripcionGeneralUnidadManejo:" + descripcionGeneralUnidadManejo);
            System.out.println("isActivadaUnidadManejo:" + isActivadaUnidadManejo);

            rt = unidadManejoModelo.registrarUnidadManejo(unidadManejo);
            System.out.println("rt:" + rt.getCodigoError());

            if (rt != null && rt.getCodigoError() == 0) {
                mensajeVentanaUnidadManejo = "Se ha registrado correctamente la unidad de manejo";
            } else if (rt != null && (rt.getCodigoError() == -1 || rt.getCodigoError() == 1)) {
                mensajeVentanaUnidadManejo = "No se ha registrado correctamente la unidad de manejo";
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo registrar Unidad de Manejo del objeto EstacionesUIBeans:" + ex.getMessage());

        } finally {
            actualizarTablaUnidadesManejo();
        }

    }

    private void seleccionarUnidadManejo(int idUnidadManejo) {

        disabledBotonActualizarUnidadManejo = "false";
        try {


//            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//            idCAR = (String) session.getAttribute("entityid");
//            String idP = (String) session.getAttribute("projectid");
//
//            if (idCAR != null || idP != null ) {
//
//                System.out.println("id unidad de manejo:" + idUnidadManejo);
//                
//                System.out.println("idCAR:" + idCAR);
//
//                idProyecto = 2239;
//                System.out.println("idproyecto:" + idProyecto);
  //          unidadesManejo = unidadManejoModelo.getUnidadManejoPorID("UT", 2239, idUnidadManejo);
            // unidadManejo = unidadesManejo.get(0);
//                categoriaUnidadManejo = String.valueOf(unidadManejo.getCategoria());
//                System.out.println("categoria um:" + categoriaUnidadManejo);
//                System.out.println("de um:" + unidadManejo.getDescripcion());
            //}

            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            idCAR = (String) session.getAttribute("entityid");
            String idP = (String) session.getAttribute("projectid");

            if (idCAR != null || idP != null) {

                System.out.println("id unidad de manejo:" + idUnidadManejo);
                
                System.out.println("idCAR:" + idCAR);

                idProyecto = Integer.parseInt(idP);
                System.out.println("idproyecto:" + idProyecto);

                unidadesManejo = unidadManejoModelo.getUnidadManejoPorID(idproyecto, idUnidadManejo);
                unidadManejo = unidadesManejo.get(0);
                categoriaUnidadManejo = String.valueOf(unidadManejo.getCategoria());
                System.out.println("categoria um:" + categoriaUnidadManejo);
                System.out.println("de um:" + unidadManejo.getDescripcion());
            }

        } catch (Exception e) {
            System.out.println("Error buscando los datos de la unidad de manejo" + e.getMessage());
        }

    }

    public void registrarParcela(ActionEvent actionEvent) {
        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        try {

            Parcela parcela = new Parcela();
            parcela.setUnidadManejo(unidadManejo.getId());
            parcela.setFormaParcela(Integer.parseInt(formaParcela));
            parcela.setAzimut(azimut);
            parcela.setExtension(areaParcela);
            parcela.setFechaInstalacion(new java.sql.Date(fechaInstalacion.getTime()));
            parcela.setLongitud(longitudParcela);
            parcela.setLatitud(latitudParcela);
            parcela.setDescripcion(descripcionParcela);
            parcela.setIdCAR(unidadManejo.getIdCAR());
            parcela.setCodProyecto(unidadManejo.getCodProyecto());
            parcela.setIsActivo(isParcelaActiva);
            rt = parcelaModelo.registrarParcela(parcela);

            if (rt != null && rt.getCodigoError() == 0) {
                mensajeVentanaParcela = "Se ha registrado correctamente la parcela";
            } else if (rt != null && rt.getCodigoError() == -1) {
                mensajeVentanaParcela = "No se ha registrado correctamente la parcela";
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo rregistrarParcela  del objeto EstacionesUIBeans:" + ex.getMessage());
            System.out.println("resultado de la insercion:" + rt.getMensaje());
        } finally {
            actualizarTablaParcelas();
        }

    }

    public void actualizarParcela(ActionEvent actionEvent) {
        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        try {
            int idParcela = parcela.getId();
            Parcela parcela = new Parcela();
            parcela.setId(idParcela);
            parcela.setUnidadManejo(unidadManejo.getId());
            parcela.setFormaParcela(Integer.parseInt(formaParcela));
            parcela.setAzimut(azimut);
            parcela.setExtension(areaParcela);
            parcela.setFechaInstalacion(new java.sql.Date(fechaInstalacion.getTime()));
            parcela.setLongitud(longitudParcela);
            parcela.setLatitud(latitudParcela);
            parcela.setDescripcion(descripcionParcela);
            parcela.setIdCAR(unidadManejo.getIdCAR());
            parcela.setCodProyecto(unidadManejo.getCodProyecto());
            parcela.setIsActivo(isParcelaActiva);
            rt = parcelaModelo.actualizarParcela(parcela);

            if (rt != null) {
                System.out.println("rt no es null");
            } else {
                System.out.println("rt es null");
            }

            if (rt != null && rt.getCodigoError() == 0) {
                mensajeVentanaParcela = "Se ha actualizado correctamente la parcela";
            } else if (rt != null && rt.getCodigoError() == -1) {
                mensajeVentanaParcela = "No se ha actualizado correctamente la parcela";
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo actualizarParcela  del objeto EstacionesUIBeans:" + ex.getMessage());
            System.out.println("resultado de la insercion:" + rt.getMensaje());
        } finally {
            actualizarTablaParcelas();
        }
    }

    private void seleccionarParcela() {

        disabledBotonActualizarParcela = "false";
        if (parcela != null) {
            int idParcela = parcela.getId();
//            System.out.println("idParcela:" + idParcela);
            parcelas = parcelaModelo.getParcelaPorID(idproyecto, idParcela);
            if (parcelas != null) {
                parcela = parcelas.get(0);
                formaParcela = String.valueOf(parcela.getFormaParcela());
                azimut = parcela.getAzimut();
                areaParcela = parcela.getExtension();
//                System.out.println("area parcela seleccionada:"+areaParcela);
                fechaInstalacion = parcela.getFechaInstalacion();
                latitudParcela = parcela.getLatitud();
                longitudParcela = parcela.getLongitud();
                descripcionParcela = parcela.getDescripcion();
                isParcelaActiva = parcela.getIsActivo();
            } else {
                System.out.println("el vector parcelas esta vacio");
            }
            actualizarTablaParcelas();

        } else {
            System.out.println("No se ha seleccionado parcela alguhna");
        }
    }

    public void actualizarTablaParcelas() {
        if (unidadManejo != null && unidadManejo.getId() != 0) {
            parcelas = parcelaModelo.getTodosLasParcelasPorCorporacion(unidadManejo.getId(), idCAR, idproyecto);
            if (parcelas != null) {
                modeloDatosParcela = new ModeloDatosParcela(parcelas);
            }
        }

    }

    public void onChangeTipoParcela(AjaxBehaviorEvent event) {

        if (formaParcela != null && formaParcela.equals("44")) {
            azimutActivo = "true";

        } else if (formaParcela != null && formaParcela.equals("45")) {
            azimutActivo = "false";
        }
    }

    public void onRowSelectTablaParcela(SelectEvent event) {
        seleccionarParcela();

    }

    public SitioManglarModel getSitiomanglarmodel() {
        return sitiomanglarmodel;
    }

    public String getNombreSitio() {
        return nombreSitio;
    }

    public void setNombreSitio(String nombreSitio) {
        this.nombreSitio = nombreSitio;
    }

    public String getDescripcionGeneralSitio() {
        return descripcionGeneralSitio;
    }

    public void setDescripcionGeneralSitio(String descripcionGeneralSitio) {
        this.descripcionGeneralSitio = descripcionGeneralSitio;
    }

    public String getMensajeVentanaNuevoSitio() {
        return mensajeVentanaNuevoSitio;
    }

    public void setMensajeVentanaNuevoSitio(String mensajeVentanaNuevoSitio) {
        this.mensajeVentanaNuevoSitio = mensajeVentanaNuevoSitio;
    }

    public ModeloDatosSitio getModelodatositios() {
        return modelodatositios;
    }

    public void setModelodatositios(ModeloDatosSitio modelodatositios) {
        this.modelodatositios = modelodatositios;
    }

    public Sitio getSitio() {
        return sitio;
    }

    public void setSitio(Sitio sitio) {
        this.sitio = sitio;
    }

    public void setSitiomanglarmodel(SitioManglarModel sitiomanglarmodel) {
        this.sitiomanglarmodel = sitiomanglarmodel;
    }

    public ArrayList<Sitio> getSitios() {
        return sitios;
    }

    public void setSitios(ArrayList<Sitio> sitios) {
        this.sitios = sitios;
    }

    public String getAbreviaturaSitio() {
        return abreviaturaSitio;
    }

    public void setAbreviaturaSitio(String abreviaturaSitio) {
        this.abreviaturaSitio = abreviaturaSitio;
    }

    public String getSitioActivo() {
        return sitioActivo;
    }

    public void setSitioActivo(String sitioActivo) {
        this.sitioActivo = sitioActivo;
    }

    public String getLabelbotonRegistroSitios() {
        return labelbotonRegistroSitios;
    }

    public void setLabelbotonRegistroSitios(String labelbotonRegistroSitios) {
        this.labelbotonRegistroSitios = labelbotonRegistroSitios;
    }

    public String getDisabledBotonActualizarSitio() {
        return disabledBotonActualizarSitio;
    }

    public void setDisabledBotonActualizarSitio(String disabledBotonActualizarSitio) {
        this.disabledBotonActualizarSitio = disabledBotonActualizarSitio;
    }

    public String getPathImageSitio() {
        return pathImageSitio;
    }

    public void setPathImageSitio(String pathImageSitio) {
        this.pathImageSitio = pathImageSitio;
    }

    public String getPathkmlSitio() {
        return pathkmlSitio;
    }

    public void setPathkmlSitio(String pathkmlSitio) {
        this.pathkmlSitio = pathkmlSitio;
    }

    public UploadedFile getKmlsitio() {
        return kmlsitio;
    }

    public void setKmlsitio(UploadedFile kmlsitio) {
        this.kmlsitio = kmlsitio;
    }

    public UploadedFile getPicSitio() {
        return picSitio;
    }

    public void setPicSitio(UploadedFile picSitio) {
        this.picSitio = picSitio;
    }

    public int getIdSector() {
        return idSector;
    }

    public void setIdSector(int idSector) {
        this.idSector = idSector;
    }

    public int getIdSitio() {
        return idSitio;
    }

    public void setIdSitio(int idSitio) {
        this.idSitio = idSitio;
    }

    public String getRutaDescargaKml() {
        return rutaDescargaKml;
    }

    public void setRutaDescargaKml(String rutaDescargaKml) {
        this.rutaDescargaKml = rutaDescargaKml;
    }

    public String getRutaImagenSitio() {
        return rutaImagenSitio;
    }

    public void setRutaImagenSitio(String rutaImagenSitio) {
        this.rutaImagenSitio = rutaImagenSitio;
    }

    public SectorManglarVo getSector() {
        return sector;
    }

    public void setSector(SectorManglarVo sector) {
        this.sector = sector;
    }

    public String getRutaAplicacion() {
        return rutaAplicacion;
    }

    public void setRutaAplicacion(String rutaAplicacion) {
        this.rutaAplicacion = rutaAplicacion;
    }

    public String getRutarelativa() {
        return rutarelativa;
    }

    public void setRutarelativa(String rutarelativa) {
        this.rutarelativa = rutarelativa;
    }

    public String getPathKml() {
        return pathKml;
    }

    public void setPathKml(String pathKml) {
        this.pathKml = pathKml;
    }

    public String getPathFotos() {
        return pathFotos;
    }

    public void setPathFotos(String pathFotos) {
        this.pathFotos = pathFotos;
    }

    public String getPathTemp() {
        return pathTemp;
    }

    public void setPathTemp(String pathTemp) {
        this.pathTemp = pathTemp;
    }

    public String getIdCAR() {
        return idCAR;
    }

    public void setIdCAR(String idCAR) {
        this.idCAR = idCAR;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    public int getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(int idproyecto) {
        this.idproyecto = idproyecto;
    }

    public int getIdUnidadManejo() {
        return idUnidadManejo;
    }

    public void setIdUnidadManejo(int idUnidadManejo) {
        this.idUnidadManejo = idUnidadManejo;
    }

    public String getNombreUnidadManejo() {
        return nombreUnidadManejo;
    }

    public void setNombreUnidadManejo(String nombreUnidadManejo) {
        this.nombreUnidadManejo = nombreUnidadManejo;
    }

    public String getDescripcionGeneralUnidadManejo() {
        return descripcionGeneralUnidadManejo;
    }

    public void setDescripcionGeneralUnidadManejo(String descripcionGeneralUnidadManejo) {
        this.descripcionGeneralUnidadManejo = descripcionGeneralUnidadManejo;
    }

    public String getCategoriaUnidadManejo() {
        return categoriaUnidadManejo;
    }

    public void setCategoriaUnidadManejo(String categoriaUnidadManejo) {
        this.categoriaUnidadManejo = categoriaUnidadManejo;
    }

    public String getIsActivadaUnidadManejo() {
        return isActivadaUnidadManejo;
    }

    public void setIsActivadaUnidadManejo(String isActivadaUnidadManejo) {
        this.isActivadaUnidadManejo = isActivadaUnidadManejo;
    }

    public String getNombreCortoUnidadManejo() {
        return nombreCortoUnidadManejo;
    }

    public void setNombreCortoUnidadManejo(String nombreCortoUnidadManejo) {
        this.nombreCortoUnidadManejo = nombreCortoUnidadManejo;
    }

    public String getMensajeVentanaUnidadManejo() {
        return mensajeVentanaUnidadManejo;
    }

    public void setMensajeVentanaUnidadManejo(String mensajeVentanaUnidadManejo) {
        this.mensajeVentanaUnidadManejo = mensajeVentanaUnidadManejo;
    }

    public ModeloDatosUnidadManejo getUnidadmanejomodelodatos() {
        return unidadmanejomodelodatos;
    }

    public void setUnidadmanejomodelodatos(ModeloDatosUnidadManejo unidadmanejomodelodatos) {
        this.unidadmanejomodelodatos = unidadmanejomodelodatos;
    }

    public UnidadManejoModel getUnidadManejoModelo() {
        return unidadManejoModelo;
    }

    public void setUnidadManejoModelo(UnidadManejoModel unidadManejoModelo) {
        this.unidadManejoModelo = unidadManejoModelo;
    }

    public UnidadManejo getUnidadManejo() {
        return unidadManejo;
    }

    public void setUnidadManejo(UnidadManejo unidadManejo) {
        this.unidadManejo = unidadManejo;
    }

    public ArrayList<UnidadManejo> getUnidadesManejo() {
        return unidadesManejo;
    }

    public void setUnidadesManejo(ArrayList<UnidadManejo> unidadesManejo) {
        this.unidadesManejo = unidadesManejo;
    }

    public String getDisabledBotonActualizarUnidadManejo() {
        return disabledBotonActualizarUnidadManejo;
    }

    public void setDisabledBotonActualizarUnidadManejo(String disabledBotonActualizarUnidadManejo) {
        this.disabledBotonActualizarUnidadManejo = disabledBotonActualizarUnidadManejo;
    }

    public String getFormaParcela() {
        return formaParcela;
    }

    public void setFormaParcela(String formaParcela) {
        this.formaParcela = formaParcela;
    }

    public int getAzimut() {
        return azimut;
    }

    public void setAzimut(int azimut) {
        this.azimut = azimut;
    }

    public double getAreaParcela() {
        return areaParcela;
    }

    public void setAreaParcela(int areaParcela) {
        this.areaParcela = areaParcela;
    }

    public Date getFechaInstalacion() {
        return fechaInstalacion;
    }

    public void setFechaInstalacion(Date fechaInstalacion) {
        this.fechaInstalacion = fechaInstalacion;
    }

    public double getLatitudParcela() {
        return latitudParcela;
    }

    public void setLatitudParcela(double latitudParcela) {
        this.latitudParcela = latitudParcela;
    }

    public double getLongitudParcela() {
        return longitudParcela;
    }

    public void setLongitudParcela(double longitudParcela) {
        this.longitudParcela = longitudParcela;
    }

    public int getIdParcela() {
        return idParcela;
    }

    public void setIdParcela(int idParcela) {
        this.idParcela = idParcela;
    }

    public String getDescripcionParcela() {
        return descripcionParcela;
    }

    public void setDescripcionParcela(String descripcionParcela) {
        this.descripcionParcela = descripcionParcela;
    }

    public ModeloDatosParcela getModeloDatosParcela() {
        return modeloDatosParcela;
    }

    public void setModeloDatosParcela(ModeloDatosParcela modeloDatosParcela) {
        this.modeloDatosParcela = modeloDatosParcela;
    }

    public String getNombreCortoParcela() {
        return nombreCortoParcela;
    }

    public void setNombreCortoParcela(String nombreCortoParcela) {
        this.nombreCortoParcela = nombreCortoParcela;
    }

    public String getNombreParcela() {
        return nombreParcela;
    }

    public void setNombreParcela(String nombreParcela) {
        this.nombreParcela = nombreParcela;
    }

    public String getIsParcelaActiva() {
        return isParcelaActiva;
    }

    public void setIsParcelaActiva(String isParcelaActiva) {
        this.isParcelaActiva = isParcelaActiva;
    }

    public String getMensajeVentanaParcela() {
        return mensajeVentanaParcela;
    }

    public void setMensajeVentanaParcela(String mensajeVentanaParcela) {
        this.mensajeVentanaParcela = mensajeVentanaParcela;
    }

    public Parcela getParcela() {
        return parcela;
    }

    public void setParcela(Parcela parcela) {
        this.parcela = parcela;
    }

    public ArrayList<Parcela> getParcelas() {
        return parcelas;
    }

    public void setParcelas(ArrayList<Parcela> parcelas) {
        this.parcelas = parcelas;
    }

    public String getDisabledBotonActualizarParcela() {
        return disabledBotonActualizarParcela;
    }

    public void setDisabledBotonActualizarParcela(String disabledBotonActualizarParcela) {
        this.disabledBotonActualizarParcela = disabledBotonActualizarParcela;
    }

    public ParcelaModel getParcelaModelo() {
        return parcelaModelo;
    }

    public void setParcelaModelo(ParcelaModel parcelaModelo) {
        this.parcelaModelo = parcelaModelo;
    }

    public String getAzimutActivo() {
        return azimutActivo;
    }

    public void setAzimutActivo(String azimutActivo) {
        this.azimutActivo = azimutActivo;
    }

    public String getMansajeVentanaUnidadManejo() {
        return mansajeVentanaUnidadManejo;
    }

    public void setMansajeVentanaUnidadManejo(String mansajeVentanaUnidadManejo) {
        this.mansajeVentanaUnidadManejo = mansajeVentanaUnidadManejo;
    }

    public String getDisableBotonAgregarORegistrarUnidadManejo() {
        return disableBotonAgregarORegistrarUnidadManejo;
    }

    public void setDisableBotonAgregarORegistrarUnidadManejo(String disableBotonAgregarORegistrarUnidadManejo) {
        this.disableBotonAgregarORegistrarUnidadManejo = disableBotonAgregarORegistrarUnidadManejo;
    }

    public String getDisableBotonAgregarORegistrarParcela() {
        return disableBotonAgregarORegistrarParcela;
    }

    public void setDisableBotonAgregarORegistrarParcela(String disableBotonAgregarORegistrarParcela) {
        this.disableBotonAgregarORegistrarParcela = disableBotonAgregarORegistrarParcela;
    }

    public String getTituloVentanaUnidadManejo() {
        return tituloVentanaUnidadManejo;
    }

    public void setTituloVentanaUnidadManejo(String tituloVentana) {
        this.tituloVentanaUnidadManejo = tituloVentana;
    }
}
