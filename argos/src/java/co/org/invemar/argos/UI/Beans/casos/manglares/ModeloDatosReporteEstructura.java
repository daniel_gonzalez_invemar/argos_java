/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.manglares.vo.TablaGrafica;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author usrsig15
 */
public class ModeloDatosReporteEstructura extends ListDataModel<TablaGrafica> implements Serializable, SelectableDataModel<TablaGrafica> {
   
    public ModeloDatosReporteEstructura(List<TablaGrafica> tablagrafica) {         
        super(tablagrafica);
    }  
      
    @Override  
    public TablaGrafica getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<TablaGrafica> filas = (List<TablaGrafica>) getWrappedData();  
          
        for(TablaGrafica tablagrafica : filas) {  
            if(String.valueOf(tablagrafica.getAnio()).equals(rowKey))  
                return tablagrafica;  
        }            
        return null;  
    }  
  
    @Override  
    public Object getRowKey(TablaGrafica tablagrafica) {  
        return tablagrafica.getAnio();
    }
}
