/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.geograficos.model.Ecoregiones;
import co.org.invemar.argos.geograficos.model.Toponimia;
import co.org.invemar.argos.geograficos.model.ZonasProtegidas;
import co.org.invemar.argos.usuarios_sci.model.UDirentidadesModel;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import co.org.invemar.argos.configuracion.*;
import co.org.invemar.argos.manglares.model.SectorManglaresModel;
import co.org.invemar.argos.manglares.model.SitioManglarModel;
import co.org.invemar.argos.manglares.model.ParcelaModel;
import co.org.invemar.argos.manglares.model.TiposFisiograficosModel;
import co.org.invemar.argos.manglares.model.UnidadManejoModel;
import co.org.invemar.argos.manglares.vo.Componente;
import co.org.invemar.argos.manglares.vo.Parcela;
import co.org.invemar.argos.manglares.vo.SectorManglarVo;
import co.org.invemar.argos.manglares.vo.Sitio;
import co.org.invemar.argos.manglares.vo.UnidadManejo;
import co.org.invemar.library.siam.FinderFile;
import co.org.invemar.library.siam.Utilidades;
import co.org.invemar.library.siam.convertirs.Convert;
import co.org.invemar.library.siam.convertirs.CoordenateGrade;
import co.org.invemar.ushahidi.ManagmentFormFieldUshahidi;
import co.org.invemar.util.ConnectionDBFactory;
import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.ResultadoTransaccion;
import java.io.File;
import org.primefaces.event.FileUploadEvent;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author usrsig15
 */
@ManagedBean(name = "estacionesBean1")
@SessionScoped
public class EstacionesUIBeans1 implements Serializable {

    private String SiteStyle = null;
    private String region;
    private String descripcion = null;
    private String ecoregion = null;
    private String abreviaturaSector = null;
    private String toponimo = null;
    private String ecoregiones = null;
    private String areageografica = null;
    private String cuencahidrografica = null;
    private String zonaprotegidaCodigo = "no tiene";
    private String EsAreaprotegida = null;
    private String autoridadambiental = null;
    private String styleCmBoxAreasProtegidas = null;
    private String impactosConocidos = null;
    private String intervencionAntropica = null;
    private String actividaeconomica = null;
    private String descripcionGeneral = "";
    private String renderizarComboBoxAreasProtegidas = "false";
    private String renderizarpoutputLabelAreasProtegidas = "true";
    private String renderizarMessagesAreasProtegidas = "true";
    private String mensajeRegistro = "";
    private String mensajeRegistroStyle = "";
    private String mensaje = "";
    private int extension;
    private String idCAR = "";
    String idRol ="";
    private int idProyecto;
    private int idSector;
    private SectorManglarVo sector = null;
    private String sectorActivo = null;
    private List<SectorManglarVo> listaAreasManglares = null;
    private ModeloDatosAreaManglar modelodatosareamanglar = null;
    private Map<String, String> toponimosMap = new HashMap<String, String>();
    private Map<String, String> ecorregionesMap = new HashMap<String, String>();
    private Map<String, String> areasprotegidasMap = new HashMap<String, String>();
    private Map<String, String> zonasProtegidasMap = new HashMap<String, String>();
    private Map<String, String> tiposFisiograficos = new HashMap<String, String>();    
    private TreeMap<String, String> entityMap = new TreeMap<String, String>();
    private String[] selectedEntityBySector;   
    
    
    private Map<String, String> listaautoridadesAmbientales = null;
    private Map<String, String> listaimpactosconocidos = null;
    private Map<String, String> listaiIntervencionAntropica = null;
    private Map<String, String> listaiActividadesEconomicas = null;
    private ZonasProtegidas zonasprotegidas = new ZonasProtegidas();

    private String urlKmlFileSector;
    private String urlPicFileSector;

    private String rutaArchivoPicCargadoSector;
    private String rutaArchivoKmlCargadoSector;

    private Properties propiedades = null;
    private String URLInicio;
    ////////////////////////////////////////////
    private SectorManglaresModel sectorManglarModel;
    private String nombreSitio;
    private String descripcionGeneralSitio;
    private String mensajeVentanaNuevoSitio;
    private ModeloDatosSitio modelodatositios;
    private Sitio sitio;
    private SitioManglarModel sitiomanglarmodel;
    private ArrayList<Sitio> sitios;
    private String abreviaturaSitio;
    private String sitioActivo;
    private String labelbotonRegistroSitios;
    private String disabledBotonActualizarSitio;
    private String auditoriaSitio;

    private String urlKmlFileSitio;
    private String urlPicFileSitio;

    private String rutaArchivoPicCargadoSitio;
    private String rutaArchivoKmlCargadoSitio;

    ////////////////////////////////////
    private int idUnidadManejo;
    private String entitySector;
    private String entityManagmentUnit;
    private String entityStation;
    private String entityParcel;
    private String nombreUnidadManejo;
    private String descripcionGeneralUnidadManejo;
    private String categoriaUnidadManejo;
    private String isActivadaUnidadManejo;
    private String nombreCortoUnidadManejo;
    private String mensajeVentanaUnidadManejo;
    private ModeloDatosUnidadManejo unidadmanejomodelodatos;
    private UnidadManejoModel unidadManejoModelo;
    private UnidadManejo unidadManejo;
    private String unidadManejoSeleccionada;
    private int unidadManejoActual = 0;
    private ArrayList<UnidadManejo> unidadesManejo;
    private Map<String, String> unidadesManejoSeleccinable = null;
    private String disabledBotonActualizarUnidadManejo;

    private String urlKmlFileUnidadManejo;
    private String urlPicFileUnidadManejo;

    private String rutaArchivoPicCargadoUnidadManejo;
    private String rutaArchivoKmlCargadoUnidadManejo;

    ////////////////////////////////////////
    private String formaParcela;
    private int azimut;

    private double gradeLatitud = 0;
    private double minuteLatitud = 0;
    private double secondLatitud = 0;

    private double gradeLongitud = 0;
    private double minuteLongitud = 0;
    private double secondLongitud = 0;

    private String vigenciaLoc;
    private double areaParcela;
    private double areaParcelas;
    private Date fechaInstalacion;
    private double latitudParcela;
    private double longitudParcela;
    private int idParcela;
    private String descripcionParcela;
    private ModeloDatosParcela modeloDatosParcela;
    private String nombreCortoParcela;
    private String nombreParcela;
    private String isParcelaActiva;
    private String idTipoFisiograficos;
    private String mensajeVentanaParcela;
    private Parcela parcela;
    private ArrayList<Parcela> parcelas;
    private String disabledBotonActualizarParcela;
    private ParcelaModel parcelaModelo;
    private String azimutActivo = "true";
    private String latitudgradepanelActivo = "display:inline";
    private String longitudgradepanelActivo = "display:inline";

    private String latitudDecimalActivo = "display:inline";
    private String longituDecimalActivo = "display:inline";

    private String formatoCoordenate = "Decimal";

    private String disableBotonAgregarORegistrarSector;
    private String disableBotonAgregarORegistrarNuevoSitio;
    private String disableBotonAgregarORegistrarUnidadManejo;
    private String disableBotonAgregarORegistrarParcela;

    private String rutaAplicacion = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");
    private String pathFotos = rutaAplicacion + File.separator + "ingresos" + File.separator + "manglares" + File.separator + "fotos" + File.separator;
    private String pathKml = rutaAplicacion + File.separator + "ingresos" + File.separator + "manglares" + File.separator + "kml" + File.separator;

    private UploadedFile kml;

    private int componente;
    private double topoAreaParcelasRegeneracion = 500;
    private double valueConverted = 0;

    private Map<String, String> departamentAndInitial = null;

    private String aportaDatos = "";

    private String disableBotonActualizarSector = "false";
    private String disableBotonRegistrarSector = "false";
    private String disableBotonLoadSector = "true";
  

    private String disablecambiarunidadmanejo = "false";
    private String disabledBotonRegistrarUnidadManejo = "false";
    private String disabledBotonLoadUnidadManejo = "false";

    private String disabledBotonRegistrarSitio = "false";
    private String disabledBotonLoadSitio = "false";
    
    private String disableListAsociateEntity = "true";

    private String idCARValueInitial = "";
    private final String ID_ROL_ADMINISTRADOR="158";

    
    private void setDepartament(){
        
        departamentAndInitial = new HashMap<String, String>();
        departamentAndInitial.put("80000000", "ATL");
        departamentAndInitial.put("50000000", "ANT");
        departamentAndInitial.put("130000000", "BOL");
        departamentAndInitial.put("190000000", "CAU");
        departamentAndInitial.put("270000000", "CHO");
        departamentAndInitial.put("230000000", "COR");
        departamentAndInitial.put("440000000", "GUA");
        departamentAndInitial.put("470000000", "MAG");
        departamentAndInitial.put("520000000", "NAR");
        departamentAndInitial.put("880000000", "SAI");
        departamentAndInitial.put("700000000", "SUC");
        departamentAndInitial.put("760000000", "VAL");
    }
    
    public EstacionesUIBeans1() {

        controlaSession();

        disabledBotonActualizarParcela = "true";
        disabledBotonActualizarSitio = "true";
        disabledBotonActualizarUnidadManejo = "true";

        disableBotonAgregarORegistrarSector = "true";
        disableBotonAgregarORegistrarNuevoSitio = "true";
        disableBotonAgregarORegistrarUnidadManejo = "true";
        disableBotonAgregarORegistrarParcela = "true";

        labelbotonRegistroSitios = "Registrar";

        sectorManglarModel = new SectorManglaresModel();
        sitiomanglarmodel = new SitioManglarModel();
        unidadManejoModelo = new UnidadManejoModel();
        parcelaModelo = new ParcelaModel();

        sector = new SectorManglarVo();

        actualizaTablaSectoresAgregados();

        URLInicio = "../../index.jsp";

        zonasProtegidasMap = zonasprotegidas.getAllZonasProtegidas();

        EsAreaprotegida = "si";

        TiposFisiograficosModel tpm = new TiposFisiograficosModel();
        tiposFisiograficos = tpm.getAllTiposFisiograficos();

        CargaConfiguracion configuracion = new CargaConfiguracion();
        String PathArchivo = rutaAplicacion + File.separator + "WEB-INF" + File.separator + "classes" + File.separator;
        propiedades = configuracion.leeArchivoConfiguracion(PathArchivo);

        actualizarTablaUnidadesManejo();

        setDepartament();
        
        fillEntityMap(idCAR);

    }

    private void controlaSession() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        idRol=(String) session.getAttribute("idrol");       
        idCAR = (String) session.getAttribute("entityid");
        String idP = (String) session.getAttribute("projectid");
        aportaDatos = (String) session.getAttribute("aportadatos");

        handleDataContribute();

        if (idCAR == null) {
            try {
                String servername = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
                int serverport = FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort();
                FacesContext.getCurrentInstance().getExternalContext().redirect("http://" + servername + ":" + serverport + "/argos/login?action=cerrarSession");
            } catch (IOException ex) {
                Logger.getLogger(EstacionesUIBeans1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        idProyecto = Integer.parseInt(idP);
        System.out.println("idProyecto:" + idProyecto);
        System.out.println("idCAR:" + idCAR);
        System.out.println("idRol:" + idRol);
    }

   public void fillEntityMap(String mainEntity){
       UDirentidadesModel entities = new UDirentidadesModel();
       entityMap = entities.getAllEntity(mainEntity);
   }
  
     
    public void handleEcorregionesChange() {
        Ecoregiones ecoregiones = new Ecoregiones();
        if (region != null && !region.equals("")) {
            Toponimia toponimia = new Toponimia();
            ecorregionesMap = ecoregiones.EcoregionesPorRegion(region);
            toponimosMap = toponimia.FillMapToponimosPorRegion("D", conversorCodigoRegion(region));
        } else {
            ecorregionesMap = new HashMap<String, String>();
        }

    }

    private void handleDataContribute() {

        System.out.println("aportaDatos:" + aportaDatos);

        if (aportaDatos != null && !aportaDatos.equalsIgnoreCase("")) {
            idCARValueInitial = idCAR;
            idCAR = idCAR + "','" + aportaDatos;
            disablebuttonByLinkedWithCAR();
        }

        System.out.println("idCAR:" + idCAR);
    }

    private void disablebuttonByLinkedWithCAR() {

        if (aportaDatos != null && !aportaDatos.equalsIgnoreCase("")) {

            disableBotonLoadSector = "true";
            disableBotonRegistrarSector = "true";
            disableBotonActualizarSector = "true";
            disableBotonAgregarORegistrarNuevoSitio = "false";

            disabledBotonRegistrarUnidadManejo = "true";
            disablecambiarunidadmanejo = "true";
            disabledBotonActualizarUnidadManejo = "true";
            disabledBotonLoadUnidadManejo = "true";

            disabledBotonActualizarSitio = "true";
            disabledBotonActualizarSitio = "true";
            disabledBotonRegistrarSitio = "true";
            disabledBotonLoadSitio = "true";

        }

    }

    public void seleccionarSector() {

        ArrayList<SectorManglarVo> listasectorActualizar = sectorManglarModel.getSectorPorID(idCAR, idProyecto, sector.getId());
       try{
           
        disableBotonLoadSector="false"    ;
           
        sector = listasectorActualizar.get(0);
        region = sector.getRegion();
        Ecoregiones ecoregiones = new Ecoregiones();
        ecorregionesMap = ecoregiones.EcoregionesPorRegion(region);
        ecoregion = sector.getUAC();
        Toponimia toponimia = new Toponimia();
        toponimosMap = toponimia.FillMapToponimosPorRegion("D", conversorCodigoRegion(region));

        toponimo = sector.getDepartamento();
        abreviaturaSector = sector.getAbreriatura();
        descripcionGeneral = sector.getDescripcion();
        cuencahidrografica = sector.getCuencaHidrografica();
        zonaprotegidaCodigo = sector.getAreaProtegida();
        EsAreaprotegida = sector.getIsAreaProtegida();
        areageografica = sector.getNombre();
        sectorActivo = sector.getActivo();     
        idCAR = sector.getIdCAR();
        selectedEntityBySector=sector.getEntities();

        if (sectorActivo.equalsIgnoreCase("Activado")) {
            disableBotonAgregarORegistrarNuevoSitio = "false";
        } else {
            disableBotonAgregarORegistrarNuevoSitio = "true";
        }

        urlPicFileSector = CreatePathFileArea("../../" + "ingresos" + "/" + "manglares" + "/" + "fotos" + "/", pathFotos, sector.getId());
        urlKmlFileSector = CreatePathFileArea("../../" + "ingresos" + "/" + "manglares" + "/" + "kml" + "/", pathKml, sector.getId());

        CleanFieldFormUnidadManejo();
        actualizarTablaUnidadesManejo();
       }catch(Exception e){
          Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,  e.getMessage() );
       }
       

    }

    private void seleccionarSitio() {
        disabledBotonActualizarSitio = "false";

        int idsitio = sitio.getId();
        sitios = sitiomanglarmodel.getSitioPorID(idProyecto, idsitio);
        sitio = sitios.get(0);
        nombreSitio = sitio.getNombre();

        descripcionGeneralSitio = sitio.getDescripcion();
        abreviaturaSitio = sitio.getAbreriatura();
        sitioActivo = sitio.getIsActivo();
        auditoriaSitio = sitio.getAuditoria();
        
        

        urlPicFileSitio = CreatePathFileArea("../../" + "ingresos" + "/" + "manglares" + "/" + "fotos" + "/", pathFotos, idsitio);
        urlKmlFileSitio = CreatePathFileArea("../../" + "ingresos" + "/" + "manglares" + "/" + "kml" + "/", pathKml, idsitio);

        if (sitioActivo.equalsIgnoreCase("Activado")) {
            disableBotonAgregarORegistrarUnidadManejo = "false";
        } else {
            disableBotonAgregarORegistrarUnidadManejo = "true";
        }
        actualizarTablaSitiosAgregados();
        actualizarTablaParcelas();
        disablebuttonByLinkedWithCAR();

    }

    private void seleccionarUnidadManejo() {

        disabledBotonActualizarUnidadManejo = "false";
        if (unidadManejo != null) {
            int idUnidadManejo = unidadManejo.getId();
            unidadesManejo = unidadManejoModelo.getUnidadManejoPorID(idProyecto, idUnidadManejo);
            unidadManejo = unidadesManejo.get(0);
            categoriaUnidadManejo = String.valueOf(unidadManejo.getCategoria());
            descripcionGeneralUnidadManejo = unidadManejo.getDescripcion();
            isActivadaUnidadManejo = unidadManejo.getIsActivo();

            urlPicFileUnidadManejo = CreatePathFileArea("../../" + "ingresos" + "/" + "manglares" + "/" + "fotos" + "/", pathFotos, idUnidadManejo);
            urlKmlFileUnidadManejo = CreatePathFileArea("../../" + "ingresos" + "/" + "manglares" + "/" + "kml" + "/", pathKml, idUnidadManejo);

            if (isActivadaUnidadManejo.equalsIgnoreCase("Activado")) {
                disableBotonAgregarORegistrarParcela = "false";
            } else {
                disableBotonAgregarORegistrarParcela = "true";
            }

            unidadManejoActual = idUnidadManejo;
            actualizarTablaUnidadesManejo();
            actualizarTablaSitiosAgregados();
            disablebuttonByLinkedWithCAR();

        }
    }

    private void seleccionarParcela() {

        disabledBotonActualizarParcela = "false";
        if (parcela != null) {
            int idParcela = parcela.getId();

            parcelas = parcelaModelo.getParcelaPorID(idProyecto, idParcela);
            if (parcelas != null) {
                parcela = parcelas.get(0);
                formaParcela = String.valueOf(parcela.getFormaParcela());
                azimut = parcela.getAzimut();
                areaParcelas = parcela.getExtension();
                idTipoFisiograficos = String.valueOf(parcela.getTipoFisiografico());
                componente = parcela.getComponente().getId();
                onChangeTipoParcela(null);
                fechaInstalacion = parcela.getFechaInstalacion();
                latitudParcela = parcela.getLatitud();
                longitudParcela = parcela.getLongitud();
                descripcionParcela = parcela.getDescripcion();
                isParcelaActiva = parcela.getIsActivo();
            } else {
                System.out.println("el vector parcelas esta vacio");
            }
            actualizarTablaParcelas();

        } else {
            System.out.println("No se ha seleccionado parcela alguhna");
        }
    }

    public void actualizaTablaSectoresAgregados() {
        SectorManglaresModel smm = new SectorManglaresModel();
        ArrayList<SectorManglarVo> listaAreaManglar=null;
        
        if (idRol.equalsIgnoreCase(ID_ROL_ADMINISTRADOR)) {
            listaAreaManglar = smm.getTodosLosSectores(idProyecto);
            this.setDisableListAsociateEntity("false");
        }else{
            listaAreaManglar = smm.getTodosLosSectoresPorCorporacion(idCAR, idProyecto);
           
           
            
           this.setDisableListAsociateEntity("true");
        }
      
        modelodatosareamanglar = new ModeloDatosAreaManglar(listaAreaManglar);
    }
    
    
    
       

    public void actualizarTablaSitiosAgregados() {

        if (unidadManejo != null && unidadManejo.getId() != 0) {
            sitios = sitiomanglarmodel.getTodosLosSectoresPorCorporacion(unidadManejo.getId(), unidadManejo.getIdCAR(), unidadManejo.getCodProyecto());
            if (sitios != null) {
                modelodatositios = new ModeloDatosSitio(sitios);
            }
        }

    }

    public void actualizarTablaParcelas() {
        if (sitio != null && sitio.getId() != 0) {
            parcelas = parcelaModelo.getTodosLasParcelasPorEstacion(sitio.getId(), idProyecto);
            if (parcelas != null) {
                modeloDatosParcela = new ModeloDatosParcela(parcelas);
            }
        }

    }

    public void actualizarTablaUnidadesManejo() {

        if (sector != null && sector.getId() != 0) {
            unidadesManejo = unidadManejoModelo.getTodosLasUnidadesManejoPorSector(sector.getId(), idProyecto);
            unidadesManejoSeleccinable = unidadManejoModelo.getTodosLasUnidadesManejoActivas(sector.getId(), unidadManejoActual, idCAR, idProyecto);
            if (unidadesManejo != null) {
                unidadmanejomodelodatos = new ModeloDatosUnidadManejo(unidadesManejo);
            }
        }

    }

    public void handleToCheckBoxListaAreasProtegidas(AjaxBehaviorEvent event) {

        if (EsAreaprotegida != null && EsAreaprotegida.equals("si")) {
            renderizarComboBoxAreasProtegidas = "true";
            styleCmBoxAreasProtegidas = "display:block";

        } else if (EsAreaprotegida != null && EsAreaprotegida.equals("no")) {
            styleCmBoxAreasProtegidas = "display:none;";
            renderizarComboBoxAreasProtegidas = "false";

        }
    }

    public void onChangeTipoParcela(AjaxBehaviorEvent event) {

        if (formaParcela != null && formaParcela.equals("44")) {
            azimutActivo = "true";

        } else if (formaParcela != null && formaParcela.equals("45")) {
            azimutActivo = "false";
        }
    }

    public void onChangeFormatType(AjaxBehaviorEvent event) {

    }

    public void handleToponimiaChange(AjaxBehaviorEvent e) {

        if (region != null && !region.equals("")) {
            Toponimia toponimia = new Toponimia();
            toponimosMap = toponimia.FillMapToponimosPorRegion("D", ecoregion);

        } else {
            toponimosMap = new HashMap<String, String>();

        }
    }

    public void handleToListaGetAllCAR() {
        UDirentidadesModel model = new UDirentidadesModel();
        listaautoridadesAmbientales = model.getAllCAR();

    }

    public void handleFileUploadPicSitio(FileUploadEvent event) {

        rutaArchivoPicCargadoSitio = subirArchivo(event.getFile().getFileName(), event.getFile(), pathFotos);

    }

    public void handleFileUploadkmlSitio(FileUploadEvent event) {
        rutaArchivoKmlCargadoSitio = subirArchivo(event.getFile().getFileName(), event.getFile(), pathKml);

    }

    public void handleFileUploadPicUnnidadManejo(FileUploadEvent event) {

        rutaArchivoPicCargadoUnidadManejo = subirArchivo(event.getFile().getFileName(), event.getFile(), pathFotos);

    }

    public void handleFileUploadkmlUnidadManejo(FileUploadEvent event) {

        rutaArchivoKmlCargadoUnidadManejo = subirArchivo(event.getFile().getFileName(), event.getFile(), pathKml);

    }

    public void handleFileUploadPicSector(FileUploadEvent event) {

        rutaArchivoPicCargadoSector = subirArchivo(event.getFile().getFileName(), event.getFile(), pathFotos);
        renombraArchivos(pathFotos, new File(rutaArchivoPicCargadoSector), String.valueOf(sector.getId()));  
       
    }

    public void handleFileUploadkmlSector(FileUploadEvent event) {

        rutaArchivoKmlCargadoSector = subirArchivo(event.getFile().getFileName(), event.getFile(), pathKml);

    }

    public void handleActualizarSector() {

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        idCAR = (String) session.getAttribute("entityid");
        String idSector = "";
        if (kml != null) {
            idSector = registrarSector();
            try {

                FacesMessage msg = new FacesMessage("Succesful", kml.getFileName() + " is uploaded.");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                String ruta = FacesContext.getCurrentInstance().getExternalContext().getRealPath("");

                ruta = ruta + File.separator + "ingresos" + File.separator + "manglares" + File.separator + "kml";
                System.out.println("ruta:" + ruta);
                String nombreArchivo = kml.getFileName();

                int i = nombreArchivo.lastIndexOf('.');
                String extension = "jpg";
                if (i > 0 && i < nombreArchivo.length() - 1) {
                    extension = nombreArchivo.substring(i + 1).toLowerCase();
                }

                copyFile(ruta, String.valueOf(sector.getId()) + "." + extension, kml.getInputstream());
            } catch (Exception ex) {
                System.out.println("Error en la carga de datos:" + ex.getMessage());
            }
        } else {
            System.out.println("kml es null");
        }

    }

    public void onRowSelectTabla(SelectEvent event) {
        seleccionarSector();

    }

    public void onRowSelectTablaSitios(SelectEvent event) {
        seleccionarSitio();
    }

    public void onRowSelectTablaUnidadManejo(SelectEvent event) {
        seleccionarUnidadManejo();
    }

    public void onRowSelectTablaParcela(SelectEvent event) {
        seleccionarParcela();

    }

    public void registrarUnidadManejo(ActionEvent actionEvent) {

        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        try {

            UnidadManejo unidadManejo = new UnidadManejo();
            unidadManejo.setCategoria(Integer.parseInt(categoriaUnidadManejo));
            unidadManejo.setIdSector(sector.getId());
            unidadManejo.setIdCAR(sector.getIdCAR());
            unidadManejo.setCodProyecto(sector.getCodProyecto());
            unidadManejo.setDescripcion(descripcionGeneralUnidadManejo);
            unidadManejo.setIsActivo(isActivadaUnidadManejo);

            rt = unidadManejoModelo.registrarUnidadManejo(unidadManejo);
            System.out.println("rt codigo error:" + rt.getCodigoError());
            if (rt != null && rt.getCodigoError() > 1) {

                updateFormEgretta("ManagmentUnit");

                renombraArchivos(pathFotos, new File(rutaArchivoPicCargadoUnidadManejo), String.valueOf(rt.getCodigoError()));
                renombraArchivos(pathKml, new File(rutaArchivoKmlCargadoUnidadManejo), String.valueOf(rt.getCodigoError()));

                FacesMessage msg = new FacesMessage("Se ha registrado correctamente la unidad de manejo", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);

            } else if (rt != null && rt.getCodigoError() == -1) {
                FacesMessage msg = new FacesMessage("No se ha registrado correctamente la unidad de manejoo", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);

            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo registrar Unidad de Manejo del objeto EstacionesUIBeans:" + ex.getMessage());

        } finally {
            actualizarTablaUnidadesManejo();
        }

    }

    private String conversorCodigoRegion(String codRegion) {

        String auxregion = "";
        if (codRegion.equalsIgnoreCase("11")) {
            auxregion = "10";
        } else if (codRegion.equalsIgnoreCase("21")) {
            auxregion = "20";
        } else {
            auxregion = "13";
        }
        return auxregion;
    }

     
    
    public String registrarSector() {

        sector = new SectorManglarVo();
        String idsector = "0";
        ResultadoTransaccion rt = null;
        controlaSession();

    
        
        try {
            sector.setNombre(areageografica);
            sector.setIdCAR(idCAR);
            sector.setUAC(ecoregion);
            sector.setRegion(region);
            sector.setActividadesEconomnicas(actividaeconomica);
            sector.setAreaProtegida(zonaprotegidaCodigo);
            sector.setCuencaHidrografica(cuencahidrografica);
            sector.setDepartamento(toponimo);
            sector.setDescripcion(descripcionGeneral);
            sector.setExtension(extension);
            sector.setIsAreaProtegida(EsAreaprotegida);
            sector.setAutoridadAmbientalACargo(idCAR);
            sector.setImpactosConocidos(impactosConocidos);
            sector.setNivelIntervencionAntropica(intervencionAntropica);
            sector.setAbreriatura(abreviaturaSector);
            sector.setActivo(sectorActivo);
            sector.setEntities(selectedEntityBySector);
          
            
           

            rt = sectorManglarModel.registraSector(sector);
           

            if (rt != null && rt.getCodigoError() == 0) {
                idsector = String.valueOf(rt.getCodigo());

                updateFormEgretta("sectors");
                renombraArchivos(pathFotos, new File(rutaArchivoPicCargadoSector), String.valueOf(idsector));
                renombraArchivos(pathKml, new File(rutaArchivoKmlCargadoSector), String.valueOf(idsector));               

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha registrado correctamente el sector", " "));
            } else {

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se ha registrado correctamente el sector", " "));
            }
        } catch (Exception ex) {
            System.out.println("Error en el m�todo registrarSector del objeto EstacionesUIBeans:" + ex.getMessage());
        } finally {
            actualizaTablaSectoresAgregados();
        }
        return idsector;

    }

    private void updateFormEgretta(String typeArea) {
        Connection conDbUshahidi = null;
        Connection conOracle = null;
        try {

            ConnectionFactory cFactory = new ConnectionFactory();
            conOracle = cFactory.createConnection("manglar");

            ConnectionDBFactory conectionfactory = new ConnectionDBFactory();
            conDbUshahidi = conectionfactory.getConnection("mysql", "ushahidiegreta");

            ManagmentFormFieldUshahidi mffu = new ManagmentFormFieldUshahidi();
            if (typeArea.equalsIgnoreCase("Sectors")) {
                mffu.fillFieldSectores(conOracle, conDbUshahidi);
            } else {
                mffu.fillFieldManagmentUnit(conOracle, conDbUshahidi);
            }

        } catch (SQLException ex) {
            Logger.getLogger(EstacionesUIBeans1.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conDbUshahidi.close();
                conDbUshahidi = null;
                conOracle.close();
                conOracle = null;
            } catch (SQLException ex) {
                Logger.getLogger(EstacionesUIBeans1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void registrarSitio(ActionEvent actionEvent) {

        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        controlaSession();
        try {

            Sitio s = new Sitio();

            s.setIdUnidadManejo(unidadManejo.getId());
            s.setIdCAR(unidadManejo.getIdCAR());
            s.setCodProyecto(unidadManejo.getCodProyecto());
            s.setNombre(nombreSitio);
            s.setDescripcion(descripcionGeneralSitio);
            s.setAbreriatura(abreviaturaSitio);
            s.setIsActivo(sitioActivo);
           

            rt = sitiomanglarmodel.registraSitio(s);

            if (rt != null && rt.getCodigoError() != 0) {

                renombraArchivos(pathFotos, new File(rutaArchivoPicCargadoSitio), String.valueOf(rt.getCodigoError()));
                renombraArchivos(pathKml, new File(rutaArchivoKmlCargadoSitio), String.valueOf(rt.getCodigoError()));

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha registrado correctamente el sitio", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);

            } else if (rt != null && rt.getCodigoError() == -1) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se ha registrado correctamente el sitio", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);

            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo registrarSitio del objeto EstacionesUIBeans:" + ex.getMessage());
        } finally {
            actualizarTablaSitiosAgregados();
        }

    }

    public void registrarParcela(ActionEvent actionEvent) {
        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        boolean isAreaCorrect=false;
        boolean isCoordenateCorrect=false;
        controlaSession();
        try {

            Parcela parcela = new Parcela();
            parcela.setIdEstacion(sitio.getId());
            parcela.setFormaParcela(Integer.parseInt(formaParcela));
            parcela.setAzimut(azimut);
            parcela.setExtension(areaParcelas);
            parcela.setFechaInstalacion(new java.sql.Date(fechaInstalacion.getTime()));
  
            parcela.setLongitud(longitudParcela);
            parcela.setLatitud(latitudParcela);
            parcela.setDescripcion(descripcionParcela);
            parcela.setIdCAR(unidadManejo.getIdCAR());
            parcela.setCodProyecto(unidadManejo.getCodProyecto());
            parcela.setIsActivo(isParcelaActiva);
            parcela.setTipoFisiografico(Long.parseLong(idTipoFisiograficos));
            parcela.setComponente(new Componente(componente));

           // System.out.println("latitud:"+latitudParcela);
           // System.out.println("Longitud:"+longitudParcela);
            if ((latitudParcela >= 1d && latitudParcela <= 14d) && (longitudParcela >= -82d && longitudParcela <=-71d )) {    
                isCoordenateCorrect=true;
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Las coordenadas que esta tratando de ingresar no estan en el territorio colombiano.", " "));
                isCoordenateCorrect=false;
            }
            
            
            if (areaParcelas>=1) {
               isAreaCorrect=true;
            }else{
                isAreaCorrect=false;
               FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "El area de la parcela no puede tener un valor de 0.", " "));  
            }
                
            
            if (isAreaCorrect && isCoordenateCorrect) {
                 rt = parcelaModelo.registrarParcela(parcela);
                 if (rt != null && rt.getCodigoError() == 0) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha registrado correctamente la parcela", " "));

                } else if (rt != null && rt.getCodigoError() == -1) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se ha registrado correctamente la parcela", " "));

                }
            }else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se ha registrado correctamente la parcela", " ")); 
            }
              

        } catch (Exception ex) {
           
             System.out.println("Error en el m�todo rregistrarParcela  del objeto EstacionesUIBeans:" + ex.getMessage());          
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se ha registrado correctamente la parcela. El area de la parcela es 0.", ex.getMessage()));
           
            
        } finally {
            actualizarTablaParcelas();
        }

    }

    public void reasignarEstacion(javax.faces.event.ActionEvent actionEvent) {

        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        controlaSession();
        try {

            Integer idUnidadManejo = Integer.parseInt(unidadManejoSeleccionada);
            rt = sitiomanglarmodel.reasignaEstacionAUnidadManejo(sitio, idUnidadManejo);

            if (rt != null && rt.getCodigoError() == 0) {

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha reasignado la estacion a la unidad de manejo.", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);

            } else if (rt != null && rt.getCodigoError() != 0) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se reasignado la estacion a la unidad de manejo.", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);

            }
        } catch (Exception ex) {
            System.out.println("Error en el m�todo reasignarEstacionANuevaEstacion del objeto EstacionesUIBeans:" + ex.getMessage());
        } finally {

        }

    }

    public void actualizarUnidadManejo(ActionEvent actionEvent) {
        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        controlaSession();
        try {
            int idUnidadManejo = unidadManejo.getId();
            UnidadManejo unidadManejo = new UnidadManejo();
            unidadManejo.setId(idUnidadManejo);
            unidadManejo.setIdSector(sector.getId());
            unidadManejo.setIdCAR(sector.getIdCAR());
            unidadManejo.setCodProyecto(idProyecto);
            unidadManejo.setCategoria(Integer.parseInt(categoriaUnidadManejo));
            unidadManejo.setDescripcion(descripcionGeneralUnidadManejo);
            unidadManejo.setIsActivo(isActivadaUnidadManejo);

            rt = unidadManejoModelo.actualizarUnidadManejo(unidadManejo);

            if (rt != null && rt.getCodigoError() >= 1) {
                updateFormEgretta("ManagmentUnit");
                actualizaArchivosAreaGeograficas(rutaArchivoPicCargadoUnidadManejo, rutaArchivoKmlCargadoUnidadManejo, idUnidadManejo);

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actulizado correctamente la unidad de manejo", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);

            } else if (rt != null && rt.getCodigo() == -1) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se ha actualizado correctamente la unidad de manejo", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);

                //mensajeVentanaNuevoSitio = "No se ha actualizado correctamente la unidad de manejo";
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo actualizarUnidadManejo del objeto EstacionesUIBeans:" + ex.getMessage());
        } finally {
            actualizarTablaUnidadesManejo();
        }

    }

    private void actualizaArchivosAreaGeograficas(String rutaPic, String rutaKml, int area) {
        co.org.invemar.library.siam.File f = new co.org.invemar.library.siam.File();
        FinderFile ff = new FinderFile();

        if (rutaPic != null) {
            if (ff.findFile(String.valueOf(area), new File(pathFotos))) {
                f.deleteFile(String.valueOf(ff.getPathfile()));
                renombraArchivos(pathFotos, new File(rutaPic), String.valueOf(area));
            } else {
                renombraArchivos(pathFotos, new File(rutaPic), String.valueOf(area));
            }
        }

        if (rutaKml != null) {
            if (ff.findFile(String.valueOf(area), new File(pathKml))) {
                f.deleteFile(String.valueOf(ff.getPathfile()));
                renombraArchivos(pathKml, new File(rutaKml), String.valueOf(area));
            } else {
                renombraArchivos(pathKml, new File(rutaKml), String.valueOf(area));
            }
        }

    }

    public void convertCoordenate() {
        Convert c = new Convert();
        valueConverted = c.GrateMinuteAndSecondToDecimal(gradeLongitud, (int) minuteLongitud, secondLongitud);

    }

    public void actualizarParcela(ActionEvent actionEvent) {

        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        controlaSession();
        try {
            int idParcela = parcela.getId();
            Parcela parcela = new Parcela();
            parcela.setId(idParcela);
            parcela.setUnidadManejo(unidadManejo.getId());
            parcela.setFormaParcela(Integer.parseInt(formaParcela));
            parcela.setAzimut(azimut);
            parcela.setExtension(areaParcelas);
            parcela.setFechaInstalacion(new java.sql.Date(fechaInstalacion.getTime()));
            longitudParcela = longitudParcela * (-1);
            parcela.setLongitud(longitudParcela);
            parcela.setLatitud(latitudParcela);
            parcela.setDescripcion(descripcionParcela);
            parcela.setIdCAR(unidadManejo.getIdCAR());
            parcela.setCodProyecto(unidadManejo.getCodProyecto());
            parcela.setIsActivo(isParcelaActiva);
            parcela.setTipoFisiografico(Long.parseLong(idTipoFisiograficos));

            parcela.setComponente(new Componente(componente));

            rt = parcelaModelo.actualizarParcela(parcela);

            if (rt != null) {
                System.out.println("rt no es null");
            } else {
                System.out.println("rt es null");
            }

            if (rt != null && rt.getCodigoError() == 0) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado correctamente la parcela", " "));
            } else if (rt != null && rt.getCodigoError() == -1) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se ha actualizado correctamente la parcela", " "));
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo actualizarParcela  del objeto EstacionesUIBeans:" + ex.getMessage());
            System.out.println("resultado de la insercion:" + rt.getMensaje());
        } finally {
            actualizarTablaParcelas();
        }
    }

    
    
    public void actualizarSector() {
        
        
        int idSector = sector.getId();
        sector = new SectorManglarVo();
        ResultadoTransaccion rt = null;
        Connection conDbUshahidi = null;
        controlaSession();
        try {

            sector.setId(idSector);
            sector.setNombre(areageografica);
            sector.setIdCAR(idCAR);
            sector.setUAC(ecoregion);
            sector.setRegion(region);

            sector.setAreaProtegida(zonaprotegidaCodigo);
            sector.setCuencaHidrografica(cuencahidrografica);
            sector.setDepartamento(toponimo);
            sector.setDescripcion(descripcionGeneral);
            sector.setExtension(extension);
            sector.setIsAreaProtegida(EsAreaprotegida);
            sector.setAbreriatura(abreviaturaSector);
            sector.setActivo(sectorActivo);
            sector.setEntities(selectedEntityBySector);
          
            rt = sectorManglarModel.actualizarSector(sector);

            if (rt.getCodigoError() == 0) {
                actualizaArchivosAreaGeograficas(rutaArchivoPicCargadoSector, rutaArchivoKmlCargadoSector, idSector);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha actualizado correctamente el area", " "));

                updateFormEgretta("sectors");

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se ha actualizado  correctamente el area", " "));
            }
        } catch (Exception ex) {
            System.out.println("Error en el m�todo actualizarSector del objeto EstacionesUIBeans:" + ex.getMessage());
            
            if (rt.getMensaje()!=null) {
                 System.out.println("resultado de la actualizacion:" + rt.getMensaje());
            }
             
        } finally {
            actualizaTablaSectoresAgregados();
        }

    }

    public void actualizarSitio() {
        ResultadoTransaccion rt = new ResultadoTransaccion();
        rt.setCodigoError(-2);
        controlaSession();
        try {
            int idSitio = sitio.getId();
            System.out.println("idSitio para actualizar:" + idSitio);
            Sitio s = new Sitio();
            s.setId(idSitio);
            s.setNombre(nombreSitio);
            s.setDescripcion(descripcionGeneralSitio);
            s.setAbreriatura(abreviaturaSitio);
            s.setIsActivo(sitioActivo);
            
            

            rt = sitiomanglarmodel.actualizarSitio(s);

            if (rt != null && rt.getCodigoError() == 0) {

                actualizaArchivosAreaGeograficas(rutaArchivoPicCargadoSitio, rutaArchivoKmlCargadoSitio, idSitio);

                FacesMessage msg = new FacesMessage("Se ha actulizado correctamente el sector", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);

            } else if (rt != null && rt.getCodigoError() == -1) {
                FacesMessage msg = new FacesMessage("No se ha actualizado correctamente el sector", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }

        } catch (Exception ex) {
            System.out.println("Error en el m�todo actualizarSitio del objeto EstacionesUIBeans:" + ex.getMessage());

        } finally {
            actualizarTablaSitiosAgregados();
        }
    }

    private String subirArchivo(String nombre, UploadedFile file, String pathfile) {
        String rutaArchivoCargado = null;
        try {
            co.org.invemar.library.siam.File f = new co.org.invemar.library.siam.File();
           
            f.copyFile(pathfile, file.getFileName(), file.getInputstream(), 5024);
            rutaArchivoCargado = pathfile + file.getFileName();
           

        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error cargando el archivo:" + ex.getMessage(), file.getFileName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            System.out.println("error:" + ex.getMessage());
        } finally {
            FacesMessage msg = new FacesMessage("Archivo cargado.Seleccione nuevamente el sector para actualizar la foto", file.getFileName() + "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        return rutaArchivoCargado;
    }

    private void renombraArchivos(String pathOrigen, File file, String nuevoNombre) {
        if (file != null) {
            co.org.invemar.library.siam.File f = new co.org.invemar.library.siam.File();
            String extension = f.getExtension(file.getName());
            file.renameTo(new File(pathOrigen + File.separator + nuevoNombre + "." + extension));
        } else {
            System.out.println("archivo nulo");
        }

    }

    private void copyFile(String ruta, String fileName, InputStream in) {
        try {
            OutputStream out = new FileOutputStream(new File(ruta + File.separator + fileName));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

            System.out.println("New file created!");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private String CreatePathFileArea(String path, String pathLoad, int idArea) {
        FinderFile ff = new FinderFile();
        String p = null;
        boolean r = ff.findFile(String.valueOf(idArea), new File(pathLoad));
        if (r) {
            p = path + idArea + "." + ff.getExtension();
        }
        return p;
    }

    public void CleanFieldFormUnidadManejo() {
        descripcionGeneralUnidadManejo = "";
        urlKmlFileUnidadManejo = "";
        urlPicFileUnidadManejo = "";
        isActivadaUnidadManejo = "Activado";
        categoriaUnidadManejo = "42";
    }

    public void cleanFieldFormParcela() {
        areaParcelas = 0;
        azimut = 0;
        fechaInstalacion = new Date();
        latitudParcela = 0;
        longitudParcela = 0;
        descripcionParcela = "";
        gradeLatitud = 0;
        minuteLatitud = 0;
        secondLatitud = 0;
        gradeLongitud = 0;
        minuteLongitud = 0;
        secondLongitud = 0;
        idTipoFisiograficos = "";

    }

    public void CleanFieldFormSitio() {
        nombreSitio = "";
        descripcionGeneralSitio = "";
        abreviaturaSitio = "";
        sitioActivo = "Activado";
        urlKmlFileSitio = "";
        urlPicFileSitio = "";
        auditoriaSitio = "";

    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEcoregion() {
        return ecoregion;
    }

    public void setEcoregion(String ecoregion) {
        this.ecoregion = ecoregion;
    }

    public String getToponimo() {
        return toponimo;
    }

    public void setToponimo(String toponimo) {
        this.toponimo = toponimo;
    }

    public String getEcoregiones() {
        return ecoregiones;
    }

    public void setEcoregiones(String ecoregiones) {
        this.ecoregiones = ecoregiones;
    }

    public Map<String, String> getToponimosMap() {
        return toponimosMap;
    }

    public void setToponimosMap(Map<String, String> toponimosMap) {
        this.toponimosMap = toponimosMap;
    }

    public Map<String, String> getEcorregionesMap() {
        return ecorregionesMap;
    }

    public void setEcorregionesMap(Map<String, String> ecorregionesMap) {
        this.ecorregionesMap = ecorregionesMap;
    }

    public String getAreageografica() {
        return areageografica;
    }

    public void setAreageografica(String areageografica) {
        this.areageografica = areageografica;
    }

    public String getCuencahidrografica() {
        return cuencahidrografica;
    }

    public void setCuencahidrografica(String cuencahidrografica) {
        this.cuencahidrografica = cuencahidrografica;
    }

    public Map<String, String> getAreasprotegidasMap() {
        return areasprotegidasMap;
    }

    public void setAreasprotegidasMap(Map<String, String> areasprotegidasMap) {
        this.areasprotegidasMap = areasprotegidasMap;
    }

    public Map<String, String> getZonasProtegidasMap() {
        return zonasProtegidasMap;
    }

    public void setZonasProtegidasMap(Map<String, String> zonasProtegidasMap) {
        this.zonasProtegidasMap = zonasProtegidasMap;
    }

    public String getZonaprotegidaCodigo() {
        return zonaprotegidaCodigo;
    }

    public void setZonaprotegidaCodigo(String zonaprotegidaCodigo) {
        this.zonaprotegidaCodigo = zonaprotegidaCodigo;
    }

    public String getEsAreaprotegida() {
        return EsAreaprotegida;
    }

    public void setEsAreaprotegida(String EsAreaprotegida) {
        this.EsAreaprotegida = EsAreaprotegida;
    }

    public String getStyleCmBoxAreasProtegidas() {
        return styleCmBoxAreasProtegidas;
    }

    public void setStyleCmBoxAreasProtegidas(String styleCmBoxAreasProtegidas) {
        this.styleCmBoxAreasProtegidas = styleCmBoxAreasProtegidas;
    }

    public Map<String, String> getListaautoridadesAmbientales() {
        return listaautoridadesAmbientales;
    }

    public void setListaautoridadesAmbientales(Map<String, String> listaautoridadesAmbientales) {
        this.listaautoridadesAmbientales = listaautoridadesAmbientales;
    }

    public String getAutoridadambiental() {
        return autoridadambiental;
    }

    public void setAutoridadambiental(String autoridadambiental) {
        this.autoridadambiental = autoridadambiental;
    }

    public String getImpactosConocidos() {
        return impactosConocidos;
    }

    public void setImpactosConocidos(String impactosConocidos) {
        this.impactosConocidos = impactosConocidos;
    }

    public Map<String, String> getListaimpactosconocidos() {
        return listaimpactosconocidos;
    }

    public void setListaimpactosconocidos(Map<String, String> listaimpactosconocidos) {
        this.listaimpactosconocidos = listaimpactosconocidos;
    }

    public String getIntervencionAntropica() {
        return intervencionAntropica;
    }

    public void setIntervencionAntropica(String intervencionAntropica) {
        this.intervencionAntropica = intervencionAntropica;
    }

    public Map<String, String> getListaiIntervencionAntropica() {
        return listaiIntervencionAntropica;
    }

    public void setListaiIntervencionAntropica(Map<String, String> listaiIntervencionAntropica) {
        this.listaiIntervencionAntropica = listaiIntervencionAntropica;
    }

    public String getActividaeconomica() {
        return actividaeconomica;
    }

    public void setActividaeconomica(String actividaeconomica) {
        this.actividaeconomica = actividaeconomica;
    }

    public Map<String, String> getListaiActividadesEconomicas() {
        return listaiActividadesEconomicas;
    }

    public void setListaiActividadesEconomicas(Map<String, String> listaiActividadesEconomicas) {
        this.listaiActividadesEconomicas = listaiActividadesEconomicas;
    }

    public String getDescripcionGeneral() {
        return descripcionGeneral;
    }

    public void setDescripcionGeneral(String descripcionGeneral) {
        this.descripcionGeneral = descripcionGeneral;
    }

    public String getSiteStyle() {
        return SiteStyle;
    }

    public void setSiteStyle(String SiteStyle) {
        this.SiteStyle = SiteStyle;
    }

    public SectorManglarVo getSector() {
        return sector;
    }

    public void setSector(SectorManglarVo areamanglar) {
        this.sector = areamanglar;
    }

    public List<SectorManglarVo> getListaAreasManglares() {
        return listaAreasManglares;
    }

    public void setListaAreasManglares(List<SectorManglarVo> listaAreasManglares) {
        this.listaAreasManglares = listaAreasManglares;
    }

    public ModeloDatosAreaManglar getModelodatosareamanglar() {
        return modelodatosareamanglar;
    }

    public void setModelodatosareamanglar(ModeloDatosAreaManglar modelodatosareamanglar) {
        this.modelodatosareamanglar = modelodatosareamanglar;
    }

    public int getExtension() {
        return extension;
    }

    public void setExtension(int extension) {
        this.extension = extension;
    }

    public String getIdCAR() {
        return idCAR;
    }

    public void setIdCAR(String idCAR) {
        this.idCAR = idCAR;
    }

    public String getRenderizarComboBoxAreasProtegidas() {
        return renderizarComboBoxAreasProtegidas;
    }

    public void setRenderizarComboBoxAreasProtegidas(String renderizarComboBoxAreasProtegidas) {
        this.renderizarComboBoxAreasProtegidas = renderizarComboBoxAreasProtegidas;
    }

    public String getRenderizarpoutputLabelAreasProtegidas() {
        return renderizarpoutputLabelAreasProtegidas;
    }

    public void setRenderizarpoutputLabelAreasProtegidas(String renderizarpoutputLabelAreasProtegidas) {
        this.renderizarpoutputLabelAreasProtegidas = renderizarpoutputLabelAreasProtegidas;
    }

    public String getRenderizarMessagesAreasProtegidas() {
        return renderizarMessagesAreasProtegidas;
    }

    public void setRenderizarMessagesAreasProtegidas(String renderizarMessagesAreasProtegidas) {
        this.renderizarMessagesAreasProtegidas = renderizarMessagesAreasProtegidas;
    }

    public String getMensajeRegistro() {
        return mensajeRegistro;
    }

    public void setMensajeRegistro(String mensajeRegistro) {
        this.mensajeRegistro = mensajeRegistro;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensajeRegistroStyle() {
        return mensajeRegistroStyle;
    }

    public void setMensajeRegistroStyle(String mensajeRegistroStyle) {
        this.mensajeRegistroStyle = mensajeRegistroStyle;
    }

    public String getAbreviaturaSector() {
        return abreviaturaSector;
    }

    public void setAbreviaturaSector(String abreviaturaSector) {
        this.abreviaturaSector = abreviaturaSector;
    }

    public String getURLInicio() {
        return URLInicio;
    }

    public void setURLInicio(String URLInicio) {
        this.URLInicio = URLInicio;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    public ZonasProtegidas getZonasprotegidas() {
        return zonasprotegidas;
    }

    public void setZonasprotegidas(ZonasProtegidas zonasprotegidas) {
        this.zonasprotegidas = zonasprotegidas;
    }

    public String getRutaAplicacion() {
        return rutaAplicacion;
    }

    public void setRutaAplicacion(String rutaAplicacion) {
        this.rutaAplicacion = rutaAplicacion;
    }

    public Properties getPropiedades() {
        return propiedades;
    }

    public void setPropiedades(Properties propiedades) {
        this.propiedades = propiedades;
    }

    public int getIdSector() {
        return idSector;
    }

    public void setIdSector(int idSector) {
        this.idSector = idSector;
    }

    public String getNombreSitio() {
        return nombreSitio;
    }

    public void setNombreSitio(String nombreSitio) {
        this.nombreSitio = nombreSitio;
    }

    public String getDescripcionGeneralSitio() {
        return descripcionGeneralSitio;
    }

    public void setDescripcionGeneralSitio(String descripcionGeneralSitio) {
        this.descripcionGeneralSitio = descripcionGeneralSitio;
    }

    public String getMensajeVentanaNuevoSitio() {
        return mensajeVentanaNuevoSitio;
    }

    public void setMensajeVentanaNuevoSitio(String mensajeVentanaNuevoSitio) {
        this.mensajeVentanaNuevoSitio = mensajeVentanaNuevoSitio;
    }

    public ModeloDatosSitio getModelodatositios() {
        return modelodatositios;
    }

    public void setModelodatositios(ModeloDatosSitio modelodatositios) {
        this.modelodatositios = modelodatositios;
    }

    public Sitio getSitio() {
        return sitio;
    }

    public void setSitio(Sitio sitio) {
        this.sitio = sitio;
    }

    public String getLabelbotonRegistroSitios() {
        return labelbotonRegistroSitios;
    }

    public void setLabelbotonRegistroSitios(String labelbotonRegistroSitios) {
        this.labelbotonRegistroSitios = labelbotonRegistroSitios;
    }

    public String getAbreviaturaSitio() {
        return abreviaturaSitio;
    }

    public void setAbreviaturaSitio(String abreviaturaSitio) {
        this.abreviaturaSitio = abreviaturaSitio;
    }

    public String getSitioActivo() {
        return sitioActivo;
    }

    public void setSitioActivo(String sitioActivo) {
        this.sitioActivo = sitioActivo;
    }

    public String getNombreUnidadManejo() {
        return nombreUnidadManejo;
    }

    public void setNombreUnidadManejo(String nombreUnidadManejo) {
        this.nombreUnidadManejo = nombreUnidadManejo;
    }

    public String getDescripcionGeneralUnidadManejo() {
        return descripcionGeneralUnidadManejo;
    }

    public void setDescripcionGeneralUnidadManejo(String descripcionGeneralUnidadManejo) {
        this.descripcionGeneralUnidadManejo = descripcionGeneralUnidadManejo;
    }

    public String getCategoriaUnidadManejo() {
        return categoriaUnidadManejo;
    }

    public void setCategoriaUnidadManejo(String categoriaUnidadManejo) {
        this.categoriaUnidadManejo = categoriaUnidadManejo;
    }

    public String getIsActivadaUnidadManejo() {
        return isActivadaUnidadManejo;
    }

    public void setIsActivadaUnidadManejo(String isActivadaUnidadManejo) {
        this.isActivadaUnidadManejo = isActivadaUnidadManejo;
    }

    public String getNombreCortoUnidadManejo() {
        return nombreCortoUnidadManejo;
    }

    public void setNombreCortoUnidadManejo(String nombreCortoUnidadManejo) {
        this.nombreCortoUnidadManejo = nombreCortoUnidadManejo;
    }

    public String getMansajeVentanaUnidadManejo() {
        return mensajeVentanaUnidadManejo;
    }

    public void setMensajeVentanaUnidadManejo(String mensajeVentanaUnidadManejo) {
        this.mensajeVentanaUnidadManejo = mensajeVentanaUnidadManejo;
    }

    public ModeloDatosUnidadManejo getUnidadmanejomodelodatos() {
        return unidadmanejomodelodatos;
    }

    public void setUnidadmanejomodelodatos(ModeloDatosUnidadManejo unidadmanejomodelodatos) {
        this.unidadmanejomodelodatos = unidadmanejomodelodatos;
    }

    public UnidadManejo getUnidadManejo() {
        return unidadManejo;
    }

    public void setUnidadManejo(UnidadManejo unidadManejo) {
        this.unidadManejo = unidadManejo;
    }

    public SectorManglaresModel getSectorManglarModel() {
        return sectorManglarModel;
    }

    public void setSectorManglarModel(SectorManglaresModel sectorManglarModel) {
        this.sectorManglarModel = sectorManglarModel;
    }

    public ArrayList<Sitio> getSitios() {
        return sitios;
    }

    public void setSitios(ArrayList<Sitio> sitios) {
        this.sitios = sitios;
    }

    public int getIdUnidadManejo() {
        return idUnidadManejo;
    }

    public void setIdUnidadManejo(int idUnidadManejo) {
        this.idUnidadManejo = idUnidadManejo;
    }

    public SitioManglarModel getSitiomanglarmodel() {
        return sitiomanglarmodel;
    }

    public void setSitiomanglarmodel(SitioManglarModel sitiomanglarmodel) {
        this.sitiomanglarmodel = sitiomanglarmodel;
    }

    public String getFormaParcela() {
        return formaParcela;
    }

    public void setFormaParcela(String formaParcela) {
        this.formaParcela = formaParcela;
    }

    public int getAzimut() {
        return azimut;
    }

    public void setAzimut(int azimut) {
        this.azimut = azimut;
    }

    public double getAreaParcela() {
        return areaParcela;
    }

    public void setAreaParcela(int areaParcela) {
        this.areaParcela = areaParcela;
    }

    public Date getFechaInstalacion() {
        return fechaInstalacion;
    }

    public void setFechaInstalacion(Date fechaInstalacion) {
        this.fechaInstalacion = fechaInstalacion;
    }

    public double getLatitudParcela() {
        return latitudParcela;
    }

    public void setLatitudParcela(double latitudParcela) {
        this.latitudParcela = latitudParcela;
    }

    public double getLongitudParcela() {
        return longitudParcela;
    }

    public void setLongitudParcela(double longitudParcela) {
        this.longitudParcela = longitudParcela;
    }

    public int getIdParcela() {
        return idParcela;
    }

    public void setIdParcela(int idParcela) {
        this.idParcela = idParcela;
    }

    public ModeloDatosParcela getModeloDatosParcela() {
        return modeloDatosParcela;
    }

    public void setModeloDatosParcela(ModeloDatosParcela modeloDatosParcela) {
        this.modeloDatosParcela = modeloDatosParcela;
    }

    public String getDescripcionParcela() {
        return descripcionParcela;
    }

    public void setDescripcionParcela(String descripcionParcela) {
        this.descripcionParcela = descripcionParcela;
    }

    public String getNombreCortoParcela() {
        return nombreCortoParcela;
    }

    public void setNombreCortoParcela(String nombreCortoParcela) {
        this.nombreCortoParcela = nombreCortoParcela;
    }

    public String getIsParcelaActiva() {
        return isParcelaActiva;
    }

    public void setIsParcelaActiva(String isParcelaActiva) {
        this.isParcelaActiva = isParcelaActiva;
    }

    public String getMensajeVentanaParcela() {
        return mensajeVentanaParcela;
    }

    public void setMensajeVentanaParcela(String mensajeVentanaParcela) {
        this.mensajeVentanaParcela = mensajeVentanaParcela;
    }

    public Parcela getParcela() {
        return parcela;
    }

    public void setParcela(Parcela parcela) {
        this.parcela = parcela;
    }

    public ArrayList<UnidadManejo> getUnidadesManejo() {
        return unidadesManejo;
    }

    public void setUnidadesManejo(ArrayList<UnidadManejo> unidadesManejo) {
        this.unidadesManejo = unidadesManejo;
    }

    public ArrayList<Parcela> getParcelas() {
        return parcelas;
    }

    public void setParcelas(ArrayList<Parcela> parcelas) {
        this.parcelas = parcelas;
    }

    public String getNombreParcela() {
        return nombreParcela;
    }

    public void setNombreParcela(String nombreParcela) {
        this.nombreParcela = nombreParcela;
    }

    public String getDisabledBotonActualizarSitio() {
        return disabledBotonActualizarSitio;
    }

    public void setDisabledBotonActualizarSitio(String disabledBotonActualizarSitio) {
        this.disabledBotonActualizarSitio = disabledBotonActualizarSitio;
    }

    public String getDisabledBotonActualizarUnidadManejo() {
        return disabledBotonActualizarUnidadManejo;
    }

    public void setDisabledBotonActualizarUnidadManejo(String disabledBotonActualizarUnidadManejo) {
        this.disabledBotonActualizarUnidadManejo = disabledBotonActualizarUnidadManejo;
    }

    public String getDisabledBotonActualizarParcela() {
        return disabledBotonActualizarParcela;
    }

    public void setDisabledBotonActualizarParcela(String disabledBotonActualizarParcela) {
        this.disabledBotonActualizarParcela = disabledBotonActualizarParcela;
    }

    public UnidadManejoModel getUnidadManejoModelo() {
        return unidadManejoModelo;
    }

    public void setUnidadManejoModelo(UnidadManejoModel unidadManejoModelo) {
        this.unidadManejoModelo = unidadManejoModelo;
    }

    public ParcelaModel getParcelaModelo() {
        return parcelaModelo;
    }

    public void setParcelaModelo(ParcelaModel parcelaModelo) {
        this.parcelaModelo = parcelaModelo;
    }

    public String getDisableBotonAgregarORegistrarSector() {
        return disableBotonAgregarORegistrarSector;
    }

    public void setDisableBotonAgregarORegistrarSector(String disableBotonAgregarORegistrarSector) {
        this.disableBotonAgregarORegistrarSector = disableBotonAgregarORegistrarSector;
    }

    public String getDisableBotonAgregarORegistrarNuevoSitio() {
        return disableBotonAgregarORegistrarNuevoSitio;
    }

    public void setDisableBotonAgregarORegistrarNuevoSitio(String disableBotonAgregarORegistrarNuevoSitio) {
        this.disableBotonAgregarORegistrarNuevoSitio = disableBotonAgregarORegistrarNuevoSitio;
    }

    public String getDisableBotonAgregarORegistrarUnidadManejo() {
        return disableBotonAgregarORegistrarUnidadManejo;
    }

    public void setDisableBotonAgregarORegistrarUnidadManejo(String disableBotonAgregarORegistrarUnidadManejo) {
        this.disableBotonAgregarORegistrarUnidadManejo = disableBotonAgregarORegistrarUnidadManejo;
    }

    public String getDisableBotonAgregarORegistrarParcela() {
        return disableBotonAgregarORegistrarParcela;
    }

    public void setDisableBotonAgregarORegistrarParcela(String disableBotonAgregarORegistrarParcela) {
        this.disableBotonAgregarORegistrarParcela = disableBotonAgregarORegistrarParcela;
    }

    public UploadedFile getKml() {
        return kml;
    }

    public void setKml(UploadedFile kml) {
        this.kml = kml;
    }

    public String getSectorActivo() {
        return sectorActivo;
    }

    public void setSectorActivo(String sectorActivo) {
        this.sectorActivo = sectorActivo;
    }

    public String getAzimutActivo() {
        return azimutActivo;
    }

    public void setAzimutActivo(String azimutActivo) {
        this.azimutActivo = azimutActivo;
    }

    public String getUrlKmlFileSector() {
        return urlKmlFileSector;
    }

    public void setUrlKmlFileSector(String urlKmlFileSector) {
        this.urlKmlFileSector = urlKmlFileSector;
    }

    public String getUrlPicFileSector() {
        return urlPicFileSector;
    }

    public void setUrlPicFileSector(String urlPicFileSector) {
        this.urlPicFileSector = urlPicFileSector;
    }

    public String getUrlKmlFileSitio() {
        return urlKmlFileSitio;
    }

    public void setUrlKmlFileSitio(String urlKmlFileSitio) {
        this.urlKmlFileSitio = urlKmlFileSitio;
    }

    public String getUrlPicFileSitio() {
        return urlPicFileSitio;
    }

    public void setUrlPicFileSitio(String urlPicFileSitio) {
        this.urlPicFileSitio = urlPicFileSitio;
    }

    public String getUrlKmlFileUnidadManejo() {
        return urlKmlFileUnidadManejo;
    }

    public void setUrlKmlFileUnidadManejo(String urlKmlFileUnidadManejo) {
        this.urlKmlFileUnidadManejo = urlKmlFileUnidadManejo;
    }

    public String getUrlPicFileUnidadManejo() {
        return urlPicFileUnidadManejo;
    }

    public void setUrlPicFileUnidadManejo(String urlPicFileUnidadManejo) {
        this.urlPicFileUnidadManejo = urlPicFileUnidadManejo;
    }

    public int getComponente() {
        return componente;
    }

    public void setComponente(int componente) {
        this.componente = componente;
    }

    public double getTopoAreaParcelasRegeneracion() {
        return topoAreaParcelasRegeneracion;
    }

    public void setTopoAreaParcelasRegeneracion(double topoAreaParcelasRegeneracion) {
        this.topoAreaParcelasRegeneracion = topoAreaParcelasRegeneracion;
    }

    public String getUnidadManejoSeleccionada() {
        return unidadManejoSeleccionada;
    }

    public void setUnidadManejoSeleccionada(String unidadManejoSeleccionada) {
        this.unidadManejoSeleccionada = unidadManejoSeleccionada;
    }

    public Map<String, String> getUnidadesManejoSeleccinable() {
        return unidadesManejoSeleccinable;
    }

    public void setUnidadesManejoSeleccinable(Map<String, String> unidadesManejoSeleccinable) {
        this.unidadesManejoSeleccinable = unidadesManejoSeleccinable;
    }

    public int getUnidadManejoActual() {
        return unidadManejoActual;
    }

    public void setUnidadManejoActual(int unidadManejoActual) {
        this.unidadManejoActual = unidadManejoActual;
    }

    public String getAuditoriaSitio() {
        return auditoriaSitio;
    }

    public void setAuditoriaSitio(String auditoriaSitio) {
        this.auditoriaSitio = auditoriaSitio;
    }

    public double getAreaParcelas() {
        return areaParcelas;
    }

    public void setAreaParcelas(double areaParcelas) {
        this.areaParcelas = areaParcelas;
    }

    public String getFormatoCoordenate() {
        return formatoCoordenate;
    }

    public void setFormatoCoordenate(String formatoCoordenate) {
        this.formatoCoordenate = formatoCoordenate;
    }

    public String getLatitudgradepanelActivo() {
        return latitudgradepanelActivo;
    }

    public void setLatitudgradepanelActivo(String latitudgradepanelActivo) {
        this.latitudgradepanelActivo = latitudgradepanelActivo;
    }

    public String getLongitudgradepanelActivo() {
        return longitudgradepanelActivo;
    }

    public void setLongitudgradepanelActivo(String longitudgradepanelActivo) {
        this.longitudgradepanelActivo = longitudgradepanelActivo;
    }

    public String getLatitudDecimalActivo() {
        return latitudDecimalActivo;
    }

    public void setLatitudDecimalActivo(String latitudDecimalActivo) {
        this.latitudDecimalActivo = latitudDecimalActivo;
    }

    public String getLongituDecimalActivo() {
        return longituDecimalActivo;
    }

    public void setLongituDecimalActivo(String longituDecimalActivo) {
        this.longituDecimalActivo = longituDecimalActivo;
    }

    public double getGradeLongitud() {
        return gradeLongitud;
    }

    public void setGradeLongitud(double gradeLongitud) {
        this.gradeLongitud = gradeLongitud;
    }

    public double getMinuteLongitud() {
        return minuteLongitud;
    }

    public void setMinuteLongitud(double minuteLongitud) {
        this.minuteLongitud = minuteLongitud;
    }

    public double getGradeLatitud() {
        return gradeLatitud;
    }

    public void setGradeLatitud(double gradeLatitud) {
        this.gradeLatitud = gradeLatitud;
    }

    public double getMinuteLatitud() {
        return minuteLatitud;
    }

    public void setMinuteLatitud(double minuteLatitud) {
        this.minuteLatitud = minuteLatitud;
    }

    public double getSecondLatitud() {
        return secondLatitud;
    }

    public void setSecondLatitud(double secondLatitud) {
        this.secondLatitud = secondLatitud;
    }

    public double getSecondLongitud() {
        return secondLongitud;
    }

    public void setSecondLongitud(double secondLongitud) {
        this.secondLongitud = secondLongitud;
    }

    public String getVigenciaLoc() {
        return vigenciaLoc;
    }

    public void setVigenciaLoc(String vigenciaLoc) {
        this.vigenciaLoc = vigenciaLoc;
    }

    public Map<String, String> getTiposFisiograficos() {
        return tiposFisiograficos;
    }

    public void setTiposFisiograficos(Map<String, String> tiposFisiograficos) {
        this.tiposFisiograficos = tiposFisiograficos;
    }

    public String getIdTipoFisiograficos() {
        return idTipoFisiograficos;
    }

    public void setIdTipoFisiograficos(String idTipoFisiograficos) {
        this.idTipoFisiograficos = idTipoFisiograficos;
    }

    public double getValueConverted() {
        return valueConverted;
    }

    public void setValueConverted(double valueConverted) {
        this.valueConverted = valueConverted;
    }

    public String getDisableBotonActualizarSector() {
        return disableBotonActualizarSector;
    }

    public void setDisableBotonActualizarSector(String disableBotonActualizarSector) {
        this.disableBotonActualizarSector = disableBotonActualizarSector;
    }

    public String getDisableBotonRegistrarSector() {
        return disableBotonRegistrarSector;
    }

    public void setDisableBotonRegistrarSector(String disableBotonRegistrarSector) {
        this.disableBotonRegistrarSector = disableBotonRegistrarSector;
    }

    public String getDisablecambiarunidadmanejo() {
        return disablecambiarunidadmanejo;
    }

    public void setDisablecambiarunidadmanejo(String disablecambiarunidadmanejo) {
        this.disablecambiarunidadmanejo = disablecambiarunidadmanejo;
    }

    public String getDisabledBotonRegistrarUnidadManejo() {
        return disabledBotonRegistrarUnidadManejo;
    }

    public void setDisabledBotonRegistrarUnidadManejo(String disabledBotonRegistrarUnidadManejo) {
        this.disabledBotonRegistrarUnidadManejo = disabledBotonRegistrarUnidadManejo;
    }

    public String getDisabledBotonRegistrarSitio() {
        return disabledBotonRegistrarSitio;
    }

    public void setDisabledBotonRegistrarSitio(String disabledBotonRegistrarSitio) {
        this.disabledBotonRegistrarSitio = disabledBotonRegistrarSitio;
    }

    public String getDisableBotonLoadSector() {
        return disableBotonLoadSector;
    }

    public void setDisableBotonLoadSector(String disableBotonLoadSector) {
        this.disableBotonLoadSector = disableBotonLoadSector;
    }

    public String getDisabledBotonLoadUnidadManejo() {
        return disabledBotonLoadUnidadManejo;
    }

    public void setDisabledBotonLoadUnidadManejo(String disabledBotonLoadUnidadManejo) {
        this.disabledBotonLoadUnidadManejo = disabledBotonLoadUnidadManejo;
    }

    public String getDisabledBotonLoadSitio() {
        return disabledBotonLoadSitio;
    }

    public void setDisabledBotonLoadSitio(String disabledBotonLoadSitio) {
        this.disabledBotonLoadSitio = disabledBotonLoadSitio;
    }

    public Map<String, String> getEntityMap() {
        return entityMap;
    }

    public void setEntityMap(TreeMap<String, String> entityMap) {
        this.entityMap = entityMap;
    }

    public String getEntitySector() {
        return entitySector;
    }

    public void setEntitySector(String entitySector) {
        this.entitySector = entitySector;
    }

    public String getEntityManagmentUnit() {
        return entityManagmentUnit;
    }

    public void setEntityManagmentUnit(String entityManagmentUnit) {
        this.entityManagmentUnit = entityManagmentUnit;
    }

    public String getEntityStation() {
        return entityStation;
    }

    public void setEntityStation(String entityStation) {
        this.entityStation = entityStation;
    }

    public String getEntityParcel() {
        return entityParcel;
    }

    public void setEntityParcel(String entityParcel) {
        this.entityParcel = entityParcel;
    }

   

    public String getIdCARValueInitial() {
        return idCARValueInitial;
    }

    public void setIdCARValueInitial(String idCARValueInitial) {
        this.idCARValueInitial = idCARValueInitial;
    }

    public String[] getSelectedEntityBySector() {
        return selectedEntityBySector;
    }

    public void setSelectedEntityBySector(String[] selectedEntityBySector) {
        this.selectedEntityBySector = selectedEntityBySector;
    }

    public String getDisableListAsociateEntity() {
        return disableListAsociateEntity;
    }

    public void setDisableListAsociateEntity(String disableListAsociateEntity) {
        this.disableListAsociateEntity = disableListAsociateEntity;
    }
}
