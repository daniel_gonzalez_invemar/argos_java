/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.manglares.model.ParcelaModel;
import co.org.invemar.argos.manglares.model.UnidadManejoModel;
import co.org.invemar.argos.manglares.vo.Parcela;
import co.org.invemar.argos.manglares.vo.UnidadManejo;
import co.org.invemar.util.ResultadoTransaccion;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author usrsig15
 */
@ManagedBean(name = "unidadManejo")
@SessionScoped
public class UnidadesManejoUIBeans {

    private int idSitio;

    private String idCAR;
    private int idproyecto;

    private int idUnidadManejo;
    private String nombreUnidadManejo;
    private String descripcionGeneralUnidadManejo;
    private String categoriaUnidadManejo;
    private String isActivadaUnidadManejo;
    private String nombreCortoUnidadManejo;
    private String mensajeVentanaUnidadManejo;
    private ModeloDatosUnidadManejo unidadmanejomodelodatos;
    private UnidadManejoModel unidadManejoModelo;
    private UnidadManejo unidadManejo;
    private ArrayList<UnidadManejo> unidadesManejo;
    private String disabledBotonActualizarUnidadManejo;
    private String mensajeVentanaNuevoSitio;

    ////////////////////////////////////////
    private String formaParcela;
    private int azimut;
    private int areaParcela;
    private Date fechaInstalacion;
    private double latitudParcela;
    private double longitudParcela;
    private int idParcela;
    private String descripcionParcela;
    private ModeloDatosParcela modeloDatosParcela;
    private String nombreCortoParcela;
    private String nombreParcela;
    private String isParcelaActiva;
    private String mensajeVentanaParcela;
    private Parcela parcela;
    private ArrayList<Parcela> parcelas;
    private String disabledBotonActualizarParcela;
    private ParcelaModel parcelaModelo;
    private String azimutActivo = "true";

    private String mansajeVentanaUnidadManejo = "";

    private String disableBotonAgregarORegistrarUnidadManejo;
    private String disableBotonAgregarORegistrarParcela;

    public UnidadesManejoUIBeans() {

       
    }

   
    public int getIdSitio() {
        return idSitio;
    }

    public void setIdSitio(int idSitio) {
        this.idSitio = idSitio;
    }

    public String getIdCAR() {
        return idCAR;
    }

    public void setIdCAR(String idCAR) {
        this.idCAR = idCAR;
    }

    public int getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(int idproyecto) {
        this.idproyecto = idproyecto;
    }

    public int getIdUnidadManejo() {
        return idUnidadManejo;
    }

    public void setIdUnidadManejo(int idUnidadManejo) {
        this.idUnidadManejo = idUnidadManejo;
    }

    public String getNombreUnidadManejo() {
        return nombreUnidadManejo;
    }

    public void setNombreUnidadManejo(String nombreUnidadManejo) {
        this.nombreUnidadManejo = nombreUnidadManejo;
    }

    public String getDescripcionGeneralUnidadManejo() {
        return descripcionGeneralUnidadManejo;
    }

    public void setDescripcionGeneralUnidadManejo(String descripcionGeneralUnidadManejo) {
        this.descripcionGeneralUnidadManejo = descripcionGeneralUnidadManejo;
    }

    public String getCategoriaUnidadManejo() {
        return categoriaUnidadManejo;
    }

    public void setCategoriaUnidadManejo(String categoriaUnidadManejo) {
        this.categoriaUnidadManejo = categoriaUnidadManejo;
    }

    public String getIsActivadaUnidadManejo() {
        return isActivadaUnidadManejo;
    }

    public void setIsActivadaUnidadManejo(String isActivadaUnidadManejo) {
        this.isActivadaUnidadManejo = isActivadaUnidadManejo;
    }

    public String getNombreCortoUnidadManejo() {
        return nombreCortoUnidadManejo;
    }

    public void setNombreCortoUnidadManejo(String nombreCortoUnidadManejo) {
        this.nombreCortoUnidadManejo = nombreCortoUnidadManejo;
    }

    public String getMensajeVentanaUnidadManejo() {
        return mensajeVentanaUnidadManejo;
    }

    public void setMensajeVentanaUnidadManejo(String mensajeVentanaUnidadManejo) {
        this.mensajeVentanaUnidadManejo = mensajeVentanaUnidadManejo;
    }

    public ModeloDatosUnidadManejo getUnidadmanejomodelodatos() {
        return unidadmanejomodelodatos;
    }

    public void setUnidadmanejomodelodatos(ModeloDatosUnidadManejo unidadmanejomodelodatos) {
        this.unidadmanejomodelodatos = unidadmanejomodelodatos;
    }

    public UnidadManejoModel getUnidadManejoModelo() {
        return unidadManejoModelo;
    }

    public void setUnidadManejoModelo(UnidadManejoModel unidadManejoModelo) {
        this.unidadManejoModelo = unidadManejoModelo;
    }

    public UnidadManejo getUnidadManejo() {
        return unidadManejo;
    }

    public void setUnidadManejo(UnidadManejo unidadManejo) {
        this.unidadManejo = unidadManejo;
    }

    public ArrayList<UnidadManejo> getUnidadesManejo() {
        return unidadesManejo;
    }

    public void setUnidadesManejo(ArrayList<UnidadManejo> unidadesManejo) {
        this.unidadesManejo = unidadesManejo;
    }

    public String getDisabledBotonActualizarUnidadManejo() {
        return disabledBotonActualizarUnidadManejo;
    }

    public void setDisabledBotonActualizarUnidadManejo(String disabledBotonActualizarUnidadManejo) {
        this.disabledBotonActualizarUnidadManejo = disabledBotonActualizarUnidadManejo;
    }

    public String getMensajeVentanaNuevoSitio() {
        return mensajeVentanaNuevoSitio;
    }

    public void setMensajeVentanaNuevoSitio(String mensajeVentanaNuevoSitio) {
        this.mensajeVentanaNuevoSitio = mensajeVentanaNuevoSitio;
    }

    public String getFormaParcela() {
        return formaParcela;
    }

    public void setFormaParcela(String formaParcela) {
        this.formaParcela = formaParcela;
    }

    public int getAzimut() {
        return azimut;
    }

    public void setAzimut(int azimut) {
        this.azimut = azimut;
    }

    public int getAreaParcela() {
        return areaParcela;
    }

    public void setAreaParcela(int areaParcela) {
        this.areaParcela = areaParcela;
    }

    public Date getFechaInstalacion() {
        return fechaInstalacion;
    }

    public void setFechaInstalacion(Date fechaInstalacion) {
        this.fechaInstalacion = fechaInstalacion;
    }

    public double getLatitudParcela() {
        return latitudParcela;
    }

    public void setLatitudParcela(double latitudParcela) {
        this.latitudParcela = latitudParcela;
    }

    public double getLongitudParcela() {
        return longitudParcela;
    }

    public void setLongitudParcela(double longitudParcela) {
        this.longitudParcela = longitudParcela;
    }

    public int getIdParcela() {
        return idParcela;
    }

    public void setIdParcela(int idParcela) {
        this.idParcela = idParcela;
    }

    public String getDescripcionParcela() {
        return descripcionParcela;
    }

    public void setDescripcionParcela(String descripcionParcela) {
        this.descripcionParcela = descripcionParcela;
    }

    public ModeloDatosParcela getModeloDatosParcela() {
        return modeloDatosParcela;
    }

    public void setModeloDatosParcela(ModeloDatosParcela modeloDatosParcela) {
        this.modeloDatosParcela = modeloDatosParcela;
    }

    public String getNombreCortoParcela() {
        return nombreCortoParcela;
    }

    public void setNombreCortoParcela(String nombreCortoParcela) {
        this.nombreCortoParcela = nombreCortoParcela;
    }

    public String getNombreParcela() {
        return nombreParcela;
    }

    public void setNombreParcela(String nombreParcela) {
        this.nombreParcela = nombreParcela;
    }

    public String getIsParcelaActiva() {
        return isParcelaActiva;
    }

    public void setIsParcelaActiva(String isParcelaActiva) {
        this.isParcelaActiva = isParcelaActiva;
    }

    public String getMensajeVentanaParcela() {
        return mensajeVentanaParcela;
    }

    public void setMensajeVentanaParcela(String mensajeVentanaParcela) {
        this.mensajeVentanaParcela = mensajeVentanaParcela;
    }

    public Parcela getParcela() {
        return parcela;
    }

    public void setParcela(Parcela parcela) {
        this.parcela = parcela;
    }

    public ArrayList<Parcela> getParcelas() {
        return parcelas;
    }

    public void setParcelas(ArrayList<Parcela> parcelas) {
        this.parcelas = parcelas;
    }

    public String getDisabledBotonActualizarParcela() {
        return disabledBotonActualizarParcela;
    }

    public void setDisabledBotonActualizarParcela(String disabledBotonActualizarParcela) {
        this.disabledBotonActualizarParcela = disabledBotonActualizarParcela;
    }

    public ParcelaModel getParcelaModelo() {
        return parcelaModelo;
    }

    public void setParcelaModelo(ParcelaModel parcelaModelo) {
        this.parcelaModelo = parcelaModelo;
    }

    public String getAzimutActivo() {
        return azimutActivo;
    }

    public void setAzimutActivo(String azimutActivo) {
        this.azimutActivo = azimutActivo;
    }

    public String getDisableBotonAgregarORegistrarUnidadManejo() {
        return disableBotonAgregarORegistrarUnidadManejo;
    }

    public void setDisableBotonAgregarORegistrarUnidadManejo(String disableBotonAgregarORegistrarUnidadManejo) {
        this.disableBotonAgregarORegistrarUnidadManejo = disableBotonAgregarORegistrarUnidadManejo;
    }

    public String getDisableBotonAgregarORegistrarParcela() {
        return disableBotonAgregarORegistrarParcela;
    }

    public void setDisableBotonAgregarORegistrarParcela(String disableBotonAgregarORegistrarParcela) {
        this.disableBotonAgregarORegistrarParcela = disableBotonAgregarORegistrarParcela;
    }

    public String getMansajeVentanaUnidadManejo() {
        return mansajeVentanaUnidadManejo;
    }

    public void setMansajeVentanaUnidadManejo(String mansajeVentanaUnidadManejo) {
        this.mansajeVentanaUnidadManejo = mansajeVentanaUnidadManejo;
    }

}
