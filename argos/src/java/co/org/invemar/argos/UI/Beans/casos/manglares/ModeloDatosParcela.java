/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.manglares.vo.Parcela;
import co.org.invemar.argos.manglares.vo.Sitio;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author usrsig15
 */
public class ModeloDatosParcela extends ListDataModel<Parcela> implements Serializable, SelectableDataModel<Parcela> {
   
    public ModeloDatosParcela(List<Parcela> parcelas) {         
        super(parcelas);
    }  
      
    @Override  
    public Parcela getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<Parcela> parcelas = (List<Parcela>) getWrappedData();  
          
        for(Parcela parcela : parcelas) {  
            String idParcela = String.valueOf(parcela.getId());
            if(idParcela.equals(rowKey))  
                return parcela;  
        }            
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Parcela parcela) {  
        return String.valueOf(parcela.getId());  
    }
    
}

