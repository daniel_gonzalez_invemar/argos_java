/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.geograficos.model.Toponimia;
import co.org.invemar.argos.manglares.model.ActividadesEconomicasModel;
import co.org.invemar.argos.manglares.model.FactoresForzanteModel;
import co.org.invemar.argos.manglares.model.PresionesModel;
import co.org.invemar.argos.manglares.model.ReportePresionesModel;
import co.org.invemar.argos.manglares.model.ReportesModel;
import co.org.invemar.argos.manglares.model.SectorManglaresModel;
import co.org.invemar.argos.manglares.model.SitioManglarModel;
import co.org.invemar.argos.manglares.vo.FactorForzante;
import co.org.invemar.argos.manglares.vo.Presion;
import co.org.invemar.argos.manglares.vo.Reporte;
import co.org.invemar.util.ResultadoTransaccion;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Daniel Gonzalez
 */
@ManagedBean(name = "presionBean")
@SessionScoped
public class PresionBean {

    private String idCar;
    private Date fecha;
    private Map<String, String> anios = new HashMap<String, String>();
    private String departamento;
    private Map<String, String> departamentos = new HashMap<String, String>();
    private String sector;
    private Map<String, String> sectores = new HashMap<String, String>();
    private String sitio;
    private Map<String, String> sitios = new HashMap<String, String>();

    private List<String> listaActividadesEconomicas;
    private Map<String, String> actividadesEconomicas = new HashMap<String, String>();

    private TreeNode raizArbol;

    private String factorForzante;
    private FactoresForzanteModel factorForzanteModel = new FactoresForzanteModel();

    private int intensidadFactorForzante;
    private int frecuenciaFactorForzante;

    private String intensidadDisturbio = "-1";
    private String frecuenciaDelDisturbio = "-1";

    private TreeNode[] nodosSeleccionados;
    //private TreeNode nodoSeleccionado;
    private ResultadoTransaccion resultadoTransaccion;
    
    private Nodos nodoSeleccionado; 
    
    private Reporte reporte;
    private List<Reporte> reportes; 
    
    private String actividadesEconomicasSeleccionadas;
    

    public PresionBean() {

        CargaCmbSitios();
        CargaCheckBoxActividadesEconomicas();
        CargaArbol();

        PresionesModel presionModel = new PresionesModel();
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        idCar = (String) session.getAttribute("entityid");
        //idProyecto= new Integer((String) session.getAttribute("projectid")).intValue();
       // int idProyecto=2239;
        //idCar = "CORPAMAG";
         System.out.println("ID CAR:"+idCar);
        if (idCar==null ) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("loginSigma.jsp");
            } catch (IOException ex) {
                Logger.getLogger(EstacionesUIBeans1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        

    }

    public void CargaCheckBoxActividadesEconomicas() {

        ActividadesEconomicasModel actividadesEconomicasModel = new ActividadesEconomicasModel();
        actividadesEconomicas = actividadesEconomicasModel.getTodosLasActividadesEconomicas();

    }

    public void CargaArbol() {

        raizArbol = new DefaultTreeNode("raiz", null);

        FactoresForzanteModel factorForzanteModel = new FactoresForzanteModel();
        ArrayList<FactorForzante> factoresForzantes = factorForzanteModel.getTodosLosFactoresNivel1();

        Iterator<FactorForzante> it = factoresForzantes.iterator();

        TreeNode ff1 = new DefaultTreeNode(new Nodos("Factores Forzante", "", " "), raizArbol);

        while (it.hasNext()) {
            FactorForzante factorForzante = it.next();
            TreeNode node = new DefaultTreeNode(new Nodos(factorForzante.getNombre(), String.valueOf(factorForzante.getId()), "Factor Forzante"), ff1);
            CargarHijosDelNodo(node);
        }

    }

    public void CargaCmbSitios() {
        Toponimia toponimia = new Toponimia();
        departamentos = toponimia.FillMapTodosToponimos("D");

    }

    public void CargaCmbSectorDelDepartamento() {
        SectorManglaresModel sectorModel = new SectorManglaresModel();

        if (departamento != null) {
            sectores = sectorModel.getTodosLosSectoresDelDepartamento(idCar, departamento);
        }
    }

    public void CargaCmbSitiosDelSector() {
        SitioManglarModel sitioModel = new SitioManglarModel();

        if (sector != null) {
            sitios = sitioModel.getTodosLosSitiosDelSector(sector, idCar);
        }
    }

    private void CargarHijosDelNodo(TreeNode nodo) {

        Nodos data = (Nodos) nodo.getData();
//        System.out.println("Id del node:" + data.getId());
        ArrayList<FactorForzante> factoresForzantes = factorForzanteModel.getTodosLosFactoresNivel2(Integer.parseInt(data.getId()));
        Iterator<FactorForzante> it = factoresForzantes.iterator();

        while (it.hasNext()) {
            FactorForzante factorForzante = it.next();
            TreeNode node = new DefaultTreeNode(new Nodos(factorForzante.getNombre(), String.valueOf(factorForzante.getId()), "Factor Forzante"), nodo);
            CargarPresionesAFactores(node);
            CargarHijosDelNodo(node);
        }
    }

    private void CargarPresionesAFactores(TreeNode factor) {
        Nodos data = (Nodos) factor.getData();
        //System.out.println("Id del padre++++++++++++++++:" + data.getId());
        PresionesModel presionModel = new PresionesModel();
        ArrayList<Presion> listapresiones = presionModel.getTodosLasPresionesDelFactor(Integer.parseInt(data.getId()));
        Iterator<Presion> it2 = listapresiones.iterator();
        while (it2.hasNext()) {
            Presion presion = it2.next();
            TreeNode node1 = new DefaultTreeNode(new Nodos(presion.getNombre(), String.valueOf(presion.getId()), "Presion"), factor);
        }

    }

    public void reportaPresiones(ActionEvent actionEvent) {
//        System.out.println("reportando presiones del bosque.....");
        ArrayList<String> listaPresiones = new ArrayList<String>();

        if (nodosSeleccionados != null && nodosSeleccionados.length != 0) {
            for (int i = 0; i < nodosSeleccionados.length; i++) {
                
                TreeNode treeNode = nodosSeleccionados[i];               
                Nodos padre = (Nodos)treeNode.getParent().getData();
                Nodos nodo = (Nodos) treeNode.getData();
                
               // System.out.println("Datos del nodo:" + "id:" + nodo.getId() + "Nombre:" + nodo.getNombre() + " tipo:" + nodo.getTipo()+" Intensidad:"+nodo.getIntensidad()+"Frecuencia:"+nodo.getFrecuencia());
                if (nodo.getTipo().equals("Presion")) {
                   listaPresiones.add(nodo.getId()+"-"+nodo.getFrecuencia()+"-"+nodo.getIntensidad()+"-"+padre.getId());
                }
                
            }

        }
       
        ReportePresionesModel reportePresionesModel = new ReportePresionesModel();
        String mensajeError="Debe seleccionar alguna presi�n,sector,departamento y sitio";
        if ((listaPresiones != null && listaPresiones.size() != 0) && !sector.equals("Seleccione") && (!sitio.equals("Seleccione") )) {
            resultadoTransaccion = reportePresionesModel.ingresaReporte(idCar ,fecha, departamento, sector, sitio, listaActividadesEconomicas, listaPresiones);
//            System.out.println("Resultado de la transacci�n:" + resultadoTransaccion.getCodigoError());
            mensajeError= resultadoTransaccion.getMensaje();
            if (resultadoTransaccion.getCodigoError()==-1) {
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Un error ha ocurrido:",mensajeError));  
            }else{
                ReportesModel reporteModel = new ReportesModel();   
                
                actividadesEconomicasSeleccionadas= reporteModel.getTodasLasActividadesEconomicasPorReporte(resultadoTransaccion.getCodigoError());
                
                reportes = reporteModel.getTablaDiagnostico(resultadoTransaccion.getCodigoError());
                
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Se ha guardado con exito el nuevo reporte.",""));               
               
            }
            
            
        }else
        {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Aviso:",mensajeError));
        }
        
        
        
        

    }

    public void listenerIntensidadDisturbio() {
//        System.out.println("ejectuando el combo box");
        if (intensidadDisturbio != "-1") {
//            System.out.println("ejectuando el combo box...2."+intensidadDisturbio);
//            System.out.println(intensidadDisturbio);
            nodoSeleccionado.setIntensidad(Integer.parseInt(intensidadDisturbio));
            
            
        }
    }

    public void listenerFrecuencia() {
//        System.out.println("ejectuando la frecuencia");
        if (intensidadDisturbio != "-1") {
//             System.out.println("Frecuencia del disturbio:"+frecuenciaDelDisturbio);
             nodoSeleccionado.setFrecuencia(Integer.parseInt(frecuenciaDelDisturbio));

        }
    }

    public void onNodeSelect(NodeSelectEvent event) {
//        System.out.println("seleccionando el nodo");
        nodoSeleccionado = (Nodos)event.getTreeNode().getData();
        if (nodoSeleccionado.getIntensidad()!=0 && nodoSeleccionado.getFrecuencia()!=0) {
            intensidadDisturbio= String.valueOf(nodoSeleccionado.getIntensidad());
            frecuenciaDelDisturbio = String.valueOf(nodoSeleccionado.getFrecuencia());
        }
       
        
//        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected", event.getTreeNode().toString());  
//        FacesContext.getCurrentInstance().addMessage(null, message);  
    }

    public Date getAnio() {
        return fecha;
    }

    public void setAnio(Date fecha) {
        this.fecha = fecha;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getFactorForzante() {
        return factorForzante;
    }

    public void setFactorForzante(String factorForzante) {
        this.factorForzante = factorForzante;
    }

    public int getIntensidadFactorForzante() {
        return intensidadFactorForzante;
    }

    public void setIntensidadFactorForzante(int intensidadFactorForzante) {
        this.intensidadFactorForzante = intensidadFactorForzante;
    }

    public int getFrecuenciaFactorForzante() {
        return frecuenciaFactorForzante;
    }

    public void setFrecuenciaFactorForzante(int frecuenciaFactorForzante) {
        this.frecuenciaFactorForzante = frecuenciaFactorForzante;
    }

    public Map<String, String> getAnios() {
        return anios;
    }

    public void setAnios(Map<String, String> anios) {
        this.anios = anios;
    }

    public Map<String, String> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(Map<String, String> departamentos) {
        this.departamentos = departamentos;
    }

    public Map<String, String> getSectores() {
        return sectores;
    }

    public void setSectores(Map<String, String> sectores) {
        this.sectores = sectores;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getIdCar() {
        return idCar;
    }

    public void setIdCar(String IdCar) {
        this.idCar = IdCar;
    }

    public String getSitio() {
        return sitio;
    }

    public void setSitio(String sitio) {
        this.sitio = sitio;
    }

    public Map<String, String> getSitios() {
        return sitios;
    }

    public void setSitios(Map<String, String> sitios) {
        this.sitios = sitios;
    }

    public List<String> getListaActividadesEconomicas() {
        return listaActividadesEconomicas;
    }

    public void setListaActividadesEconomicas(List<String> listaActividadesEconomicas) {
        this.listaActividadesEconomicas = listaActividadesEconomicas;
    }

    public Map<String, String> getActividadesEconomicas() {
        return actividadesEconomicas;
    }

    public void setActividadesEconomicas(Map<String, String> actividadesEconomicas) {
        this.actividadesEconomicas = actividadesEconomicas;
    }

    public TreeNode getRaizArbol() {
        return raizArbol;
    }

    public void setRaizArbol(TreeNode raizArbol) {
        this.raizArbol = raizArbol;
    }

    public FactoresForzanteModel getFactorForzanteModel() {
        return factorForzanteModel;
    }

    public void setFactorForzanteModel(FactoresForzanteModel factorForzanteModel) {
        this.factorForzanteModel = factorForzanteModel;
    }

    public TreeNode[] getNodosSeleccionados() {
        return nodosSeleccionados;
    }

    public void setNodosSeleccionados(TreeNode[] nodosSeleccionados) {
        this.nodosSeleccionados = nodosSeleccionados;
    }

    public String getIntensidadDisturbio() {
        return intensidadDisturbio;
    }

    public void setIntensidadDisturbio(String intensidadDisturbio) {
        this.intensidadDisturbio = intensidadDisturbio;
    }

    public String getfrecuenciaDelDisturbio() {
        return frecuenciaDelDisturbio;
    }

    public void setFrecuenciaDelDisturbio(String frecuenciaDelDisturbio) {
        this.frecuenciaDelDisturbio = frecuenciaDelDisturbio;
    }

//    public TreeNode getNodoSeleccionado() {
//        return nodoSeleccionado;
//    }
//
//    public void setNodoSeleccionado(TreeNode nodoSeleccionado) {
//        this.nodoSeleccionado = nodoSeleccionado;
//    }

    public ResultadoTransaccion getResultadoTransaccion() {
        return resultadoTransaccion;
    }

    public void setResultadoTransaccion(ResultadoTransaccion resultadoTransaccion) {
        this.resultadoTransaccion = resultadoTransaccion;
    }

    public Nodos getNodoSeleccionado() {      
        return nodoSeleccionado;
    }

    public void setNodoSeleccionado(Nodos nodoSeleccionado) {
        this.nodoSeleccionado = nodoSeleccionado;
    }

    public Reporte getReporte() {
        return reporte;
    }

    public void setReporte(Reporte reporte) {
        this.reporte = reporte;
    }

    public List<Reporte> getReportes() {
        return reportes;
    }

    public void setReportes(List<Reporte> reportes) {
        this.reportes = reportes;
    }

    public String getActividadesEconomicasSeleccionadas() {
        return actividadesEconomicasSeleccionadas;
    }

    public void setActividadesEconomicasSeleccionadas(String actividadesEconomicasSeleccionadas) {
        this.actividadesEconomicasSeleccionadas = actividadesEconomicasSeleccionadas;
    }

}
