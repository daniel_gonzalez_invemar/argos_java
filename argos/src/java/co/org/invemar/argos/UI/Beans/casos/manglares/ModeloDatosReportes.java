/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.manglares.vo.Reporte;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Daniel Gonzalez
 */
public class ModeloDatosReportes extends ListDataModel<Reporte> implements Serializable, SelectableDataModel<Reporte> {
   
    public ModeloDatosReportes(List<Reporte> reportes) {         
        super(reportes);
    }  
      
    @Override  
    public Reporte getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<Reporte> reportes = (List<Reporte>) getWrappedData();  
          
        for(Reporte reporte : reportes) {  
            if(reporte.getId().equals(rowKey))  
                return reporte;  
        }            
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Reporte reporte) {  
        return reporte.getId();  
    }
      
}
