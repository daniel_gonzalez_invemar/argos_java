/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.manglares.vo.SectorManglarVo;
import co.org.invemar.argos.manglares.vo.UnidadManejo;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author usrsig15
 */
public class ModeloDatosUnidadManejo extends ListDataModel<UnidadManejo> implements Serializable, SelectableDataModel<UnidadManejo> {
   
    public ModeloDatosUnidadManejo(List<UnidadManejo> lista) {         
        super(lista);
    }  
      
    @Override  
    public UnidadManejo getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<UnidadManejo> unidadesmanejo = (List<UnidadManejo>) getWrappedData();  
          
        for(UnidadManejo unidadmanejo : unidadesmanejo) {  
            String idUnidadManejo =String.valueOf(unidadmanejo.getId());
            if(idUnidadManejo.equals(rowKey))  
                return unidadmanejo;  
        }            
        return null;  
    }  
  
    @Override  
    public Object getRowKey(UnidadManejo unidadmanejo) {  
        return String.valueOf(unidadmanejo.getId());  
    }
}
