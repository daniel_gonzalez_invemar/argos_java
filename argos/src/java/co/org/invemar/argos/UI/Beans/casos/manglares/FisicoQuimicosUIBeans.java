/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.manglares.model.FisicoQuimicosModel;
import co.org.invemar.argos.manglares.model.Graficador;
import co.org.invemar.argos.manglares.model.SectorManglaresModel;
import co.org.invemar.argos.manglares.model.SitioManglarModel;
import java.util.HashMap;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author Daniel Gonzalez
 */
@ManagedBean(name = "fisicoquimicosBeans")
@SessionScoped
public class FisicoQuimicosUIBeans {
 
    private String sector;
    private Map<String, String> sectores = new HashMap<String, String>();  
    
    
    private String parcela;
    private Map<String, String> parcelas = new HashMap<String, String>();
  
    private String variable;
    private Map<String, String> variables = new HashMap<String, String>();
    
    private FisicoQuimicosModel fisicoQuimicosModel = new FisicoQuimicosModel();
    
    private CartesianChartModel linearModel; 

    public FisicoQuimicosUIBeans() {
        
        sectores = fisicoQuimicosModel.getSectores();
        variables =fisicoQuimicosModel.getVariables();
        linearModel = new CartesianChartModel();
        LineChartSeries prom2 = new LineChartSeries();
        prom2.set(0,0);
        prom2.setLabel("");
        linearModel.addSeries(prom2);
        
        
        
    }
    public void handleParcelas()
    {
        if (sector!=null) {
             parcelas = fisicoQuimicosModel.getParcelas(sector);
        }
    }
  
    public void handleGraficosFQ()
    {
        if ((sector !=null  && !sector.equals("Seleccione")) && (parcela!=null && !parcela.equals("Seleccione") ) ) {
            Graficador graficador = new Graficador();
            String[] param = variable.split("-");
            String var    = param[0];
            String nivel  = param[1];
            linearModel = graficador.getValoresFQ(parcela, var,nivel);
            
        }else
        {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Seleccione todos los parametros", ""));
        }
    }
    

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public Map<String, String> getSectores() {
        return sectores;
    }

    public void setSectores(Map<String, String> sectores) {
        this.sectores = sectores;
    }

    public String getParcela() {
        return parcela;
    }

    public void setParcela(String parcela) {
        this.parcela = parcela;
    }

    public Map<String, String> getParcelas() {
        return parcelas;
    }

    public void setParcelas(Map<String, String> parcelas) {
        this.parcelas = parcelas;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public Map<String, String> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, String> variables) {
        this.variables = variables;
    }

    public FisicoQuimicosModel getFisicoQuimicosModel() {
        return fisicoQuimicosModel;
    }

    public void setFisicoQuimicosModel(FisicoQuimicosModel fisicoQuimicosModel) {
        this.fisicoQuimicosModel = fisicoQuimicosModel;
    }

    public CartesianChartModel getLinearModel() {
        return linearModel;
    }

    public void setLinearModel(CartesianChartModel linearModel) {
        this.linearModel = linearModel;
    }
    
    
    
    
}
