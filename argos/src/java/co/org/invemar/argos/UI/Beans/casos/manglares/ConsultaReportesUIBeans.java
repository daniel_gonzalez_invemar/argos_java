/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.UI.Beans.casos.manglares.ModeloDatosReportes;
import co.org.invemar.argos.manglares.model.ReportePresionesModel;
import co.org.invemar.argos.manglares.model.ReportesModel;
import co.org.invemar.argos.manglares.vo.Presion;
import co.org.invemar.argos.manglares.vo.Reporte;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Daniel Gonzalez
 */
@ManagedBean(name = "consultareportes")
@SessionScoped
public class ConsultaReportesUIBeans {

    private String fecha;
    private Map<String, String> fechas = new HashMap<String, String>();

    private String departamento;
    private Map<String, String> departamentos = new HashMap<String, String>();

    private String sector;
    private Map<String, String> sectores = new HashMap<String, String>();

    private String sitio;
    private Map<String, String> sitios = new HashMap<String, String>();

    private ReportePresionesModel reporteModel = new ReportePresionesModel();
    private ReportesModel modeloReporte = new ReportesModel();

    private Reporte reporteSeleccionado;
    private ModeloDatosReportes modeloDatosReporte;
    private List<Reporte> diagnosticos; 
    private String actividadesEconomicasReportadas;    
   

    public ConsultaReportesUIBeans() {

        fechas = reporteModel.getTodasLasFechasReportes();
        ArrayList<Reporte> misreportes = new ArrayList<Reporte>();
        Reporte reporte = new Reporte();
        reporte.setEstados("");
        reporte.setId("");
        misreportes.add(reporte);
        modeloDatosReporte = new ModeloDatosReportes(misreportes);

    }

   

    public void handleDepartamentos() {
        if (fecha != null) {
            departamentos = reporteModel.getTodasLosDepartamentos(fecha);
        }
    }

    public void handleSectores() {
        if (departamento != null) {
            sectores = reporteModel.getTodasLosSectores(departamento);
        }
    }

    public void handleSitios() {
        if (sector != null) {
            sitios = reporteModel.getTodasLosSitios(sector);
        }
    }

    public void handleBuscarReportes() {

        if ((fecha != null && !fecha.equals("Seleccione")) && (departamento != null && !departamento.equals("Seleccione")) && (sector != null && !sector.equals("Seleccione") && (sitio != null && !sitio.equals("Seleccione")))) {
            ArrayList<Reporte> reportes = reporteModel.getTodaLosReportes(fecha, departamento, sector, sitio);
            modeloDatosReporte = new ModeloDatosReportes(reportes);
        }

    }

    public void onRowSelect(SelectEvent event) {
        int id = Integer.parseInt(((Reporte) event.getObject()).getId());
        FacesMessage msg = new FacesMessage("Reporte seleccioando:"+id);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        diagnosticos = modeloReporte.getTablaDiagnostico(id);       
        actividadesEconomicasReportadas = modeloReporte.getTodasLasActividadesEconomicasPorReporte(id);

    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Map<String, String> getFechas() {
        return fechas;
    }

    public void setFechas(Map<String, String> fechas) {
        this.fechas = fechas;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Map<String, String> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(Map<String, String> departamentos) {
        this.departamentos = departamentos;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public Map<String, String> getSectores() {
        return sectores;
    }

    public void setSectores(Map<String, String> sectores) {
        this.sectores = sectores;
    }

    public String getSitio() {
        return sitio;
    }

    public void setSitio(String sitio) {
        this.sitio = sitio;
    }

    public Map<String, String> getSitios() {
        return sitios;
    }

    public void setSitios(Map<String, String> sitios) {
        this.sitios = sitios;
    }

    public ReportePresionesModel getReporteModel() {
        return reporteModel;
    }

    public void setReporteModel(ReportePresionesModel reporteModel) {
        this.reporteModel = reporteModel;
    }

    public Reporte getReporteSeleccionado() {
        return reporteSeleccionado;
    }

    public void setReporteSeleccionado(Reporte reporteSeleccionado) {
        this.reporteSeleccionado = reporteSeleccionado;
    }

    public ModeloDatosReportes getModeloDatosReporte() {
        return modeloDatosReporte;
    }

    public void setModeloDatosReporte(ModeloDatosReportes modeloDatosReporte) {
        this.modeloDatosReporte = modeloDatosReporte;
    }

    public List<Reporte> getDiagnosticos() {
        return diagnosticos;
    }

    public void setDiagnosticos(List<Reporte> diagnosticos) {
        this.diagnosticos = diagnosticos;
    }

    public ReportesModel getModeloReporte() {
        return modeloReporte;
    }

    public void setModeloReporte(ReportesModel modeloReporte) {
        this.modeloReporte = modeloReporte;
    }

    public String getActividadesEconomicasReportadas() {
        return actividadesEconomicasReportadas;
    }

    public void setActividadesEconomicasReportadas(String actividadesEconomicasReportadas) {
        this.actividadesEconomicasReportadas = actividadesEconomicasReportadas;
    }

   
}
