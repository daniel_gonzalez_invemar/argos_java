/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.manglares.vo.SectorManglarVo;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author usrsig15
 */
public class ModeloDatosAreaManglar extends ListDataModel<SectorManglarVo> implements Serializable, SelectableDataModel<SectorManglarVo> {
   
    public ModeloDatosAreaManglar(List<SectorManglarVo> lista) {         
        super(lista);
    }  
      
    @Override  
    public SectorManglarVo getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<SectorManglarVo> areasmanglares = (List<SectorManglarVo>) getWrappedData();  
          
        for(SectorManglarVo areamanglar : areasmanglares) { 
            String id = String.valueOf(areamanglar.getId());
            if(id.equals(rowKey))  
                return areamanglar;  
        }            
        return null;  
    }  
  
    @Override  
    public Object getRowKey(SectorManglarVo areamanglar) {  
        return  String.valueOf(areamanglar.getId());  
    }
}
