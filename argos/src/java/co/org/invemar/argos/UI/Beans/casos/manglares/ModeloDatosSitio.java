/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.manglares.vo.SectorManglarVo;
import co.org.invemar.argos.manglares.vo.Sitio;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author usrsig15
 */
public class ModeloDatosSitio extends ListDataModel<Sitio> implements Serializable, SelectableDataModel<Sitio> {
   
    public ModeloDatosSitio(List<Sitio> lista) {         
        super(lista);
    }  
      
    @Override  
    public Sitio getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<Sitio> sitios = (List<Sitio>) getWrappedData();  
          
        for(Sitio sitio : sitios) {  
             String idSitio = String.valueOf(sitio.getId());
            if(idSitio.equals(rowKey))  
                return sitio;  
        }            
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Sitio sitio) {  
        return String.valueOf(sitio.getId());  
    }
    
}
