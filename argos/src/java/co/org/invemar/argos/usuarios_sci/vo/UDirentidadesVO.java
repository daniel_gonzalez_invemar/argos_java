/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.usuarios_sci.vo;

import java.io.Serializable;

/**
 *
 * @author usrsig15
 */
public class UDirentidadesVO implements Serializable {
    private String codigo=null;
    private String nombrein=null;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombrein() {
        return nombrein;
    }

    public void setNombrein(String nombrein) {
        this.nombrein = nombrein;
    }
    
    
    
    
}
