/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.usuarios_sci.model;

import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usrsig15
 */
public class UDirentidadesModel {
    
    public TreeMap<String,String> getAllEntity(String mainEntity){
        TreeMap<String,String>  entityList = new TreeMap<String, String>();
        Connection con= null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;      
        String query = "SELECT * FROM UDIRENTIDADES where codigo_in!=? order by nombre_in";
        ConnectionFactory conectionfactory = new ConnectionFactory();
        
        
                
        try {
            con = conectionfactory.createConnection("usuario");                  
            pstmt = con.prepareStatement(query);
            pstmt.setString(1,mainEntity);
            rs = pstmt.executeQuery();            
            while (rs.next()) {                             
               entityList.put(rs.getString("nombre_in"),rs.getString("codigo_in"));                
            }                
           
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el objeto UDirentidadesModel.getAllEntity:" + ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(UDirentidadesModel.class.getName()).log(Level.SEVERE, null, ex);
            }
           con=null;
        }           
       return entityList;
    }
    public Map<String,String> getAllCAR(){
      
        Map<String,String>  listaautoridadesAmbientales = new HashMap<String, String>();
        Connection con= null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;      
        String query = "SELECT * FROM UDIRENTIDADES where naturaleza_in=318 order by nombre_in";
        ConnectionFactory conectionfactory = new ConnectionFactory();
        
        try {
            con = conectionfactory.createConnection("usuario");                  
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();           
            while (rs.next()) {                             
               listaautoridadesAmbientales.put(rs.getString("nombre_in"),rs.getString("codigo_in"));                
            }          
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el objeto UDirentidadesModel  m�todo getAllCAR:" + ex.getMessage());

        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(UDirentidadesModel.class.getName()).log(Level.SEVERE, null, ex);
            }
           con=null;
        }           
       return listaautoridadesAmbientales;
    }    
}
