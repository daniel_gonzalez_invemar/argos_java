/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.usuarios_sci.model;

import co.org.invemar.library.siam.vo.DatosUsuarios;
import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author usrsig15
 */
public class DatosUsuariosModel {

    public DatosUsuarios getUsuarioEntidadVinculada(String idUsuario, String idProyecto) {

        ArrayList<DatosUsuariosModel> listadatosusuarios = new ArrayList<DatosUsuariosModel>();
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = "SELECT ID_ROL,\n"
                + "  DESCRIPCION,\n"
                + "  ID,\n"
                + "  USERNAME_EMAIL,\n"
                + "  PASSWORD,\n"
                + "  LLAVEPASSWORD,\n"
                + "  NOMBRE,\n"
                + "  APELLIDO,\n"
                + "  CODIGO_ATRIBUTO,\n"
                + "  VALOR_ASIGNADO,\n"
                + "  ACTIVO,\n"
                + "  ID_PROYECTO\n"
                + "FROM DATOS_USUARIOS where ID=? and id_proyecto=?";
        ConnectionFactory conectionfactory = new ConnectionFactory();
        DatosUsuarios du = new DatosUsuarios();                

        try {
            con = conectionfactory.createConnection("usuario");
            pstmt = con.prepareStatement(query);
            pstmt.setString(1,idUsuario);
            pstmt.setString(2,idProyecto);
            rs = pstmt.executeQuery();
            //System.out.println("query:"+query);
            if (rs.next()) {               
                du.setVinculado(rs.getString("valor_asignadoi"));       
                
            }
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }

        } catch (Exception ex) {
            System.out.println("Error en el objeto DatosUsuariosModel  m�todo getUsuarioEntidadVinculada:" + ex.getMessage());

        } finally {
            conectionfactory.closeConnectionInstancia(con);
        }
        return du;
    }
}
