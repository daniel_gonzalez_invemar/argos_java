/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.org.invemar.argos.usuarios_sci.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author usrsig15
 */
@Entity
@Table(name = "UDIRENTIDADES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Udirentidades.findAll", query = "SELECT u FROM Udirentidades u"),
    @NamedQuery(name = "Udirentidades.findByCodigoIn", query = "SELECT u FROM Udirentidades u WHERE u.codigoIn = :codigoIn"),
    @NamedQuery(name = "Udirentidades.findByNombreIn", query = "SELECT u FROM Udirentidades u WHERE u.nombreIn = :nombreIn"),
    @NamedQuery(name = "Udirentidades.findByDireccionIn", query = "SELECT u FROM Udirentidades u WHERE u.direccionIn = :direccionIn"),
    @NamedQuery(name = "Udirentidades.findByCiudadIn", query = "SELECT u FROM Udirentidades u WHERE u.ciudadIn = :ciudadIn"),
    @NamedQuery(name = "Udirentidades.findByDptoIn", query = "SELECT u FROM Udirentidades u WHERE u.dptoIn = :dptoIn"),
    @NamedQuery(name = "Udirentidades.findByPaisIn", query = "SELECT u FROM Udirentidades u WHERE u.paisIn = :paisIn"),
    @NamedQuery(name = "Udirentidades.findByCorreoeIn", query = "SELECT u FROM Udirentidades u WHERE u.correoeIn = :correoeIn"),
    @NamedQuery(name = "Udirentidades.findByWebsiteIn", query = "SELECT u FROM Udirentidades u WHERE u.websiteIn = :websiteIn"),
    @NamedQuery(name = "Udirentidades.findByPostalIn", query = "SELECT u FROM Udirentidades u WHERE u.postalIn = :postalIn"),
    @NamedQuery(name = "Udirentidades.findByApAereoIn", query = "SELECT u FROM Udirentidades u WHERE u.apAereoIn = :apAereoIn"),
    @NamedQuery(name = "Udirentidades.findByTelefonoIn", query = "SELECT u FROM Udirentidades u WHERE u.telefonoIn = :telefonoIn"),
    @NamedQuery(name = "Udirentidades.findByFaxIn", query = "SELECT u FROM Udirentidades u WHERE u.faxIn = :faxIn"),
    @NamedQuery(name = "Udirentidades.findByTitulodIn", query = "SELECT u FROM Udirentidades u WHERE u.titulodIn = :titulodIn"),
    @NamedQuery(name = "Udirentidades.findByDirectorIn", query = "SELECT u FROM Udirentidades u WHERE u.directorIn = :directorIn"),
    @NamedQuery(name = "Udirentidades.findByOficinaIn", query = "SELECT u FROM Udirentidades u WHERE u.oficinaIn = :oficinaIn"),
    @NamedQuery(name = "Udirentidades.findByContactonameIn", query = "SELECT u FROM Udirentidades u WHERE u.contactonameIn = :contactonameIn"),
    @NamedQuery(name = "Udirentidades.findByCargocontactoIn", query = "SELECT u FROM Udirentidades u WHERE u.cargocontactoIn = :cargocontactoIn"),
    @NamedQuery(name = "Udirentidades.findByCompartidoIn", query = "SELECT u FROM Udirentidades u WHERE u.compartidoIn = :compartidoIn"),
    @NamedQuery(name = "Udirentidades.findBySistemaIn", query = "SELECT u FROM Udirentidades u WHERE u.sistemaIn = :sistemaIn"),
    @NamedQuery(name = "Udirentidades.findByNaturalezaIn", query = "SELECT u FROM Udirentidades u WHERE u.naturalezaIn = :naturalezaIn"),
    @NamedQuery(name = "Udirentidades.findBySeccionalIn", query = "SELECT u FROM Udirentidades u WHERE u.seccionalIn = :seccionalIn")})

public class Udirentidades implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO_IN")
    private String codigoIn;
    @Basic(optional = false)
    @Column(name = "NOMBRE_IN")
    private String nombreIn;
    @Column(name = "DIRECCION_IN")
    private String direccionIn;
    @Column(name = "CIUDAD_IN")
    private String ciudadIn;
    @Column(name = "DPTO_IN")
    private String dptoIn;
    @Column(name = "PAIS_IN")
    private String paisIn;
    @Column(name = "CORREOE_IN")
    private String correoeIn;
    @Column(name = "WEBSITE_IN")
    private String websiteIn;
    @Column(name = "POSTAL_IN")
    private String postalIn;
    @Column(name = "AP_AEREO_IN")
    private String apAereoIn;
    @Column(name = "TELEFONO_IN")
    private String telefonoIn;
    @Column(name = "FAX_IN")
    private String faxIn;
    @Column(name = "TITULOD_IN")
    private String titulodIn;
    @Column(name = "DIRECTOR_IN")
    private String directorIn;
    @Column(name = "OFICINA_IN")
    private String oficinaIn;
    @Column(name = "CONTACTONAME_IN")
    private String contactonameIn;
    @Column(name = "CARGOCONTACTO_IN")
    private String cargocontactoIn;
    @Column(name = "COMPARTIDO_IN")
    private String compartidoIn;
    @Column(name = "SISTEMA_IN")
    private String sistemaIn;
    @Column(name = "NATURALEZA_IN")
    private Short naturalezaIn;
    @Column(name = "SECCIONAL_IN")
    private String seccionalIn;
    @Lob
    @Column(name = "LOGO_ENTIDAD")
    private Serializable logoEntidad;

    public Udirentidades() {
    }

    public Udirentidades(String codigoIn) {
        this.codigoIn = codigoIn;
    }

    public Udirentidades(String codigoIn, String nombreIn) {
        this.codigoIn = codigoIn;
        this.nombreIn = nombreIn;
    }

    public String getCodigoIn() {
        return codigoIn;
    }

    public void setCodigoIn(String codigoIn) {
        this.codigoIn = codigoIn;
    }

    public String getNombreIn() {
        return nombreIn;
    }

    public void setNombreIn(String nombreIn) {
        this.nombreIn = nombreIn;
    }

    public String getDireccionIn() {
        return direccionIn;
    }

    public void setDireccionIn(String direccionIn) {
        this.direccionIn = direccionIn;
    }

    public String getCiudadIn() {
        return ciudadIn;
    }

    public void setCiudadIn(String ciudadIn) {
        this.ciudadIn = ciudadIn;
    }

    public String getDptoIn() {
        return dptoIn;
    }

    public void setDptoIn(String dptoIn) {
        this.dptoIn = dptoIn;
    }

    public String getPaisIn() {
        return paisIn;
    }

    public void setPaisIn(String paisIn) {
        this.paisIn = paisIn;
    }

    public String getCorreoeIn() {
        return correoeIn;
    }

    public void setCorreoeIn(String correoeIn) {
        this.correoeIn = correoeIn;
    }

    public String getWebsiteIn() {
        return websiteIn;
    }

    public void setWebsiteIn(String websiteIn) {
        this.websiteIn = websiteIn;
    }

    public String getPostalIn() {
        return postalIn;
    }

    public void setPostalIn(String postalIn) {
        this.postalIn = postalIn;
    }

    public String getApAereoIn() {
        return apAereoIn;
    }

    public void setApAereoIn(String apAereoIn) {
        this.apAereoIn = apAereoIn;
    }

    public String getTelefonoIn() {
        return telefonoIn;
    }

    public void setTelefonoIn(String telefonoIn) {
        this.telefonoIn = telefonoIn;
    }

    public String getFaxIn() {
        return faxIn;
    }

    public void setFaxIn(String faxIn) {
        this.faxIn = faxIn;
    }

    public String getTitulodIn() {
        return titulodIn;
    }

    public void setTitulodIn(String titulodIn) {
        this.titulodIn = titulodIn;
    }

    public String getDirectorIn() {
        return directorIn;
    }

    public void setDirectorIn(String directorIn) {
        this.directorIn = directorIn;
    }

    public String getOficinaIn() {
        return oficinaIn;
    }

    public void setOficinaIn(String oficinaIn) {
        this.oficinaIn = oficinaIn;
    }

    public String getContactonameIn() {
        return contactonameIn;
    }

    public void setContactonameIn(String contactonameIn) {
        this.contactonameIn = contactonameIn;
    }

    public String getCargocontactoIn() {
        return cargocontactoIn;
    }

    public void setCargocontactoIn(String cargocontactoIn) {
        this.cargocontactoIn = cargocontactoIn;
    }

    public String getCompartidoIn() {
        return compartidoIn;
    }

    public void setCompartidoIn(String compartidoIn) {
        this.compartidoIn = compartidoIn;
    }

    public String getSistemaIn() {
        return sistemaIn;
    }

    public void setSistemaIn(String sistemaIn) {
        this.sistemaIn = sistemaIn;
    }

    public Short getNaturalezaIn() {
        return naturalezaIn;
    }

    public void setNaturalezaIn(Short naturalezaIn) {
        this.naturalezaIn = naturalezaIn;
    }

    public String getSeccionalIn() {
        return seccionalIn;
    }

    public void setSeccionalIn(String seccionalIn) {
        this.seccionalIn = seccionalIn;
    }

    public Serializable getLogoEntidad() {
        return logoEntidad;
    }

    public void setLogoEntidad(Serializable logoEntidad) {
        this.logoEntidad = logoEntidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoIn != null ? codigoIn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Udirentidades)) {
            return false;
        }
        Udirentidades other = (Udirentidades) object;
        if ((this.codigoIn == null && other.codigoIn != null) || (this.codigoIn != null && !this.codigoIn.equals(other.codigoIn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.org.invemar.argos.usuarios_sci.model.Udirentidades[ codigoIn=" + codigoIn + " ]";
    }
    
}
