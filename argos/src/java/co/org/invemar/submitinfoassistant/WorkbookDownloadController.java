package co.org.invemar.submitinfoassistant;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.jdom.Document;

import co.org.invemar.util.ConnectionFactory;
import co.org.invemar.util.UtilFunctions;
import co.org.invemar.util.RequestParameterManager;
import co.org.invemar.workbook.Area;
import co.org.invemar.workbook.Entity;
import co.org.invemar.workbook.EntityManager;
import co.org.invemar.workbook.Property;
import co.org.invemar.workbook.Sheet;
import co.org.invemar.workbook.WorkbookManager;
import co.org.invemar.workbook.WhereFilter;

/**
 *
 * @author Victor Adri�n Santaf� Torres.
 *
 *
 */
public class WorkbookDownloadController {

    private javax.servlet.ServletContext servletContext;
    //schema
    private String schema = null;
    //thematic
    private int thematicId;
    //inforef pathes
    private String wbXmlPath = null;
    private String infoRefXmlPath = null;
    //new inforef request path
    //private String infoRefXmlPath = null;
    //xml documents
    private Document infoRefWorkbookDoc = null;
    private Document infoRefDoc = null;
    //content managers
    WorkbookManager infoRefWbman = null;
    EntityManager inforefman = null;
    //util variables
    private UtilFunctions util = new UtilFunctions();
    //workbook to return
    private HSSFWorkbook workBookResult = null;
    //DB connection variables
    private ConnectionFactory cFactory;
    private Connection conn = null;
    //Request Parameter Manager
    private RequestParameterManager rpManager = null;

    /*Constructor*/
    public WorkbookDownloadController(String schema, int thematicId, RequestParameterManager rpManager, javax.servlet.ServletContext servletContext) {

        this.rpManager = rpManager;
        this.servletContext = servletContext;
        this.schema = schema;
        this.thematicId = thematicId;
    }

    public HSSFWorkbook createInfoRefWorkbook() {

        if (loadInfoRefFiles()) {

            if (createInfoRefFilesObjects()) {

                try {
                    processInfoRefWorkbook();
                } catch (Exception e) {
                    System.out.println("I could not create the worbook : createWorkbook() : " + e);
                    workBookResult = null;
                }

            } else {
                workBookResult = null;
            }
        } else {
            workBookResult = null;
        }

        return workBookResult;
    }

    /*creates a workbook for users to add new info ref*/
    public HSSFWorkbook createAddNewInfoRefWorkbook() {

        if (loadAddNewInfoRefFiles()) {

            if (createInfoRefFilesObjects()) {

                try {
                    processInfoRefWorkbook();
                } catch (Exception e) {
                    System.out.println("I could not create the worbook : createWorkbook() : " + e);
                    workBookResult = null;
                }

            } else {
                workBookResult = null;
            }
        } else {
            workBookResult = null;
        }

        return workBookResult;
    }

    private void processInfoRefWorkbook() throws SQLException {

        //create workbook
        this.workBookResult = new HSSFWorkbook();

        //create temp sheet
        HSSFSheet wbSheet;

        //get all sheets from workbook
        List<Sheet> sheets = this.infoRefWbman.getSheets();

        for (Sheet sheetObj : sheets) {
            //create new sheet
            wbSheet = this.workBookResult.createSheet(sheetObj.getName());

            createInfoRefSheet(sheetObj, wbSheet);

        }

    }

    /*Creates a sheet for inforef*/
    private void createInfoRefSheet(Sheet sheetObj, HSSFSheet wbSheet) throws SQLException {

        //get external id areas
        List<Area> Areas = sheetObj.getAreasList();
        List<Property> properties = null;
        String sqlSentence = null;

        Entity entity = null;
        HSSFCell cell = null;
        HSSFCellStyle style = this.workBookResult.createCellStyle();

        //connection variables
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        int columnsCount = 0;
        int titlesColumnsCount = 0;

        //create row for titles
        HSSFRow overTitleRow = wbSheet.createRow(0);
        HSSFRow titleRow = wbSheet.createRow(1);
        HSSFRow infoRefRow = null;
        int infoRefRowsCount = 2;

        //process by area
        for (Area area : Areas) {

            //GET ENTITY BY ID from inforef manager
            entity = inforefman.getEntityById(area.getExternalId());

            properties = entity.getAttributes();//now we have the entity's properties

            //if title should be shown
            if (area.isShowTitle()) {

                //create over title cell
                cell = overTitleRow.createCell(columnsCount);

                //enter over title cell value
                cell.setCellValue(new HSSFRichTextString(entity.getDescription().toUpperCase()));

                //get and set over title cell Style
                cell.setCellStyle(getInfoRefOverTitleHeaderStyle());

                //merge properties columns on row 0 for title

                //wbSheet.addMergedRegion(new CellRangeAddress(0, columnsCount, 0,
                //  (properties.size() - 1 + columnsCount)));

                infoRefRowsCount = 2; //index row from where inforef is going to be
            } else {
                titleRow = overTitleRow;
                infoRefRowsCount = 1;
            }

            titlesColumnsCount = columnsCount;//get temp columncount value and use it for putting title 

            //now put titles
            for (Property property : properties) {

                //create title cell
                cell = titleRow.createCell(titlesColumnsCount++);

                //enter title cell value
                cell.setCellValue(new HSSFRichTextString(property.getAlias().toUpperCase()));

                //get and set title cell Style
                cell.setCellStyle(getInfoRefTitleHeaderStyle());

            }

            // fill some rows with reference information

            if (infoRefWbman.isLoadInfoRef()) {
                try {

                    //prepare SQL sentence
                    sqlSentence = entity.getSql();

                    if (area.getWhereCriteria() != null) {
                        sqlSentence = sqlSentence + " " + area.getWhereCriteria();
                    } else {
                        sqlSentence = sqlSentence + " " + entity.getWhereCriteria();
                    }

                    if (area.getOrderByCriteria() != null) {
                        sqlSentence = sqlSentence + " " + area.getOrderByCriteria();
                    } else {
                        sqlSentence = sqlSentence + " " + entity.getOrderByCriteria();
                    }

                    //System.out.println(sqlSentence);

                    //get dbconnection
                    cFactory = new ConnectionFactory();
                    conn = cFactory.createConnection(entity.getSchema());
                    pstmt = conn.prepareStatement(sqlSentence);

                     System.out.println("sqlSentence:"+sqlSentence);
                    //configure where parameters to filter the query
                    this.setStatementIndexValues(pstmt, area.getWhereFilterTypeList());

                    rs = pstmt.executeQuery();

                    while (rs.next()) {//keep in mind each result set is a new row

                        titlesColumnsCount = columnsCount;
                        //creates new row at index 2 (third row)
                        infoRefRow=wbSheet.getRow(infoRefRowsCount);
                        infoRefRow = (infoRefRow==null)?wbSheet.createRow(infoRefRowsCount):infoRefRow;
                        infoRefRowsCount++;
                        //for each property (mmm titles values :) ) we create new cell and put value
                        //we are moving left to right
                        for (Property property : properties) {

                            //creates cell
                            cell = infoRefRow.createCell(titlesColumnsCount++);

                            //enter cell value
                           // System.out.println("properties:"+property.getAlias());
                            if (property.getType().equalsIgnoreCase("int")
                                    || property.getType().equalsIgnoreCase("double")) {
                                cell.setCellValue(rs.getInt(property.getAlias()));


                            } else if (property.getType().equalsIgnoreCase("String")) {
                                cell.setCellValue(new HSSFRichTextString(rs
                                        .getString(property.getAlias())));

                            }
                            if (property.getType().equalsIgnoreCase("Date")) {
                                style.setDataFormat(HSSFDataFormat
                                        .getBuiltinFormat("m/d/yy"));
                                cell.setCellValue(rs.getDate(property.getAlias()));

                            }

                        }

                    }

                } catch (Exception e) {
                    System.out.println("I failed when creating inforef sheet : createInfoRefSheet(...) : " + e.getMessage());
                    e.printStackTrace();
                } finally {
                    if (rs != null) {
                        rs.close();
                    }
                    if (pstmt != null) {
                        pstmt.close();
                    }
                    if (conn != null) {
                        ConnectionFactory.closeConnection(conn);
                    }

                }
            }

            //increase columns count as long as properties list size plus 1 to leave a blank space between inforef
            columnsCount = columnsCount + properties.size() + 1;
        }

    }

    /*set the statement index value*/
    private void setStatementIndexValues(PreparedStatement pstm, List<WhereFilter> filterList) throws SQLException {

        String filterName = null;
        String filterType = null;
        String filterValue = null;
        int index = 1;

        for (WhereFilter filter : filterList) {
            filterName = filter.getName();
            filterType = filter.getType();
            filterValue = this.rpManager.getRequestParameterValueByName(filterName);

            if (filterType.equals("int")) {
                pstm.setInt(index, Integer.parseInt(filterValue));
            } else if (filterType.equals("string")) {
                pstm.setString(index, filterValue);
            }
            index++;
        }

    }

    /*holds the style options for inforef sheet header*/
    private HSSFCellStyle getInfoRefOverTitleHeaderStyle() {

        HSSFCellStyle style = this.workBookResult.createCellStyle();
        style.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        HSSFFont font = this.workBookResult.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        style.setFont(font);

        return style;
    }

    /*holds the style options for inforef sheet header*/
    private HSSFCellStyle getInfoRefTitleHeaderStyle() {

        HSSFCellStyle style = this.workBookResult.createCellStyle();

        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBottomBorderColor(HSSFColor.BLACK.index);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setLeftBorderColor(HSSFColor.BLACK.index);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setRightBorderColor(HSSFColor.BLACK.index);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setTopBorderColor(HSSFColor.BLACK.index);

        HSSFFont font = this.workBookResult.createFont();
        font.setFontHeightInPoints((short) 8);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        style.setFont(font);

        return style;

    }

    /*get file path and get xml documents*/
    private boolean loadInfoRefFiles() {

        boolean result = true;

        String schemaPath = servletContext.getRealPath("WEB-INF") + File.separator + "xmlconfig" + File.separator + this.schema + File.separator + this.thematicId + File.separator;

        wbXmlPath = schemaPath + "inforefworkbook.xml";
        infoRefXmlPath = schemaPath + "inforef.xml";

        try {
            this.infoRefWorkbookDoc = util.readXML(wbXmlPath);
            this.infoRefDoc = util.readXML(infoRefXmlPath);

        } catch (Exception e) {
            result = false;
            System.out.println("Exception: I couldn't find some books ; " + e);
        }

        return result;
    }

    /*create manager objects from xml documents*/
    private boolean createInfoRefFilesObjects() {

        boolean result = true;

        try {
            infoRefWbman = new WorkbookManager(this.infoRefWorkbookDoc);
            inforefman = new EntityManager(this.infoRefDoc);

            /*Entity test = inforefman.getEntityById(18);
			
             System.out.println("id:"+test.getId()+" name:"+test.getName()+" description:"+test.getDescription());*/

        } catch (Exception e) {
            result = false;
            System.out.println("Exception: I couldn't create some books ; " + e);
            e.printStackTrace();
        }

        return result;

    }

    /*get file path and get xml documents*/
    private boolean loadAddNewInfoRefFiles() {

        boolean result = true;

        String schemaPath = servletContext.getRealPath("WEB-INF") + File.separator + "xmlconfig" + File.separator + this.schema + File.separator + this.thematicId + File.separator;

        wbXmlPath = schemaPath + "newinforefworkbook.xml";
        infoRefXmlPath = schemaPath + "newinforef.xml";

        try {
            this.infoRefWorkbookDoc = util.readXML(wbXmlPath);
            this.infoRefDoc = util.readXML(infoRefXmlPath);

        } catch (Exception e) {
            result = false;
            System.out.println("Exception: I couldn't find some books ; " + e);
        }

        return result;
    }
}
