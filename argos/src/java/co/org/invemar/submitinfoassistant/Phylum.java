package co.org.invemar.submitinfoassistant;

public class Phylum {
	private int codPhylum;
	private String phylum;
	
	
	/**
	 * @param codPhylum
	 * @param phylum
	 */
	public Phylum(int codPhylum, String phylum) {
		
		this.codPhylum = codPhylum;
		this.phylum = phylum;
	}
	/**
	 * @return the codPhylum
	 */
	public int getCodPhylum() {
		return codPhylum;
	}
	/**
	 * @param codPhylum the codPhylum to set
	 */
	public void setCodPhylum(int codPhylum) {
		this.codPhylum = codPhylum;
	}
	/**
	 * @return the phylum
	 */
	public String getPhylum() {
		return phylum;
	}
	/**
	 * @param phylum the phylum to set
	 */
	public void setPhylum(String phylum) {
		this.phylum = phylum;
	}

}
