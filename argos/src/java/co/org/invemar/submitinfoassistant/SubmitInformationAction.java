package co.org.invemar.submitinfoassistant;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jdom.Document;
import org.jdom.Element;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.org.invemar.user.UserAdminInfoRequestController;
import co.org.invemar.user.UserPermission;
import co.org.invemar.util.RequestParameterManager;
import co.org.invemar.util.UtilFunctions;
import javax.servlet.ServletContext;

/**
 *
 * @author Victor Adri�n Santaf� Torres.
 *
 *
 */
public class SubmitInformationAction {

    private SubmitInformationInfoRequestController infoRequest = new SubmitInformationInfoRequestController(null);

    private ServletContext servletContext;
    private UtilFunctions util = new UtilFunctions();
    private String xmlConfigPath = null;

    private HttpSession session = null;

    private String username = null;
    private UserPermission userp = null;

    public SubmitInformationAction(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext)
            throws ServletException, IOException {

        response.setContentType("text/text; charset=ISO-8859-1");
        response.addHeader("Expires", "0");
        response.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.addHeader("Pragma", "no-cache");

        this.servletContext = servletContext;

        session = request.getSession();

        try {
            username = session.getAttribute("username").toString();
            userp = (UserPermission) session.getAttribute("userpermission");
        } catch (Exception e) {
            username = null;
        }

        if (username == null) {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            return;
        } else {
            //check user permission
            UserPermission userp = (UserPermission) session.getAttribute("userpermission");

            if (!userp.couldUser(UserPermission.INGRESAR_DATOS) && !userp.couldUser(UserPermission.ACTUALIZAR_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) && !userp.couldUser(UserPermission.ACTUALIZAR_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                return;
            }
        }

        xmlConfigPath = servletContext.getRealPath("xmlconfig");

        String action = request.getParameter("action");
        JSONArray result = null;
        RequestParameterManager rpManager = null;
        
       

        if (action.equals("getThematics")) {

            try {
                int projectId = Integer.parseInt(session.getAttribute("projectid").toString());

                result = infoRequest.getThematicsByProject(projectId);

                if (result != null) {
                    response.setContentType("application/json");
                    response.getWriter().write(result.toString());
                } else {
                    response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
            } catch (Exception e) {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }

        } else if (action.equals("getInfoRefDoc")) {

            //create a new request parameter manager to hold all parameter - specially filters ;)
            rpManager = new RequestParameterManager(request);

            String schema = rpManager.getRequestParameterValueByName("schema");//request.getParameter("schema");
            int thematicId = Integer.parseInt(rpManager.getRequestParameterValueByName("thematicid"));

            this.getInfoRefDoc(schema, thematicId, rpManager, response);

        } else if (action.equals("getFilterInfo")) {

            System.out.println(request.getParameter("schema") + request.getParameter("infoType"));
            getFilterInfo(request.getParameter("schema"), request.getParameter("infoType"), response);

        } else if (action.equals("getAddNewInfoRefDoc")) {

            int thematicId = Integer.parseInt(request.getParameter("thematicid"));
            this.getAddNewInfoRefDoc(request.getParameter("schema"), thematicId, null, response);

        } else if (action.equals("getUploadTableNames")) {

            result = this.getUploadTableNames(request.getParameter("schema"));

            if (result != null) {
                response.setContentType("application/json");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().write(result.toString());
            } else {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }

        } else if (action.equals("getUserInformation")) {

            response.setContentType("application/json");

            try {
                if (userp.couldUser(UserPermission.ACTUALIZAR_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {
                    int projectId = Integer.parseInt(session.getAttribute("projectid").toString());
                    int userId = Integer.parseInt(session.getAttribute("userid").toString());
                    String entityId = session.getAttribute("entityid").toString();
                    response.getWriter().write(infoRequest.getAllowedUserInformation(projectId, entityId, userId).toString());
                } else {
                    response.getWriter().write("NO");
                }
            } catch (Exception e) {
                response.getWriter().write("NO");
                e.printStackTrace();
            }

        } else {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }

    }

    /*reads the thematic xml and return available thematic*/
    private JSONArray getThematics() {

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = null;

        Document templates = util.readXML(xmlConfigPath + File.separator + "thematic.xml");
        Element root = templates.getRootElement();
        List<Element> rootChildren = root.getChildren();

        try {
            for (Element element : rootChildren) {

                jsonObject = new JSONObject();
                jsonObject.put("name", element.getAttributeValue("name"));
                jsonObject.put("folder", element.getAttributeValue("folder"));
                jsonArray.put(jsonObject);
            }
        } catch (JSONException e) {
            System.out.println("Exception: " + e);
        }

        return jsonArray;

    }

    /*returns information for filtering*/
    private void getFilterInfo(String schema, String infoType, HttpServletResponse response) throws ServletException, IOException {

        JSONArray result = null;

        if (schema.equals("curador")) {

            if (infoType.equals("especies")) {
                infoRequest.setSchema(schema);
                result = infoRequest.findAllPhylumToJSON();
            }
        }

        if (result != null) {
            response.setContentType("application/json");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().write(result.toString());
        } else {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }
    }

    /*process excel doc with reference information for an specific schema*/
    private void getInfoRefDoc(String schema, int thematicId, RequestParameterManager rpManager, HttpServletResponse response) throws ServletException, IOException {

        WorkbookDownloadController creator;
        String templatePath;
        String fileName = null;

        if (rpManager.getRequestParameterValueByName("inforeftype").equals("1")) {
            fileName = "datos_referentes_previos.xls";
        } else {
            fileName = "datos_referentes_completos.xls";
        }

        
        
        creator = new WorkbookDownloadController(schema, thematicId, rpManager, servletContext);
        HSSFWorkbook infoRefWorkbook = creator.createInfoRefWorkbook();

        if (infoRefWorkbook != null) {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=\""
                    + fileName + "\"");
            response.setHeader("Pragma", "no-cache");

            ServletOutputStream outs = response.getOutputStream();
            infoRefWorkbook.write(outs);
            outs.flush();
            outs.close();
        } else {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }

    }

    /*process excel doc with reference information for an specific schema*/
    private void getAddNewInfoRefDoc(String schema, int thematicId, RequestParameterManager rpManager, HttpServletResponse response) throws ServletException, IOException {

        WorkbookDownloadController creator;
        String templatePath;
        String fileName = "hoja_datos.xls";

        creator = new WorkbookDownloadController(schema, thematicId, rpManager, servletContext);
        HSSFWorkbook infoRefWorkbook = creator.createAddNewInfoRefWorkbook();

        if (infoRefWorkbook != null) {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=\""
                    + fileName + "\"");
            response.setHeader("Pragma", "no-cache");

            ServletOutputStream outs = response.getOutputStream();
            infoRefWorkbook.write(outs);
            outs.flush();
            outs.close();
        } else {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }

    }

    /*reads the uploadtables xml and return tables names and alias*/
    private JSONArray getUploadTableNames(String schema) {

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;

        Document templates = util.readXML(xmlConfigPath + File.separator + schema + File.separator + "uploadtables.xml");
        Element root = templates.getRootElement();
        List<Element> rootChildren = root.getChildren();

        try {
            for (Element element : rootChildren) {

                jsonObject = new JSONObject();
                jsonObject.put("name", element.getAttributeValue("name"));
                jsonObject.put("alias", element.getAttributeValue("alias"));
                jsonArray.put(jsonObject);
            }
        } catch (JSONException e) {
            System.out.println("Exception: " + e);
        }

        return jsonArray;

    }

}
