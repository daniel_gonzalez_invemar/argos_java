package co.org.invemar.submitinfoassistant;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import co.org.invemar.util.ConnectionFactory;

/**
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 */
public class SubmitInformationInfoRequestController {

	// DB connection variables
	private ConnectionFactory cFactory;
	private Connection conn = null;
	private String schema = null;

	// SQL Strings
	private String SQL_ALLPHYLUM = "SELECT DISTINCT phylum_cdg, phylum FROM vm_planilla order by 2";

	public SubmitInformationInfoRequestController(String schema) {
		this.schema = schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	/* get thematics related with a specific project */
	public JSONArray getThematicsByProject(int projectId) throws SQLException {

		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn
					.prepareStatement("SELECT BUBICACION_TEMATICA.CODIGO, vc.COMPONENTE, LOWER(BUBICACION_TEMATICA.ESQUEMA)" +
							" FROM ((BUBICACION_TEMATICA INNER JOIN BPROYECTO_X_COMP ON BPROYECTO_X_COMP.ID_COMPONENTE=BUBICACION_TEMATICA.CODIGO) INNER JOIN ( SELECT  codigo, SYS_CONNECT_BY_PATH (descripcion, '/') AS componente FROM bubicacion_tematica WHERE ultimo = 1 START WITH raiz = 0 CONNECT BY PRIOR codigo = raiz) vc  ON BUBICACION_TEMATICA.CODIGO=vc.CODIGO) " +
							"WHERE BPROYECTO_X_COMP.ID_PROYECTO=?");
			pstmt.setInt(1, projectId);

			rs = pstmt.executeQuery();

			while (rs.next()) {

				jsonObject = new JSONObject();
				jsonObject.put("name", rs.getString(2));
				jsonObject.put("folder", rs.getString(1) + ";"
						+ rs.getString(3));

				jsonArray.put(jsonObject);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if (conn != null) {
				ConnectionFactory.closeConnection(conn);
			}

		}

		return jsonArray;
	}

	public List<Phylum> findAllPhylum() throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		List<Phylum> result = null;
		Phylum phylum = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection(schema);
			pstmt = conn.prepareStatement(SQL_ALLPHYLUM);
			rs = pstmt.executeQuery();

			result = new ArrayList<Phylum>();
			while (rs.next()) {
				phylum = new Phylum(rs.getInt(1), rs.getString(2));

				result.add(phylum);
			}

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if (conn != null) {
				ConnectionFactory.closeConnection(conn);
			}

		}

		return result;
	}

	public JSONArray findAllPhylumToJSON() {

		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject;

		try {

			List<Phylum> phylumList = findAllPhylum();

			for (Phylum phylum : phylumList) {
				jsonObject = new JSONObject();
				jsonObject.put("value", phylum.getCodPhylum());
				jsonObject.put("name", phylum.getPhylum());
				jsonArray.put(jsonObject);
			}
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}

		return jsonArray;

	}

	/* returns user information but just those that user is allowed to see */
	public JSONArray getAllowedUserInformation(int projectId, String entityId,
			int userId) throws SQLException {

		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = null;

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			cFactory = new ConnectionFactory();
			conn = cFactory.createConnection();
			pstmt = conn
					.prepareStatement("SELECT USUARIOS.ID, USUARIOS.NOMBRE ||' '|| USUARIOS.APELLIDO FROM (((USUARIOS INNER JOIN UROLES_USUARIOS ON USUARIOS.ID=UROLES_USUARIOS.ID_USUARIO) INNER JOIN UROLES ON UROLES_USUARIOS.ID_ROL=UROLES.ID) INNER JOIN USUARIOS_DETALLES ON USUARIOS_DETALLES.ID_USUARIO=USUARIOS.ID) WHERE UROLES.ID_PROYECTO=? AND USUARIOS_DETALLES.CODIGO_ATRIBUTO=19 AND USUARIOS_DETALLES.VALOR_ASIGNADO=? AND USUARIOS.ID!=? ORDER BY USUARIOS.USERNAME_EMAIL ASC");
			pstmt.setInt(1, projectId);
			pstmt.setString(2, entityId);
			pstmt.setInt(3, userId);

			rs = pstmt.executeQuery();

			while (rs.next()) {

				jsonObject = new JSONObject();
				jsonObject.put("id", rs.getString(1));
				jsonObject.put("name", rs.getString(2));
				// System.out.println(jsonObject.toString());

				jsonArray.put(jsonObject);

			}

		} catch (Exception e) {
			jsonObject = null;
			System.out.println(e);
			e.printStackTrace();
		} finally {
			if (rs != null)
				rs.close();
			if (pstmt != null)
				pstmt.close();
			if (conn != null) {
				ConnectionFactory.closeConnection(conn);
			}

		}

		return jsonArray;
	}

}
