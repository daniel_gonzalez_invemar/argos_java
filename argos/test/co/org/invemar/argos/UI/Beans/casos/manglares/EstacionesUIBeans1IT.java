/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.UI.Beans.casos.manglares;

import co.org.invemar.argos.geograficos.model.ZonasProtegidas;
import co.org.invemar.argos.manglares.model.ParcelaModel;
import co.org.invemar.argos.manglares.model.SectorManglaresModel;
import co.org.invemar.argos.manglares.model.SitioManglarModel;
import co.org.invemar.argos.manglares.model.UnidadManejoModel;
import co.org.invemar.argos.manglares.vo.Parcela;
import co.org.invemar.argos.manglares.vo.SectorManglarVo;
import co.org.invemar.argos.manglares.vo.Sitio;
import co.org.invemar.argos.manglares.vo.UnidadManejo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author usrsig15
 */
public class EstacionesUIBeans1IT {
    
    public EstacionesUIBeans1IT() {
    }

    /**
     * Test of handleEcorregionesChange method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleEcorregionesChange() {
        System.out.println("handleEcorregionesChange");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleEcorregionesChange();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of seleccionarSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSeleccionarSector() {
        System.out.println("seleccionarSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.seleccionarSector();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of actualizaTablaSectoresAgregados method, of class EstacionesUIBeans1.
     */
    @Test
    public void testActualizaTablaSectoresAgregados() {
        System.out.println("actualizaTablaSectoresAgregados");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.actualizaTablaSectoresAgregados();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of actualizarTablaSitiosAgregados method, of class EstacionesUIBeans1.
     */
    @Test
    public void testActualizarTablaSitiosAgregados() {
        System.out.println("actualizarTablaSitiosAgregados");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.actualizarTablaSitiosAgregados();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of actualizarTablaParcelas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testActualizarTablaParcelas() {
        System.out.println("actualizarTablaParcelas");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.actualizarTablaParcelas();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of actualizarTablaUnidadesManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testActualizarTablaUnidadesManejo() {
        System.out.println("actualizarTablaUnidadesManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.actualizarTablaUnidadesManejo();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleToCheckBoxListaAreasProtegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleToCheckBoxListaAreasProtegidas() {
        System.out.println("handleToCheckBoxListaAreasProtegidas");
        AjaxBehaviorEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleToCheckBoxListaAreasProtegidas(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of onChangeTipoParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testOnChangeTipoParcela() {
        System.out.println("onChangeTipoParcela");
        AjaxBehaviorEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.onChangeTipoParcela(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of onChangeFormatType method, of class EstacionesUIBeans1.
     */
    @Test
    public void testOnChangeFormatType() {
        System.out.println("onChangeFormatType");
        AjaxBehaviorEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.onChangeFormatType(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleToponimiaChange method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleToponimiaChange() {
        System.out.println("handleToponimiaChange");
        AjaxBehaviorEvent e = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleToponimiaChange(e);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleToListaGetAllCAR method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleToListaGetAllCAR() {
        System.out.println("handleToListaGetAllCAR");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleToListaGetAllCAR();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUploadPicSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleFileUploadPicSitio() {
        System.out.println("handleFileUploadPicSitio");
        FileUploadEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleFileUploadPicSitio(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUploadkmlSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleFileUploadkmlSitio() {
        System.out.println("handleFileUploadkmlSitio");
        FileUploadEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleFileUploadkmlSitio(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUploadPicUnnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleFileUploadPicUnnidadManejo() {
        System.out.println("handleFileUploadPicUnnidadManejo");
        FileUploadEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleFileUploadPicUnnidadManejo(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUploadkmlUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleFileUploadkmlUnidadManejo() {
        System.out.println("handleFileUploadkmlUnidadManejo");
        FileUploadEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleFileUploadkmlUnidadManejo(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUploadPicSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleFileUploadPicSector() {
        System.out.println("handleFileUploadPicSector");
        FileUploadEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleFileUploadPicSector(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUploadkmlSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleFileUploadkmlSector() {
        System.out.println("handleFileUploadkmlSector");
        FileUploadEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleFileUploadkmlSector(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of handleActualizarSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testHandleActualizarSector() {
        System.out.println("handleActualizarSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.handleActualizarSector();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of onRowSelectTabla method, of class EstacionesUIBeans1.
     */
    @Test
    public void testOnRowSelectTabla() {
        System.out.println("onRowSelectTabla");
        SelectEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.onRowSelectTabla(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of onRowSelectTablaSitios method, of class EstacionesUIBeans1.
     */
    @Test
    public void testOnRowSelectTablaSitios() {
        System.out.println("onRowSelectTablaSitios");
        SelectEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.onRowSelectTablaSitios(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of onRowSelectTablaUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testOnRowSelectTablaUnidadManejo() {
        System.out.println("onRowSelectTablaUnidadManejo");
        SelectEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.onRowSelectTablaUnidadManejo(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of onRowSelectTablaParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testOnRowSelectTablaParcela() {
        System.out.println("onRowSelectTablaParcela");
        SelectEvent event = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.onRowSelectTablaParcela(event);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of registrarUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testRegistrarUnidadManejo() {
        System.out.println("registrarUnidadManejo");
        ActionEvent actionEvent = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.registrarUnidadManejo(actionEvent);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of registrarSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testRegistrarSector() {
        System.out.println("registrarSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.registrarSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of registrarSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testRegistrarSitio() {
        System.out.println("registrarSitio");
        ActionEvent actionEvent = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.registrarSitio(actionEvent);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of registrarParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testRegistrarParcela() {
        System.out.println("registrarParcela");
        ActionEvent actionEvent = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.registrarParcela(actionEvent);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of reasignarEstacion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testReasignarEstacion() {
        System.out.println("reasignarEstacion");
        ActionEvent actionEvent = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.reasignarEstacion(actionEvent);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of actualizarUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testActualizarUnidadManejo() {
        System.out.println("actualizarUnidadManejo");
        ActionEvent actionEvent = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.actualizarUnidadManejo(actionEvent);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of convertCoordenate method, of class EstacionesUIBeans1.
     */
    @Test
    public void testConvertCoordenate() {
        System.out.println("convertCoordenate");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.convertCoordenate();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of actualizarParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testActualizarParcela() {
        System.out.println("actualizarParcela");
        ActionEvent actionEvent = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.actualizarParcela(actionEvent);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of actualizarSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testActualizarSector() {
        System.out.println("actualizarSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.actualizarSector();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of actualizarSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testActualizarSitio() {
        System.out.println("actualizarSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.actualizarSitio();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of CleanFieldFormUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testCleanFieldFormUnidadManejo() {
        System.out.println("CleanFieldFormUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.CleanFieldFormUnidadManejo();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of cleanFieldFormParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testCleanFieldFormParcela() {
        System.out.println("cleanFieldFormParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.cleanFieldFormParcela();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of CleanFieldFormSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testCleanFieldFormSitio() {
        System.out.println("CleanFieldFormSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.CleanFieldFormSitio();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRegion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetRegion() {
        System.out.println("getRegion");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getRegion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRegion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetRegion() {
        System.out.println("setRegion");
        String region = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setRegion(region);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDescripcion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDescripcion() {
        System.out.println("getDescripcion");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDescripcion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDescripcion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDescripcion() {
        System.out.println("setDescripcion");
        String descripcion = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDescripcion(descripcion);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEcoregion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetEcoregion() {
        System.out.println("getEcoregion");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getEcoregion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEcoregion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetEcoregion() {
        System.out.println("setEcoregion");
        String ecoregion = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setEcoregion(ecoregion);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getToponimo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetToponimo() {
        System.out.println("getToponimo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getToponimo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setToponimo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetToponimo() {
        System.out.println("setToponimo");
        String toponimo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setToponimo(toponimo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEcoregiones method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetEcoregiones() {
        System.out.println("getEcoregiones");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getEcoregiones();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEcoregiones method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetEcoregiones() {
        System.out.println("setEcoregiones");
        String ecoregiones = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setEcoregiones(ecoregiones);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getToponimosMap method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetToponimosMap() {
        System.out.println("getToponimosMap");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getToponimosMap();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setToponimosMap method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetToponimosMap() {
        System.out.println("setToponimosMap");
        Map<String, String> toponimosMap = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setToponimosMap(toponimosMap);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEcorregionesMap method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetEcorregionesMap() {
        System.out.println("getEcorregionesMap");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getEcorregionesMap();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEcorregionesMap method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetEcorregionesMap() {
        System.out.println("setEcorregionesMap");
        Map<String, String> ecorregionesMap = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setEcorregionesMap(ecorregionesMap);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAreageografica method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetAreageografica() {
        System.out.println("getAreageografica");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getAreageografica();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAreageografica method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetAreageografica() {
        System.out.println("setAreageografica");
        String areageografica = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setAreageografica(areageografica);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCuencahidrografica method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetCuencahidrografica() {
        System.out.println("getCuencahidrografica");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getCuencahidrografica();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCuencahidrografica method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetCuencahidrografica() {
        System.out.println("setCuencahidrografica");
        String cuencahidrografica = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setCuencahidrografica(cuencahidrografica);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAreasprotegidasMap method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetAreasprotegidasMap() {
        System.out.println("getAreasprotegidasMap");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getAreasprotegidasMap();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAreasprotegidasMap method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetAreasprotegidasMap() {
        System.out.println("setAreasprotegidasMap");
        Map<String, String> areasprotegidasMap = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setAreasprotegidasMap(areasprotegidasMap);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getZonasProtegidasMap method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetZonasProtegidasMap() {
        System.out.println("getZonasProtegidasMap");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getZonasProtegidasMap();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setZonasProtegidasMap method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetZonasProtegidasMap() {
        System.out.println("setZonasProtegidasMap");
        Map<String, String> zonasProtegidasMap = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setZonasProtegidasMap(zonasProtegidasMap);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getZonaprotegidaCodigo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetZonaprotegidaCodigo() {
        System.out.println("getZonaprotegidaCodigo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getZonaprotegidaCodigo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setZonaprotegidaCodigo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetZonaprotegidaCodigo() {
        System.out.println("setZonaprotegidaCodigo");
        String zonaprotegidaCodigo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setZonaprotegidaCodigo(zonaprotegidaCodigo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEsAreaprotegida method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetEsAreaprotegida() {
        System.out.println("getEsAreaprotegida");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getEsAreaprotegida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setEsAreaprotegida method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetEsAreaprotegida() {
        System.out.println("setEsAreaprotegida");
        String EsAreaprotegida = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setEsAreaprotegida(EsAreaprotegida);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStyleCmBoxAreasProtegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetStyleCmBoxAreasProtegidas() {
        System.out.println("getStyleCmBoxAreasProtegidas");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getStyleCmBoxAreasProtegidas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setStyleCmBoxAreasProtegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetStyleCmBoxAreasProtegidas() {
        System.out.println("setStyleCmBoxAreasProtegidas");
        String styleCmBoxAreasProtegidas = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setStyleCmBoxAreasProtegidas(styleCmBoxAreasProtegidas);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaautoridadesAmbientales method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetListaautoridadesAmbientales() {
        System.out.println("getListaautoridadesAmbientales");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getListaautoridadesAmbientales();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListaautoridadesAmbientales method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetListaautoridadesAmbientales() {
        System.out.println("setListaautoridadesAmbientales");
        Map<String, String> listaautoridadesAmbientales = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setListaautoridadesAmbientales(listaautoridadesAmbientales);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAutoridadambiental method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetAutoridadambiental() {
        System.out.println("getAutoridadambiental");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getAutoridadambiental();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAutoridadambiental method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetAutoridadambiental() {
        System.out.println("setAutoridadambiental");
        String autoridadambiental = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setAutoridadambiental(autoridadambiental);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getImpactosConocidos method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetImpactosConocidos() {
        System.out.println("getImpactosConocidos");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getImpactosConocidos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setImpactosConocidos method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetImpactosConocidos() {
        System.out.println("setImpactosConocidos");
        String impactosConocidos = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setImpactosConocidos(impactosConocidos);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaimpactosconocidos method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetListaimpactosconocidos() {
        System.out.println("getListaimpactosconocidos");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getListaimpactosconocidos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListaimpactosconocidos method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetListaimpactosconocidos() {
        System.out.println("setListaimpactosconocidos");
        Map<String, String> listaimpactosconocidos = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setListaimpactosconocidos(listaimpactosconocidos);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIntervencionAntropica method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetIntervencionAntropica() {
        System.out.println("getIntervencionAntropica");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getIntervencionAntropica();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIntervencionAntropica method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetIntervencionAntropica() {
        System.out.println("setIntervencionAntropica");
        String intervencionAntropica = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setIntervencionAntropica(intervencionAntropica);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaiIntervencionAntropica method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetListaiIntervencionAntropica() {
        System.out.println("getListaiIntervencionAntropica");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getListaiIntervencionAntropica();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListaiIntervencionAntropica method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetListaiIntervencionAntropica() {
        System.out.println("setListaiIntervencionAntropica");
        Map<String, String> listaiIntervencionAntropica = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setListaiIntervencionAntropica(listaiIntervencionAntropica);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getActividaeconomica method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetActividaeconomica() {
        System.out.println("getActividaeconomica");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getActividaeconomica();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setActividaeconomica method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetActividaeconomica() {
        System.out.println("setActividaeconomica");
        String actividaeconomica = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setActividaeconomica(actividaeconomica);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaiActividadesEconomicas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetListaiActividadesEconomicas() {
        System.out.println("getListaiActividadesEconomicas");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getListaiActividadesEconomicas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListaiActividadesEconomicas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetListaiActividadesEconomicas() {
        System.out.println("setListaiActividadesEconomicas");
        Map<String, String> listaiActividadesEconomicas = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setListaiActividadesEconomicas(listaiActividadesEconomicas);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDescripcionGeneral method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDescripcionGeneral() {
        System.out.println("getDescripcionGeneral");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDescripcionGeneral();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDescripcionGeneral method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDescripcionGeneral() {
        System.out.println("setDescripcionGeneral");
        String descripcionGeneral = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDescripcionGeneral(descripcionGeneral);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSiteStyle method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetSiteStyle() {
        System.out.println("getSiteStyle");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getSiteStyle();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSiteStyle method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetSiteStyle() {
        System.out.println("setSiteStyle");
        String SiteStyle = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setSiteStyle(SiteStyle);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetSector() {
        System.out.println("getSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        SectorManglarVo expResult = null;
        SectorManglarVo result = instance.getSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetSector() {
        System.out.println("setSector");
        SectorManglarVo areamanglar = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setSector(areamanglar);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaAreasManglares method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetListaAreasManglares() {
        System.out.println("getListaAreasManglares");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        List<SectorManglarVo> expResult = null;
        List<SectorManglarVo> result = instance.getListaAreasManglares();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListaAreasManglares method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetListaAreasManglares() {
        System.out.println("setListaAreasManglares");
        List<SectorManglarVo> listaAreasManglares = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setListaAreasManglares(listaAreasManglares);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getModelodatosareamanglar method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetModelodatosareamanglar() {
        System.out.println("getModelodatosareamanglar");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        ModeloDatosAreaManglar expResult = null;
        ModeloDatosAreaManglar result = instance.getModelodatosareamanglar();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setModelodatosareamanglar method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetModelodatosareamanglar() {
        System.out.println("setModelodatosareamanglar");
        ModeloDatosAreaManglar modelodatosareamanglar = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setModelodatosareamanglar(modelodatosareamanglar);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExtension method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetExtension() {
        System.out.println("getExtension");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        int expResult = 0;
        int result = instance.getExtension();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setExtension method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetExtension() {
        System.out.println("setExtension");
        int extension = 0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setExtension(extension);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIdCAR method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetIdCAR() {
        System.out.println("getIdCAR");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getIdCAR();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIdCAR method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetIdCAR() {
        System.out.println("setIdCAR");
        String idCAR = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setIdCAR(idCAR);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRenderizarComboBoxAreasProtegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetRenderizarComboBoxAreasProtegidas() {
        System.out.println("getRenderizarComboBoxAreasProtegidas");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getRenderizarComboBoxAreasProtegidas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRenderizarComboBoxAreasProtegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetRenderizarComboBoxAreasProtegidas() {
        System.out.println("setRenderizarComboBoxAreasProtegidas");
        String renderizarComboBoxAreasProtegidas = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setRenderizarComboBoxAreasProtegidas(renderizarComboBoxAreasProtegidas);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRenderizarpoutputLabelAreasProtegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetRenderizarpoutputLabelAreasProtegidas() {
        System.out.println("getRenderizarpoutputLabelAreasProtegidas");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getRenderizarpoutputLabelAreasProtegidas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRenderizarpoutputLabelAreasProtegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetRenderizarpoutputLabelAreasProtegidas() {
        System.out.println("setRenderizarpoutputLabelAreasProtegidas");
        String renderizarpoutputLabelAreasProtegidas = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setRenderizarpoutputLabelAreasProtegidas(renderizarpoutputLabelAreasProtegidas);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRenderizarMessagesAreasProtegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetRenderizarMessagesAreasProtegidas() {
        System.out.println("getRenderizarMessagesAreasProtegidas");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getRenderizarMessagesAreasProtegidas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRenderizarMessagesAreasProtegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetRenderizarMessagesAreasProtegidas() {
        System.out.println("setRenderizarMessagesAreasProtegidas");
        String renderizarMessagesAreasProtegidas = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setRenderizarMessagesAreasProtegidas(renderizarMessagesAreasProtegidas);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMensajeRegistro method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetMensajeRegistro() {
        System.out.println("getMensajeRegistro");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getMensajeRegistro();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMensajeRegistro method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetMensajeRegistro() {
        System.out.println("setMensajeRegistro");
        String mensajeRegistro = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setMensajeRegistro(mensajeRegistro);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMensaje method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetMensaje() {
        System.out.println("getMensaje");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getMensaje();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMensaje method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetMensaje() {
        System.out.println("setMensaje");
        String mensaje = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setMensaje(mensaje);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMensajeRegistroStyle method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetMensajeRegistroStyle() {
        System.out.println("getMensajeRegistroStyle");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getMensajeRegistroStyle();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMensajeRegistroStyle method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetMensajeRegistroStyle() {
        System.out.println("setMensajeRegistroStyle");
        String mensajeRegistroStyle = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setMensajeRegistroStyle(mensajeRegistroStyle);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAbreviaturaSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetAbreviaturaSector() {
        System.out.println("getAbreviaturaSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getAbreviaturaSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAbreviaturaSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetAbreviaturaSector() {
        System.out.println("setAbreviaturaSector");
        String abreviaturaSector = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setAbreviaturaSector(abreviaturaSector);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getURLInicio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetURLInicio() {
        System.out.println("getURLInicio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getURLInicio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setURLInicio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetURLInicio() {
        System.out.println("setURLInicio");
        String URLInicio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setURLInicio(URLInicio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIdProyecto method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetIdProyecto() {
        System.out.println("getIdProyecto");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        int expResult = 0;
        int result = instance.getIdProyecto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIdProyecto method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetIdProyecto() {
        System.out.println("setIdProyecto");
        int idProyecto = 0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setIdProyecto(idProyecto);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getZonasprotegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetZonasprotegidas() {
        System.out.println("getZonasprotegidas");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        ZonasProtegidas expResult = null;
        ZonasProtegidas result = instance.getZonasprotegidas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setZonasprotegidas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetZonasprotegidas() {
        System.out.println("setZonasprotegidas");
        ZonasProtegidas zonasprotegidas = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setZonasprotegidas(zonasprotegidas);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRutaAplicacion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetRutaAplicacion() {
        System.out.println("getRutaAplicacion");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getRutaAplicacion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRutaAplicacion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetRutaAplicacion() {
        System.out.println("setRutaAplicacion");
        String rutaAplicacion = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setRutaAplicacion(rutaAplicacion);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPropiedades method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetPropiedades() {
        System.out.println("getPropiedades");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Properties expResult = null;
        Properties result = instance.getPropiedades();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPropiedades method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetPropiedades() {
        System.out.println("setPropiedades");
        Properties propiedades = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setPropiedades(propiedades);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIdSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetIdSector() {
        System.out.println("getIdSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        int expResult = 0;
        int result = instance.getIdSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIdSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetIdSector() {
        System.out.println("setIdSector");
        int idSector = 0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setIdSector(idSector);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNombreSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetNombreSitio() {
        System.out.println("getNombreSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getNombreSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNombreSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetNombreSitio() {
        System.out.println("setNombreSitio");
        String nombreSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setNombreSitio(nombreSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDescripcionGeneralSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDescripcionGeneralSitio() {
        System.out.println("getDescripcionGeneralSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDescripcionGeneralSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDescripcionGeneralSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDescripcionGeneralSitio() {
        System.out.println("setDescripcionGeneralSitio");
        String descripcionGeneralSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDescripcionGeneralSitio(descripcionGeneralSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMensajeVentanaNuevoSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetMensajeVentanaNuevoSitio() {
        System.out.println("getMensajeVentanaNuevoSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getMensajeVentanaNuevoSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMensajeVentanaNuevoSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetMensajeVentanaNuevoSitio() {
        System.out.println("setMensajeVentanaNuevoSitio");
        String mensajeVentanaNuevoSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setMensajeVentanaNuevoSitio(mensajeVentanaNuevoSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getModelodatositios method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetModelodatositios() {
        System.out.println("getModelodatositios");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        ModeloDatosSitio expResult = null;
        ModeloDatosSitio result = instance.getModelodatositios();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setModelodatositios method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetModelodatositios() {
        System.out.println("setModelodatositios");
        ModeloDatosSitio modelodatositios = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setModelodatositios(modelodatositios);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetSitio() {
        System.out.println("getSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Sitio expResult = null;
        Sitio result = instance.getSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetSitio() {
        System.out.println("setSitio");
        Sitio sitio = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setSitio(sitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLabelbotonRegistroSitios method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetLabelbotonRegistroSitios() {
        System.out.println("getLabelbotonRegistroSitios");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getLabelbotonRegistroSitios();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLabelbotonRegistroSitios method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetLabelbotonRegistroSitios() {
        System.out.println("setLabelbotonRegistroSitios");
        String labelbotonRegistroSitios = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setLabelbotonRegistroSitios(labelbotonRegistroSitios);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAbreviaturaSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetAbreviaturaSitio() {
        System.out.println("getAbreviaturaSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getAbreviaturaSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAbreviaturaSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetAbreviaturaSitio() {
        System.out.println("setAbreviaturaSitio");
        String abreviaturaSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setAbreviaturaSitio(abreviaturaSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSitioActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetSitioActivo() {
        System.out.println("getSitioActivo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getSitioActivo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSitioActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetSitioActivo() {
        System.out.println("setSitioActivo");
        String sitioActivo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setSitioActivo(sitioActivo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNombreUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetNombreUnidadManejo() {
        System.out.println("getNombreUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getNombreUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNombreUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetNombreUnidadManejo() {
        System.out.println("setNombreUnidadManejo");
        String nombreUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setNombreUnidadManejo(nombreUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDescripcionGeneralUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDescripcionGeneralUnidadManejo() {
        System.out.println("getDescripcionGeneralUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDescripcionGeneralUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDescripcionGeneralUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDescripcionGeneralUnidadManejo() {
        System.out.println("setDescripcionGeneralUnidadManejo");
        String descripcionGeneralUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDescripcionGeneralUnidadManejo(descripcionGeneralUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCategoriaUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetCategoriaUnidadManejo() {
        System.out.println("getCategoriaUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getCategoriaUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCategoriaUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetCategoriaUnidadManejo() {
        System.out.println("setCategoriaUnidadManejo");
        String categoriaUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setCategoriaUnidadManejo(categoriaUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIsActivadaUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetIsActivadaUnidadManejo() {
        System.out.println("getIsActivadaUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getIsActivadaUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIsActivadaUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetIsActivadaUnidadManejo() {
        System.out.println("setIsActivadaUnidadManejo");
        String isActivadaUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setIsActivadaUnidadManejo(isActivadaUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNombreCortoUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetNombreCortoUnidadManejo() {
        System.out.println("getNombreCortoUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getNombreCortoUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNombreCortoUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetNombreCortoUnidadManejo() {
        System.out.println("setNombreCortoUnidadManejo");
        String nombreCortoUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setNombreCortoUnidadManejo(nombreCortoUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMansajeVentanaUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetMansajeVentanaUnidadManejo() {
        System.out.println("getMansajeVentanaUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getMansajeVentanaUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMensajeVentanaUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetMensajeVentanaUnidadManejo() {
        System.out.println("setMensajeVentanaUnidadManejo");
        String mensajeVentanaUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setMensajeVentanaUnidadManejo(mensajeVentanaUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUnidadmanejomodelodatos method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUnidadmanejomodelodatos() {
        System.out.println("getUnidadmanejomodelodatos");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        ModeloDatosUnidadManejo expResult = null;
        ModeloDatosUnidadManejo result = instance.getUnidadmanejomodelodatos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUnidadmanejomodelodatos method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUnidadmanejomodelodatos() {
        System.out.println("setUnidadmanejomodelodatos");
        ModeloDatosUnidadManejo unidadmanejomodelodatos = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUnidadmanejomodelodatos(unidadmanejomodelodatos);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUnidadManejo() {
        System.out.println("getUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        UnidadManejo expResult = null;
        UnidadManejo result = instance.getUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUnidadManejo() {
        System.out.println("setUnidadManejo");
        UnidadManejo unidadManejo = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUnidadManejo(unidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSectorManglarModel method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetSectorManglarModel() {
        System.out.println("getSectorManglarModel");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        SectorManglaresModel expResult = null;
        SectorManglaresModel result = instance.getSectorManglarModel();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSectorManglarModel method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetSectorManglarModel() {
        System.out.println("setSectorManglarModel");
        SectorManglaresModel sectorManglarModel = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setSectorManglarModel(sectorManglarModel);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSitios method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetSitios() {
        System.out.println("getSitios");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        ArrayList<Sitio> expResult = null;
        ArrayList<Sitio> result = instance.getSitios();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSitios method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetSitios() {
        System.out.println("setSitios");
        ArrayList<Sitio> sitios = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setSitios(sitios);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIdUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetIdUnidadManejo() {
        System.out.println("getIdUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        int expResult = 0;
        int result = instance.getIdUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIdUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetIdUnidadManejo() {
        System.out.println("setIdUnidadManejo");
        int idUnidadManejo = 0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setIdUnidadManejo(idUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSitiomanglarmodel method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetSitiomanglarmodel() {
        System.out.println("getSitiomanglarmodel");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        SitioManglarModel expResult = null;
        SitioManglarModel result = instance.getSitiomanglarmodel();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSitiomanglarmodel method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetSitiomanglarmodel() {
        System.out.println("setSitiomanglarmodel");
        SitioManglarModel sitiomanglarmodel = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setSitiomanglarmodel(sitiomanglarmodel);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFormaParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetFormaParcela() {
        System.out.println("getFormaParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getFormaParcela();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFormaParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetFormaParcela() {
        System.out.println("setFormaParcela");
        String formaParcela = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setFormaParcela(formaParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAzimut method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetAzimut() {
        System.out.println("getAzimut");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        int expResult = 0;
        int result = instance.getAzimut();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAzimut method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetAzimut() {
        System.out.println("setAzimut");
        int azimut = 0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setAzimut(azimut);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAreaParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetAreaParcela() {
        System.out.println("getAreaParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getAreaParcela();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAreaParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetAreaParcela() {
        System.out.println("setAreaParcela");
        int areaParcela = 0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setAreaParcela(areaParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFechaInstalacion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetFechaInstalacion() {
        System.out.println("getFechaInstalacion");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Date expResult = null;
        Date result = instance.getFechaInstalacion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFechaInstalacion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetFechaInstalacion() {
        System.out.println("setFechaInstalacion");
        Date fechaInstalacion = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setFechaInstalacion(fechaInstalacion);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLatitudParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetLatitudParcela() {
        System.out.println("getLatitudParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getLatitudParcela();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLatitudParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetLatitudParcela() {
        System.out.println("setLatitudParcela");
        double latitudParcela = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setLatitudParcela(latitudParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLongitudParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetLongitudParcela() {
        System.out.println("getLongitudParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getLongitudParcela();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLongitudParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetLongitudParcela() {
        System.out.println("setLongitudParcela");
        double longitudParcela = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setLongitudParcela(longitudParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIdParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetIdParcela() {
        System.out.println("getIdParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        int expResult = 0;
        int result = instance.getIdParcela();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIdParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetIdParcela() {
        System.out.println("setIdParcela");
        int idParcela = 0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setIdParcela(idParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getModeloDatosParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetModeloDatosParcela() {
        System.out.println("getModeloDatosParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        ModeloDatosParcela expResult = null;
        ModeloDatosParcela result = instance.getModeloDatosParcela();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setModeloDatosParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetModeloDatosParcela() {
        System.out.println("setModeloDatosParcela");
        ModeloDatosParcela modeloDatosParcela = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setModeloDatosParcela(modeloDatosParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDescripcionParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDescripcionParcela() {
        System.out.println("getDescripcionParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDescripcionParcela();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDescripcionParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDescripcionParcela() {
        System.out.println("setDescripcionParcela");
        String descripcionParcela = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDescripcionParcela(descripcionParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNombreCortoParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetNombreCortoParcela() {
        System.out.println("getNombreCortoParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getNombreCortoParcela();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNombreCortoParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetNombreCortoParcela() {
        System.out.println("setNombreCortoParcela");
        String nombreCortoParcela = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setNombreCortoParcela(nombreCortoParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIsParcelaActiva method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetIsParcelaActiva() {
        System.out.println("getIsParcelaActiva");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getIsParcelaActiva();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIsParcelaActiva method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetIsParcelaActiva() {
        System.out.println("setIsParcelaActiva");
        String isParcelaActiva = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setIsParcelaActiva(isParcelaActiva);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMensajeVentanaParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetMensajeVentanaParcela() {
        System.out.println("getMensajeVentanaParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getMensajeVentanaParcela();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMensajeVentanaParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetMensajeVentanaParcela() {
        System.out.println("setMensajeVentanaParcela");
        String mensajeVentanaParcela = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setMensajeVentanaParcela(mensajeVentanaParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetParcela() {
        System.out.println("getParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Parcela expResult = null;
        Parcela result = instance.getParcela();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetParcela() {
        System.out.println("setParcela");
        Parcela parcela = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setParcela(parcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUnidadesManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUnidadesManejo() {
        System.out.println("getUnidadesManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        ArrayList<UnidadManejo> expResult = null;
        ArrayList<UnidadManejo> result = instance.getUnidadesManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUnidadesManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUnidadesManejo() {
        System.out.println("setUnidadesManejo");
        ArrayList<UnidadManejo> unidadesManejo = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUnidadesManejo(unidadesManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getParcelas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetParcelas() {
        System.out.println("getParcelas");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        ArrayList<Parcela> expResult = null;
        ArrayList<Parcela> result = instance.getParcelas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setParcelas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetParcelas() {
        System.out.println("setParcelas");
        ArrayList<Parcela> parcelas = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setParcelas(parcelas);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getNombreParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetNombreParcela() {
        System.out.println("getNombreParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getNombreParcela();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNombreParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetNombreParcela() {
        System.out.println("setNombreParcela");
        String nombreParcela = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setNombreParcela(nombreParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisabledBotonActualizarSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisabledBotonActualizarSitio() {
        System.out.println("getDisabledBotonActualizarSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisabledBotonActualizarSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisabledBotonActualizarSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisabledBotonActualizarSitio() {
        System.out.println("setDisabledBotonActualizarSitio");
        String disabledBotonActualizarSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisabledBotonActualizarSitio(disabledBotonActualizarSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisabledBotonActualizarUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisabledBotonActualizarUnidadManejo() {
        System.out.println("getDisabledBotonActualizarUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisabledBotonActualizarUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisabledBotonActualizarUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisabledBotonActualizarUnidadManejo() {
        System.out.println("setDisabledBotonActualizarUnidadManejo");
        String disabledBotonActualizarUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisabledBotonActualizarUnidadManejo(disabledBotonActualizarUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisabledBotonActualizarParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisabledBotonActualizarParcela() {
        System.out.println("getDisabledBotonActualizarParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisabledBotonActualizarParcela();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisabledBotonActualizarParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisabledBotonActualizarParcela() {
        System.out.println("setDisabledBotonActualizarParcela");
        String disabledBotonActualizarParcela = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisabledBotonActualizarParcela(disabledBotonActualizarParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUnidadManejoModelo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUnidadManejoModelo() {
        System.out.println("getUnidadManejoModelo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        UnidadManejoModel expResult = null;
        UnidadManejoModel result = instance.getUnidadManejoModelo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUnidadManejoModelo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUnidadManejoModelo() {
        System.out.println("setUnidadManejoModelo");
        UnidadManejoModel unidadManejoModelo = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUnidadManejoModelo(unidadManejoModelo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getParcelaModelo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetParcelaModelo() {
        System.out.println("getParcelaModelo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        ParcelaModel expResult = null;
        ParcelaModel result = instance.getParcelaModelo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setParcelaModelo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetParcelaModelo() {
        System.out.println("setParcelaModelo");
        ParcelaModel parcelaModelo = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setParcelaModelo(parcelaModelo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisableBotonAgregarORegistrarSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisableBotonAgregarORegistrarSector() {
        System.out.println("getDisableBotonAgregarORegistrarSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisableBotonAgregarORegistrarSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisableBotonAgregarORegistrarSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisableBotonAgregarORegistrarSector() {
        System.out.println("setDisableBotonAgregarORegistrarSector");
        String disableBotonAgregarORegistrarSector = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisableBotonAgregarORegistrarSector(disableBotonAgregarORegistrarSector);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisableBotonAgregarORegistrarNuevoSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisableBotonAgregarORegistrarNuevoSitio() {
        System.out.println("getDisableBotonAgregarORegistrarNuevoSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisableBotonAgregarORegistrarNuevoSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisableBotonAgregarORegistrarNuevoSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisableBotonAgregarORegistrarNuevoSitio() {
        System.out.println("setDisableBotonAgregarORegistrarNuevoSitio");
        String disableBotonAgregarORegistrarNuevoSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisableBotonAgregarORegistrarNuevoSitio(disableBotonAgregarORegistrarNuevoSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisableBotonAgregarORegistrarUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisableBotonAgregarORegistrarUnidadManejo() {
        System.out.println("getDisableBotonAgregarORegistrarUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisableBotonAgregarORegistrarUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisableBotonAgregarORegistrarUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisableBotonAgregarORegistrarUnidadManejo() {
        System.out.println("setDisableBotonAgregarORegistrarUnidadManejo");
        String disableBotonAgregarORegistrarUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisableBotonAgregarORegistrarUnidadManejo(disableBotonAgregarORegistrarUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisableBotonAgregarORegistrarParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisableBotonAgregarORegistrarParcela() {
        System.out.println("getDisableBotonAgregarORegistrarParcela");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisableBotonAgregarORegistrarParcela();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisableBotonAgregarORegistrarParcela method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisableBotonAgregarORegistrarParcela() {
        System.out.println("setDisableBotonAgregarORegistrarParcela");
        String disableBotonAgregarORegistrarParcela = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisableBotonAgregarORegistrarParcela(disableBotonAgregarORegistrarParcela);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getKml method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetKml() {
        System.out.println("getKml");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        UploadedFile expResult = null;
        UploadedFile result = instance.getKml();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setKml method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetKml() {
        System.out.println("setKml");
        UploadedFile kml = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setKml(kml);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSectorActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetSectorActivo() {
        System.out.println("getSectorActivo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getSectorActivo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSectorActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetSectorActivo() {
        System.out.println("setSectorActivo");
        String sectorActivo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setSectorActivo(sectorActivo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAzimutActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetAzimutActivo() {
        System.out.println("getAzimutActivo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getAzimutActivo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAzimutActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetAzimutActivo() {
        System.out.println("setAzimutActivo");
        String azimutActivo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setAzimutActivo(azimutActivo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUrlKmlFileSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUrlKmlFileSector() {
        System.out.println("getUrlKmlFileSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getUrlKmlFileSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUrlKmlFileSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUrlKmlFileSector() {
        System.out.println("setUrlKmlFileSector");
        String urlKmlFileSector = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUrlKmlFileSector(urlKmlFileSector);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUrlPicFileSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUrlPicFileSector() {
        System.out.println("getUrlPicFileSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getUrlPicFileSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUrlPicFileSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUrlPicFileSector() {
        System.out.println("setUrlPicFileSector");
        String urlPicFileSector = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUrlPicFileSector(urlPicFileSector);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUrlKmlFileSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUrlKmlFileSitio() {
        System.out.println("getUrlKmlFileSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getUrlKmlFileSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUrlKmlFileSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUrlKmlFileSitio() {
        System.out.println("setUrlKmlFileSitio");
        String urlKmlFileSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUrlKmlFileSitio(urlKmlFileSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUrlPicFileSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUrlPicFileSitio() {
        System.out.println("getUrlPicFileSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getUrlPicFileSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUrlPicFileSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUrlPicFileSitio() {
        System.out.println("setUrlPicFileSitio");
        String urlPicFileSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUrlPicFileSitio(urlPicFileSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUrlKmlFileUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUrlKmlFileUnidadManejo() {
        System.out.println("getUrlKmlFileUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getUrlKmlFileUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUrlKmlFileUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUrlKmlFileUnidadManejo() {
        System.out.println("setUrlKmlFileUnidadManejo");
        String urlKmlFileUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUrlKmlFileUnidadManejo(urlKmlFileUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUrlPicFileUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUrlPicFileUnidadManejo() {
        System.out.println("getUrlPicFileUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getUrlPicFileUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUrlPicFileUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUrlPicFileUnidadManejo() {
        System.out.println("setUrlPicFileUnidadManejo");
        String urlPicFileUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUrlPicFileUnidadManejo(urlPicFileUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getComponente method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetComponente() {
        System.out.println("getComponente");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        int expResult = 0;
        int result = instance.getComponente();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setComponente method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetComponente() {
        System.out.println("setComponente");
        int componente = 0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setComponente(componente);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTopoAreaParcelasRegeneracion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetTopoAreaParcelasRegeneracion() {
        System.out.println("getTopoAreaParcelasRegeneracion");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getTopoAreaParcelasRegeneracion();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTopoAreaParcelasRegeneracion method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetTopoAreaParcelasRegeneracion() {
        System.out.println("setTopoAreaParcelasRegeneracion");
        double topoAreaParcelasRegeneracion = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setTopoAreaParcelasRegeneracion(topoAreaParcelasRegeneracion);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUnidadManejoSeleccionada method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUnidadManejoSeleccionada() {
        System.out.println("getUnidadManejoSeleccionada");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getUnidadManejoSeleccionada();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUnidadManejoSeleccionada method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUnidadManejoSeleccionada() {
        System.out.println("setUnidadManejoSeleccionada");
        String unidadManejoSeleccionada = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUnidadManejoSeleccionada(unidadManejoSeleccionada);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUnidadesManejoSeleccinable method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUnidadesManejoSeleccinable() {
        System.out.println("getUnidadesManejoSeleccinable");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getUnidadesManejoSeleccinable();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUnidadesManejoSeleccinable method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUnidadesManejoSeleccinable() {
        System.out.println("setUnidadesManejoSeleccinable");
        Map<String, String> unidadesManejoSeleccinable = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUnidadesManejoSeleccinable(unidadesManejoSeleccinable);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getUnidadManejoActual method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetUnidadManejoActual() {
        System.out.println("getUnidadManejoActual");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        int expResult = 0;
        int result = instance.getUnidadManejoActual();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setUnidadManejoActual method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetUnidadManejoActual() {
        System.out.println("setUnidadManejoActual");
        int unidadManejoActual = 0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setUnidadManejoActual(unidadManejoActual);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAuditoriaSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetAuditoriaSitio() {
        System.out.println("getAuditoriaSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getAuditoriaSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAuditoriaSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetAuditoriaSitio() {
        System.out.println("setAuditoriaSitio");
        String auditoriaSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setAuditoriaSitio(auditoriaSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAreaParcelas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetAreaParcelas() {
        System.out.println("getAreaParcelas");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getAreaParcelas();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAreaParcelas method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetAreaParcelas() {
        System.out.println("setAreaParcelas");
        double areaParcelas = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setAreaParcelas(areaParcelas);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFormatoCoordenate method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetFormatoCoordenate() {
        System.out.println("getFormatoCoordenate");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getFormatoCoordenate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFormatoCoordenate method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetFormatoCoordenate() {
        System.out.println("setFormatoCoordenate");
        String formatoCoordenate = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setFormatoCoordenate(formatoCoordenate);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLatitudgradepanelActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetLatitudgradepanelActivo() {
        System.out.println("getLatitudgradepanelActivo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getLatitudgradepanelActivo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLatitudgradepanelActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetLatitudgradepanelActivo() {
        System.out.println("setLatitudgradepanelActivo");
        String latitudgradepanelActivo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setLatitudgradepanelActivo(latitudgradepanelActivo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLongitudgradepanelActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetLongitudgradepanelActivo() {
        System.out.println("getLongitudgradepanelActivo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getLongitudgradepanelActivo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLongitudgradepanelActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetLongitudgradepanelActivo() {
        System.out.println("setLongitudgradepanelActivo");
        String longitudgradepanelActivo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setLongitudgradepanelActivo(longitudgradepanelActivo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLatitudDecimalActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetLatitudDecimalActivo() {
        System.out.println("getLatitudDecimalActivo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getLatitudDecimalActivo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLatitudDecimalActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetLatitudDecimalActivo() {
        System.out.println("setLatitudDecimalActivo");
        String latitudDecimalActivo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setLatitudDecimalActivo(latitudDecimalActivo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLongituDecimalActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetLongituDecimalActivo() {
        System.out.println("getLongituDecimalActivo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getLongituDecimalActivo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLongituDecimalActivo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetLongituDecimalActivo() {
        System.out.println("setLongituDecimalActivo");
        String longituDecimalActivo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setLongituDecimalActivo(longituDecimalActivo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGradeLongitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetGradeLongitud() {
        System.out.println("getGradeLongitud");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getGradeLongitud();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setGradeLongitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetGradeLongitud() {
        System.out.println("setGradeLongitud");
        double gradeLongitud = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setGradeLongitud(gradeLongitud);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMinuteLongitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetMinuteLongitud() {
        System.out.println("getMinuteLongitud");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getMinuteLongitud();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMinuteLongitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetMinuteLongitud() {
        System.out.println("setMinuteLongitud");
        double minuteLongitud = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setMinuteLongitud(minuteLongitud);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGradeLatitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetGradeLatitud() {
        System.out.println("getGradeLatitud");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getGradeLatitud();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setGradeLatitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetGradeLatitud() {
        System.out.println("setGradeLatitud");
        double gradeLatitud = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setGradeLatitud(gradeLatitud);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMinuteLatitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetMinuteLatitud() {
        System.out.println("getMinuteLatitud");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getMinuteLatitud();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setMinuteLatitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetMinuteLatitud() {
        System.out.println("setMinuteLatitud");
        double minuteLatitud = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setMinuteLatitud(minuteLatitud);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSecondLatitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetSecondLatitud() {
        System.out.println("getSecondLatitud");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getSecondLatitud();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSecondLatitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetSecondLatitud() {
        System.out.println("setSecondLatitud");
        double secondLatitud = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setSecondLatitud(secondLatitud);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSecondLongitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetSecondLongitud() {
        System.out.println("getSecondLongitud");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getSecondLongitud();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setSecondLongitud method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetSecondLongitud() {
        System.out.println("setSecondLongitud");
        double secondLongitud = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setSecondLongitud(secondLongitud);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getVigenciaLoc method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetVigenciaLoc() {
        System.out.println("getVigenciaLoc");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getVigenciaLoc();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setVigenciaLoc method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetVigenciaLoc() {
        System.out.println("setVigenciaLoc");
        String vigenciaLoc = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setVigenciaLoc(vigenciaLoc);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTiposFisiograficos method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetTiposFisiograficos() {
        System.out.println("getTiposFisiograficos");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getTiposFisiograficos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTiposFisiograficos method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetTiposFisiograficos() {
        System.out.println("setTiposFisiograficos");
        Map<String, String> tiposFisiograficos = null;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setTiposFisiograficos(tiposFisiograficos);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getIdTipoFisiograficos method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetIdTipoFisiograficos() {
        System.out.println("getIdTipoFisiograficos");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getIdTipoFisiograficos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setIdTipoFisiograficos method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetIdTipoFisiograficos() {
        System.out.println("setIdTipoFisiograficos");
        String idTipoFisiograficos = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setIdTipoFisiograficos(idTipoFisiograficos);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getValueConverted method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetValueConverted() {
        System.out.println("getValueConverted");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        double expResult = 0.0;
        double result = instance.getValueConverted();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setValueConverted method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetValueConverted() {
        System.out.println("setValueConverted");
        double valueConverted = 0.0;
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setValueConverted(valueConverted);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisableBotonActualizarSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisableBotonActualizarSector() {
        System.out.println("getDisableBotonActualizarSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisableBotonActualizarSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisableBotonActualizarSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisableBotonActualizarSector() {
        System.out.println("setDisableBotonActualizarSector");
        String disableBotonActualizarSector = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisableBotonActualizarSector(disableBotonActualizarSector);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisableBotonRegistrarSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisableBotonRegistrarSector() {
        System.out.println("getDisableBotonRegistrarSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisableBotonRegistrarSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisableBotonRegistrarSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisableBotonRegistrarSector() {
        System.out.println("setDisableBotonRegistrarSector");
        String disableBotonRegistrarSector = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisableBotonRegistrarSector(disableBotonRegistrarSector);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisablecambiarunidadmanejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisablecambiarunidadmanejo() {
        System.out.println("getDisablecambiarunidadmanejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisablecambiarunidadmanejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisablecambiarunidadmanejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisablecambiarunidadmanejo() {
        System.out.println("setDisablecambiarunidadmanejo");
        String disablecambiarunidadmanejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisablecambiarunidadmanejo(disablecambiarunidadmanejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisabledBotonRegistrarUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisabledBotonRegistrarUnidadManejo() {
        System.out.println("getDisabledBotonRegistrarUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisabledBotonRegistrarUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisabledBotonRegistrarUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisabledBotonRegistrarUnidadManejo() {
        System.out.println("setDisabledBotonRegistrarUnidadManejo");
        String disabledBotonRegistrarUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisabledBotonRegistrarUnidadManejo(disabledBotonRegistrarUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisabledBotonRegistrarSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisabledBotonRegistrarSitio() {
        System.out.println("getDisabledBotonRegistrarSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisabledBotonRegistrarSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisabledBotonRegistrarSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisabledBotonRegistrarSitio() {
        System.out.println("setDisabledBotonRegistrarSitio");
        String disabledBotonRegistrarSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisabledBotonRegistrarSitio(disabledBotonRegistrarSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisableBotonLoadSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisableBotonLoadSector() {
        System.out.println("getDisableBotonLoadSector");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisableBotonLoadSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisableBotonLoadSector method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisableBotonLoadSector() {
        System.out.println("setDisableBotonLoadSector");
        String disableBotonLoadSector = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisableBotonLoadSector(disableBotonLoadSector);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisabledBotonLoadUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisabledBotonLoadUnidadManejo() {
        System.out.println("getDisabledBotonLoadUnidadManejo");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisabledBotonLoadUnidadManejo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisabledBotonLoadUnidadManejo method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisabledBotonLoadUnidadManejo() {
        System.out.println("setDisabledBotonLoadUnidadManejo");
        String disabledBotonLoadUnidadManejo = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisabledBotonLoadUnidadManejo(disabledBotonLoadUnidadManejo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDisabledBotonLoadSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testGetDisabledBotonLoadSitio() {
        System.out.println("getDisabledBotonLoadSitio");
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        String expResult = "";
        String result = instance.getDisabledBotonLoadSitio();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDisabledBotonLoadSitio method, of class EstacionesUIBeans1.
     */
    @Test
    public void testSetDisabledBotonLoadSitio() {
        System.out.println("setDisabledBotonLoadSitio");
        String disabledBotonLoadSitio = "";
        EstacionesUIBeans1 instance = new EstacionesUIBeans1();
        instance.setDisabledBotonLoadSitio(disabledBotonLoadSitio);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
