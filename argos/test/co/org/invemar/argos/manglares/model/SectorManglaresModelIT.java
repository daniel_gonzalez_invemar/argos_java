/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.argos.manglares.model;

import co.org.invemar.util.ResultadoTransaccion;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author usrsig15
 */
public class SectorManglaresModelIT {
    /**
     * 
     */
    public SectorManglaresModelIT() {
    }

    /**
     * Test of getTodosLosSectores method, of class SectorManglaresModel.
     */
    
    public void testGetTodosLosSectores() {
    }

    /**
     * Test of getTodosLosSectoresPorCorporacion method, of class SectorManglaresModel.
     */
   
    public void testGetTodosLosSectoresPorCorporacion() {
    }

    /**
     * Test of getSectorPorID method, of class SectorManglaresModel.
     */
    
    public void testGetSectorPorID() {
    }

    /**
     * Test of registraSector method, of class SectorManglaresModel.
     */
   
    public void testRegistraSector() throws Exception {
        SectorManglaresModel smm = new SectorManglaresModel();
        String[] entities = new String[2];
        entities[0]="entidad 1";
        entities[1]="entidad 2";
     
    }
   /**
    * 
    */
    public void testInsertEntityInCaseStudi(){
         SectorManglaresModel smm = new SectorManglaresModel();
         ResultadoTransaccion rt =smm.insertEntityInCaseStudio("15284", "daniel");
         System.out.println(rt.getCodigoError());
         System.out.println(rt.getMensaje());
         System.out.println(rt.getCodigo());
    }
    
    /**
     * 
     */
    @Test     
     public void testInsertEntitiesOfCaseStudio(){
          SectorManglaresModel smm = new SectorManglaresModel();
          String[] entities = new String[]{"corpamag","CVS"};
          System.out.println("result:"+smm.insertEntitiesOfCaseStudio("15",entities));
     }

    /**
     * Test of main method, of class SectorManglaresModel.
     */
   
    public void testMain() {
    }

    /**
     * Test of actualizarSector method, of class SectorManglaresModel.
     */
    
    public void testActualizarSector() {
    }

    /**
     * Test of getTodosLosSectoresSectoresConEstadistica method, of class SectorManglaresModel.
     */
   
    public void testGetTodosLosSectoresSectoresConEstadistica() {
    }

    /**
     * Test of getAllSectorWithStatisticByDepartament method, of class SectorManglaresModel.
     */
    
    public void testGetAllSectorWithStatisticByDepartament() {
    }

    /**
     * Test of getTodosLosSectoresEstadisticaTasaCrecimiento method, of class SectorManglaresModel.
     */
    
    public void testGetTodosLosSectoresEstadisticaTasaCrecimiento() {
    }

    /**
     * Test of getTodosLosSectoresDelDepartamento method, of class SectorManglaresModel.
     */
   
    public void testGetTodosLosSectoresDelDepartamento() {
    }
    
}
