/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.org.invemar.ushahidi;

import co.org.invemar.siam.util.TesterConexion;
import co.org.invemar.siam.util.Testmysql;
import co.org.invemar.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jdk.nashorn.internal.objects.NativeRegExp.test;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author usrsig15
 */
public class ManagmentFormFieldUshahidiTest { 
  
    
   
    
    
    public static void setUpClass() {
    }
    
  
    public static void tearDownClass() {
    }
    
 
    public void setUp() {
    }
    
   
    public void tearDown() {
    }
    
    
    @Test
    public void getJsonObjectTest(){
         ManagmentFormFieldUshahidi mffu = new ManagmentFormFieldUshahidi();
         Category c = new Category();
         c.setCategoryTitle("prueba");
         c.setId("5");
         c.setCategoryVisible("1");
         c.setCategoryDescription("des");
         System.out.println(mffu.getJsonCategory(c));
    }
    
  
   
    public void getCategoryParentTest(){
       ManagmentFormFieldUshahidi mffu = new ManagmentFormFieldUshahidi();
      
       Testmysql testMysql = new Testmysql();
       Connection conmysql=testMysql.connect("jdbc:mysql://192.168.3.90:3306/ushahidiegreta", "egretta", "startegretta158"); 
       
       System.out.println(mffu.getCategoryParent("144", conmysql)) ;
    }    
    
    public void testManagmentUnit(){
       ManagmentFormFieldUshahidi mffu = new ManagmentFormFieldUshahidi();
       TesterConexion tc = new TesterConexion();
       Connection conOracle=tc.connect("manglar", "laguncularia");
       
       Testmysql testMysql = new Testmysql();
       Connection conmysql=testMysql.connect("jdbc:mysql://192.168.3.90:3306/ushahidiegreta", "egretta", "startegretta158");
       
       String newString="";
        try {
            
          // mffu.deleteItemFormField("43748", "sigmaform_field", conmysql, 8);    
         // mffu.deleteItemFormField("43749", "sigmaform_field", conmysql, 8);  
           mffu.deleteItemFormField("43746", "sigmaform_field", conmysql, 8); 
           mffu.deleteItemFormField("43751", "sigmaform_field", conmysql, 8); 
           mffu.deleteItemFormField("43750", "sigmaform_field", conmysql, 8); 
           mffu.deleteItemFormField("43745", "sigmaform_field", conmysql, 8);
           mffu.deleteItemFormField("43747", "sigmaform_field", conmysql, 8); 
         
           
        } catch (SQLException ex) {
            Logger.getLogger(ManagmentFormFieldUshahidiTest.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    
    
   
    public void testDeleteSectores(){
       ManagmentFormFieldUshahidi mffu = new ManagmentFormFieldUshahidi();
       TesterConexion tc = new TesterConexion();
       Connection conOracle=tc.connect("manglar", "laguncularia");
       
       Testmysql testMysql = new Testmysql();
       Connection conmysql=testMysql.connect("jdbc:mysql://192.168.3.90:3306/ushahidiegreta", "egretta", "startegretta158");       
       String newString="";
        try {            
           mffu.deleteItemFormField("43748", "sigmaform_field", conmysql, 16);                      
           
        } catch (SQLException ex) {
            Logger.getLogger(ManagmentFormFieldUshahidiTest.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    
    
 
    public void testfillFieldSectores(){
        
       ManagmentFormFieldUshahidi mffu = new ManagmentFormFieldUshahidi();
       TesterConexion tc = new TesterConexion();
       Connection conOracle=tc.connect("manglar", "laguncularia");
       
       Testmysql testMysql = new Testmysql();
       Connection conmysql=testMysql.connect("jdbc:mysql://192.168.3.90:3306/ushahidiegreta", "egretta", "startegretta158");
      
       mffu.fillFieldSectores(conOracle, conmysql);
       System.out.println("executed fill field sectores...");
    }
    
    
    public void testfillFieldManagmentUnit(){
        
       ManagmentFormFieldUshahidi mffu = new ManagmentFormFieldUshahidi();
       TesterConexion tc = new TesterConexion();
       Connection conOracle=tc.connect("manglar", "laguncularia");
       
       Testmysql testMysql = new Testmysql();
       Connection conmysql=testMysql.connect("jdbc:mysql://192.168.3.90:3306/ushahidiegreta", "egretta", "startegretta158");
      
       mffu.fillFieldManagmentUnit(conOracle, conmysql);
       System.out.println("executed fill ...");
    }
    
    /**
     * Test of updateFormField method, of class ManagmentFormFieldUshahidi.
     */
 
    public void testUpdateFormField() {
        System.out.println("updateFormField");
        String tableName = "";
        String jdbcResource = "";
        String value = "";
        long id = 0L;
        ManagmentFormFieldUshahidi instance = new ManagmentFormFieldUshahidi();
        boolean expResult = false;
      //  boolean result = instance.updateFormField(tableName, jdbcResource, value, id);
      //  assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findDefaultValueFormField method, of class ManagmentFormFieldUshahidi.
     */

    public void testFindDefaultValueFormField() {
        System.out.println("findDefaultValueFormField");
        String tableName = "";
        String jdbcResource = "";
        long id = 0L;
        ManagmentFormFieldUshahidi instance = new ManagmentFormFieldUshahidi();
        String expResult = "";
        //String result = instance.findDefaultValueFormField(tableName, jdbcResource, id);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of fillFieldWithRealData method, of class ManagmentFormFieldUshahidi.
     */
  
    public void testFillFieldWithRealData() {
        System.out.println("fillFieldWithRealData");
        ManagmentFormFieldUshahidi instance = new ManagmentFormFieldUshahidi();
      //  instance.fillFieldWithRealData();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
