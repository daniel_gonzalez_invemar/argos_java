<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true"  import="co.org.invemar.user.UserPermission" %>
<%
		String username = null;
		UserPermission userp = null;
		String projectid = null, projectname=null, projecttitle=null;
		String CSS=null;
    	try{
        	username = session.getAttribute("username").toString();
        	projectid = session.getAttribute("projectid").toString();
        	projectname = session.getAttribute("projectname").toString();
        	projecttitle = session.getAttribute("projecttitle").toString();
                CSS = (String) session.getAttribute("CSSPRO");
			userp = (UserPermission)session.getAttribute("userpermission");
        	   		
    	}catch(Exception e){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
	   	}
		
		 if(username==null){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}
try{
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="this is my page">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="css/siamcss.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/cmar.css" type="text/css">
  <script type="text/javascript" src="js/lib/prototype.js"></script>
  <script type="text/javascript" src="js/lib/controlmodal.js"></script>
  <script type="text/javascript" src="js/userpanel.js"></script>
   <link href="css/<%=CSS%>" rel="stylesheet" type="text/css">
</head>

<body marginwidth="0" marginheight="0">

<%@ include file="bannerp.jsp" %>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr class="tabla_fondotitulo">
      <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
      <td width="364" class="linksnegros">Cont&aacute;ctenos</td>
      <td width="365" class="texttablas"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0"
        cellspacing="0">
          <tbody>
            <tr>
              <td background="images/dotted_line.gif"><img
                src="images/dotted_line.gif" width="12" height="3"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/spacer.gif" width="3" height="3"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
  <tbody>
    <tr class="tabla_fondotitulo2">
      <td width="7" > </td>
      <td width="370" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp; <a href="index.jsp">Inicio</a> &gt; <a href="about.jsp" id="route">Legal </a> </td>
      <td width="371" class="texttablas"><div align="right"></div></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr>
      <td colspan="3" ><p align="justify" class="texttablas"><br>
          </p>      </td>
    </tr>
    <tr>
      <td ><p align="justify" class="texttablas"><br>
      </p></td>
      <td colspan="2"  class="texttablas"><strong>Compromiso de acceso y uso</strong></td>
    </tr>
    
    <tr>
      <td ><p align="justify" class="texttablas"><br>
      </p></td>
      <td colspan="2"  class="texttablasreg"><p class="textnormal">El usuario se compromete a cumplir las
        siguientes condiciones de uso de la informaci&oacute;n proporcionada:</p>
        <p class="textnormal"> * Los datos no deben ser traspasados de ninguna manera a terceros<br>
          * No podr&aacute;n vender la informaci&oacute;n que les sea suministrada.<br>
          * En los productos de informaci&oacute;n que de el se generen, sean impresos
          o electr&oacute;nicos, se debe reconocer la coautoria al Instituto de Investigaciones
          Marinas y Costeras &#8220;Jos&eacute; Benito Vives de Andr&eacute;is&#8221; -
          INVEMAR.<br>
          * Se debe citar en d&oacute;nde quiera que se usen los datos o informaci&oacute;n
          y en los productos de informaci&oacute;n que de el se generen sean impresos
          o electr&oacute;nicos de la siguiente manera: &quot;INVEMAR. ARGOS [en l&iacute;nea]:
          Sistema de Soporte Multitem&aacute;tico para el monitoreo Ambiental. [Santa Marta]: Instituto
          de investigaciones Marinas y Costeras &#8220;Jos&eacute; Benito Vives de Andr&eacute;is&#8221;,
          [Abr. 2009]. &lt;http://cinto.invemar.org.co/cmar&gt; [Consulta: 4/5/2009
          9:16 AM ]&quot;<br>
          * Cu&aacute;ndo se trate de una publicaci&oacute;n impresa o video se debe
          enviar al Instituto de Investigaciones Marinas y Costeras una copia del trabajo
          final.</p>
        <p class="textnormal">No se garantiza la precisi&oacute;n global
          de los datos. El solicitante es advertido de posibles errores
          en los datos suministrados, y
          asume la responsabilidad de realizar los chequeos necesarios para
          detectarlos, corregirlos e interpretarlos de manera acorde. El
          solicitante hace uso de los datos bajo su propia responsabilidad.</p>
        <p class="textnormal">El solicitante acepta las limitaciones existentes en los datos,
          provengan de su naturaleza o que sean impuestas como resultado
          de este acuerdo.</p>    </td>
    </tr>
    
    
    <tr>
      <td colspan="3" ><p align="justify">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<%@ include file="footer.jsp" %>

<%
}catch(Exception e){
		 %>
<script type="text/javascript">location.href="login.jsp"</script>
		 <%
}
%>
</body>
</html>