<%@page contentType="text/html; charset=iso-8859-1" language="java" session="true" import="co.org.invemar.user.UserPermission" %>
<%
    String username = null;
    UserPermission userp = null;
    String projectid = null, projectname = null, projecttitle = null, pais = null;
    String CSS = "siamcss.css";
    String userID = "";
    String idRol = "";
    String idEntity="";

    if (null != session.getAttribute("username")) {
        try {
            username = session.getAttribute("username").toString();
            projectid = session.getAttribute("projectid").toString();
            projectname = session.getAttribute("projectname").toString();
            projecttitle = session.getAttribute("projecttitle").toString();
            userp = (UserPermission) session.getAttribute("userpermission");
            pais = session.getAttribute("pais").toString();
            
            idEntity = (String) session.getAttribute("entityid").toString();
           
            
            CSS = (String) session.getAttribute("CSSPRO");
            idRol = (String) session.getAttribute("idrol");
            userID = (String) session.getAttribute("userid");

        } catch (Exception e) {
            e.printStackTrace();
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%
    }
} else if (username == null) {
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%    }
    try {
%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="favicon.ico">

        <title>Plataforma de monitoreo-ARGOS</title>
        <link href="css/<%=CSS%>" rel="stylesheet" type="text/css">

        <!-- Bootstrap core CSS -->
        <link href="responsive/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="responsive/css/cover.css" rel="stylesheet">
        <link href="responsive/css/log.css" rel="stylesheet">
        <link href="responsive/css/color.css" rel="stylesheet">
        <link href="responsive/css/layout.css" rel="stylesheet">
        <link href="http://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script  type="text/javascript">
            function cargaDatosEntidad() {

                $.get("/argos/DatosEntidades?entidad=INVEMAR", function(data) {
                    $("#entidad").html(data);

                });
            }

        </script>
    </head>
    <body onload="cargaDatosEntidad()">
        <header id="mainHeader" class="navbar-fixed-top" role="banner">
            <div class="container">
                <nav class="navbar navbar-default scrollMenu" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="index.html"><img src="imagenAction?codigo=<%=session.getAttribute("projectid").toString()%>" alt="Invemar" height="100%" /></a>                         
                        <a class="navbar-brand " href="index.html"><img src="http://siam.invemar.org.co/siam/plantillaSitio/img/MADSminambientergbhorizontal2012.png" alt="logo MADS" height="100%" /></a> 
                        <a class="navbar-brand" href="#"><h3>Plataforma de monitoreo</h3></a>
                    </div>

                    <div class="collapse navbar-collapse navbar-ex1-collapse" id="scrollTarget">                       

                    </div>
                </nav>
            </div>
            <section id="top" >
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 hidden-sm hidden-xs">
                            <ul class="nav navbar-nav nb-n1 pull-right">
                                <li><a href="/argos/login?action=cerrarSession" style="text-transform: none">Salir de forma segura.</a></li>
                            </ul>
                            <ul class="nav navbar-nav pull-left">
                                <li>Estas es inicio:</li>                                                              

                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </header>
        <!-- secc-publicaciones -->
        <section class="slice" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">                        
                        <h2 class="subTitle"><%=projectname%></h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row tab">
                    <div class="col-md-3 tlf">
                        <ul class="nav nav-pills nav-stacked admin-menu">
                            <li class="active"><a href="#" data-target-id="funcP"></i>Funciones primarias</a></li>
                            <li><a href="#" data-target-id="funcS">Funciones secundarias</a></li>
                            <li><a href="#" data-target-id="funcV">Funciones de visualización</a></li>
                            <!--li><a href="http://www.jquery2dotnet.com" data-target-id="charts"><i class="fa fa-bar-chart-o fa-fw"></i>Charts</a></li>
                            <li><a href="http://www.jquery2dotnet.com" data-target-id="table"><i class="fa fa-table fa-fw"></i>Table</a></li>
                            <li><a href="http://www.jquery2dotnet.com" data-target-id="forms"><i class="fa fa-tasks fa-fw"></i>Forms</a></li>
                            <li><a href="http://www.jquery2dotnet.com" data-target-id="calender"><i class="fa fa-calendar fa-fw"></i>Calender</a></li>
                            <li><a href="http://www.jquery2dotnet.com" data-target-id="library"><i class="fa fa-book fa-fw"></i>Library</a></li>
                            <li><a href="http://www.jquery2dotnet.com" data-target-id="applications"><i class="fa fa-pencil fa-fw"></i>Applications</a></li>
                            <li><a href="http://www.jquery2dotnet.com" data-target-id="settings"><i class="fa fa-cogs fa-fw"></i>Settings</a></li-->
                        </ul>
                    </div>
                    <div class="col-md-9 well well1 admin-content" id="funcP">
                        <p>Funciones primarias</p>
                        <div class="bs-glyphicons">
                            <ul class="bs-glyphicons-list">
                                <%
                                    if (userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO) || userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)) {%>
                                <a href="useradmin.jsp">
                                    <li>
                                        <span class="glyphicon glyphicon-user"></span>
                                        <i class="glyphicon glyphicon-user"></i>
                                        <span class="glyphicon-class">Administración de usuarios</span>
                                    </li>
                                </a>	
                                <%}%>
                                <%
                                    if (userp.couldUser(UserPermission.VER_MI_HISTORIAL_DE_EVENTOS_PARA_EL_PROYECTO_ACTUAL) || userp.couldUser(UserPermission.VER_HISTORIAL_DE_EVENTOS_DE_LA_ENTIDAD_PARA_EL_PROYECTO_ACTUAL)) {%>		  
                                <a href="logadmin.jsp">
                                    <li>
                                        <span class="glyphicon glyphicon-time"></span>
                                        <span class="glyphicon-class">Historial de eventos</span>
                                    </li>
                                </a>
                                <%}%>

                                <%
                                    if (userp.couldUser(UserPermission.ADMINISTRAR_ROLES)) {%>      
                                <a href="roleadmin.jsp">
                                    <li>
                                        <span class="glyphicon glyphicon-user"></span>
                                        <span class="glyphicon-class">Administrar Roles</span>
                                    </li>
                                </a> 

                                <a href="http://portete.invemar.org.co:7003/Argos/faces/referentes?user=<%=userID%>">
                                    <li>
                                        <span class="glyphicon glyphicon-filter"></span>
                                        <span class="glyphicon-class">Administrar Referentes</span>
                                    </li>
                                </a>

                                <a href="/argos/faces/administracion/index.xhtml">
                                    <li>
                                        <span class="glyphicon glyphicon-list-alt"></span>
                                        <span class="glyphicon-class">Administrar Proyectos</span>
                                    </li>
                                </a>

                                <%}%>

                                <%
                                    if (userp.couldUser(UserPermission.INGRESAR_DATOS)) {%>
                                <a href="infoassistant.jsp">
                                    <li id="subirActualizar">
                                        <span class="glyphicon glyphicon-open"></span>
                                        <span class="glyphicon-class">Subir/actualizar</span>
                                    </li></a>
                                    <%}%>


                            </ul>


                        </div>


                        <%
                            if (projectid.equalsIgnoreCase("2239")) {%>      
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-primary" >
                                    <div class="panel-heading" >
                                        <h3 class="panel-title">
                                            Herramientas SIGMA</h3>
                                        <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-minus"></i></span>
                                    </div>
                                    <div class="panel-body " >
                                        <div class="bs-glyphicons">
                                            <ul class="bs-glyphicons-list">
                                                <a href="redirectSigma.jsp">
                                                    <li>
                                                        <span class="glyphicon glyphicon-tree-deciduous"></span>
                                                        <span class="glyphicon-class">Caracterización de los bosques</span>
                                                    </li></a>
                                                <a href="infoassistant.jsp">
                                                    <li>
                                                        <span class="glyphicon glyphicon-open"></span>
                                                        <span class="glyphicon-class">Subir/actualizar modulo de estado</span>
                                                    </li></a>

                                                <a href="http://cinto.invemar.org.co/egreta/reports/submit" target="_blank">
                                                    <li>
                                                        <span class="glyphicon glyphicon-open"> </span>
                                                        <span class="glyphicon-class">Subir/actualizar datos modulo de presiones</span>
                                                    </li></a>

                                                <a href="modulogestion.jsp?user=<%=userID%>&rol=<%=idRol%>">
                                                    <li>
                                                        <span class="glyphicon glyphicon-open"></span>
                                                        <span class="glyphicon-class">Subir/actualizar modulo de gestión</span>
                                                    </li></a>
                                                    <a href="http://cinto.invemar.org.co/share/page/" target="_blank">
                                                    <li>
                                                        <span class="glyphicon glyphicon-folder-open"></span>
                                                        <span class="glyphicon-class">Gestor documental</span>
                                                    </li></a>

                                                    <a href="http://gis.invemar.org.co/sigma_geo/" target="_blank">
                                                    <li>
                                                        <span class="glyphicon glyphicon-map-marker"></span>
                                                        <span class="glyphicon-class">Geovisor</span>
                                                    </li></a>

                                                    <a href="http://sigma.invemar.org.co/estadisticas-estructura" target="_blank">
                                                    <li>
                                                        <span class="glyphicon glyphicon-leaf"></span>
                                                        <i class="glyphicon glyphicon-stats"></i>
                                                        <span class="glyphicon-class">Consultar estadística de estructura</span>
                                                    </li></a>
                                                    <a href="http://sigma.invemar.org.co/fisicoquimicos" target="_blank">
                                                    <li>
                                                        <span class="glyphicon glyphicon-stats"></span>
                                                        <i class="glyphicon glyphicon-tint"></i>                                                        
                                                        <span class="glyphicon-class">Consultar estadísticas  Fisicoquímicos</span>
                                                    </li></a>
                                                    <a href="http://sigma.invemar.org.co/estadisticas-crecimiento" target="_blank">
                                                    <li>
                                                        <span class="glyphicon glyphicon-leaf"></span><i class="glyphicon glyphicon-stats"></i>
                                                        <span class="glyphicon-class">Consultar estadística de crecimiento</span>
                                                    </li></a>	    

                                                    <a href="http://sigma.invemar.org.co/estadisticas-regeneracion" target="_blank">
                                                    <li>
                                                        <span class="glyphicon glyphicon-leaf"></span><i class="glyphicon glyphicon-stats"></i>
                                                        <span class="glyphicon-class">Consultar estadística de regeneración</span>
                                                    </li></a>	                                                
                                                    <a href="http://cinto.invemar.org.co/egreta/page/index/2" target="_blank">
                                                    <li>
                                                        <span class="glyphicon glyphicon-search"></span>
                                                        <span class="glyphicon-class">Consulta modulo de presiones</span>
                                                    </li></a>
                                                    <a href="administracion/dashboardsigma/pages/tables/coordenadasindividuos.jsp" >
                                                    <li>
                                                        <span class="glyphicon glyphicon-open"></span>
                                                        <span class="glyphicon-class">Registrar coordenadas de individuos</span>
                                                    </li></a>
                                                    
                                                    <a href="docs/SIGMA/FT-Estructura.xlsx" >
                                                    <li>
                                                        <span class="glyphicon glyphicon-open"></span>
                                                        <span class="glyphicon-class">FT Estructura </span>
                                                    </li></a>
                                                    <a href="docs/SIGMA/FT-Estructura_inicial.xlsx" >
                                                    <li>
                                                        <span class="glyphicon glyphicon-open"></span>
                                                        <span class="glyphicon-class">FT Estructura Inicial </span>
                                                    </li>
                                                    </a>
                                                    
                                                    <a href="docs/SIGMA/FT-Estructuras_siguientes.xlsx" >
                                                    <li>
                                                        <span class="glyphicon glyphicon-open"></span>
                                                        <span class="glyphicon-class">FT Estructura Siguiente</span>
                                                    </li>
                                                    </a>
                                                    
                                                    <a href="docs/SIGMA/FT-Fisico- Químicos.xlsx" >
                                                    <li>
                                                        <span class="glyphicon glyphicon-open"></span>
                                                        <span class="glyphicon-class">FT Fisico Quimico</span>
                                                    </li>
                                                    </a>
                                                    
                                                    <a href="docs/SIGMA/FT-Regeneración natural.xlsx" >
                                                    <li>
                                                        <span class="glyphicon glyphicon-open"></span>
                                                        <span class="glyphicon-class">FT Estructura Regeneracion</span>
                                                    </li>
                                                    </a>
                                                    
                                                   
                                                    
                                            </ul>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>	
                        <%}%>

                        <%
                            if (projectid.equalsIgnoreCase("2027")) {

                        %>      
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-primary" >
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Herramientas REPCAR</h3>
                                        <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-minus"></i></span>
                                    </div>
                                    <div class="panel-body "   >
                                        <div class="bs-glyphicons">
                                            <ul class="bs-glyphicons-list">                                               

                                                <a href="http://cinto.invemar.org.co:9090/pentaho/Login">
                                                    <li>
                                                        <span class="glyphicon glyphicon-stats"></span>
                                                        <span class="glyphicon-class">Pentaho Reportes</span>
                                                    </li></a>                                               
                                                <a href="geovisor_unep.jsp?pais=<%=pais%>&up=706">
                                                    <li>
                                                        <span class="glyphicon glyphicon-map-marker"> </span>
                                                        <span class="glyphicon-class">Geovisor (SIG/GIS)</span>
                                                    </li></a>
                                                    <%
                                                        if (pais.equals("CO")) {
                                                    %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/co/agua/repcar.html">
                                                    <li>
                                                        <span class="glyphicon glyphicon-tint"></span>
                                                        <i class="glyphicon glyphicon-stats"></i>
                                                        <span class="glyphicon-class">Ver datos agua</span>    
                                                    </li></a>
                                                    <%
                                                        }
                                                        if (pais.equals("CR")) {
                                                    %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/cr/agua/repcar.html">
                                                    <li>
                                                        <span class="glyphicon glyphicon-glyphicon-tint"></span>
                                                        <i class="glyphicon glyphicon-stats"></i>
                                                        <span class="glyphicon-class">Ver datos agua</span>
                                                    </li></a>
                                                    <%
                                                        }
                                                        if (pais.equals("NI")) {
                                                    %>
                                                <a href="http://gis.invemar.org.co/manglares/">
                                                    <li>
                                                        <span class="glyphicon glyphicon-tint"></span>
                                                        <i class="glyphicon glyphicon-stats"></i>
                                                        <span class="glyphicon-class">Ver datos agua</span>
                                                    </li></a>
                                                    <%
                                                        }
                                                    %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/total/agua/repcar.html">
                                                    <li>
                                                        <span class="glyphicon glyphicon-tint"></span>
                                                        <i class="glyphicon glyphicon-stats"></i>
                                                        <span class="glyphicon-class">Ver total datos agua</span>
                                                    </li>
                                                </a>
                                                <% if (pais.equals("CO")) {%>
                                                <a href="http://cinto.invemar.org.co/argos/visor/co/sedimento/repcar.html">
                                                    <li>
                                                        <span class="glyphicon glyphicon-stats"></span>
                                                        <i class="glyphicon glyphicon-road"></i>
                                                        <span class="glyphicon-class">Ver datos sedimento</span>
                                                    </li>
                                                </a>
                                                <%}
                                                    if (pais.equals("CR")) {
                                                %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/cr/sedimento/repcar.html">
                                                    <li>
                                                        <span class="glyphicon glyphicon-stats"></span>
                                                        <i class="glyphicon glyphicon-road"></i>
                                                        <span class="glyphicon-class">Ver datos de sedimento</span>
                                                    </li>
                                                </a>	    
                                                <%
                                                    }
                                                    if (pais.equals("NI")) {
                                                %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/total/sedimento/repcar.html">
                                                    <li>
                                                        < <span class="glyphicon glyphicon-stats"></span>
                                                        <i class="glyphicon glyphicon-road"></i>
                                                        <span class="glyphicon-class">Ver datos de sedimento</span>
                                                    </li>
                                                </a>
                                                <%
                                                } else {%>

                                                <a href="http://cinto.invemar.org.co/argos/visor/total/sedimento/repcar.html">
                                                    <li>
                                                        <span class="glyphicon glyphicon-stats"></span>
                                                        <i class="glyphicon glyphicon-road"></i>
                                                        <span class="glyphicon-class">Ver datos sedimento</span>
                                                    </li>
                                                </a>
                                                <%}%>                                            
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>	
                        <%
                            }
                        %> 



                    </div>
                    <div class="col-md-9 well well1 admin-content" id="funcS">
                        <p>Funciones secundarias</p>

                        <div class="bs-glyphicons">
                            <ul class="bs-glyphicons-list">

                                <a href="usercpass.jsp">
                                    <li>
                                        <span class="glyphicon glyphicon-wrench"></span>
                                        <span class="glyphicon-class">Cambiar contraseña</span>
                                    </li></a>                                  
                                    <%
                                        if (userp.couldUser(UserPermission.INGRESAR_DATOS_EN_BRUTO)) {%>    
                                <a href="genericdbupload.jsp">
                                    <li>
                                        <span class="glyphicon glyphicon-open"></span>
                                        <span class="glyphicon-class">Subir datos en bruto</span>
                                    </li>
                                </a>
                                <%}%>  
                            </ul>
                        </div>


                    </div>
                    <div class="col-md-9 well well1 admin-content" id="funcV">
                        <p>Funciones de visualización</p>

                        <div class="bs-glyphicons">
                            <ul class="bs-glyphicons-list">

                                <a href="services?proy=<%=projectid%>&action=Proyecto">
                                    <li>
                                        <span class="glyphicon glyphicon-screenshot"></span><i class="glyphicon glyphicon-hdd"></i>
                                        <span class="glyphicon-class">Estaciones y metadata</span>
                                    </li></a>
                                <a href="#">
                                    <li>
                                        <span class="glyphicon glyphicon-screenshot"></span><i class="glyphicon glyphicon-list-alt"></i>
                                        <span class="glyphicon-class">Ver datos por estaciones</span>
                                    </li></a>   
                                    <%
                                        if (userp.couldUser(UserPermission.VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) || userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD) || userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_TODAS_LAS_ENTIDADES_PARTICIPANTES)) {%>		     
                                <a href="http://localhost:8084/argos/infodownload.jsp">
                                    <li>
                                        <span class="glyphicon glyphicon-save"></span><i class="glyphicon glyphicon-list-alt"></i>
                                        <span class="glyphicon-class">Descargar datos</span>
                                    </li></a>   
                                    <%}%>
                            </ul>


                        </div>


                    </div>
                    <div class="col-md-9 well well1 admin-content" id="charts">
                        Charts
                    </div>
                    <div class="col-md-9 well well1 admin-content" id="table">
                        Table
                    </div>
                    <div class="col-md-9 well well1 admin-content" id="forms">
                        Forms
                    </div>
                    <div class="col-md-9 well well1 admin-content" id="calender">
                        Calender
                    </div>
                    <div class="col-md-9 well well1 admin-content" id="library">
                        Library
                    </div>
                    <div class="col-md-9 well well1 admin-content" id="applications">
                        Applications
                    </div>
                    <div class="col-md-9 well well1 admin-content" id="settings">
                        Settings
                    </div>
                </div>
            </div>						



        </section>
        <!-- fin sec -->


        <!-- footer -->
        <footer>
            <section id="mainFooter">
                <div class="container" id="footer">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="footerWidget">
                                <img src="http://siam.invemar.org.co/siam/plantillaSitio/img/logowebinvemar.png" alt="Invemar" id="footerLogo" width="20%" height="20%" >
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="footerWidget">

                                <h3>Invemar</h3>
                                <address >
                                    <p id='entidad'>

                                    </p>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </footer>
        <section  id="footerRights">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Copyright © 2014 <a href="#" target="blank">Invemar</a> / All rights reserved.</p>
                    </div>
                    <!--div class="col-sm-6">
                            <ul class="socialNetwork pull-right">
                                    <li><a href="#" class="tips" title="follow me on Facebook"><i class="icon-facebook-1 iconRounded"></i></a></li>
                                    <li><a href="https://twitter.com/LIttleNeko1" class="tips" title="follow me on Twitter" target="_blank"><i class="icon-twitter-bird iconRounded"></i></a></li>
                                    <li><a href="#" class="tips" title="follow me on Google+"><i class="icon-gplus-1 iconRounded"></i></a></li>
                                    <li><a href="#" class="tips" title="follow me on Linkedin"><i class="icon-linkedin-1 iconRounded"></i></a></li>
                                    <li><a href="#" class="tips" title="follow me on Dribble"><i class="icon-dribbble iconRounded"></i></a></li>
                            </ul>     
                    </div-->
                </div>
            </div>
        </section>
        <!-- End footer -->



        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="responsive/js/bootstrap.min.js"></script>        
        <script>
        $(document).ready(function()
        {
            var navItems = $('.admin-menu li > a');
            var navListItems = $('.admin-menu li');
            var allWells = $('.admin-content');
            var allWellsExceptFirst = $('.admin-content:not(:first)');

            allWellsExceptFirst.hide();
            navItems.click(function(e)
            {
                e.preventDefault();
                navListItems.removeClass('active');
                $(this).closest('li').addClass('active');

                allWells.hide();
                var target = $(this).attr('data-target-id');
                $('#' + target).show();
            });
        });

        $(document).on('click', '.panel-heading span.clickable', function(e) {
            var $this = $(this);
            if (!$this.hasClass('panel-collapsed')) {
                $this.parents('.panel').find('.panel-body').slideUp();
                $this.addClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
            } else {
                $this.parents('.panel').find('.panel-body').slideDown();
                $this.removeClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
            }
        });
        $(document).on('click', '.panel div.clickable', function(e) {
            var $this = $(this);
            if (!$this.hasClass('panel-collapsed')) {
                $this.parents('.panel').find('.panel-body').slideUp();
                $this.addClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
            } else {
                $this.parents('.panel').find('.panel-body').slideDown();
                $this.removeClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
            }
        });
        $(document).ready(function() {
            $('.panel-heading span.clickable').click();
            $('.panel div.clickable').click();
        });
        </script>
        <%
        } catch (Exception e) {
        %>
        <script type="text/javascript">location.href = "login.jsp"</script>
        <%                     }
        %>        

    </body>
</html>


