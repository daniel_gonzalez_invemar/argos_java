<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true" %>
<%
    String username = null;
    String thematicId = null;
    String projectid = null, projectname = null, projecttitle = null;
    String CSS = "siamcss.css";

    try {
        username = session.getAttribute("username").toString();
        projectid = session.getAttribute("projectid").toString();
        projectname = session.getAttribute("projectname").toString();
        projecttitle = session.getAttribute("projecttitle").toString();
        CSS = (String) session.getAttribute("CSSPRO");

    } catch (Exception e) {
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%
    }
 System.out.println("username:"+username);
    if (username == null) {
        System.out.println("username is null:"+username);
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%
    }

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
        <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
        <meta http-equiv="description" content="this is my page">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="css/<%=CSS%>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/cmar.css" type="text/css">
        <script type="text/javascript" src="js/lib/prototype.js"></script>
        <script type="text/javascript" src="js/lib/scriptaculous/scriptaculous.js?load=effects"></script>
        <script type="text/javascript" src="js/lib/controlmodal.js"></script>
        <%
            thematicId = request.getParameter("thematicid");

            if (thematicId != null) {
                out.println("<script type='text/javascript' src='js/thematics/" + thematicId + "/filter.js'></script>");
            }

        %>
        <script type="text/javascript" src="js/submitinfoassistant.js"></script>
    </head>

    <body marginwidth="0" marginheight="0">

        <%@ include file="bannerp.jsp" %>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr class="tabla_fondotitulo">
                    <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
                    <td width="364" class="linksnegros">Ingreso de informaci&oacute;n al sistema</td>
                    <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
            <tbody>
                <tr>
                    <td>
                        <table width="100%" border="0" align="center" cellpadding="0"
                               cellspacing="0">
                            <tbody>
                                <tr>
                                    <td background="images/dotted_line.gif"><img
                                            src="images/dotted_line.gif" width="12" height="3"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><img src="images/spacer.gif" width="3" height="3"></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
            <tbody>
                <tr	class="tabla_fondotitulo2">
                    <td width="9" >&nbsp;</td>
                    <td width="503" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="index.jsp">Inicio</a> &gt; <a href="infoassistant.jsp">Subir/Actualizar datos </a> </td>
                    <td width="238" class="texttablas"><div align="right"><a href="ayuda_upload.jsp" target="_blank">Intrucciones de uso</a></div></td>
                </tr>
                <tr>
                    <td colspan="3" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr>
                    <td colspan="3" ><br/>
                        <p align="center" class="asistanttitle">ASISTENTE PARA EL INGRESO DE
                            INFORMACI&Oacute;N AL SISTEMA</p>
                        <p align="justify" class="texttablas">&nbsp;&nbsp; Por favor siga los
                            pasos indicados por &eacute;ste asistente para evitar errores en
                            el proceso y agilizar de &eacute;sta forma su trabajo<br>
                            <br>
                        </p>      </td>
                </tr>
                <tr>
                    <td colspan="3" ><table id="tablestep1" width="724" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasoactual.gif">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="actualstepnumber">1</div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="actualsteptitle">Seleccionar
                                                                tem&aacute;tica</div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(2);">2</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(2);">Descargar
                                                                    referentes previos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(3);">3</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(3);">Incluir
                                                                    nuevos datos referentes</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(4);">4</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(4);">Descargar
                                                                    referentes completos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(5);">5</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(5);">Descargar
                                                                    hoja para incluir datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(6);">6</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(6);">Diligenciar
                                                                    datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(7);">7</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(7);">Preparar
                                                                    datos para ingreso al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="87"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasofinal.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(8);">8</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(8);">Ingresar
                                                                    datos al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="91">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="8"><div align="center" class="stetptitle">
                                        <p>1. SELECCIONAR TEM&Aacute;TICA</p>
                                    </div>
                                    <div align="center">
                                        <select id="thematiclist" name="tematica">
                                            <option value="no">Seleccione tem&aacute;tica
                                        </select>
                                    </div>
                                    <p align="center">
                                        <input id="buttongoto2" name="Siguiente" type="button" value="Paso 2 &gt;" disabled="true">
                                    </p>            </td>
                            </tr>
                        </table>
                        <table id="tablestep2" width="724" border="0" align="center" cellpadding="0" cellspacing="0" style="display:none;">
                            <tr>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(1);">1</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(1);">Seleccionar
                                                                    tem&aacute;tica</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasoactual.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="actualstepnumber">2</div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="actualsteptitle">Descargar
                                                                referentes previos</div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(3);">3</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(3);">Incluir
                                                                    nuevos datos referentes</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(4);">4</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(4);">Descargar
                                                                    referentes completos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(5);">5</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(5);">Descargar
                                                                    hoja para incluir datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(6);">6</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(6);">Diligenciar
                                                                    datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(7);">7</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(7);">Preparar
                                                                    datos para ingreso al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="87"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasofinal.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(8);">8</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(8);">Ingresardatos
                                                                    al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="91">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="724" colspan="8">
                                    <div align="center" class="stetptitle">
                                        <p>2. DESCARGAR REFERENTES PREVIOS</p>
                                    </div>
                                    <p align="center">
                                        <input id="buttonbackto1" name="buttonbackto1" type="button" value="&lt; Paso 1">&nbsp;
                                        <input id="buttongoto3" name="buttongoto3" type="button" value="Paso 3 &gt;">
                                    </p>              
                                    <div class="texttablas">
                                        <p align="justify">Es necesario alimentar el sistema con los datos referentes
                                            a:</p>
                                        <p align="justify"> * El proyecto<br>
                                            * Las estaciones o puntos de recolecci&oacute;n de las muestras<br>
                                            * Los Investigadoresque participar&oacute;n en la recolecci&oacute;n,
                                            identificaci&oacute;n, digitalizaci&oacute;n, y/o revisi&oacute;n de
                                            los registros biol&oacute;gicos</p>
                                        <p align="justify">Utilice la hoja de referentes previos para conocer cu&aacute;les
                                            de los datos mencionados se encuentran en el sistema, y
                                            tener acceso a las listas de c&oacute;digos normalizados.</p>
                                        <p align="justify">Si tiene referentes bibliogr&aacute;ficos es recomendable
                                            anexarlos en esta etapa del proceso.</p>
                                    </div>
                                    <div align="center"><a id="linkpreviousinforef" href="javascript:void(0);" class="texttablas" style="display:none"><img src="images/download22.png"  border="0" align="absmiddle">&nbsp;DESCARGAR REFERENTES PREVIOS</a></div>
                                </td>
                            </tr>
                        </table>          
                        <table id="tablestep3" width="724" border="0" align="center" cellpadding="0" cellspacing="0" style="display:none;">
                            <tr>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(1);">1</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(1);">Seleccionar
                                                                    tem&aacute;tica</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(2);">2</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(2);">Descargar
                                                                    referentes previos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasoactual.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="actualstepnumber">3</div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="actualsteptitle">Incluir
                                                                nuevos datos referentes</div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(4);">4</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(4);">Descargar
                                                                    referentes completos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(5);">5</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(5);">Descargar
                                                                    hoja para incluir datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(6);">6</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(6);">Diligenciar
                                                                    datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(7);">7</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(7);">Preparar
                                                                    datos para ingreso al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="87"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasofinal.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(8);">8</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(8);">Ingresardatos
                                                                    al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="91">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <div align="center" class="stetptitle">
                                        <p>3. INCLUIR NUEVOS DATOS REFERENTES</p>
                                    </div>
                                    <p align="center">
                                        <input id="buttonbackto2" name="buttonbackto2" type="button" value="&lt; Paso 2">
                                        &nbsp;
                                        <input id="buttongoto4" name="buttongoto4" type="button" value="Paso 4 &gt;">
                                    </p>              

                                    <div class="texttablas">
                                        <p align="justify">Remitir al correo electr&oacute;nico <strong><a href="mailto:daniel.gonzalez@invemar.org.co">daniel.gonzalez@invemar.org.co</a></strong> (Administrador del sistema)
                                            la Hoja para incluir datos de referentes, para su validaci&oacute;n e ingreso al sistema. Especifique claramente sus datos personales y en general toda aquella informaci&oacute;n que permita identificar la fuente de los datos, la justificaci&oacute;n vinculante
                                            con el sistema y el prop&oacute;sito espec&iacute;fico.  El Administrador le notifcar&aacute; cuando sus datos
                                            de referencia hayan sido incluidos en el sistema o los
                                            resultados de los procesos de validaci&oacute;n, en caso
                                            de que no haya sido posible ingresar con certeza los datos
                                            remitidos.
                                        </p>
                                    </div>
                                    <div align="center"><a id="linkaddnewinforef" href="javascript:void(0);" class="texttablas"><img src="images/download22.png"  border="0" align="absmiddle">&nbsp;DESCARGAR HOJA PARA INCLUIR DATOS DE REFERENTES</a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table id="tablestep4" width="724" border="0" align="center" cellpadding="0" cellspacing="0" style="display:none;">
                            <tr>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(1);">1</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(1);">Seleccionar
                                                                    tem&aacute;tica</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(2);">2</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(2);">Descargar
                                                                    referentes previos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(3);">3</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(3);">Incluir
                                                                    nuevos datos referentes</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasoactual.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="actualstepnumber">4</div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="actualsteptitle">Descargar
                                                                referentes completos</div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(5);">5</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(5);">Descargar
                                                                    hoja para incluir datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(6);">6</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(6);">Diligenciar
                                                                    datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(7);">7</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(7);">Preparar
                                                                    datos para ingreso al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="87"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasofinal.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(8);">8</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(8);">Ingresardatos
                                                                    al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="91">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <div align="center" class="stetptitle">
                                        <p>4. DESCARGAR REFERENTES COMPLETOS</p>
                                    </div>
                                    <p align="center">
                                        <input id="buttonbackto3" name="buttonbackto3" type="button" value="&lt; Paso 3">
                                        &nbsp;
                                        <input id="buttongoto5" name="buttongoto5" type="button" value="Paso 5 &gt;">
                                    </p>              
                                    <div class="texttablas">
                                        <p align="justify">Descargar la hoja de Excel de Referentes
                                            Completos <strong>en el directorio c:\ingresoregistros</strong>. Verifique que contiene los
                                            referentes que solicito incluir en la
                                            etapa anterior del proceso.</p>
                                    </div>
                                    <div align="center"><a id="linkfullinforef" href="javascript:void(0);" class="texttablas" ><img src="images/download22.png"  border="0" align="absmiddle">&nbsp;DESCARGAR
                                            REFERENTES COMPLETOS</a></div>
                                </td>
                            </tr>
                        </table>
                        <table id="tablestep5" width="724" border="0" align="center" cellpadding="0" cellspacing="0" style="display:none;">
                            <tr>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(1);">1</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(1);">Seleccionar
                                                                    tem&aacute;tica</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(2);">2</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(2);">Descargar
                                                                    referentes previos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(3);">3</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(3);">Incluir
                                                                    nuevos datos referentes</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(4);">4</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(4);">Descargar
                                                                    referentes completos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasoactual.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="actualstepnumber">5</div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="actualsteptitle">Descargar
                                                                hoja para incluir datos</div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(6);">6</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(6);">Diligenciar
                                                                    datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(7);">7</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(7);">Preparar
                                                                    datos para ingreso al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                                <td width="87"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasofinal.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(8);">8</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(8);">Ingresardatos
                                                                    al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                  </td>
                                        </tr>
                                    </table>            </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="91">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <div align="center" class="stetptitle">
                                        <p>5. DESCARGAR HOJA PARA INCLUIR DATOS</p>
                                    </div>
                                    <p align="center">
                                        <input id="buttonbackto4" name="buttonbackto4" type="button" value="&lt; Paso 4">
                                        &nbsp;
                                        <input id="buttongoto6" name="buttongoto6" type="button" value="Paso 6 &gt;">
                                    </p>              

                                    <div class="texttablas">
                                        <p align="justify"><strong>Descargar en el
                                                directorio c:\ingresoregistros</strong> la
                                            hoja de Excel para ingresar los nuevos registros.
                                            Al abrir la hoja <strong>presione Ctrl + D</strong> para integrarla con los referentes descargados previamente.</p>
                                    </div>
                                    <div align="center"><a id="linknewdatasheet" href="javascript:void(0);" class="texttablas"><img src="images/download22.png"  border="0" align="absmiddle">&nbsp;DESCARGAR
                                            HOJA PARA INCLUIR DATOS</a></div>
                                </td>
                            </tr>
                        </table>
                        <table id="tablestep6" width="724" border="0" align="center" cellpadding="0" cellspacing="0" style="display:none;">
                            <tr>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(1);">1</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(1);">Seleccionar
                                                                    tem&aacute;tica</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(2);">2</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(2);">Descargar
                                                                    referentes previos</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(3);">3</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(3);">Incluir
                                                                    nuevos datos referentes</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(4);">4</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(4);">Descargar
                                                                    referentes completos</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(5);">5</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(5);">Descargar
                                                                    hoja para incluir datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>              </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasoactual.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="actualstepnumber">6</div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="actualsteptitle">Diligenciar
                                                                datos</div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>              </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(7);">7</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(7);">Preparar
                                                                    datos para ingreso al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>              </td>
                                <td width="87"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasofinal.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(8);">8</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(8);">Ingresar
                                                                    datos al sistema</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="91">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <div align="center" class="stetptitle">
                                        <p>6. DILIGENCIAR DATOS</p>
                                    </div>
                                    <p align="center">
                                        <input id="buttonbackto5" name="buttonbackto42" type="button" value="&lt; Paso 5">
                                        &nbsp;
                                        <input id="buttongoto7" name="buttongoto7" type="button" value="Paso 7 &gt;">
                                    </p>              
                                    <div class="texttablas">
                                        <p align="justify">Al hacerlo, siga las instrucciones
                                            contenidas en los estandares para documentaci&oacute;n de la tem&aacute;tica espec&iacute;fica.               
                                        </p>
                                        <p align="justify">IMPORTANTE: NO inserte o elimine filas o columnas de la
                                            hoja de c&aacute;lculo. Cada celda esta relacionada con
                                            varias mas en otras hojas del libro, borrar celdas conlleva
                                            la p&eacute;rdida de estos vinculos lo que hace imposible
                                            mantener la integridad de los datos.</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table id="tablestep7" width="724" border="0" align="center" cellpadding="0" cellspacing="0" style="display:none;">
                            <tr>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(1);">1</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(1);">Seleccionar
                                                                    tem&aacute;tica</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(2);">2</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(2);">Descargar
                                                                    referentes previos</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(3);">3</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(3);">Incluir
                                                                    nuevos datos referentes</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(4);">4</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(4);">Descargar
                                                                    referentes completos</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(5);">5</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(5);">Descargar
                                                                    hoja para incluir datos</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(6);">6</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(6);">Diligenciar
                                                                    datos</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>              </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasoactual.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="actualstepnumber">7</div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="actualsteptitle">Preparar
                                                                datos para ingreso al sistema</div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>              </td>
                                <td width="87"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasofinal.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(8);">8</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(8);">Ingresar
                                                                    datos al sistema</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="91">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="8"><div align="center" class="stetptitle">
                                        <p>7. PREPARAR DATOS PARA INGRESO AL SISTEMA</p>
                                    </div>
                                    <p align="center">
                                        <input id="buttonbackto6" name="buttonbackto6" type="button" value="&lt; Paso 6">
                                        &nbsp;
                                        <input id="buttongoto8" name="buttongoto8" type="button" value="Paso 8 &gt;">
                                    </p>              

                                    <div class="texttablas">
                                        <p align="justify">Al terminar de diligenciar los datos <strong>presione
                                                Ctrl + K</strong>, esto generar&aacute; dos archivos, uno de
                                            nombre <strong>muestras.xls</strong> y otro llamado <strong>final.csv</strong>. &Eacute;ste &uacute;ltimo
                                            contiene toda la informaci&oacute;n que usted ha
                                            ingresado anteriormente y ser&aacute; subido al servidor
                                            en el siguiente paso. </p>
                                        <p align="justify">Por favor no realice ning&uacute;n cambio en el archivo resultante, esto podr&iacute;a ocasionar errores. </p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table id="tablestep8" width="724" border="0" align="center" cellpadding="0" cellspacing="0" style="display:none;">
                            <tr>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(1);">1</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(1);">Seleccionar
                                                                    tem&aacute;tica</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(2);">2</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(2);">Descargar
                                                                    referentes previos</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(3);">3</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(3);">Incluir
                                                                    nuevos datos referentes</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(4);">4</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(4);">Descargar
                                                                    referentes completos</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91" valign="top"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(5);">5</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(5);">Descargar
                                                                    hoja para incluir datos</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(6);">6</a></div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(6);">Diligenciar
                                                                    datos</a></div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                                <td width="91"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/paso.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="nonactualstepnumber"><a href="javascript:submitAsistant.jumpToStep(7);">7</a></div>                        </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="nonactualsteptitle"><a href="javascript:submitAsistant.jumpToStep(7);">Preparar
                                                                    datos para ingreso al sistema</a></div>                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>              </td>
                                <td width="87"><table width="87" height="75" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top" background="images/pasofinalactual.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="7%" height="32">&nbsp;</td>
                                                        <td width="86%" valign="top"><div class="actualstepnumber">8</div>                          </td>
                                                        <td width="7%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td valign="top"><div class="actualsteptitle">Ingresar
                                                                datos al sistema</div>                          </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>                    </td>
                                        </tr>
                                    </table>              </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="91">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <div align="center" class="stetptitle">
                                        <p>8. INGRESAR DATOS AL SISTEMA</p>
                                    </div>
                                    <p align="center">
                                        <input id="buttonbackto7" name="buttonbackto7" type="button" value="&lt; Paso 7">
                                    </p>              

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="texttablas">
                                        <tr>
                                            <td width="60%" valign="top">
                                                <form action="upload" method="post" name="uploadform" id="uploadform" target="target_upload" enctype="multipart/form-data">
                                                    <table width="97%" border="0" align="center" cellpadding="0" cellspacing="0" class="texttablas">
                                                        <tr>
                                                            <td><div align="justify">
                                                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="texttablas" id="uploadoptions">
                                                                        <tr>
                                                                            <td><div align="justify"><strong>Seleccione una operaci&oacute;n:</strong></div></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="42"><blockquote>
                                                                                    <div align="justify">
                                                                                        <label>
                                                                                            <input name="operation" id="operation" type="radio" value="newregister" checked="checked">
                                                                                            Ingresar nuevos registros</label>
                                                                                        <br><label>
                                                                                            <input name="operation" id="operation" type="radio" value="updateregister" disabled="disabled">
                                                                                            Actualizar registros</label> 
                                                                                        <select name="userlist" id="userlist" disabled="disabled">
                                                                                            <option value="no" selected>Actualizar datos de
                                                                                                mi propiedad</option>
                                                                                        </select>
                                                                                        <br>
                                                                                    </div>
                                                                                </blockquote></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><div align="justify">En el paso anterior, se gener&oacute; un archivo llamado <strong>final.csv</strong>, por favor haga click en el bot&oacute;n examinar, busque el archivo y despu&eacute;s presione el bot&oacute;n <strong>procesar archivo</strong>.</div></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><br />
                                                                                <strong>Buscar archivo a subir:</strong><br />
                                                                                <input name="filetouploadbox" type="file" id="filetouploadbox" size="50" />
                                                                                <br />
                                                                                <br />                                    </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="button" name="processfilebutton" id="processfilebutton" value="Procesar archivo">                                    </td>
                                                                        </tr>
                                                                    </table>
                                                                </div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td><table  width="100%" border="0" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF" class="texttablas" id="resultstable" style="display:none">
                                                                    <tr>
                                                                        <td>                              
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="texttablas">
                                                                                <tr>
                                                                                    <td colspan="3"> <strong>Resultados de
                                                                                            la operaci&oacute;n: </strong> </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3">                                          <blockquote>
                                                                                            <div id="processok" align="justify" style="display:none">
                                                                                                <p><font color="#006699"><strong><br>
                                                                                                            El
                                                                                                            archivo fue subido y procesado
                                                                                                            exit&oacute;samente, las muestras y la informaci&oacute;n asociada que se encontraban en el archivo han sido guardadas en la base de datos y est&aacute;n disponibles.</strong></font></p>
                                                                                                <p align="center"><a href="javascript:submitAsistant.restartAssistant();" >Reiniciar &eacute;ste asistente</a> || <a href="index.jsp">Ir a la p&aacute;gina principal </a></p>
                                                                                            </div>
                                                                                        </blockquote>  
                                                                                        <blockquote>
                                                                                            <div id="updateprocessok" align="justify" style="display:none">
                                                                                                <p><font color="#006699"><strong><br>
                                                                                                            La actualizaci&oacute;n fue realizada exit&oacute;samente.</strong></font></p>
                                                                                                <p align="center"><a href="javascript:submitAsistant.restartAssistant();" >Reiniciar &eacute;ste asistente</a> || <a href="index.jsp">Ir a la p&aacute;gina principal </a></p>
                                                                                            </div>
                                                                                        </blockquote>   
                                                                                        <blockquote> 
                                                                                            <div id="processerror" align="justify" style="display:none"><br/><font color="#FF0000"><strong>Se
                                                                                                        encontraron errores, por favor
                                                                                                        revise la hoja en la cu&aacute;l ingres&oacute; las muestras y verifique que no existan errores (tenga en cuenta las sugerencias mostradas), si es necesario repita los pasos anteriores de &eacute;ste asistente e intente subir el
                                                                                                        archivo
                                                                                                        nuevamente. </strong></font></div>
                                                                                        </blockquote>                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="texttablas" style="display:none" id="suggestiontable">
                                                                                            <tr>
                                                                                                <td><strong>Sugerencias:
                                                                                                    </strong></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><br><blockquote><div id="suggestion" align="justify">

                                                                                                        </div></blockquote></td>
                                                                                            </tr>
                                                                                        </table></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="7%">&nbsp;</td>
                                                                                    <td width="93%" colspan="2">
                                                                                        <table width="200" border="0" cellpadding="0" cellspacing="2" class="texttablas" id="generatedfiles" style="display:none">
                                                                                            <tr>
                                                                                                <td><strong>Archivos generados:</strong></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <table width="200" border="0" cellpadding="0" cellspacing="2" class="texttablas" id="logfileoptions" style="display:none">
                                                                                            <tr>
                                                                                                <td width="172"><img src="images/sheet.gif" width="16" height="16" align="absmiddle"><a id="linklogfile" target="_blank" href="javascript:void(0);">historial.log</a></td>
                                                                                                <td width="22"><img src="images/help.gif" width="16" height="16" align="absmiddle" id="logfilehelp"></td>
                                                                                            </tr>
                                                                                        </table>

                                                                                        <table width="200" class="texttablas" id="badfileoptions" border="0" cellspacing="2" cellpadding="0" style="display:none" >
                                                                                            <tr>
                                                                                                <td width="172"><img src="images/sheet.gif" width="16" height="16" align="absmiddle"><a id="linkbadfile" target="_blank" href="javascript:void(0);">registroserroneos.cvs</a>											</td>
                                                                                                <td width="22"><img src="images/help.gif" width="16" height="16" align="absmiddle" id="badfilehelp">											</td>
                                                                                            </tr>
                                                                                        </table>

                                                                                        <table width="200" class="texttablas" id="discardfileoptions" border="0" cellspacing="2" cellpadding="0" style="display:none" >
                                                                                            <tr>
                                                                                                <td width="172"><img src="images/sheet.gif" width="16" height="16" align="absmiddle"><a id="linkdiscardfile" target="_blank" href="javascript:void(0);">registrosdescartados.cvs</a>											</td>
                                                                                                <td width="22"><img src="images/help.gif" width="16" height="16" align="absmiddle" id="discardfilehelp">											</td>
                                                                                            </tr>
                                                                                        </table>										</td>
                                                                                </tr>
                                                                            </table>                              </td>
                                                                    </tr>
                                                                </table>							  </td>
                                                        </tr>
                                                    </table>
                                                </form></td>
                                        </tr>
                                    </table>
                                    <p align="justify">   
                                        <!-- This iframe is used as a place for the post to load -->
                                        <iframe id='target_upload' name='target_upload' src='' style='display: none'></iframe>
                                        <!-- This is the upload status area --></p>
                                </td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td colspan="3" ><p align="justify">&nbsp;</p></td>
                </tr>
            </tbody>
        </table>

        <%@ include file="footer.jsp" %>

        <div id="filterbox" style="display: none; z-index:1; position: absolute; top: 207px; left:342px; margin: 10px; padding: 0px; border-width: 0px; width: 327px; height: 130px;">
        </div>

    </body>

</html>
