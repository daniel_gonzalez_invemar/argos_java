<%@page import="org.apache.commons.lang.*"%>
<%@page import="co.org.invemar.siam.sibm.vo.Departamentos"%>
<%@page import="co.org.invemar.siam.sibm.vo.*"%>
<%@page contentType="text/html; charset=iso-8859-1" language="java" session="true"  import="co.org.invemar.user.UserPermission" %>
<%@page import="java.util.*"%>
<%@page import="co.org.invemar.util.ConnectionFactory"%>
<%@page import="co.org.invemar.siam.sibm.model.ProyectosDM"%>
<%@page import="java.sql.Connection"%>
<%
    response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server
%>

<%
    String username = null, projectname = null, projectid = null, projecttitle = null;

    /*
     * username = session.getAttribute("username").toString(); projectid =
     * session.getAttribute("projectid").toString(); projectname =
     * session.getAttribute("projectname").toString(); projecttitle =
     * session.getAttribute("projecttitle").toString(); UserPermission userp =
     * null;
     */
    ConnectionFactory cFactory = null;
    Connection connection = null;
    cFactory = new ConnectionFactory();
    ProyectosDM pdm = new ProyectosDM();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>
        <link href="../siamccs.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <META name="keywords" content="invemar, ciencia marina, investigac&iacute;on, marino, costera, costero">
        <meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">
        <link href="css/siamcss.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/cmar.css" type="text/css">
        <link rel="stylesheet" href="css/estacionesCam/tablesorter.css" type="text/css">
        <script type="text/javascript" language="javascript" src="js/estacionesCam/tablesorter/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/estacionesCam/tablesorter/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            
                
            
            
            function PopWindow(popPage,windowName,winWidth,winHeight)
            {
   
                //the center properties
                var winLeft = (screen.width -winWidth)/2;
                var winTop = (screen.height -winHeight)/2;

                newWindow = window.open(popPage,windowName,'width=' + winWidth + ',height='+ winHeight + ',top=' + winTop +',left=' + winLeft + ',resizable=yes,scrollbars=yes,status=yes');
            }
            
            function generarReporte() {
                var codigoDepart =  $('#selDepart').val();       
                var codigoSector =  $('#selSector').val(); 
                
                $.ajax({
                    type: "post",
                    url: "<%=request.getContextPath()%>/services",                   
                   data: "action=ReporteEstacionesREDCAMAction&proy=2050&codSector="+codigoSector+"&codDepar="+codigoDepart,
                    beforeSend: function(msg){ 
                        $('#sector').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='images/ajaxloader.gif' alt='Loading...'/>");
  
                    },
                    success: function(msg){
                        $('#sector').html('');
                                    

                    },
                    error: function(xml,msg){
                        $('#sector').html('<span style="color:red"><Error cargando los sectores monitoreados.</span>');
                  
                    }
                });
                
            }
            function getSectorByDepartament() {
                var codigoDepart =  $('#selDepart').val();          
               
                $.ajax({
                    type: "post",
                    url: "<%=request.getContextPath()%>/services",                   
                    data: "action=SectoresPorDepartamento&codDepar="+codigoDepart,
                    beforeSend: function(msg){ 
                        $('#sector').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='images/ajaxloader.gif' alt='Loading...'/>");
  
                    },
                    success: function(msg){
                        $('#sector').html('');                        
                        $('#sector').html(msg);                 

                    },
                    error: function(xml,msg){
                        $('#sector').html('<span style="color:red"><Error cargando los sectores monitoreados.</span>');
                  
                    }
                });
            }
            
            function buscarEstaciones() {
                var codigoDepart =  $('#selDepart').val();       
                var codigoSector =  $('#selSector').val();     
                
               
                
                $.ajax({
                    type: "post",
                    url: "<%=request.getContextPath()%>/services",                   
                    data: "action=listaEstacionesCam&proy=2050&codSector="+codigoSector+"&codDepar="+codigoDepart,
                    beforeSend: function(msg){ 
                        $('#listaestaciones').html("<div style='margin:auto;text-align:center'><img border='0'   style='margin-right:32px; margin-top: 32px;' src='images/ajaxloader.gif' alt='Loading...'/></div>");
  
                    },
                    success: function(msg){
                        $('#listaestaciones').html('');
                        $('#listaestaciones').html(msg);                 
                        $('.dataTable').dataTable();  
                    },
                    error: function(xml,msg){
                        $('#listaestaciones').html('<span style="margin:auto;text-align:center;color:red">Error listando las estaciones.</span>');
                  
                    }
                });
            }

            
            
            
        </script>
    </head>

    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="buscarEstaciones()" >        
        <div>            
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">

                <tr class="tabla_fondotitulo2">
                    <td width="9">&nbsp;</td>
                    <td width="503" class="texttablas"></td>
                    <td width="238" class="texttablas"><div align="right"></div></td>
                </tr>           

                <tr>
                    <td height="16" colspan="3" ><div align="center" class="linksnavinferior">

                            <table cellpadding="0" border="0"><tr>
                                    <td><span class="linksnegros">Estaciones REDCAM</span></td>                                        
                                </tr></table>
                            <input type="hidden" name="action" value="GenericProyecto" /> 

                        </div></td>
                </tr>

            </table>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="tabla_fondotitulo2"> 
                    <td  >
                        <table width="100%" border="0" cellspacing="0">
                            <td height="24"  align="left" class="stetptitle">Proyecto </td>
                            <td width="100%" align="right">

                            </td>
                        </table>
                    </td>
                </tr>
            </table>


            <table width="100%" border="0" align="center" cellpadding="4" cellspacing="1">


                <tr class="tabla_fondotitulo2">
                    <td nowrap="nowrap" class="texttitulo">Titulo</td>
                    <td nowrap="nowrap" class="texttitulo">Nombre alterno</td>
                    <td nowrap="nowrap" class="texttitulo">Periodo de ejecucion</td>
                </tr>
                <tr class="tabla_fondopagina">
                    <td class="texttablasreg"></td>
                    <td nowrap="nowrap" class="texttablas" ></td>
                    <td nowrap="nowrap" class="texttablas" ></td>
                </tr>
            </table>
            <div  id="parameters" style="margin:auto;text-align:center">
                <span id="titleFirtsParameter" style="font-weight: bold;">Departamentos con Monitoreo</span>
                <div id="Depar">
                    <select id="selDepart" name ="selDepart"  onchange="getSectorByDepartament()"> 
                        <%
                            connection = cFactory.createConnection();
                            List<Departamentos> listadepartamentos = pdm.findDepartamentByRedCam(connection);
                            System.out.println(listadepartamentos);
                            Iterator<Departamentos> it = listadepartamentos.iterator();
                            while (it.hasNext()) {
                                Departamentos departamento = it.next();
                                out.println("<option value=" + departamento.getCodigo() + ">" + departamento.getNombre() + "</option>");
                            }


                        %>


                    </select>
                </div>
                <span id="TitleSecondParameter" style="font-weight: bold;"></span>
                <div id="sector">

                </div>
                <div id="sessionbusqueda">
                    <input type="button" id="buscarbtn" name="buscarbtn" value="Buscar..." onclick="buscarEstaciones()" />
                </div>
            </div>

            <div style="margin:auto;text-align:right;padding-left: 5%;padding-right: 5%" >
                <a href="#" onclick="generarReporte()"><img src="" />Exportar a excel.</a>
                <span ></span>
            </div> 
            <div id="listaestaciones">

            </div>
        </div>
    </body>
</html>
