<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true" %>
<%    
    String username = null;
    
    try {
        username = session.getAttribute("username").toString();
        
    } catch (Exception e) {
    }    
    /*if (username != null) {
        response.sendRedirect("index.jsp");
    }*/

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
        <meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
        <meta http-equiv="description" content="this is my page" />
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
        <link rel="stylesheet" href="css/style.css" type="text/css" />
        <link rel="stylesheet" href="css/cmar.css" type="text/css" />
        <script type="text/javascript" src="js/lib/prototype.js"></script>
        <script type="text/javascript" src="js/lib/scriptaculous/scriptaculous.js?load=effects"></script>
        <script type="text/javascript" src="js/lib/controlmodal.js"></script>
        <script type="text/javascript" src="js/lib/yav/yav.js"></script>
        <script type="text/javascript" src="js/lib/yav/yav-config-es.js"></script>
        <script type="text/javascript" src="js/login.js"></script>
        <script type="text/javascript">
            
            var loginrules = new Array();
            loginrules[0] = 'email:correo electr&oacute;nico|required';
            loginrules[1] = 'email: |email';
            loginrules[2] = 'password:contrase&ntilde;a|required';
            
            var pregrules = new Array();
            pregrules[0] = 'prgemail:correo electr&oacute;nico|required';
            pregrules[1] = 'prgemail: |email';
            
        </script>    
        <link href="plantillaSitio/css/misiamccs.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
    </head>

    <body>

        <jsp:include page="plantillaSitio/headermodulosv3.jsp?idsitio=120" />
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td >  </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td ><p align="justify" class="texttablas"><br/>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td >
                        <form name="loginform" method="post" action="">
                            <table width="612" class="texttablas" id="logintable" style="display:none;text-align:center" >
                                <tr>
                                    <td width="17">&nbsp;</td>
                                    <td width="583"><strong>Ingresar al sistema </strong><br/>Proporcione sus datos de usuario para ingresar al
                                        sistema: </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Correo electr&oacute;nico: </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><!--<label title="label_error">-->
                                        <input name="email" type="text" id="email" value="daniel.gonzalez@invemar.org.co" size="30" title="txtemail"/> <span id="errorsDiv_email"/>
                                        <!--</label>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Contrase&ntilde;a:</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><!--<label title="label_error">-->
                                        <input name="password" type="password" id="password" size="30" value="startargos" title="password"/> <span id="errorsDiv_password"/>
                                        <!--</label>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><input name="login" type="button" id="login" value="Ingresar" title="btnIngresar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><a href="javascript:void(0);" class="texttablas" id="linkrecover">Olvid&eacute; mi
                                            contrase&ntilde;a.</a></td>
                                </tr>
                            </table>
                        </form>
                        <form name="pregform" action="" method="get">
                            <table width="613" class="texttablas" id="prgtable" style="display:none; text-align:center">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><strong >Olvid&eacute; mi contrase&ntilde;a!</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="17">&nbsp;</td>
                                    <td width="584">Proporcione su direcci&oacute;n de correo electr&oacute;nico: </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Correo electr&oacute;nico:</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><!--<label title="mensaje">-->
                                        <input name="prgemail" type="text" id="prgemail" size="30" title="pregemail"/> <span id="errorsDiv_prgemail"/>
                                        <!--</label>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><input name="prgbutton" type="button" id="prgbutton" value="Generar nueva contrase&ntilde;a" title="btnGenerar"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td> </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><a href="login.jsp" class="texttablas">Intentar nuevamente el
                                            ingreso al sistema.</a></td>
                                </tr>
                            </table>
                        </form>
                        <table width="743" class="texttablas">
                            <tr>
                                <td width="17">&nbsp;</td>
                                <td width="714"><div class="texttablas"><div id='errorresult' style="color:#FF0000;font-weight:bold"></div>
                                        <div id='okresult' style="color:#006699;font-weight:bold"></div></div></td>
                            </tr>
                        </table></td>
                </tr>
                <tr  >
                    <td ><p align="justify">&nbsp;</p></td>
                </tr>
            </tbody>
        </table>

        <%@ include file="plantillaSitio/footermodulesV3.jsp" %>

        <h1></h1>
    </body>
</html>
