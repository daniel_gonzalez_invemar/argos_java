<%-- 
    Document   : editarProyecto
    Created on : 23/11/2011, 04:51:05 PM
    Author     : usrsig15
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Editar Datos del Proyecto</title>        
        <link rel="stylesheet" href="../pluginsJS/tabz/myTabz.css" type="text/css" />      
        <script type='text/javascript' src='../js/lib/prototype.js'></script>
        <script type="text/javascript" src="../pluginsJS/tabz/myTabz.js" ></script>
        <script type="text/javascript" src="../pluginsJS/calendar/calendar.js"></script>
        <script type="text/javascript" src="../pluginsJS/calendar/calendar-es.js"></script>
        <script type="text/javascript" src="../pluginsJS/calendar/calendar-setup.js"></script>
        <link rel="stylesheet" href="../pluginsJS/calendar/calendar-blue.css" />       

        <script type="text/javascript">           
               
            var ActivateTabs=function(){
	
                if(typeof(MT)=='undefined'){
                    var MyTabs= new mt('tabs','div.my_tab');
					
                    MyTabs.removeTabTitles('h5.tab_title');
                    MyTabs.addTab('t1','Editar Proyecto-');
                    MyTabs.addTab('t2','Configuración Componente-');                   
                    MyTabs.makeActive('t1');
                }
					
            }	
	
           
		
        </script>        
    </head> 
    <body onload=" ActivateTabs('tabs');  ">  
        <div id="tabs">
            <div id="t1" class="my_tab">
                <h5 class="tab_title">Editar Proyecto-</h5>
                <div id="formularioEdicionproyectos">
                    <div><p>Código:</p></div>
                    <div><span>Título:</span></div>
                    <div><input type="text" id="titulotxt" name="titulotxt"/></div>
                    <div><span>Nombre Alterno:</span></div>
                    <div><input type="text" id="nombrealternotxt" name="nombrealternotxt"/></div>
                    <div><span>Fecha Inicio</span></div>
                    <div><input type="text" id="fechainiciotxt" name="fechainiciotxt" disabled="true"/><img src="../images/calendar.gif" width="20" height="20" id="filanzador" style="cursor: pointer; border: 0px solid red;" title="Date selector" onMouseOver="this.style.background='red';" onMouseOut="this.style.background=''" /></div>
                    <div><span>Fecha Finalización</span></div>
                    <div><input type="text" id="fechafinalizaciontxt" name="fechafinalizaciontxt" disabled="true"/><img src="../images/calendar.gif" width="20" height="20" id="fflanzador" style="cursor: pointer; border: 0px solid red;" title="Date selector" onMouseOver="this.style.background='red';" onMouseOut="this.style.background=''" /></div>
                    <div><span>Ejecutor:</span></div>
                    <div><input type="text" id="ejecutortxt" name="ejecutortxt"/></div>
                    <div><span>Sistema SIAM que administra los datos del registro</span></div>
                    <div><input type="text" id="sistemasiamcmb" name="sistemasiamcmb"/></div>
                    <div style="height: 50px;">
                        <div style="position: relative;float:left;">
                            <span style="clear: left">Logo Proyecto:</span>
                            <div style="clear: left"><input type="text" id="logoproyectofile" name="logoproyectofile" disabled="true"/></div> 
                        </div>
                        <div style="position: relative;float:left;padding-left: 10px; padding-top: 10px;">
                            Activar subir logo. <input type="checkbox" id="activarsubirlogochox" name="activarsubirlogochox" style="clear:right"/>
                        </div>
                    </div>
                    <div style="height: 50px;">
                        <div style="position: relative;float:left;">
                            <span style="clear:left">Css proyecto:</span>
                            <div style="clear: left"><input type="text" id="cssproyectofile" name="logoproyectofile" disabled="true" /></div>
                        </div>
                        <div style="position: relative;float:left;padding-left: 10px; padding-top: 10px;">
                            Activar subir css. <input type="checkbox" id="activarsubircsschox" name="activarsubircsschox" style="clear:right" />
                        </div>
                    </div>      

                    <div style="padding-top:10px;padding-bottom:10px;"><input type="button" id="actualizarbtn" name="actualizarbtn" value="Actualizar"/></div>

                    <div></div>       
                </div>	
            </div>
            <div id="t2" class="my_tab">
                <h5 class="tab_title">Configuración Componente-</h5>
                <p>
                    tab2: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Fusce eget pede. Ut faucibus urna. Donec sagittis. Suspendisse bibendum tortor sed metus. Vivamus aliquet. Sed mi dolor, sollicitudin sed, commodo sed, vestibulum cursus, ligula. Cras laoreet bibendum ante. Sed tristique quam id tellus. Donec sodales. Integer sit amet diam vitae tellus iaculis tincidunt. Curabitur mauris nulla, blandit id, eleifend at, facilisis adipiscing, urna. Vestibulum vel metus. Mauris lacinia, lectus nec tincidunt dictum, tortor erat pretium nulla, a cursus arcu neque vel nunc. Sed consectetuer laoreet est. Duis nec augue. Maecenas eu ipsum nec lectus feugiat iaculis. Suspendisse a nisl vitae neque consectetuer bibendum. Quisque non nulla.
                    Vivamus rhoncus, nisl quis elementum malesuada, nisi ligula aliquet ligula, sed fringilla ipsum lectus in leo. Quisque semper faucibus nunc. Nunc elit lacus, nonummy eu, egestas ultricies, ullamcorper sed, ipsum. Nulla elit lectus, venenatis id, fermentum ut, fringilla vel, erat. Morbi lorem urna, consectetuer ut, blandit id, dapibus sit amet, nunc. Nam nec nisi. Maecenas congue. Sed pulvinar imperdiet neque. Aenean aliquam nunc. Nunc feugiat nonummy felis. Integer diam justo, egestas commodo, hendrerit eget, blandit id, massa. Ut nec nunc. Cras dui purus, auctor sit amet, auctor sed, scelerisque eu, leo. Nullam erat lectus, mollis ut, molestie eget, dapibus ut, nisl. Aenean aliquet enim. Nunc arcu lacus, viverra rutrum, elementum a, posuere sed, risus. In hac habitasse platea dictumst. Sed facilisis vehicula orci. 
                </p>	
            </div>
        </div>
        <script type="text/javascript">
            Calendar.setup({

                inputField : "fechainiciotxt",// id del campo de texto
                ifFormat : "%d/%m/%Y", // formato de la fecha que se escriba en el campo de texto
                button : "filanzador" // el id del bot?n que lanzar? el calendario           

            });
  
            Calendar.setup({

                inputField : "fechafinalizaciontxt",// id del campo de texto
                ifFormat : "%d/%m/%Y", // formato de la fecha que se escriba en el campo de texto
                button : "fflanzador" // el id del bot?n que lanzar? el calendario*/

            });

        </script> 
    </body> 
</html>
