<%-- 
    Document   : editarProyecto
    Created on : 23/11/2011, 04:51:05 PM
    Author     : usrsig15
--%>

<%@page import="java.util.Iterator"%>
<%@page import="co.org.invemar.siam.sibm.vo.Proyecto"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="co.org.invemar.util.ConnectionFactory"%>
<%@page import="co.org.invemar.siam.sibm.model.ProyectosDM"%>
<%    
    String codigoProyecto = request.getParameter("codigoproyecto");
    String resultadoTransaccion = null;
           resultadoTransaccion= request.getParameter("mensaje");
    
    String titulo = "";
    String nombreAlterno = "";
    String fechaInicio = "";
    String fechafinalizacion = "";
    String ejecutor = "";
    String sistemaSIAMqueloadministra = "";
    String rutalogoproyecto = "";
    String rutacssadministra = "";
    String idmetadatopro = "0";
    boolean ocurriounError = false;
    String mensajeerror = "";
    byte[] bytelogo = null;
    try {
        ConnectionFactory cFactory = new ConnectionFactory();
        Connection connection = cFactory.createConnection();
        ProyectosDM proyecto = new ProyectosDM();
        List<Proyecto> lista = proyecto.findProjectsPorCodigo(connection, Long.parseLong(codigoProyecto));
        bytelogo = proyecto.getByteImagenLogoProyecto(connection, codigoProyecto);
        
        Iterator<Proyecto> it = lista.iterator();
        while (it.hasNext()) {
            Proyecto datosproyecto = it.next();
            titulo = datosproyecto.getTitulo();
            if (titulo == null) {
                titulo = "";
            }
            nombreAlterno = datosproyecto.getNombreAlterno();
            if (nombreAlterno == null) {
                nombreAlterno = "";
            }
            fechaInicio = datosproyecto.getFechaInicio();
            if (fechaInicio == null) {
                fechaInicio = "";
            }
            fechafinalizacion = datosproyecto.getFechaFinalizo();
            if (fechafinalizacion == null) {
                fechafinalizacion = "";
            }
            ejecutor = datosproyecto.getEjecutor();
            if (ejecutor == null) {
                ejecutor = "";
            }
            idmetadatopro = String.valueOf(datosproyecto.getMetadato());
            
            rutacssadministra = datosproyecto.getCssaplicado();
            
        }
    } catch (Exception ex) {
        ocurriounError = true;
        mensajeerror = ex.toString();
        
    }
    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Editar Datos del Proyecto</title>        
        <link rel="stylesheet" href="../../pluginsJS/tabz/myTabz.css" type="text/css" />      
        <script type='text/javascript' src='../../js/lib/prototype.js'></script>
        <script type="text/javascript" src="../../pluginsJS/tabz/myTabz.js" ></script>
        <script type="text/javascript" src="../../pluginsJS/calendar/calendar.js"></script>
        <script type="text/javascript" src="../../pluginsJS/calendar/calendar-es.js"></script>
        <script type="text/javascript" src="../../pluginsJS/calendar/calendar-setup.js"></script>
        <link rel="stylesheet" href="../../pluginsJS/calendar/calendar-blue.css" />       
        <link rel="stylesheet" href="../../css/UI.css" />  
        <link rel="stylesheet" href="../../css/siamcss.css" /> 
        <script type="text/javascript">           
               
            var ActivateTabs=function(){
                //document.getElementById('activarsubircsschox').checked=false;
                document.getElementById('activarsubirlogochox').checked=false;
                 
                if(typeof(MT)=='undefined'){
                    var MyTabs= new mt('tabs','div.my_tab');
					
                    MyTabs.removeTabTitles('h5.tab_title');
                    MyTabs.addTab('t1','Editar Proyecto-');
                    MyTabs.addTab('t2','Configuración Componente-');                   
                    MyTabs.makeActive('t1');
                }
					
            }	
            /**
             * Comment
             */
            function actualizarProyecto() {
                if (validar(document.getElementById('logoproyectofile').value)) {
                   document.forms.editarproyectoform.submit();
                }
                  
            }
            function validar(archivo){
                var b = archivo.split('.');               
                if(b[b.length-1] == 'jpeg' || b[b.length-1]=='jpg' || b[b.length-1]=='png')
                    return true;
                else{
                    alert('Error:El tipo de archivo para el logo del proyecto debe ser: .jpeg .jpg .png');
                    return false;
                }
            }
            function actualizarLogoOCSS(sender)
            {   
                /*if (sender.name=='activarsubircsschox'){
                    var element= document.getElementById('cssproyectofile');
                    if (element.disabled==true){
                        element.disabled = false;
                        document.forms.editarproyectoform.action ='<%= request.getContextPath()%>/EditarProyectoConArchivoLogoOCssAction';
                        document.forms.editarproyectoform.enctype='multipart/form-data';
                    }else {
                        element.disabled = true;   
                        document.forms.editarproyectoform.action = '<%= request.getContextPath()%>/';
                        document.forms.editarproyectoform.enctype='';
                    }
                }*/
                if (sender.name=='activarsubirlogochox'){
                    var element= document.getElementById('logoproyectofile');
                    if (element.disabled==true){
                        element.disabled = false;
                        document.forms.editarproyectoform.action ='<%= request.getContextPath()%>/EditarProyectoConArchivoLogoOCssAction';
                        document.forms.editarproyectoform.enctype='multipart/form-data';
                    }else{
                        element.disabled = true;   
                        document.forms.editarproyectoform.action = '<%= request.getContextPath()%>/';
                        document.forms.editarproyectoform.enctype='';
                    }
                } 
               
            }
           
		
        </script>        
    </head> 
    <body onload=" ActivateTabs('tabs');  ">  
        <div id="tabs">
            <div id="t1" class="my_tab">
                <h5 class="tab_title">Editar Proyecto-</h5>
                <div id="formularioEdicionproyectos">
                    <form action="" name="editarproyectoform" id="editarproyectoform" method="post" >
                        <div><p>Código:<%=codigoProyecto%></p></div>
                        <input type="hidden" value="<%=codigoProyecto%>"  id="codigo" name="codigo" /> 
                        <div><span>Título:</span></div>
                        <div><input type="text" id="titulotxt" name="titulotxt" value="<%=titulo%>"/></div>
                        <div><span>Nombre Alterno:</span></div>
                        <div><input type="text" id="nombrealternotxt" name="nombrealternotxt" value="<%=nombreAlterno%>"/></div>
                        <div><span>Fecha Inicio</span></div>
                        <div><input type="text" id="fechainiciotxt" name="fechainiciotxt" readonly="true" value="<%=fechaInicio%>" /><img src="../images/calendar.gif" width="20" height="20" id="filanzador" style="cursor: pointer; border: 0px solid red;" title="Date selector" onMouseOver="this.style.background='red';" onMouseOut="this.style.background=''" /></div>
                        <div><span>Fecha Finalización</span></div>
                        <div><input type="text" id="fechafinalizaciontxt" name="fechafinalizaciontxt" readonly="true" value="<%=fechafinalizacion%>"/><img src="../images/calendar.gif" width="20" height="20" id="fflanzador" style="cursor: pointer; border: 0px solid red;" title="Date selector" onMouseOver="this.style.background='red';" onMouseOut="this.style.background=''" /></div>
                        <div><span>Ejecutor:</span></div>
                        <div><input type="text" id="ejecutortxt" name="ejecutortxt" value="<%=ejecutor%>"/></div>
                        <div><span>Sistema SIAM que administra los datos del registro</span></div>
                        <div><input type="text" id="sistemasiamcmb" name="sistemasiamcmb" /></div>
                        <div><span>Identificador del Proyecto Cassia o Geonetwork donde se describe el metadato:</span></div>
                        <div><input type="text" id="idmetadatopro" name="idmetadatopro"  value="<%=idmetadatopro%>"/></div>
                        <span style="clear:left">Css actual proyecto.<span style="color:blue"><%=rutacssadministra%></span> Seleccione nuevo css para el proyecto:</span>
                        <div style="clear: left;"><input type="text" id="cssproyectofile" name="cssproyectofile" value="<%=rutacssadministra%>" /></div>
                        <div style="height: 150px;">
                            <div style="">
                                <%                                    
                                    if (bytelogo != null) {
                                        out.println("<img  src=" + request.getContextPath() + "/services?action=getLogoProyecto&codigoproyecto=" + codigoProyecto + " alt='logo' style='padding:5px;' />");
                                    } else {
                                        out.println("<img  src='../images/sin_foto.png' alt='proyecto sin imagen' style='padding:5px;' />");
                                    }
                                %>
                            </div>
                            <div style="position: relative;float:left;width:40%;border:0px solid #006699">
                                <span style="clear: left">Logo Proyecto:</span>
                                <div style="clear: left;"><input type="file"  id="logoproyectofile" name="logoproyectofile" disabled="true"/></div> 
                            </div>
                            <div style="border:0px solid #aaf;text-align: left;position: relative;width: 50%;float:right;padding-left: 10px; padding-top: 10px;">
                                Activar subir logo. <input type="checkbox" id="activarsubirlogochox" name="activarsubirlogochox" onclick="actualizarLogoOCSS(this)" style="clear:right"/>
                            </div>
                        </div>
                        <div style="height: 50px;border:0px solid #F9EDBE;display: none">
                            <div style="position: relative;float:left;width:50%">

                            </div>
                            <div style="position: relative;display:none;float:left;text-align: left;width:30%;padding-left: 0px; padding-top: 10px;">
                                Activar subir css. <input type="checkbox" id="activarsubircsschox" name="activarsubircsschox" onclick="actualizarLogoOCSS(this)" style="clear:right" />
                            </div>

                        </div>

                        <div style="padding-top:10px;padding-bottom:10px;">
                            <input type="button" id="actualizarbtn" name="actualizarbtn" value="Actualizar" onclick="actualizarProyecto()"/>
                        </div>
                        <%                            
                            if (ocurriounError) {
                                out.println("<div id='alerta' class='mensajerror'><img src='../images/notification_error.png' alt='notificacion de error'/>Error:" + mensajeerror + "</div>");
                            } else {
                                out.println("<div id='alerta'> </div>");
                            }
                        %>

                        <div></div>   
                    </form> 
                </div>	
            </div>
            <div id="t2" class="my_tab">
                <h5 class="tab_title">Configuración Componente-</h5>
                <p>
                    tab2: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Fusce eget pede. Ut faucibus urna. Donec sagittis. Suspendisse bibendum tortor sed metus. Vivamus aliquet. Sed mi dolor, sollicitudin sed, commodo sed, vestibulum cursus, ligula. Cras laoreet bibendum ante. Sed tristique quam id tellus. Donec sodales. Integer sit amet diam vitae tellus iaculis tincidunt. Curabitur mauris nulla, blandit id, eleifend at, facilisis adipiscing, urna. Vestibulum vel metus. Mauris lacinia, lectus nec tincidunt dictum, tortor erat pretium nulla, a cursus arcu neque vel nunc. Sed consectetuer laoreet est. Duis nec augue. Maecenas eu ipsum nec lectus feugiat iaculis. Suspendisse a nisl vitae neque consectetuer bibendum. Quisque non nulla.
                    Vivamus rhoncus, nisl quis elementum malesuada, nisi ligula aliquet ligula, sed fringilla ipsum lectus in leo. Quisque semper faucibus nunc. Nunc elit lacus, nonummy eu, egestas ultricies, ullamcorper sed, ipsum. Nulla elit lectus, venenatis id, fermentum ut, fringilla vel, erat. Morbi lorem urna, consectetuer ut, blandit id, dapibus sit amet, nunc. Nam nec nisi. Maecenas congue. Sed pulvinar imperdiet neque. Aenean aliquam nunc. Nunc feugiat nonummy felis. Integer diam justo, egestas commodo, hendrerit eget, blandit id, massa. Ut nec nunc. Cras dui purus, auctor sit amet, auctor sed, scelerisque eu, leo. Nullam erat lectus, mollis ut, molestie eget, dapibus ut, nisl. Aenean aliquet enim. Nunc arcu lacus, viverra rutrum, elementum a, posuere sed, risus. In hac habitasse platea dictumst. Sed facilisis vehicula orci. 
                </p>	
            </div>
        </div>
        <script type="text/javascript">
            Calendar.setup({

                inputField : "fechainiciotxt",// id del campo de texto
                ifFormat : "%d/%m/%Y", // formato de la fecha que se escriba en el campo de texto
                button : "filanzador" // el id del bot?n que lanzar? el calendario           

            });
  
            Calendar.setup({

                inputField : "fechafinalizaciontxt",// id del campo de texto
                ifFormat : "%d/%m/%Y", // formato de la fecha que se escriba en el campo de texto
                button : "fflanzador" // el id del bot?n que lanzar? el calendario*/

            });

        </script> 
    </body> 
</html>
