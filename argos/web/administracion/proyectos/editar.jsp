<%-- 
    Document   : editarProyecto
    Created on : 23/11/2011, 04:51:05 PM
    Author     : usrsig15
--%>

<%@page import="java.util.Iterator"%>
<%@page import="co.org.invemar.siam.sibm.vo.Proyecto"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="co.org.invemar.util.ConnectionFactory"%>
<%@page import="co.org.invemar.siam.sibm.model.ProyectosDM"%>
<%
    String codigoProyecto = request.getParameter("codigoproyecto");
    String resultadoTransaccion = null;
    resultadoTransaccion = request.getParameter("mensaje");

    String titulo = "";
    String nombreAlterno = "";
    String fechaInicio = "";
    String fechafinalizacion = "";
    String ejecutor = "";
    String sistemaSIAMqueloadministra = "";
    String rutalogoproyecto = "";
    String rutacssadministra = "";
    String idmetadatopro = "0";
    boolean ocurriounError = false;
    String mensajeerror = "";
    byte[] bytelogo = null;
    try {
        ConnectionFactory cFactory = new ConnectionFactory();
        Connection connection = cFactory.createConnection();
        ProyectosDM proyecto = new ProyectosDM();
        List<Proyecto> lista = proyecto.findProjectsPorCodigo(connection, Long.parseLong(codigoProyecto));
        bytelogo = proyecto.getByteImagenLogoProyecto(connection, codigoProyecto);

        Iterator<Proyecto> it = lista.iterator();
        while (it.hasNext()) {
            Proyecto datosproyecto = it.next();
            titulo = datosproyecto.getTitulo();
            if (titulo == null) {
                titulo = "";
            }
            nombreAlterno = datosproyecto.getNombreAlterno();
            if (nombreAlterno == null) {
                nombreAlterno = "";
            }
            fechaInicio = datosproyecto.getFechaInicio();
            if (fechaInicio == null) {
                fechaInicio = "";
            }
            fechafinalizacion = datosproyecto.getFechaFinalizo();
            if (fechafinalizacion == null) {
                fechafinalizacion = "";
            }
            ejecutor = datosproyecto.getEjecutor();
            if (ejecutor == null) {
                ejecutor = "";
            }
            idmetadatopro = String.valueOf(datosproyecto.getMetadato());

            rutacssadministra = datosproyecto.getCssaplicado();

        }
    } catch (Exception ex) {
        ocurriounError = true;
        mensajeerror = ex.toString();

    }

%>
<div id="tabs">
    <div id="t1" class="my_tab">        
        <div id="formularioEdicionproyectos">
            <form action="" name="editarproyectoform" id="editarproyectoform" method="post" >
                <div style="height:82px;">
                    <div>
                        <div><p>C�digo:<%=codigoProyecto%></p></div>
                        <div>
                            <div style="float:left;">
                                <span>Fecha Inicio</span>
                                <div><input type="text" id="fechainiciotxt" name="fechainiciotxt" readonly="true" value="<%=fechaInicio%>" /><img src="../../images/calendar.gif" width="20" height="20" id="cal1" style="cursor: pointer; border: 0px solid red;" title="Date selector" onclick="displayCalendar(document.forms[0].fechainiciotxt,'dd/mm/yyyy',this)" /></div>
                            </div>
                            <div style="float:left;margin-left: 15px;">
                                <span>Fecha Finalizaci�n</span>
                                <div><input type="text" id="fechafinalizaciontxt" name="fechafinalizaciontxt" readonly="true" value="<%=fechafinalizacion%>"/><img src="../../images/calendar.gif" width="20" height="20" id="cal2" style="cursor: pointer; border: 0px solid red;" title="Date selector" onclick="displayCalendar(document.forms[0].fechafinalizaciontxt,'dd/mm/yyyy',this)"   /></div>
                            </div>
                            <div style="float:left;margin-left: 20px" >
                                <span>Ejecutor:</span>
                                <div><input type="text" id="ejecutortxt" name="ejecutortxt" value="<%=ejecutor%>"/></div>
                            </div>
                        </div>
                    </div>
                    <div style="float: right;padding-right:100px;">
                        <%
                            if (bytelogo != null) {
                                out.println("<img  src=" + request.getContextPath() + "/services?action=getLogoProyecto&codigoproyecto=" + codigoProyecto + " alt='logo' style='padding:5px;' />");
                            } else {
                                out.println("<img  src='../../images/sin_foto.png' alt='proyecto sin imagen' style='padding:5px;' />");
                            }
                        %>
                    </div>
                </div>

                <input type="hidden" value="<%=codigoProyecto%>"  id="codigo" name="codigo" /> 
                <div><span>T�tulo:</span></div>
                <div><input type="text" id="titulotxt" name="titulotxt" value="<%=titulo%>"/></div>
                <div><span>Nombre Alterno:</span></div>
                <div><input type="text" id="nombrealternotxt" name="nombrealternotxt" value="<%=nombreAlterno%>"/></div>
                <div style="height: 40px;">
                    <div style="float:left;">
                        <span>Sistema SIAM que administra los datos del registro</span>
                        <div><input type="text" id="sistemasiamcmb" name="sistemasiamcmb" /></div>
                    </div>
                    <div style="float:left;margin-left:20px;">
                        <span>Identificador del Proyecto Cassia o Geonetwork donde se describe el metadato:</span>
                        <div><input type="text" id="idmetadatopro" name="idmetadatopro"  value="<%=idmetadatopro%>"/></div>
                    </div>
                </div>    
                <div style="height: 50px;">
                    <div style="float:left">
                        <span style="color:blue">CSS para el proyecto:</span>
                        <div ><input type="text" id="cssproyectofile" name="cssproyectofile" value="<%=rutacssadministra%>" /></div>
                    </div>
                    <div style="float:left;margin-left: 15px; ">
                        <div style="position: relative;float:left;width:40%;border:0px solid #006699;margin-right: 50px;">
                            <span style="clear: left">Logo Proyecto:</span>
                            <div style="clear: left;"><input type="file"  id="logoproyectofile" name="logoproyectofile" disabled="true"/></div> 
                        </div>
                        <div style="border:0px solid #aaf;text-align: left;position: relative;float:right;">
                           <span>Activar subir logo.</span>
                            <input type="checkbox" id="activarsubirlogochox" name="activarsubirlogochox" onclick="actualizarLogoOCSS(this)" style="clear:right"/>
                        </div>
                    </div>
                </div>
                <div style="padding-top:10px;padding-bottom:10px;">
                    <input type="button" id="actualizarbtn" name="actualizarbtn" value="Actualizar" onclick="actualizarProyecto()"/>
                </div>
                <!-- <div style="height: 50px;border:0px solid #F9EDBE;display: none">
                     <div style="position: relative;float:left;width:50%">

                     </div>
                     <div style="position: relative;display:none;float:left;text-align: left;width:30%;padding-left: 0px; padding-top: 10px;">
                         Activar subir css. <input type="checkbox" id="activarsubircsschox" name="activarsubircsschox" onclick="actualizarLogoOCSS(this)" style="clear:right" />
                     </div>
                 </div>-->


                <%
                    if (ocurriounError) {
                        out.println("<div id='alerta' class='mensajerror'><div style='float:left;margin:2px;'><img src='../../images/notification_error.png' alt='notificacion de error'/></div><span style='margin-left:5px; float:left'>Error:" + mensajeerror + "</span></div>");
                    } else {
                        out.println("<div id='alerta'> </div>");
                    }
                %>                  
            </form> 
        </div>           
    </div>
    <div id="t2" class="my_tab">
        <h5 class="tab_title">Configuraci�n Componente-</h5>
        <p>
            tab2: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Fusce eget pede. Ut faucibus urna. Donec sagittis. Suspendisse bibendum tortor sed metus. Vivamus aliquet. Sed mi dolor, sollicitudin sed, commodo sed, vestibulum cursus, ligula. Cras laoreet bibendum ante. Sed tristique quam id tellus. Donec sodales. Integer sit amet diam vitae tellus iaculis tincidunt. Curabitur mauris nulla, blandit id, eleifend at, facilisis adipiscing, urna. Vestibulum vel metus. Mauris lacinia, lectus nec tincidunt dictum, tortor erat pretium nulla, a cursus arcu neque vel nunc. Sed consectetuer laoreet est. Duis nec augue. Maecenas eu ipsum nec lectus feugiat iaculis. Suspendisse a nisl vitae neque consectetuer bibendum. Quisque non nulla.
            Vivamus rhoncus, nisl quis elementum malesuada, nisi ligula aliquet ligula, sed fringilla ipsum lectus in leo. Quisque semper faucibus nunc. Nunc elit lacus, nonummy eu, egestas ultricies, ullamcorper sed, ipsum. Nulla elit lectus, venenatis id, fermentum ut, fringilla vel, erat. Morbi lorem urna, consectetuer ut, blandit id, dapibus sit amet, nunc. Nam nec nisi. Maecenas congue. Sed pulvinar imperdiet neque. Aenean aliquam nunc. Nunc feugiat nonummy felis. Integer diam justo, egestas commodo, hendrerit eget, blandit id, massa. Ut nec nunc. Cras dui purus, auctor sit amet, auctor sed, scelerisque eu, leo. Nullam erat lectus, mollis ut, molestie eget, dapibus ut, nisl. Aenean aliquet enim. Nunc arcu lacus, viverra rutrum, elementum a, posuere sed, risus. In hac habitasse platea dictumst. Sed facilisis vehicula orci. 
        </p>	
    </div>
</div>
 
