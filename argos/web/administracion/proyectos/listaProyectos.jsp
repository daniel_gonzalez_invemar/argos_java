<%-- 
    Document   : listaProyectos
    Created on : 23/11/2011, 11:27:55 AM
    Author     : Daniel Gonzalez Polo
--%>

<%@page import="java.sql.Connection"%>
<%@page import="co.org.invemar.util.ConnectionFactory"%>
<%@page import="co.org.invemar.siam.sibm.model.ProyectosDM"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Argos-Lista de Proyectos</title>
        <script type="text/javascript" language="JavaScript" src="../js/lib/plugintableorder/prototype.js"></script>
        <script type="text/javascript" language="JavaScript" src="../js/lib/plugintableorder/tableorderer.js"></script>
        <link rel="stylesheet" href="../css/plugintablerorder/tableorderer.css" type="text/css" media="screen" />     
    </head> 
    <body>
        <%
            ProyectosDM proyectos = new ProyectosDM();
            ConnectionFactory cFactory = new ConnectionFactory();
            Connection connection = cFactory.createConnection();
            String jsonarray = proyectos.findProjectsInJSON(connection);
            jsonarray = jsonarray.replace("[", "");
            jsonarray = jsonarray.replace("]", "");
            
        %>
        <div id="tablaproyectosargos"></div>
        <script type="text/javascript" language="JavaScript">
           data3 = new Array(<%=jsonarray%>);
           new TableOrderer('tablaproyectosargos',{data : data3,search:'top',pagination:'bottom',paginate : true,pageCount:10});
        </script>
    </body>
</html>
