<%-- 
    Document   : administrar
    Created on : 6/12/2011, 03:46:18 PM
    Author     : usrsig15
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Administrar ARGOS</title>
        <script type="text/javascript" language="JavaScript" src="../../js/lib/plugintableorder/prototype.js"></script>
        <script type="text/javascript" language="JavaScript" src="../../js/lib/plugintableorder/tableorderer.js"></script>
        <script type="text/javascript" language="JavaScript" src="../../js/administrar.js"></script>        
        <link rel="stylesheet" href="../../css/plugintablerorder/tableorderer.css" type="text/css" media="screen" />     
        
        <link rel="stylesheet" href="../../pluginsJS/tabz/myTabz.css" type="text/css" />      
      
        <script type="text/javascript" src="../../pluginsJS/tabz/myTabz.js" ></script>
        <script type="text/javascript" src="../../pluginsJS/calendar/calendar.js"></script>
        <script type="text/javascript" src="../../pluginsJS/calendar/calendar-es.js"></script>
        <script type="text/javascript" src="../../pluginsJS/calendar/calendar-setup.js"></script>
        <link rel="stylesheet" href="../../pluginsJS/calendar/calendar-blue.css" />       
        <link rel="stylesheet" href="../../css/UI.css" />  
        <link rel="stylesheet" href="../../css/siamcss.css" /> 
    </head>
    <body>
        <div id="contenedor" style="width: 100%;height: 768px">
            <div id="header" style="position:relative;width: 100%;border:2px solid #46a">Sistema de Monitoreo Marino</div>
            <div id="contenido" style="position:relative;width: 100%;height: 100%">
                <div id="menuadministracion" style="float:left;position:relative;width: 19%;border:2px solid #666">
                    <ul>
                        <li><img src="../../images/projects.png"   /><a href="javascript:administrarProyecto('<%=request.getContextPath()%>/administracion/proyectos/administrar.jsp')" >Proyectos</a></li>
                        <li><img src="../../images/estaciones.png"   /><a href="javascript:administrarProyecto('<%=request.getContextPath()%>/administracion/estaciones/administrar.jsp')" >Estaciones</a></li>
                        <li><img src="../../images/componentesv2.png"   /><a href="javascript:administrarProyecto('<%=request.getContextPath()%>/administracion/componentesProyectos/administrar.jsp')" >Componentes </a></li>
                        <li><img src="../../images/unidadmedida.png"   /><a href="#" onclick="administrarUnidadesMedida()">Unidades de Medida</a></li>
                        <li><img src="../../images/variable3.png"   /><a href="#" onclick="administrarVariables()"> Variables</a></li>
                    </ul>
                </div>
                <div id="zonaadministracion" style="float:right;position:relative;width:80%;height:100% ;border:2px solid #666">
                   
                    
                </div>
            </div>
            <div id="footer" style="position:relative;clear:both">Argos Sistema de Proyectos Marinos</div>
        </div>
        
    </body>
</html>
