<%-- 
    Document   : listaProyectos
    Created on : 23/11/2011, 11:27:55 AM
    Author     : Daniel Gonzalez Polo
--%>

<%@page import="java.sql.Connection"%>
<%@page import="co.org.invemar.util.ConnectionFactory"%>
<%@page import="java.util.Iterator"%>
<%@page import="co.org.invemar.siam.sibm.vo.Proyecto"%>
<%@page import="java.util.List"%>
<%@page import="co.org.invemar.siam.sibm.model.ProyectosDM"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Argos-Lista de Proyectos</title>
        <script type="text/javascript" language="JavaScript" src="../../js/lib/plugintableorder/prototype.js"></script>
        <script type="text/javascript" language="JavaScript" src="../../js/lib/plugintableorder/tableorderer.js"></script>
        <link rel="stylesheet" href="../../css/plugintablerorder/tableorderer.css" type="text/css" media="screen" />     
        
        <link rel="stylesheet" href="../../pluginsJS/tabz/myTabz.css" type="text/css" />      
        <script type='text/javascript' src='../../js/lib/prototype.js'></script>
        <script type="text/javascript" src="../../pluginsJS/tabz/myTabz.js" ></script>
        
        <link type="text/css" rel="stylesheet" href="../../pluginsJS/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
	<script type="text/javascript" src="../../pluginsJS/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
        
        <link rel="stylesheet" href="../../css/UI.css" />  
        <link rel="stylesheet" href="../../css/siamcss.css" /> 
        <script type="text/javascript">           
            /**
             * act
             */
            function activarTab() {
                 var MyTabs= new mt('tabs','div.my_tab');
                 MyTabs.addTab('t1','Editar Proyecto-');
                 MyTabs.addTab('t2','Configuración Componente-');  
                 MyTabs.makeActive('t1');
            }  
               
            var ActivateTabs=function(){
                //document.getElementById('activarsubircsschox').checked=false;
                document.getElementById('activarsubirlogochox').checked=false;
                 
                if(typeof(MT)=='undefined'){
                    var MyTabs= new mt('tabs','div.my_tab');
					
                    MyTabs.removeTabTitles('h5.tab_title');
                    MyTabs.addTab('t1','Editar Proyecto-');
                    MyTabs.addTab('t2','Configuración Componente-');                   
                    MyTabs.makeActive('t1');
                }
					
            }	
            /**
             * Comment
             */
            function actualizarProyecto() {
                if (validar(document.getElementById('logoproyectofile').value)) {
                    document.forms.editarproyectoform.submit();
                }
                  
            }
            function validar(archivo){
                var b = archivo.split('.');               
                if(b[b.length-1] == 'jpeg' || b[b.length-1]=='jpg' || b[b.length-1]=='png')
                    return true;
                else{
                    alert('Error:El tipo de archivo para el logo del proyecto debe ser: .jpeg .jpg .png');
                    return false;
                }
            }
            
            function editarProyecto(micodigoproyecto) {
                var url = 'editar.jsp';
               new Ajax.Request(url, {
                    method:'post',
                    parameters: {codigoproyecto:micodigoproyecto},
                    asynchronous: true,      
                    onSuccess: function(transport){
                        document.getElementById("crud").innerHTML = transport.responseText;
                        activarTab();
                    },
                    onFailure: function(err){ 
                        document.getElementById("crud").innerHTML = '<p class="mensajerror">error cargardo el formulario de edicion</p>';
                    }
                });
                
            }
            function actualizarLogoOCSS(sender)
            {   
                /*if (sender.name=='activarsubircsschox'){
                    var element= document.getElementById('cssproyectofile');
                    if (element.disabled==true){
                        element.disabled = false;
                        document.forms.editarproyectoform.action ='<%= request.getContextPath()%>/EditarProyectoConArchivoLogoOCssAction';
                        document.forms.editarproyectoform.enctype='multipart/form-data';
                    }else {
                        element.disabled = true;   
                        document.forms.editarproyectoform.action = '<%= request.getContextPath()%>/';
                        document.forms.editarproyectoform.enctype='';
                    }
                }*/
                if (sender.name=='activarsubirlogochox'){
                    var element= document.getElementById('logoproyectofile');
                    if (element.disabled==true){
                        element.disabled = false;
                        document.forms.editarproyectoform.action ='<%= request.getContextPath()%>/EditarProyectoConArchivoLogoOCssAction';
                        document.forms.editarproyectoform.enctype='multipart/form-data';
                    }else{
                        element.disabled = true;   
                        document.forms.editarproyectoform.action = '<%= request.getContextPath()%>/';
                        document.forms.editarproyectoform.enctype='';
                    }
                } 
               
            }
           
		
        </script> 
    </head> 
    <body>
        <div id="listarproyectos" ></div>
        <%
            ProyectosDM proyectos = new ProyectosDM();
            ConnectionFactory cFactory = new ConnectionFactory();
            Connection connection = cFactory.createConnection();
            String jsonarray = proyectos.findProjectsInJSON(connection);
            jsonarray = jsonarray.replace("[", "");
            jsonarray = jsonarray.replace("]", "");

        %>
        <div id="tablaproyectosargos" ></div>
        <script type="text/javascript" language="JavaScript">
            data3 = new Array(<%=jsonarray%>);
            new TableOrderer('tablaproyectosargos',{data : data3,search:'top',pagination:'bottom',paginate : true,pageCount:3});
            editarProyecto(202);
        </script>
        <div id="crud" style="margin-top: 10px;">


        </div>
            
    </body>
</html>
