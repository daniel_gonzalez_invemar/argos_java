<%-- 
    Document   : editarProyecto
    Created on : 23/11/2011, 04:51:05 PM
    Author     : usrsig15
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
        <title>Editar Datos del Proyecto</title>        
        <link rel="stylesheet" href="../css/tabplugin.css" type="text/css" media="screen" />     
    </head> 
    <body> 
        <ul id="globalnav">
            <li><a href="#">Home</a></li>
            <li><a href="#" class="here">About</a>
                <ul>
                    <li><a href="#">Vision</a></li>
                    <li><a href="#">Team</a></li>
                    <li><a href="#">Culture</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#" class="here">History</a></li>
                    <li><a href="#">Sponsorship</a></li>
                </ul>
            </li>
            <li><a href="#">News</a></li>
            <li><a href="#">Proof</a></li>
            <li><a href="#">Process</a></li>
            <li><a href="#">Expertise</a></li>
            <li><a href="#">Help</a></li>
        </ul>
    </body> 
</html>
