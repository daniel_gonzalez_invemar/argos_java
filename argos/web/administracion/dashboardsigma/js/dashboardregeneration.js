var app = angular.module("dashboardregeneration", []);
app.controller("view", function ($scope, $compile) {

    $scope._timeSeries = "";
    $scope.init = function () {
        
        $("#groupmessagevariable").hide();
        $("#messagevariablebycategory").hide();
        $("#messagevariableIER").hide();
        
        fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=getAllDepartamentColombia", {}, '#coddepartament');

    };
    $scope.handleVariableChange = function () {

        if ($scope.variable == 'ABUNDANCIAABSOLUTAVIVOS' || $scope.variable == 'ABUNDANCIARELATIVAVIVOS' || $scope.variable == 'ALTURAPROMEDIO' || $scope.variable == 'DENSIDADABSOLUTAVIVOS') {
            $("#groupcategoria").show();
            $("#groupmessagevariable").show();
            $("#messagevariablebycategory").show();


        } else {
            $("#groupcategoria").hide();
            $scope.categoria = "-1";
            $("#groupmessagevariable").hide();
            $("#messagevariablebycategory").hide();
        }

        if ($scope.variable == 'IER') {
            $("#groupmessagevariable").show();
            $("#messagevariableIER").show();
        } else {
            $("#messagevariableIER").hide();
        }

    };
   
    $scope.handleOnChangeLevel = function () {
        if ($scope.nivel == "1") {
            /*Parcel*/
            $("#groupparcel").show();
            $("#groupstation").show();

        } else if ($scope.nivel == "2") {
            /*Stations*/
            $("#groupparcel").hide();
            $("#groupstation").show();
        } else if ($scope.nivel == "3") {
            /*Managment Unit*/
            $("#groupparcel").hide();
            $("#groupstation").hide();
        }
    }
    $scope.handleOnChangeDepartament = function () {

        fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=getAllSectorByCodDepartament", {coddepartament: $scope.coddepartament}, '#codsector');

    };
    $scope.handleOnChangeSector = function () {

        fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=getAllManagmentUnitByCodSector", {codSector: $scope.codsector}, '#codmanagmentunit');

    };
    $scope.handleOnChangeManagmentUnit = function () {

        fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=getAllStationsByCodManagmentUnit", {codManagmentUnit: $scope.codmanagmentunit}, '#codstation');

    };
    $scope.handleOnChangeStation = function () {

        fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=getAllParcelByCodSite", {codSite: $scope.codstation}, '#codparcel');

    };
    $scope.DisplayStatistic = function () {

        var caseStudio = "";
        $scope._timeSeries = "";
        var GraphicType = "normal";

        var textVariable = $("#variable option:selected").text();
        var textcategoria = $("#categoria option:selected").text();

        if ($scope.nivel == "1") {
            $scope.codCasoStudio = $scope.codparcel;
            caseStudio = $("#codparcel option:selected").text();



        } else if ($scope.nivel == "2") {
            $scope.codCasoStudio = $scope.codstation;
            caseStudio = $("#codstation option:selected").text();


        } else if ($scope.nivel == "3") {
            $scope.codCasoStudio = $scope.codmanagmentunit;
            caseStudio = $("#codmanagmentunit option:selected").text();


        }
        
        if ($scope.variable=='ap' || $scope.variable=='ALTURAPROMEDIO'){
           GraphicType='';
          
        }



        url = "/argos/argoservice?pkg=actions.sigma.&action=GraphicStatistic";
        $.post(url, {
            variable: $scope.variable,
            nivel: $scope.nivel,
            codCasoStudio: $scope.codCasoStudio,
            categoria: $scope.categoria
        }, function (data) {
            categories = data.categories

           

            $('#container').highcharts({
                chart: {
                    type: 'column',
                    events: {
                        addSeries: function () {
                            var label = this.renderer.label('Nueva estadisticas generada', 100, 120)
                                    .attr({
                                        fill: Highcharts.getOptions().colors[0],
                                        padding: 10,
                                        r: 5,
                                        zIndex: 8
                                    })
                                    .css({
                                        color: '#FFFFFF'
                                    })
                                    .add();

                            setTimeout(function () {
                                label.fadeOut();
                            }, 1000);
                        }
                    }
                },
                xAxis: {
                    categories: data.categories
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: textVariable
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                plotOptions: {
                    column: {
                        stacking: GraphicType,
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            style: {
                                textShadow: '0 0 3px black'
                            }
                        }
                    }
                },
                title: {
                    text: textVariable + ' - �rea ' + caseStudio
                },
                credits: {
                    enabled: false
                },
            });
            var chart = $('#container').highcharts();

            for (var s = 0; s < data.series.length; s++) {
                chart.addSeries(data.series[s]);
            }


            if ($.fn.dataTable.isDataTable('#example1')) {

                table = $('#example1').DataTable();
                table.destroy();

            }

            $("#export").attr("href", "/argos/argoservice?pkg=actions.sigma.&action=ExportExcelStatisticRegeneration&variable=" + $scope.variable + "&nivel=" + $scope.nivel + "&codCasoStudio=" + $scope.codCasoStudio + "&categoria=" + $scope.categoria);

            $("#tablebody").empty();
            var $html = "";
            $scope._timeSeries = "";
            jQuery.each(data.data, function (index, item) {
                $html = $("<tr><td>" + item.especie + "</td><td>" + item.ano + "</td><td>" + item.dato + "</td></tr>").appendTo('#tablebody');
                $scope._timeSeries = $scope._timeSeries + item.ano + ",";
            });



            $compile($html)($scope);

            $('#example1').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "language": {
                    "lengthMenu": "Visualizando  _MENU_ registros por p&aacute;gina",
                    "zeroRecords": "Cargando informaci&oacute;n...",
                    "info": "Visualizando  p&aacute;gina  _PAGE_ of _PAGES_",
                    "infoEmpty": "Informaci&oacute;n no encontrada",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente",
                    },
                }

            });

            url = "/argos/argoservice?pkg=actions.sigma.&action=GetResearcher";
            $.post(url, {
                thematic: 4,
                codsector: $scope.codsector,
                timeseries: $scope._timeSeries
            }, function (data) {
                $("#listtab2").empty();
                jQuery.each(data, function (index, item) {
                    $("#listtab2").append("<li>" + item.lastname + " " + item.name + " - " + item.company + "</li>");

                });

            });

        });





    };


});








