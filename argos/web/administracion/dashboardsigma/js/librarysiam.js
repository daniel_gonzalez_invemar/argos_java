function fillComboBox(url, payload, element) {
    $
            .post(
                    url,
                    payload,
                    function (data) {
                        $(element).get(0).options.length = 0;
                        $(element).get(0).options[0] = new Option(
                                "----- Seleccione -------", "-1");
                        jQuery
                                .each(
                                        data,
                                        function (index, item) {
                                            $(element).get(0).options[$(
                                                    element).get(0).options.length] = new Option(
                                                    item.name, item.id);
                                        });
                    }, "json");
}



