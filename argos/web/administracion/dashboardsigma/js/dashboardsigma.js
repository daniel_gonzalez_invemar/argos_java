var app = angular.module("dashboardsigma", []);
app.controller("view", function ($scope, $compile) {


    $scope.init = function () {

        fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=ListCaseStudioByEntity", {entity: myEntity, typecasestudio: 94}, '#codparcel');
        fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=ListCaseStudioByEntity", {entity: myEntity, typecasestudio: 94, AttibuteValue: 'Circular'}, '#circuleparcel');
        fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=ListCaseStudioByEntity", {entity: myEntity, typecasestudio: 94, AttibuteValue: 'Cuadrada'}, '#rectangleparcel');
        fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=ListAllSpeciesByProject", {idproject: 2239}, '#codspecies');


        $scope.hideXAndYField();
        $scope.hideDistanceAndAzimutField();
        $("#divmessage").hide();
        $("#oidelement").hide();
        $("#labeloidelement").hide();
        url = "/argos/argoservice?pkg=actions.sigma.&action=ListCoordenateIndividuos&entity=" + myEntity;
        $scope.drawTable(url);
    };
    $scope.hideXAndYField = function () {
        $("#groupx").hide();
        $("#groupy").hide();
    };
    $scope.visibleXAndYField = function () {
        $("#groupx").show();
        $("#groupy").show();
    };
    $scope.hideDistanceAndAzimutField = function () {
        $("#groupazimut").hide();
        $("#groupdistance").hide();
    };
    $scope.visibleDistanceAndAzimutField = function () {
        $("#groupazimut").show();
        $("#groupdistance").show();
    };
    $scope.graphRentangleParcel = function () {

        var idRectangleParcel = $("#rectangleparcel").val();
        var parcelName =$("#rectangleparcel :selected").text();
        url = "/argos/argoservice?pkg=actions.sigma.&action=ListCoordenateIndividuos";
        $.post(url, {
            parcelId: idRectangleParcel,
            entity: myEntity
        }, function (data) {

            coordenatesRectangleParcelAV = [];
            coordenatesRectangleParcelAB = [];
            coordenatesRectangleParcelCE = [];
            coordenatesRectangleParcelLR = [];
            coordenatesRectangleParcelMO = [];
            coordenatesRectangleParcelPR = [];
            coordenatesRectangleParcelRH = [];
            coordenatesRectangleParcelRM = [];
            coordenatesRectangleParcelRR = [];
            coordenatesRectangleParcelSP = [];

            jQuery.each(data, function (index, item) {

                /*Rentangle parcel data: */
                
                
                if ("2040.55.687" == item.CODIGO_ESPECIE) {
                    /*Avicenia bicocor 2040.55.687*/
                    coordenatesRectangleParcelAB.push([
                        parseInt(item.X),
                        parseInt(item.Y), 1
                    ]);
                }else  if ("2040.55.685" == item.CODIGO_ESPECIE) {
                    /*Avicenia */
                    coordenatesRectangleParcelAV.push([
                        parseInt(item.X),
                        parseInt(item.Y), 1
                    ]);
                } else if ("2040.55.293" == item.CODIGO_ESPECIE) {
                    /*Conocarpus erectus */
                    coordenatesRectangleParcelCE.push([
                        parseInt(item.X),
                        parseInt(item.Y), 1
                    ]);
                } else if ("2040.55.290" == item.CODIGO_ESPECIE) {
                    /*Laguncularia racemosa */
                    coordenatesRectangleParcelLR.push([
                        parseInt(item.X),
                        parseInt(item.Y), 1
                    ]);
                } else if ("2040.55.507" == item.CODIGO_ESPECIE) {
                    /*Mora oleifera  */
                    coordenatesRectangleParcelMO.push([
                        parseInt(item.X),
                        parseInt(item.Y), 1
                    ]);
                } else if ("2040.55.532" == item.CODIGO_ESPECIE) {
                    /*Pelliciera rhizophorae */
                    coordenatesRectangleParcelPR.push([
                        parseInt(item.X),
                        parseInt(item.Y), 1
                    ]);
                } else if ("2040.55.1509" == item.CODIGO_ESPECIE) {
                    /*Rhizophora harrisonii */
                    coordenatesRectangleParcelRH.push([
                        parseInt(item.X),
                        parseInt(item.Y), 1
                    ]);
                } else if ("2040.55.325" == item.CODIGO_ESPECIE) {
                    /*Rhizophora mangle */
                    coordenatesRectangleParcelRM.push([
                        parseInt(item.X),
                        parseInt(item.Y), 1
                    ]);
                } else if ("2040.55.326" == item.CODIGO_ESPECIE) {
                    /*Rhizophora racemosa*/
                    coordenatesRectangleParcelRR.push([
                        parseInt(item.X),
                        parseInt(item.Y), 1
                    ]);
                } else if ("2040.5.2040" == item.CODIGO_ESPECIE) {
                    /*Sp. manglar*/
                    coordenatesRectangleParcelSP.push([
                        parseInt(item.X),
                        parseInt(item.Y), 1
                    ]);
                }


            });

            $('#bubbles').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'bubble',
                    zoomType: 'xy'
                },
                title: {
                    text: 'Distribucion de individuos en la parcela rectangular '+parcelName
                },
                yAxis: {
                    title: {
                        text: 'Distancia Y(m)'
                    },
                    min: 0,
                },
                xAxis: {
                    title: {
                        text: 'Distancia X(m)'
                    },
                    gridLineWidth: 1
                },
                series: [
                    {name:'Avicennia bicolor',
                     data:coordenatesRectangleParcelAB   
                    },
                    {
                        name: 'Avicennia germinans',
                        data: coordenatesRectangleParcelAV
                    }, {
                        name: 'Conocarpus erectus',
                        data: coordenatesRectangleParcelCE
                    }, {
                        name: 'Laguncularia racemosa',
                        data: coordenatesRectangleParcelLR
                    },
                    {
                        name: 'Mora oleifera',
                        data: coordenatesRectangleParcelMO
                    },
                    {
                        name: 'Pelliciera rhizophorae',
                        data: coordenatesRectangleParcelPR
                    },
                    {
                        name: 'Rhizophora harrisonii',
                        data: coordenatesRectangleParcelRH
                    },
                    {
                        name: 'Rhizophora mangle',
                        data: coordenatesRectangleParcelRM
                    },
                    {
                        name: 'Rhizophora racemosa',
                        data: coordenatesRectangleParcelRR
                    },
                    {
                        name: 'No especificada',
                        data: coordenatesRectangleParcelSP
                    }
                ]
            });

        });

    };
    $scope.graphCirculeParcel = function () {

        var idCirculeParcel = $("#circuleparcel").val();
        var parcelName =$("#circuleparcel :selected").text();
        url = "/argos/argoservice?pkg=actions.sigma.&action=ListCoordenateIndividuos";
        $.post(url, {
            parcelId: idCirculeParcel,
            entity: myEntity
        }, function (data) {
            coordenatesCirculeParcelAV = [];
            coordenatesCirculeParcelCE = [];
            coordenatesCirculeParcelLR = [];
            coordenatesCirculeParcelMO = [];
            coordenatesCirculeParcelPR = [];
            coordenatesCirculeParcelRH = [];
            coordenatesCirculeParcelRM = [];
            coordenatesCirculeParcelRR = [];
            coordenatesCirculeParcelSP = [];

            jQuery.each(data, function (index, item) {


                /*Avicenia*/


                if ("2040.55.685" == item.CODIGO_ESPECIE) {
                    coordenatesCirculeParcelAV.push([
                        parseInt(item.AZIMUT),
                        parseInt(item.DISTANCIA)
                    ]);
                } if ("2040.55.687" == item.CODIGO_ESPECIE) {
                    /*Avicenia bicolor*/
                    coordenatesCirculeParcelAV.push([
                        parseInt(item.AZIMUT),
                        parseInt(item.DISTANCIA)
                    ]);
                }else if ("2040.55.293" == item.CODIGO_ESPECIE) {
                    /*Conocarpus erectus */
                    coordenatesCirculeParcelCE.push([
                        parseInt(item.AZIMUT),
                        parseInt(item.DISTANCIA)
                    ]);
                } else if ("2040.55.290" == item.CODIGO_ESPECIE) {
                    /*Laguncularia racemosa */
                    coordenatesCirculeParcelLR.push([
                        parseInt(item.AZIMUT),
                        parseInt(item.DISTANCIA)
                    ]);
                } else if ("2040.55.507" == item.CODIGO_ESPECIE) {
                    /*Mora oleifera  */
                    coordenatesCirculeParcelMO.push([
                        parseInt(item.AZIMUT),
                        parseInt(item.DISTANCIA)
                    ]);
                } else if ("2040.55.532" == item.CODIGO_ESPECIE) {
                    /*Pelliciera rhizophorae */
                    coordenatesCirculeParcelPR.push([
                        parseInt(item.AZIMUT),
                        parseInt(item.DISTANCIA)
                    ]);
                } else if ("2040.55.1509" == item.CODIGO_ESPECIE) {
                    /*Rhizophora harrisonii */
                    coordenatesCirculeParcelRH.push([
                        parseInt(item.AZIMUT),
                        parseInt(item.DISTANCIA)
                    ]);
                } else if ("2040.55.325" == item.CODIGO_ESPECIE) {
                    /*Rhizophora mangle */
                    coordenatesCirculeParcelRM.push([
                        parseInt(item.AZIMUT),
                        parseInt(item.DISTANCIA)
                    ]);
                } else if ("2040.55.326" == item.CODIGO_ESPECIE) {
                    /*Rhizophora racemosa*/
                    coordenatesCirculeParcelRR.push([
                        parseInt(item.AZIMUT),
                        parseInt(item.DISTANCIA)
                    ]);
                } else if ("2040.5.2040" == item.CODIGO_ESPECIE) {
                    /*Sp. manglar*/
                    coordenatesCirculeParcelSP.push([
                        parseInt(item.AZIMUT),
                        parseInt(item.DISTANCIA)
                    ]);
                }
            });



            $('#polarchart').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    polar: true

                },
                title: {
                    text: 'Distribucion de individuos en la parcela circular '+parcelName
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360
                },
                xAxis: {
                    tickInterval: 45,
                    min: 0,
                    max: 360,
                    labels: {
                        formatter: function () {
                            return this.value + '°';
                        }
                    }
                },
                yAxis: {
                    min: 0
                },
                plotOptions: {
                    series: {
                        pointStart: 0,
                        pointInterval: 45
                    },
                    column: {
                        pointPadding: 0,
                        groupPadding: 0
                    }
                },
                series: [{
                        type: 'scatter',
                        name: 'Avicennia germinans',
                        data: coordenatesCirculeParcelAV

                    },
                    {
                        type: 'scatter',
                        name: 'Conocarpus erectus',
                        data: coordenatesCirculeParcelCE

                    },
                    {
                        type: 'scatter',
                        name: 'Laguncularia racemosa',
                        data: coordenatesCirculeParcelLR

                    },
                    {
                        type: 'scatter',
                        name: 'Mora oleifera ',
                        data: coordenatesCirculeParcelMO

                    },
                    {
                        type: 'scatter',
                        name: 'Pelliciera rhizophorae',
                        data: coordenatesCirculeParcelPR

                    },
                    {
                        type: 'scatter',
                        name: 'Rhizophora harrisonii',
                        data: coordenatesCirculeParcelRH

                    },
                    {
                        type: 'scatter',
                        name: 'Rhizophora mangle',
                        data: coordenatesCirculeParcelRM

                    },
                    {
                        type: 'scatter',
                        name: 'Rhizophora racemosa',
                        data: coordenatesCirculeParcelRR

                    },
                    {
                        type: 'scatter',
                        name: 'No especificado',
                        data: coordenatesCirculeParcelSP

                    }


                ]
            });



        }, "json");
    };
    $scope.editElement = function (element, parcelId, SpeciesId, ShadeParcel) {

        $("#oidelement").show();
        $("#labeloidelement").show();
        var patt1 = /\d/g;
        var result = element.match(patt1);
        pk = result.toString().replace(/,/g, '');
        $("#oidelement").val(pk);

        /*Parcela circular*/



        if (ShadeParcel == '45') {
            $scope.visibleDistanceAndAzimutField();
            $scope.hideXAndYField();
           
        } else {
            $scope.hideDistanceAndAzimutField();
            $scope.visibleXAndYField();
           
        }


        $("#" + element).each(function () {

            $(this).children("td").each(function (index) {
                var $td = $(this);
                if (index == 2) {
                    $scope.idindividuo = $td.text();
                }
                if (index == 3) {
                    $scope.tagindividuo = $td.text();
                }
                if (index == 4) {
                    $("#codspecies").val(SpeciesId);

                }
                if (index == 5) {
                    if ($td.text() == 'No aplica') {
                        $scope.azimut = 0;

                    } else {
                        $scope.azimut = $td.text();

                    }

                }
                if (index == 6) {
                    if ($td.text() == 'No aplica') {
                        $scope.distance = 0;

                    } else {
                        $scope.distance = $td.text();

                    }

                }
                if (index == 7) {
                    if ($td.text() == 'No aplica') {
                        $scope.coorx = 0;
                    } else {
                        $scope.coorx = $td.text();
                    }


                }

                if (index == 8) {

                    if ($td.text() == 'No aplica') {
                        $scope.coory = 0;

                    } else {
                        $scope.coory = $td.text();
                    }

                }
                if (index == 9) {
                    $("#codparcel").val(parcelId);
                    $("select#codparcel").change();

                }


            });
        });
    };
    $scope.register = function (Type) {

        if (angular.isUndefined($scope.codarcel) || $scope.codparcel == null)
            $scope.copdparcel = 0;
        if (angular.isUndefined($scope.idindividuo) || $scope.idindividuo == null)
            $scope.idindividuo = 0;

        if (angular.isUndefined($scope.tagindividuo) || $scope.tagindividuo == null)
            $scope.tagindividuo = 0;

        if (angular.isUndefined($scope.distance) || $scope.distance == null)
            $scope.distance = -1;
        if (angular.isUndefined($scope.azimut) || $scope.azimut == null)
            $scope.azimut = -1;
        if (angular.isUndefined($scope.coory) || $scope.coory == null)
            $scope.coory = -1;
        if (angular.isUndefined($scope.coorx) || $scope.coorx == null)
            $scope.coorx = -1;
        url = "";
        if (Type == 'new') {
            url = "/argos/argoservice?pkg=actions.sigma.&action=RegisterCoordenadasIndividuos";
        } else {
            url = "/argos/argoservice?pkg=actions.sigma.&action=UpdateCoordenadasIndividuos";
        }


        $.post(url, {
            codParcel: $("#codparcel").val(),
            idindividuo: $scope.idindividuo,
            tagindividuo: $scope.tagindividuo,
            codSpecie: $("#codspecies").val(),
            distance: $scope.distance,
            coorx: $scope.coorx,
            coory: $scope.coory,
            azimut: $scope.azimut,
            entity: myEntity,
            pk: $("#oidelement").val()
        }, function (data) {



            if (data.result == 0) {
                $("#divmessage").show();
                $("#divmessage").removeClass("alert-danger");
                $("#divmessage").addClass("alert-info");
                $("#messagealert").html("La operación fue exitosa");
            } else {
                $("#divmessage").show();
                $("#divmessage").addClass("alert-danger");
                $("#divmessage").removeClass("alert-info");
                $("#messagealert").html(data.message);
            }


            if ($.fn.dataTable.isDataTable('#example1')) {

                table = $('#example1').DataTable();
                table.destroy();
            }

            url = "/argos/argoservice?pkg=actions.sigma.&action=ListCoordenateIndividuos&entity=" + myEntity;
            $scope.drawTable(url);
        }, "json");
    };
    $scope.drawTable = function (url) {

        $("#tablebody").empty();
        $.post(url, {}, function (data) {

            jQuery.each(data, function (index, item) {
                /*parcela circular*/
                if (item.FORMA_PARCELA == '45') {
                    item.X = 'No aplica';
                    item.Y = 'No aplica';
                } else {
                    item.AZIMUT = 'No aplica';
                    item.DISTANCIA = 'No aplica';
                }

                var $html = $("<tr id='pk" + item.PK + "'><td><button type='button' class='btn btn-primary' ng-click='editElement(\"pk" + item.PK + "\",\"" + item.ID_PARCELA + "\",\"" + item.CODIGO_ESPECIE + "\",\"" + item.FORMA_PARCELA + "\")'>Editar</button></td><td>" + item.PK + " </td><td >" + item.ID_ELEMENTO + "</td><td>" + item.TAG + "</td><td>" + item.nombreEspecie + "</td><td>" + item.AZIMUT + "</td><td>" + item.DISTANCIA + "</td><td>" + item.X + "</td><td>" + item.Y + "</td><td>" + item.nombreParcela + "</td></tr>").appendTo('#tablebody');
                $compile($html)($scope);



            });
            $('#example1').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "language": {
                    "lengthMenu": "Visualizando  _MENU_ registros por p&aacute;gina",
                    "zeroRecords": "Cargando informaci&oacute;n...",
                    "info": "Visualizando  p&aacute;gina  _PAGE_ of _PAGES_",
                    "infoEmpty": "Informaci&oacute;n no encontrada",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente",
                    },
                }

            });


        }, "json");
    };
    $scope.handleOnChangeCodParcel = function () {
        parcelName = $("#codparcel option:selected").text();
        vector = parcelName.split(' ');
       
        if (vector[1] == 'Circular') {
            $scope.hideXAndYField();
            $scope.visibleDistanceAndAzimutField();
        } else {
            $scope.visibleXAndYField();
            $scope.hideDistanceAndAzimutField();
        }
    }
});








