<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true"%>
<%
    String username = null;

    String projectid = null, projectname = null, projecttitle = null, idEntity = null, userid = null;
    String CSS = "siamcss.css";
    String entity = "";

    try {
        username = session.getAttribute("username").toString();
        projectid = session.getAttribute("projectid").toString();
        projectname = session.getAttribute("projectname").toString();
        projecttitle = session.getAttribute("projecttitle").toString();
        idEntity = (String) session.getAttribute("entityid").toString();

        userid = (String) session.getAttribute("userid").toString();
        CSS = (String) session.getAttribute("CSSPRO");

      
        
    } catch (Exception e) {
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%
    }

%>   

<!DOCTYPE html>
<html ng-app="dashboardsigma">
    <head>
        <meta charset="UTF-8">
        <title>Dashboard SIGMA</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.4 -->
        <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="../../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="../../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->



    </head>
    <body class="skin-blue sidebar-mini" ng-controller="view" data-ng-init="init()"  >

        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="../../../../index.jsp" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>SIGMA</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li class="dropdown messages-menu" style="display:none">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="display:none">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="label label-success">4</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 4 messages</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li><!-- start message -->
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                                                    </div>
                                                    <h4>
                                                        Support Team
                                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li><!-- end message -->
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="../../dist/img/user3-128x128.jpg" class="img-circle" alt="user image"/>
                                                    </div>
                                                    <h4>
                                                        AdminLTE Design Team
                                                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="../../dist/img/user4-128x128.jpg" class="img-circle" alt="user image"/>
                                                    </div>
                                                    <h4>
                                                        Developers
                                                        <small><i class="fa fa-clock-o"></i> Today</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="../../dist/img/user3-128x128.jpg" class="img-circle" alt="user image"/>
                                                    </div>
                                                    <h4>
                                                        Sales Department
                                                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="../../dist/img/user4-128x128.jpg" class="img-circle" alt="user image"/>
                                                    </div>
                                                    <h4>
                                                        Reviewers
                                                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                                                    </h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">See All Messages</a></li>
                                </ul>
                            </li>
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="display:none">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-warning">10</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 10 notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-red"></i> 5 new members joined
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-user text-red"></i> You changed your username
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">View all</a></li>
                                </ul>
                            </li>
                            <!-- Tasks: style can be found in dropdown.less -->
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="display:none">
                                    <i class="fa fa-flag-o"></i>
                                    <span class="label label-danger">9</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 9 tasks</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li><!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                        Design some buttons
                                                        <small class="pull-right">20%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">20% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li><!-- end task item -->
                                            <li><!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                        Create a nice theme
                                                        <small class="pull-right">40%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">40% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li><!-- end task item -->
                                            <li><!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                        Some task I need to do
                                                        <small class="pull-right">60%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">60% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li><!-- end task item -->
                                            <li><!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                        Make beautiful transitions
                                                        <small class="pull-right">80%</small>
                                                    </h3>
                                                    <div class="progress xs">
                                                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">80% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li><!-- end task item -->
                                        </ul>
                                    </li>
                                    <li class="footer">
                                        <a href="#">View all tasks</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="display:none">
                                    <img  style="display:none" src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                                    <span class="hidden-xs"><a href="../../../../login.jsp?action=cerrarSession">Salir de forma segura</a></span>
                                </a>
                                <ul class="dropdown-menu" style="display:none">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                                        <p >
                                            Salir de forma segura
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body">
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Followers</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Sales</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Friends</a>
                                        </div>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li style="display:none">
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel" style="display:none">
                        <div class="pull-left image">
                            <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Alexander Pierce</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form" style="display:none">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='button' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>

                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Men� Pincipal</li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Registro de Invididuos</span> <i class="fa fa-angle-left pull-right" style="display:none"></i>
                            </a>
                            <ul class="treeview-menu" style="display:none">
                                <li style="display:none"><a href="../../indexold.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                                <li style="display:none"><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                            </ul>
                        </li>
                        <li class="treeview" style="display:none">
                            <a href="#">
                                <i class="fa fa-files-o"></i>
                                <span>Layout Options</span>
                                <span class="label label-primary pull-right">4</span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
                                <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
                                <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
                                <li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="../widgets.html" style="display:none">
                                <i class="fa fa-th"></i> <span>Widgets</span> <small class="label pull-right bg-green">new</small>
                            </a>
                        </li>
                        <li class="treeview" style="display:none">
                            <a href="#">
                                <i class="fa fa-pie-chart"></i>
                                <span>Charts</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
                                <li><a href="../charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                                <li><a href="../charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                                <li><a href="../charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                            </ul>
                        </li>
                        <li class="treeview" style="display:none">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>UI Elements</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu" >
                                <li><a href="../UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                                <li><a href="../UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                                <li><a href="../UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                                <li><a href="../UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                                <li><a href="../UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                                <li><a href="../UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
                            </ul>
                        </li>
                        <li class="treeview" style="display:none">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>Forms</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
                                <li><a href="../forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
                                <li><a href="../forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
                            </ul>
                        </li>
                        <li class="treeview active" style="display:none">
                            <a href="#">
                                <i class="fa fa-table"></i> <span>Tables</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
                                <li class="active"><a href="data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="../calendar.html" style="display:none">
                                <i class="fa fa-calendar"></i> <span>Calendar</span>
                                <small class="label pull-right bg-red">3</small>
                            </a>
                        </li>
                        <li>
                            <a href="../mailbox/mailbox.html" style="display:none">
                                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                                <small class="label pull-right bg-yellow">12</small>
                            </a>
                        </li>
                        <li class="treeview" style="display:none">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Examples</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="../examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
                                <li><a href="../examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
                                <li><a href="../examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
                                <li><a href="../examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
                                <li><a href="../examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
                                <li><a href="../examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
                                <li><a href="../examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
                            </ul>
                        </li>
                        <li class="treeview" style="display:none">
                            <a href="#">
                                <i class="fa fa-share"></i> <span>Multilevel</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                                <li>
                                    <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu">
                                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                                <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                            </ul>
                        </li>
                        <li><a href="../../documentation/index.html" style="display:none" ><i class="fa fa-book"></i> <span>Documentation</span></a></li>
                        <li class="header" style="display:none">LABELS</li>
                        <li><a href="#" style="display:none"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
                        <li><a href="#" style="display:none"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
                        <li><a href="#" style="display:none"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Registro de Individuos
                        <small>M&oacute;dulo de gesti&oacute;n de coordenadas</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../../../../index.jsp"><i class="fa fa-dashboard"></i> Inicio</a></li>
                        <li><a href="../../../../index.jsp">Menu SIGMA</a></li>
                        <li class="active">Gestion de coordenadas</li>
                    </ol>
                </section>

                <!-- Main content -->
                 <form role="form" class="form-horizontal"  >
                <section class="content">

                    <div class="row clearfix">
                        <div class="col-md-8 column">                               

                           
                                <div class="box-body">
                                    
                                      <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a aria-expanded="true" href="#tab_1" data-toggle="tab">Informaci&oacute;n</a></li>
                                        <li class=""><a aria-expanded="false" href="#tab_2" data-toggle="tab" style="display:none">Investigadores que aportaron datos</a></li>
                                        <li class=""><a aria-expanded="false" href="#tab_3" data-toggle="tab"  style="display:none">Tab 3</a></li>                                       
                                        <li class="pull-right" style="display:none"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            <p style="text-align: justify;font-size: 12px">En este m&oacute;dulo podr&aacute; registrar las coordenadas en las que se ubican espacialmente los individuos en las parcelas de monitoreo.Para parcelas rectangulares o cuadradas ingrese las distancias X o Y de cada individuo medidas desde un punto 0. Para parcelas circulares, ingrese la distancia de cada individuo al centro de la parcela, as&iacute; como su grado azimut. En ambos casos deber&aacute; registrar el ID del individuo (c&oacute;digo de identificaci&oacute;n en la Base de datos), su TAG (C&oacute;digo de identificaci&oacute;n en campo), as&iacute; como su especie.   
                                            </p>

                                        </div><!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_2">
                                            <ul id="listtab2"></ul>
                                        </div><!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_3">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                            It has survived not only five centuries, but also the leap into electronic typesetting,
                                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                                            like Aldus PageMaker including versions of Lorem Ipsum.
                                        </div><!-- /.tab-pane -->
                                    </div><!-- /.tab-content -->
                                </div>
                                    
                                    <div class="form-group">
                                        <label for="oidelement" id="labeloidelement" >Id Registro:</label>
                                        <output name="oidelement" id="oidelement"  ></output>

                                    </div>
                                    <div class="form-group">
                                        <label for="codparcel">Parcela</label>
                                        <select class="form-control" name="codparcel" id="codparcel" ng-model="codparcel" ng-change="handleOnChangeCodParcel()" >
                                            <option value="-1">----- Seleccione parcela -------</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="idtree">Id Individuo:Ejemplo: 25</label>
                                        <input class="form-control" id="idtree" placeholder="Id Individuo" name="idindividuo" id="idindividuo" type="text" ng-model="idindividuo" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="tagtree">Tag Individuo:Ejemplo: 58</label>
                                        <input class="form-control" id="tagtree" placeholder="Tag Individuo" type="text" ng-model="tagindividuo" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="codspecies">Especie</label>
                                        <select class="form-control" name="codspecies" id="codspecies" ng-model="codSpecie" required>
                                            <option value="-1">----- Seleccione especie -------</option>

                                        </select>
                                    </div>
                                    <div class="form-group" id="groupdistance">
                                        <label for="distance">Distancia (m):Ejemplo: 1.25 o 25</label>
                                        <input class="form-control" id="distance" placeholder="Distancia" type="text" ng-model="distance" required>
                                    </div>
                                    <div class="form-group" id="groupazimut" >
                                        <label for="azimut">Azimut: De 0 a 360 �(grados)</label>
                                        <input class="form-control" id="azimut" placeholder="Azimut" min="0" max="360" type="text" type="number" ng-model="azimut" >
                                    </div>
                                    <div class="form-group" style="" id="groupx">
                                        <label for="x">X(m): Ejemplo: 85 o 85.26</label>
                                        <input class="form-control" id="x" placeholder="X" type="text" ng-model="coorx" >
                                    </div>
                                    <div class="form-group" style="" id="groupy">
                                        <label for="y">Y(m):Ejemplo: 25 o 25.6</label>
                                        <input class="form-control" id="y" placeholder="Y" type="text" ng-model="coory" >
                                    </div>



                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <button type="button" class="btn btn-primary" ng-click="register('new')">Registrar</button>
                                    <button type="button" class="btn btn-primary" ng-click="register('update')">Actualizar</button>
                                </div>

                                <div class="alert alert-info alert-dismissable" id="divmessage">
                                    <button type="button" class="close" data-dismiss="alert" style="display:none" aria-hidden="true">�</button>
                                    <h4><i class="icon fa fa-info"></i> Mensaje del sistema!</h4>
                                    <span id="messagealert"></span>
                                </div>



                           



                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Individuos registrados.</h3>
                                    <a id='export' href="/argos/argoservice?pkg=actions.sigma.&action=ExportExcelListCoordenateIndividuos&entity=<%=idEntity%>"><img src="../../../../images/excel.png" width="50px"  title="Exportar a Excel" /></a>           
                                </div><!-- /.box-header -->
                                <div id='dvData' class="box-body">
                                    <table id="example1" class="table table-bordered table-striped"  >
                                        <thead>
                                            <tr>
                                                <th>Accion</th>
                                                <th>ID registro</th>
                                                <th>ID elemento</th>
                                                <th>TAG</th>
                                                <th>Especie</th>
                                                <th>Azimut <br/>(Grados)</th>
                                                <th>Distancia(m)</th>
                                                <th>X(m)</th>
                                                <th>Y(m)</th>
                                                <th>Parcela</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tablebody">

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Accion</th>
                                                <th>ID elemento</th>
                                                <th>ID registro</th>
                                                <th>TAG</th>
                                                <th>Especie</th>
                                                <th>Azimut<br/>(Grados)</th>
                                                <th>Distancia(m)</th>
                                                <th>X(m)</th>
                                                <th>Y(m)</th>
                                                <th>Parcela</th>


                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->







                        </div>
                        <div class="col-md-4 column">
                            
                            <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a aria-expanded="true" href="#tab_1" data-toggle="tab">Informaci&oacute;n</a></li>
                                        <li class=""><a aria-expanded="false" href="#tab_2" data-toggle="tab" style="display:none">Investigadores que aportaron datos</a></li>
                                        <li class=""><a aria-expanded="false" href="#tab_3" data-toggle="tab"  style="display:none">Tab 3</a></li>                                       
                                        <li class="pull-right" style="display:none"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            <p style="text-align: justify;font-size: 12px">Consulte la ubicaci�n espacial de los individuos registrados en cada una de sus parcelas seg&uacute;n corresponda (circular/rectangular o cuadrada) en los siguientes paneles:   
                                            </p>

                                        </div><!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_2">
                                            <ul id="listtab2"></ul>
                                        </div><!-- /.tab-pane -->
                                        <div class="tab-pane" id="tab_3">
                                            
                                        </div><!-- /.tab-pane -->
                                    </div><!-- /.tab-content -->
                                </div>

                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Grafique la parcela Circular</h3> 
                                    <span data-original-title="" data-toggle="tooltip" title="" class="badge bg-light-blue" style="cursor:pointer" ng-click="graphCirculeParcel()">Refrescar</span>
                                    <select class="form-control" name="circuleparcel" id="circuleparcel" ng-model="circuleparcel" ng-change="graphCirculeParcel()">
                                            <option value="-1">----- Seleccione parcela circular -------</option>

                                        </select>
                                    
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-box-tool" data-widget="remove" style="display:none"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="chart">
                                        <div id="polarchart" style="min-width: 310px; max-width: 400px; height: 400px; margin: 0 auto"></div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div>


                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Grafique parcela rectangular</h3>
                                    <span data-original-title="" data-toggle="tooltip" title="" class="badge bg-light-blue" style="cursor:pointer" ng-click="graphRentangleParcel()">Refrescar</span>
                                    <select class="form-control" name="rectangleparcel" id="rectangleparcel" ng-model="rectangleparcel" ng-change="graphRentangleParcel()">
                                            <option value="-1">----- Seleccione parcela rectangular -------</option>

                                    </select>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-box-tool" data-widget="remove" style="display:none"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="chart">
                                        <div id="bubbles" style="height: 400px; min-width: 310px; max-width: 600px; margin: 0 auto"></div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div>

                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <i class=""></i>
                                    <h3 class="box-title">Informaci&oacute;n</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body clearfix">
                                    <blockquote class="">
                                        <p style="text-align: justify;font-size: 12px">El tama�o de los c�rculos en las gr&aacute;ficas no corresponde al di&aacute;metro de cada &aacute;rbol, solo constituyen una representaci&oacute;n gr&aacute;fica de los individuos monitoreados</p>
                                        
                                    </blockquote>
                                </div><!-- /.box-body -->
                            </div>


                        </div>
                    </div>




                </section><!-- /.content -->
                </form>
            </div><!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs" style='display:none'>
                    <b >Version</b> 2.0
                </div>
                <strong>Copyright &copy; 2014-2015 <a  style='display:none' href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
            </footer>

            <!-- Control Sidebar -->      
            <aside class="control-sidebar control-sidebar-dark">                
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li><a href="#control-sidebar-home-tab" data-toggle="tab"  style="display:none"><i class="fa fa-home"></i></a></li>

                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"  style="display:none"><i class="fa fa-gears"></i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class='control-sidebar-menu'>
                            <li>
                                <a href='javascript::;'>
                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href='javascript::;'>
                                    <i class="menu-icon fa fa-user bg-yellow"></i>
                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
                                        <p>New phone +1(800)555-1234</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href='javascript::;'>
                                    <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>
                                        <p>nora@example.com</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href='javascript::;'>
                                    <i class="menu-icon fa fa-file-code-o bg-green"></i>
                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
                                        <p>Execution time 5 seconds</p>
                                    </div>
                                </a>
                            </li>
                        </ul><!-- /.control-sidebar-menu -->

                        <h3 class="control-sidebar-heading">Tasks Progress</h3> 
                        <ul class='control-sidebar-menu'>
                            <li>
                                <a href='javascript::;'>               
                                    <h4 class="control-sidebar-subheading">
                                        Custom Template Design
                                        <span class="label label-danger pull-right">70%</span>
                                    </h4>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                    </div>                                    
                                </a>
                            </li> 
                            <li>
                                <a href='javascript::;'>               
                                    <h4 class="control-sidebar-subheading">
                                        Update Resume
                                        <span class="label label-success pull-right">95%</span>
                                    </h4>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                    </div>                                    
                                </a>
                            </li> 
                            <li>
                                <a href='javascript::;'>               
                                    <h4 class="control-sidebar-subheading">
                                        Laravel Integration
                                        <span class="label label-waring pull-right">50%</span>
                                    </h4>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                    </div>                                    
                                </a>
                            </li> 
                            <li>
                                <a href='javascript::;'>               
                                    <h4 class="control-sidebar-subheading">
                                        Back End Framework
                                        <span class="label label-primary pull-right">68%</span>
                                    </h4>
                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                    </div>                                    
                                </a>
                            </li>               
                        </ul><!-- /.control-sidebar-menu -->         

                    </div><!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">            
                        
                            <h3 class="control-sidebar-heading">General Settings</h3>
                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Report panel usage
                                    <input type="checkbox" class="pull-right" checked />
                                </label>
                                <p>
                                    Some information about this general settings option
                                </p>
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Allow mail redirect
                                    <input type="checkbox" class="pull-right" checked />
                                </label>
                                <p>
                                    Other sets of options are available
                                </p>
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Expose author name in posts
                                    <input type="checkbox" class="pull-right" checked />
                                </label>
                                <p>
                                    Allow the user to show his name in blog posts
                                </p>
                            </div><!-- /.form-group -->

                            <h3 class="control-sidebar-heading">Chat Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Show me as online
                                    <input type="checkbox" class="pull-right" checked />
                                </label>                
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Turn off notifications
                                    <input type="checkbox" class="pull-right" />
                                </label>                
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Delete chat history
                                    <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                                </label>                
                            </div><!-- /.form-group -->
                        </form>
                    </div><!-- /.tab-pane -->
                </div>
            </aside><!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class='control-sidebar-bg'></div>
        </div><!-- ./wrapper -->
 


        <script src="../../../../js/angular.min.js"></script>
        <!-- jQuery 2.1.4 -->
        <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="../../plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="../../plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <!-- SlimScroll -->
        <script src="../../plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <!-- FastClick -->
        <script src='../../plugins/fastclick/fastclick.min.js'></script>
        <!-- AdminLTE App -->
        <script src="../../dist/js/app.min.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../../dist/js/demo.js" type="text/javascript"></script>

        <script src="http://code.highcharts.com/highcharts.js"></script>  
        <script src="http://code.highcharts.com/highcharts-more.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
        <script src="../../js/dashboardsigma.js"></script>
        <script src="../../js/librarysiam.js"></script>
        <script src="../../../../js/excellentexport.min.js"></script>
        <!-- page script -->
        <script type="text/javascript">

                                                        myEntity = '<%=idEntity%>';
                                                       
                                                                                                              
       </script>

    </body>
</html>
