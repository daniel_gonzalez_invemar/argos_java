<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Informaci&oacute;n de Areas de bosques de Manglar</title>
        <!-- Latest compiled and minified CSS -->
        <script src="js/excellentexport.min.js"></script>
        <script src="js/jquery.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">  
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


        <script src="js/jquery.dataTables.js"></script>
        <link rel="stylesheet" href="css/jquery.dataTables.css"/>    

        <link rel="stylesheet" href="http://cdn.datatables.net/responsive/1.0.6/css/dataTables.responsive.css"/>
        <link rel="stylesheet" href="http://cdn.datatables.net/responsive/1.0.6/js/dataTables.responsive.js"/>

        <script>


            $(document).ready(function () {

                fillTable(86);
            });
            function Activate(element) {

                $("[role='presentation']").removeClass("active");
                $(element).addClass("active");
                fillTable(element.id);
            }

            function requestTable(columnArray, typeCaseStudio) {


                var table = $('#example').dataTable({
                    responsive: true,
                    "order": [[0, "desc"]],
                    "ajax": "/argos/argoservice?pkg=actions.sigma.&action=ListCaseStudioByType&casestudiotype=" + typeCaseStudio,
                    "columns": columnArray,
                    "language": {
                        "lengthMenu": "Visualizando  _MENU_ registros por p&aacute;gina",
                        "zeroRecords": "Cargando informaci&oacute;n...",
                        "info": "Visualizando  p&aacute;gina  _PAGE_ of _PAGES_",
                        "infoEmpty": "Informaci&oacute;n no encontrada",
                        "search": "B&uacute;squeda:",
                        "paginate": {
                            "previous": "Anterior",
                            "next": "Siguiente",
                        },
                        "infoFiltered": "(filtrado de un total de  _MAX_ registros)",
                        "sPaginationType": "full_numbers",
                        "dom": '<"top"flp>rt<"bottom"i><"clear">'
                    }
                });
            }
            function fillTable(typeCaseStudio) {

                var ca;
                $('#headertable').empty();
                $('#tbodydata').empty();
                $('.btn-lg').hide();
                if (typeCaseStudio == 86)/*sectores*/ {
                    $('#headertable').append("<tr><th>id</th><th >Nombre Sector</th><th style='display:none' >Nombre corto sector</th><th style='display:none' >Nombre Corto</th><th>Regi&oacute;n</th><th>Departamento</th><th>UAC</th><th>Zona Protegida</th><th>Cuenca hidrogr&aacute;fica</th><th>Descripci&oacute;n</th><th>CAR que Administra</th><th style='display:none' >Tipo Unidad de Manejo</th><th style='display:none' >Azimut</th><th style='display:none' >Fecha Instalaci&oacute;n</th><th style='display:none' >&Aacute;rea</th><th style='display:none' >Forma de parcela</th><th style='display:none' >Longitud</th><th style='display:none' >Latidud</th><th style='display:none' >Tipo fisiogr&aacute;fico</th><th style='display:none' >Datos Disponibles</th></tr>");
                    ca = [{"data": "id"}, {"data": "nombre"}, {"data": "nombresector", "visible": false}, {"data": "nombrecorto", "visible": false}, {"data": "nombreregion"}, {"data": "departamento"}, {"data": "UAC"}, {"data": "zonaprotegida"}, {"data": "cuencahidrografica"}, {"data": "descripcion", "render": function (data, type, full, meta) {
                                return '<textarea rows="2" cols="50">' + data + '</textarea>';
                            }}, {"data": "CAR_Administra"}, {"data": "categoriaUnidadManejo", "visible": false}, {"data": "azimut", "visible": false}, {"data": "fechaInstalacion", "visible": false}, {"data": "area", "visible": false}, {"data": "formaParcela", "visible": false}, {"data": "longitud", "visible": false}, {"data": "latitud", "visible": false}, {"data": "tipoFisiografico", "visible": false}, {"data": "datosdisponibles", "visible": false}];
                } else if (typeCaseStudio == 2)/*zona manejo*/ {

                    $('#headertable').append("<tr><th>id</th><th >Nombre</th><th >Nombre sector</th><th style='display:none' >Nombre Corto</th><th>Regi&oacute;n</th><th>Departamento</th><th>UAC</th><th>Zona Protegida</th><th>Cuenca hidrogr&aacute;fica</th><th>Descripci&oacute;n</th><th>CAR que Administra</th><th style='' >Tipo Unidad de Manejo</th><th style='display:none' >Azimut</th><th style='display:none' >Fecha Instalaci&oacute;n</th><th style='display:none' >&Aacute;rea</th><th style='display:none' >Forma de parcela</th><th style='display:none' >Longitud</th><th style='display:none' >Latidud</th><th style='display:none' >Tipo fisiogr&aacute;fico</th><th style='display:none' >Datos Disponibles</th></tr>");
                    ca = [{"data": "id"}, {"data": "nombre"}, {"data": "nombresector", "visible": false}, {"data": "nombrecorto", "visible": false}, {"data": "nombreregion"}, {"data": "departamento"}, {"data": "UAC"}, {"data": "zonaprotegida"}, {"data": "cuencahidrografica"}, {"data": "descripcion", "render": function (data, type, full, meta) {
                                return '<textarea rows="2" cols="50">' + data + '</textarea>';
                            }}, {"data": "CAR_Administra"}, {"data": "categoriaUnidadManejo", "visible": true}, {"data": "azimut", "visible": false}, {"data": "fechaInstalacion", "visible": false}, {"data": "area", "visible": false}, {"data": "formaParcela", "visible": false}, {"data": "longitud", "visible": false}, {"data": "latitud", "visible": false}, {"data": "tipoFisiografico", "visible": false}, {"data": "datosdisponibles", "visible": false}];
                } else if (typeCaseStudio == 1)/*estaciones*/ {

                    $('#headertable').append("<tr><th>id</th><th >Nombre Estaci&oacute;n</th><th >Nombre sector</th><th style='display:none'>Nombre Corto</th><th>Regi&oacute;n</th><th>Departamento</th><th>UAC</th><th>Zona Protegida</th><th>Cuenca hidrogr&aacute;fica</th><th>Descripci&oacute;n</th><th>CAR que Administra</th><th style='' >Tipo Unidad de Manejo</th><th style='display:none' >Azimut</th><th style='display:none' >Fecha Instalaci&oacute;n</th><th style='display:none' >&Aacute;rea</th><th style='display:none' >Forma de parcela</th><th style='display:none' >Longitud</th><th style='display:none' >Latidud</th><th style='display:none' >Tipo fisiogr&aacute;fico</th><th style='display:none' >Datos Disponibles(muestras)</th></tr>");
                    ca = [{"data": "id"}, {"data": "nombre"}, {"data": "nombresector"}, {"data": "nombrecorto", "visible": false}, {"data": "nombreregion"}, {"data": "departamento"}, {"data": "UAC"}, {"data": "zonaprotegida"}, {"data": "cuencahidrografica"}, {"data": "descripcion", "render": function (data, type, full, meta) {
                                return '<textarea rows="2" cols="50">' + data + '</textarea>';
                            }}, {"data": "CAR_Administra"}, {"data": "categoriaUnidadManejo", "visible": true}, {"data": "azimut", "visible": false}, {"data": "fechaInstalacion", "visible": false}, {"data": "area", "visible": false}, {"data": "formaParcela", "visible": false}, {"data": "longitud", "visible": false}, {"data": "latitud", "visible": false}, {"data": "tipoFisiografico", "visible": false}, {"data": "datosdisponibles", "visible": false}];
                } else if (typeCaseStudio == 94)/*parcelas*/ {
                    $('#headertable').append("<tr><th>id</th><th >Nombre Parcela</th><th >Nombre sector</th><th style='display:none'>Nombre Corto</th><th>Regi&oacute;n</th><th>Departamento</th><th>UAC</th><th>Zona Protegida</th><th>Cuenca hidrogr&aacute;fica</th><th>Descripci&oacute;n</th><th>CAR que Administra</th><th style='' >Tipo Unidad de Manejo</th><th style='' >Azimut</th><th style='' >Fecha Instalaci&oacute;n</th><th style='' >&Aacute;rea(m2) </th><th style='' >Forma</th><th style='' >Longitud</th><th style='' >Latitud</th><th style='' >Tipo fisiogr&aacute;fico</th><th  >Datos Disponibles(muestras)</th></tr>");
                    ca = [{"data": "id"}, {"data": "nombre"}, {"data": "nombresector"}, {"data": "nombrecorto", "visible": false}, {"data": "nombreregion"}, {"data": "departamento"}, {"data": "UAC"}, {"data": "zonaprotegida"}, {"data": "cuencahidrografica"}, {"data": "descripcion", "render": function (data, type, full, meta) {
                                return '<textarea rows="2" cols="50">' + data + '</textarea>';
                            }}, {"data": "CAR_Administra"}, {"data": "categoriaUnidadManejo", "visible": true}, {"data": "azimut", "visible": true}, {"data": "fechaInstalacion", "visible": true}, {"data": "area", "visible": true}, {"data": "formaParcela", "visible": true}, {"data": "longitud", "visible": true}, {"data": "latitud", "visible": true}, {"data": "tipoFisiografico", "visible": true}, {"data": "datosdisponibles", "visible": true}];
                    $('.btn-lg').show();
                }


                if ($.fn.dataTable.isDataTable('#example')) {

                    table = $('#example').DataTable();
                    table.destroy();
                    requestTable(ca, typeCaseStudio);
                } else {
                    requestTable(ca, typeCaseStudio);
                }
            }

        </script>

    </head>
    <body style="">     
               
        <section class="content-header">
            <h1>
                M&oacute;dulo de caracterizaci&oacute;n
                <small style="display:none">Estad�sticas de regeneraci�n</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="http://sigma.invemar.org.co/"><i class="fa fa-dashboard"></i> Inicio</a></li>               
                <li class="active">Caracterizaci&oacute;n de bosques </li>
            </ol>
        </section>
        <div style="margin:20px">
            <p>
                <span>En este m&oacute;dulo podr&aacute; consultar  los sectores, estaciones y parcelas de monitoreo actualmente registrados en el Sistema de Informaci&oacute;n para la Gesti&oacute;n de los Manglares de Colombia (SIGMA). Consulte en la tabla su ubicaci&oacute;n, entidad responsable, cantidad de datos incluidos y otros aspectos de inter&eacute;s relacionados. Tenga en cuenta la siguiente informaci&oacute;n: </span>
                <h2>Sector:</h2>Unidad de bosque en un &aacute;rea geogr&aacute;fica con caracter&iacute;sticas estructurales, funcionales y niveles de intervenci&oacute;n humana que puede incluir una o m&oacute;s categor&iacute;as de manejo.
                <h2>Estaciones de monitoreo:</h2>&Aacute;rea de bosque dentro del sector que se define basada en criterios f&iacute;sicos y biol&oacute;gicos establecidos a una escala local con un inter&eacute;s particular de investigaci&oacute;n<br/>
                <h2> Parcela:</h2>Unidad de &aacute;rea que se establece f&iacute;sicamente en el bosque para realizar el monitoreo de algunos par&aacute;metros de inter&eacute:s.
                </p>

        </div>
        <ul class="nav nav-tabs">
            <li role="presentation" id="86" class="active" onclick="Activate(this)"><a href="#" title="Unidad de bosque en un �rea geogr�fica con caracteristicas estructurales, funcionales y niveles de intervenci�n humana que puede incluir una o m�s categor�as de manejo">Sectores</a></li>
            <li role="presentation" id="2" onclick="Activate(this)" style="display:none"><a href="#"  >Unidades de Manejo</a></li>
            <li role="presentation" id="1" onclick="Activate(this)"><a href="#" title="�rea de bosque dentro del sector que se define basada en criterios fisicos y biol�gicos establecidos a una escala local con un interes particular de investigaci�n">Estaciones</a></li>
            <li role="presentation" id="94"  onclick="Activate(this)"><a href="#" title="Unidad de �rea que se establece fisicamente en el bosque para realizar el monitoreo de algunos parametros de inter�s">Parcelas</a></li>
        </ul>
        <div>

            <a download="data.xls" href="#" onclick="return ExcellentExport.excel(this, 'example', 'Data');"><img src="img/excel.png" width="50px"  title="Exportar a Excel" /></a>           
           <!-- <a href="#" class="btn btn-lg" data-toggle="modal" data-target=".bs-example-modal-lg">
               <span class="glyphicon glyphicon-map-marker"></span> Ver ubicaci&oacute;n de parcelas.</a>-->
           <a  target="blank" href="http://siam.invemar.org.co/sibm-distribucion-espacial/2239" class="btn btn-lg"  >
               <span class="glyphicon glyphicon-map-marker"></span> Ver ubicaci&oacute;n de parcelas.</a>
           
            <table id="example" class="display responsive no-wrap compact" cellspacing="0" width="100%">
                <thead id="headertable">

                </thead>

                <tfoot id="headertable">

                </tfoot>

                <tbody id="tbodydata" >


                </tbody>
            </table>    
        </div>

        <!-- Large modal -->


        <!--<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <iframe f src="http://cinto.invemar.org.co/argos/estacionmap.jsp?proyecto=2239&nombre=SIGMA" width="100%" height="400px" ></iframe> 
                </div>
            </div>
        </div>-->



    </body>
</html>