<%-- 
    Document   : infocaracterizacionbosques
    Created on : 8/05/2015, 03:18:48 PM
    Author     : usrsig15
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Informacion caracterizacion de bosques </title>
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
        <script  type="text/javascript">

            fillTable(86);
            
            function fillTable(typeCaseStudio) {
 
                 
                $.post('/argos/argoservice?pkg=actions.sigma.&action=ListCaseStudioByType', {
                    casestudiotype: typeCaseStudio,
                }, function(data) {
                   $('#headertable').empty(); 
                   $('#tbodydata').empty();
                   
                    if (typeCaseStudio==86) {
                        
                      $('#headertable').append("<tr><th data-priority='1'>N�</th><th >id</th><th data-priority='2'>Nombre</th><th data-priority='3'>Nombre Corto</th><th data-priority='5'>Regi&oacute;n</th><th data-priority='10'>Departamento</th><th data-priority='11'>UAC</th><th data-priority='6'>Zona Protegida</th><th data-priority='7'>Cuenca hidrogr&aacute;fica</th><th data-priority='8'>Descripci&oacute;n</th><th data-priority='9'>CAR Administra</th></tr>");                      
                     jQuery.each(data.datos, function(index, item) {                      
                        $('#tbodydata').append("<tr><th>"+index+"</th><th >"+item.id+"</th><th>"+item.nombre+"</th><th>"+item.nombrecorto+"</th><th>"+item.nombreregion+"</th><th>"+item.zonaprotegida+"</th><th>"+item.departamento+"</th><th>"+item.UAC+"</th><th>"+item.cuencahidrografica+"</th><th><textarea rows='4' cols='50'>"+item.descripcion+"</textarea></th><th>"+item.CAR_Administra+"</th></tr>"); 
                    });
                    }else if (typeCaseStudio==2){
                       
                         $('#headertableManagmentUnit').empty(); 
                         $('#tbodydataManagmentUnit').empty();
                         
                         $('#headertableManagmentUnit').append("<tr><th data-priority='1'>N�</th><th >id</th><th data-priority='2'>Nombre</th><th data-priority='5'>Regi&oacute;n</th><th data-priority='6'>Zona Protegida</th><th data-priority='7'>Cuenca hidrogr&aacute;fica</th><th data-priority='8'>Descripci&oacute;n</th><th data-priority='9'>CAR Administra</th><th data-priority='10'>Tipo Unidad de Manejo</th></tr>");                      
                         jQuery.each(data.datos, function(index, item) {                      
                           $('#tbodydataManagmentUnit').append("<tr><th>"+index+"</th><th >"+item.id+"</th><th>"+item.nombre+"</th><th>"+item.nombreregion+"</th><th>"+item.zonaprotegida+"</th><th>"+item.cuencahidrografica+"</th><th><textarea rows='4' cols='50'>"+item.descripcion+"</textarea></th><th>"+item.CAR_Administra+"</th><th>"+item.categoriaUnidadManejo+"</th></tr>"); 
                         });
                         
                    }else if (typeCaseStudio==1){
                       
                         $('#headertableStations').empty(); 
                         $('#tbodydataStations').empty();
                         
                         $('#headertableStations').append("<tr><th data-priority='1'>N�</th><th >id</th><th data-priority='2'>Nombre</th><th data-priority='3'>Nombre Corto</th><th data-priority='5'>Regi&oacute;n</th><th data-priority='6'>Zona Protegida</th><th data-priority='7'>Cuenca hidrogr&aacute;fica</th><th data-priority='8'>Descripci&oacute;n</th><th data-priority='9'>CAR Administra</th><th data-priority='10'>Tipo Unidad de Manejo</th></tr>");                      
                         jQuery.each(data.datos, function(index, item) {                      
                           $('#tbodydataStations').append("<tr><th>"+index+"</th><th >"+item.id+"</th><th>"+item.nombre+"</th><th>"+item.nombrecorto+"</th><th>"+item.nombreregion+"</th><th>"+item.zonaprotegida+"</th><th>"+item.cuencahidrografica+"</th><th><textarea rows='4' cols='50'>"+item.descripcion+"</textarea></th><th>"+item.CAR_Administra+"</th><th>"+item.categoriaUnidadManejo+"</th></tr>"); 
                         });
                         
                    }else if (typeCaseStudio==94){
                       
                         $('#headertableParcel').empty(); 
                         $('#tbodydataParcel').empty();
                         
                         $('#headertableParcel').append("<tr><th data-priority='1'>N�</th><th >id</th><th data-priority='2'>Nombre Parcela</th><th data-priority='3'>Nombre Corto</th><th data-priority='5'>Regi&oacute;n</th><th data-priority='6'>Zona Protegida</th><th data-priority='7'>Cuenca hidrogr&aacute;fica</th><th data-priority='8'>Descripci&oacute;n</th><th data-priority='9'>CAR Administra</th><th data-priority='10'>Tipo Unidad de Manejo</th></tr>");                      
                         jQuery.each(data.datos, function(index, item) {                      
                           $('#tbodydataParcel').append("<tr><th>"+index+"</th><th >"+item.id+"</th><th>"+item.nombre+"</th><th>"+item.nombrecorto+"</th><th>"+item.nombreregion+"</th><th>"+item.zonaprotegida+"</th><th>"+item.cuencahidrografica+"</th><th><textarea rows='4' cols='50'>"+item.descripcion+"</textarea></th><th>"+item.CAR_Administra+"</th><th>"+item.categoriaUnidadManejo+"</th></tr>"); 
                         });
                         
                    }

                   
                }, "json");
            }
        </script>    
    </head>
    <body>       
        <div data-role="tabs" id="tabs">
            <div data-role="navbar">
                <ul>
                    <li><a href="#one" data-ajax="true" onclick="fillTable(86)" >Sectores</a></li>
                    <li><a href="#two" data-ajax="true" onclick="fillTable(2)">Unidades de manejo</a></li>
                    <li><a href="#three" data-ajax="true" onclick="fillTable(1)">Estaciones</a></li>
                    <li><a href="#four" data-ajax="true" onclick="fillTable(94)">Parcelas</a></li>
                </ul>
            </div>
            <div id="one" class="ui-body-d ui-content">	
                <form>
                    <input id="filterTable-input" data-type="search">
                </form>
                <table data-role="table" id="table-column-toggle"  data-filter="true" data-input="#filterTable-input"  data-mode="columntoggle" class="ui-responsive table-stroke">
                    <thead id="headertable">
                       
                    </thead>
                    <tbody id="tbodydata">
                                             
                    </tbody>
                </table>
            </div>
            <div id="two">
                <form>
                    <input id="ManagementUnitfilterTable-input" data-type="search">
                </form>
                <table data-role="table" id="tableManagmentUnit"  data-filter="true" data-input="#ManagementUnitfilterTable-input"  data-mode="columntoggle" class="ui-responsive table-stroke">
                    <thead id="headertableManagmentUnit">
                       
                    </thead>
                    <tbody id="tbodydataManagmentUnit">
                                             
                    </tbody>
                </table>
            </div>
            <div id="three">
                <form>
                    <input id="StationsfilterTable-input" data-type="search">
                </form>
                <table data-role="table" id="tableStations"  data-filter="true" data-input="#StationsfilterTable-input"  data-mode="columntoggle" class="ui-responsive table-stroke">
                    <thead id="headertableStations">
                       
                    </thead>
                    <tbody id="tbodydataStations">
                                             
                    </tbody>
                </table>
            </div>
             <div id="four">
                <form>
                    <input id="ParcelfilterTable-input" data-type="search">
                </form>
                <table data-role="table" id="tableParcel"  data-filter="true" data-input="#ParcelfilterTable-input"  data-mode="columntoggle" class="ui-responsive table-stroke">
                    <thead id="headertableParcel">
                       
                    </thead>
                    <tbody id="tbodydataParcel">
                                             
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
