<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true" import="co.org.invemar.user.UserPermission"%>
<%
		String username = null;
		UserPermission userp = null;
		String projectid = null, projectname=null, projecttitle=null;
		String CSS="siamcss.css";
		
    	try{
        	username = session.getAttribute("username").toString();
        	projectid = session.getAttribute("projectid").toString();
        	projectname = session.getAttribute("projectname").toString();
        	projecttitle = session.getAttribute("projecttitle").toString();
			CSS=(String)session.getAttribute("CSSPRO");
			
        	   		
    	}catch(Exception e){
		 %>
    		<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}
    	
    	if(username==null){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}else{
			//check user permission
			userp = (UserPermission)session.getAttribute("userpermission");
			
			if(!userp.couldUser(UserPermission.VER_MI_HISTORIAL_DE_EVENTOS_PARA_EL_PROYECTO_ACTUAL)&&!userp.couldUser(UserPermission.VER_HISTORIAL_DE_EVENTOS_DE_LA_ENTIDAD_PARA_EL_PROYECTO_ACTUAL)){
					%>
						<script type="text/javascript">location.href="index.jsp"</script>
		 			<%
			}
			
		}
try{
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="this is my page">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="css/<%=CSS %>" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/cmar.css" type="text/css">
  <link title="winter" href="js/lib/jscalendar/calendar-blue.css" media="all" type="text/css" rel="stylesheet"/>
  <script type="text/javascript" src="js/lib/prototype.js"></script>
  <script type="text/javascript" src="js/lib/scriptaculous/scriptaculous.js?load=effects"></script>
  <script type="text/javascript" src="js/lib/controlmodal.js"></script>
  <script type="text/javascript" src="js/lib/abop.js"></script>
  <script type="text/javascript" src="js/logadmin.js"></script>
</head>

<body marginwidth="0" marginheight="0">

<%@ include file="bannerp.jsp" %>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr class="tabla_fondotitulo">
      <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
      <td width="364" class="linksnegros">Historial de eventos</td>
      <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>
      
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0"
        cellspacing="0">
          <tbody>
            <tr>
              <td background="images/dotted_line.gif"><img
                src="images/dotted_line.gif" width="12" height="3"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/spacer.gif" width="3" height="3"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
  <tbody>
	<tr class="tabla_fondotitulo2">
      <td width="9">&nbsp;</td>
      <td width="503" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="index.jsp">Inicio</a> &gt; <a href="logadmin.jsp">Historial
          de eventos</a> </td>
      <td width="238" class="texttablas"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr>
      <td colspan="3" ><p align="justify" class="texttablas"><br>
          </p>
      </td>
    </tr>
    <tr>
      <td colspan="3" class="tabla_fondopagina"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="texttablas">
        <tr>
          <td width="60%" valign="top">Realice las selecciones solicitadas y
            a continuaci&oacute;n presione el bot&oacute;n Ver historial.<br></td>
        </tr>
      </table>
        <table id="uploadoptions" width="97%" border="0" align="center" cellpadding="0" cellspacing="0" class="texttablas">
          <tr>
            <td colspan="2"><div align="justify">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="texttablas" id="uploadoptions">
                  <tr>
                    <td width="100%"><div align="justify">
                        <p>Seleccione la tem&aacute;tica:<br>
                          <br>
                          <select name="schema" id="schema">
                            <option value="-1" selected>Seleccione tem&aacute;tica</option>
                          </select>
                          <br>
                          <br>
Seleccione el propietario del historial:<br>
<br>
<select name="userlist" id="userlist">
  <option value="-1" selected>Mostrar mi historial</option>
 <%
  			if(userp.couldUser(UserPermission.VER_HISTORIAL_DE_EVENTOS_DE_LA_ENTIDAD_PARA_EL_PROYECTO_ACTUAL)){
					%>
						  <option value="0">Mostrar historial de todos los usuarios</option>
		 			<%
			}
%>
  </select>
<br>
<br>
Filtrar historial por tipo de evento:<br>
<br>
<select name="event" id="event">
  <option value="0" selected>Todos los eventos</option>
</select>
(seleccione un filtro para ver los registros totales afectados)<br>
<br>
<input name="seelog" type="submit" id="seelog" value="Ver historial">
</p>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Historial de eventos:</td>
                  </tr>
                  <tr>
                    <td>&nbsp; </td>
                  </tr>
                  <tr>
                    <td>&nbsp;<div id="ABOPndiv_logtbody"></div></td>
                  </tr>
                  <tr>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td><table id="logtable" width="96%" border="0" align="center" cellpadding="2" cellspacing="3" class="texttablas">
                      <thead>
                        <tr bgcolor="#FFFFFF">
                          <td width="29%">Usuario</td>
                          <td width="34%">Evento</td>
                          <td width="15%" align="center">Registros afectados</td>
                          <td width="22%" align="center">Fecha/Hora</td>
                        </tr>
                      </thead>
                      <tbody id="logtbody">
                      </tbody>
                    </table></td>
                  </tr>
                  <tr>
                    <td>&nbsp;                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="37%">Registros totales afectados en el evento seleccionado: 
              </td>
            <td width="63%"><div id="eventstats"></div></td>
          </tr>
        </table>
        <p align="justify">&nbsp;      </p></td>
    </tr>
    <tr>
      <td colspan="3" ><p align="justify">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<%@ include file="footer.jsp" %>

</body>
</html>
<%
}catch(Exception e){}
%>