<%@page import="co.org.invemar.user.UserPermission"%>
<%@page import="java.util.*"%>
<%@page import="co.org.invemar.util.ConnectionFactory"%>
<%@page import="co.org.invemar.siam.sibm.model.ProyectosDM"%>
<%@page import="co.org.invemar.user.UserPermission"%>
<%@page import="java.sql.Connection"%>
<%
    response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    String username = null, projectname = null, projectid = null, projecttitle = null;
    String CSS = null;
    UserPermission userp = null;
  try{
    username = session.getAttribute("username").toString();
    projectid = session.getAttribute("projectid").toString();
    projectname = session.getAttribute("projectname").toString();
    projecttitle = session.getAttribute("projecttitle").toString();
    userp = null;
    CSS = "siamcss.css";
    CSS = (String) session.getAttribute("CSSPRO");
    username = session.getAttribute("username").toString();
    userp = (UserPermission) session.getAttribute("userpermission");
  }catch(Exception e){
      System.out.println("Un error ha ocurrido:"+e.getMessage());
  }


%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
        <link href="css/<%=CSS%>" rel="stylesheet" type="text/css">

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <META name="keywords" content="invemar, ciencia marina, investigac&iacute;on, marino, costera, costero">
        <meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/cmar.css" type="text/css">
    </head>

    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

        <%@ include file="bannerp.jsp" %>

        <jsp:directive.page import="co.org.invemar.siam.sibm.vo.Localidad"/>
        <jsp:directive.page import="co.org.invemar.siam.sibm.vo.Proyecto"/>
        <jsp:directive.page import="org.apache.commons.lang.*"/>

        <script type="text/javascript">

            function PopWindow(popPage, windowName, winWidth, winHeight)
            {

                //the center properties
                var winLeft = (screen.width - winWidth) / 2;
                var winTop = (screen.height - winHeight) / 2;

                newWindow = window.open(popPage, windowName, 'width=' + winWidth + ',height=' + winHeight + ',top=' + winTop + ',left=' + winLeft + ',resizable=yes,scrollbars=yes,status=yes');
            }
        </script>


        <div>

            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr class="tabla_fondotitulo">
                        <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
                        <td width="364" class="linksnegros">Panel de control </td>
                        <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>
                    </tr>
                </tbody>
            </table>


            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td><img src="images/spacer.gif" width="5" height="5"></td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
                <tbody>
                    <tr>
                        <td>
                            <table width="100%" border="0" align="center" cellpadding="0"
                                   cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td background="images/dotted_line.gif"><img
                                                src="images/dotted_line.gif" width="12" height="3"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="images/spacer.gif" width="3" height="3"></td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">

                <tr class="tabla_fondotitulo2">
                    <td width="9">&nbsp;</td>
                    <td width="503" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="index.jsp">Inicio</a> &gt; Ver estaciones </td>
                    <td width="238" class="texttablas"><div align="right"></div></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>

                <tr>
                    <td height="16" colspan="3" ><div align="center" class="linksnavinferior">
                            <form method="get" action="services" id="formproys">
                                <table cellpadding="0" border="0"><tr><td><span class="linksnegros">Seleccione el proyecto</span></td>
                                        <td>
                                            <%
                                                String proj = request.getParameter("proy");
                                                List<Proyecto> projects = (List) request.getAttribute("PROYS");
                                                Proyecto proyecto = null;
                                                if (proj == null) {
                                                    proyecto = projects.get(0);
                                                } else {
                                                    for (Proyecto project : projects) {
                                                        if (String.valueOf(project.getCodigo()).equals(proj)) {
                                                            proyecto = project;
                                                        }
                                                    }
                                                }    
                                            %>
                                            <select name="proy" >                                               
                                                <option value="<%=proyecto.getCodigo()%>" selected="selected"><%=proyecto.getNombreAlterno()%></option>                                            

                                            </select>

                                        </td>
                                    </tr></table>
                                <input type="hidden" name="action" value="Proyecto" /> 
                            </form>

                        </div></td>
                </tr>

            </table>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr>
                    <td background="images/dotted_line.gif"><img src="images/dotted_line.gif" width="12" height="3"></td>
                </tr>
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
            </table>


            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="tabla_fondotitulo2"> 
                    <td  >
                        <table border="0" cellspacing="0">
                            <td width="200" height="24"  align="left" class="linksnegros">Proyecto </td>
                            <td><span style="vertical-align:middle;">
                                    <%if (proyecto.getMetadato() != 0) {%>
                                    <a class="asistanttitle" target="_blank" href="http://cinto.invemar.org.co:8080/metadatos/showMetadato.jsp?conjunto=<%=proyecto.getMetadato()%>">Ver metadatos del proyecto aqu&iacute;</a>
                                    <%}%>

                                </span></td>
                        </table>
                    </td>
                </tr>
            </table>


            <table width="100%" border="0" align="center" cellpadding="4" cellspacing="1">


                <tr class="tabla_fondotitulo2">
                    <td nowrap="nowrap" class="titulostablas">Titulo</td>
                    <td nowrap="nowrap" class="titulostablas">Nombre alterno</td>
                    <td nowrap="nowrap" class="titulostablas">Periodo de ejecucion</td>
                </tr>
                <tr class="tabla_fondopagina">
                    <td class="texttablas"><%=proyecto.getTitulo()%></td>
                    <td nowrap="nowrap" class="texttablas" ><%=proyecto.getNombreAlterno()%></td>
                    <td nowrap="nowrap" class="texttablas" ><%=proyecto.getFechaInicio()%></td>
                </tr>
            </table>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr>
                    <td background="images/dotted_line.gif"><img src="images/dotted_line.gif" width="12" height="3"></td>
                </tr>
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
            </table>
            <%
                ConnectionFactory cFactory = null;
                Connection connection = null;
                cFactory = new ConnectionFactory();
                ProyectosDM pdm = new ProyectosDM();

                try {
                    connection = cFactory.createConnection();
                    List<Localidad> estaciones = pdm.findStationByProject(connection, proyecto.getCodigo(), userp, (String) session.getAttribute("pais"));


            %>


            <table align="center" border="0" width="100%">
                <tbody>
                    <tr  >
                        <td colspan="24" >
                            <table border="0" width="100%" cellspacing="0">
                                <tr class="tabla_fondopagina">
                                    <td  width="200" height="24"  align="left" class="linksnegros">Estaciones Asociadas </td>
                                    <td width="300"><img src="./images/browser.png" border="0" style="vertical-align:middle;"/><a class="linksnegros" href="javascript:PopWindow('estacionmap.jsp?proyecto=<%=proyecto.getCodigo()%>&nombre=<%=proyecto.getNombreAlterno()%>','Estaciones','890','600')">Ver distribuci&oacute;n espacial aqu&iacute;</a></td>
                                    <td ><img src="./images/Excel_icon.png" border="0" style="vertical-align:middle;"/><a class="linksnegros" href="./services?action=ProyectoExcel&proy=<%=proyecto.getCodigo()%>&name=<%=proyecto.getNombreAlterno()%>"> Descargar informaci&oacute;n en formato excel</a></td>
                                </tr>				
                            </table>
                        </td>
                    </tr>

                    <tr class="tabla_fondotitulo2">
                        <td nowrap="nowrap" class="titulostablas"><span class="titulostablas">Estacion</span></td>
                        <td nowrap="nowrap" class="titulostablas">Ecorregi&oacute;n</td>
                        <td nowrap="nowrap" class="titulostablas">Top&oacute;nimo</td>
                        <td nowrap="nowrap" class="titulostablas">Zona protegida</td>
                        <td nowrap="nowrap" class="titulostablas">Descripcion</td>
                        <td nowrap="nowrap" class="titulostablas">Pais</td>

                        <td nowrap="nowrap" class="titulostablas">Codigo  --- Lugar</td>
                        <td nowrap="nowrap" class="titulostablas">Latitud de inicio</td>
                        <td nowrap="nowrap" class="titulostablas">Latitud de fin</td>
                        <td nowrap="nowrap" class="titulostablas">Longitud de inicio</td>
                        <td nowrap="nowrap" class="titulostablas">Longitud de fin</td>
                        <td nowrap="nowrap" class="titulostablas">Cuerpo de agua</td>
                        <td nowrap="nowrap" class="titulostablas">Distancia de costa</td>
                        <td nowrap="nowrap" class="titulostablas">Tipo de costa</td>
                        <td nowrap="nowrap" class="titulostablas">Profundidad</td>


                        <td nowrap="nowrap" class="titulostablas">Barco</td>
                        <td nowrap="nowrap" class="titulostablas">Campa&ntilde;a</td>
                        <td nowrap="nowrap" class="titulostablas">Sustrato</td>
                        <td nowrap="nowrap" class="titulostablas">Ambiente</td>
                        <td nowrap="nowrap" class="titulostablas">Notas</td>
                        <td nowrap="nowrap" class="titulostablas">Mar o R&iacute;o</td>
                        <td nowrap="nowrap" class="titulostablas">Salinidad</td>
                        <td nowrap="nowrap" class="titulostablas">Profundidad min (Metros)</td>
                        <td nowrap="nowrap" class="titulostablas">Profundidad max (Metros)</td>


                    </tr>
                    <%for (Localidad estacion : estaciones) {%>
                    <tr class="tabla_fondopagina">
                        <td nowrap="nowrap" class="texttablas"><%=estacion.getPrefijoEstacion() == "" ? "" : estacion.getPrefijoEstacion() + "-"%><%=estacion.getCodigoEstacion()%></td>
                        <td nowrap="nowrap" class="texttablas"><%=estacion.getEcorregion()%></td>
                        <td nowrap="nowrap" class="texttablas"><%=estacion.getToponimo()%></td>
                        <td nowrap="nowrap" class="texttablas"><%=estacion.getZonaProtegida()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getDescripcionEstacion()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getPais()%></td>

                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getSecuencialoc()%>     -- <%=estacion.getLugar()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getLatitudInicio()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getLatitudFin()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getLongitudInicio()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getLongitudFin()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getCuerpoAgua()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getDistanciaCosta()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getTipoCosta()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getHondoAgua()%></td>


                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getBarco()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getCampana()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getSustrato()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getAmbiente()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getNotas()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getMarrio()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getSalodulce()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getProfMin()%></td>
                        <td nowrap="nowrap"  class="texttablas"><%=estacion.getProfMax()%></td>


                    </tr>
                    <%}%>

                </tbody>
            </table>
            <%} finally {
                    ConnectionFactory.closeConnection(connection);
                }%>


        </div>

        <%@ include file="footer.jsp" %>


    </body>
</html>