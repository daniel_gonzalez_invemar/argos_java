<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true"  import="co.org.invemar.user.UserPermission" %>
<%
		String username = null;
		String projectid = null;
		String projectname = null, projecttitle=null;
                String CSS="siamcss.css";
		
    	try{
        	username = session.getAttribute("username").toString();
        	projectid = session.getAttribute("projectid").toString();
        	projectname = session.getAttribute("projectname").toString();
        	projecttitle = session.getAttribute("projecttitle").toString();
                CSS=(String)session.getAttribute("CSSPRO");
        	   		
    	}catch(Exception e){
		 %>
    		<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}
    	
    	if(username==null){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="this is my page">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="css/<%=CSS %>" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/cmar.css" type="text/css">
  <script type="text/javascript" src="js/lib/prototype.js"></script>
  <script type="text/javascript" src="js/lib/scriptaculous/scriptaculous.js?load=effects"></script>
  <script type="text/javascript" src="js/lib/controlmodal.js"></script>
  <script type="text/javascript" src="js/lib/yav/yav.js"></script>
  <script type="text/javascript" src="js/lib/yav/yav-config-es.js"></script>
  <script type="text/javascript" src="js/usercpass.js"></script>
  <script type="text/javascript">
   
	var passrules=new Array();
	passrules[0]='password:contrase&ntilde;a|required';
	passrules[1]='password:contrase&ntilde;a|minlength|8';
	passrules[2]='password:contrase&ntilde;a|regexp|^(([0-9a-z_])*([^A-Za-z0-9_])*)+$|solo puede contener min&uacute;sculas, n&uacute;meros y caracteres espc';
	passrules[3]='cpassword:la confirmacion|equal|$password:la contrase&ntilde;a ingresada';
  </script>  
</head>

<body marginwidth="0" marginheight="0">

<%@ include file="bannerp.jsp" %>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr class="tabla_fondotitulo">
      <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
      <td width="364" class="linksnegros">Cambiar contrase&ntilde;a</td>
      <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0"
        cellspacing="0">
          <tbody>
            <tr>
              <td background="images/dotted_line.gif"><img
                src="images/dotted_line.gif" width="12" height="3"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/spacer.gif" width="3" height="3"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
  <tbody>
    <tr class="tabla_fondotitulo2">
      <td width="7" > </td>
      <td width="370" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp; <a href="index.jsp">Inicio</a> &gt;<a href="usercpass.jsp" id="route">Cambiar contrase&ntilde;a </a> </td>
      <td width="371" class="texttablas"></td>
    </tr>
    <tr>
      <td colspan="4" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr>
      <td colspan="4" ><p align="justify" class="texttablas"><br>
          </p>      </td>
    </tr>
    <tr>
      <td colspan="4" ><form name="passform" method="post" action="">
      <table id="geninfo" width="724" border="0" align="center" cellpadding="0" cellspacing="2" style="display:block;" class="texttablas">
        <tr>
          <td colspan="3"> </td>
        </tr>
        <tr>
          <td colspan="3"><strong>CAMBIAR CONTRASE&Ntilde;A :</strong></td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
          <td width="163" align="right">Contrase&ntilde;a</td>
          <td width="9">&nbsp;</td>
          <td width="552"><input name="password" type="password" id="password" size="30" maxlength="15"><span id="errorsDiv_password"/>          </td>
        </tr>
        <tr>
          <td align="right">Confirmar contrase&ntilde;a</td>
          <td>&nbsp;</td>
          <td><input name="cpassword" type="password" id="cpassword" size="30" maxlength="15"><span id="errorsDiv_cpassword"/>          </td>
        </tr>
        <tr>
          <td align="right">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><input name="save" type="button" id="save" value="Guardar">          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
        </form>      </td>
    </tr>
    <tr>
      <td ><p align="justify" class="texttablas"><br>
        </p>      </td>
      <td colspan="3" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4" ><p align="justify">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<%@ include file="footer.jsp" %>
</body>
</html>
