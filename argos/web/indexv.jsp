<%@page contentType="text/html; charset=iso-8859-1" language="java" session="true" import="co.org.invemar.user.UserPermission" %>
<%
    String username = null;
    UserPermission userp = null;
    String projectid = null, projectname = null, projecttitle = null, pais = null;
    String CSS = "siamcss.css";
    String userID = "";
    String idRol = "";
    if (null != session.getAttribute("username")) {
        try {
            username = session.getAttribute("username").toString();
            projectid = session.getAttribute("projectid").toString();
            projectname = session.getAttribute("projectname").toString();
            projecttitle = session.getAttribute("projecttitle").toString();
            userp = (UserPermission) session.getAttribute("userpermission");
            pais = session.getAttribute("pais").toString();
            CSS = (String) session.getAttribute("CSSPRO");
            idRol = (String) session.getAttribute("idrol");
            userID = (String) session.getAttribute("userid");

        } catch (Exception e) {
            e.printStackTrace();
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%
    }
} else if (username == null) {
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%    }
    try {
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
        <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
        <meta http-equiv="description" content="this is my page">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="css/<%=CSS%>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/cmar.css" type="text/css">
        <script type="text/javascript" src="js/lib/prototype.js"></script>
        <script type="text/javascript" src="js/lib/controlmodal.js"></script>        

    </head>

    <body marginwidth="0" marginheight="0" >
        <%@ include file="bannerp.jsp" %>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr class="tabla_fondotitulo">
                    <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
                    <td width="364" class="linksnegros">Panel de control </td>
                    <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%>/<%=pais%> </div></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
            </tbody>
        </table>
        <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
            <tbody>
                <tr>
                    <td>
                        <table width="100%" border="0" align="center" cellpadding="0"
                               cellspacing="0">
                            <tbody>
                                <tr>
                                    <td background="images/dotted_line.gif"><img
                                            src="images/dotted_line.gif" width="12" height="3"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><img src="images/spacer.gif" width="3" height="3"></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
            <tbody>
                <tr class="tabla_fondotitulo2">
                    <td width="7"> </td>
                    <td width="357" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="index.jsp">Inicio </a>
                    </td>
                    <td width="386" class="texttablas"><div align="right"></div></td>
                </tr>
                <tr>
                    <td colspan="3" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr>
                    <td colspan="3" ><p align="justify" class="texttablas"><br>
                        </p>      </td>
                </tr>
                <tr>
                    <td><p align="justify" class="texttablas"><br>
                        </p></td>
                    <td colspan="2" class="texttablas"><strong>Funciones primarias</strong> </td>
                </tr>
                <tr>
                    <td ><p align="justify" class="texttablas"><br>
                        </p></td>
                    <td colspan="2" ><table width="10" border="0" align="left" cellpadding="0" cellspacing="2" class="texttablas" id="detail" style="display:block;">
                            <tr>
                                <td colspan="7"></td>
                            </tr>
                            <tr>

                                <%
                                    if (userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO) || userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)) {%>	  
                                <td><table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center"><a href="useradmin.jsp"><img src="images/userpanel.gif" width="40" height="40" border="0"><br>
                                                    Administrar<br>
                                                    Usuarios</a></td>
                                        </tr>
                                    </table></td>
                                    <%}%>
                                    <%
                                        if (userp.couldUser(UserPermission.ADMINISTRAR_ROLES)) {%>	  
                                <td><table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center"><a href="roleadmin.jsp"><img src="images/rolepanel.gif" width="40" height="40" border="0"><br>
                                                    Administrar<br>
                                                    Roles</a></td>
                                        </tr>
                                    </table></td>

                                 <td><table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center"><a href=" http://portete.invemar.org.co:7003/Argos/faces/referentes?user=<%=userID%>"><img src="administracion/img/ConfigProject.png" width="40" height="40" border="0"><br>
                                                    Administrar<br>
                                                    Referentes</a></td>
                                        </tr>
                                    </table></td>    
                                    
                                <td><table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center"><a href="/argos/faces/administracion/index.xhtml"><img src="administracion/img/ConfigProject.png" width="40" height="40" border="0"><br>
                                                    Administrar<br>
                                                    Proyectos</a></td>
                                        </tr>
                                    </table></td>
                                    <%}%>
                                    <%
                                        
                                        if (userp.couldUser(UserPermission.VER_MI_HISTORIAL_DE_EVENTOS_PARA_EL_PROYECTO_ACTUAL) || userp.couldUser(UserPermission.VER_HISTORIAL_DE_EVENTOS_DE_LA_ENTIDAD_PARA_EL_PROYECTO_ACTUAL)) {%>		  
                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center"><a href="logadmin.jsp"><img src="images/logpanel.gif" width="40" height="40" border="0"><br>
                                                    Historial 
                                                    de eventos </a></td>
                                        </tr>
                                    </table>    
                                </td>
                                <%}%>
                                <%
                                    if (userp.couldUser(UserPermission.INGRESAR_DATOS)) {%>		  
                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center"><a href="infoassistant.jsp"><img src="images/uploadpanel.gif" width="40" height="40" border="0"><br>
                                                    Subir/actualizar<br>
                                                    datos</a></td>
                                        </tr>
                                    </table>
                                </td>	
                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="modsigma">
                                        <tr>
                                            <td align="center"><a href="redirectSigma.jsp" target="_blank"><img src="images/uploadpanel.gif" width="40" height="40" border="0"><br>
                                                    Caracterizaci&oacute;n para el monitoreo<br/>
                                                </a></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="modsigma">
                                        <tr>
                                            <td align="center"><a href="faces/salidas/manglares/estadisticaEstructura.xhtml" target="_blank"><img src="images/uploadpanel.gif" width="40" height="40" border="0"><br>
                                                    Ver estadisticas Estructura<br>
                                                </a></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="modsigma">
                                        <tr>
                                            <td align="center"><a href="http://cinto.invemar.org.co/fisicoquimico/index.html" target="blank"><img src="images/uploadpanel.gif" width="40" height="40" border="0"><br>
                                                    Ver estadisticas FisicoQuímicos<br>
                                                </a></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="modsigma">
                                        <tr>
                                            <td align="center"><a href="faces/salidas/manglares/presiones.xhtml" target="blank"><img src="images/uploadpanel.gif" width="40" height="40" border="0"><br>
                                                    Reportar presiones<br>
                                                </a></td>
                                        </tr>
                                    </table>
                                </td>	



                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="modsigma">
                                        <tr>
                                            <td align="center"><a href="http://cinto.invemar.org.co/share/page/site-index" target="_blank"><img src="images/uploadpanel.gif" width="40" height="40" border="0"><br>
                                                    Gestor documental<br>
                                                </a></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="modsigma" >                                    
                                        <tr>
                                            <td align="center"><a href="http://portete.invemar.org.co:7003/sigma/faces/init?user=<%=userID%>&rol=<%=idRol%>" target="_blank"><img src="images/uploadpanel.gif" width="40" height="40" border="0"><br>
                                                    Módulo de Gestión<br>
                                                </a></td>
                                        </tr>
                                    </table>
                                </td>

                                <%}%>
                                <%
                                    if (userp.couldUser(UserPermission.VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) || userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD) || userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_TODAS_LAS_ENTIDADES_PARTICIPANTES)) {%>		

                                <td><table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center"><a href="infodownload.jsp"><img src="images/downloadpanel.gif" width="40" height="40" border="0"><br>
                                                    Descargar<br>
                                                    datos </a></td>
                                        </tr>                                
                                    </table>

                                </td>

                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="modsigma">
                                        <tr>
                                            <td align="center"><a href="http://gis.invemar.org.co/manglares/" target="blank"><img src="images/uploadpanel.gif" width="40" height="40" border="0"><br>
                                                    Ver Mapa<br>
                                                </a></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="modsigma">
                                        <tr>
                                            <td align="center"><a href="faces/salidas/manglares/consultaReportes.xhtml" target="blank"><img src="images/uploadpanel.gif" width="40" height="40" border="0"><br>
                                                    Consultar presiones<br>
                                                </a></td>
                                        </tr>
                                    </table>
                                </td>

                                <%}%>

                                <%
                                    if (userp.couldUser(UserPermission.INGRESAR_DATOS_EN_BRUTO)) {%>	
                                <td><table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center"><a href="genericdbupload.jsp"><img src="images/guploadpanel.gif" width="40" height="40" border="0"><br>
                                                    Subir datos<br>
                                                    en bruto </a></td>
                                        </tr>
                                    </table></td>
                                    <%}%>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td ><p align="justify" class="texttablas"><br>
                        </p></td>
                    <td colspan="2" >&nbsp;</td>
                </tr>
                <tr>
                    <td ><p align="justify" class="texttablas"><br>
                        </p></td>
                    <td colspan="2"  class="texttablas"><strong>Funciones secundarias </strong></td>
                </tr>
                <tr>
                    <td ><p align="justify" class="texttablas"><br>
                        </p>      </td>
                    <td colspan="2" ><table id="detail" border="0" align="left" cellpadding="0" cellspacing="2" style="display:block;" >
                            <tr>
                                <td colspan="7"></td>
                            </tr>
                            <tr>
                                <td><table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center"><a href="usercpass.jsp"><img src="images/passwordpanel.gif" width="40" height="40" border="0"><br>
                                                    Cambiar<br>
                                                    Contrase&ntilde;a</a></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td ><p align="justify" class="texttablas"><br>
                        </p></td>
                    <td colspan="2" >&nbsp;</td>
                </tr>
                <tr>
                    <td ><p align="justify" class="texttablas"><br>
                        </p></td>
                    <td colspan="2"  class="texttablas"><strong>Funciones de visualizaci&oacute;n</strong></td>
                </tr>
                <tr>
                    <td ><p align="justify" class="texttablas"><br>
                        </p>      </td>
                    <td colspan="2" ><table id="detail" border="0" align="left" cellpadding="0" cellspacing="2" style="display:block;" class="texttablas">
                            <tr>
                                <td colspan="7"></td>
                            </tr>
                            <tr>
                                <td><table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center"><a href="services?proy=<%=projectid%>&action=Proyecto"><img src="images/browser2.png" alt="Ver listado de estaciones y metadato del proyecto" width="40" height="40" border="0"><br>
                                                    Estaciones <br>
                                                    y metadata</a></td>
                                        </tr>
                                    </table></td>
                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center">
                                                <%if (projectid.equals("2027")) {
                                                    if (userp.couldUser(userp.VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) || userp.couldUser(userp.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {
                                                        //				
                                                %>
                                                                                              <!--<a href="http://cinto.invemar.org.co/argoschart/?up=706&pais=<%//=pais%>" target="_blank"> -->
                                                <a href="http://cinto.invemar.org.co:9090/pentaho/Login" target="_blank"><img src="images/graph.png" alt="Reportes y an&aacute;lisis gr&aacute;fico" width="40" height="40" border="0"><br>
                                                    Pentaho<br>
                                                    (Reportes)</a>
                                                    <%                } else {%>
                                                <!--<a href="http://cinto.invemar.org.co/argoschart/" target="_blank">-->
                                                <a href="http://cinto.invemar.org.co:9090/pentaho/Login" target="_blank">				
                                                    <img src="images/graph.png" alt="Reportes y an&aacute;lisis gr&aacute;fico" width="40" height="40" border="0"><br>
                                                    Pentaho<br>
                                                    (Reportes)</a>

                                                <% }
                                                } else {%>                

                                                <a href="#"><img src="images/graph.png" width="40" height="40" border="0"><br>
                                                    Ver datos<br>
                                                    por estaci&oacute;n</a>
                                                <%}%>                </td>
                                        </tr>
                                    </table>
                                </td>
                                <%if (projectid.equals("2027")) {%>

                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center">


                                                <%
                                                    //verificación de permisos de acceso a datos 
                                                    if (userp.couldUser(userp.VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) || userp.couldUser(userp.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {

                                                        //				if (pais.equals("JM")){pais = "CO";}
%>
                                                <a href="geovisor_unep.jsp?pais=<%=pais%>&up=706" target="_blank"><img src="images/globe.png" alt="Acceso a los mapas" width="40" height="40" border="0"><br>
                                                    Geovisor<br>(SIG/GIS)</a>
                                                    <%} else {%>               
                                                    <%
                                                        //				if (pais.equals("JM")){pais="CO";}
%>                    
                                                <a href="geovisor_unep.jsp?pais=<%=pais%>&up=" target="_blank"><img src="images/globe.png" alt="Acceso a los mapas" width="40" height="40" border="0"><br>
                                                    Geovisor<br>(SIG/GIS)</a>
                                                    <%}%>                       
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <%}%>               
                                <%if (projectid.equals("2027")) {%>

                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center">


                                                <%
                                                    //verificación de permisos de acceso a datos 
                                                    if (userp.couldUser(userp.VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) || userp.couldUser(userp.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {

                                                        if (pais.equals("CO")) {
                                                %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/co/agua/repcar.html" target="_blank"><img src="images/group_data.png" alt="Acceso a los mapas" width="40" height="40" border="0"><br>
                                                    Ver <br>datos Agua</a>
                                                    <%                    }
                                                        if (pais.equals("CR")) {
                                                    %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/cr/agua/repcar.html" target="_blank"><img src="images/group_data.png" alt="Acceso a los mapas" width="40" height="40" border="0"><br>
                                                    Ver <br>datos Agua</a>
                                                    <%                    }
                                                        if (pais.equals("NI")) {
                                                    %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/ni/agua/repcar.html" target="_blank"><img src="images/group_data.png" alt="Acceso a los mapas" width="40" height="40" border="0"><br>
                                                    Ver <br>datos Agua</a>
                                                    <%                    }
                                                    } else {%>               
                                                    <%//				if (pais.equals("JM")){pais="CO";}
                                                    %>                    
                                                <a href="http://cinto.invemar.org.co/argos/visor/total/agua/repcar.html" target="_blank"><img src="images/group_data.png" alt="Acceso a los datos de AGUA" width="40" height="40" border="0"><br>
                                                    Ver total<br>datos Agua</a>
                                                    <%}%>                       
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <%}%>               
                                <%if (projectid.equals("2027")) {%>

                                <td>
                                    <table width="100" border="0" cellpadding="5" cellspacing="0" class="panelTable" id="adminuser">
                                        <tr>
                                            <td align="center">


                                                <%
                                                    //verificación de permisos de acceso a datos 
                                                    if (userp.couldUser(userp.VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) || userp.couldUser(userp.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD)) {

                                                        if (pais.equals("CO")) {
                                                %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/co/sedimento/repcar.html" target="_blank"><img src="images/group_data.png" alt="Acceso a los mapas" width="40" height="40" border="0"><br>
                                                    Ver datos<br>sedimento</a>
                                                    <%                    }
                                                        if (pais.equals("CR")) {
                                                    %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/cr/sedimento/repcar.html" target="_blank"><img src="images/group_data.png" alt="Acceso a los mapas" width="40" height="40" border="0"><br>
                                                    Ver datos<br>sedimento</a>
                                                    <%                    }
                                                        if (pais.equals("NI")) {
                                                    %>
                                                <a href="http://cinto.invemar.org.co/argos/visor/ni/sedimento/repcar.html" target="_blank"><img src="images/group_data.png" alt="Acceso a los mapas" width="40" height="40" border="0"><br>
                                                    Ver datos<br>sedimento</a>
                                                    <%                    }
                                                    } else {%>               
                                                    <%//				if (pais.equals("JM")){pais="CO";}
                                                    %>                    
                                                <a href="http://cinto.invemar.org.co/argos/visor/total/sedimento/repcar.html" target="_blank"><img src="images/group_data.png" alt="Acceso a los datos de AGUA" width="40" height="40" border="0"><br>
                                                    Ver datos<br>sedimento</a>
                                                    <%}%>                       
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <%}%>               

                            </tr>
                        </table></td>
                </tr>

                <tr>
                    <td colspan="3" ><p align="justify">&nbsp;</p></td>
                </tr>
            </tbody>
        </table>

        <%@ include file="footer.jsp" %>

        <%
            }catch (Exception e) {
        %>
        <script type="text/javascript">location.href = "login.jsp"</script>
        <%                     }
        %>
    </body>
</html>