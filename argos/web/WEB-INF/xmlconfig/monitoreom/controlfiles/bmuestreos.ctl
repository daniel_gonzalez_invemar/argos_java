LOAD DATA
APPEND INTO TABLE monitoreom.bmuestreos
WHEN (ID_MUESTREO != '0' )
FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
	ID_MUESTREO INTEGER EXTERNAL, 
	ID_ESTACION CHAR, 
	FECHA_HORA "TO_DATE(:FECHA_HORA, 'mm/dd/yyyy HH24:MI:SS')", 
	ID_PROYECTO CHAR, 
	PROFUNDIDAD INTEGER EXTERNAL, 
	SUSTRATO CHAR, 
	TEMPORADA CHAR, 
	ID_ENTIDAD CHAR, 
	CLASE_SUSTRATO CHAR, 
	OBSERVACIONES CHAR, 
	ID_METODOLOGIA INTEGER EXTERNAL, 
	ID_COMPONENTE INTEGER EXTERNAL, 
	PROF_MIN INTEGER EXTERNAL, 
	PROF_MAX INTEGER EXTERNAL, 
	UPLOADFLAG CHAR
)