LOAD DATA
APPEND INTO TABLE monitoreom.bresponsables
WHEN (MUESTREO  != '0' )
FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
	MUESTREO INTEGER EXTERNAL, 
	TAREA INTEGER EXTERNAL, 
	ORDEN INTEGER EXTERNAL, 
	CODIGO_FUNCIONARIO INTEGER EXTERNAL, 
	FECHA "TO_DATE(:FECHA, 'mm/dd/yyyy HH24:MI:SS')",  
	NOTAS CHAR,
	UPLOADFLAG CHAR

)