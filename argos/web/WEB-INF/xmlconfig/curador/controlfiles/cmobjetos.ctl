LOAD DATA
APPEND INTO TABLE curador.CMOBJETOS
WHEN (nroinvemar_ob != '0' )
FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
  nroinvemar_ob   INTEGER EXTERNAL,
  OBJETO_OB       "loaderexcel.vcodigoslov(5,:OBJETO_OB)",
  unidades_ob     INTEGER EXTERNAL,
  notas_ob        CHAR,
  ubicacion_ob    CHAR,
  consecutivo_ob  INTEGER EXTERNAL,
  uploadflag CHAR
)