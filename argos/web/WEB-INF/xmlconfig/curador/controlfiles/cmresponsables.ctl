LOAD DATA
APPEND INTO TABLE curador.CMRESPONSABLES
WHEN (nroinvemar_re != '0' ) AND (codigo_co != '0' )
FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
  nroinvemar_re        INTEGER EXTERNAL,
  tarea_re             INTEGER EXTERNAL,
  orden_re             INTEGER EXTERNAL,
  codigo_co            INTEGER EXTERNAL,
  consecutivo_re       INTEGER EXTERNAL,
  fecha_re             "loaderexcel.vfecha(:fecha_re)",
  id_responsables      INTEGER EXTERNAL,
  clave_re             CHAR,
  seccion_re           "loaderexcel.vcodigoslov(29,:seccion_re)",
  uploadflag CHAR
)