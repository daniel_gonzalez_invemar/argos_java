LOAD DATA
APPEND INTO TABLE curador.cmnomenclaturas
WHEN (nroinvemar_no != '0' ) 
FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS

(
nroinvemar_no INTEGER EXTERNAL,
id_sinonimia_no INTEGER EXTERNAL,
fecha_no "loaderexcel.vfecha(:fecha_no)",
notas_no CHAR,
actual_no INTEGER EXTERNAL,
tecnica_no INTEGER EXTERNAL,
consecutivo_no INTEGER EXTERNAL,
secuenciabbg_no INTEGER EXTERNAL,
uploadflag CHAR
)