LOAD DATA
APPEND INTO TABLE curador.cmuestra
WHEN (nroinvemar_mu != '0' ) 
FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"'

TRAILING NULLCOLS

(
	nroinvemar_mu INTEGER EXTERNAL,
	seccion_mu "loaderexcel.vseccion_mu(:seccion_mu)",
	nrocatalogo INTEGER EXTERNAL,
	nroacceso_mu INTEGER EXTERNAL,
	ubica_mu CHAR,
	fecha_mu "loaderexcel.vfecha(:fecha_mu)",
	modalidad_mu "loaderexcel.vcodigoslov(24,:modalidad_mu)",
	instituto_mu CHAR,
	phylum_mu INTEGER EXTERNAL,
	clave_mu CHAR,
	nroespecimenes_mu INTEGER EXTERNAL,
	origen_mu CHAR,
	codigo_coleccion_origen_mu CHAR,
	publicado_mu CHAR,
	tipoespe_mu "loaderexcel.vcodigoslov(4,:tipoespe_mu)",
	notas_mu CHAR,
	intercambio_mu CHAR,
	preservativo_mu "loaderexcel.vcodigoslov(6,:preservativo_mu)",
	concentracion_mu DECIMAL EXTERNAL,
	materialtipo_mu "loaderexcel.vcodigoslov(3,:materialtipo_mu)",
	fechacap "loaderexcel.vfecha(:fechacap)",
	metodocap "loaderexcel.vcodigoslov(1,:metodocap)",
	estado_capt "loaderexcel.vcodigoslov(25,:estado_capt)",
	prof_capt_min DECIMAL EXTERNAL,
	prof_capt_max DECIMAL EXTERNAL,
	notas_capt CHAR,
	fecha_id "loaderexcel.vfecha(:fecha_id)",
	tecnica_id "loaderexcel.vcodigoslov(2,:tecnica_id)",
	notas_id CHAR,
	secuencia_es INTEGER EXTERNAL,
	notas_es CHAR,
	secuencia_cr INTEGER EXTERNAL,
	notas_cr CHAR,
	prof_capt DECIMAL EXTERNAL,
	proyecto_id INTEGER EXTERNAL,
	consecutivo_mu INTEGER EXTERNAL,
	evidencia_mu  "loaderexcel.vcodigoslov(28,:evidencia_mu)",
	nodos_activos  INTEGER EXTERNAL,
	uploadflag CHAR
)




 