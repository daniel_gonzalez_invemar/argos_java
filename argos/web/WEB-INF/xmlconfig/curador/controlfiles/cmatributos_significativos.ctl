LOAD DATA
APPEND INTO TABLE curador.cmatributos_significativos
WHEN (nroinvemar_at != '0' )
FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(
  nroinvemar_at  INTEGER EXTERNAL,
  atributo       INTEGER EXTERNAL,
  consecutivo_at INTEGER EXTERNAL,
  uploadflag CHAR
)