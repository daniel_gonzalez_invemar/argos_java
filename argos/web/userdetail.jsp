<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true"  import="co.org.invemar.user.UserPermission" %>
<%
		String username = null;
		String projectid = null, projectname=null, projecttitle=null;
		
    	try{
        	username = session.getAttribute("username").toString();
        	projectid = session.getAttribute("projectid").toString();
        	projectname = session.getAttribute("projectname").toString();
        	projecttitle = session.getAttribute("projecttitle").toString();
        	   		
    	}catch(Exception e){
		 %>
    		<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}
    	
    	if(username==null){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}else{
			//check user permission
			UserPermission userp = (UserPermission)session.getAttribute("userpermission");
			
			if(!userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)&&!userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
					%>
						<script type="text/javascript">location.href="index.jsp"</script>
		 			<%
			}
			
			
		}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="this is my page">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="css/siamcss.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/cmar.css" type="text/css">
  <script type="text/javascript" src="js/lib/prototype.js"></script>
  <script type="text/javascript" src="js/lib/scriptaculous/scriptaculous.js?load=effects"></script>
  <script type="text/javascript" src="js/lib/controlmodal.js"></script>
  <script type="text/javascript" src="js/lib/yav/yav.js"></script>
  <script type="text/javascript" src="js/lib/yav/yav-config-es.js"></script>
  <script type="text/javascript" src="js/userdetail.js"></script>
  <script type="text/javascript">
   
	var detailrules=new Array();
	detailrules[0]='detailtext:la caracter&iacute;stica|maxlength|100';
	detailrules[1]='detailtext:la caracter&iacute;stica|minlength|1';
  </script>  
</head>

<body marginwidth="0" marginheight="0">

<%@ include file="bannerp.jsp" %>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr class="tabla_fondotitulo">
      <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
      <td width="364" class="linksnegros">Caracter&iacute;sticas de usuario</td>
      <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>
    </tr>
  </tbody>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="750" border="0" align="center" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0"
        cellspacing="0">
          <tbody>
            <tr>
              <td background="images/dotted_line.gif"><img
                src="images/dotted_line.gif" width="12" height="3"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/spacer.gif" width="3" height="3"></td>
    </tr>
  </tbody>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
  <tbody>
    <tr class="tabla_fondotitulo2">
      <td width="7" > </td>
      <td width="370" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp; <a href="index.jsp">Inicio</a> &gt; <a href="useradmin.jsp">Administraci&oacute;n de usuarios</a> &gt; <a href="userdetail.jsp" id="route">Roles de usuario</a> </td>
      <td width="371" class="texttablas"></td>
    </tr>
    <tr>
      <td colspan="4" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr>
      <td colspan="4" ><p align="justify" class="texttablas"><br>
          </p>      </td>
    </tr>
    <tr>
      <td colspan="4" ><table id="detail" width="724" border="0" align="center" cellpadding="0" cellspacing="2" style="display:block;" class="texttablas">
          <tr>
            <td colspan="3"> </td>
          </tr>
          <tr>
            <td colspan="3">Ingrese la informaci&oacute;n del usuario: <b><div id="refuseremail" style="display:inline"></div></b></td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><strong>CARACTER&Iacute;STICAS DEL USUARIO:</strong></td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <td width="163"><div align="left">Seleccione una caracter&iacute;stica:</div></td>
            <td width="9">&nbsp;</td>
            <td width="552">&nbsp;            </td>
          </tr>
          <tr>
            <td valign="top"><select name="detaillist" size="10" id="detaillist"  style="width:200;" >
            </select>            </td>
            <td>&nbsp;</td>
            <td valign="top"><form name="detailform" method="post" action="">
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="texttablas" id="desc_caractable">
                <tr>
                  <td>Caracter&iacute;stica seleccionada:</td>
                </tr>
                <tr>
                  <td><b><i>
                    <div id="seldetail">Seleccione una caracter&iacute;stica</div>
                  </i></b></td>
                </tr>
                <tr>
                  <td><br>
                    Detalle:</td>
                </tr>
                <tr>
                  <td><textarea name="detailtext" cols="50" rows="2" id="detailtext" disabled></textarea>
                  <br/><span id="errorsDiv_detailtext"/></td>
                </tr>
                <tr>
                  <td><input name="adddetail" type="button" id="adddetail" value="Agregar" disabled></td>
                </tr>
              </table>
                        </form>            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3">Caracter&iacute;sticas asociadas al usuario:</td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><table id="detailtable" width="100%" border="0" align="center" cellpadding="2" cellspacing="3" class="texttablas"><thead>
              <tr bgcolor="#FFFFFF">
                <td width="28%">Caracter&iacute;stica</td>
                <td width="52%">Detalle</td>
                <td width="10%" align="center">Actualizar</td>
                <td width="10%" align="center">Eliminar</td>
              </tr>
			  </thead>
			  <tbody id="detailtbody">
			  </tbody>
            </table>            </td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td ><p align="justify" class="texttablas"><br>
        </p>      </td>
      <td colspan="3" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4" ><p align="justify">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<%@ include file="footer.jsp" %>

</body>
</html>
