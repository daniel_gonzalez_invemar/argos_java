<%-- 
    Document   : indicadorPresiones
    Created on : 11/11/2014, 04:15:48 PM
    Author     : usrsig15
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.highcharts.com/highcharts.js"></script>    
        <script src="http://code.highcharts.com/highcharts-3d.js"></script>
        <script src="http://code.highcharts.com/modules/data.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
        <script src="../../js/lib/excellentexport.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">      
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script>
            var typeSearch = "";
            $(document).ready(
                    function () {

                        $("#exportaexcel").css("display", "none");
                        activeDepartmentAndManagmentUnit(2);
                        fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=DepartamentoUshashidi", {}, '#departamento');
                        $("select#tipobusqueda").change(function () {
                            tipobusqueda = $("select#tipobusqueda option:selected").attr("value");
                            activeDepartmentAndManagmentUnit(tipobusqueda);
                        });
                        $("select#departamento").change(
                                function () {
                                    var departamento = $("select#departamento option:selected").attr("value");
                                    cleanElement(unidadmanejo);
                                    fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=UnidadManejoByDepartment", {d: departamento}, '#unidadmanejo');
                                    $("#anio").empty();
                                    fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=findYearByDepartment", {d: departamento}, '#anio');
                                });
                        $("select#unidadmanejo").change(
                                function () {
                                    var departamento = $("select#departamento option:selected").attr("value");
                                    var managmentUnit = $("select#unidadmanejo option:selected").attr("value");
                                    $("#anio").empty();
                                    fillComboBox("/argos/argoservice?pkg=actions.sigma.&action=findYearByDepartmentAndSector", {d: departamento, s: managmentUnit}, '#anio');
                                });
                        $("#calcularindicator").click(
                                function (event) {
                                    event.preventDefault();
                                    createTable();
                                });
                    });
            function activeDepartmentAndManagmentUnit(tipobusqueda) {

                typeSearch = tipobusqueda;
                $("#anio").empty();
                fillComboBoxByDefault("#anio");
                $("#unidadmanejo").empty();
                fillComboBoxByDefault("#unidadmanejo");
                if (tipobusqueda == 2) {
                    $("#labelunidadmanejo").css("display", "none");
                    $("#unidadmanejo").css("display", "none");
                } else if (tipobusqueda == 1) {
                    $("#labelunidadmanejo").css("display", "inline");
                    $("#unidadmanejo").css("display", "inline");
                }
            }
            function cleanElement(element) {
                $(element).empty();
            }
            function addHTML(element, content) {
                $(element).add(content);
            }
            function fillComboBoxByDefault(element) {
                $(element).append("<option value='-1'>----- Seleccione -------</option>");
            }
            function fillComboBox(url, payload, element) {
                $.post(
                        url,
                        payload,
                        function (data) {


                            $(element).get(0).options.length = 0;
                            $(element).get(0).options[0] = new Option("----- Seleccione -------", "-1");
                            jQuery
                                    .each(
                                            data,
                                            function (index, item) {
                                                $(element).get(0).options[$(element).get(0).options.length] = new Option(item.name, item.id);
                                            });
                        }, "json");
            }

            function createTable() {

                $("#datatable").empty();
                $("#detailsector").empty();

                var department = $("select#departamento option:selected").attr("value");
                var anio = $("select#anio option:selected").attr("value");
                var headerRow = "";
                var dataRow = "<tbody><tr>";
                var currentYear = "";
                headerRow = "<thead><tr><th></th>";
                $.getJSON("/argos/argoservice?pkg=actions.sigma.&action=IndicatorByDepartment", {d: department, a: anio})
                        .done(function (json) {
                            dataRow = dataRow + "<th>'" + json.anio + "'</th>";
                            $.each(json.sectores, function (i, item) {
                                headerRow = headerRow + "<th><a href='#' onclick=\"graphicPieDetailSector('" + item.sector + "','" + department + "'," + anio + ")\">" + item.sector + "</a></th>";
                                dataRow = dataRow + "<td>" + item.indicator + "</td>";
                            });
                            headerRow = headerRow + "</tr></thead>";
                            dataRow = dataRow + "</tr></tbody>";
                            $("#datatable").append(headerRow);
                            $("#datatable").append(dataRow);
                            $('#container').highcharts({
                                data: {
                                    table: document.getElementById('datatable')
                                },
                                chart: {
                                    type: 'column',
                                    options3d: {
                                        enabled: true,
                                        alpha: 15,
                                        beta: 15,
                                        viewDistance: 50,
                                        depth: 30
                                    },
                                    marginTop: 20,
                                    marginRight: 40
                                },
                                title: {
                                    text: 'Indicador para el departamento '
                                },
                                yAxis: {
                                    allowDecimals: true,
                                    title: {
                                        text: 'Indicador'
                                    }
                                },
                                tooltip: {
                                    formatter: function () {
                                        return '<b>' + this.series.name + '</b><br/>' +
                                                this.point.y + ' ' + this.point.name.toLowerCase();
                                    }
                                },
                                plotOptions: {
                                    column: {
                                        depth: 40
                                    }
                                },
                            });
                        })
                        .fail(function (jqxhr, textStatus, error) {
                            var err = textStatus + ", " + error;
                            alert("Request failed. Contact administrator.")
                        });
                $("#exportaexcel").css("display", "inline");
            }


            function graphicPieDetailSector(sector, department, year) {

                $.getJSON("/argos/argoservice?pkg=actions.sigma.&action=DetailPresionBySector&d=" + department + "&a=" + year + "&s=" + sector, function (result) {

                    $.each(result, function (key, val) {
                        drawPies(result, sector);
                    });
                });
            }
            /**
             * Comment
             */
            function drawPies(mydata, namesector) {
                $('#detailsector').highcharts({
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: 'Detalle de presiones para el sector ' + namesector
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}'
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Porcentaje de la presion',
                            data: mydata
                        }]
                });
            }


        </script>
    </head>
    <body>  
        <div class="container">
            <div class="row clearfix">
                <div class="col-md-6 column">
                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Configure Par&aacute;metros</div>
                        <div class="panel-body">

                            <form role="form">
                                <div class="form-group">
                                    <label for="tipobusqueda" id="labeltipobusqueda">Tipo de b&uacute;squedas:</label>                 
                                    <select  name="tipobusqueda" id="tipobusqueda" class="form-control">
                                        <option value="-1">----- Seleccione -------</option>
                                        <option value="2" selected="">----- Por Departamento -------</option>
                                        <option value="1" style="display:none">----- Por Unida de manejo -------</option>
                                    </select>              

                                </div>
                                <div class="form-group">
                                    <label for="departamento" id="labeldepartamento">Departamentos:</label>          
                                    <select  name="departamento" id="departamento" class="form-control">
                                        <option value="-1">----- Seleccione -------</option>
                                    </select>
                                </div>
                                <div class="form-group">                      
                                    <label for="unidadmanejo" id="labelunidadmanejo">Sectores:</label>        
                                    <select  name="unidadmanejo" id="unidadmanejo" class="form-control">
                                        <option value="-1">----- Seleccione -------</option>               
                                    </select>
                                </div>

                                <div class="form-group">                        
                                    <label for="anio" id="labelanio">A&ntilde;o:</label>
                                    <select  name="anio" id="anio" class="form-control">
                                        <option value="-1">----- Seleccione -------</option>                 
                                    </select> 
                                </div>

                                <div class="form-group">
                                    <button type="button" class="btn btn-default" id="calcularindicator" >Visualizar el indicador</button>
                                </div>
                            </form>

                        </div> 
                    </div> 
                </div>
                <div class="col-md-6 column">
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto">       

                    </div>
                    <a id="exportaexcel" download="data.csv" href="#" onclick="return ExcellentExport.csv(this, 'datatable', ';')">Exporta a Excel</a>
                    <br/>
                    <table id="datatable" class="table">

                    </table>
                    <div id="detailsector" style="height: 400px">

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>