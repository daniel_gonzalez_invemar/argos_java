<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true"  import="co.org.invemar.user.UserPermission" %>
<%
		String username = null;
		UserPermission userp = null;
		String projectid = null, projectname=null, projecttitle=null;
                String CSS = "siamcss.css";
		
    	try{
        	username = session.getAttribute("username").toString();
        	projectid = session.getAttribute("projectid").toString();
        	projectname = session.getAttribute("projectname").toString();
        	projecttitle = session.getAttribute("projecttitle").toString();
			userp = (UserPermission)session.getAttribute("userpermission");
                         CSS = (String) session.getAttribute("CSSPRO");
        	   		
    	}catch(Exception e){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
	   	}
		
		 if(username==null){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}
try{
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Corredor Marino de Conservaci&oacute;n del Pac&iacute;fico Este Tropical</title>
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="this is my page">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   <link href="css/<%=CSS %>" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/cmar.css" type="text/css">
  <script type="text/javascript" src="js/lib/prototype.js"></script>
  <script type="text/javascript" src="js/lib/controlmodal.js"></script>
  <script type="text/javascript" src="js/userpanel.js"></script>
</head>

<body marginwidth="0" marginheight="0">

<%@ include file="bannerp.jsp" %>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr class="tabla_fondotitulo">
      <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
      <td width="364" class="linksnegros">Acerca de </td>
      <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0"
        cellspacing="0">
          <tbody>
            <tr>
              <td background="images/dotted_line.gif"><img
                src="images/dotted_line.gif" width="12" height="3"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/spacer.gif" width="3" height="3"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
  <tbody>
    <tr class="tabla_fondotitulo2">
      <td width="7"> </td>
      <td width="370"  class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp; <a href="index.jsp">Inicio</a> &gt; <a href="about.jsp" id="route">Acerca de </a> </td>
      <td width="371" class="texttablas"><div align="right"></div></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr>
      <td colspan="3" ><p align="justify" class="texttablas"><br>
          </p>      </td>
    </tr>
    <tr>
      <td ><p align="justify" class="texttablas"><br>
      </p></td>
      <td colspan="2"  class="texttablas"><strong>&iquest;Que significa ARGOS?</strong></td>
    </tr>
    
    <tr>
      <td ><p align="justify" class="texttablas"><br>
      </p></td>
      <td colspan="2"  class="texttablasreg">Argos Panoptes era un gigante con cien ojos. Era por tanto un guardi&aacute;n muy efectivo, pues s&oacute;lo algunos de sus ojos dorm&iacute;an en cada momento, habiendo siempre varios otros a&uacute;n despiertos. Era un fiel sirviente de Hera. Su gran servicio al pante&oacute;n ol&iacute;mpico fue matar al monstruo ct&oacute;nico con cola de serpiente Equidna cuando &eacute;sta dorm&iacute;a en su cueva (<em>Homero, La Il&iacute;ada ii.783; Hes&iacute;odo, Teogon&iacute;a, 295ff; Apolodoro, ii.i.2</em>).<br>
        <br>
      El &uacute;ltimo trabajo de Hera para Argos fue guardar de Zeus una ternera blanca. &laquo;Ata esta vaca con cuidado a un olivo en Nemea&raquo;, le encarg&oacute;. Hera sab&iacute;a que la ternera era en realidad &Iacute;o, una de las muchas ninfas con las que Zeus se estaba apareando para establecer el nuevo orden. Para liberarla, Zeus mand&oacute; a Hermes que matase a Argos. Hermes lo logr&oacute; disfraz&aacute;ndose de pastor y haciendo que todos los ojos de Argos cayesen dormidos con historias aburridas. Para conmemorar a su fiel guardi&aacute;n, Hera hizo que los cien ojos de Argos fuesen preservados para siempre en las colas de los pavos reales (<em>Ovidio I, 625</em>).</td>
    </tr>
    <tr>
      <td ><p align="justify" class="texttablas"><br>
      </p></td>
      <td colspan="2"  class="texttablas"><p><strong><br>
        &iquest;Que es ARGOS en el SIAM? </strong></p>      </td>
    </tr>
    <tr>
      <td >&nbsp;</td>
      <td colspan="2"  class="texttablas"><span class="texttablasreg">Argos hace parte de los subsistemas del SIAM que se encargan de acopiar informaci&oacute;n obtenida durante las actividades de investigaci&oacute;n ejecutadas de manera peri&oacute;dica o eventuales.<br>
        <br>
        Esta concebido como un metamodelo de bases de datos de tal manera que la herramienta puede servir como repositorio de datos en diferentes proyectos tendientes a hacer monitoreo ambiental sin importar su tem&aacute;tica o &iacute;ndole.<br>
        <br>
        Actualmente, este repositorio es utilizado por el proyecto de monitoreo de manglares de la CGSM, por las instituciones del <a href="http://www.conservation.org.co/programasdetalle.php?nivel=1&idu=37" target="_blank">Corredor marino de Conservaci&oacute;n del pac&iacute;fico oriental tropical CMAR</a> para almacenar los datos de monitoreos biol&oacute;gicos all&iacute; realizados, entre otros.<br>
      </span></td>
    </tr>
    <tr>
      <td >&nbsp;</td>
      <td colspan="2"  class="texttablas"><strong><br>
      &iquest;Qui&eacute;n desarrolla y administra ARGOS?</strong></td>
    </tr>
    <tr>
      <td >&nbsp;</td>
      <td colspan="2"  class="texttablas"><span class="texttablasreg">Argos hace parte del Sistema de informaci&oacute;n Ambiental Marina - SIAM, Coordinado y administrador por el INVEMAR. Su conceptualizaci&oacute;n y desarrollo a estado a cargo del Laboratorio de Sistemas de Informaci&oacute;n - <a href="http://www.invemar.org.co/labsi" target="_blank">LabSI</a> del Invemar..</span></td>
    </tr>
    
    <tr>
      <td colspan="3" ><p align="justify">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<%@ include file="footer.jsp" %>

<%
}catch(Exception e){
		 %>
<script type="text/javascript">location.href="login.jsp"</script>
		 <%
}
%>
</body>
</html>