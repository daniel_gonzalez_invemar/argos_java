<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true" import="co.org.invemar.user.UserPermission"%>
<%
    String username = null;
    UserPermission userp = null;
    String projectid = null, projectname = null, projecttitle = null, idEntity = null, userid = null;
    String CSS = "siamcss.css";

    try {
        username = session.getAttribute("username").toString();
        projectid = session.getAttribute("projectid").toString();
        projectname = session.getAttribute("projectname").toString();
        projecttitle = session.getAttribute("projecttitle").toString();
        idEntity = (String) session.getAttribute("entityid").toString();
        userid = (String) session.getAttribute("userid").toString();
        CSS = (String) session.getAttribute("CSSPRO");

    } catch (Exception e) {
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%
    }

    if (username == null || projectid == null) {
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%
} else {
    //check user permission
    userp = (UserPermission) session.getAttribute("userpermission");

    if (!userp.couldUser(UserPermission.VER_MI_SET_DE_DATOS_PARA_EL_PROYECTO_ACTUAL) && !userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD) && !userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_TODAS_LAS_ENTIDADES_PARTICIPANTES)) {
%>
<script type="text/javascript">location.href = "index.jsp"</script>
<%
        }

    }

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
        <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
        <meta http-equiv="description" content="this is my page">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="css/<%=CSS%>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/cmar.css" type="text/css">
        <link title="winter" href="js/lib/jscalendar/calendar-blue.css" media="all" type="text/css" rel="stylesheet"/>
        <script type="text/javascript" src="js/lib/prototype.js"></script>
        <script type="text/javascript" src="js/lib/scriptaculous/scriptaculous.js?load=effects"></script>
        <script type="text/javascript" src="js/lib/controlmodal.js"></script>            
        <script type="text/javascript" src="js/lib/jscalendar/calendar-setup.js"></script>
        <script type="text/javascript" src="js/infodownload.js"></script>
        <script>

    var typeDownload = "";

    RefreshForm();

    function genereLinkDownload() {

        idComponent = $F('schema').split(';');        

        if (typeDownload == 'entity') {
            link = "http://cinto.invemar.org.co:8081/argos/argoservice?pkg=actions.argos.&action=DownLoadData&idEntity=<%=idEntity%>&idproject=<%=projectid%>&idThematic=" + idComponent[0] + "&year=" + $F('yearList');
        } else if (typeDownload == 'user') {
            link = "http://cinto.invemar.org.co:8081/argos/argoservice?pkg=actions.argos.&action=DownLoadData&idUser=<%=userid%>&idproject=<%=projectid%>&idThematic=" + idComponent[0] + "&year=" + $F('yearList');
        } else if (typeDownload == 'allEntity') {
            link = "http://cinto.invemar.org.co:8081/argos/argoservice?pkg=actions.argos.&action=DownLoadData&idUser=<%=userid%>&idproject=<%=projectid%>&idThematic=" + idComponent[0] + "&idproyect=<%=projectid%>&year=" + $F('yearList');
        }

        $("downloadsection").update("Generando el archivo.Esta operacion puede demorar, por favor, espere...<img src='images/ajax-loaderArrow.gif' />");
        
        new Ajax.Request(link,
                {
                    asynchronous: true,
                    method: 'post',
                    onSuccess: function (transport) {
                        var response = transport.responseText;
                        var json = JSON.parse(response);
                        $("downloadsection").update("<br/><br/><a target='blank' href='" + json.pathCSV + "' ><img src='images/CSV_1.png' width='2%'/> Descarga data en CSV.</a><br><a target='blank' href='" + json.pathEXCEL + "' ><img src='images/excelXLSX.png' width='2%'/> Descarga data en EXCEL.</a>");
                        
                    },
                    onFailure: function () {
                        alert('Un error ha ocurrido consulte el administrador.');
                    },
                    onLoading: function () {
                        $("downloadsection").update("Generando el archivo.Esta operacion puede demorar, por favor, espere...<img src='images/ajax-loaderArrow.gif' />");
                    },
                       
                });
    }


    /**
     * checkedToFalseRadioButton
     */
    function checkedToFalseRadioButton(idRadioButton) {
        var radioButton = document.getElementById(idRadioButton);
        if (typeof (radioButton) !== 'undefined' && radioButton !== null)
        {
            radioButton.checked = false;
            
        }
    }
    function RefreshForm() {

      
        
        document.getElementById("downloadbutton").disabled = true;
        $('downloadsection').hide();

        var yearList = document.getElementById("yearList");
        if (yearList !== 'undefined' || yearList !== null) {
            $('yearList').empty();
            $('yearList').setStyle({display: 'none'});
        }



       




        checkedToFalseRadioButton('dtype1');
        checkedToFalseRadioButton('dtype2');
        checkedToFalseRadioButton('dtype3');
    }

    function fillComboYearByThematic(typeSearch) {


        $('downloadsection').hide();
        document.getElementById("downloadbutton").disabled = true;
        
        var pathServlet = "";
        typeDownload = typeSearch;
        idComponent = $F('schema').split(';');
        if (typeSearch === 'entity') {
            pathServlet = "/argos/argoservice?pkg=actions.argos.&action=GetYearMonitoreomThematic&idEntity=<%=idEntity%>&idThematic=" + idComponent[0];
        } else if (typeSearch == 'user') {
            pathServlet = "/argos/argoservice?pkg=actions.argos.&action=GetYearMonitoreomThematic&idUser=<%=userid%>&idThematic=" + idComponent[0];
        } else if (typeSearch == 'allEntity') {
            pathServlet = "/argos/argoservice?pkg=actions.argos.&action=GetYearMonitoreomThematic&idUser=<%=userid%>&idproject=<%=projectid%>&idThematic=" + idComponent[0];
        }



        new Ajax.Request(pathServlet,
                {
                    method: 'post',
                    asynchronous: true,
                    onSuccess: function (transport) {
                        var response = transport.responseText;
                        var data = transport.responseText.evalJSON();
                        Options = "";
                        $('yearList').empty();
                        $('yearList').setStyle({display: 'none'});
                        $('downloadbutton').enable();
                        for (var i = 0; i < data.length; i++) {
                            Options = Options + "<option value='" + data[i].year + "'>" + data[i].year + "</option>";
                        }
                        //$('yearlabel').setStyle({display: 'none'});
                        $('yearList').update(Options);
                        $('yearList').setStyle({display: 'inline'});
                        $('ajaximage').setStyle({display: 'none'});
                    },
                    onFailure: function (response) {                        
                        alert('Un error ha ocurrido. consulte con el administrador.');
                    },
                    onLoading: function () {
                        //$('yearlabel').html("Buscando a�os con datos");
                        $('yearlabel').setStyle({display: 'inline'});
                        $('ajaximage').setStyle({display: 'inline'});
                    }

                });
    }
        </script>
    </head>

    <body marginwidth="0" marginheight="0">

        <%@ include file="bannerp.jsp" %>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr class="tabla_fondotitulo">
                    <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
                    <td width="364" class="linksnegros">Descargar muestras</td>
                    <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>

                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
            <tbody>
                <tr>
                    <td>
                        <table width="100%" border="0" align="center" cellpadding="0"
                               cellspacing="0">
                            <tbody>
                                <tr>
                                    <td background="images/dotted_line.gif"><img
                                            src="images/dotted_line.gif" width="12" height="3"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><img src="images/spacer.gif" width="3" height="3"></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
            <tbody>
                <tr class="tabla_fondotitulo2">
                    <td width="9" >&nbsp;</td>
                    <td width="503" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="index.jsp">Inicio</a> &gt; <a href="infodownload.jsp">Descargar
                            datos</a> </td>
                    <td width="238" class="texttablas"></td>
                </tr>
                <tr>
                    <td colspan="3" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr>
                    <td colspan="3" ><p align="justify" class="texttablas"><br>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" ><table width="100%" border="0" cellspacing="0" cellpadding="0" class="texttablas">
                            <tr>
                                <td width="60%" valign="top">
                                    <table id="uploadoptions" width="97%" border="0" align="center" cellpadding="0" cellspacing="0" class="texttablas">
                                        <tr>
                                            <td><div align="justify">
                                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="texttablas" id="uploadoptions">
                                                        <tr>
                                                            <td width="100%"><div align="justify">
                                                                    <p>Seleccione la tem&aacute;tica
                                                                        de la cu&aacute;l desea descargar las muestras:<br>
                                                                        <br>
                                                                        <select name="schema" id="schema" onchange="RefreshForm()">
                                                                            <option value="-1" selected>Seleccione tem&aacute;tica</option>
                                                                        </select>
                                                                        <br>
                                                                        <br>
                                                                    </p>
                                                                </div>                          </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Seleccione el tipo de descarga </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>
                                                                    <blockquote>
                                                                        <p>
                                                                            <input name="dtype" id="dtype3" type="radio"  onclick="fillComboYearByThematic('user')"  value="3"  >
                                                                            Descargar mis datos ingresados.

                                                                            <%if (userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_MI_ENTIDAD) || userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_TODAS_LAS_ENTIDADES_PARTICIPANTES)) {	%>				
                                                                            <br>				
                                                                            <input name="dtype" id="dtype2" type="radio" value="2" onclick="fillComboYearByThematic('entity')" >
                                                                            Descargar los datos ingresados por mi entidad.
                                                                            <%}%>
                                                                            <%if (userp.couldUser(UserPermission.VER_SET_DE_DATOS_DEL_PROYECTO_ACTUAL_INGRESADOS_POR_TODAS_LAS_ENTIDADES_PARTICIPANTES)) {	%>								
                                                                            <br>
                                                                            <input name="dtype" id="dtype1" type="radio" value="1" onclick="fillComboYearByThematic('allEntity')" >
                                                                            Descargar los datos ingresados por todas las entidades.
                                                                            <%}%>                                
                                                                        </p>
                                                                        <div>
                                                                            <span id="yearlabel" style="display:none"></span>
                                                                            <img src="images/ajax-loaderArrow.gif" id="ajaximage" style="display:none"/> 
                                                                            <select id="yearList"  name="yearList" style="display:none">

                                                                            </select>
                                                                        </div>
                                                                    </blockquote>
                                                                </label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="button" name="downloadbutton" id="downloadbutton" value="Generar archivos de datos" onclick="genereLinkDownload()"  disabled >                          </td>
                                                        </tr>
                                                        <tr><td><div id="messageDownLoadDocument"></div></td></tr>
                                                        <tr id="downloadsection" >
                                                        <a href="" id="linkdownloaddata" style="display:none">Descarga data.</a>

                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" ><p align="justify">&nbsp;</p></td>
                </tr>
            </tbody>
        </table>

        <%@ include file="footer.jsp" %>
    </body>
</html>
