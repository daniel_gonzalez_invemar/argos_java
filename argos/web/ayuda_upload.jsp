<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true" %>
<%
		String username = null;
		String thematicId  = null;
		String projectid = null, projectname=null, projecttitle=null;
		
    	try{
        	username = session.getAttribute("username").toString();
        	projectid = session.getAttribute("projectid").toString();
        	projectname = session.getAttribute("projectname").toString();
        	projecttitle = session.getAttribute("projecttitle").toString();
        	   		
    	}catch(Exception e){
		 %>
    		<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}
    	
    	if(username==null){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>.::ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental::.</title>
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="this is my page">
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <link href="css/siamcss.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body marginwidth="0" marginheight="0">

<%@ include file="bannerp.jsp" %>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#8DC0E3">
      <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
      <td width="729" class="linksnegros"> Intrucciones para el Ingreso de informaci&oacute;n al sistema</td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0"
        cellspacing="0">
          <tbody>
            <tr>
              <td background="images/dotted_line.gif"><img
                src="images/dotted_line.gif" width="12" height="3"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/spacer.gif" width="3" height="3"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td width="10" bgcolor="#B5D7DE">�</td>
      <td width="733" bgcolor="#B5D7DE" class="texttablas"></td>
      <td width="10" bgcolor="#B5D7DE">�</td>
    </tr>
    <tr>
      <td height="8" colspan="3"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#E6E6E6"><p align="justify" class="texttablasreg">
	  El procedimiento de acopio de datos provenientes de actividades de monitoreo
	      ambiental descrito aqu&iacute; aplica a todos los  proyectos de investigaci&oacute;n
	      registrados en el sistema incluyendo el ingreso de registros biol&oacute;gicos
	      al <strong>Sistema de Informaci&oacute;n sobre Biodiversidad Marina (SIBM)</strong>.
        <p align="justify" class="texttablasreg">Para conveniencia de los usuarios,
          el procedimento se soporta en el uso de plantillas dise�adas
	      en archivos con formato Excel, estas  contienen listas
          de datos que es necesario definir
	      previamente
	      y una
	      hoja de
	      trabajo en la que se ingresan
	  por filas los resultados de las observaciones. 
	  
        <p align="justify" class="texttablasreg">Las plantillas Excel  contienen
           macro instrucciones programadas que para su correcto funcionamiento
          requieren:
        <ul class="texttablasreg">
<li> Estar ubicadas en un directorio denominado <strong>c:\ingresoregistros</strong> </li>
<li> Que <strong>no</strong> se modifiquen los  nombres de los archivos Excel de trabajo y referentes</li>
<li> Tener activa la  opci�n ejecutar macros de Excel</li>
<li> Mantener la distribuci&oacute;n de la plantilla, por tanto no se deben eliminar,
  insertar o copiar filas o columnas en la hoja de trabajo</li>
</ul>
<p align="justify" class="texttablasreg">Las plantillas y los procedimentos programados
  son sujeto de permanentes actualizaciones que buscan optimizarlas y/o agregarles
  servicios por tanto<strong> siempre</strong> que se inicien tareas encaminadas a ingresar al
  sistema un nuevo conjunto de datos <strong>descargue nuevamente </strong>la plantilla de trabajo.</p>
<p align="justify" class="texttablasreg"> En todos los casos, de ser necesario
  apoyese en el Administrador del Sistema, para
    resolver
    dudas
    y/o
    avanzar en
    las actividades.</p>
<p class="alertHd" > Descripci�n del procedimiento de carga de datos al sistema </p>
  </td>
    </tr>
    <tr>
      <td height="1718" bgcolor="#E6E6E6"></td>
      <td bgcolor="#E6E6E6">      
	  <table height="1651" border="0" cellpadding="2" cellspacing="0" id="content2">
        <tbody>
          <tr>
            <td width="48" height="87" class="row"><h1>1.</h1>
            </td>
            <td class="row" width="637"><p class="texttablasreg">Los proyectos
                de investigaci&oacute;n ambiental miden variables provenientes de diferentes
                componentes, cada uno de ellos debe estar identificado en el
                sistema y tener definida previamente una plantilla para la recolecci&oacute;n
                de los  datos</p>
              </td>
            <td class="row" width="44"> </td>
          </tr>
          <tr>
            <td height="300" bgcolor="#CCCCCC" class="row"><h1>2.</h1>
            </td>
            <td bgcolor="#CCCCCC" class="row"><p class="texttablasreg">Prepare los datos
                de referencia.</p>
                <p class="texttablasreg">Antes de ingresar las observaciones
                  ambientales al sistema es necesario que defina listados de
                  c�digos que le serviran para agilizar, depurar y estandarizar
                  la tarea de digitalizar. Ejemplos de esos conjuntos de datos
                  son:</p>
                <ul class="texttablasreg">
                  <li> La informaci�n sobre el proyecto</li>
                  <li> Las estaciones o puntos de recolecci�n de las muestras</li>
                  <li> Las variables observadas, las unidades de medida y el
                    m&eacute;todo de medida</li>
                  <li>Los nombres de especies y/o grupos taxon&oacute;micos de interes
                    para el proyecto</li>
                  <li style="color: #000000"> <span
                    style="color: #FF0000"><span style="color: #000000">Los investigadores
                        que participaron en la recolecci�n y procesamiento previo
                        de los datos mediante an�lisis de campo, tax�nomicos
                        o de laboratorio</span></span></li>
                </ul>
                <p class="texttablasreg">Si tiene referentes bibliogr�ficos es
                  recomendable anexarlos en esta etapa del proceso.</p>
                <p align="justify" class="texttablasreg">Utilice la plantilla Excel de referentes
                  previos para conocer cu�les de los datos mencionados se encuentran
                  en el sistema, y tener acceso a las listas de c�digos normalizados.</p>
              </td>
            <td bgcolor="#CCCCCC" class="row"> </td>
          </tr>
              <tr>
              <td height="151" class="row"><h1>3.</h1>
              </td>
              <td class="row">
                <p align="justify" class="texttablasreg">Remitir por correo electr�nico
                  una hoja excel que contenga los datos de referentes faltantes
                  al Administrador del Sistema. Especifique claramente sus datos
                  personales y en general toda aquella informaci�n que permita
                  identificar la fuente de los datos, la justificaci�n vinculante
                  y el prop�sito espec�fico.</p>
                <p align="justify" class="texttablasreg">El Administrador le
                  notificar� cuando sus datos de referencia hayan sido incluidos
                  en el sistema o de los resultados de los procesos de validaci�n,
                  en caso de que no haya sido posible insertar con certeza los
                  datos remitidos.</p></td>
              <td class="row"> </td>
            </tr>
		<tr>
          <td bgcolor="#CCCCCC" class="row"><h1>4.</h1>
          </td>
          <td bgcolor="#CCCCCC" class="row"><p class="texttablasreg"> Descargar la hoja de Excel
              de Referentes Completos. Verifique que contiene los referentes
              que solicito incluir en la etapa anterior del proceso.</p>
            </td>
          <td bgcolor="#CCCCCC" class="row"> </td>
        </tr>
		<tr>
          <td class="row"><h1>5.</h1>
          </td>
          <td class="row"><p class="texttablasreg">Descargar en el directorio <strong>c:\ingresoregistros </strong>la hoja de Excel en la que se ingresan los datos. Al iniciar,
                pulse el comando <strong>Ctrl + D</strong>, con lo que se ejecuta la macro que
                copia en su hoja de trabajo los referentes tomados de la hoja
                de referentes previos.</p>
            </td>
          <td class="row"> </td>
        </tr>
        <tr>
          <td height="306" bgcolor="#CCCCCC" class="row"><h1>6.</h1>
          </td>
          <td bgcolor="#CCCCCC" class="row"><p class="texttablasreg">Ingresar los datos en la hoja
              de trabajo de acuerdo a las reglas establecidas previamente, esta
              actividad <strong>NO</strong> requiere ningun tipo de conexi&oacute;n
              a la web.</p>
            <p align="justify" class="texttablasreg">La plantilla incorpora un conjunto de reglas
              que sirven para verificar la calidad de los datos, en algunos casos
              las celdas con errores en la informaci&oacute;n se resaltan mediante un
              color especial. Las hojas excel que tengan celdas marcadas con
              error son rechazadas al momento de efectuarse el intento de ingresar
              los datos al sistema.</p>
            <p align="justify" class="texttablasreg"> Por ejemplo, en el caso
                particular de registros biol�gicos, en concordancia con la iniciativa
                SIB Colombia, los datos deben contener al menos un referente
                taxon&oacute;mico, un referente temporal y un referente espacial,
                tal y como se establece en el Estandar para Intercambiar Informaci�n
                sobre Biodiversidad al Nivel de Organismos (<em>Su�rez Mayorga A.
                M et al, 2005</em>). Si las observaciones corresponden a mas de una
                divisi�n o phylum taxon�mico es necesario generar libros excel
                independientes para cada uno de ellos. La plantilla solo aplica
                para el ingreso de <b>registros nuevos</b>, en consecuencia las
                tareas de modificar o eliminar registros existentes se deben
                realizar con herramientas diferentes.</p>
              <p class="texttablasreg"><strong>IMPORTANTE</strong>: NO inserte
                o elimine filas o columnas de la hoja de c�lculo. Cada celda
                esta relacionada con varias mas en otras hojas del libro, borrar
                celdas conllleva la p�rdida de estos vinculos lo que hace imposible
                mantener la unidad de cada registro representado por la fila.</p>
          </td>
          <td bgcolor="#CCCCCC" class="row">�</td>
        </tr>
        <tr>
          <td height="221" class="row"><h1>7.</h1>
          </td>
          <td class="row" ><p align="justify" class="texttablasreg">Al terminar
              de ingresar los datos digite <strong>Ctrl + K</strong>, para que se ejecute la macro
              excel que elimina los referentes y guarda los datos en formato
              texto en un nuevo archivo de nombre <strong>final.csv</strong>.
              Para organizar sus datos se recomienda que renombre su hoja Excel
              de trabajo modo que pueda identificar la fecha en la que la diligencio
              y la versi�n de la misma, por ejemplo: </p>
              <p class="texttablasreg">AAAAMMDD_SSS_Registros_XXXXXXXXXX.xls,
                en d�nde: </p>
              <p class="texttablasreg"><strong>AAAA</strong>: A�o, cuatro digitos, <br>
                  <strong>MM</strong>: Mes, dos digitos,<br>
                  <strong>DD</strong>: Dia, dos digitos,<br>
                  <strong>SSS</strong>: componente tem�tico, tres letras mayusculas. <br>
                                <strong>XXXXXXXXXX</strong>: Datos que identifican
                                el proyecto y/o el investigador al que pertenecen
                                los registros </td>
          <td class="row">�</td>
        </tr>
        <tr>
          <td height="168" bgcolor="#CCCCCC" class="row"><h1>8.</h1>
          </td>
          <td bgcolor="#CCCCCC" class="row" ><p align="justify" class="texttablasreg">Conectarse
              al Sistema de Informaci�n ARGOS, en el Asistente para Ingreso
              de Informaci�n al Sistema,. ejecute el procedimiento que carga
              el archivo al servidor. Si los datos cumplen con las condiciones
              de validaci&oacute;n establecidas son almacenados
              en la bodega de datos, </p>
            <p align="justify" class="texttablasreg">Si los datos  pasan las
              pruebas de validaci&oacute;n son almacenados en el sistema, al tiempo
              que se registra para efectos de control cu�ntos
              registros fueron ingresados, por qu�en y en qu� fecha, en caso
              contrario, <strong>todo</strong> el conjunto de datos es rechazado,
              en este caso, consulte el registro
              de actividades
              del sistema para determinar y corregir las causas posibles de los
              errores.</p></td>
          <td bgcolor="#CCCCCC" class="row">� </td>
        </tr>
        <tr>
          <td height="227" colspan="3" bgcolor="#E6E6E6"><p class="texttablasreg"><b>Excepci�n
                  de Responsabilidad </b> </p>
            <p class="texttablasreg">Al usar las herramientas
                de software desarrolladas en el contexto del Sistema de Soporte
                Multitem�tico para el Monitoreo Ambiental -ARGOS-, el usuario
                explicitamente acepta las condiciones y pol�ticas de administraci�n
                de informaci�n establecidas por INVEMAR. Especif�camente el INVEMAR
                asume unicamente la custodia de los datos. Aunque el Instituto
                implementa pr�cticas seguras para el manejo de los datos que
                recibe en custodia, y vela por la incorporaci�n de las tecnologias
                mas apropiadas para el beneficio de sus usuarios no asume responsabilidad
                alguna por los da�os que puedan surgir de la perdida total o
                parcial de los datos, o de la suspensi�n de los servicios, debidas
                a da�os, errores o fallas en la infraestructura tecnologica de
                soporte (hardware, software o redes de comunicaci�n).</p>
              <p class="texttablasreg">El INVEMAR con el pr�posito
                de realizar la mejor custodia de los datos podr� de ser necesario,
                entregar la custodia de los datos a un tercero sin que para ello
                requiera de una autorizaci�n explicita por parte del propietario.
                En este evento INVEMAR enviara una notificaci�n mediante correo
                electr�nico o cualquier otro medio al propietario registrado
                en el sistema.</p>
          </td>
        </tr>
      </table></td>
      <td bgcolor="#E6E6E6">�</td>
    </tr>
  </tbody>
</table>

<%@ include file="footer.jsp" %>

</body>
</html>
