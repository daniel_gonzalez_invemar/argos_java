<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true" import="co.org.invemar.user.UserPermission" %>
<%
		String username = null;
		String projectid = null, projectname=null, projecttitle=null;
		
    	try{
        	username = session.getAttribute("username").toString();
        	projectid = session.getAttribute("projectid").toString();
        	projectname = session.getAttribute("projectname").toString();
        	projecttitle = session.getAttribute("projecttitle").toString();
        	   		
    	}catch(Exception e){
		 %>
    		<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}
    	
    	if(username==null){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}else{
			//check user permission
			UserPermission userp = (UserPermission)session.getAttribute("userpermission");
			
			if(!userp.couldUser(UserPermission.INGRESAR_DATOS_EN_BRUTO)){
					%>
						<script type="text/javascript">location.href="index.jsp"</script>
		 			<%
			}
			
		}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="this is my page">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="css/siamcss.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/cmar.css" type="text/css">
  <script type="text/javascript" src="js/lib/prototype.js"></script>
  <script type="text/javascript" src="js/lib/scriptaculous/scriptaculous.js?load=effects"></script>
  <script type="text/javascript" src="js/lib/controlmodal.js"></script>
  <script type="text/javascript" src="js/genericdbupload.js"></script>
</head>

<body marginwidth="0" marginheight="0">

<%@ include file="bannerp.jsp" %>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr class="tabla_fondotitulo">
      <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
      <td width="364" class="linksnegros">Ingresar  informaci&oacute;n en
        la base de datos</td>
      <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td height="5"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0"
        cellspacing="0">
          <tbody>
            <tr>
              <td background="images/dotted_line.gif"><img
                src="images/dotted_line.gif" width="12" height="3"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/spacer.gif" width="3" height="3"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
  <tbody>
    <tr class="tabla_fondotitulo2">
      <td width="9" >&nbsp;</td>
      <td width="503" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="index.jsp">Inicio</a> &gt; <a href="genericdbupload.jsp">Subir datos
          en bruto</a></td>
      <td width="238" class="texttablas"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr>
      <td colspan="3" ><p align="justify" class="texttablas"><br>
          </p>
      </td>
    </tr>
    <tr>
      <td colspan="3" ><table width="100%" border="0" cellspacing="0" cellpadding="0" class="texttablas">
        <tr>
          <td width="60%" valign="top">
              <table id="uploadoptions" width="97%" border="0" align="center" cellpadding="0" cellspacing="0" class="texttablas">
                <tr>
                  <td><div align="justify">
                      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="texttablas" id="uploadoptions">
                        <tr>
                          <td><div align="justify"><strong>A. </strong>Seleccione
                              el esquema al que desea subir datos:<br>
                              <br>
                              <select name="schema" id="schema">
                              <option value="-1" selected >Seleccione esquema                              
                              </select>
                              <br>
                              <br>
                              <strong>B. </strong>Seleccione la tabla destino en la base de datos:<br>
                              <br>
							  <select name="table" id="table" disabled="true">
							    <option value="-1">Seleccione tabla</option>
                              </select>
                              <br>
                              <br>
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td><div align="justify">Usted debe generar un archivo
                              tipo csv a partir de la hoja excel que est&eacute; utilizando
                              para formatear la informaci&oacute;n que desea
                              incluir (ES NECESARIO QUE LA PRIMERA FILA CONTENGA
                              LOS
                              T&Iacute;TULOS DE LAS COLUMNAS). Una
                              vez generado dicho archivo cambie el nombre del
                              mismo
                              por <strong>dbupload.csv</strong>,
                              luego proporcione el archivo presionando el bot&oacute;n
                              examinar y finalmente presione el bot&oacute;n Subir y
                              Configurar.</div>
                          </td>
                        </tr>
                        <tr>
                          <td><br />
                              <strong>C. </strong>Buscar archivo a subir:<br>
                              <br />
                              <table width="567" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="374" valign="middle"><form action="upload" method="post" name="uploadform" id="uploadform" target="target_upload" enctype="multipart/form-data">
                                    <input name="filetouploadbox" type="file" id="filetouploadbox" size="50" disabled="true"/>
                                  </form></td>
                                  <td width="193" valign="top">
                                  <input name="uploadandconfig" type="button" id="uploadandconfig" value="Subir y configurar" disabled="true"></td>
                                </tr>
                              </table>
                              <br />
                          </td>
                        </tr>
                        <tr>
                          <td>La siguiente tabla muestra la estructura de la
                            tabla seleccionada (columnas con sus respectivos
                            tipos, tama&ntilde;os y restricciones), por favor
                            indique cu&aacute;les columnas del archivo proporcionado
                            equivalen a las columnas de la tabla, luego
                            presione el bot&oacute;n procesar para finalizar
                            el proceso.</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>
                          </td>
                        </tr>
                        <tr>
                          <td><strong>D.</strong> Configurar y preparar subida de informaci&oacute;n</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><div align="center" id="tableconfig" style="display:block"></div></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><input type="button" name="processfilebutton" id="processfilebutton" value="Procesar archivo" disabled="true">
                          </td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table>
          </td>
        </tr>
      </table>
        <table  width="97%" border="0" align="center" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF" class="texttablas" id="resultstable" style="display:none">
          <tr>
            <td>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="texttablas">
                <tr>
                  <td colspan="3"> <strong>Resultados de la operaci&oacute;n: </strong> </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <blockquote>
                      <div id="processok" align="justify" style="display:none">
                        <p><font color="#006699"><strong><br>
                  El archivo fue subido y procesado exit&oacute;samente, la informaci&oacute;n
                  que se encontraba en el archivo ha sido guardada
                  en la base de datos y est&aacute; disponible.</strong></font><br>
                  <a href="genericdbupload.jsp"><br>
                  Comenzar nueva subida de datos</a></p>
                        <table width="41%" border="0" cellspacing="0" cellpadding="0" class="texttablas">
                    <tr>
                      <td width="53%">Total de registros subidos:</td>
                      <td width="47%"><div id="totalrows"></div></td>
                    </tr>
                  </table> 
                      </div>
                    </blockquote>
                    <blockquote>
                      <div id="processerror" align="justify" style="display:none"><br/>
                          <font color="#FF0000"><strong>Se encontraron errores,
                          por favor revise la hoja en la cu&aacute;l ingres&oacute; la
                          informaci&oacute;n  y verifique que no
                          existan errores,
                          verifique que haya seleccionado apropiadamente las
                          equivalencias en la tabla que de configuraci&oacute;n.</strong></font></div>
                    </blockquote>
                  </td>
                </tr>
                <tr>
                  <td colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="texttablas" style="display:none" id="suggestiontable">

                    </table>
                  </td>
                </tr>
                <tr>
                  <td width="7%">&nbsp;</td>
                  <td width="93%" colspan="2">
                    <table width="200" border="0" cellpadding="0" cellspacing="2" class="texttablas" id="generatedfiles" style="display:none">
                      <tr>
                        <td><strong>Archivos generados:</strong></td>
                      </tr>
                    </table>
                    <table width="200" border="0" cellpadding="0" cellspacing="2" class="texttablas" id="logfileoptions" style="display:none">
                      <tr>
                        <td width="172"><img src="images/sheet.gif" width="16" height="16" align="absmiddle"><a id="linklogfile" target="_blank" href="javascript:void(0);">historial.log</a></td>
                        <td width="22"><img src="images/help.gif" width="16" height="16" align="absmiddle" id="logfilehelp"></td>
                      </tr>
                    </table>
                    <table width="200" class="texttablas" id="badfileoptions" border="0" cellspacing="2" cellpadding="0" style="display:none" >
                      <tr>
                        <td width="172"><img src="images/sheet.gif" width="16" height="16" align="absmiddle"><a id="linkbadfile" target="_blank" href="javascript:void(0);">registroserroneos.cvs</a> </td>
                        <td width="22"><img src="images/help.gif" width="16" height="16" align="absmiddle" id="badfilehelp"> </td>
                      </tr>
                    </table>
                    <table width="200" class="texttablas" id="discardfileoptions" border="0" cellspacing="2" cellpadding="0" style="display:none" >
                      <tr>
                        <td width="172"><img src="images/sheet.gif" width="16" height="16" align="absmiddle"><a id="linkdiscardfile" target="_blank" href="javascript:void(0);">registrosdescartados.cvs</a> </td>
                        <td width="22"><img src="images/help.gif" width="16" height="16" align="absmiddle" id="discardfilehelp"> </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <p align="justify">
          <!-- This iframe is used as a place for the post to load -->
          <iframe id='target_upload' name='target_upload' src='' style='display: none'></iframe>
          <!-- This is the upload status area -->
      </p></td>
    </tr>
    <tr>
      <td colspan="3" ><p align="justify">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<%@ include file="footer.jsp" %>
</body>
</html>
