<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true"  import="co.org.invemar.user.UserPermission" %>
<%
		String username = null;
		String projectid = null, projectname=null, projecttitle=null;
		String CSS="siamcss.css";
		
    	try{
        	username = session.getAttribute("username").toString();
        	projectid = session.getAttribute("projectid").toString();
        	projectname = session.getAttribute("projectname").toString();
        	projecttitle = session.getAttribute("projecttitle").toString();
			CSS=(String)session.getAttribute("CSSPRO");
        	   		
    	}catch(Exception e){
		 %>
    		<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}
    	
    	if(username==null){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}else{
			//check user permission
			UserPermission userp = (UserPermission)session.getAttribute("userpermission");
			
			if(!userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_PARA_UN_PROYECTO_ESPECIFICO)&&!userp.couldUser(UserPermission.REGISTRAR_NUEVOS_USUARIOS_INCONDICIONALMENTE)){
					%>
						<script type="text/javascript">location.href="index.jsp"</script>
		 			<%
			}
			
			
		}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="this is my page">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="css/<%=CSS %>" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/cmar.css" type="text/css">
  <script type="text/javascript" src="js/lib/prototype.js"></script>
  <script type="text/javascript" src="js/lib/scriptaculous/scriptaculous.js?load=effects"></script>
  <script type="text/javascript" src="js/lib/controlmodal.js"></script>
  <script type="text/javascript" src="js/lib/abop.js"></script>
  <script type="text/javascript" src="js/useradmin.js"></script>

</head>

<body marginwidth="0" marginheight="0">

<%@ include file="bannerp.jsp" %>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr class="tabla_fondotitulo">
      <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
      <td width="364" class="linksnegros">Administraci&oacute;n de usuarios</td>
      <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0"
        cellspacing="0">
          <tbody>
            <tr>
              <td background="images/dotted_line.gif"><img
                src="images/dotted_line.gif" width="12" height="3"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/spacer.gif" width="3" height="3"></td>
    </tr>
  </tbody>
</table>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
  <tbody>
    <tr class="tabla_fondotitulo2">
      <td width="7"> </td>
      <td width="370" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp; <a href="index.jsp">Inicio</a> &gt; <a href="useradmin.jsp">Administraci&oacute;n de usuarios</a> </td>
      <td width="371" class="texttablas"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr>
      <td colspan="3" ><p align="justify" class="texttablas"><br>
          </p>      </td>
    </tr>
    <tr>
      <td colspan="3" ><table id="detail" width="738" border="0" align="center" cellpadding="0" cellspacing="2" style="display:block;" class="texttablas">
          <tr>
            <td colspan="2"> </td>
          </tr>
          <tr>
            <td colspan="2">Lista de usuarios:</td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">Seleccione proyecto: </td>
          </tr>
          <tr>
            <td colspan="2"><select name="project" id="project">
              </select>            </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">Buscar usuario:</td>
          </tr>
          <tr>
            <td colspan="2">Criterio:
              <select name="searchoption" id="searchoption">
              <option value="1" selected>Nombre</option>
              <option value="2">Correo electr&oacute;nico</option>
            </select>&nbsp;&nbsp;Texto a buscar:
             <input name="searchtext" type="text" id="searchtext" value="deje en blanco para traer todos los usuarios" size="40" maxlength="70">
             &nbsp;
             <input name="search" type="button" id="search" value="Buscar"></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="565">&nbsp;<div id="ABOPndiv_usertbody"></div></td>
            <td width="167" align="right"><a href="userreg.jsp?action=newreg"><img src="images/adduser.gif" width="16" height="16" border="0" align="absmiddle">Agregar nuevo usuario</a> </td>
          </tr>
          <tr>
            <td colspan="2"><table id="usertable" width="100%" border="0" align="center" cellpadding="2" cellspacing="3" class="texttablas"><thead>
              <tr bgcolor="#FFFFFF">
                <td width="27%" height="18">Correo electr&oacute;nico</td>
                <td width="35%">Nombre</td>
                <td width="8%" align="center">Registro</td>
                <td width="13%" align="center">Caracter&iacute;sticas</td>
                <td width="9%" align="center">Permisos</td>
                <td width="8%" align="center">Activado</td>
              </tr>
			  </thead>
			  <tbody id="usertbody">
			  </tbody>
            </table>              </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td ><p align="justify" class="texttablas"><br>
        </p>      </td>
      <td colspan="2" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" ><p align="justify">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<%@ include file="footer.jsp" %>
</body>
</html>
