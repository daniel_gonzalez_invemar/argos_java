/****------------------------------------------------------///

                A BIT Of Paginator -ABOP- 1.0
                   Prototype based version
		     		
                A javascript paginator script
       Copyright (C) 2008, Victor Adri�n Santaf� Torres
                      codex.biktor.info
				
  License:
  
  This software is FREE software, you can redistribute it
  and/or modify it but in any case you must retain all 
  copyright messages and license intact.
  
  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR 
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
  PARTICULAR PURPOSE ARE DISCLAIMED.
  
  --------------------------------------------------------
  
  Requirements:
  
  	-+> This ABOP version needs Prototype version 1.6 to 
  		work properly, so download it :P
  			 - www.prototypejs.org 
  			 
  Documentation:
  	
  	-+> See ABOP documentation at codex.biktor.info
  
  
///-----------------------------------------------------****/

var ABOP = Class.create({
	
	/*constructor*/	
	initialize:function(tbody, pSize){
		
		this.contentTableTbody = $(tbody);
		this.contentTableTbodyId = tbody;
		this.pageSize=pSize;
		this.totalRows=0;
		
		this.totalPages=0;
		this.currentPage="ABOP_1";
		this.pagesText="P�ginas: ";
		this.previousText = "<<Anterior";
		this.nextText = "Siguiente>>";
		
		this.initABOP();
				
	},
	
	/*init ABOP*/
	initABOP:function(){
		
		var cTableNodes = this.contentTableTbody.getElementsByTagName("tr");
		
		this.hideTable();
			
		this.totalRows = cTableNodes.length;
			
		this.createPageNumbers();
			
		this.goToPage(1);
		
	},
	
	createPageNumbers:function(){
		
		this.totalPages = Math.ceil(this.totalRows/this.pageSize);
			
		var numberDiv = $('ABOPndiv_'+this.contentTableTbodyId);
		var a;
		var space;
		var number;
		
		numberDiv.appendChild(document.createTextNode(this.pagesText));
		
		//create previous
		space = document.createTextNode("  |  ");
		
		a = document.createElement("a");
		a.id ="ABOP_previous";
		a.href="javascript:void(0);";
		a.onclick=this.goToPreviousPageEvent.bindAsEventListener(this);
		
		number = document.createTextNode(this.previousText);
		
		a.appendChild(number);
		
		a.style.cssText = "display:none";
		
		numberDiv.appendChild(a);
		numberDiv.appendChild(space);
		
		for(var i=1; i<= this.totalPages; i++){
			
			space = document.createTextNode(" ");
			
			a = document.createElement("a");
			a.id ="ABOP_"+i;
			a.href="javascript:void(0);";
			a.onclick=this.goToPageEvent.bindAsEventListener(this);
			
			number = document.createTextNode(i);
			
			a.appendChild(number);
			
			numberDiv.appendChild(space);
			numberDiv.appendChild(a);			
		}
				
		//create next		
		space = document.createTextNode("  |  ");
		
		a = document.createElement("a");
		a.id ="ABOP_next";
		a.href="javascript:void(0);";
		a.onclick=this.goToNextPageEvent.bindAsEventListener(this);
		
		number = document.createTextNode(this.nextText);
		
		a.appendChild(number);
		
		numberDiv.appendChild(space);
		numberDiv.appendChild(a);		
		
	},
	
	goToPageEvent:function(evt){
		
		var pageNumber = this.getNumberIdFromEvent(evt);
		
		this.hideCurrentPageRows();	
		
		this.goToPage(pageNumber);
		
	},
	
	goToNextPageEvent:function(evt){
		
		var pageNumber = this.currentPage.split("_")[1];
		
		pageNumber = parseInt(pageNumber) + 1;
		
		if(pageNumber<=this.totalPages){
			this.hideCurrentPageRows();	
			this.goToPage(pageNumber);			
		}
		
	},
	
	goToPreviousPageEvent:function(){

		var pageNumber = this.currentPage.split("_")[1];
		
		pageNumber = parseInt(pageNumber) - 1;
		
		if(pageNumber>0){
			this.hideCurrentPageRows();	
			this.goToPage(pageNumber);			
		}
		
	},
	
	goToPage:function(pageNumber){
				
		var sRNumber;
		var eRNumber;
		
		this.startRowNumber = (pageNumber - 1) * this.pageSize;
		this.endRowNumber = pageNumber * this.pageSize;
		
		if(this.endRowNumber > this.totalRows)
			this.endRowNumber = this.totalRows;
		
		sRNumber = this.startRowNumber;
		
		eRNumber = this.endRowNumber;
		
		var tbodyRows = this.contentTableTbody.getElementsByTagName("tr");
		
		for(sRNumber; sRNumber<eRNumber; sRNumber++){
			
			tbodyRows[sRNumber].style.cssText = "display:table-row";
			
		}
		
		this.configCurrentPageNumber("ABOP_"+pageNumber);
		
		this.currentPage = "ABOP_"+pageNumber;
		
		this.configPreviousAndNext();
		
	},
	
	/*returns the page number from event*/
 	getNumberIdFromEvent:function(evt){
	
		var number;
		var numberId;
		
		if(evt.target){
			number = evt.target;
		}
			else if(evt.srcElement){
				number = evt.srcElement;
			}
		
		numberId = number.id.split("_")[1];
		
		return numberId;		
	},
	
	/*hide contents of table*/
	hideTable:function(){
		
		var tbodyRows = this.contentTableTbody.getElementsByTagName("tr");

		for(var i = 0; i< tbodyRows.length ; i++){

			tbodyRows[i].style.cssText = "display:none";
			
		}
		
	},
	
	/*hide actual page rows*/
	hideCurrentPageRows:function(){
		
		sRNumber = this.startRowNumber;
		eRNumber = this.endRowNumber;
		
		var tbodyRows = this.contentTableTbody.getElementsByTagName("tr");
		
		for(sRNumber; sRNumber<eRNumber; sRNumber++){
			
			tbodyRows[sRNumber].style.cssText = "display:none";
			
		}
		
	},
	
	/*config page numbers*/
	configCurrentPageNumber:function(clickedPage){
		
		$(clickedPage).removeAttribute("href");
		
		if(clickedPage!=this.currentPage)
			$(this.currentPage).setAttribute("href", "javascript:void(0);");
				
	},
	
	/*config previous and next links*/
	configPreviousAndNext:function(){
		
		var pageNumber = this.currentPage.split("_")[1];
		
		if(pageNumber==this.totalPages){
			$("ABOP_next").style.cssText = "display:none";
		}else if(pageNumber<this.totalPages){
			$("ABOP_next").style.cssText = "display:inline";
		}
		
		if(pageNumber==1){
			$("ABOP_previous").style.cssText = "display:none";
		}else if(pageNumber>1){
			$("ABOP_previous").style.cssText = "display:inline";
		}
		
	}
		
});