var MapSibmEstacion=new Class.create({
    initialize:function(divmap){
        this.map=new GMap2($(divmap), {
            mapTypes: [G_NORMAL_MAP, G_HYBRID_MAP, G_SATELLITE_MAP]
            });
        this.markers=[];
		
        this.queryParams=location.href.toQueryParams();
		
        this.minLatitud= null;
        this.maxLatitud= null;
        this.minLongitud= null;
        this.maxLongitud= null;

        this.redImg='http://siam.invemar.org.co/siam/sibm/images/estacionRa.png';
        this.yellowImg='http://siam.invemar.org.co/siam/sibm/images/estacionAa.png';
        this.shadowImg='http://siam.invemar.org.co/siam/sibm/images/estacionSombra.png';
	
        this._initializeMapBase();
    },
    _initializeMapBase:function(){
		
        this.map.addControl(new GLargeMapControl(), new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(10, 10)));
        this.map.addControl(new GOverviewMapControl());
        this.map.addControl(new GMapTypeControl(), new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(10, 10)));
        this.map.addControl(new GScaleControl());
        this.map.enableContinuousZoom();
        this.map.setCenter(new GLatLng(11, -75), 8);
        this.renderEstation();
    },
    getIcon:function(img){
		
        var icon = new GIcon();
		
        icon.image = img;
        icon.shadow = this.shadowImg;
        icon.iconSize = new GSize(32, 32);
        icon.shadowSize = new GSize(56, 32);
        icon.iconAnchor = new GPoint(16, 32);
        icon.infoWindowAnchor = new GPoint(16, 0);
        return icon;
    },
    renderEstation:function(){
		
        var param="action=ListaEstaciones&proyect="+this.queryParams.proyecto;
		
        new Ajax.Request('services',{
            parameters:param,
            onSuccess:this._renderEstationCallback.bind(this)
										
        });
		
    },
    _renderEstationCallback:function(request){
		
        var response=request.responseText.evalJSON(true);
        var _marker;
        var _polyline;
        var info;
        var markerOptionsRed = {
            icon:this.getIcon(this.redImg)
            };
        var markerOptionsYellow = {
            icon:this.getIcon(this.yellowImg)
            };
        var bounds = new GLatLngBounds();
        for(var i=0;i<response.length;i++){
			
            if(!response[i].latfin && !response[i].longfin){
                if(response[i].latinicio || response[i].longinicio){
                                    
                    info={
                        proyecto:this.queryParams.nombre,
                        titulo:'Estaci\xF3n',
                        nombre:'<span style="font-weight: bold">Cod Esta:</span>'+response[i].estacion+'  <span style="font-weight: bold">Lugar</span>:'+response[i].lugar,
                        latitud:response[i].latinicio,
                        longitud:response[i].longinicio
                        }
					
                    bounds.extend(new GLatLng(parseFloat(response[i].latinicio),parseFloat(response[i].longinicio)));
                    if (response[i].vigente=='Actual'){
                        _marker=this.createMarker(info,markerOptionsRed);   
                    }else{
                        _marker=this.createMarker(info,markerOptionsYellow);     
                    }
					
                    this.markers.push(_marker);
                    this.map.addOverlay(_marker);
                }
            }else{
                bounds.extend(new GLatLng(parseFloat(response[i].latinicio),parseFloat(response[i].longinicio)));
                _polyline=this.createPolyLine(response[i],markerOptionsYellow);
                this.map.addOverlay(_polyline);
            }
			
            if (this.minLatitud == null || response[i].latinicio < this.minLatitud) this.minLatitud = response[i].latinicio;
            if (this.maxLatitud == null || response[i].latinicio > this.maxLatitud) this.maxLatitud = response[i].latinicio;
            if (this.minLongitud == null || response[i].longinicio < this.minLongitud) this.minLongitud = response[i].longinicio;
            if (this.maxLongitud == null || response[i].longinicio > this.maxLongitud) this.maxLongitud = response[i].longinicio;
		
        }
		
        //this.zoomMap(parseFloat(this.minLatitud),parseFloat(this.maxLatitud),parseFloat(this.minLongitud),parseFloat(this.maxLongitud));
        this.zoomMap(bounds);
		
    },
    zoomMap:function(bounds){
        var zoomLevel = this.map.getBoundsZoomLevel(bounds);
        this.map.setCenter(bounds.getCenter());
        this.map.setZoom(zoomLevel);
  		
    },
    createMarker:function(info,markerOptions) {
		
        var marker = new GMarker(new GLatLng(info.latitud,info.longitud),markerOptions);		  
        GEvent.addListener(marker, "click", this.openInfoWin.bind(this,{
            object:marker,
            messaje:info
        }));
		
        return marker;
    },
    createPolyLine:function(data,markerOptions){
        var point1=new GLatLng(data.latinicio,data.longinicio);
        var point2=new GLatLng(data.latfin,data.longfin);
		
        var polyline = new GPolyline([
            point1,
            point2
            ], "#f2bf24", 10);
	  
        var info={
            proyecto:this.queryParams.nombre,
            titulo:'Inicio de Estaci\xF3n',
            nombre:data.lugar,
            latitud:data.latinicio,
            longitud:data.longinicio
        }
        this.map.addOverlay(this.createMarker(info,markerOptions));
	
        info={
            proyecto:this.queryParams.nombre,
            titulo:'Fin de Estaci\xF3n',
            nombre:data.lugar,
            latitud:data.latfin,
            longitud:data.longfin
        }
        this.map.addOverlay(this.createMarker(info,markerOptions));
        return polyline;
    },
    openInfoWin:function(){
		
        var options=arguments[0];
		
        /*var myHtml = '<table border="0" cellpadding="4" cellspacing="2">'+
        '  <tr> <td> <span style="font-weight: bold">'+options.messaje.titulo+' :</span></td> <td><span style="font-weight: bold">'+options.messaje.nombre+'</span></td> </tr>'+
        '  <tr> <td><span style="font-weight: bold">Latitud: </span> </td> <td>'+options.messaje.latitud+'</td> </tr>'+
        '  <tr> <td><span style="font-weight: bold">Longitud: </span></td> <td>'+options.messaje.longitud+'</td> </tr>'+
        '  <tr> <td><span style="font-weight: bold">Proyecto: </span></td> <td>'+options.messaje.proyecto+'</td> </tr>'+
        '</table> ';*/
    
        var myHtml="<div id='infomarker' style='display: table;'>"+
        "<div id='row' style='display: table-row'>"+
        "<div id='left' style='display: table-cell'>"+
        "<span style='font-weight: bold'>"+options.messaje.titulo+":</span>"+
        "<span style=''>"+options.messaje.nombre+"</span>"+
        "</div>"+       
        "</div>"+
        "<div id='row' style='display: table-row'>"+
        "<div id='left' style='display: table-cell'>"+
        "<span style='font-weight: bold'>Latitud:</span>"+
        "<span style=''>"+options.messaje.latitud+"</span>"+
        "</div>"+       
        "</div>"+
      "<div id='row' style='display: table-row'>"+
        "<div id='left' style='display: table-cell'>"+
        "<span style='font-weight: bold'>Longitud:</span>"+
        "<span style=''>"+options.messaje.longitud+"</span>"+
        "</div>"+       
        "</div>"+
       "<div id='row' style='display: table-row'>"+
        "<div id='left' style='display: table-cell'>"+
        "<span style='font-weight: bold'>Proyecto:</span>"+
        "<span style=''>"+options.messaje.proyecto+"</span>"+
        "</div>"+       
        "</div>"
        "</div>";
		
		
        //console.log('myHtml '+myHtml);
					
        options.object.openInfoWindowHtml(myHtml);
    }
	
		
});

Event.observe(window, 'load',function(event){
    new MapSibmEstacion('map');									  
});
	
function PopWindow(popPage,windowName,winWidth,winHeight)
{

    var winLeft = (screen.width -winWidth)/2;
    var winTop = (screen.height -winHeight)/2;
    newWindow = window.open(popPage,windowName,'width=' + winWidth + ',height='+ winHeight + ',top=' + winTop +',left=' + winLeft + ',resizable=yes,scrollbars=yes,status=yes');
}