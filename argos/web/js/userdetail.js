var userDetail;

var UserDetail = Class.create({
	
	initialize:function(){
		
		this.params='';
		this.loadingWindow=null;
		this.userId = null;
		this.details = 0;
		this.removeDetailId = null;
		
		//get query params * never believed it, javascript can do it! amazing! :P
		var prmeter=location.href.toQueryParams();

		$('route').href = $('route').href + "?userid="+prmeter.userid+"&email="+prmeter.email;
		$('refuseremail').innerHTML = prmeter.email;
				
		this.userId=prmeter.userid;
		
		this.checkUserEditionPermission();
				
	},

/////USER DETAILS - START

	checkUserEditionPermission:function(){
		
		this.createLoaderBox("Obteniendo informaci\xF3n de usuario...");
		
		this.params="action=checkUserEditionPermission&userid="+this.userId;

		new Ajax.Request('userDetail',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._checkUserEditionPermission.bind(this)					
			});
			
	},
	
	_checkUserEditionPermission:function(request){
		
		response = request.responseText;
		
		if(response=="OK"){
			this.setUserDetails();
		}else{
			this.removeLoaderBox();
			alert("Usted no tiene privilegios suficientes para modificar la información de éste usuario");
			location.href="index.jsp";
		}
		
	},


	setUserDetails:function(){
		
		$('detaillist').observe('change',this.setDetail.bindAsEventListener(this));

		$('adddetail').observe('click',this.saveDetail.bindAsEventListener(this));
		
		this.getUserDetailTitles();
	},
	
	getUserDetailTitles:function(){
		
		this.params="action=getUserDetailTitles&userid="+this.userId;

		new Ajax.Request('userDetail',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getUserDetailTitles.bind(this)					
			});
			
	},
	
	_getUserDetailTitles:function(request){
		
		var result = request.responseText;
		var response = request.responseText.evalJSON(true);
		
		if(result != "NO"){
			//filling list
			for(var i=0;i<=response.length-1;i++){
				
				$("detaillist").options[i]=new Option(response[i].name,response[i].id);		
				
			}
		}else{
			alert("No fue posible obtener la información de usuario");
		}
		
		this.getUserDetails();
		
	},
	
	getUserDetails:function(){
		
		this.params="action=getUserDetails&userid="+this.userId;

		new Ajax.Request('userDetail',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getUserDetails.bind(this)					
			});
	},
	
	_getUserDetails:function(request){
		
		var result = request.responseText;
		var response = request.responseText.evalJSON(true);
		
		if(result != "NO"){
			//filling list
			for(var i=0;i<=response.length-1;i++){
				
				this.createDetailRow(response[i].id, response[i].name, response[i].text);
				
			}
		}else{
			alert("No fue posible obtener la información de usuario");
		}
				
		this.removeLoaderBox();
		
	},
	
	setDetail:function(){
		
		$("seldetail").innerHTML = "";
		$("seldetail").appendChild(document.createTextNode($("detaillist").options[$("detaillist").selectedIndex].text));
		
		$("detailtext").value = "";
		$("detailtext").disabled = false;
		$("adddetail").disabled = false;
				
	},
	
	saveDetail:function(){
		
		if(!performCheck('detailform', detailrules, 'inline'))
			return;
		
		var detailId = $("detaillist").options[$("detaillist").selectedIndex].value;
		
		var detailText = $("detailtext").value;
		
		this.createLoaderBox("Guardando informaci\xF3n de usuario...");
		
		this.params="action=saveUserDetail&userid="+this.userId+"&detailid="+detailId+"&detailtext="+detailText;

		new Ajax.Request('userDetail',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._saveDetail.bind(this)					
			});
			
	},
	
	_saveDetail:function(request){
		
		var response = request.responseText;
		
		this.removeLoaderBox();
		
		if(response=="OK"){
			
			//create detail row
			var detailId = $("detaillist").options[$("detaillist").selectedIndex].value;
			var detailName = $("detaillist").options[$("detaillist").selectedIndex].text;
			
			this.createDetailRow(detailId, detailName, $("detailtext").value);
			
			//delete detail from list
			$("detaillist").options[$("detaillist").selectedIndex]=null;
			$("seldetail").innerHTML = "";
			$("seldetail").appendChild(document.createTextNode("Seleccione una característica"));
			$("detailtext").value = "";
			$("detailtext").disabled = true;
			$("adddetail").disabled = true;
			
			alert("Característica asociada satisfactoriamente");
			
		}else if(response=="NO"){
			alert("No fue posible guardar la información del usuario");
		}
		
		
		
	},
	
 	updateDetail:function(detailId){
		
		var detailText = $(detailId+"_text").value;
		
		this.createLoaderBox("Actualizando informaci\xF3n de usuario...");
		
		this.params="action=updateUserDetail&userid="+this.userId+"&detailid="+detailId+"&detailtext="+detailText;

		new Ajax.Request('userDetail',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._updateDetail.bind(this)					
			});
			
	},
	
	_updateDetail:function(request){
		
		response = request.responseText;
		
		this.removeLoaderBox();
		
		if(response=="OK"){
			alert("Información de usuario actualizada satisfactoriamente");
		}else{
			alert("No fue posible actualizar la información de usuario");
		}
		
	},
	
	 deleteDetail:function(detailId){
		
		var conf = confirm("¿Está seguro que desea eliminar ésta característica del usuario?");
		
		if(!conf)
			return;
		
		this.createLoaderBox("Actualizando informaci\xF3n de usuario...");
		
		this.removeDetailId =detailId;
		
		this.params="action=deleteUserDetail&userid="+this.userId+"&detailid="+detailId;

		new Ajax.Request('userDetail',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._deleteDetail.bind(this)					
			});
			
	},
	
	_deleteDetail:function(request){
		
		response = request.responseText;
		
		this.removeLoaderBox();
		
		if(response=="OK"){
			
			this.details = this.details-1;
			
			$("detaillist").options[$("detaillist").length]=new Option($(this.removeDetailId+"_name").innerHTML,$(this.removeDetailId));
			
			$("detailtbody").removeChild($(this.removeDetailId+"_row"));
			
			alert("Característica de usuario eliminada satisfactoriamente");
			
		}else if(response=="NO"){
			alert("No fue posible eliminar la característica del usuario");
		}
		
	},

/////USER DETAILS - END

/////DETAIL TABLE - START

	createDetailRow:function(detailId, detailName, detailText){
		
		var tr;
		var td;
		var textarea;
		var img;
		var div;
		var a;
		
		tr= document.createElement("tr");
		tr.setAttribute("height","30");
		tr.id=detailId+"_row";
				
		if((this.details+1)%2==0){
			tr.setAttribute("bgColor", "#FFFFFF");
		}
		
		div = document.createElement("div");
		div.id = detailId+"_name";
		div.appendChild(document.createTextNode(detailName));
		
		td= document.createElement("td");
		td.appendChild(div);
		tr.appendChild(td);

		//create text area
		
		textarea = document.createElement("textarea");
		textarea.id = detailId+"_text";
		textarea.cols = 40;
		textarea.rows = 2;
		textarea.value = detailText;
		
		td= document.createElement("td");
		td.appendChild(textarea);
		tr.appendChild(td);
		
		//update option
		a = document.createElement("a");
		a.href = "javascript:userDetail.updateDetail("+detailId+")";
		img = document.createElement("img");
		img.src = "images/save.gif";
		img.setAttribute("border","0");
		
		a.appendChild(img);
		
		td= document.createElement("td");
		td.setAttribute("align", "center");
		td.appendChild(a);
		tr.appendChild(td);
		
		//delete option
		a = document.createElement("a");
		a.href = "javascript:userDetail.deleteDetail("+detailId+")";
		img = document.createElement("img");
		img.src = "images/delete.gif";
		img.setAttribute("border","0");
		
		a.appendChild(img);
		
		td= document.createElement("td");
		td.setAttribute("align", "center");		
		td.appendChild(a);
		tr.appendChild(td);
		
		$("detailtbody").appendChild(tr);
		
		this.details = this.details+1;
	},

/////DETAIL TABLE - END
	
	createLoaderBox:function(lText){

		var loadingContent = document.createElement('div');
		loadingContent.id	= 'loadingContent';
	
		var loadingText=document.createElement('div');
		loadingText.id	= 'loadingText';
		
		if(lText){
			loadingText.innerHTML=lText;
		}else{
			loadingText.innerHTML="Verificando credenciales de usuario...";			
		}

		
		var loadingImg=document.createElement('IMG');
		loadingImg.src="images/ajax-loader.gif";
		
		loadingContent.appendChild(loadingText);
		loadingContent.appendChild(loadingImg);
		
		this.loadingWindow=new Control.Modal(false,{
			contents: loadingContent.innerHTML,
			overlayCloseOnClick: false
			});

		this.loadingWindow.open();

	},
	
	removeLoaderBox:function(){
		
		this.loadingWindow.close();
		
	},
	
	/*util function*/
	noCache:function(uri){
		
		return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
	}
		
});

Event.observe(window, 'load',function(event){
			userDetail = new UserDetail();									  
	});
	
