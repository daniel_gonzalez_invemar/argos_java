var userCPassword;

var UserChangePassword= Class.create({
	
	initialize:function(){
		
		this.params='';
		this.loadingWindow=null;
		
		$("save").observe('click',this.userChangePassword.bindAsEventListener(this));
				
	},

	userChangePassword:function(){
		
		if(!performCheck('passform', passrules, 'inline'))
			return;

		this.createLoaderBox("Cambiando contrase\xF1a...");
		
		this.params="newpassword="+$("password").value;

		new Ajax.Request('userChangePassword',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._userChangePassword.bind(this)					
			});
			
	},
	
	_userChangePassword:function(request){
		
		var response = request.responseText;
		
		this.removeLoaderBox();
		
		if(response=="OK"){
			
			alert("Contraseña actualizada exitósamente");
			
		}else if(response=="NO"){
			alert("No fue posible actualizar contraseña");
		}
		
		location.href="index.jsp";
		
	},
	
	
	createLoaderBox:function(lText){

		var loadingContent = document.createElement('div');
		loadingContent.id	= 'loadingContent';
	
		var loadingText=document.createElement('div');
		loadingText.id	= 'loadingText';
		
		if(lText){
			loadingText.innerHTML=lText;
		}else{
			loadingText.innerHTML="Verificando credenciales de usuario...";			
		}

		
		var loadingImg=document.createElement('IMG');
		loadingImg.src="images/ajax-loader.gif";
		
		loadingContent.appendChild(loadingText);
		loadingContent.appendChild(loadingImg);
		
		this.loadingWindow=new Control.Modal(false,{
			contents: loadingContent.innerHTML,
			overlayCloseOnClick: false
			});

		this.loadingWindow.open();

	},
	
	removeLoaderBox:function(){
		
		this.loadingWindow.close();
		
	},
	
	/*util function*/
	noCache:function(uri){
		
		return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
	}
		
});

Event.observe(window, 'load',function(event){
			userCPassword = new UserChangePassword();									  
	});
	
