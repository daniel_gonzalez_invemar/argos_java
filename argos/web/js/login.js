/*
 * 
 * @author Victor Adri�n Santaf� Torres.
 * 
 */

var login;

var Login = Class.create({
	
    initialize:function(){
		
        this.params='';
        this.loadingWindow=null;
        this.loadingWindowClose=0;
		
        //get query params * never believed it, javascript can do it! amazing! :P
        var prmeter=location.href.toQueryParams();
		
        if(prmeter.action=="cpassgen"){
			
            this.confirmPasswordRegeneration(prmeter);
			
        }else{
			
            $("logintable").style.cssText = "display:block";
			
        }
		
        //event for login button
        $('login').observe('click',this.login.bindAsEventListener(this));
        //event for prg button		
        $('prgbutton').observe('click',this.passwordRegeneration.bindAsEventListener(this));
		
        $('linkrecover').observe('click',this.showPasswordRegenerationBox.bindAsEventListener(this));
		
				
    },
	
	
    /////LOGIN - START
	
    login:function(){
		
        if(!performCheck('loginform', loginrules, 'inline')){
            return;
        }
				
        this.createLoaderBox();
		
        this.params="action=ver&email="+$('email').value+"&password="+$('password').value;

        new Ajax.Request('login',{
            method:"post",				
            parameters:this.noCache(this.params),
            onSuccess:this._login.bind(this)					
        });
    },
	
    _login:function(request){
		
        if(request.responseText=="NO"){
            $("errorresult").innerHTML="";
            this.removeLoaderBox();
            $("errorresult").appendChild(document.createTextNode("ERROR: Direcci\xF3n de correo electr\xF3nico y/o contrase\xF1a inv\xE1lidos"));
        }else{
			
            $('password').value = "";
			
            response = request.responseText.evalJSON(true);
			
            this.removeLoaderBox();
			
            this.createProjectSelectionBox();
			
            //filling list
            for(var i=0;i<=response.length-1;i++){
				
                $("userprojects").options[i]=new Option(response[i].name,response[i].id+'::'+response[i].projecttitle);		
            }
            $("errorresult").innerHTML="";
            $('selproject').observe('click',this.selectProject.bindAsEventListener(this));

        }

    },
	
    selectProject:function(){
		
        var vl = $("userprojects").options[$("userprojects").selectedIndex].value;
        var pname = $("userprojects").options[$("userprojects").selectedIndex].text;
        var pid=vl.split("::");
        
        //alert('pname:'+pname+'pid 0:'+pid[0]+'pid 1:'+pid[1]);
        
        
        this.params="action=setuserproject&username="+$('email').value+"&pid="+pid[0]+"&pname="+pname+"&projecttitle="+pid[1];

        new Ajax.Request('login',{
            method:"post",				
            parameters:this.noCache(this.params),
            onSuccess:this._selectProject.bind(this)					
        });
		
    },
	
    _selectProject:function(request){
		
        response = request.responseText;
		
        if(response=="OK"){           
            window.location.replace('temp2.jsp');
            
        }else{
            alert("No fue posible establecer el proyecto, intente nuevamente");
        }
		
    },
	
    /////LOGIN - END

    /////PASSWORD REGENERATION - START

    showPasswordRegenerationBox:function(){
		
        $("prgtable").style.cssText = "display:block";
        $("logintable").style.cssText = "display:none";
		
    },

    passwordRegeneration:function(){
		
        if(!performCheck('pregform', pregrules, 'inline')){
            return;
        }
		
        this.createLoaderBox("Verificando informaci\xF3n de usuario...");
		
        this.params="action=passregen&email="+$('prgemail').value;

        new Ajax.Request('login',{
            method:"post",				
            parameters:this.noCache(this.params),
            onSuccess:this._passwordRegeneration.bind(this)					
        });
    },
	
    _passwordRegeneration:function(request){
		
        this.removeLoaderBox();
		
        var response = request.responseText.evalJSON(true);
		
        $("errorresult").innerHTML="";
					
        if(response.result=="OK"){
            $("okresult").appendChild(document.createTextNode(response.desc));
            $("prgtable").style.cssText = "display:none";
        }else{
            $("errorresult").appendChild(document.createTextNode("ERROR: "+response.desc));
        }
		
    },
	
    verifyPasswordGeneration:function(){
		
        var result = true;
		
        $("errorresult").innerHTML="";
        if($("prgemail").value==""){
            $("errorresult").appendChild(document.createTextNode("ERROR: Debe ingresar el correo electr\xF3nico correspondiente a su nombre de usuario."));
            result = false;
        }
		
        return result;
		
    },
	
	
    /////PASSWORD REGENERATION - END

    /////PASSWORD REGENERATION CONFIRMATION - START

    confirmPasswordRegeneration:function(prmeter){
		
        $("logintable").style.cssText = "display:none";
		
        this.createLoaderBox("Verificando c\xF3digo de confirmaci\xF3n y generando nueva contrase\xF1a...");
		
        this.params="action=cpassgen&username="+prmeter.username+"&uid="+prmeter.uid+"&email="+prmeter.email+"&ckey="+prmeter.ckey;

        new Ajax.Request('login',{
            method:"post",				
            parameters:this.noCache(this.params),
            onSuccess:this._passwordRegeneration.bind(this)					
        });
		
    },

    /////PASSWORD REGENERATION CONFIRMATION - END
	
    createLoaderBox:function(lText){

        var loadingContent = document.createElement('div');
        loadingContent.id	= 'loadingContent';
	
        var loadingText=document.createElement('div');
        loadingText.id	= 'loadingText';
		
        if(lText){
            loadingText.innerHTML=lText;
        }else{
            loadingText.innerHTML="Verificando credenciales de usuario...";			
        }

		
        var loadingImg=document.createElement('IMG');
        loadingImg.src="images/ajax-loader.gif";
		
        loadingContent.appendChild(loadingText);
        loadingContent.appendChild(loadingImg);
		
        this.loadingWindowClose=0;
		
        this.loadingWindow=new Control.Modal(false,{
            contents: loadingContent.innerHTML,
            overlayCloseOnClick: false,
            beforeClose:function(){
                if (login.loadingWindowClose==0){
                    throw $break;
                }
            }
        });

        this.loadingWindow.open();

    },
	
    createProjectSelectionBox:function(lText){
		
        var loadingContent = document.createElement('div');
        loadingContent.id	= 'seluserproject';
		
        loadingContent.style.cssText = "width: 301px; height: 130px;background:#FFFFFF;";
		
        var table = document.createElement("table");
        var tbody = document.createElement("tbody");
		
        table.appendChild(tbody);
        table.setAttribute("class", "alerts");
        table.setAttribute("className", "alerts");
        loadingContent.appendChild(table);
		
        //title row
		
        var tr = document.createElement("tr");
        var td = document.createElement("th");
        tr.appendChild(td);
		
        td.align="center";
        td.appendChild(document.createElement("br"));
        td.appendChild(document.createTextNode("Seleccione el proyecto que desea trabajar:"));
        td.appendChild(document.createElement("br"));
        td.appendChild(document.createElement("br"));
		
        tbody.appendChild(tr);
		
        //select row
		
        tr = document.createElement("tr");
        td = document.createElement("td");
        tr.appendChild(td);
		
        var select = document.createElement("select");
        select.id = "userprojects";
		
        td.align="center";
        td.appendChild(select);
        td.appendChild(document.createElement("br"));
        td.appendChild(document.createElement("br"));
		
        tbody.appendChild(tr);
		
        //button row
		
        tr = document.createElement("tr");
        td = document.createElement("td");
        tr.appendChild(td);
		
        var button = document.createElement("input");
        button.type = "button"
        button.id = "selproject";
        button.name = "selproject";
        button.value = "Seleccionar"
		
        td.align="center";
        td.appendChild(button);
        td.appendChild(document.createElement("br"));
        td.appendChild(document.createElement("br"));
		
        tbody.appendChild(tr);
		
        this.loadingWindowClose=0;
		
        this.loadingWindow=new Control.Modal(false,{
            contents: loadingContent.innerHTML,
            overlayCloseOnClick: false,
            beforeClose:function(){
                if (login.loadingWindowClose==0){
                    throw $break;
                }
            }
        });
				
        this.loadingWindow.open();

    },
	
    _keypress:function(){
        alert("you pressed something");
    },
	
    removeLoaderBox:function(){
		
        this.loadingWindowClose=1;
        this.loadingWindow.close();
        this.loadingWindowClose=0;
		
    },
	
    restartAssistant:function(){
		
        location.href="infoassistant.htm";
		
    },
	
    /*util function*/
    noCache:function(uri){
		
        return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
    }
		
});

Event.observe(window, 'load',function(event){
    login = new Login();									  
});
	
