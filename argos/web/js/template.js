
var TemplatesManager = Class.create({
	
	initialize:function(){
		
		this.params='';
		this.getTemplates();
	},
	
	/*get availables templates*/
	getTemplates:function(){
		
		this.params="action=getTemplates";

		new Ajax.Request('templatesRequest',{
				method:"get",				
				parameters:this.noCache(this.params),
				onSuccess:this._getTemplates.bind(this)					
			});
		
	},
	
	_getTemplates:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		var table = document.createElement("table");
		var tbody = document.createElement("tbody");
		var thead = document.createElement("thead");
		var A;

		table.appendChild(thead);
		table.appendChild(tbody);
		
		//header
		thead.insertRow(0);
		thead.rows[0].insertCell(0);
		thead.rows[0].insertCell(1);
		thead.rows[0].cells[0].appendChild(document.createTextNode("Nombre de la plantilla"));
		thead.rows[0].cells[1].appendChild(document.createTextNode("Descripci\xF3n"));
		
		//filling table
		for(var i=0;i<response.length;i++){
			
			tbody.insertRow(i);
			tbody.rows[i].insertCell(0);
			tbody.rows[i].insertCell(1);
			
			A = document.createElement("a");
			A.href="templatesRequest?action=getTemplateDoc&id="+response[i].id;
			A.appendChild(document.createTextNode(response[i].name));
			
			tbody.rows[i].cells[0].appendChild(A);
			tbody.rows[i].cells[1].appendChild(document.createTextNode(response[i].description));
			
		}
		
		$("templates").appendChild(table);
		
	},
	
	/*util function*/
	noCache:function(uri){
		
		return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
	}
	
});

Event.observe(window, 'load',function(event){
			new TemplatesManager();									  
	});