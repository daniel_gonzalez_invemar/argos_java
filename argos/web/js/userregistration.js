var userReg;

var UserReg = Class.create({
	
	initialize:function(){
		
		this.params='';
		this.loadingWindow=null;
		this.selectEntity = null;
		this.userId = null;
		this.checkemailaction= null;
		
		//get query params * never believed it, javascript can do it! amazing! :P
		var prmeter=location.href.toQueryParams();
		
		if(prmeter.action=="newreg"){
			
			$('route').href = $('route').href + "?action=newreg";
			
			this.setNewRegistration();
			
		}else if(prmeter.action=="editgi"){
			
			$('route').href = $('route').href + "?action=editgi&userid="+prmeter.userid+"&email="+prmeter.email;
			$('refuseremail').innerHTML = prmeter.email;
			
			this.userId=prmeter.userid;
			
			this.checkUserEditionPermission();
			
		}

		
				
	},

/////NEW REGISTRATION - START

	setNewRegistration:function(){
	
		$('giemail').observe('change',this.checkEmail.bindAsEventListener(this));
		
		$('gisubmit').observe('click',this.performNewRegistration.bindAsEventListener(this));
		
		this.checkemailaction = "checkEmail";
		
		this.getEntities();
		
	},
	
	performNewRegistration:function(){
		
		var sendpassword;
		
		if(!performCheck('giform', girules, 'inline'))
			return;
		
		if($("gisendmail").checked){
			sendpassword = true;
		}else{
			sendpassword = false;
		}
		
		this.createLoaderBox("Registrando usuario...");
		
		this.params="action=regUserGenInfo&fname="+$("gifirstname").value+"&lname="+$("gilastname").value+"&entity="+$("gientity").options[$("gientity").selectedIndex].value+"&email="+$("giemail").value+"&password="+$("gipassword").value+"&sendmail="+sendpassword;

		new Ajax.Request('userRegistration',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._performNewRegistration.bind(this)					
			});
			
	},
	
	_performNewRegistration:function(request){
		
		var response = request.responseText;
		
		if(response=="OK"){
			
			$("geninfo").style.cssText = "display:none";
			
		}else if(response=="EMAILE"){
			$("emailok").style.cssText = "color:#FF0000;";
			$("emailok").appendChild(document.createTextNode($("giemail").value+" est\xE1 siendo usada, ingrese una direcci\xF3n diferente"));	
			$("giemail").value = ""
		}else{
			alert("Ocurri\xF3 un error en el servidor, por favor intente nuevamente");
		}
		
		this.removeLoaderBox();
		
		alert("Ha registrado un nuevo usuario, para permitir su acceso al sistema deber� asignar roles para el nuevo usuario.");
		location.href="useradmin.jsp";
		
	},
	

/////NEW REGISTRATION - END

/////EDIT USER GENERAL INFORMATION - START

	checkUserEditionPermission:function(){
		
		this.createLoaderBox("Obteniendo informaci\xF3n de usuario...");
		
		this.params="action=checkUserEditionPermission&userid="+this.userId;

		new Ajax.Request('userDetail',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._checkUserEditionPermission.bind(this)					
			});
			
	},
	
	_checkUserEditionPermission:function(request){
		
		response = request.responseText;
		
		if(response=="OK"){
			this.setUserGeneralInformationEdition();
		}else{
			this.removeLoaderBox();
			alert("Usted no tiene privilegios suficientes para modificar la informaci�n de �ste usuario");
			location.href="index.jsp";
		}
		
	},

	setUserGeneralInformationEdition:function(){
		
		$('giemail').observe('change',this.checkEmail.bindAsEventListener(this));
		
		$('gisubmit').observe('click',this.performUserGIUpdate.bindAsEventListener(this));
		
		this.checkemailaction = "checkEmailToUpdate&userid="+this.userId;
		
		this.getUserGeneralInformation();
		
	},
	
	getUserGeneralInformation:function(){
				
		this.params="action=getUserGenInfo&userid="+this.userId;

		new Ajax.Request('userRegistration',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getUserGeneralInformation.bind(this)					
			});
			
	},
	
	_getUserGeneralInformation:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		$("gifirstname").value = response.fname;
		$("gilastname").value = response.lname;
		$("giemail").value = response.email;
		$("gipassword").value = "current77password";
		$("gicpassword").value = "current77password";
		this.selectEntity = response.entity;
		this.removeLoaderBox();
		this.getEntities();
	},
	
	performUserGIUpdate:function(){
		
		var sendpassword;
		var password = "null";
		
		if(!performCheck('giform', girules, 'inline'))
			return;
		
		if($("gisendmail").checked){
			sendpassword = true;
		}else{
			sendpassword = false;
		}
		
		if($("gipassword").value!="current77password"){
			password = $("gipassword").value;
		}
		
		this.createLoaderBox("Actualizando informaci\xF3n usuario...");
		
		this.params="action=updateUserGenInfo&userid="+this.userId+"&fname="+$("gifirstname").value+"&lname="+$("gilastname").value+"&entity="+$("gientity").options[$("gientity").selectedIndex].value+"&email="+$("giemail").value+"&password="+password+"&sendmail="+sendpassword;

		new Ajax.Request('userRegistration',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._performUserGIUpdate.bind(this)					
			});
			
	},
	
	_performUserGIUpdate:function(request){
		
		var response = request.responseText;
		
		this.removeLoaderBox();
		
		if(response=="OK"){
			alert("La informaci�n del usuario fue actualizada exit�samente");
		}else{
			alert("No fue posible actualizar la informaci�n del usuario");
		}
		
	},

	getEntities:function(){

		this.createLoaderBox("Obteniendo entidades...");
		
		this.params="action=getEntities";

		new Ajax.Request('userRegistration',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getEntities.bind(this)					
			});
		
	},
	
	_getEntities:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		//filling list
		for(var i=0;i<=response.length-1;i++){
			
			$("gientity").options[i]=new Option(response[i].name,response[i].id);		
			
			if(this.selectEntity!=null){
				
				if(response[i].id==this.selectEntity){
					
					$("gientity").selectedIndex = $("gientity").length-1;
				}
			}
		}
		
		this.removeLoaderBox();
		
	},
	
	checkEmail:function(){
		
		 var expresionRegular =  /^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/;  
		 if(!expresionRegular.test($("giemail").value))
		 {
		 	$("emailok").innerHTML="";
			$("emailok").style.cssText = "color:#FF0000;";
			$("emailok").appendChild(document.createTextNode($("giemail").value+" no es una direcci\xF3n de correo v\xE1lida"));	
			$("giemail").value = "";
			
		 }else{
		 	
			this.createLoaderBox("Verificando direcci\xF3n de correo electr\xF3nico...");
		 	
			this.params="action="+this.checkemailaction+"&email="+$("giemail").value;

			new Ajax.Request('userRegistration',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._checkEmail.bind(this)					
			});		 	
		 }

	},

	_checkEmail:function(request){
		
		var response = request.responseText;
		
		$("emailok").innerHTML="";
		
		if(response=="OK"){
			$("emailok").style.cssText = "color:#009900;";
			$("emailok").appendChild(document.createTextNode("La direcci\xF3n de correo electr\xF3nico ingresada est\xE1 disponible"));
		}else{
			$("emailok").style.cssText = "color:#FF0000;";
			$("emailok").appendChild(document.createTextNode($("giemail").value+" est\xE1 siendo usada, ingrese una direcci\xF3n diferente"));	
			$("giemail").value = "";
		}
		this.removeLoaderBox();
	},

/////EDIT USER GENERAL INFORMATION - END

	
	createLoaderBox:function(lText){

		var loadingContent = document.createElement('div');
		loadingContent.id	= 'loadingContent';
	
		var loadingText=document.createElement('div');
		loadingText.id	= 'loadingText';
		
		if(lText){
			loadingText.innerHTML=lText;
		}else{
			loadingText.innerHTML="Verificando credenciales de usuario...";			
		}

		
		var loadingImg=document.createElement('IMG');
		loadingImg.src="images/ajax-loader.gif";
		
		loadingContent.appendChild(loadingText);
		loadingContent.appendChild(loadingImg);
		
		this.loadingWindow=new Control.Modal(false,{
			contents: loadingContent.innerHTML,
			overlayCloseOnClick: false
			});

		this.loadingWindow.open();

	},
	
	removeLoaderBox:function(){
		
		this.loadingWindow.close();
		
	},
	
	/*util function*/
	noCache:function(uri){
		
		return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
	}
		
});

Event.observe(window, 'load',function(event){
			userReg = new UserReg();									  
	});
	
