var infoDownload;

var InformationDownload = Class.create({
    initialize: function () {

        this.params = '';
        this.schema = null;
        this.thematicId = null;

        this.getThematics();

        $('schema').observe('change', this.thematicListChange.bindAsEventListener(this));
        $('downloadbutton').observe('click', this.download.bindAsEventListener(this));

    },
/////THEMATIC SETTINGS - START

    /*get availables thematics*/
    getThematics: function () {

        this.params = "action=getThematics";

        new Ajax.Request('informationDownload', {
            method: "post",
            parameters: this.noCache(this.params),
            onSuccess: this._getThematics.bind(this)
        });

    },
    _getThematics: function (request) {

        var response = request.responseText.evalJSON(true);

        //filling list
        for (var i = 1; i <= response.length; i++) {

            $("schema").options[i] = new Option(response[i - 1].name, response[i - 1].folder);
        }

        this.thematicListChange();
    },
    /*respond to thematic list change event*/
    thematicListChange: function () {

        var compound = ($("schema").options[$("schema").selectedIndex].value).split(";");

        this.thematicId = compound[0];
        this.schema = compound[1];

        if ($("schema").options[$("schema").selectedIndex].value == '-1') {
            $('downloadbutton').disabled = true;
        } else {
            $('downloadbutton').disabled = false;
        }

    },
/////THEMATIC SETTINGS - END

/////DOWNLOAD FUNCTIONS - START

    getTypeSearch: function () {
   
       var typeSearch = 0;
       var radio1 = document.getElementById("dtype1");
        if (radio1!== 'undefined' || radio1!==null) {
           /* if ($('dtype1').checked) {
                typeSearch = 1;
            }*/
        }
        
        var radio2 = document.getElementById("dtype2");
        if (radio2!== 'undefined' || radio2!==null) {
            if ($('dtype2').checked) {
                typeSearch = 2;
            }
        }
        
        var radio3 = document.getElementById("dtype3");
        if (radio3!== 'undefined' || radio3!==null) {
            if ($('dtype3').checked) {
                typeSearch = 3;
            }
        }
        
     
       return typeSearch;


    },
    download: function () {

        //this.params="

        var dtype;
        var url;     
        
       dtype=this.getTypeSearch();

        url = "informationDownload?action=getWorkbook&schema=" + this.schema + "&thematicid=" + this.thematicId + "&dtype=" + dtype;
        $('downloadsection').show();
        //  location.href = url;


    },
/////DOWNLOAD FUNCTIONS - END

    /*util function*/
    noCache: function (uri) {

        return uri.concat("&noCache=", (new Date).getTime(), ".", Math.random() * 1234567);
    },
    createLoaderBox: function (msg) {

        var loadingContent = document.createElement('div');
        loadingContent.id = 'loadingContent';

        var loadingText = document.createElement('div');
        loadingText.id = 'loadingText';
        if (msg) {
            loadingText.innerHTML = msg;
        } else {
            loadingText.innerHTML = "Subiendo archivo al servidor...";
        }


        var loadingImg = document.createElement('IMG');
        loadingImg.src = "images/ajax-loader.gif";

        loadingContent.appendChild(loadingText);
        loadingContent.appendChild(loadingImg);

        this.loadingWindow = new Control.Modal(false, {
            contents: loadingContent.innerHTML,
            overlayCloseOnClick: false
        });

        this.loadingWindow.open();

    },
    removeLoaderBox: function () {

        this.loadingWindow.close();

    }

});

Event.observe(window, 'load', function (event) {
    infoDownload = new InformationDownload();
});

