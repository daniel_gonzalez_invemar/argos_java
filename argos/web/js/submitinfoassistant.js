var submitAsistant;

var SubmitInfoAsistantManager = Class.create({
	
	initialize:function(){
		
		this.params='';
		this.prmeter;
		this.schema = 'no';
		this.thematicId = null;
		this.actualStep = 1;
		this.jumpStep = -1;
		this.infoRefType = -1;
		this.loadingWindow=null;
		this.uploadedRows = 0;
		this.filetoupload = "final.csv";
		
		//get query params * never believed it, javascript can do it! amazing! :P
		this.prmeter=location.href.toQueryParams();
		
		//put an event on thematic list
		$('thematiclist').observe('change',this.thematicListChange.bindAsEventListener(this));
		//event for buttons to go through steps
		$('buttongoto2').observe('click',this.goToStepTwo.bindAsEventListener(this));
		$('buttonbackto1').observe('click',this.backToStepOne.bindAsEventListener(this));
		$('buttongoto3').observe('click',this.goToStepThree.bindAsEventListener(this));
		$('buttonbackto2').observe('click',this.backToStepTwo.bindAsEventListener(this));
		$('buttongoto4').observe('click',this.goToStepFour.bindAsEventListener(this));
		$('buttonbackto3').observe('click',this.backToStepThree.bindAsEventListener(this));
		$('buttongoto5').observe('click',this.goToStepFive.bindAsEventListener(this));
		$('buttonbackto4').observe('click',this.backToStepFour.bindAsEventListener(this));
		$('buttongoto6').observe('click',this.goToStepSix.bindAsEventListener(this));
		$('buttonbackto5').observe('click',this.backToStepFive.bindAsEventListener(this));		
		$('buttongoto7').observe('click',this.goToStepSeven.bindAsEventListener(this));
		$('buttonbackto6').observe('click',this.backToStepSix.bindAsEventListener(this));		
		$('buttongoto8').observe('click',this.goToStepEigth.bindAsEventListener(this));
		$('buttonbackto7').observe('click',this.backToStepSeven.bindAsEventListener(this));		
		
		//event for link to download previous info ref
		$('linkpreviousinforef').observe('click',this.downloadPreviousInfoRef.bindAsEventListener(this));
		$('linkfullinforef').observe('click',this.downloadFullInfoRef.bindAsEventListener(this));
		
		//event for button to download filtered information for curador schema
		//$('buttoncuradorfilter').observe('click',this.downloadInfoRefCurador.bindAsEventListener(this));
				
		//event for button to PROCESS FILE - I mean upload and process :)
		$('processfilebutton').observe('click',this.processFile.bindAsEventListener(this));
		
		//event for operation type
		var radioButton=$('uploadform').getElements('input[type=radio][name=operation]');
		(radioButton[0]).observe('click',this.getUserToUpdate.bindAsEventListener(this));
		(radioButton[1]).observe('click',this.getUserToUpdate.bindAsEventListener(this));
		
		this.createToolTips();
		
		this.getThematics();

	},
	getOperation:function(){
		return $('uploadform').getElements('input[type=radio][name=operation]').filter (function(e,i) {if (e.getProperty('checked')==true) {return  e.value} })[0];
	
	},

/////SCHEMA SETTINGS - START		
	
	/*controller for previous info ref download*/
	downloadPreviousInfoRef:function(){
		this.infoRefType = 1;
		this.downloadInfoRef();
	},

	/*controller for previous info ref download*/
	downloadFullInfoRef:function(){
		this.infoRefType = 2;
		this.downloadInfoRef();
	},	
	
	/*controller for info ref download*/
	downloadInfoRef:function(){
		
		var filter = new Filter(this.schema, this.thematicId, this.infoRefType);
		
	},
	
	/*puts the correct url to A elements wich need to be configured due to schema selection*/
	configureAppLinks:function(){
		
		$('linkaddnewinforef').href="submitInformation?action=getAddNewInfoRefDoc&schema="+this.schema+"&thematicid="+this.thematicId;

		if (this.thematicId==60)
		{
				$('linknewdatasheet').href="docs/"+this.schema+"/"+this.thematicId+"/ingresomuestras.xlsm";
			}
			else {
				$('linknewdatasheet').href="docs/"+this.schema+"/"+this.thematicId+"/ingresomuestras.xls";
			}
			
		
	},
	
	getUserToUpdate:function(){
		
		if($('uploadform').operation[0].checked){
			return;
		}
		
		this.params="action=getUserInformation";

		new Ajax.Request('submitInformation',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getUserToUpdate.bind(this)					
			});
		
	},
	
	_getUserToUpdate:function(request){
		
		var response = request.responseText.evalJSON(true);
		var responseText = request.responseText;
		
		if(responseText=="NO")
			return;
	
		$("userlist").length = 1;

		//filling list
		for(var i=1;i<=response.length;i++){
			
			$("userlist").options[i]=new Option(response[i-1].name,response[i-1].id);
			
		}
		
	},
	
/////SCHEMA SETTINGS - END	

/////THEMATIC SETTINGS - START
	
	/*get availables thematics*/
	getThematics:function(){
		
		this.params="action=getThematics";

		new Ajax.Request('submitInformation',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getThematics.bind(this)					
			});
		
	},
	
	_getThematics:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		$("thematiclist").length = 1;
		
		//filling list
		for(var i=1;i<=response.length;i++){
			
			$("thematiclist").options[i]=new Option(response[i-1].name,response[i-1].folder);
			
			if(this.prmeter.thematicid){

				if(response[i-1].folder.split(";")[0]==this.prmeter.thematicid){
					$("thematiclist").selectedIndex = i;
					this.configureVariables();
				}
			}
			
		}
		$("buttongoto2").disabled=false;
	},
	
	/*respond to thematic list change event*/
	thematicListChange:function(){
		var compound = ($("thematiclist").options[$("thematiclist").selectedIndex].value).split(";");
		
		if(compound[0]!="no")
			location.href= "infoassistant.jsp?thematicid="+compound[0];

	},
	
	/*configure variables*/
	configureVariables:function(){
		var compound = ($("thematiclist").options[$("thematiclist").selectedIndex].value).split(";");
		this.thematicId = compound[0];
		this.schema = compound[1];
		this.actualStep = 1;
		this.configureAppLinks();
	},

/////THEMATIC SETTINGS - END


/////UPLOAD FUNCTIONS - START

	/*get upload table names*/
	getUploadTableNames:function(){
		
		this.params="action=getUploadTableNames&schema="+this.schema;

		new Ajax.Request('submitInformation',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getUploadTableNames.bind(this)					
			});
		
	},
	
	_getUploadTableNames:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		$("filenamelist").length=1;
		
		//filling list
		for(var i=1;i<=response.length;i++){
			
			$("filenamelist").options[i]=new Option(response[i-1].alias,response[i-1].name);				
		}

	},

	/*process file */
	processFile:function(){
		
		this.prepareResultTable();
		
		//check if correct file was selected
		if(!this.validateUploadForm()){
			return;
		}

		//configure and send uploadform
		//$("uploadform").action="upload";
		$("uploadform").submit();
		
		this.createLoaderBox();
		this.viewLoadProcess();
	},
	
	/*validate upload form*/
	validateUploadForm:function(){
		
		var result = true;
		
		//var filename = $("filenamelist").options[$("filenamelist").selectedIndex].text;
		var filePath = $("filetouploadbox").value;
		
		var startIndex = filePath.length - 10;
			
		//check if correct file was selected
/*		if(filePath.indexOf("\\"+this.filetoupload, startIndex)==-1 && filePath.indexOf("/"+this.filetoupload, startIndex)==-1){
			alert("El archivo seleccionado no es v\xE1lido.");
			$("filetouploadbox").value = "";
			result = false;
		}
*/		
		return result;
		
	},
	
	viewLoadProcess:function(){
		
		this.params="action=view";

		new Ajax.Request('upload',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._viewLoadProcess.bind(this)					
			});
	},
		
	_viewLoadProcess:function(request){

		if(request.responseText=="OK"){
			
			this.splitUploadedFile();
			
		}else if(request.responseText=="NO"){
			setTimeout("submitAsistant.viewLoadProcess()", 100);
		}else{
			this.removeLoaderBox();
			alert("Ocurri\xF3 un error mientras se intentaba subir el archivo, es posible que su sesi\xF3n de usuario haya caducado, salga del sistema, vuelva a iniciar sesi\xF3n e int\xE9ntelo nuevamente");
		}
		
	},
/////UPLOAD FUNCTIONS - END

/////SPLIT UPLOADED FILE - START

	splitUploadedFile:function(){
		
		var ownerId = $("userlist").options[$("userlist").selectedIndex].value;
		
		$('loadingText').innerHTML="Procesando archivo...";
		
		this.params="schema="+this.schema+"&thematicid="+this.thematicId+"&filename="+this.filetoupload+ "&ownerid="+ownerId;
				
		if($('uploadform').operation[1].checked){
			this.params=this.params+"&negativefile=true";
		}

		new Ajax.Request('fileSpliter',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._splitUploadedFile.bind(this)					
			});
	},
	
	_splitUploadedFile:function(request){
		
		var response = parseInt(request.responseText);
		
		if(response>0){
			this.uploadedRows = response;
			this.processUploadedFile();
		}else{
			this.removeLoaderBox();
			alert("Error al procesar la hoja de datos, debido a que NO se diligenciaron una o varias celdas de una columna obligatoria o se colocaron c�digos equivocados. Revise y complete los datos especialmente las celdas con contenido de #N/A, o vacias con encabezado naranja (obligatorias)");
		}

	},

/////SPLIT UPLOADED FILE - END

/////PROCESS UPLOADED FILE - START

	processUploadedFile:function(){

		var ownerId;
		this.params="schema="+this.schema+"&thematicid="+this.thematicId+"&urows="+this.uploadedRows;
				
		if($('uploadform').operation[0].checked){
			
			$('loadingText').innerHTML="Guardando informaci\xF3n en la base de datos...";
			
			new Ajax.Request("multipleDBUpload",{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._processUploadedFile.bind(this)					
			});
			
		}else if($('uploadform').operation[1].checked){

			$('loadingText').innerHTML="Validando informaci\xF3n y preparando actualizaci\xF3n...";

			ownerId = $("userlist").options[$("userlist").selectedIndex].value
			
			this.params = this.params + "&ownerid=" + ownerId+"&urows="+this.uploadedRows;
			
			new Ajax.Request("multipleDBUploadUpdate",{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._processUploadedFileToUpdate.bind(this)					
			});
			
		}

	},
	
	_processUploadedFile:function(request){

		$('loadingText').innerHTML="Finalizando tarea...";

		var response = request.responseText.evalJSON(true);
		
		//config results options
		if (response.filestatus==1){
			$('processok').style.cssText = "display:block";
			$('uploadoptions').style.cssText = "display:none";
		}else if(response.filestatus>1){
			$('processerror').style.cssText = "display:block";
			//show suggestion
			$('suggestiontable').style.cssText = "display:block";
			$('suggestion').appendChild(document.createTextNode(response.description));
			//configure links
			$('generatedfiles').style.cssText = "display:block";
			$('logfileoptions').style.cssText = "display:block";
			$('linklogfile').href=response.logfile;
		}
						
		//shows table results
		$('resultstable').style.cssText = "display:block";
		
		//show generated files
		if (response.filestatus==2){
			
			$('linkbadfile').href=response.badfile;
			$('linkdiscardfile').href=response.discardfile;
			$('badfileoptions').style.cssText = "display:block";
			$('discardfileoptions').style.cssText = "display:block";
			
		}else if(response.filestatus==3){
			$('linkbadfile').href=response.badfile;
			$('badfileoptions').style.cssText = "display:block";
			
		}else if(response.filestatus==4){
			
			$('linkdiscardfile').href=response.discardfile;
			$('discardfileoptions').style.cssText = "display:block";
			
		}
		
		setTimeout("submitAsistant.removeLoaderBox()", 300);
	},

	_processUploadedFileToUpdate:function(request){

		$('loadingText').innerHTML="Finalizando tarea...";
				
		var response = request.responseText.evalJSON(true);
		
		if(response.updatestatus=='1'){
			$('updateprocessok').style.cssText = "display:block";
			$('uploadoptions').style.cssText = "display:none";
		}else if(response.updatestatus=='2'){
			$('processerror').style.cssText = "display:block";
			//show suggestion
			$('suggestiontable').style.cssText = "display:block";
			$('suggestion').appendChild(document.createTextNode(response.description));			
		}else if(response.updatestatus=='3'){
			$('processerror').style.cssText = "display:block";
			//show suggestion
			$('suggestiontable').style.cssText = "display:block";
			$('suggestion').appendChild(document.createTextNode(response.description));
			var a = document.createElement("a");
			a.href= response.badidfilepath;
			a.appendChild(document.createTextNode("Descargue archivo"));
			$('suggestion').appendChild(a);
			
		}else if(response.updatestatus=='4'){
			
			$('processerror').style.cssText = "display:block";
			//show suggestion
			$('suggestiontable').style.cssText = "display:block";
			$('suggestion').appendChild(document.createTextNode(response.description));
			//configure links
			$('generatedfiles').style.cssText = "display:block";
			$('logfileoptions').style.cssText = "display:block";
			$('linklogfile').href=response.logfile;
			
			//shows table results
			$('resultstable').style.cssText = "display:block";
		
			//show generated files
			if (response.filestatus==2){
				
				$('linkbadfile').href=response.badfile;
				$('linkdiscardfile').href=response.discardfile;
				$('badfileoptions').style.cssText = "display:block";
				$('discardfileoptions').style.cssText = "display:block";
				
			}else if(response.filestatus==3){
				$('linkbadfile').href=response.badfile;
				$('badfileoptions').style.cssText = "display:block";
				
			}else if(response.filestatus==4){
				
				$('linkdiscardfile').href=response.discardfile;
				$('discardfileoptions').style.cssText = "display:block";
				
			}
		}else if(response.updatestatus=='6'){
			$('processerror').style.cssText = "display:block";
			//show suggestion
			$('suggestiontable').style.cssText = "display:block";
			$('suggestion').appendChild(document.createTextNode(response.description));			
		}
		
		//shows table results
		$('resultstable').style.cssText = "display:block";
		
		setTimeout("submitAsistant.removeLoaderBox()", 300);
	},
		

	prepareResultTable:function(){
		
		$('processok').style.cssText = "display:none";

		$('processerror').style.cssText = "display:none";
		
		$('generatedfiles').style.cssText = "display:none";

		$('suggestiontable').style.cssText = "display:none";
		
		$('suggestion').innerHTML = "";

		//shows table results
		$('resultstable').style.cssText = "display:none";
		
		$('linklogfile').href="";
		$('linkbadfile').href="";
		$('linkdiscardfile').href="";
		
		$('logfileoptions').style.cssText = "display:none";
		$('badfileoptions').style.cssText = "display:none";
		$('discardfileoptions').style.cssText = "display:none";
		
	},

/////PROCESS UPLOADED FILE - END

/////PASSING THROUGH STEPS - START	

	/*go to step two from step one*/
	goToStepTwo:function(){
		
		var compound = ($("thematiclist").options[$("thematiclist").selectedIndex].value).split(";");
		
		if(compound[0]!="no"){
			$("thematiclist").style.visibility = 'hidden';
			new Effect.Fade($('tablestep1'));
			new Effect.Appear($('tablestep2'));
			
			if(this.actualStep<2)this.actualStep = 2;
			this.jumpStep = 2;
		}else{
			alert("Debe seleccionar una tem\xE1tica");
		}

	},
	
	/*back to step one from step two*/
	backToStepOne:function(){
		
		$("thematiclist").style.visibility = 'visible';
		new Effect.Fade($('tablestep2'));
		new Effect.Appear($('tablestep1'));
		
		try{
			new Effect.Fade($(this.schema+'filter'));
		}catch(e){}
		
		this.jumpStep = 1;
	},

	/*go to step two from step one*/
	goToStepThree:function(){

		try{
			new Effect.Fade($(this.schema+'filter'));
		}catch(e){}
		
		new Effect.Fade($('tablestep2'));
		new Effect.Appear($('tablestep3'));
		
		if(this.actualStep<3)this.actualStep = 3;
		this.jumpStep = 3;
	},
	
	/*back to step two from step three*/
	backToStepTwo:function(){
		
		new Effect.Fade($('tablestep3'));
		new Effect.Appear($('tablestep2'));
		this.jumpStep = 2;
	},
	
	/*go to step four from step three*/
	goToStepFour:function(){
		
		new Effect.Fade($('tablestep3'));
		new Effect.Appear($('tablestep4'));
		if(this.actualStep<4)this.actualStep = 4;
		this.jumpStep = 4;

	},
	
	/*back to step three from step four*/
	backToStepThree:function(){
		
		new Effect.Fade($('tablestep4'));
		new Effect.Appear($('tablestep3'));
		this.jumpStep = 3;
	},
	
	/*go to step five from step four*/
	goToStepFive:function(){
		
		try{
			new Effect.Fade($(this.schema+'filter'));
		}catch(e){}
		
		new Effect.Fade($('tablestep4'));
		new Effect.Appear($('tablestep5'));
			if(this.actualStep<5)this.actualStep = 5;
			this.jumpStep = 5;

	},
	
	/*back to step four from step five*/
	backToStepFour:function(){
		
		new Effect.Fade($('tablestep5'));
		new Effect.Appear($('tablestep4'));
		this.jumpStep = 4;
	},
	
	/*go to step six from step five*/
	goToStepSix:function(){
		
		new Effect.Fade($('tablestep5'));
		new Effect.Appear($('tablestep6'));
		if(this.actualStep<6)this.actualStep = 6;
		this.jumpStep = 6;

	},
	
	/*back to step five from step six*/
	backToStepFive:function(){
		
		new Effect.Fade($('tablestep6'));
		new Effect.Appear($('tablestep5'));
		this.jumpStep = 5;
	},
	
	/*go to step seven from step six*/
	goToStepSeven:function(){
		
		new Effect.Fade($('tablestep6'));
		new Effect.Appear($('tablestep7'));
		if(this.actualStep<7)this.actualStep = 7;
		this.jumpStep = 7;

	},
	
	/*back to step six from step seven*/
	backToStepSix:function(){
		
		new Effect.Fade($('tablestep7'));
		new Effect.Appear($('tablestep6'));
		this.jumpStep = 6;
	},
	
	/*go to step eigth from step seven*/
	goToStepEigth:function(){
		
		new Effect.Fade($('tablestep7'));
		new Effect.Appear($('tablestep8'));
		if(this.actualStep<8)this.actualStep = 8;
		this.jumpStep = 8;
		
	},
	
	/*back to step six from step seven*/
	backToStepSeven:function(){
		
		new Effect.Fade($('tablestep8'));
		new Effect.Appear($('tablestep7'));
		this.jumpStep = 7;
	},
	
	//TODO
	jumpToStep:function(goTo){
		
		if(goTo>this.actualStep){
			alert("Debe seguir los pasos en el orden sugerido");
			return;
		}
		
		if(this.jumpStep==-1){
			this.jumpStep = this.actualStep;			
		}
		//alert("jumpStep: "+this.jumpStep+" goTo:"+goTo);
		new Effect.Fade($('tablestep'+this.jumpStep));
		new Effect.Appear($('tablestep'+goTo));
		
		try{
			new Effect.Fade($(this.schema+'filter'));
		}catch(e){}
		
		/*show or hidde thematic list*/
		if(goTo==1){
			$("thematiclist").style.visibility = 'visible';
		}
		if(this.jumpStep==1){
			$("thematiclist").style.visibility = 'hidden';
		}
		
		this.jumpStep = goTo;
	},
	

/////PASSING THROUGH STEPS - END	
	
/////UTIL FUNCTIONS - START		
	
	/*util function*/
	noCache:function(uri){
		
		return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
	},
	
	createToolTips:function(){
		    
		new Control.Modal('logfilehelp',{
		contents: 'Contiene el historial que describe<br/>el procesamiento del archivo',
		position: 'mouse', 
		offsetTop: 20
	    });
		
		new Control.Modal('badfilehelp',{
		contents: 'Contiene aquellos registros que causaron<br/>problemas durante el proceso o<br/>cuyo formato era incorrecto',
		position: 'mouse', 
		offsetTop: 20
	    });

		new Control.Modal('discardfilehelp',{
		contents: 'Contiene aquellos registros que fueron descartados<br/>y por ello no fueron subidos a la base de datos.',
		position: 'mouse', 
		offsetTop: 20
	    });
		
	},
	
	createLoaderBox:function(){

		var loadingContent = document.createElement('div');
		loadingContent.id	= 'loadingContent';
	
		var loadingText=document.createElement('div');
		loadingText.id	= 'loadingText';
		loadingText.innerHTML="Subiendo archivo al servidor...";
		
		var loadingImg=document.createElement('IMG');
		loadingImg.src="images/ajax-loader.gif";
		
		loadingContent.appendChild(loadingText);
		loadingContent.appendChild(loadingImg);
		
		this.loadingWindow=new Control.Modal(false,{
			contents: loadingContent.innerHTML,
			overlayCloseOnClick: false
			});

		this.loadingWindow.open();

	},
	
	removeLoaderBox:function(){
		
		this.loadingWindow.close();
		
	},
	
	restartAssistant:function(){
		
		location.href="infoassistant.jsp";
		
	}
		
});

Event.observe(window, 'load',function(event){
			submitAsistant = new SubmitInfoAsistantManager();									  
	});
	
