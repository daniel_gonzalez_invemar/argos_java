var roleAdmin;

var RoleAdmin = Class.create({
	
	initialize:function(){
		
		this.params='';
		this.loadingWindow=null;
		this.userId = null;
		this.roles = 0;
		this.editRoleId = null;
		
		//new role events
		$('project').observe('change',this.getRolesByProject.bindAsEventListener(this));
		$('addp_new').observe('click',this.addPermissionForNewRole.bindAsEventListener(this));
		$('removep_new').observe('click',this.removePermissionForNewRole.bindAsEventListener(this));		
		$('createrol').observe('click',this.createNewRole.bindAsEventListener(this));
		
		//role edition events
		$('addp_edit').observe('click',this.addPermissionForRoleEdition.bindAsEventListener(this));
		$('removep_edit').observe('click',this.removePermissionForRoleEdition.bindAsEventListener(this));
		$('updateroledescp').observe('click',this.updateRoleDescription.bindAsEventListener(this));
		
		this.getProjects();

	},

/////ROLE ADMIN - START

	/*get availables projects*/
	getProjects:function(){
		
		this.createLoaderBox("Trayendo informaci\xF3n de configuraci\xF3n");
		
		this.params="action=getProjects";

		new Ajax.Request('roleAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getProjects.bind(this)					
			});
		
	},
	
	_getProjects:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		$("project").length = 1;
		$("project_new").length = 0;
		
		//filling list
		for(var i=1;i<=response.length;i++){
			
			$("project").options[i]=new Option(response[i-1].name,response[i-1].id);
			$("project_new").options[i-1]=new Option(response[i-1].name,response[i-1].id);
		}
		
		this.getPermissions();

	},
	
	/*get availables permissions*/
	getPermissions:function(){
		
		this.params="action=getPermissions";

		new Ajax.Request('roleAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getPermissions.bind(this)					
			});
		
	},
	
	_getPermissions:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		$("permlist_new").length = 0;
		
		//filling list
		for(var i=0;i<response.length;i++){
			
			$("permlist_new").options[i]=new Option(response[i].description,response[i].id);
		}
		
		this.removeLoaderBox();

	},
	
	//when user selects a project
	getRolesByProject:function(){
		
		var projectId = $("project").options[$("project").selectedIndex].value;
		
		if(projectId==-1){
			alert("Debe seleccionar un proyecto");
			return;
		}
		
		this.createLoaderBox("Obteniendo los roles existentes para el proyecto seleccionado...");
		
		this.params="action=getRolesByProject&projectid="+projectId;

		new Ajax.Request('roleAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getRolesByProject.bind(this)					
			});
		
	},
	
	_getRolesByProject:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		this.emptyTable();
		
		for(var i=0;i<=response.length-1;i++){
			this.createRoleRow(response[i].id,response[i].description);	
		}
		
		if (response.length>0)
			var abop = new ABOP("roletbody", 7);
			
		this.removeLoaderBox();
		
	},
	
	//add permission to the permission list for a new role
	addPermissionForNewRole:function(){
		
		if($("permlist_new").length==0 || $("permlist_new").selectedIndex==-1){
			return;
		}
		
		var id = $("permlist_new").options[$("permlist_new").selectedIndex].value;
		var description = $("permlist_new").options[$("permlist_new").selectedIndex].text;
		
		$("spermlist_new").options[$("spermlist_new").length]=new Option(description, id);
		
		$("permlist_new").options[$("permlist_new").selectedIndex] = null;
		
	},
	
	//remove permission from the permission list for a new role
	removePermissionForNewRole:function(){
		
		if($("spermlist_new").length==0 || $("spermlist_new").selectedIndex==-1){
			return;
		}
		
		var id = $("spermlist_new").options[$("spermlist_new").selectedIndex].value;
		var description = $("spermlist_new").options[$("spermlist_new").selectedIndex].text;
		
		$("permlist_new").options[$("permlist_new").length]=new Option(description, id);
		
		$("spermlist_new").options[$("spermlist_new").selectedIndex] = null;
		
	},
	
	//create new role
	createNewRole:function(){
		
		var name = $("name_new").value;
		var projectId = $("project_new").options[$("project_new").selectedIndex].value;
		var queryString ="";
		var permission;
		
		if(name==""||name==" "){
			alert("Debe ingresar un nombre para el nuevo rol");
			return;
		}
		
		if($("spermlist_new").length==0){
			alert("Debe asignar permisos para el nuevo rol");
			return;
		}
		
		//create parameters
		for(var i = 0; i<$("spermlist_new").length; i++){
			
			permission = "perm"+i;
			
			if(i>0){
				queryString = queryString+"&";
			}
			
			queryString = queryString + permission + "=" + $("spermlist_new").options[i].value;
		}
		
		this.params="action=createNewRole&projectid=" + projectId + "&rolname=" + name + "&" +queryString;

		new Ajax.Request('roleAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._createNewRole.bind(this)					
			});
		
	},
	
	_createNewRole:function(request){
		
		var response = request.responseText;
		
		if(response=="OK"){
			alert("Rol creado exit�samente");
			$("spermlist_new").length=0;
			$("name_new").value="";
			this.createLoaderBox("Trayendo informaci\xF3n de configuraci\xF3n");
			this.getPermissions();
		}else if(response=="ENAME"){
			alert("El nombre introducido est� siendo usado por otro rol");
		}else{
			alert("No fue posible crear el nuevo rol");
		}
		
	},
	
	//role edition
	
	//role edit function
	editRole:function(roleId){
				
		this.editRoleId = roleId;
		
		$('name_edit').value = $("rolname_"+roleId).innerHTML;
				
		new Effect.Appear($('editionbox'));
		
		location.href="roleadmin.jsp#editionpoint";
		
		this.createLoaderBox("Obteniendo informaci\xF3n del rol...");
		
		this.getDeniedPermissions();
		
	},
	
	/*get denied role permissions*/
	getDeniedPermissions:function(){
		
		this.params="action=getDeniedPermissionsByRole&roleid="+this.editRoleId;

		new Ajax.Request('roleAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getDeniedPermissions.bind(this)					
			});
		
	},
	
	_getDeniedPermissions:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		$("permlist_edit").length = 0;
		
		//filling list
		for(var i=0;i<response.length;i++){
			
			$("permlist_edit").options[i]=new Option(response[i].description,response[i].id);
		}

		this.getAllowedPermissions();
		
	},
	
	/*get allowed role permissions*/
	getAllowedPermissions:function(){
		
		this.params="action=getAllowedPermissionsByRole&roleid="+this.editRoleId;

		new Ajax.Request('roleAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getAllowedPermissions.bind(this)					
			});
		
	},
	
	_getAllowedPermissions:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		$("spermlist_edit").length = 0;
		
		//filling list
		for(var i=0;i<response.length;i++){
			
			$("spermlist_edit").options[i]=new Option(response[i].description,response[i].id);
		}
		
		this.removeLoaderBox();
		
	},
	
	
	
	//add permission to the permission list for role edition
	addPermissionForRoleEdition:function(){
				
		if($("permlist_edit").length==0 || $("permlist_edit").selectedIndex==-1){
			return;
		}
		
		this.createLoaderBox("Agregando permiso...");
		
		var permissionId = $("permlist_edit").options[$("permlist_edit").selectedIndex].value;
		
		this.params="action=addRolePermission&roleid="+this.editRoleId+"&permissionid="+permissionId;

		new Ajax.Request('roleAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._addPermissionForRoleEdition.bind(this)					
			});
		
	},
	
	_addPermissionForRoleEdition:function(request){

		this.removeLoaderBox();
				
		var response = request.responseText;
		
		if(response=="OK"){

			var id = $("permlist_edit").options[$("permlist_edit").selectedIndex].value;
			var description = $("permlist_edit").options[$("permlist_edit").selectedIndex].text;
			
			$("spermlist_edit").options[$("spermlist_edit").length]=new Option(description, id);
			
			$("permlist_edit").options[$("permlist_edit").selectedIndex] = null;
						
		}else{
			alert("No fue posible a�adir el nuevo permiso");
		}
		
	},
	
	//remove permission to the permission list for role edition
	removePermissionForRoleEdition:function(){
		
		if($("spermlist_edit").length==0 || $("spermlist_edit").selectedIndex==-1){
			return;
		}
		
		this.createLoaderBox("Removiendo permiso...");
		
		var permissionId = $("spermlist_edit").options[$("spermlist_edit").selectedIndex].value;
		
		this.params="action=removeRolePermission&roleid="+this.editRoleId+"&permissionid="+permissionId;

		new Ajax.Request('roleAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._removePermissionForRoleEdition.bind(this)					
			});
		
	},
	
	_removePermissionForRoleEdition:function(request){
		
		var response = request.responseText;
		
		this.removeLoaderBox();
		
		if(response=="OK"){

			var id = $("spermlist_edit").options[$("spermlist_edit").selectedIndex].value;
			var description = $("spermlist_edit").options[$("spermlist_edit").selectedIndex].text;
			
			$("permlist_edit").options[$("permlist_edit").length]=new Option(description, id);
			
			$("spermlist_edit").options[$("spermlist_edit").selectedIndex] = null;
				
		}else{
			alert("No fue posible remover el permiso");
		}
		
	},
	
	//update role description
	updateRoleDescription:function(){
		
		this.createLoaderBox("Actualizando nombre...");
		
		var roleDescription = $("name_edit").value;
		
		this.params="action=updateRoleDescription&roleid="+this.editRoleId+"&description="+roleDescription;

		new Ajax.Request('roleAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._updateRoleDescription.bind(this)					
			});
		
	},
	
	_updateRoleDescription:function(request){
		
		var response = request.responseText;
		
		this.removeLoaderBox();
		
		if(response=="OK"){
			
			$("rolname_"+this.editRoleId).innerHTML ="";
			$("rolname_"+this.editRoleId).appendChild(document.createTextNode($('name_edit').value));

			alert("Nombre del rol actualizado exit�samente");
				
		}else{
			alert("No fue posible actualizar el nombre del rol");
		}
		
	},
	
	closeRoleEditor:function(){
		new Effect.Fade($('editionbox'));
	},


	//delete role
	
	//role delete function
	deleteRole:function(roleId){
		
		this.editRoleId = roleId;
		
		var conf = confirm("Si elimina un rol, los usuarios relacionados con �ste perder�n los permisos asignados �desea continuar?");
		
		if(!conf)
			return;
		
		this.createLoaderBox("Eliminando rol...");
		
		this.params="action=deleteRole&roleid="+roleId;

		new Ajax.Request('roleAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._deleteRole.bind(this)					
			});
		
	},
	
	_deleteRole:function(request){
				
		var response = request.responseText;
		
		if(response == "OK"){
			$("roletbody").removeChild($("row_"+this.editRoleId));
			alert("Rol eliminado exitosamente");
		}else{
			alert("No fue posible eliminar el rol");
		}
		
		this.removeLoaderBox();
		
	},

/////ROLE ADMIN - END

/////ROLE TABLE - START

	createRoleRow:function(roleId, name){

		var tbody = $("roletbody");	
		var tr;
		var td;
		var img;
		var a;
		var div;

		tr= document.createElement("tr");
		tr.id="row_"+roleId;

		if((this.roles+1)%2==0){
			tr.setAttribute("bgColor", "#FFFFFF");
		}
		
		td= document.createElement("td");
		
		div = document.createElement("div");
		div.id = "rolname_"+roleId;
		div.appendChild(document.createTextNode(name));
		
		td.appendChild(div);
		tr.appendChild(td);
		
		//edit
		a = document.createElement("a");
		a.href = "javascript:roleAdmin.editRole("+roleId+");";
		img = document.createElement("img");
		img.src = "images/edit.gif";
		img.setAttribute("border","0");
		
		a.appendChild(img);
		
		td= document.createElement("td");
		td.setAttribute("align", "center");
		td.appendChild(a);
		tr.appendChild(td);
	
		tbody.appendChild(tr);
		
		//delete
		a = document.createElement("a");
		a.href = "javascript:roleAdmin.deleteRole("+roleId+");";
		img = document.createElement("img");
		img.src = "images/delete.gif";
		img.setAttribute("border","0");
		
		a.appendChild(img);
		
		td= document.createElement("td");
		td.setAttribute("align", "center");
		td.appendChild(a);
		tr.appendChild(td);
	
		tbody.appendChild(tr);
		
		this.roles = this.roles+1;
		
	},

/////ROLE TABLE - END

	emptyTable:function(){
		
		var tbodyRows = $("roletbody").getElementsByTagName("tr");
		var tbodylength = tbodyRows.length -1;
		
		for(tbodylength; tbodylength>=0 ; tbodylength--){
			$("roletbody").removeChild(tbodyRows[tbodylength]);
		}
		
		$("ABOPndiv_roletbody").innerHTML="";
		
		this.logs = 0;
		
	},
	
	createLoaderBox:function(lText){

		var loadingContent = document.createElement('div');
		loadingContent.id	= 'loadingContent';
	
		var loadingText=document.createElement('div');
		loadingText.id	= 'loadingText';
		
		if(lText){
			loadingText.innerHTML=lText;
		}else{
			loadingText.innerHTML="Verificando credenciales de usuario...";			
		}

		
		var loadingImg=document.createElement('IMG');
		loadingImg.src="images/ajax-loader.gif";
		
		loadingContent.appendChild(loadingText);
		loadingContent.appendChild(loadingImg);
		
		this.loadingWindow=new Control.Modal(false,{
			contents: loadingContent.innerHTML,
			overlayCloseOnClick: false
			});

		this.loadingWindow.open();

	},
	
	removeLoaderBox:function(){
		
		this.loadingWindow.close();
		
	},
	
	/*util function*/
	noCache:function(uri){
		
		return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
	}
		
});

Event.observe(window, 'load',function(event){
			roleAdmin = new RoleAdmin();									  
	});
	
