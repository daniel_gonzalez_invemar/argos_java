
var proyectID;
var proyectName;
var proyectTitle;

function hideHeaderAndFooter(idProyect) {
    if (idProyect != '0') {
        $("#mainHeader").css("display", "none");
        $("#mainFooter").css("display", "none");
        $("#footerRights").css("display", "none");
    } else {
        $("#mainHeader").css("display", "inline");
        $("#mainFooter").css("display", "inline");
        $("#footerRights").css("display", "inline");
    }
}

function confirmPasswordRegeneration(Myuid, MyEmail, MyCkey) {

    $.post("login", {action: 'cpassgen', uid: Myuid, email: MyEmail, ckey: MyCkey})
            .done(function(data) {
                if (data.result == "OK") {
                    $("#message").css("color", "blue");
                    $("#message").html(data.desc);
                } else {
                    $("#message").css("color", "red");
                    $("#message").html(data.desc);
                }
            });
}

function setSeleccion(nameProyect) {
   
    var vectorData = nameProyect.split(";");
   
    proyectID = vectorData[0];
   
    proyectName = vectorData[1];
   
    proyectTitle = vectorData[2];
   
    $("#opcionprincipal").html(proyectName);

}

function seleccionarPreyecto() {

    var Myemail = document.getElementById('email').value;
    $.post("login", {action: 'setuserproject', username: Myemail, pid: proyectID, pname: proyectName, projecttitle: proyectTitle})
            .done(function(data) {
                if (data == "OK") {
                    window.location.replace('temp2.jsp');
                } else {
                    alert("No fue posible establecer el proyecto, intente nuevamente");

                }
            });

}
function controlAcceso() {
    var Myemail = document.getElementById('email').value;
    var Mypassword = document.getElementById('password').value;

     
    
    $.post("login", {action: 'ver', email: Myemail, password: Mypassword})
            .done(function(data) {

                if (data == "NO") {
                    $("#message").css("color", "red");
                    $("#message").html('Direcci\xF3n de correo electr\xF3nico y/o contrase\xF1a inv\xE1lidos');
                } else {
                    $("#listaproyectos").empty();
                    $("#opcionprincipal").html('Seleccione');
                    $('#myModalProyectos').modal('show');

                    $.each(data, function(key, value) {
                        //if (key == 1) {                              
                        //    $("#opcionprincipal").html(value.name);
                        //} else {
                        
                            $("#listaproyectos").append("<li><a  onclick='setSeleccion(\"" + value.id + ";" + value.name + ";" + value.projecttitle + "\")'  href='#' id='" + value.id + ":" + value.name + ":" + value.projecttitle + "'>" + value.name + "</a></li>");
                       // }
                    });
                }
            })                
           



}

function showPasswordChangeDialog() {
    $('#result').html('');
    $('#myChangePassword').modal('show');

}
/**
 * Comment
 */
function changePassword() {

    var Myemail = document.getElementById('diremail').value;
      $('#result').html("<img src='images/ajax-loaderArrow.gif' id='loaderimage' />");
    $.post("login", {action: 'passregen', email: Myemail})

            .done(function(data) {
                if (data.result == "OK") {
                    $('#result').html(data.desc);
                } else {
                    $('#result').html(data.desc);

                }

            })
            .fail(function() {
                alert("error comunicandonse con el servidor");
            });
}

