
var Filter = Class.create({
	
	initialize:function(schema, thematicId, infoRefType){
		
		this.schema = schema;
		this.thematicId = thematicId;
		this.infoRefType = infoRefType;
				
		this.configureInfoRefCurador();

	},
	
	configureInfoRefCurador:function(){
		
		this.createFilterTable(this.schema,this.infoRefType,this.thematicId);
		
		new Effect.Appear($('filterbox'));
		
		$("buttoncuradorfilter").disabled=true;
		
		//fill filters
		this.getSpeciesInfo();

	},
	
	getSpeciesInfo:function(){
		
		this.params="action=getFilterInfo&infoType=especies&schema="+this.schema;

		new Ajax.Request('submitInformation',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getSpeciesInfo.bind(this)					
			});
	},
	
	_getSpeciesInfo:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		//reset list
		$("phylumcod").length=0;
		
		for(var i=0;i<response.length;i++){
			
			$("phylumcod").options[i]=new Option(response[i].name,response[i].value);				
		}	
		
		$("buttoncuradorfilter").disabled=false;
		
	},
	
	/*download reference information for curador schema*/
	downloadInfoRefCurador:function(){
		
		$('curadorfilterform').schema.value = this.schema;
		$('curadorfilterform').inforeftype.value = this.infoRefType;
		$('curadorfilterform').thematicid.value=this.thematicId;
		$('curadorfilterform').submit();
	},
	
	createFilterTable:function(schema,inforeftype,thematicid){
		
		var form;
		var table;
		var thead;
		var tbody;
		var tr;
		var td;
		var select;
		var input;
		var a;
		
		form = document.createElement("form");
		form.id = "curadorfilterform";
		form.method = "post";
		form.action="submitInformation";
		
		table = document.createElement("table");
		table.setAttribute("class", "alerts");
		table.setAttribute("className", "alerts");

		thead = document.createElement("thead");
		
		//thead
		tr= document.createElement("tr");

		td= document.createElement("td");
		td.setAttribute("class", "alertHd");
		td.setAttribute("className", "alertHd");
		td.appendChild(document.createTextNode("Descargar"));
		tr.appendChild(td);
		thead.appendChild(tr);
		
		//tbody
		tbody = document.createElement("tbody");
		
		tr= document.createElement("tr");

		td= document.createElement("td");
		td.setAttribute("class", "alertBod");
		td.setAttribute("className", "alertBod");
		td.appendChild(document.createTextNode("Filtrar especies:"));
		tr.appendChild(td);			
		
		tbody.appendChild(tr);
		
		//
		
		tr= document.createElement("tr");

		td= document.createElement("td");
		td.setAttribute("class", "alertBod");
		td.setAttribute("className", "alertBod");
		td.appendChild(document.createTextNode("Seleccione phylum"));
		tr.appendChild(td);			
		
		tbody.appendChild(tr);

		//selector
		
		tr= document.createElement("tr");

		td= document.createElement("td");
		td.setAttribute("class", "alertBod");
		td.setAttribute("className", "alertBod");

		select = document.createElement("select");
		select.id="phylumcod";
		select.name="phylumcod";
		
		td.appendChild(select);

		//hidden inputs
		
		input = document.createElement("input");
		input.id = "schema";
		input.name = "schema";
		input.type = "hidden";
		input.value = schema;
		
		td.appendChild(input);
		
		input = document.createElement("input");
		input.id = "inforeftype";
		input.name = "inforeftype";
		input.type = "hidden";
		input.value = inforeftype;

		td.appendChild(input);
		
		input = document.createElement("input");
		input.id = "thematicid";
		input.name = "thematicid";
		input.type = "hidden";
		input.value = thematicid;

		td.appendChild(input);
		
		input = document.createElement("input");
		input.id = "action";
		input.name = "action";
		input.type = "hidden";
		input.value="getInfoRefDoc"

		td.appendChild(input);
	
		tr.appendChild(td);			
		
		tbody.appendChild(tr);
		
		//button
		
		tr= document.createElement("tr");

		td= document.createElement("td");
		td.setAttribute("class", "alertBod");
		td.setAttribute("className", "alertBod");
		
		input = document.createElement("input");
		input.id = "buttoncuradorfilter";
		input.name = "buttoncuradorfilter";
		input.type = "submit";
		input.value = "Descargar";

		td.appendChild(input);
	
		tr.appendChild(td);			
		
		tbody.appendChild(tr);
		
		//close link
		tr= document.createElement("tr");

		td= document.createElement("td");
		
		a = document.createElement("a");
		a.href = "javascript:void(0);";
		a.onclick=this.closeFilter.bindAsEventListener(this);
		a.appendChild(document.createTextNode("Cerrar"));
		
		td.appendChild(a);
	
		tr.appendChild(td);			
		
		tbody.appendChild(tr);

		table.appendChild(thead);		
		table.appendChild(tbody);
		
		form.appendChild(table);
		
		$("filterbox").innerHTML="";
		$("filterbox").appendChild(form);
		
	},
	
	closeFilter:function(){
		new  Effect.Fade('filterbox');
	},
	
/////UTIL FUNCTIONS - START		
	
	/*util function*/
	noCache:function(uri){
		
		return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
	}
	
		
});
	
