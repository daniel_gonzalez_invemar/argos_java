var gdbUpload;

var GenericDBUpload = Class.create({
	
	initialize:function(){
		
		this.params='';
		this.schema= null;
		this.table = null;
		this.uploadedRows = 0;

		this.loadingWindow=null;
		this.filetoupload = "dbupload.csv";
		
		//table configuration variables
		this.columns = null; //columns found on uploaded file
		this.tColumnsCount = 0; //number of columns of db table
		
		//state variable
		this.readyToProcess=false;
		
		//put an event on thematic list
		$('schema').observe('change',this.schemaListChange.bindAsEventListener(this));
		$('table').observe('change',this.tableListChange.bindAsEventListener(this));	
		
		//event for button to PROCESS FILE - I mean upload and process :)
		$('processfilebutton').observe('click',this.processFile.bindAsEventListener(this));
		$('uploadandconfig').observe('click',this.uploadFile.bindAsEventListener(this));

		this.createToolTips();
		
		this.getSchemas();

	},


/////THEMATIC SETTINGS - START
	
	/*get availables thematics*/
	getSchemas:function(){
		
		this.params="action=getSchemas";

		new Ajax.Request('genericSingleDBUpload',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getSchemas.bind(this)					
			});
		
	},
	
	_getSchemas:function(request){
		
		var response = request.responseText.evalJSON(true);
	
		//filling list
		for(var i=1;i<=response.length;i++){
			
			$("schema").options[i]=new Option(response[i-1].name,response[i-1].name);		
		}

		this.schemaListChange();
	},
	
	/*respond to schema list change event*/
	schemaListChange:function(){

		this.schema = $("schema").options[$("schema").selectedIndex].value;
		$("table").length=1;
		
		if(this.schema=='-1'){
			
			$("table").disabled = true;
			$("filetouploadbox").disabled = true;
			$("uploadandconfig").disabled = true;
			$("processfilebutton").disabled = true;
			$("tableconfig").innerHTML= '';
			
			this.readyToProcess=false;
			
		}else{
			this.getThematicUploadTables();
			$("table").disabled = false;
			$("filetouploadbox").disabled = true;
			$("uploadandconfig").disabled = true;
			$("processfilebutton").disabled = true;
			$("tableconfig").innerHTML= '';
			
			this.readyToProcess=false;
		}

	},
	
	/*respond to table list change event*/
	tableListChange:function(){

		this.table = $("table").options[$("table").selectedIndex].text;
		
		if(this.table=='-1'){
			
			$("filetouploadbox").disabled = true;
			$("uploadandconfig").disabled = true;
			$("processfilebutton").disabled = true;
			$("tableconfig").innerHTML= '';
			
			this.readyToProcess=false;
			
		}else{
			
			$("filetouploadbox").disabled = false;
			$("uploadandconfig").disabled = false;
			$("processfilebutton").disabled = true;
			$("tableconfig").innerHTML= '';
			
			this.readyToProcess=false;
			
		}
		
	},

/////THEMATIC SETTINGS - END

/////UPLOAD TABLE SETTINGS - START

	/*get thematic's upload table*/
	getThematicUploadTables:function(){
		
		this.params="action=getUploadTables&schema="+this.schema;

		new Ajax.Request('genericSingleDBUpload',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getThematicUploadTables.bind(this)					
			});
		
	},
	
	_getThematicUploadTables:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		//filling list
		for(var i=1;i<=response.length;i++){
			
			$("table").options[i]=new Option(response[i-1].name,response[i-1].id);		
		}

	},

/////UPLOAD TABLE SETTINGS - END

/////UPLOAD FUNCTIONS - START

	/*upload file */
	uploadFile:function(){
		
		this.prepareResultTable();
		
		//check if correct file was selected
		if(!this.validateUploadForm()){
			return;
		}

		//configure and send uploadform
		//$("uploadform").action="upload";
		$("uploadform").submit();
		
		this.createLoaderBox();
		this.viewLoadProcess();
	},
	
	/*validate upload form*/
	validateUploadForm:function(){
		
		var result = true;
		
		//var filename = $("filenamelist").options[$("filenamelist").selectedIndex].text;
		var filePath = $("filetouploadbox").value;
		
		var startIndex = filePath.length - 18;
			
		//check if correct file was selected
/*		if(filePath.indexOf("\\"+this.filetoupload, startIndex)==-1 && filePath.indexOf("/"+this.filetoupload, startIndex)==-1){
			alert("El archivo seleccionado no es v�lido.");
			$("filetouploadbox").value = "";
			result = false;
		}
*/		
		return result;
		
	},
	
	viewLoadProcess:function(){
		
		this.params="action=view";

		new Ajax.Request('upload',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._viewLoadProcess.bind(this)					
			});
	},
		
	_viewLoadProcess:function(request){

		if(request.responseText=="OK"){
			
			this.getUploadedFileColumnNames();
			
			$("processfilebutton").disabled = true;
			$("tableconfig").innerHTML= '';
			
			this.readyToProcess=false;
			
		}else if(request.responseText=="NO"){
			setTimeout("gdbUpload.viewLoadProcess()", 100);
		}else{
			this.removeLoaderBox();
			alert("Ocurri� un error mientras se intentaba subir el archivo, es posible que su sesi�n de usuario haya caducado, salga del sistema, vuelva a iniciar sesi�n e int�ntelo nuevamente");
			$("processfilebutton").disabled = true;
			$("tableconfig").innerHTML= '';
			
			this.readyToProcess=false;
		}
		
	},
/////UPLOAD FUNCTIONS - END

/////TABLE CONFIGURATION FUNCTIONS - START

	getUploadedFileColumnNames:function(){

		$('loadingText').innerHTML="Obteniendo nombres de columnas en el archivo subido...";

		this.params="action=getUploadedFileColumnNames&schema="+this.schema+"&table="+this.table;

		new Ajax.Request('genericSingleDBUpload',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getUploadedFileColumnNames.bind(this)					
			});
	},
	
	_getUploadedFileColumnNames:function(request){
		
		this.columns = request.responseText.evalJSON(true);
		
		if(this.columns){
			//location.href = "genericdbupload.jsp";
			this.getTableStructure();
		}else{
			this.removeLoaderBox();
			alert("Ocurri� un error mientras se intentaba recuperar informaci�n del servidor");

		}
		
	},

	getTableStructure:function(){

		$('loadingText').innerHTML="Obteniendo informaci�n de tabla en la base de datos...";

		this.params="action=getTableStructure&schema="+this.schema+"&table="+this.table;

		new Ajax.Request('genericSingleDBUpload',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getTableStructure.bind(this)					
			});
	},
	
	_getTableStructure:function(request){
		
		$('loadingText').innerHTML="Generando opciones de configuraci�n...";
		
		$("tableconfig").innerHTML= '';
					
		var response = request.responseText.evalJSON(true);

		if(response.length>0){
		
			var ctable;
			var tr;
			var td;
			var strong;
			
			var columnlist;
			
			//create configuration table
			ctable = document.createElement("table");
			ctable.border="0";
			ctable.align="center";
			ctable.cellpadding="3";			
			ctable.cellspacing="3";		
			ctable.setAttribute("class", "texttablas");
			ctable.setAttribute("className", "texttablas");
						
			tr= document.createElement("tr");
			tr.setAttribute("bgcolor","#FFFFFF");
			tr.setAttribute("height","30");
			
			td= document.createElement("td");
			strong = document.createElement("strong");
			strong.appendChild(document.createTextNode("COLUMNA"));
			td.appendChild(strong);
			td.align="center";
			tr.appendChild(td);

			td= document.createElement("td");
			strong = document.createElement("strong");
			strong.appendChild(document.createTextNode("TIPO"));
			td.appendChild(strong);
			td.align="center";
			tr.appendChild(td);
			
			td= document.createElement("td");
			strong = document.createElement("strong");
			strong.appendChild(document.createTextNode("TAMA�O"));
			td.appendChild(strong);
			td.align="center";
			tr.appendChild(td);

			td= document.createElement("td");
			strong = document.createElement("strong");
			strong.appendChild(document.createTextNode("NULO"));
			td.appendChild(strong);
			td.align="center";
			tr.appendChild(td);

			td= document.createElement("td");
			strong = document.createElement("strong");
			strong.appendChild(document.createTextNode("EQUIVALENCIA"));
			td.appendChild(strong);
			td.align="center";
			tr.appendChild(td);
			
			ctable.appendChild(tr);	
			
			this.tColumnsCount = response.length;
			
			//create rows for each table's column
			
			for(var i = 0; i<response.length; i++){
				
				tr= document.createElement("tr");
				tr.setAttribute("height","30");
				
				if((i+1)%2==0){
					tr.setAttribute("bgcolor","#FFFFFF");
				}
			
				td= document.createElement("td");
				td.appendChild(document.createTextNode(response[i].colname));
				tr.appendChild(td);

				td= document.createElement("td");
				td.appendChild(document.createTextNode(response[i].datatype));
				tr.appendChild(td);				

				td= document.createElement("td");
				td.appendChild(document.createTextNode(response[i].datalenght));
				td.align="center";
				tr.appendChild(td);	
				
				td= document.createElement("td");
				td.appendChild(document.createTextNode(response[i].nullable));
				td.align="center";
				tr.appendChild(td);	

				//create list
				columnlist = document.createElement("select");
				columnlist.name = "col"+i;
				columnlist.id = "col"+i;
				
				//fill list
				this.createColumnList(columnlist,response[i].nullable);			

				td= document.createElement("td");
				td.align="center";
				td.appendChild(columnlist);
				tr.appendChild(td);	

				ctable.appendChild(tr); 
			}
			

			$("tableconfig").appendChild(ctable);
			$("tableconfig").innerHTML = $("tableconfig").innerHTML;

			$("processfilebutton").disabled = false;
			this.readyToProcess=true;
			
		}else{
			alert("Ocurri� un error mientras se intentaba recuperar informaci�n del servidor");
			location.href = "genericdbupload.jsp";
		}
		this.removeLoaderBox();
	},
	
	/*appends values to user lists*/
	createColumnList:function(list, nullable){
				
		if(nullable=="Y"){
			
			list.options[0] = new Option("Seleccione una opci�n", "-1");
			
			for(var i=1 ; i<=this.columns.length; i++){
				list.options[i] = new Option(this.columns[i-1].name, this.columns[i-1].value);
			}
			
		}else{
				
			for(var i=0 ; i<this.columns.length; i++){
				list.options[i] = new Option(this.columns[i].name, this.columns[i].value);
			}

		}			
		
	},
	
	/*request the server to create new file from users customization*/
	configureFileToUpload:function(){
		
		var queryString = '';
		var selName = null;
		
		this.createLoaderBox('Configurando archivo a subir seg�n criterios de usuario...');
		
		//create params from user lists
		for(var i = 0; i<this.tColumnsCount; i++){
			selectName = "col"+i;
			
			if(i>0){
				queryString = queryString+"&";
			}
			
			queryString = queryString + selectName + "=" + $(selectName).options[$(selectName).selectedIndex].value;
		}

		this.params="action=configureFileToUpload&schema="+this.schema+"&"+queryString;

		new Ajax.Request('genericSingleDBUpload',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._configureFileToUpload.bind(this)					
			});
		
	},
	
	_configureFileToUpload:function(request){
		
		var response = parseInt(request.responseText);
		
		this.uploadedRows = response;
		
		if(response>0){
			this.createCustomControlFile();
		}else{
			alert("Ocurri� un error mientras se intentaba recuperar informaci�n del servidor");
			//location.href = "genericdbupload.jsp";
		}
		
	},
	
	createCustomControlFile:function(){
		
		$('loadingText').innerHTML="Creando archivo de control...";
		
		this.params="action=createCustomControlFile&schema="+this.schema+"&table="+this.table;

		new Ajax.Request('genericSingleDBUpload',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._createCustomControlFile.bind(this)					
			});
		
	},
	
	_createCustomControlFile:function(request){
		
		var response = request.responseText;
		
		if(response=='OK'){
			this.processUploadedFile();
			//this.removeLoaderBox();
		}else{
			alert("Ocurri� un error mientras se intentaba recuperar informaci�n del servidor");
			//location.href = "genericdbupload.jsp";
		}
		
	},
	
	
	processFile:function(){
		
		this.prepareResultTable();
		
		if(this.readyToProcess){			
			this.configureFileToUpload();
		}else{
			alert("Debe realizar todos los pasos antes de finalizar el proceso");
		}

		
	},

/////TABLE CONFIGURATION FUNCTIONS - END

/////PROCESS UPLOADED FILE - START

	processUploadedFile:function(){

		$('loadingText').innerHTML="Guardando informaci�n en la base de datos...";
		
		this.params="action=processFile&schema="+this.schema+"&table="+this.table+"&urows="+this.uploadedRows;

		new Ajax.Request('genericSingleDBUpload',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._processUploadedFile.bind(this)					
			});

	},
	
	_processUploadedFile:function(request){

		$('loadingText').innerHTML="Finalizando tarea...";

		var response = request.responseText.evalJSON(true);
		
		//config results options
		if (response.filestatus==1){
			$('totalrows').appendChild(document.createTextNode(response.totalrows));
			$('processok').style.cssText = "display:block";
			$('uploadoptions').style.cssText = "display:none";
		}else if(response.filestatus>1){
			$('processerror').style.cssText = "display:block";
			//configure links
			$('generatedfiles').style.cssText = "display:block";
			$('logfileoptions').style.cssText = "display:block";
			$('linklogfile').href=response.logfile;
		}
						
		//shows table results
		$('resultstable').style.cssText = "display:block";
		
		//show generated files
		if (response.filestatus==2){
			
			$('linkbadfile').href=response.badfile;
			$('linkdiscardfile').href=response.discardfile;
			$('badfileoptions').style.cssText = "display:block";
			$('discardfileoptions').style.cssText = "display:block";
			
		}else if(response.filestatus==3){
			$('linkbadfile').href=response.badfile;
			$('badfileoptions').style.cssText = "display:block";
			
		}else if(response.filestatus==4){
			
			$('linkdiscardfile').href=response.discardfile;
			$('discardfileoptions').style.cssText = "display:block";
			
		}
		
		setTimeout("gdbUpload.removeLoaderBox()", 300);
	},

	prepareResultTable:function(){
		
		$('processok').style.cssText = "display:none";

		$('processerror').style.cssText = "display:none";
		
		$('generatedfiles').style.cssText = "display:none";

		//shows table results
		$('resultstable').style.cssText = "display:none";
		
		$('linklogfile').href="";
		$('linkbadfile').href="";
		$('linkdiscardfile').href="";
		
		$('logfileoptions').style.cssText = "display:none";
		$('badfileoptions').style.cssText = "display:none";
		$('discardfileoptions').style.cssText = "display:none";
		
	},

/////PROCESS UPLOADED FILE - END
	
/////UTIL FUNCTIONS - START		
	
	/*util function*/
	noCache:function(uri){
		
		return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
	},
	
	createToolTips:function(){
		    
		new Control.Modal('logfilehelp',{
		contents: 'Contiene el historial que describe<br/>el procesamiento del archivo',
		position: 'mouse', 
		offsetTop: 20
	    });
		
		new Control.Modal('badfilehelp',{
		contents: 'Contiene aquellos registros que causaron<br/>problemas durante el proceso o<br/>cuyo formato era incorrecto',
		position: 'mouse', 
		offsetTop: 20
	    });

		new Control.Modal('discardfilehelp',{
		contents: 'Contiene aquellos registros que fueron descartados<br/>y por ello no fueron subidos a la base de datos.',
		position: 'mouse', 
		offsetTop: 20
	    });
		
	},
	
	createLoaderBox:function(msg){

		var loadingContent = document.createElement('div');
		loadingContent.id	= 'loadingContent';
	
		var loadingText=document.createElement('div');
		loadingText.id	= 'loadingText';
		if(msg){
			loadingText.innerHTML=msg;
		}else{
			loadingText.innerHTML="Subiendo archivo al servidor...";			
		}

		
		var loadingImg=document.createElement('IMG');
		loadingImg.src="images/ajax-loader.gif";
		
		loadingContent.appendChild(loadingText);
		loadingContent.appendChild(loadingImg);
		
		this.loadingWindow=new Control.Modal(false,{
			contents: loadingContent.innerHTML,
			overlayCloseOnClick: false
			});

		this.loadingWindow.open();

	},
	
	removeLoaderBox:function(){
		
		this.loadingWindow.close();
		
	},
	
	restartAssistant:function(){
		
		location.href="infoassistant.jsp";
		
	}
		
});

Event.observe(window, 'load',function(event){
			gdbUpload = new GenericDBUpload();									  
	});
	
