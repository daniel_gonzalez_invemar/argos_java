var userAdmin;

var UserAdmin = Class.create({
	
	initialize:function(){
		
		this.params='';
		this.loadingWindow=null;
		this.userId = null;
		this.users = 0;
		this.removeDetailId = null;
		
		$('search').observe('click',this.searchUserInformation.bindAsEventListener(this));
		$('searchtext').observe('click',this.searchTextChange.bindAsEventListener(this));
	
		this.getProjects();
		
		$('project').observe('change',this.getUserInformation.bindAsEventListener(this));
				
	},

/////USER ADMIN - START

	getProjects:function(){
		
		this.createLoaderBox("Obteniendo informaci\xF3n de usuarios...");
		
		this.params="action=getProjects";

		new Ajax.Request('userAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getProjects.bind(this)					
			});
			
	},
	
	_getProjects:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		for(var i=0;i<=response.length-1;i++){
				
			$("project").options[i]=new Option(response[i].name,response[i].id);		
				
		}
		
		this.removeLoaderBox();
		this.getUserInformation();
		
	},

	getUserInformation:function(){
		
		this.createLoaderBox("Obteniendo informaci\xF3n de usuarios...");
		
		var projectId = $("project").options[$("project").selectedIndex].value;
		
		this.params="action=getUserInformation&projectid="+projectId;

		new Ajax.Request('userAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getUserInformation.bind(this)					
			});
		
	},
	
	_getUserInformation:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		this.emptyTable();
		
		for(var i=0;i<=response.length-1;i++){
				
			this.createUserRow(response[i].id,response[i].name, response[i].email,response[i].active);
				
		}
		
		if (response.length>0)
			var abop = new ABOP("usertbody", 7);
		
		this.removeLoaderBox();
	},
	
	searchTextChange:function(){
		
		if($("searchtext").value == "deje en blanco para traer todos los usuarios"){
			$("searchtext").value="";
		}
		
	},
	
	searchUserInformation:function(){
		
		this.createLoaderBox("Obteniendo informaci\xF3n de usuarios...");
		
		var searchOption = $("searchoption").options[$("searchoption").selectedIndex].value;
		var paramOptions = null;
		
		if(searchOption==1){
			paramOptions="&name="+$("searchtext").value+"&email=null";
		}else{
			paramOptions="&name=null&email="+$("searchtext").value;
		}
		
		this.params="action=searchUserInformation"+paramOptions;

		new Ajax.Request('userAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getUserInformation.bind(this)					
			});
		
	},
	
	/*change user status*/
	changeStatus:function(userId, newStatus){
		
		this.createLoaderBox("Actualizando informaci\xF3n de usuario...");
		
		this.params="action=changeUserStatus&userid="+userId+"&newstatus="+newStatus;

		new Ajax.Request('userAdmin',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._changeStatus.bind(this)					
			});
			
	},

	_changeStatus:function(request){
		
		var responseText = request.responseText;
		var resultOk;

		if(responseText=="NOP"){
			alert("Usted no tiene privilegios suficientes para modificar la información de éste usuario");
		}else if(responseText=="NO"){
			alert("No fue posible actualizar la información de usuario");
		}else{
			
			resultOk = responseText.split("_");
			
			//change status image
			if(resultOk[1]=="S"){
				$("status_"+resultOk[0]).src="images/active.gif";
				$("astatus_"+resultOk[0]).href = "javascript:userAdmin.changeStatus("+resultOk[0]+",'N');";
			}else if(resultOk[1]=="N"){
				$("status_"+resultOk[0]).src="images/inactive.gif";
				$("astatus_"+resultOk[0]).href = "javascript:userAdmin.changeStatus("+resultOk[0]+",'S');";				
			}

		}
		
		this.removeLoaderBox();
	},
	
/////USER ADMIN - END

/////DETAIL TABLE - START

	createUserRow:function(userId, name, email, active){

		var tbody = $("usertbody");	
		var tr;
		var td;
		var img;
		var a;
		var newStatus;
		var imgSrc;

		tr= document.createElement("tr");

		if((this.users+1)%2==0){
			tr.setAttribute("bgColor", "#FFFFFF");
		}
		
		td= document.createElement("td");
		td.appendChild(document.createTextNode(email));
		tr.appendChild(td);
			
		td= document.createElement("td");
		td.appendChild(document.createTextNode(name));
		tr.appendChild(td);

		//registration
		a = document.createElement("a");
		a.href = "userreg.jsp?action=editgi&userid="+userId+"&email="+email;
		img = document.createElement("img");
		img.src = "images/registration.gif";
		img.setAttribute("border","0");
		
		a.appendChild(img);
		
		td= document.createElement("td");
		td.setAttribute("align", "center");
		td.appendChild(a);
		tr.appendChild(td);

		//details
		a = document.createElement("a");
		a.href = "userdetail.jsp?userid="+userId+"&email="+email;
		img = document.createElement("img");
		img.src = "images/detail.gif";
		img.setAttribute("border","0");
		
		a.appendChild(img);
		
		td= document.createElement("td");
		td.setAttribute("align", "center");
		td.appendChild(a);
		tr.appendChild(td);

		//roles

		a = document.createElement("a");
		a.href = "userrole.jsp?userid="+userId+"&email="+email;
		img = document.createElement("img");
		img.src = "images/role.gif";
		img.setAttribute("border","0");
		
		a.appendChild(img);
		
		td= document.createElement("td");
		td.setAttribute("align", "center");
		td.appendChild(a);
		tr.appendChild(td);
		
		//activate
		
		if(active=="S"){
			newStatus="N";
			imgSrc = "images/active.gif";
		}
		else{
			newStatus="S";
			imgSrc = "images/inactive.gif";
		}
		
		a = document.createElement("a");
		a.id="astatus_"+userId;
		a.href = "javascript:userAdmin.changeStatus("+userId+",'"+newStatus+"');";
		img = document.createElement("img");
		img.id="status_"+userId;
		img.src = imgSrc;
		img.setAttribute("border","0");
		
		a.appendChild(img);
		
		td= document.createElement("td");
		td.setAttribute("align", "center");
		td.appendChild(a);
		tr.appendChild(td);		
	
		tbody.appendChild(tr);
		
		this.users = this.users+1;
		
	},

/////DETAIL TABLE - END

	emptyTable:function(){
		
		var tbodyRows = $("usertbody").getElementsByTagName("tr");
		var tbodylength = tbodyRows.length -1;
		
		for(tbodylength; tbodylength>=0 ; tbodylength--){
			$("usertbody").removeChild(tbodyRows[tbodylength]);
		}
		
		$("ABOPndiv_usertbody").innerHTML="";
		
		this.users = 0;
		
	},
	
	createLoaderBox:function(lText){

		var loadingContent = document.createElement('div');
		loadingContent.id	= 'loadingContent';
	
		var loadingText=document.createElement('div');
		loadingText.id	= 'loadingText';
		
		if(lText){
			loadingText.innerHTML=lText;
		}else{
			loadingText.innerHTML="Verificando credenciales de usuario...";			
		}

		
		var loadingImg=document.createElement('IMG');
		loadingImg.src="images/ajax-loader.gif";
		
		loadingContent.appendChild(loadingText);
		loadingContent.appendChild(loadingImg);
		
		this.loadingWindow=new Control.Modal(false,{
			contents: loadingContent.innerHTML,
			overlayCloseOnClick: false
			});

		this.loadingWindow.open();

	},
	
	removeLoaderBox:function(){
		
		this.loadingWindow.close();
		
	},
	
	/*util function*/
	noCache:function(uri){
		
		return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
	}
		
});

Event.observe(window, 'load',function(event){
			userAdmin = new UserAdmin();									  
	});
	
