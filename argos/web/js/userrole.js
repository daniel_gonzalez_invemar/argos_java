var userRole;

var UserRole = Class.create({
	
	initialize:function(){
		
		this.params='';
		this.loadingWindow=null;
		this.userId = null;
		this.details = 0;
		this.removeRoleId = null;
		
		//get query params * never believed it, javascript can do it! amazing! :P
		var prmeter=location.href.toQueryParams();
		
		$('route').href = $('route').href + "?userid="+prmeter.userid+"&email="+prmeter.email;
		$('refuseremail').innerHTML = prmeter.email;
				
		this.userId=prmeter.userid;
		
		$('project').observe('change',this.getProjectRoles.bindAsEventListener(this));
		$('associate').observe('click',this.saveRole.bindAsEventListener(this));

		this.checkUserEditionPermission();
				
	},

/////USER DETAILS - START

	checkUserEditionPermission:function(){
		
		this.createLoaderBox("Obteniendo informaci\xF3n de usuario...");
		
		this.params="action=checkUserEditionPermission&userid="+this.userId;

		new Ajax.Request('userRole',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._checkUserEditionPermission.bind(this)					
			});
			
	},
	
	_checkUserEditionPermission:function(request){
		
		response = request.responseText;
		
		if(response=="OK"){
			this.getUserRoles();
		}else{
			this.removeLoaderBox();
			alert("Usted no tiene privilegios suficientes para modificar la informaci�n de �ste usuario");
			location.href="index.jsp";
		}
		
	},
	
	getUserRoles:function(){
				
		this.params="action=getUserRoles&userid="+this.userId;

		new Ajax.Request('userRole',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getUserRoles.bind(this)					
			});
	},
	
	_getUserRoles:function(request){
		
		var result = request.responseText;
		var response = request.responseText.evalJSON(true);
		
		if(result != "NO"){
			//filling list
			for(var i=0;i<=response.length-1;i++){
				
				this.createRoleRow(response[i].projectname, response[i].roleid, response[i].rolename);

			}
		}else{
			alert("No fue posible obtener la informaci�n de usuario");
		}
		
		this.getProjects();
		
	},

	getProjects:function(){
		
		this.params="action=getProjects";

		new Ajax.Request('userRole',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getProjects.bind(this)					
			});
			
	},
	
	_getProjects:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		for(var i=0;i<=response.length-1;i++){
				
			$("project").options[i]=new Option(response[i].name,response[i].id);		
				
		}
		
		this.removeLoaderBox();
		this.getProjectRoles();
		
	},

	getProjectRoles:function(){
		
		$("role").length=0;
		
		var projectId = $("project").options[$("project").selectedIndex].value;		
		
		this.params="action=getProjectRoles&projectid="+projectId;

		new Ajax.Request('userRole',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._getProjectRoles.bind(this)					
			});
			
	},
	
	_getProjectRoles:function(request){
		
		var response = request.responseText.evalJSON(true);
		
		for(var i=0;i<=response.length-1;i++){

			$("role").options[i]=new Option(response[i].name,response[i].id);	
				
		}
		
	},
	
	 deleteRole:function(roleId){
		
		var conf = confirm("�Est� seguro que desea remover �ste rol del usuario?");
		
		if(!conf)
			return;
		
		this.createLoaderBox("Actualizando informaci\xF3n de usuario...");
		
		this.removeRoleId =roleId;
		
		this.params="action=removeUserRole&userid="+this.userId+"&roleid="+roleId;

		new Ajax.Request('userRole',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._deleteRole.bind(this)					
			});
			
	},
	
	_deleteRole:function(request){
		
		response = request.responseText;
		
		this.removeLoaderBox();
		
		if(response=="OK"){
			
			this.details = this.details-1;
					
			$("detailtbody").removeChild($(this.removeRoleId+"_row"));
			
			alert("Rol del usuario removido satisfactoriamente");
			
		}else if(response=="NO"){
			alert("No fue posible remover el rol del usuario");
		}
		
	},	

	saveRole:function(){
		
		var roleId = $("role").options[$("role").selectedIndex].value;		
		var projectId = $("project").options[$("project").selectedIndex].value;
		
		this.createLoaderBox("Guardando informaci\xF3n de usuario...");
		
		this.params="action=saveUserRole&userid="+this.userId+"&roleid="+roleId+"&projectid="+projectId;

		new Ajax.Request('userRole',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._saveRole.bind(this)					
			});
			
	},
	
	_saveRole:function(request){
		
		var response = request.responseText;
		
		this.removeLoaderBox();
		
		if(response=="OK"){
			
			//create detail row
			var projectName = $("project").options[$("project").selectedIndex].text;
			var roleId = $("role").options[$("role").selectedIndex].value;	
			var roleName = $("role").options[$("role").selectedIndex].text;	
			
			this.createRoleRow(projectName, roleId, roleName);
			
			alert("Rol asociado satisfactoriamente");
			
		}else if(response=="NO"){
			alert("No fue posible guardar la informaci�n del usuario");
		}else if(response=="NOR"){
			alert("Este usuario actualmente cuenta con un rol asociado para el proyecto seleccionado, no es posible asociar m�s de un rol para un proyecto");
		}
		
		
		
	},
	
 	updateDetail:function(detailId){
		
		var detailText = $(detailId+"_text").value;
		
		this.createLoaderBox("Actualizando informaci\xF3n de usuario...");
		
		this.params="action=updateUserDetail&userid="+this.userId+"&detailid="+detailId+"&detailtext="+detailText;

		new Ajax.Request('userDetail',{
				method:"post",				
				parameters:this.noCache(this.params),
				onSuccess:this._updateDetail.bind(this)					
			});
			
	},
	
	_updateDetail:function(request){
		
		response = request.responseText;
		
		this.removeLoaderBox();
		
		if(response=="OK"){
			alert("Informaci�n de usuario actualizada satisfactoriamente");
		}else{
			alert("No fue posible actualizar la informaci�n de usuario");
		}
		
	},
	


/////USER DETAILS - END

/////ROLE TABLE - START

	createRoleRow:function(projectName, roleId, roleName){
		
		var tr;
		var td;
		var textarea;
		var img;
		var div;
		var a;
		
		tr= document.createElement("tr");
		tr.id=roleId+"_row";
				
		if((this.details+1)%2==0){
			tr.setAttribute("bgColor", "#FFFFFF");
		}
		
		td= document.createElement("td");
		td.appendChild(document.createTextNode(projectName));
		tr.appendChild(td);
		
		td= document.createElement("td");
		td.appendChild(document.createTextNode(roleName));
		tr.appendChild(td);
				
		//delete option
		a = document.createElement("a");
		a.href = "javascript:userRole.deleteRole("+roleId+")";
		img = document.createElement("img");
		img.src = "images/delete.gif";
		img.setAttribute("border","0");
		
		a.appendChild(img);
		
		td= document.createElement("td");
		td.setAttribute("align", "center");
		td.appendChild(a);
		tr.appendChild(td);
		
		$("detailtbody").appendChild(tr);
		
		this.details = this.details+1;
	},

/////DETAIL TABLE - END
	
	createLoaderBox:function(lText){

		var loadingContent = document.createElement('div');
		loadingContent.id	= 'loadingContent';
	
		var loadingText=document.createElement('div');
		loadingText.id	= 'loadingText';
		
		if(lText){
			loadingText.innerHTML=lText;
		}else{
			loadingText.innerHTML="Verificando credenciales de usuario...";			
		}

		
		var loadingImg=document.createElement('IMG');
		loadingImg.src="images/ajax-loader.gif";
		
		loadingContent.appendChild(loadingText);
		loadingContent.appendChild(loadingImg);
		
		this.loadingWindow=new Control.Modal(false,{
			contents: loadingContent.innerHTML,
			overlayCloseOnClick: false
			});

		this.loadingWindow.open();

	},
	
	removeLoaderBox:function(){
		
		this.loadingWindow.close();
		
	},
	
	/*util function*/
	noCache:function(uri){
		
		return uri.concat("&noCache=",(new Date).getTime(),".",Math.random()*1234567);
	}
		
});

Event.observe(window, 'load',function(event){
			userRole = new UserRole();									  
	});
	
