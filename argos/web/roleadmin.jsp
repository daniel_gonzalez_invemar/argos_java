<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true" import="co.org.invemar.user.UserPermission"%>
<%
		String username = null;
		UserPermission userp = null;
		String projectid = null, projectname=null, projecttitle=null;
		String CSS="siamcss.css";
		
    	try{
        	username = session.getAttribute("username").toString();
        	projectid = session.getAttribute("projectid").toString();
        	projectname = session.getAttribute("projectname").toString();
        	projecttitle = session.getAttribute("projecttitle").toString();
			CSS=(String)session.getAttribute("CSSPRO");
        	   		
    	}catch(Exception e){
		 %>
    		<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}
    	
    	if(username==null){
		 %>
			<script type="text/javascript">location.href="login.jsp"</script>
		 <%
    	}else{
			//check user permission
			userp = (UserPermission)session.getAttribute("userpermission");
			
			if(!userp.couldUser(UserPermission.ADMINISTRAR_ROLES)){
					%>
						<script type="text/javascript">location.href="index.jsp"</script>
		 			<%
			}
			
		}
try{
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="this is my page">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="css/<%=CSS %>" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/cmar.css" type="text/css">
  <link title="winter" href="js/lib/jscalendar/calendar-blue.css" media="all" type="text/css" rel="stylesheet"/>
  <script type="text/javascript" src="js/lib/prototype.js"></script>
  <script type="text/javascript" src="js/lib/scriptaculous/scriptaculous.js?load=effects"></script>
  <script type="text/javascript" src="js/lib/controlmodal.js"></script>
  <script type="text/javascript" src="js/lib/abop.js"></script>
  <script type="text/javascript" src="js/roleadmin.js"></script>
</head>

<body marginwidth="0" marginheight="0">

<%@ include file="bannerp.jsp" %>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr class="tabla_fondotitulo">
      <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
      <td width="364" class="linksnegros">Administrar roles </td>
      <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>
    </tr>
  </tbody>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="750" border="0" align="center" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0"
        cellspacing="0">
          <tbody>
            <tr>
              <td background="images/dotted_line.gif"><img
                src="images/dotted_line.gif" width="12" height="3"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/spacer.gif" width="3" height="3"></td>
    </tr>
  </tbody>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
  <tbody>
	<tr class="tabla_fondotitulo2">
      <td width="9" >&nbsp;</td>
      <td width="503" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp;<a href="index.jsp">Inicio</a> &gt; <a href="roleadmin.jsp">Administrar roles </a> </td>
      <td width="238" class="texttablas"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#E6E6E6"><p align="justify" class="texttablas"><br>
          </p>
      </td>
    </tr>
    <tr>
      <td colspan="3" ><table id="uploadoptions" width="97%" border="0" align="center" cellpadding="0" cellspacing="0" class="texttablas">
          <tr>
            <td width="100%"><div align="justify">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="texttablas" id="uploadoptions">
                  <tr>
                    <td width="100%"><div align="justify">
                        <p>Seleccione proyecto:<a name="editionpoint"></a>                          <br>
                          <select name="project" id="project">
                            <option value="-1" selected>Seleccione proyecto</option>
                          </select>
                        </p>
                      </div>                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Roles para el proyecto seleccionado:</td>
                  </tr>
                  <tr>
                    <td>&nbsp; </td>
                  </tr>
                  <tr>
                    <td>&nbsp;<div id="ABOPndiv_roletbody"></div></td>
                  </tr>
                  <tr>
                    <td>                    </td>
                  </tr>
                  <tr>
                    <td><table id="roletable" width="79%" border="0" align="center" cellpadding="2" cellspacing="3" class="texttablas">
                      <thead>
                        <tr bgcolor="#FFFFFF">
                          <td width="78%">Rol</td>
                          <td width="12%" align="center">Editar</td>
                          <td width="10%" align="center">Eliminar</td>
                        </tr>
                      </thead>
                      <tbody id="roletbody">
                      </tbody>
                    </table></td>
                  </tr>
                  <tr>
                    <td>&nbsp;                    </td>
                  </tr>
                </table>
              </div>            </td>
          </tr>
          <tr>
            <td><hr></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><strong>Crear nuevo rol:</strong></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Seleccione proyecto:<br>              <select name="project_new" id="project_new">
                                    </select>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Nombre: </td>
          </tr>
          <tr>
            <td><input name="name_new" type="text" id="name_new" size="50" maxlength="100"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Seleccione los permisos para el nuevo rol: </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><table width="52%" border="0" align="center" cellpadding="0" cellspacing="3" class="texttablas">
              <tr>
                <td width="46%"><div align="left">Permisos existentes:<br><div align="right">
                      <select name="permlist_new" size="10" id="permlist_new" style="width:345px;">
                      </select></div>
                </div></td>
                <td width="10%"><input name="addp_new" type="button" id="addp_new" value="&gt;&gt;">
                  <input name="removep_new" type="button" id="removep_new" value="&lt;&lt;"></td>
                <td width="44%">Permisos asignados:<br><select name="spermlist_new" size="10" id="spermlist_new" style="width:345px;">
                                                </select></td>
              </tr>
            </table>
            <br>
            <input name="createrol" type="submit" id="createrol" value="Crear rol"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="3" ><p align="justify">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<%@ include file="footer.jsp" %>

<div id="editionbox" style="display: none; z-index:1; position: absolute; top: 197px; left:123px; margin: 10px; padding: 0px; border-width: 1px; width: 327px; height: 130px;">
  <table width="20%" border="1" align="center" cellpadding="5" cellspacing="0" bgcolor="#D9ECEC" bordercolor="#000000">
    <tr>
      <td><table width="31%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#D9ECEC" class="texttablas" id="uploadoptions">
          <tr>
            <td width="100%">Nombre: </td>
          </tr>
          <tr>
            <td><input name="name_edit" type="text" id="name_edit" size="50" maxlength="100">
                <input name="updateroledescp" type="submit" id="updateroledescp" value="Actualizar nombre">
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>Seleccione los permisos para el nuevo rol: </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><table width="52%" border="0" align="center" cellpadding="0" cellspacing="3" class="texttablas">
                <tr>
                  <td width="46%"><div align="left">Permisos existentes:<br>
                          <div align="right">
                            <select name="permlist_edit" size="10" id="permlist_edit" style="width:340px;">
                            </select>
                          </div>
                    </div>
                  </td>
                  <td width="10%"><input name="addp_edit" type="button" id="addp_edit" value="&gt;&gt;">
                      <input name="removep_edit" type="button" id="removep_edit" value="&lt;&lt;">
                  </td>
                  <td width="44%">Permisos asignados:<br>
                      <select name="spermlist_edit" size="10" id="spermlist_edit" style="width:340px;">
                      </select>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td><div align="center"><a href="javascript:roleAdmin.closeRoleEditor();">Cerrar</a></div></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>
</body>
</html>
<%
}catch(Exception e){}
%>