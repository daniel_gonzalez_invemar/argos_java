<%@page import="co.org.invemar.siam.sibm.vo.Proyecto"%>
<%@page import="java.util.List"%>
<%@page import="co.org.invemar.siam.sibm.model.ProyectosDM"%>
<%@page import="java.sql.Connection"%>
<%@page import="co.org.invemar.util.ConnectionFactory"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true" %>
<%

    String username = null;
    String idProyect =request.getParameter("p");   
    String proyectName ="SISTEMA DE SOPORTE MULTITEMÁTICO PARA EL MONITOREO AMBIENTAL";
    
    
 try {       
        if (idProyect==null) {           
            idProyect = "0";
        } else {
            ConnectionFactory conectionfactory = new ConnectionFactory();
            Connection con = conectionfactory.createConnection("geograficos");
            ProyectosDM proyect = new ProyectosDM();
            List<Proyecto> proyectList =  proyect.findProjectsPorCodigo(con, Long.parseLong(idProyect));
            proyectName = proyectList.get(0).getTitulo();
            con.close();
            con=null;
        }

    } catch (Exception e) {
        
    }
    
    
    try {
        username = session.getAttribute("username").toString();     

    } catch (Exception e) {
        
    }


%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="favicon.ico">
        <title>Argos</title>       
        <link href="responsive/css/bootstrap.min.css" rel="stylesheet">    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="responsive/js/bootstrap.min.js"></script>
        <link href="responsive/css/cover.css" rel="stylesheet">
        <link href="responsive/css/log.css" rel="stylesheet">
        <link href="responsive/css/color.css" rel="stylesheet">
        <link href="responsive/css/layout.css" rel="stylesheet">
        <script type="text/javascript" src="js/loginNew.js"></script>  
        <script type="text/javascript">
            $(document).ready(function() {
            <%                   String pAction = request.getParameter("action");
                String uid = request.getParameter("uid");
                String email = request.getParameter("email");
                String ckey = request.getParameter("ckey");

            %>



                var parameterAction = '<%=pAction%>';
                var uid = '<%=uid%>';
                var email = '<%=email%>';
                var ckey = '<%=ckey%>';
                var idProyect ='<%=idProyect%>';

                hideHeaderAndFooter(idProyect);

                if (parameterAction == 'cpassgen') {
                    confirmPasswordRegeneration(uid, email, ckey);
                }

            });

        </script>

    </head>

    <body >
        <!-- header -->
        <header id="mainHeader" class="navbar-fixed-top" role="banner">
            <div class="container">
                <nav class="navbar navbar-default scrollMenu" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="index.html"><img src="http://siam.invemar.org.co/siam/plantillaSitio/img/logowebinvemar.png" alt="Invemar" height="100px"/></a> 
                    </div>
                    <div class="collapse navbar-collapse navbar-ex1-collapse" id="scrollTarget">
                        <ul class="nav navbar-nav pull-right">
                            <li><a href="http://siam.invemar.org.co/siam/index.jsp"><i class="fa fa-home"></i>Inicio</a> </li>
                            <li><a href="http://www.invemar.org.co/archivo.jsp?id=4530&red=true"><i class="fa fa-info-circle"></i>Desarrollo <br>Conceptual del SIAM</a> </li>
                            <li><a href="http://www.invemar.org.co/psubcategorias.jsp?idsub=182&idcat=104"><i class="fa fa-cogs"></i>LABSIS</a> </li>

                        </ul>


                    </div>

                </nav>
            </div>
            <section id="top" >

                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 hidden-sm hidden-xs">
                            <ul class="nav navbar-nav nb-n1 pull-right">

                                <li></li>
                            </ul>
                            <ul class="nav navbar-nav pull-left">
                                <li><a href="http://siam.invemar.org.co/siam/mapadelsitio.jsp">Mapa del sitio</a> </li>
                                <li><a href="http://www.invemar.org.co/pciudadania.jsp">Servicio al ciudadano</a> </li>
                                <li><a href="https://spreadsheets.google.com/embeddedform?formkey=dHB1ZHdIVk5nbEs0VlRNaW9VdjU2X0E6MA">Díganos que piensa</a> </li>
                                <li><a href="http://siam.invemar.org.co/siam/contactenos.jsp?idsitio=10">Contacto </a></li>

                            </ul>
                        </div>
                    </div>
                </div>

            </section>
        </header>
        <!-- header -->






        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                    <h1 class="text-center login-title"><%=proyectName%></h1>
                    <div class="account-wall">
                        <img class="profile-img" src="imagenAction?codigo=<%=idProyect%>" alt="Logo Proyecto">
                        <form class="form-signin" name="loginform" method="post" action="">
                            <input type="text" class="form-control" placeholder="Email" name="email" id="email" value="sigmatr@corpouraba.gov.co" required autofocus />
                            <span id="errorsDiv_email"/>
                            <input name="password" id="password"  type="password" class="form-control"  value="billetico" placeholder="Contraseña" required/>
                            <span id="errorsDiv_password"/>
                            <button class="btn btn-lg btn-block" type="button" name="login" id="login"   title="Ingresar" onclick="controlAcceso()"  >
                                Ingresar</button>                                                       
                            <div class="form-group">                                
                                <a href="#" class="pull-right need-help" onclick="showPasswordChangeDialog()" >Olvide mi contraseña </a><br/>
                            </div> 
                            <div class="form-group">                                
                                <span class="clearfix" id="message"></span>
                            </div> 

                        </form>
                    </div>
                    <!--a href="#" class="text-center new-account">Regresar </a-->&nbsp;<p>
                </div>
            </div>

        </div>

        <div class="modal fade" id="myModalProyectos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Seleccione el proyecto</h4>
                    </div>
                    <div class="modal-body">
                        <div class="btn-group">
                            <button class="btn btn-default" id="opcionprincipal" >Seleccione...</button> 
                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="caret"></span></button>
                            <ul class="dropdown-menu" id="listaproyectos" style="text-align: justify">                                
                            </ul>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="seleccionarPreyecto()">Seleccionar</button>

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="myChangePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Olvid&eacute; mi contrase&ntilde;a!</h4>
                    </div>
                    <div class="modal-body">                       
                        <form role="form">
                            <div class="form-group" style="color:black">
                                <label for="diremail" style="text-align:justify" >Correo electronico:</label>
                                <input type="email" class="form-control" id="diremail" placeholder="correo electronico">
                            </div> 
                            <div class="form-group" style="color:black">                                
                                <p class="alert" id="result"></p>
                            </div>   
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"  onclick="changePassword()">Generar nueva contrase&ntilde;a!</button>

                    </div>
                </div>
            </div>
        </div>



        <!-- footer -->
        <footer>
            <section id="mainFooter">
                <div class="container" id="footer">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="footerWidget">
                                <img src="http://siam.invemar.org.co/siam/plantillaSitio/img/logowebinvemar.png" alt="Invemar" id="footerLogo" width="20%" height="20%" >
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="footerWidget">

                                <h3>Invemar</h3>
                                <address>
                                    <p> <i class="icon-location"></i>&nbsp;Calle 25 No. 2-55, Playa Salguero<br>
                                        Santa Marta D.T.C.H., Colombia <br>
                                        <i class="icon-phone"></i>&nbsp;615.987.1234 <br>
                                        <i class="icon-mail-alt"></i>&nbsp;<a href="mailto:webmaster@invemar.org.co">webmaster@invemar.org.co</a> </p>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </footer>
        <section  id="footerRights">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Copyright © 2014 <a href="http://www.invemar.org.co" target="blank">Invemar</a> / All rights reserved.</p>
                    </div>
                    <!--div class="col-sm-6">
                            <ul class="socialNetwork pull-right">
                                    <li><a href="#" class="tips" title="follow me on Facebook"><i class="icon-facebook-1 iconRounded"></i></a></li>
                                    <li><a href="https://twitter.com/LIttleNeko1" class="tips" title="follow me on Twitter" target="_blank"><i class="icon-twitter-bird iconRounded"></i></a></li>
                                    <li><a href="#" class="tips" title="follow me on Google+"><i class="icon-gplus-1 iconRounded"></i></a></li>
                                    <li><a href="#" class="tips" title="follow me on Linkedin"><i class="icon-linkedin-1 iconRounded"></i></a></li>
                                    <li><a href="#" class="tips" title="follow me on Dribble"><i class="icon-dribbble iconRounded"></i></a></li>
                            </ul>     
                    </div-->
                </div>
            </div>
        </section>
        <!-- End footer -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

    </body>
</html>


