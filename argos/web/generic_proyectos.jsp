<%@page import="co.org.invemar.siam.sibm.vo.CamposBusquedaEstaciones"%>
<%@page import="co.org.invemar.siam.sibm.vo.Departamentos"%>
<%@page contentType="text/html; charset=iso-8859-1" language="java" session="true"  import="co.org.invemar.user.UserPermission" %>
<%@page import="java.util.*"%>
<%@page import="co.org.invemar.util.ConnectionFactory"%>
<%@page import="co.org.invemar.siam.sibm.model.ProyectosDM"%>
<%@page import="java.sql.Connection"%>
<%
    response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server
%>

<%
    String username = null, projectname = null, projectid = null, projecttitle = null;

    /*
     * username = session.getAttribute("username").toString(); projectid =
     * session.getAttribute("projectid").toString(); projectname =
     * session.getAttribute("projectname").toString(); projecttitle =
     * session.getAttribute("projecttitle").toString(); UserPermission userp =
     * null;
     */
    ConnectionFactory cFactory = null;
    Connection connection = null;
    cFactory = new ConnectionFactory();
    ProyectosDM pdm = new ProyectosDM();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>.::Sistema de Informaci&oacute;n Ambiental Marina::.</title>       
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <META name="keywords" content="invemar, ciencia marina, investigac&iacute;on, marino, costera, costero">
        <meta name="keywords" content="Sistema de Informaci�n Ambiental Marina de Colombia, biolog�a, bases de datos, metadatos, SIAM, SIAC, corales, Colombia, calidad ambiental, monitoreo, REDCAM, INVEMAR, mapas, SIMAC, SISMAC">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">
        <link href="css/siamcss.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/cmar.css" type="text/css">
        <script type="text/javascript" language="javascript" src="js/estacionesCam/jquery-1.7.2.min.js"></script>
        <script type="text/javascript">
            <!--

            //-->
            var nombrecampo = null;
            function PopWindow(popPage,windowName,winWidth,winHeight)
            {
   
                //the center properties
                var winLeft = (screen.width -winWidth)/2;
                var winTop = (screen.height -winHeight)/2;

                newWindow = window.open(popPage,windowName,'width=' + winWidth + ',height='+ winHeight + ',top=' + winTop +',left=' + winLeft + ',resizable=yes,scrollbars=yes,status=yes');
            }
          
            function limpiarParametros() {
                $('#filtros31').html('');
                $('#sql1').html('');
                $('#sql2').html('');
                $('#selcampos').val(0); 
               
            }
            function generarReporte() {
                agregar('and');
                var querysql =  $('#sql2').html();       
                var idproyecto   =  $('#codigoproyecto').val();   
                
               
                
                $.ajax({
                    type: "post",
                    url: "<%=request.getContextPath()%>/services",                   
                    data: "action=ListaEstacionesPorProyecto&proy="+idproyecto+"&sql="+querysql,
                    beforeSend: function(msg){ 
                        $('#listaestaciones').html("<div style='margin:auto;text-align:center'><img border='0'   style='margin-right:32px; margin-top: 32px;' src='images/ajaxloader.gif' alt='Loading...'/></div>");
  
                    },
                    success: function(msg){
                        $('#listaestaciones').html('');
                        $('#listaestaciones').html(msg);            

                    },
                    error: function(xml,msg){
                        $('#listaestaciones').html('<span style="color:red;margin:auto;text-align:center"><Error cargando las estaciones.</span>');
                  
                    }
                });
                
            }
           
            
            

            function CallLlenarComboValores() {
                var parametrobusqueda = document.getElementById('selcampos').value;
                LLenarComboValores(parametrobusqueda);
            }
            
            function  LLenarComboValores(campo) {
                var idproyecto   =  $('#codigoproyecto').val();
                
                nombrecampo = campo;
                $.ajax({
                    type: "post",
                    url: "<%=request.getContextPath()%>/services",                   
                    data: "action=ListaValoresCampoPorProyecto&proy="+idproyecto+"&campo="+campo,
                    beforeSend: function(msg){ 
                        $('#filtros31').html("<div style='margin:auto;text-align:center'><img border='0'   style='margin-right:32px; margin-top: 32px;' src='images/ajaxloader.gif' alt='Loading...'/></div>");
  
                    },
                    success: function(msg){
                        $('#filtros31').html('');
                        $('#filtros31').html(msg);            

                    },
                    error: function(xml,msg){
                        $('#filtros31').html('<span style="color:red;margin:auto;text-align:center"><Error cargando los valores del campo.</span>');
                  
                    }
                });
                
            }
            
            function  agregar(operadorcondicional) {
                var selcampostextoseleccionado =  $("#selcampos option:selected").text();
                var selcamposvalorseleccionado =  $("#selcampos").val();
                var textoseleccionado =  $("#selparametro option:selected").text();
                var valorseleccionado =  $("#selparametro").val();
                var operador =  $("#operador").val();
                var caracter = '\'';
                var operadorNatural =null;
               
                if(operadorcondicional=='or'){
                    operadorNatural ='O';
                }else{
                    operadorNatural='Y';
                }
                if(selcamposvalorseleccionado!='-'){
                    if(valorseleccionado=='null'){                        
                        operador=' is ';
                        caracter ='';
                        $('#sql1').html(' '+operadorNatural+' '+selcampostextoseleccionado+operador+textoseleccionado+'');
                        $('#sql2').html(' '+operadorcondicional+' '+selcamposvalorseleccionado+operador+caracter+valorseleccionado+caracter);    
                    }else{                        
                        $('#sql1').html(' '+operadorNatural+' '+selcampostextoseleccionado+operador+textoseleccionado+'');
                        $('#sql2').html(' '+operadorcondicional+' '+selcamposvalorseleccionado+operador+caracter+valorseleccionado+caracter);     
                    }
                   
                }else{
                    $('#sql1').html('');
                    $('#sql2').html(''); 
                }
                
                
            }
            
            /*function agregar(operadorcondicional) {
                var textoseleccionado =  $("#selparametro option:selected").text();
                var valorseleccionado =  $("#selparametro").val();
                var operador =  $("#operador").val();
                
                
                var operadorNatural =null;
               
                if(operadorcondicional=='or'){
                    operadorNatural ='O';
                }else{
                    operadorNatural='Y';
                }
                
                if ($('#selcampos').val()=="Vigente"){         
                    $('#sql1').html(' '+operadorNatural+' Vigente'+operador+textoseleccionado+'');
                    $('#sql2').html(' '+operadorcondicional+' vigente_loc'+operador+valorseleccionado);
                }               
                
                if ($('#selcampos').val()=="Departamento"){                  
                    $('#sql1').html(' '+operadorNatural+' Departamento'+operador+textoseleccionado+'');
                    $('#sql2').html(' '+operadorcondicional+' depto_loc'+operador+valorseleccionado);
                }
                if ($('#selcampos').val()=="sector"){                  
                    $('#sql1').html(' '+operadorNatural+' Sector'+operador+textoseleccionado+'');
                    $('#sql2').html(' '+operadorcondicional+' ecorregion_loc'+operador+valorseleccionado+'');
                }
            }*/
            function setSelectvigente() {
                $('#filtros31').html('');
                $('#filtros31').append('<select id="selparametro" name ="selparametro" >');
                $('#selparametro').append('<option  value="\'S\'">Si</option>' );
                $('#selparametro').append('<option  value="\'N\'">No</option>' );
                $('#filtros31').append('</select>');
              
            }
            /* function getSectorByDepartament() {
                var codigoDepart =  $('#selDepart').val();          
                var idproyecto   =  $('#idproyecto').val();         
                $.ajax({
                    type: "post",
                    url: "<%=request.getContextPath()%>/services",                   
                    data: "action=SectoresPorDepartamento&codDepar="+codigoDepart+"&idproyecto="+idproyecto,
                    beforeSend: function(msg){ 
                        $('#sector').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='images/ajaxloader.gif' alt='Loading...'/>");
  
                    },
                    success: function(msg){
                        $('#sector').html('');
                        $('#TitleSecondParameter').html('Sectores con monitoreo');
                        $('#sector').html(msg);                 

                    },
                    error: function(xml,msg){
                        $('#sector').html('<span style="color:red;margin:auto;text-align:center"><Error cargando los sectores monitoreados.</span>');
                  
                    }
                });
            }
            function getSectorByProyecto() {
                var codigoproyecto =  $('#codigoproyecto').val();         
                    
                $.ajax({
                    type: "post",
                    url: "<%=request.getContextPath()%>/services",                   
                    data: "action=SectoresPorProyecto&idproyecto="+codigoproyecto,
                    beforeSend: function(msg){ 
                        $('#filtros31').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='images/ajaxloader.gif' alt='Loading...'/>");
  
                    },
                    success: function(msg){
                        $('#filtros31').html('');  
                        $('#filtros32').html('Valor del campo:');
                        $('#filtros31').html(msg);                     

                    },
                    error: function(xml,msg){
                        $('#filtros31').html('<span style="color:red;margin:auto;text-align:center"><Error cargando los departamentos monitoreados...</span>');
                  
                    }
                });
            }
            function getDepartamentosByProyecto() {
                var codigoproyecto =  $('#codigoproyecto').val();         
                
                $.ajax({
                    type: "post",
                    url: "<//%=request.getContextPath()%>/services",                   
                    data: "action=DepartamentosPorProyecto&idproyecto="+codigoproyecto,
                    beforeSend: function(msg){ 
                        $('#filtros31').html("<img border='0'   style='margin-right:32px; margin-top: 32px;' src='images/ajaxloader.gif' alt='Loading...'/>");
  
                    },
                    success: function(msg){
                        $('#filtros31').html('');
                        $('#filtros32').html('Valor del campo:');
                        $('#filtros31').html(msg);                     

                    },
                    error: function(xml,msg){
                        $('#filtros31').html('<span style="color:red;margin:auto;text-align:center"><Error cargando los departamentos monitoreados...</span>');
                  
                    }
                });
            }
            /*function buscarEstaciones() {
                var codigoDepart =  $('#selDepart').val();       
                var codigoSector =  $('#selSector').val();     
                
               
                
                $.ajax({
                    type: "post",
                    url: "<//%=request.getContextPath()%>/services",                   
                    data: "action=listaEstacionesCam&proy=2050&codSector="+codigoSector+"&codDepar="+codigoDepart,
                    beforeSend: function(msg){ 
                        $('#listaestaciones').html("<div style='margin:auto;text-align:center'><img border='0'   style='margin-right:32px; margin-top: 32px;' src='images/ajaxloader.gif' alt='Loading...'/></div>");
  
                    },
                    success: function(msg){
                        $('#listaestaciones').html('');
                        $('#listaestaciones').html(msg);                 
                        $('.dataTable').dataTable();  
                    },
                    error: function(xml,msg){
                        $('#listaestaciones').html('<span style="margin:auto;text-align:center;color:red">Error listando las estaciones.</span>');
                  
                    }
                });
            }*/
        </script>
    </head>

    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        
        <!--
        <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
          <tbody>
            <tr class="tdcabezote">
              <td width="3"><img src="images/borde_l.gif" alt="" name="borde_l"
                width="3" height="81" border="0" id="borde_l"></td>
              <td width="105">
                        <img name="logo" src="imagenAction?codigo=<%=request.getParameter("proy")%>" border="0" alt=""></td>
              <td width="542"><p class="titulohead">
                &nbsp;&nbsp;</p>
              </td>
              <td align="right" >&nbsp;</td>
              <td align="right">&nbsp;</td>
              <td align="right" >&nbsp;</td>
              <td colspan="2" align="right" valign="top"><img
                name="borde_r" src="images/borde_r.gif" width="3" height="81"
                border="0" alt=""></td>
            </tr>
            <tr>
              <td width="3"></td>
              <td width="105"></td>
              <td></td>
              <td width="28"></td>
              <td width="29"></td>
              <td width="23"></td>
              <td width="12"></td>
              <td width="8"></td>
            </tr>
          </tbody>
        </table>
        -->
        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
            </tbody>
        </table>

        <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>

                <tr>
                    <td colspan="3"><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
            </tbody>
        </table>

        <jsp:directive.page import="co.org.invemar.siam.sibm.vo.Localidad"/>
        <jsp:directive.page import="co.org.invemar.siam.sibm.vo.Proyecto"/>
        <jsp:directive.page import="org.apache.commons.lang.*"/>
        <div>

            <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td><img src="images/spacer.gif" width="5" height="5"></td>
                    </tr>
                </tbody>
            </table>
            <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">

                <tr class="tabla_fondotitulo2">
                    <td width="9">&nbsp;</td>
                    <td width="503" class="texttablas">Ver estaciones </td>
                    <td width="238" class="texttablas"><div align="right"></div></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>

                <tr>
                    <td height="16" colspan="3" >
                        <div align="center" class="linksnavinferior">
                            <form method="get" action="services" id="formproys">
                                <table cellpadding="0" border="0"><tr>
                                        <td><span class="linksnegros">Seleccione el proyecto</span></td>
                                        <td>
                                            <%
                                                String proj = request.getParameter("proy");
                                                List<Proyecto> projects = (List) request.getAttribute("PROYS");
                                                Proyecto proyecto = null;
                                                if (proj == null) {
                                                    proyecto = projects.get(0);
                                            %>
                                            <select name="proy" onChange="javascript:document.getElementById('formproys').submit();">
                                                <%for (Proyecto project : projects) {%>
                                                <option value="<%=project.getCodigo()%>"><%=project.getNombreAlterno()%></option>
                                                <%}%>
                                            </select>
                                            <%} else {%>
                                            <select name="proy" onChange="javascript:document.getElementById('formproys').submit();">
                                                <%for (Proyecto project : projects) {
                                                        if (String.valueOf(project.getCodigo()).equals(proj)) {
                                                            proyecto = project;
                                                %>
                                                <option value="<%=project.getCodigo()%>" selected="selected"><%=project.getNombreAlterno()%></option>
                                                <%		} else {%>
                                                <option value="<%=project.getCodigo()%>" ><%=project.getNombreAlterno()%></option>
                                                <%}%>

                                                <%}%>
                                            </select>
                                            <%}%>
                                        </td>
                                    </tr></table>
                                <input type="hidden" name="action" value="GenericProyecto" /> 
                            </form>
                        </div></td>
                </tr>

            </table>
            <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr>
                    <td background="images/dotted_line.gif"><img src="images/dotted_line.gif" width="12" height="3"></td>
                </tr>
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
            </table>


            <table width="963px" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr class="tabla_fondotitulo2"> 
                    <td  >
                        <table width="100%" border="0" cellspacing="0">
                            <td height="24"  align="left" class="stetptitle">Proyecto </td>
                            <td width="100%" align="right">

                                <%if (proyecto.getMetadato() != 0) {%>
                                <a class="asistanttitle" target="_blank" href="http://cinto.invemar.org.co/metadatos/showMetadato.jsp?conjunto=<%=proyecto.getMetadato()%>">Ver metadatos <img src="images/metadata.png" width="24" height="24" align="top" style="border:0px"></a>
                                    <%}%>
                            </td>
                        </table>
                    </td>
                </tr>
            </table>


            <table width="963px" border="0" align="center" cellpadding="4" cellspacing="1">


                <tr class="tabla_fondotitulo2">
                    <td nowrap="nowrap" class="texttitulo">Titulo</td>
                    <td nowrap="nowrap" class="texttitulo">Nombre alterno</td>
                    <td nowrap="nowrap" class="texttitulo">Periodo de ejecucion</td>
                </tr>
                <tr class="tabla_fondopagina">
                    <td class="texttablasreg"><%=proyecto.getTitulo()%></td>
                    <td nowrap="nowrap" class="texttablas" ><%=proyecto.getNombreAlterno()%></td>
                    <td nowrap="nowrap" class="texttablas" ><%=proyecto.getFechaInicio()%></td>
                </tr>        

                <tr class="tabla_fondopagina"><td colspan="3" style="text-align: center"> Filtros </td></tr>
                <tr class="tabla_fondopagina" >
                    <td class="texttablasreg" colspan="3" > 
                        <div id="filtros">
                            <div id="filtro1" style="float:left;padding-right: 20px;">
                                <span>Seleccione el campo de b&uacute;queda</span>
                                <input type="hidden" id="codigoproyecto"  name="codigoproyecto" value="<%=proyecto.getCodigo()%>"  />

                                <div id="filtro11">
                                    <select id="selcampos" name ="selcampos" onchange='CallLlenarComboValores()'  > 
                                        <option value="-" selected="true" >-</option>
                                        <%
                                            connection = cFactory.createConnection("geograficos");
                                            List<CamposBusquedaEstaciones> listacamposBusquedaEstaciones = pdm.findCamposBusquedaEstaciones(connection);

                                            Iterator<CamposBusquedaEstaciones> it = listacamposBusquedaEstaciones.iterator();
                                            while (it.hasNext()) {
                                                CamposBusquedaEstaciones campo = it.next();
                                                out.println("<option value='" + campo.getDescripcioncampo() + "' >" + campo.getEtiqueta() + "</option>");
                                            }
                                        %>


                                    </select>
                                </div>
                            </div>
                            <div id="filtros2" style="float:left;padding-right: 30px;">
                                <span>Seleccione del operador:</span>
                                <div id="filtros21">
                                    <select id='operador' name ='operador'  onchange='getSectorValores()'> 
                                        <option value='=' selected='true'> Igual</option>
                                        <option value='!='>Diferente</option>
                                    </select>   
                                </div>
                            </div>
                            <div id="filtros3" style="float:left;padding-right: 30px;">
                                <span id="filtro32"></span>
                                <div id="filtros31">

                                </div>
                            </div>
                            <div id="filtros4" style="float:left;">
                                <span></span>
                                <div id="filtros41">

                                </div>
                                <div id="filtra42">

                                </div>                                     
                            </div>
                            <div id="filtros4" style="float:left;padding-top: 20px;">

                                <div id="filtros41">


                                </div>

                            </div>
                        </div>                            
                    </td>
                <tr style="display:none">
                    <td class="tabla_fondopagina" colspan="3">
                        <div id="sql1" name="sql1" style="border: 2px solid #404040;background-color: #ffffff;display: none;" >

                        </div>
                    </td>
                </tr>
                <tr style="display:none">
                    <td class="tabla_fondopagina" colspan="3">
                        <div id="sql2" name="sql2" style="border: 2px solid #404040;background-color: #ffffff;display:none" >

                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tabla_fondopagina" colspan="3" style="text-align: center">
                        <input type="button" id="buscar" value="Buscar / Search" onclick="generarReporte()" />
                        <input type="button" id="btnagregarY" value="Limpiar parametros" onclick="limpiarParametros()" /> 
                    </td>

                </tr>

            </table>
            <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr>
                    <td background="images/dotted_line.gif"><img src="images/dotted_line.gif" width="12" height="3"></td>
                </tr>
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
            </table>
            <%

                int contadorEstaciones = 0;
                try {
                    connection = cFactory.createConnection();
                    List<Localidad> estaciones = pdm.findStationByProject(connection, proyecto.getCodigo());
                    System.out.println(estaciones.size());
            %>
            <div id="listaestaciones">
                <table align="center" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td colspan="29" >
                                <table border="0" width="100%" cellspacing="0">
                                    <tr class="tabla_fondopagina">
                                        <td  width="200" height="24"  align="left" class="linksnegros">Estaciones Asociadas: <%=estaciones.size()%></td>
                                        <td width="300"><img src="./images/browser.png" border="0" style="vertical-align:middle;"/><a class="linksnegros" href="javascript:PopWindow('estacionmap.jsp?proyecto=<%=proyecto.getCodigo()%>&nombre=<%=proyecto.getNombreAlterno()%>','Estaciones','890','600')">Ver distribuci&oacute;n espacial aqu&iacute;</a></td>
                                        <td ><img src="./images/Excel_icon.png" border="0" style="vertical-align:middle;"/><a class="linksnegros" href="./services?action=ProyectoExcel&proy=<%=proyecto.getCodigo()%>&name=<%=proyecto.getNombreAlterno()%>"> Descargar toda la informaci&oacute;n en formato excel</a></td>
                                    </tr>				
                                </table>
                            </td>
                        </tr>

                        <tr class="tabla_fondotitulo2">
                            <td nowrap="nowrap" class="titulostablas">Vigente</td>
                            <td nowrap="nowrap" class="titulostablas">Estacion</td>
                            <%
                                if (proyecto.getCodigo() == 2050) {
                                    out.println("<td nowrap='nowrap' class='titulostablas'>Sector</td>");
                                } else {
                                    out.println("<td nowrap='nowrap' class='titulostablas'>Ecorregi&oacute;n</td>");
                                }
                            %>

                            <td nowrap="nowrap" class="titulostablas">Top&oacute;nimo</td>
                            <td nowrap="nowrap" class="titulostablas">Zona protegida</td>
                            <td nowrap="nowrap" class="titulostablas">Descripcion</td>
                            <td nowrap="nowrap" class="titulostablas">Pais</td>
                            <td nowrap="nowrap" class="titulostablas">Departamento</td>
                            <td nowrap="nowrap" class="titulostablas">Municipio</td>                          
                            <td nowrap="nowrap" class="titulostablas">Lugar</td>
                            <td nowrap="nowrap" class="titulostablas">Latitud de inicio</td>
                            <td nowrap="nowrap" class="titulostablas">Latitud de fin</td>
                            <td nowrap="nowrap" class="titulostablas">Longitud de inicio</td>
                            <td nowrap="nowrap" class="titulostablas">Longitud de fin</td>
                            <td nowrap="nowrap" class="titulostablas">Cuerpo de agua</td>
                            <td nowrap="nowrap" class="titulostablas">Distancia de costa</td>
                            <td nowrap="nowrap" class="titulostablas">Tipo de costa</td>
                            <td nowrap="nowrap" class="titulostablas">Profundidad</td>


                            <td nowrap="nowrap" class="titulostablas">Barco</td>
                            <td nowrap="nowrap" class="titulostablas">Campa&ntilde;a</td>
                            <td nowrap="nowrap" class="titulostablas">Sustrato</td>
                            <td nowrap="nowrap" class="titulostablas">Ambiente</td>
                            <td nowrap="nowrap" class="titulostablas">Notas</td>
                            <td nowrap="nowrap" class="titulostablas">Mar o R&iacute;o</td>
                            <td nowrap="nowrap" class="titulostablas">Salinidad</td>
                            <td nowrap="nowrap" class="titulostablas">Profundidad min (Metros)</td>
                            <td nowrap="nowrap" class="titulostablas">Profundidad max (Metros)</td>


                        </tr>
                        <%for (Localidad estacion : estaciones) {
                                contadorEstaciones++;
                        %>
                        <tr class="tabla_fondopagina" id="rowestaciones">
                            <td nowrap="nowrap" class="texttablas"><%=estacion.getVigente()%></td>
                            <td nowrap="nowrap" class="texttablas"><span style="background-color:#E6E6E6;color:#E6E6E6;font-size: 9px;padding-right: 35px;"><%=estacion.getSecuencialoc()%></span>  <%=estacion.getPrefijoEstacion() == "" ? "" : estacion.getPrefijoEstacion() + "-"%><%=estacion.getCodigoEstacion()%></td>
                            <td nowrap="nowrap" class="texttablas"><%=estacion.getEcorregion()%></td>
                            <td nowrap="nowrap" class="texttablas"><%=estacion.getToponimo()%></td>
                            <td nowrap="nowrap" class="texttablas"><%=estacion.getZonaProtegida()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getDescripcionEstacion()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getPais()%></td>

                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getDepartamento()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getMunicipio()%></td>                                               
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getLugar()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getLatitudInicio()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getLatitudFin()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getLongitudInicio()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getLongitudFin()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getCuerpoAgua()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getDistanciaCosta()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getTipoCosta()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getHondoAgua()%></td>


                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getBarco()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getCampana()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getSustrato()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getAmbiente()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getNotas()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getMarrio()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getSalodulce()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getProfMin()%></td>
                            <td nowrap="nowrap"  class="texttablas"><%=estacion.getProfMax()%></td>


                        </tr>
                        <%}%>

                    </tbody>
                </table>
                <%} catch (Exception ex) {
                        System.out.println("Error renderizando las estaciones" + ex.toString());
                    } finally {
                        ConnectionFactory.closeConnection(connection);
                    }%>

            </div>

    </body>
</html>