<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true"  import="co.org.invemar.user.UserPermission" %>
<%
    String username = null;
    UserPermission userp = null;
    String projectid = null, projectname = null, projecttitle = null;
    String CSS = "siamcss.css";

    try {
        username = session.getAttribute("username").toString();
        projectid = session.getAttribute("projectid").toString();
        projectname = session.getAttribute("projectname").toString();
        projecttitle = session.getAttribute("projecttitle").toString();
        userp = (UserPermission) session.getAttribute("userpermission");
        CSS = (String) session.getAttribute("CSSPRO");

    } catch (Exception e) {
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%                     }

    if (username == null) {
%>
<script type="text/javascript">location.href = "login.jsp"</script>
<%                     }
    try {
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>Corredor Marino de Conservaci&oacute;n del Pac&iacute;fico Este Tropical</title>
        <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
        <meta http-equiv="description" content="this is my page">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
       <link href="css/<%=CSS %>" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/cmar.css" type="text/css">
        <script type="text/javascript" src="js/lib/prototype.js"></script>
        <script type="text/javascript" src="js/lib/controlmodal.js"></script>
        <script type="text/javascript" src="js/userpanel.js"></script>
    </head>

    <body marginwidth="0" marginheight="0">

        <%@ include file="bannerp.jsp" %>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr class="tabla_fondotitulo">
                    <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
                    <td width="364" class="linksnegros">Cont&aacute;ctenos</td>
                    <td width="365" class="texttablas"><div align="right"><img src="images/active.gif" width="16" height="16" border="0" align="absmiddle"><%=username%></div></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
            <tbody>
                <tr>
                    <td>
                        <table width="100%" border="0" align="center" cellpadding="0"
                               cellspacing="0">
                            <tbody>
                                <tr>
                                    <td background="images/dotted_line.gif"><img
                                            src="images/dotted_line.gif" width="12" height="3"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><img src="images/spacer.gif" width="3" height="3"></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tabla_fondopagina">
            <tbody>
                <tr class="tabla_fondotitulo2">
                    <td width="7"> </td>
                    <td width="370" class="texttablas"><img src="images/home.png" width="16" height="16" border="0" align="absmiddle">&nbsp; <a href="index.jsp">Inicio</a> &gt; <a href="about.jsp" id="route">Cont&aacute;ctenos </a> </td>
                    <td width="371" class="texttablas"><div align="right"></div></td>
                </tr>
                <tr>
                    <td colspan="3" bgcolor="#FFFFFF"><img src="images/spacer.gif" width="5" height="5"></td>
                </tr>
                <tr>
                    <td colspan="3" ><p align="justify" class="texttablas"><br>
                        </p>      </td>
                </tr>
                <tr>
                    <td ><p align="justify" class="texttablas"><br>
                        </p></td>
                    <td colspan="2"  class="texttablas"><strong>&iquest;A quien contactar para saber m&aacute;s de  ARGOS?</strong></td>
                </tr>

                <tr>
                    <td ><p align="justify" class="texttablas"><br>
                        </p></td>
                    <td colspan="2"  class="texttablasreg">

                        <p class="textnormal"><img src="images/fondo_morro1.jpg" width="142" height="200" align="left">Si
                            desea obtener mayor informaci&oacute;n sobre ARGOS, puede contactar
                            a las siguientes personas.</p>                       
                        <p class="textnormal"><a href="mailto:daniel.gonzalez@invemar.org.co">Daniel Gonz&aacute;lez</a><br>
                            Investigador, Desarrollo, Administraci&oacute;n SIAM.</p>
                        <p class="textnormal"> <a href="mailto:sinam@invemar.org.co">Julio Bohorquez Naranjo</a><br>
                            Coord. Bases de Datos, Desarrollo, Administraci&oacute;n SIAM. </p>
                        <p class="textnormal"><a href="mailto:psierra@invemar.org.co">Paula Sierra
                                Correa</a><br>
                            Coord. Programa de Investigaci&oacute;n para la Gesti&oacute;n Marina (GEZ). </p>
                    </td>
                </tr>


                <tr>
                    <td colspan="3" ><p align="justify">&nbsp;</p></td>
                </tr>
            </tbody>
        </table>

        <%@ include file="footer.jsp" %>

        <%
        } catch (Exception e) {
        %>
        <script type="text/javascript">location.href = "login.jsp"</script>
        <%                     }
        %>
    </body>
</html>