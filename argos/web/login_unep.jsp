<%@ page contentType="text/html; charset=iso-8859-1" language="java" session="true" %>
<%
		String username = null;
		
    	try{
        	username = session.getAttribute("username").toString();
        	   		
    	}catch(Exception e){
    	}
    	
    	if(username!=null){
		 %>
			<script type="text/javascript">location.href="index.jsp"</script>
		 <%
    	}

%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>ARGOS - Sistema de Soporte Multitem&aacute;tico para el Monitoreo Ambiental</title>
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="this is my page">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="css/2027.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <link rel="stylesheet" href="css/cmar.css" type="text/css">
  <script type="text/javascript" src="js/lib/prototype.js"></script>
  <script type="text/javascript" src="js/lib/scriptaculous/scriptaculous.js?load=effects"></script>
  <script type="text/javascript" src="js/lib/controlmodal.js"></script>
  <script type="text/javascript" src="js/lib/yav/yav.js"></script>
  <script type="text/javascript" src="js/lib/yav/yav-config-es.js"></script>
  <script type="text/javascript" src="js/login.js"></script>
  <script type="text/javascript">
  
	var loginrules=new Array();
	loginrules[0]='email:correo electr&oacute;nico|required';
	loginrules[1]='email: |email';
	loginrules[2]='password:contrase&ntilde;a|required';
	
	var pregrules=new Array();
	pregrules[0]='prgemail:correo electr&oacute;nico|required';
	pregrules[1]='prgemail: |email';	
		
  </script>    
  
  </head>

<body marginwidth="0" marginheight="0">

<%@ include file="banner_unep.jsp" %>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#096A95">
      <td colspan="2" class="tdcabezote2">&gt;</td>
    </tr>
    <tr>
      <td colspan="2"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr class="tabla_fondotitulo">
      <td width="21"><img src="images/arrow2.gif" width="20" height="20"></td>
      <td width="729" class="linksnegros">Ingresar al sistema </td>
    </tr>
  </tbody>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
  </tbody>
</table>

<table width="750" border="0" align="center" cellpadding="1" cellspacing="1">
  <tbody>
    <tr>
      <td>
        <table width="100%" border="0" align="center" cellpadding="0"
        cellspacing="0">
          <tbody>
            <tr>
              <td background="images/dotted_line.gif"><img
                src="images/dotted_line.gif" width="12" height="3"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td><img src="images/spacer.gif" width="3" height="3"></td>
    </tr>
  </tbody>
</table>

<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr class="tabla_fondotitulo2">
      <td width="132" > </td>
      <td width="608"  class="texttablas"></td>
      <td width="10" > </td>
    </tr>
    <tr>
      <td colspan="3"><img src="images/spacer.gif" width="5" height="5"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#E6E6E6"><p align="justify" class="texttablas"><br>
          </p>
      </td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#E6E6E6">
        <form name="loginform" method="post" action="">
        <table width="612" class="texttablas" id="logintable" style="display:none">
          <tr>
            <td width="17">&nbsp;</td>
            <td width="583">Proporcione sus datos de usuario para ingresar al
              sistema: </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>Correo electr&oacute;nico: </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><label>
              <input name="email" type="text" id="email" size="30"> <span id="errorsDiv_email"/>
              </label>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>Contrase&ntilde;a:</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><label>
              <input name="password" type="password" id="password" size="30"> <span id="errorsDiv_password"/>
              </label>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input name="login" type="button" id="login" value="Ingresar">
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><a href="javascript:void(0);" class="texttablas" id="linkrecover">Olvid&eacute; mi
                contrase&ntilde;a.</a></td>
          </tr>
        </table>
        </form><form name="pregform" action="" method="get">
        <table width="613" class="texttablas" id="prgtable" style="display:none">
          <tr>
            <td>&nbsp;</td>
            <td><strong>Olvid&eacute; mi contrase&ntilde;a!</strong></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td width="17">&nbsp;</td>
            <td width="584">Proporcione su direcci&oacute;n de correo electr&oacute;nico: </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>Correo electr&oacute;nico:</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><label>
              <input name="prgemail" type="text" id="prgemail" size="30"> <span id="errorsDiv_prgemail"/>
              </label>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input name="prgbutton" type="button" id="prgbutton" value="Generar nueva contrase&ntilde;a">
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td> </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><a href="login.jsp" class="texttablas">Intentar nuevamente el
                ingreso al sistema.</a></td>
          </tr>
        </table>
        </form>
        <table width="743" class="texttablas">
          <tr>
            <td width="17">&nbsp;</td>
            <td width="714"><div class="texttablas"> <strong>    <font color="#FF0000"><div id="errorresult"></div></font>
	    <font color="#006699"><div id="okresult"></div></font></strong></div></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#E6E6E6"><p align="justify">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<%@ include file="footer.jsp" %>

</body>
</html>
