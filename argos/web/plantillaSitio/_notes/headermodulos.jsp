<%@ page import="co.org.invemar.siam.sitio.model.*"%>
<%@ page import="co.org.invemar.siam.sitio.vo.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%
      Calendar c = Calendar.getInstance();
      String dia = Integer.toString(c.get(Calendar.DATE));
      String mes = Integer.toString(c.get(Calendar.MONTH)+1);
      String annio = Integer.toString(c.get(Calendar.YEAR));
	  String fecha = dia+"/"+mes+"/"+annio;
   
        String misitio=request.getParameter("idsitio");
		
		String nombresitio=null;
		String nombrecorto = null;
		String urlConceptual = null;
        String urlsubportal           = null;
		
		if (misitio!=null){
        CComponentensSiamDAO micomponentesiam = new CComponentensSiamDAO();
        ArrayList list = micomponentesiam.AdministradoresComponente(misitio);
        Iterator it = list.iterator();
		
		while (it.hasNext()) {
		   CComponenteSiam cs = (CComponenteSiam)it.next();
           nombresitio  = cs.getNombreCompleto();
           nombrecorto  = cs.getNombreCorto();	
		   urlConceptual = cs.getUrlConceptual();
		   urlsubportal  = cs.getUrl();
		   
		}  
	}  
		
     	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>..:: INVEMAR ::..</title>

<% 
 
  int  idSitio=0;
  try{  
   idSitio = Integer.parseInt(request.getParameter("idsitio"));
  }catch(Exception e){
   //response.sendRedirect("index.jsp");
  
   
  }
  
  int  idSubsitio=0;
  try{
   idSubsitio  =Integer.parseInt(request.getParameter("idsubsitio"));
   
  }catch(Exception e){
   idSubsitio=0;
   
  }
  switch(idSitio) {
   
     case 1:// sismac
     %>
 <script type="text/javascript" src="../1ibre/scriptaculous-combo.js"></script>
<!--script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script-->
<script type="text/javascript" src="../1ibre/1ibre.js"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>
<script type="text/javascript" src="../index.js"></script>
<link type="application/jsonrequest" rel="datasource" name="registros">
     <%
       switch(idSubsitio){
           case 1:
           %>
           <script type="text/javascript" src="especieagruptp.js"></script>
           <link type="application/jsonrequest" href="especieagruptp-info.jsp" rel="datasource" name="info">
           <%
           break;
           case 2:
           %>
          <script type="text/javascript" src="especiegeogtp.js"></script> 
           <link type="application/jsonrequest" href="especiegeogtp-info.jsp" rel="datasource" name="info"> 
           <%
           break;
           case 3:
           %>
           <script type="text/javascript" src="especietp.js"></script> 
           <link type="application/jsonrequest" href="especietp-info.jsp" rel="datasource" name="info"> 
           <%
           break;
           case 4:
           %>
           <script type="text/javascript" src="estacionp.js"></script>
<link type="application/jsonrequest" href="estacionp-info.jsp" rel="datasource" name="info">
           <%
           break;
           case 5:
           %>
           <link type="application/jsonrequest" href="grupo.jsp" rel="datasource" name="registros"> 
           <%
           
           break;
           case 6:
           %>
            <script type="text/javascript" src="saludp.js"></script>
<link type="application/jsonrequest" href="saludp-info.jsp" rel="datasource" name="info">
<%
           break;
         }
     break;
	 case 2:// carga libreria de buscador searcharticulo.jsp
	 %>
	 <script type="text/javascript" src="./1ibre/prototype.js"></script>
	<script type="text/javascript" src="js/search.js"></script>
  
	 <%
	 break;
	 case 3://carga librerias comunes del sipein
	 %>
	 <script type="text/javascript" src="../1ibre/prototype.js"></script>
	 <script type="text/javascript" src="js/lib/excanvas/excanvas.js"></script>
     <script type="text/javascript" src="js/plotr_uncompressed.js"></script>
	 <script type="text/javascript" src="js/utilities.js"></script>
     <script type="text/javascript" src="js/sipein.js"></script> 
	 <%
	  switch(idSubsitio){
	    case 0://index sipein
		%>
      <script type="text/javascript"> 
      function openPopUp(url){var opt="fullscreen=1,toolbar=0,menubar=0,resizable=1,scrollbars=1,width=900,height=720";var ventana = window.open(url,"Guia de usuario",opt);}
      </script>
		<%
		break;
	    case 1://arte pesca
		%>
	    <script type="text/javascript">
	     Event.observe(window, 'load', function(event){	var page="AP";loadArtePesca(page);fillStartTime(page); });
	   </script>
      <script type="text/javascript"> 
      function openPopUp(url){var opt="fullscreen=1,toolbar=0,menubar=0,resizable=1,scrollbars=1,width=900,height=720";var ventana = window.open(url,"Guia de usuario",opt);}
      </script>
		<%
		break;
		case 2://grupo especie
		case 3://desembarcada de especie
        %>
		<script type="text/javascript">
	     Event.observe(window, 'load', function(event){
			var page="CDE";
			fillStartTime(page);
		 });
	</script>
		<%		
		break;
		case 4:		
		%>
		 <script type="text/javascript">
	     Event.observe(window, 'load', function(event){
			var page="AyE";
			loadArtePesca(page,true);
			fillStartTime(page);		
		 });
	</script>
		<%
		break;
		case 5:
		%>
		 <script type="text/javascript">
	     Event.observe(window, 'load', function(event){
			var page="CPUE";
			loadArtePesca(page,true);
			fillStartTime(page);
		 });
	</script>
		<%
		break;
	  }
	  case 4:
            switch(idSubsitio){
              case 1://Filtre, busque y vea el comportamiento de las variales en las estaciones monitoreadas.
              %>
	   <script type="text/javascript" src="lib/prototype/prototype.js"></script>
<script type="text/javascript" language="JavaScript">
function validar(){
  if(document.formacam.ano1.value != ''){
    if(!ValidarFecha(document.formacam.ano1.value)){
      return false;
    }
  }
  if(document.formacam.ano2.value != ''){
    if(!ValidarFecha(document.formacam.ano2.value)){
      return false;
    }
  }
  return true;
}

function abrirprj(){
  var nproy = document.formacam.nproy.value;
    document.location.href = "consul_estadisticas_estacion.jsp?nproy="+nproy ;
    }


function openPopup(url) {
myPopup = window.open(url,'popupWindow','width=530,height=280,scrollbars=yes,status=no');
  if (!myPopup.opener)
    myPopup.opener = self;
}


/**
 * 
 * @param {Object} value
 * @param {Object} page
 */
function fillVariable(value,page){
	var url="sv1.jsp?action=variable&estacion="+value;
	new Ajax.Request(url, 
					{method:"post", 
					onComplete:function(transport){
//					alert(transport);
						fillSelects('variable',transport,true);
					}
	});
}

function fillAnos(value,page){
	var url="sv1.jsp?action=ano&variable="+value+"&estacion="+$('estacion').value;
	new Ajax.Request(url, 
					{method:"post", 
					onComplete:function(transport){
						fillSelectsAnos('ano1','ano2',transport,true);
					}
	});
}

function fillSelects(id,transport,fline){
	//var opts=transport.responseText.evalJSON(true);
	
//	var responses=transport.responseXML;
	if(window.ActiveXObject){ // If IE Windows
	var responses = new ActiveXObject("Microsoft.XMLDOM");
	responses.loadXML(transport.responseText);
	} else {
		var responses = transport.responseXML;
	}

//	alert(responses);
	
//	console.log(responses);					
	var sel = $(id);
//	console.log(sel.id);
	
	sel.length=0;
	if(fline){
		sel.options[0]=new Option("--","");
	}
	
	var variables = responses.getElementsByTagName("variables")[0];
	
	  for (i = 0; i < variables.childNodes.length; i++) {
                  var variable = variables.childNodes[i];
                  var codigo = variable.getElementsByTagName("codigo")[0];
                  var nombre = variable.getElementsByTagName("nombre")[0];
                  appendOpcionesSelect(codigo.childNodes[0].nodeValue,nombre.childNodes[0].nodeValue,sel);
             }

}

function fillSelectsAnos(id1,id2,transport,fline){
	//var opts=transport.responseText.evalJSON(true);
	
	var responses=transport.responseXML;
//	console.log(responses);					
	var sel1 = $(id1);
	var sel2 = $(id2);
//	console.log(sel.id);
	
	sel1.length=0;
	sel2.length=0;
	if(fline){
		sel1.options[0]=new Option("--","");
		sel2.options[0]=new Option("--","");
	}
	
	var anos = responses.getElementsByTagName("anos")[0];
	
	  for (i = 0; i < anos.childNodes.length; i++) {
                  var ano = anos.childNodes[i];
//                  var codigo = variable.getElementsByTagName("codigo")[0];
//                  var nombre = variable.getElementsByTagName("nombre")[0];
                  appendOpcionesSelect(ano.childNodes[0].nodeValue,ano.childNodes[0].nodeValue,sel1);
                  appendOpcionesSelect(ano.childNodes[0].nodeValue,ano.childNodes[0].nodeValue,sel2);
             }

}

function appendOpcionesSelect(valor,name,pp)
    	{
		   var optionElement;
//			console.log(valor);
//			console.log(pp.name);
			optionElement=document.createElement("option");
			optionElement.setAttribute("value",valor);
			optionElement.appendChild(document.createTextNode(name));
			pp.appendChild(optionElement);
    	}
</script>
	   <%
              break;
              case 2: //Filtre y busque las estadísticas precalculadas de los parámetros predefinidos.
              %>
              	<script src="lib/prototype/prototype.js" type="text/javascript"></script>
		<script src="lib/excanvas/excanvas.js" type="text/javascript"></script>
		<script src="plotr.js" type="text/javascript"></script>

<!-- API para http://cinto.invemar.org.co/siam/redcam/ -->
<!--		<script 
    		type="text/javascript" src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAA-XBw67So1zoGdbqjgP_YAhTmykkScV0TwSmtcPnRJu4H87rq5hSik6yceX0iHVulA3fYkO6Xd_QpyA"></script>
-->

<!-- API para http://www.invemar.org.co/siam/redcam/ -->
		<script 
    		type="text/javascript" src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAsTAu6MkeiB4rKmk8oyx1gRSYigaDQ3w0lHoxjjtDCcSvGwTszBQ_djMdzCxwFPyDcoLsPGDih4f5bw"></script>
              <%
              break;
              case 3:
              %>
              <script src="validaciones.js">

</script>
<script language="JavaScript">

function validar(){
  if(document.formacam.ano1.value != ''){
    if(!ValidarFecha(document.formacam.ano1.value)){
      return false;
    }
  }
  if(document.formacam.ano2.value != ''){
    if(!ValidarFecha(document.formacam.ano2.value)){
      return false;
    }
  }

}
function abrirprj(){
  var nproy = document.formacam.nproy.value;
    document.location.href = "consul_estadisticas.jsp?nproy="+nproy ;
    }

function abrir1(){
  var vari = document.formacam.ntipo.value;
  var nproy = document.formacam.nproy.value;
  var opcion = document.formacam.opcion.value;
  var nuser = document.formacam.nuser.value;
  var nombre = 'est_variable.jsp?vari=' + vari;
  
  nombre = nombre + '&'+'nproy='+ nproy +'&nuser=' + nuser + '&opcion='+opcion;
  openPopup(nombre);

}
function abrir2(){
  var nproy = document.formacam.nproy.value;
  var opcion = document.formacam.opcion.value;
  var nuser = document.formacam.nuser.value;
  var ndepar = document.formacam.ndepar.value;
  var nombre = 'est_sector.jsp?ndepar=' + ndepar + '&nproy='+ nproy + '&opcion=' + opcion + '&nuser=' + nuser ;
  openPopup(nombre);
}
function seleccionarest(){
  var secto = document.formacam.nsector.value;
/*  var nproy = null;
  var opcion = null;
  var nuser = null;
*/  
//  var nombre = "siicam_estacion.jsp?sect=" + secto+ "&nproy="+null+"&nuser="+null+"&opcion="+null ;
  var nombre = "est_estacion.jsp?sect=" + secto+ "&nproy=REDCAM&nuser=ljarias&opcion=2" ;
  openPopup(nombre);
}
function openPopup(url) {
myPopup = window.open(url,'popupWindow','width=530,height=280,scrollbars=yes,status=no');
  if (!myPopup.opener)
    myPopup.opener = self;
}

</script>

              <%
              break;
            }
         break;
         case 6://sitio sibm
           switch(idSubsitio){
          case 1:
          %>
          <link type="application/jsonrequest" href="totales.jsp" rel="datasource" name="totales">
<script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>

<script type="text/javascript" src="../1ibre/1ibre.js"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>

<script type="text/javascript" src="jscript/validate.js"></script>

<script type="text/javascript">

	
function spacial(){
		
	var fr= $('advancedSearch');
	var old = fr.action;
  
  
  
  fr.action = "formespacial.jsp?idsitio=6&idsubsitio=4";
  fr.submit();
  fr.action = old;
  
	}

</script>
<script type="text/javascript"> 
function openPopUp(url){
	var opt="fullscreen=1,toolbar=0,menubar=0,resizable=1,scrollbars=1,width=900,height=720";
	var ventana = window.open(url,"Guia de usuario","width=900,height=720");

}

</script>

          <%
          break;
          case 2:
          %>
          <script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="../1ibre/1ibre.js"></script>

<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>
<script type="text/javascript" src="../index.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/AdvanceSearchUtil.js"></script>
  
<link type="application/jsonrequest" href="info.jsp" rel="datasource" name="info" >
<style type="text/css">
.selected {
  background-color: #ddd;
}
.steps {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
	background-color: #CCCCCC;
	
}
</style>

          <%
          break;
          case 3://listado especies          
          %>
          
        <script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="../1ibre/1ibre.js"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>
    
          <%
          break;
          case 4://form espacial
          %>
          <script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="../1ibre/1ibre.js"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>
<script type="text/javascript" src="../index.js"></script>
<script type="text/javascript" src="../js/select.js"></script>
<script type="text/javascript" src="../js/AdvanceSearchUtil.js"></script>
<script type="text/javascript">
	function toggleL(){var elm=$('leyendas');if(elm.style.display==='table-cell'){$('sh').innerHTML='Mostrar';elm.style.display='none';}else{$('sh').innerHTML='Ocultar';elm.style.display='table-cell';}}
</script>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAsTAu6MkeiB4rKmk8oyx1gRSYigaDQ3w0lHoxjjtDCcSvGwTszBQ_djMdzCxwFPyDcoLsPGDih4f5bw"
      type="text/javascript"></script>

<script type="text/javascript" src="maps.js"></script>
<link type="application/jsonrequest" href="info.jsp" rel="datasource" name="info" ibre-onload="load(this)">
<style type="text/css">
.selected {
  background-color: #ddd;
}
</style>
          <%
          break;
          case 5://documentos del sibm
          %>
          <script type="text/javascript" src="jscript/mootools-1.2-core.js"></script>
<script type="text/javascript" src="jscript/documents.js"></script>

          <%
          break;
	  case 6: //estadisticas del sibm.
          %>
          
          <%
          break;
	 case  7:
	 %>
	 <script type="text/javascript" src="../1ibre/scriptaculous-combo.js"></script>
<!--script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script-->
<script type="text/javascript" src="../1ibre/1ibre.js"></script>

<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>
<script type="text/javascript" src="../index.js"></script>
<script type="text/javascript" src="institutos.js"></script>
<link type="application/jsonrequest" href="institutos-info.jsp" rel="datasource" name="info">
<link type="application/jsonrequest" rel="datasource" name="registros">

	 <%
     break;	 
	 case 8:
	 %>
	 <script type="text/javascript" src="../1ibre/scriptaculous-combo.js"></script>
<script type="text/javascript" src="../1ibre/1ibre.js"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>
<script type="text/javascript" src="../index.js"></script>
<script type="text/javascript" src="investigadores.js"></script>
<link type="application/jsonrequest" href="investigadores-info.jsp" rel="datasource" name="info">
<link type="application/jsonrequest" rel="datasource" name="registros">
	 <%
	 break;
         case  9://sibmuseo.
          %>
		  <script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
<script type="text/javascript" src="../1ibre/1ibre.js"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>


<SCRIPT TYPE="text/javascript">
<!--
function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
href=mylink;
else
href=mylink.href;
window.open(href, windowname, 'width=400,height=200,scrollbars=no,toolbar=no,status=no,resizable=no');
return false;
}
//-->
</SCRIPT>
		  <%
         break;
		 case 10: //zsibfichaimagen
		 %>
		 <script type="text/javascript" src="../1ibre/prototype.js"></script>
<script type="text/javascript" src="../1ibre/scriptaculous/scriptaculous.js"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/lib/init.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/lib/lang.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/lib/dom.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/lib/ajax.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/lib/grammar.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/json/json.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/whatwg/whatwg.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/whatwg/lang/en.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/whatwg/parser.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/whatwg/css.js" type="text/javascript"></script>
<script src="http://siam.invemar.org.co/siam/1ibre/whatwg/validate.js" type="text/javascript"></script>
<script type="text/javascript" src="../1ibre/whatwg/lang/es.js"></script>
		 <%
		 break;
	 }
	break;
     
   }
%>
 <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>

 
    <script type="text/javascript">   
		
	
      
      var calcHeight = function() {
        var headerDimensions = $('#header-bar').height();
        $('#preview-frame').height($(window).height() - headerDimensions);
      }
      
      $(document).ready(function() {
        calcHeight();
        $('#header-bar a.close').mouseover(function() {
          $('#header-bar a.close').addClass('activated');
        }).mouseout(function() {
          $('#header-bar a.close').removeClass('activated');
        });
      });
      
      $(window).resize(function() {
        calcHeight();
      }).load(function() {
        calcHeight();
      });
    </script>
    
    <!--[if IE 6]>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#close-button').remove();
      });
    </script>
    <![endif]-->
    <!-- JAVASCRIPT FOR AND GOOGLE ANALYTICS -->
    
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-11834194-12']);
  _gaq.push(['_trackPageLoadTime']);
    _gaq.push(['_trackPageview']);
  
  _gaq.push(['b._setAccount', 'UA-11834194-36']);
  _gaq.push(['b._trackPageLoadTime']);
    _gaq.push(['b._trackPageview']);

  
  (function() {
    var ga = document.createElement('script');
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    ga.setAttribute('async', 'true');
    document.documentElement.firstChild.appendChild(ga);
  })();
</script>

 <link type='text/css' rel="stylesheet" href="../plantillaSitio/css/misiamccs.css"/>
</head>


<% 
 if (idSitio==4 && idSubsitio==2){
 %>  
 <body onload="load()" onunload="GUnload()">  
 <p>
   <%
 }else if (idSitio==6 && idSubsitio==4 ){
 %>
   <body onUnload='GUnload()'>
   <%
 }else{
 %>
   <body >
   <%
 }
 
%>
 <table width="963px" border="0" align="center" cellpadding="00" cellspacing="0" >
   <tr>
     <td height="56" colspan="2"><table width='1000px' border="0" align="center" cellpadding="00" cellspacing="0">
       <tr>
         <td height="23" colspan="2" rowspan="2" align="right"><a href="http://twitter.com/#!/siam_colombia" target="_blank"></a></td>
         <td width="231"><span style="text-align:right" class="headerTexto">Ultima Actualizacion:<%=fecha%>&nbsp;&nbsp;&nbsp;</span><span class="headerTexto">Visitas:14</span></td>
         <td width="409"><a href="http://twitter.com/#!/siam_colombia" target="_blank"><img border="0" src="../plantillaSitio/img/twitter.png" width="16" height="16" alt="twitter" /></a><a href="http://twitter.com/statuses/user_timeline/124202805.rss" target="_blank"><img  border="0" src="../plantillaSitio/img/rss.png" width="16" height="16" alt="rss" /></a><a href="http://siam.invemar.org.co/siam/mapadelsitio.jsp" class="headerTexto" target="_blank"> &nbsp; Mapa de Sitio/</a><a target="_blank" class="headerTexto" href='http://www.invemar.org.co/pciudadania.jsp' >Servicio al Ciudadano/</a><a href="http://siam.invemar.org.co/siam/contactenos.jsp?idsitio=<%=misitio%>" target="_blank" class='headerTexto' >Cont&aacute;ctenos</a></td>
         <td  width="117" rowspan="2"   class="out" id="boton1" onmouseover="this.className='over'" onmouseout="this.className='out'"><a href="http://siam.invemar.org.co/siam/index.jsp"  class='opcionmenuprincipal'>Inicio</a><br /></td>
         <td  width="122" rowspan="2"  class="out" id="boton3" onmouseover="this.className='over'" onmouseout="this.className='out'" ><a href="<%=urlConceptual%>" class='opcionmenuprincipal' target="_blank">Desarrollo<br />
           Conceptual <%=nombrecorto%></a></td>
         <td  width="115" rowspan="2"  class='out' id="boton4" onmouseover="this.className='over'" onmouseout="this.className='out'" ><a href="http://www.invemar.org.co/psubcategorias.jsp?idsub=182&amp;idcat=104" class='opcionmenuprincipal' target="_blank">LABSIS</a></td>
       </tr>
       <tr>
         <td><div id="google_translate_element"></div>
           <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'es',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
       </script>
           <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> </td>
         <td></td>
       </tr>
     </table></td>
   </tr>
   <tr >
     <td height="25" colspan="2"><table width="963px" border="0" cellpadding="00" cellspacing="0" class="headermodulos">
       <tr>
         <td width="293" height="85" align="center"><table width="256" border="0" cellpadding="00">
          <tr>
            <td width="33">&nbsp;</td>
            <td width="217"><img src="../plantillaSitio/img/icono_invemar.png" width="217" height="81" border='0' alt="icon invemar" longdesc="http://www.invemar.org.co" title="Pagina Principal de Invemar" /></td>
          </tr>
        </table></td>
         <td colspan="2" align="left" class="titulosubportal"><span class="titulosubportal nombrecortosubportal">-<%=nombrecorto%>- <%=nombresitio%></span></td>
       </tr>
       <tr>
         <td width="293" height="61" align="right" class="titulosubportal nombrecortosubportal">&nbsp;</td>
         <td width="354">&nbsp;</td>
         <td width="352"><div id="cse-search-form" style="width: 300px;">Loading</div>
           <script src="http://www.google.es/jsapi" type="text/javascript"></script>
           <script type="text/javascript"> 
  google.load('search', '1', {language : 'es'});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('009335532891503054452:wgboazck2um');
    customSearchControl.setResultSetSize(google.search.Search.LARGE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.enableSearchboxOnly("http://siam.invemar.org.co/siam/resultadobusqueda.jsp");
    customSearchControl.draw('cse-search-form', options);
  }, true);
       </script>
          <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" /></td>
       </tr>
     </table></td>
   </tr>
   <tr>
   <td align="right">&nbsp;</td>
   <td align="right"><table width="300" border="0" cellpadding="00">
     <tr>
       <td width="179" align="right"><a href="http://www.invemar.org.co/" class="migadepan">invemar.org.co &gt; </a><a href="http://siam.invemar.org.co/siam/index.jsp" class="migadepan">SIAM &gt; <%=nombrecorto%></a></td>
       <td width="15">&nbsp;</td>
     </tr>
   </table></td>
 </table>
