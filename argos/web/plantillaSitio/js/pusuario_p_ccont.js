// JavaScript Document

function validarRegistro(){

	//contrasena actual
	if(isNullOrEmpty(document.regForm.contrasenaact.value)){
		alert(checkAccents("Debe ingresar su contraseña en el campo correspondiente"));
		document.regForm.contrasena.focus();
		return false;					
	}

	//contrasena
	if(isNullOrEmpty(document.regForm.contrasena.value)){
		alert(checkAccents("Debe ingresar su contraseña en el campo correspondiente"));
		document.regForm.contrasena.focus();
		return false;					
	}
	
	//contrasena
	if(document.regForm.contrasena.value.length<5){
		alert(checkAccents("Debe ingresar una contraseña conformada al menos de cinco caracteres"));
		document.regForm.contrasena.focus();
		return false;					
	}	

	//confirmar contrasena
	if(isNullOrEmpty(document.regForm.ccontrasena.value)){
		alert(checkAccents("Debe ingresar la confirmación de su contraseña en el campo correspondiente"));
		document.regForm.ccontrasena.focus();
		return false;					
	}
	
	//
	if(document.regForm.contrasena.value != document.regForm.ccontrasena.value){
		alert(checkAccents("La contraseña ingresada y su confirmación no coinciden."));
		document.regForm.ccontrasena.focus();
		return false;						
	}
		
	return true;
	
}
