// JavaScript Document

var REG_ALFA = /^[a-z ._-]+$/i;
var REG_ALFANUM = /^[a-z0-9 ._-]+$/i;
var REG_DIG = /^[-+]?[0-9]+$/;
var REG_NDIG = /^[^0-9]+$/;
var REG_NUM	= /^[-+]?\d*\.?\d+$/;
var REG_EMAIL = /^([a-zA-Z0-9_\.\-\+%])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var REG_TELF = /^[\d\s ().-]+$/; // alternate regex : /^[\d\s ().-]+$/,/^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/
var REG_URL = /^(http|https|ftp)\:\/\/[a-z0-9\-\.]+\.[a-z]{2,3}(:[a-z0-9]*)?\/?([a-z0-9\-\._\?\,\'\/\\\+&amp;%\$#\=~])*$/i;

function validarCorreoe(correoe){
	
	return REG_EMAIL.test(correoe);
	
}

function validarNumeroTelefono(telefono){
	
	return REG_TELF.test(telefono);

}
