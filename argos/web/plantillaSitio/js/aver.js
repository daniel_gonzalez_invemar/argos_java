function validar(){
	if(document.forma.tipoqueja.value == ''){
    alert("Debe seleccionar la clase de inquietud");
    document.forma.tipoqueja.focus();
    return false;
  }
  if(document.forma.quejadores.value == ''){
    alert("Debe seleccionar un valor para la pregunta es usted?");
    document.forma.quejadores.focus();
    return false;
  }
  if(document.forma.temas.value == ''){
    alert("Debe seleccionar un tema");
    document.forma.temas.focus();
    return false;
  }	
  if(document.forma.inquietud.value == ''){
    alert("Debe digitar su inquietud");
    document.forma.inquietud.focus();
    return false;
  }	
  if(document.forma.nombre.value == ''){
    alert("Debe digitar su nombre y apellidos");
    document.forma.nombre.focus();
    return false;
  }
  if(!ValidarEmail(document.forma.email, "")){
    return false;
  }
  x =  document.forma.inquietud.value;
  if(x.length > 1000){
    alert("La longitud de la inquietud no debe superar los 1000 caracteres");
    document.forma.inquietud.focus()
    return false;
  }

  return true;
}
