// JavaScript Document

function validarIngreso(){

	//nombre de usuario
	if(isNullOrEmpty(document.loginForm.username.value)){
		alert(checkAccents("Debe ingresar su nombre de usuario en el campo correspondiente"));
		document.loginForm.username.focus();
		return false;					
	}

	//contrasena
	if(isNullOrEmpty(document.loginForm.password.value)){
		alert(checkAccents("Debe ingresar su contraseņa en el campo correspondiente"));
		document.loginForm.password.focus();
		return false;					
	}
			
	return true;
	
}
