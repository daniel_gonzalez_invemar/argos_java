// Common js

var INV_B_MZ = 1;
var INV_B_IE = 2;
var INV_B_OP = 3;
var INV_B_SF = 4;

var invBrowName;
var invBrowVer;

function invGetBrowserInfo(){

	var ua = navigator.userAgent.toString();

	var re_opera = /Opera.([0-9\.]*)/i;
	var re_msie = /MSIE.([0-9\.]*)/i;
	var re_gecko = /gecko/i;
	var re_safari = /(applewebkit|safari)\/([\d\.]*)/i;
	var r = false;

	if ( (r = ua.match(re_opera))) {
		invBrowName = INV_B_OP;
		invBrowVer = parseFloat(r[1]);
	} else if ( (r = ua.match(re_msie))) {
		invBrowName = INV_B_IE;
		invBrowVer = parseFloat(r[1]);
	} else if ( (r = ua.match(re_safari))) {
		invBrowName = INV_B_SF;
		invBrowVer = parseFloat(r[2]);
	} else if (ua.match(re_gecko)) {
		var re_gecko_version = /rv:\s*([0-9\.]+)/i;
		r = ua.match(re_gecko_version);
		invBrowName = INV_B_MZ;
		invBrowVer = parseFloat(r[1]);
	}
		
}

function addMenuBarStyleForIE(){

	/* HACK FOR IE: to stabilize appearance of menu items; the slash in float is to keep IE 5.0 from parsing */

	document.write("<style type='text/css'>");
	document.write("@media screen, projection");
	document.write("{");				
		document.write("ul.MenuBarVertical li.MenuBarItemIE");
		document.write("{");		
				document.write("display: block;");
			
			if(invBrowName == INV_B_IE && invBrowVer >= 6 && invBrowVer < 8)
				document.write("f\loat: left;");
			else
				document.write("f\loat: none;");
				
				document.write("background: #FFF;");
		document.write("}");
	document.write("}");
	document.write("</style>");

}

invGetBrowserInfo();
addMenuBarStyleForIE();

