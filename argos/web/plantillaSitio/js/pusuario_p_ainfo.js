// JavaScript Document

var cambieCaptcha = false;

function validarRegistro(){

	//nombre de usuario
	if(isNullOrEmpty(document.regForm.usuario.value)){
		alert(checkAccents("Debe ingresar su nombre de usuario en el campo correspondiente"));
		document.regForm.usuario.focus();
		return false;					
	}
	
	//nombre de usuario
	if(document.regForm.usuario.value.length<5){
		alert(checkAccents("Debe ingresar un nombre de usuario conformado al menos de cinco caracteres"));
		document.regForm.usuario.focus();
		return false;					
	}	
	
	//correo electr�nico
	if(!validarCorreoe(document.regForm.correoe.value)){
		alert(checkAccents("Debe ingresar un correo electr&oacute;nico v&aacute;lido en el campo correspondiente"));
		document.regForm.correoe.focus();
		return false;					
	}	

	//nombres
	if(isNullOrEmpty(document.regForm.nombre.value)){
		alert(checkAccents("Debe ingresar su nombre en el campo correspondiente"));
		document.regForm.nombre.focus();
		return false;					
	}
	
	//apellidos
	if(isNullOrEmpty(document.regForm.apellido.value)){
		alert(checkAccents("Debe ingresar su apellido en el campo correspondiente"));
		document.regForm.apellido.focus();
		return false;					
	}
	
	//pa�s de origen
	if(document.regForm.att1.value=="-1"){
		alert(checkAccents("Debe seleccionar su pa�s de origen"));
		document.regForm.att1.focus();
		return false;					
	}
	
	//n�mero de tel�fono
	if(!isNullOrEmpty(document.regForm.att2.value))
		if(!validarNumeroTelefono(document.regForm.att2.value)){
			alert(checkAccents("Debe ingresar un n�mero de tel�fono v�lido."));
			document.regForm.att2.focus();
			return false;					
		}

	//ocupaci�n
	if(document.regForm.att3.value=="-1"){
		alert(checkAccents("Debe seleccionar su ocupaci�n"));
		document.regForm.att3.focus();
		return false;					
	}
				
	return true;
	
}
