
function checkDates(){

	var sdate = document.getElementById("sdate");
	var edate = document.getElementById("edate");	

	if(!isNullOrEmpty(sdate.value) && !isNullOrEmpty(edate.value))
	{
		
		var splitedFecha = sdate.value.split("-");
		var fechaInicio = new Date(splitedFecha[1] + "/" + splitedFecha[0] + "/" + splitedFecha[2]);
		
		splitedFecha = edate.value.split("-");
		var fechaFin = new Date(splitedFecha[1] + "/" + splitedFecha[0] + "/" + splitedFecha[2]);
		
		if(fechaFin <= fechaInicio){
			alert("El rango de fechas es inválido, la fecha de finalización debe ser mayor que la de inicio");
			return false;
		}else{
			return true;
		}
	
	}else{
		
		alert("Debe seleccionar las fechas indicadas");
		return false;
		
	}
		
}	

