var pestado = 0 //pánel seleccionado, 0 ninguno, o el id de alguno de los páneles

function mostrarSector(sector){
	
	var sgobierno = document.getElementById("sgobierno");
	var sambiente = document.getElementById("sambiente");
	var svivienda = document.getElementById("svivienda");
	var universidades = document.getElementById("universidades");	
	var ongs = document.getElementById("ongs");

	//primero escondemos todos los páneles
	sgobierno.style.display = "none";
	sambiente.style.display = "none";
	svivienda.style.display = "none";
	universidades.style.display = "none";
	ongs.style.display = "none";
	
	if(sector==pestado){
		pestado = 0;
		return;
	}
	
	switch(sector){
	
		case 1 :
			sgobierno.style.display = "block";
			pestado = 1;
		break;
		
		case 2 :
			sambiente.style.display = "block";
			pestado = 2;
		break;
		
		case 3 :
			svivienda.style.display = "block";
			pestado = 3;
		break;
		
		case 4 :
			universidades.style.display = "block";
			pestado = 4;
		break;
		
		case 5 :
			ongs.style.display = "block";
			pestado = 5;
		break;		
	}

}
