<%@ page import="co.org.invemar.siam.sitio.model.CComponentensSiamDAO"%>
<%@ page import="co.org.invemar.siam.sitio.vo.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%

   Calendar c = Calendar.getInstance();
      String dia = Integer.toString(c.get(Calendar.DATE));
      String mes = Integer.toString(c.get(Calendar.MONTH)+1);
      String annio = Integer.toString(c.get(Calendar.YEAR));
	  String fecha = dia+"/"+mes+"/"+annio;
      String misitio=request.getParameter("idsitio");
	 
		
		String nombresitio=null;
		String nombrecorto = null;
        String urlConceptual = null;
		String urlsubportal           = null;
		String nombrefilial   = null;
        
		if (misitio!=null){
        CComponentensSiamDAO micomponentesiam = new CComponentensSiamDAO();
        ArrayList list = micomponentesiam.AdministradoresComponente(misitio);
        Iterator it = list.iterator();
		
		while (it.hasNext()) {
		   CComponenteSiam cs = (CComponenteSiam)it.next();
           nombresitio  = cs.getNombreCompleto();
           nombrecorto  = cs.getNombreCorto();	
           urlConceptual = cs.getUrlConceptual();
		   urlsubportal  = cs.getUrl();
		   nombrefilial = cs.getNombreFilial();
                 fecha = cs.getFechaultimaActualizacionComponente();
           }  
	}  
	

 String idsubsitio=request.getParameter("idsubsitio");
 
 

%>
<div class="centrado" >
<table width="996"  style="border:0px;width:963px;padding:0px:" >
  <tr  >
    <td height="20px" colspan="3"  style="text-align:center;"  >
    <div class="centrado" style="position:relative;top:0px;height:20px;">   
    <div style="float:left">
      <div  class="headerTexto">
      Fecha ultima Actualizacion:<%=fecha%>
      </div>
      <div style="">
      <div style="float:left" ><a href="http://siam.invemar.org.co/siam/mapadelsitio.jsp" class="headerTexto" target="_blank"> Mapa de Sitio |&nbsp; </a><a target="_blank" class="headerTexto" href='http://www.invemar.org.co/pciudadania.jsp' >Servicio al Ciudadano | </a><a href="https://spreadsheets0.google.com/embeddedform?formkey=dHB1ZHdIVk5nbEs0VlRNaW9VdjU2X0E6MA" target="_blank" class="headerTexto">D&iacute;ganos que Piensa |</a> <a href="http://siam.invemar.org.co/siam/contactenos.jsp?idsitio=<%=misitio%>" target="_blank" class='headerTexto' >&nbsp; Cont&aacute;ctenos</a> <a href="http://twitter.com/#!/siam_colombia" target="_blank"><img style="border:0px" src="plantillaSitio/img/twitter.png" width="16" height="16" alt="twitter" /></a> <a href="http://twitter.com/statuses/user_timeline/124202805.rss" target="_blank"><img  style="border:0px;"  src="plantillaSitio/img/rss.png" width="16" height="16" alt="rss" /></a></div>
      </div>
            
    </div>
    <div style="float:right">
    <div style="float:right;" id="btboton3" ><a href="http://www.invemar.org.co/psubcategorias.jsp?idsub=182&amp;idcat=104">labsis</a>
    </div>
   <div title="Desarrollo Conceptual" style="float:right;" id="btboton2" ><a href="http://www.invemar.org.co/archivo.jsp?id=4530&amp;red=true">Desarrollo <br/>Conceptual SIAM</a> 
    </div>
      
    <div  id="btboton1" style="float:right"><a  href="http://siam.invemar.org.co/siam/index.jsp">Inicio<br /></a>
    </div>
    </div>
    
    
    </div></td>
  </tr>
  <tr >
    <td height="25" colspan="4"><table width="989" style="border:0px;padding:0px;border-collapse:collapse" class="headermodulos">
      <tr>
        <td width="249"  style="text-align:center" height="178"><table width="240" style="border:0px;padding:0px;border-collapse:collapse"
                                                           height="159">
                <tr>
                  <td width="14" height="83">&nbsp;</td>
                  <td width="220" height="83">
                    <img src="plantillaSitio/img/icono_invemar.png"
                         width="217" height="81" alt="icon invemar"
                         longdesc="http://www.invemar.org.co"
                         title="Pagina Principal de Invemar"
                         style="border:0px"/>
                  </td>
                </tr><tr>
            <td width="14">&nbsp;</td>
            <td width="220">
                    <p>
                      &nbsp;
                    </p>
                    <p>
                      &nbsp;
                    </p>
                  </td>
          </tr>
        </table></td>
        <td colspan="2"  style="text-align:center" class="titulosubportal"
            height="178"><table width="676" style="border:0px;padding:0px;border-collapse:collapse">
          <tr>
              <td width="519"  class="titulosubportal nombrecortosubportal" style="text-align:center" >Sistema de Soporte Multitemático <br/> para el Monitoreo Ambiental</td>
              <td width="151" style="text-align:center">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td width="249"  class="titulosubportal nombrecortosubportal" style="text-align:right"></td>
        <td width="383">&nbsp;</td>
        <td width="327" style='padding-bottom:0px;vertical-align:bottom'>
		<%if ( !misitio.equals("34") && !misitio.equals("38") && !misitio.equals("30")){%>
        <div id="cse-search-form" style="width: 300px;">Loading</div>
          <script src="http://www.google.es/jsapi" type="text/javascript"></script>
          <script type="text/javascript"> 
  google.load('search', '1', {language : 'es'});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('009335532891503054452:wgboazck2um');
    customSearchControl.setResultSetSize(google.search.Search.LARGE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.enableSearchboxOnly("http://siam.invemar.org.co/siam/resultadobusqueda.jsp");
    customSearchControl.draw('cse-search-form', options);
  }, true);
      </script>
          <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" />
		<%	
		}%></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="15" height="25">&nbsp;</td>
    <td width="162"><%if ( !misitio.equals("34") && !misitio.equals("38") && !misitio.equals("30") ){%>
        <div id="google_translate_element" style="height:25px;width:150px"></div>
        <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'es',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
      </script>
        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><%
		  }else
		  {
			  %>
              <div  style="height:22px;width:150px"></div>
              <%
		  } 
		  
		  %></td>
 <td width="787"  style="text-align:right" ><a href="http://www.invemar.org.co/" class="migadepan">invemar.org.co &gt;  </a><a href="http://siam.invemar.org.co/siam/index.jsp" class="migadepan">SIAM&nbsp; &gt;</a><a href="http://cinto.invemar.org.co/argos/login.jsp" class="migadepan">ARGOS</a></td>
 <td width="13" style="text-align:right" >&nbsp;</td>
  </tr>
</table>
</div>